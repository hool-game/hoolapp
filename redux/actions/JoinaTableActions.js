import { initialState } from "../reducers/JoinaTableReducer";

export const JoinTableConstants = {
    ADD_PLAYER: 'ADD_PLAYER',
    ADD_PLAYER_SUCCESS:'ADD_PLAYER_SUCCESS',
    CANCEL_PLAYER_SELECT: 'CANCEL_PLAYER_SELECT',
    ADD_BOT: 'ADD_BOT',
    FRIENDS_LIST: 'FRIENDS_LIST',
    CLOSE_FRIEND_LIST: 'CLOSE_FRIEND_LIST',
    CANCEL_REQUEST: 'CANCEL_REQUEST',
    SET_TABLE_ID: 'SET_TABLE_ID',
    SHOW_REMOVE_PLAYER: 'SHOW_REMOVE_PLAYER',
    GAME_INVITE: 'GAME_INVITE'
}

const joinTableActions = (state = initialState, action) => {
    switch(action.type){
        case JoinTableConstants.ADD_PLAYER: {
            return{
                ... state,
                addPlayer: action.params.addPlayer,
                removePlayer: false,
                removePlayerDirection: '',
                tableId: action.params.tableId,
                currentDirection: action.params.direction,
                directionWest: action.params.direction == 'W'  ? action.params.direction : state.directionWest,
                directionNorth: action.params.direction == 'N' ? action.params.direction : state.directionNorth,
                directionSouth: action.params.direction == 'S'  ? action.params.direction : state.directionSouth,
                directionEast: action.params.direction == 'E'  ? action.params.direction : state.directionEast,
            }
        }
        case JoinTableConstants.ADD_PLAYER_SUCCESS: {
            return{
                ... state,
                addPlayer: false,
            }
        }
        case JoinTableConstants.SHOW_REMOVE_PLAYER: {
            return{
                ... state,
                removePlayer: action.params.removePlayer,
                addPlayer: action.params.addPlayer,
                removePlayerDirection: action.params.removePlayerDirection
            }
        }
        case JoinTableConstants.CANCEL_PLAYER_SELECT: {
            return{
                ... state,
                addPlayer: action.params.addPlayer,
                direction: action.params.direction
            }
        }
        case JoinTableConstants.ADD_BOT: {
            return{
                ... state,
                addPlayer: action.params.addPlayer,
                addBot: action.params.addBot,
                direction: action.params.direction
            }
        }
        case JoinTableConstants.FRIENDS_LIST: {
            return{
                ... state,
                addFriend: action.params.addFriend,
                direction: action.params.direction,
                tablesId: action.params.tablesId
            }
        }
        case JoinTableConstants.GAME_INVITE: {
            return{
                ...state,
                gameInvite: action.params.gameInvite
            }
        }
        case JoinTableConstants.CLOSE_FRIEND_LIST: {
            return{
                ... state,
                addFriend: false,
            }
        }
        case JoinTableConstants.CANCEL_REQUEST: {
            return{
                ... state,
                directionWest: action.params.direction == 'W'  ? action.params.direction : state.directionWest,
                directionNorth: action.params.direction == 'N' ? action.params.direction : state.directionNorth,
                directionSouth: action.params.direction == 'S' ? action.params.direction : state.directionSouth,
                directionEast: action.params.direction == 'E'  ? action.params.direction : state.directionEast,
                addPlayer: false
            }
        }case JoinTableConstants.SET_TABLE_ID: {
            return{
                ... state,
                tableId: action.params.tableId,
            }
        }
        default:{
            return state;
        }
    }
}

export default joinTableActions