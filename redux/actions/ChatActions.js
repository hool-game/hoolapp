import { initialState } from "../reducers/ChatReducers";
export const ChatConstants = {
    SHOW_CHAT_WINDOW: 'SHOW_CHAT_WINDOW',
    FETCH_CHAT_INFO:'FETCH_CHAT_INFO',
    FETCH_FRIENDS_LIST: 'FETCH_FRIENDS_LIST',
    FETCH_FRIENDS_LIST_SUCCESS: 'FETCH_FRIENDS_LIST_SUCCESS',
    FETCH_FRIENDS_LIST_FAILURE: 'FETCH_FRIENDS_LIST_FAILURE',
    FETCHED_CHAT_INFO_SUCCESS: 'FETCHED_CHAT_INFO_SUCCESS',
    FETCHED_CHAT_INFO_FAILED: 'FETCHED_CHAT_INFO_FAILED',
    SET_IS_TABLE_USER_TYPE: 'SET_IS_TABLE_USER_TYPE',
    IS_FIRST_TIME_USER: 'IS_FIRST_TIME_USER',
    LAST_USER_CHATTED: 'LAST_USER_CHATTED',
    SHOW_FRIENDS_LIST: 'SHOW_FRIENDS_LIST',
    SEARCH_CHAT_QUERY: 'SEARCH_CHAT_QUERY',
    SEARCH_CHAT_LIST: 'SEARCH_CHAT_LIST',
    SET_CHAT_INDEX: 'SET_CHAT_INDEX',
    SET_IS_FRIEND: 'SET_IS_FRIEND',
    SET_REQUEST_PENDING: 'SET_REQUEST_PENDING',
    SET_IS_MESSAGE_ENABLED: 'SET_IS_MESSAGE_ENABLED',
    SET_HOME_USER: 'SET_HOME_USER',
    SET_USER_INVITED: 'SET_USER_INVITED',
    CANCEL_REQUEST: 'CANCEL_REQUEST',
    SET_HOME_USER_WITH_HOST: 'SET_HOME_USER_WITH_HOST',
    SET_HOST_NAME: 'SET_HOST_NAME',
    FETCH_USER_LIST: 'FETCH_USER_LIST',
    SELECTED_USER: 'SELECTED_USER',
    SET_USERS: 'SET_USERS',
    HIT_PROFILE: 'HIT_PROFILE',
    FETCH_NOTIFICATION_LIST: 'FETCH_NOTIFICATION_LIST',
    NOTIFICATION_LIST: 'NOTIFICATION_LIST',
    UPDATE_NOTIFICATION_LIST: 'UPDATE_NOTIFICATION_LIST',
    UPDATE_CHATLIST: 'UPDATE_CHATLIST',
    SET_LAST_CLICKED_USER: 'SET_LAST_CLICKED_USER',
    SET_NOTIFICATION_COUNT: 'SET_NOTIFICATION_COUNT',
    SET_CHAT_NOTIFICATION_COUNT: 'SET_CHAT_NOTIFICATION_COUNT',
    SET_CLICKED: 'SET_CLICKED',
    CLICKED_USER: 'CLICKED_USER',
    FETCH_USERS_LIST:'FETCH_USERS_LIST',
    FETCH_USERS_LIST_SUCCESS: 'FETCH_USERS_LIST_SUCCESS',
    FETCH_USERS_LIST_FAILURE: 'FETCH_USERS_LIST_FAILURE',
    // USER_FRIEND: 'USER_FRIEND',
    SET_USER_CLICKED: 'SET_USER_CLICKED',
    CLICKED: 'CLICKED',
    UPDATE_WAITING_LIST: 'UPDATE_WAITING_LIST',
    SET_TABLE_MESSAGES: 'SET_TABLE_MESSAGES',
    TABLE_NOTIFICATION_COUNT: 'TABLE_NOTIFICATION_COUNT',
    SET_TABLE_LIST: 'SET_TABLE_LIST',
    SET_LAST_CLICKED_USER_PROFILE: 'SET_LAST_CLICKED_USER_PROFILE'
}

const chatActions = (state = initialState, action) => {
    switch(action.type){
        case ChatConstants.SHOW_CHAT_WINDOW: {
            return{
                ... state,
                showChatWindow: action.params.showChatWindow
            }
        }
        case ChatConstants.SET_IS_TABLE_USER_TYPE: {
            return{
                ... state,
                isTableUser: action.params.isTableUser
            }
        }
        case ChatConstants.FETCH_FRIENDS_LIST: {
            return{
                ... state,
                showSpinner: true,
                errorMessage: ''
            }
        }
        case ChatConstants.IS_FIRST_TIME_USER: {
            return{
                ... state,
                isFirstTime: action.params.isFirstTime,
                lastUserChat: action.params.lastUserChat
            }
        }
        case ChatConstants.SHOW_FRIENDS_LIST: {
            return{
                ... state,
                showFriendsList: action.params.showFriendsList
            }
        }
        case ChatConstants.FETCH_FRIENDS_LIST_SUCCESS: {
            var  friendsList = []
            friendsList = action.params.data
            return{
                ... state,
                allUsers: action.params.data,
                friendsList: friendsList,
                showSpinner: false
            }
        }
        case ChatConstants.SET_CLICKED: {
            return{
                ...state,
                setClicked: action.params.setClicked
            }
        }
        case ChatConstants.CLICKED_USER: {
            return{
                ...state,
                clickedUser: action.params.clickedUser
            }
        }
        case ChatConstants.SET_USERS: {
            return{
                ... state,
                users : action.params.users
            }
        }
        case ChatConstants.FETCH_FRIENDS_LIST_FAILURE: {
            return{
                ... state,
                friendsList: [],
                allUsers: [],
                showSpinner: false,
                errorMessage: action.params.errorMessage
            }
        }
        case ChatConstants.NOTIFICATION_LIST: {
            var newItem = action.params.newItem
            return{
                ... state,
                notificationList: [newItem,...state.notificationList]
            }
        }
        case ChatConstants.SET_LAST_CLICKED_USER_PROFILE: {
            return{
                ... state,
                lastClickedUserProfile: action.params.profile
            }
        }
        case ChatConstants.UPDATE_NOTIFICATION_LIST: {
            // var index = state.notificationList.findIndex( item => item.name === action.params.updatedItem.name)

            // var updatedList = [...state.notificationList]

            // updatedList[index] = action.params.updatedItem

            return{
                ... state,
                notificationList: action.params.updatedList
            }
        }
        case ChatConstants.SET_USER_CLICKED: {
            return{
                ...state,
                userClicked: action.params.userClicked
            }
        }
        case ChatConstants.CLICKED: {
            return{
                ...state,
                clickedProf: action.params.clickedProf
            }
        }
        case ChatConstants.UPDATE_CHATLIST: {
            return{
                ... state,
                messages: action.params.messages
            }
        }
        case ChatConstants.FETCH_USERS_LIST_SUCCESS: {
            return{
                ... state,
                usersList: action.params.usersList
            }
        }
        case ChatConstants.FETCH_USERS_LIST_FAILURE: {
            return{
               ... state,
               usersList: [],
               errorMessage: action.params.error 
            }
        }
        case ChatConstants.SET_NOTIFICATION_COUNT: {
            return{
                ... state,
                notificationCount: action.params.notificationCount
            }
        }
        case ChatConstants.TABLE_NOTIFICATION_COUNT: {
            return{
                ... state,
                tableNotificationCount: action.params.tableNotificationCount
            }
        }
        case ChatConstants.SET_TABLE_LIST: {
            return{
                ... state,
                tableList: action.params.tableList
            }
        }
        case ChatConstants.SET_CHAT_NOTIFICATION_COUNT: {
            return {
                ... state,
                friendsNotificationCount: action.params.friendsNotificationCount
            }
        }
        case ChatConstants.LAST_USER_CHATTED: {
            return{
                ... state,
                lastUserChat: action.params.lastUserChat
            }
        }
        case ChatConstants.SELECTED_USER: {
            return{
                ... state,
                selectedUser: action.params.selectedUser
            }
        }
        case ChatConstants.HIT_PROFILE: {
            return {
                ...state, 
                hitProfile: action.params.hitProfile
            }
        }
        case ChatConstants.UPDATE_WAITING_LIST: {
            return {
                ... state,
                waitingList: action.params.waitingList
            }
        }
        case ChatConstants.SET_LAST_CLICKED_USER: {
            return{
                ... state,
                lastClickedUser: action.params.lastClickedUser
            }
        }
        case ChatConstants.SEARCH_CHAT_QUERY: {
            return{
                ... state,
                searchQuery: action.params.searchQuery
            }
        }
        case ChatConstants.SEARCH_CHAT_LIST: {
            return{
                ... state,
                searchList: action.params.searchList
            }
        }
        case ChatConstants.SET_CHAT_INDEX: {
            return{
                ... state,
                index: action.params.index
            }
        }
        case ChatConstants.SET_IS_FRIEND: {
            return{
                ... state,
                isFriend: action.params.isFriend
            }
        }
        case ChatConstants.SET_REQUEST_PENDING: {
            return{
                ... state,
                isRequestPending: action.params.isRequestPending
            }
        }case ChatConstants.SET_TABLE_MESSAGES: {
            return{
                ... state,
                tableMessages: action.params.tableMessages
            }
        }
        case ChatConstants.SET_IS_MESSAGE_ENABLED: {
            return{
                ... state,
                isMessageEnabled: action.params.isMessageEnabled
            }
        }
        case ChatConstants.SET_HOME_USER: {
            return{
                ... state,
                homeUser: action.params.homeUser
            }
        }
        case ChatConstants.SET_HOME_USER_WITH_HOST: {
            return{
                ... state,
                homeUser: action.params.homeUser,
                host: action.params.host,
                // isFriend: action.params.isFriend
            }
        }
        case ChatConstants.SET_HOST_NAME: {
            return{
                ... state,
                hostName: action.params.hostName,
            }
        }
    
        case ChatConstants.SET_USER_INVITED: {
            return{
                ... state,
                isNorthInvited: action.params.direction == 'N' ? true :  state.isNorthInvited,
                isSouthInvited: action.params.direction == 'S' ? true :  state.isSouthInvited,
                isWestInvited: action.params.direction == 'W'  ? true :  state.isWestInvited,
                isEastInvited: action.params.direction == 'E'  ? true :  state.isEastInvited,
                westUserName: action.params.direction == 'W' ? action.params.userName : state.westUserName,
                northUserName: action.params.direction == 'N' ? action.params.userName : state.northUserName,
                southUserName: action.params.direction == 'S'  ? action.params.userName : state.southUserName,
                eastUserName: action.params.direction == 'E'  ? action.params.userName : state.eastUserName,
                isFriend: action.params.isFriend
            }
        }
        case ChatConstants.CANCEL_REQUEST: {
            return{
                ... state,
                isNorthInvited: action.params.direction == 'N' ? false :  state.isNorthInvited,
                isSouthInvited: action.params.direction == 'S' ? false :  state.isSouthInvited,
                isWestInvited: action.params.direction == 'W'  ? false :  state.isWestInvited,
                isEastInvited: action.params.direction == 'E'  ? false :  state.isEastInvited,
                westUserName: action.params.direction == 'W' ? undefined :  state.westUserName,
                northUserName: action.params.direction == 'N' ? undefined :  state.northUserName,
                southUserName: action.params.direction == 'S'  ? undefined :  state.southUserName,
                eastUserName: action.params.direction == 'E'  ? undefined :  state.eastUserName,
            }
        }
        default:{
            return state;
        }
    }
}

export default chatActions