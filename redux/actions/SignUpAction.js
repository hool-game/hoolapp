import { initialState } from "../reducers/SignUpReducers";

export const SignUpConstants = {
    REGISTER_USER: 'REGISTER_USER',
    REGISTER_USER_SUCCESS: 'REGISTER_USER_SUCCESS',
    REGISTER_USER_FAILURE: 'REGISTER_USER_FAILURE',
    SET_USERNAME: 'SET_USERNAME',
    SET_PASSWORD: 'SET_PASSWORD',
    SET_NAME: 'SET_NAME',
    SET_COUNTRY: 'SET_COUNTRY',
    SET_CONFIRM_PASSWORD: 'SET_CONFIRM_PASSWORD',
    SET_SIGNUP_STYLE: 'SET_SIGNUP_STYLE',
    SHOW_SPINNER: 'SHOW_SPINNER', 
    SET_EMAIL: 'SET_EMAIL',
    SET_ERROR_MESSAGE: 'SET_ERROR_MESSAGE',
    SET_REGISTER_SUCCESS: 'SET_REGISTER_SUCCESS',
    SET_FILTERED_ITEMS: 'SET_FILTERED_ITEMS',
    AGREE_TERMS: 'AGREE_TERMS',
    CLEAR: 'CLEAR'
}

const chatActions = (state = initialState, action) => {
    switch(action.type){
        case SignUpConstants.REGISTER_USER: {
            return{
                ... state,
                spinner: true,
                errorMessage: ''
            }
        }
        case SignUpConstants.SET_REGISTER_SUCCESS: {
            return{
                ... state,
                isRegisterSuccess: action.params.isRegisterSuccess,
            }
        }
        case SignUpConstants.AGREE_TERMS: {
            return{
                ... state,
                agreeTerms: action.params.agreeTerms,
            }
        }
        case SignUpConstants.SET_ERROR_MESSAGE: {
            return{
                ... state,
                showError: action.params.showError,
                username: action.params.username,
                errorMessage: action.params.errorMessage
            }
        }
        case SignUpConstants.SET_FILTERED_ITEMS:{
            return{
                ...state,
                filteredItems: action.params.filteredItems
            }
        }
        case SignUpConstants.SET_NAME: {
            return{
                ... state,
                name: action.params.name,
            }
        }
        case SignUpConstants.SET_EMAIL: {
            return{
                ... state,
                email: action.params.email,
            }
        }
        case SignUpConstants.SET_SIGNUP_STYLE:{
            return{
                ...state,
                signupBtnStyle: action.params.signupBtnStyle,
                signupTextStyle: action.params.signupTextStyle,
                disableSignup: action.params.disableSignup
            }
        }
        case SignUpConstants.SET_PASSWORD: {
            return{
                ... state,
                password: action.params.password,
            }
        }
        case SignUpConstants.SET_CONFIRM_PASSWORD: {
            return{
                ... state,
                confirmPassword: action.params.confirmPassword,
            }
        }
        case SignUpConstants.SET_COUNTRY: {
            return{
                ... state,
                country: action.params.country,
            }
        }
        case SignUpConstants.REGISTER_USER_SUCCESS: {
            return{
                ... state,
                spinner: false,
                isRegisterSuccess: true,
                showError: false
            }
        }
        case SignUpConstants.REGISTER_USER_FAILURE: {
            return{
                ... state,
                showError: true,
                errorMessage: action.params.message,
                spinner: false,
            }
        }
        case SignUpConstants.CLEAR: {
            return{
                ... state,
                spinner: false,
                disableSignup: true,
                isRegisterSuccess: false,
                showError: false,
                username: '',
                password: '',
                confirmPassword: '',
                email: '',
                name: '',
                country: 'India',
                agreeTerms: false,
                signupBtnStyle: {},
                signupTextStyle: {},
                filteredItems: [],
                errorMessage: ''
            }
        }
        default:{
            return state;
        }
    }
}

export default chatActions