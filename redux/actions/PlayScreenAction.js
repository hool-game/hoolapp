import { initialState } from "../reducers/ChatReducers";

export const PlayScreenConstants = {
    PLAY_SCREEN_UPDATE: 'PLAY_SCREEN_UPDATE'
}

const chatActions = (state = initialState, action) => {
    switch(action.type){
        case PlayScreenConstants.PLAY_SCREEN_UPDATE: {
            return{
                ... state,
                  tableID:action.params.tableID,
                  gamestate:action.params.gamestate,
                  northAvtar:action.params.northAvtar,
                  eastAvtar:action.params.eastAvtar,
                  southAvtar:action.params.southAvtar,
                  westAvtar:action.params.westAvtar,
                  northUser:action.params.northUser,
                  eastUser:action.params.eastUser,
                  southUser:action.params.southUser,
                  westUser:action.params.westUser,
                  iskibitzer:action.params.iskibitzer,
                  infoScreen:action.params.infoScreen,
                  bidInfoScreen:action.params.bidInfoScreen,
                  affiliation:action.params.affiliation,
                  playScreen:action.params.playScreen,
                  tricks:action.params.tricks
            }
        }
        default:{
            return state;
        }
    }
}

export default chatActions