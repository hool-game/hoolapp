import { initialState } from "../reducers/SignInReducers";

export const SignInConstants = {
    SIGN_IN: 'SIGN_IN',
    SIGN_IN_SUCCESS: 'SIGN_IN_SUCCESS',
    SIGN_IN_FAILURE: 'SIGN_IN_FAILURE',
    SET_USERNAME: 'SET_USERNAME',
    SET_PASSWORD: 'SET_PASSWORD',
    SET_LOGIN_STYLE: 'SET_LOGIN_STYLE',
    SHOW_SPINNER: 'SHOW_SPINNER', 
    LOGIN_ENABLE_DISABLE: 'LOGIN_ENABLE_DISABLE',
    SET_TEXT_COLOR: 'SET_TEXT_COLOR',
    START_APP: 'START_APP',
    SHOW_MENU: 'SHOW_MENU',
    DISABLE_LOGIN: 'DISABLE_LOGIN',
    SIGNIN_SUCCESSFULL: 'SIGNIN_SUCCESSFULL',
    SET_INITIAL_SCREEN: 'SET_INITIAL_SCREEN'
}

const chatActions = (state = initialState, action) => {
    switch(action.type){
        case SignInConstants.SIGN_IN: {
            return{
                ... state,
                showSpinner: true,
                jwt: '',
                message: ''
            }
        }
        case SignInConstants.SHOW_MENU: {
            return{
                ... state,
                showMenu: action.params.showMenu
            }
        }
        case SignInConstants.SET_TEXT_COLOR: {
            return{
                ... state,
                txtcolor: action.params.txtcolor,
            }
        }
        case SignInConstants.SHOW_SPINNER: {
            return{
                ... state,
                showSpinner: action.params.showSpinner,
            }
        }
        case SignInConstants.LOGIN_ENABLE_DISABLE: {
            return{
                ... state,
                disableLogin: action.params.disableLogin,
            }
        }
        case SignInConstants.SET_USERNAME: {
            return{
                ... state,
                username: action.params.username,
                message: ''
            }
        }
        case SignInConstants.SET_LOGIN_STYLE:{
            return{
                ...state,
                loginBtnStyle: action.params.loginBtnStyle,
                loginTextStyle: action.params.loginTextStyle,
                disableLogin: action.params.disableLogin
            }
        }
        case SignInConstants.SET_PASSWORD: {
            return{
                ... state,
                password: action.params.password,
                disableLogin: action.params.disableLogin,
                message: ''
            }
        }
        case SignInConstants.SIGN_IN_SUCCESS: {
            return{
                ... state,
                jwt: action.params.jwt,
                message: '',
                showLogin: true
            }
        }
        case SignInConstants.DISABLE_LOGIN: {
            return{
                ... state,
                showLogin: false
            }
        }
        case SignInConstants.SIGN_IN_FAILURE: {
            return{
                ... state,
                showSpinner: false,
                message: action.params.message,
            }
        }
        case SignInConstants.SIGNIN_SUCCESSFULL: {
            return {
                ...state,
                signInSuccessfull: action.params
            }
        }
        case SignInConstants.SET_INITIAL_SCREEN: {
            return {
                ...state,
                initialScreen: action.params
            }
        }
        default:{
            return state;
        }
    }
}

export default chatActions