import ChatJson from '../../assets/data/chat.json';

export const initialState = {
    addPlayer: false,
    currentDirection: undefined,
    directionNorth: undefined,
    directionSouth: undefined,
    directionEast: undefined,
    directionWest: undefined,
    addfriend: false,
    addBot: false, 
    tableId: '',
    removePlayer: false,
    removePlayerDirection: '',
    friendsList: [],
}
