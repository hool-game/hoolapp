
export const initialState = {
    spinner: false,
    disableSignup: true,
    isRegisterSuccess: false,
    showError: false,
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    name: '',
    country: 'India',
    agreeTerms: false,
    signupBtnStyle: {},
    signupTextStyle: {},
    filteredItems: [],
    errorMessage: ''
}
