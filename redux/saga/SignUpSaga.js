import { call, put, takeEvery } from "redux-saga/effects";
import { SignUpConstants } from "../actions/SignUpAction";
import { registerUser } from "../api/config";

export function* registerUsers(action) {
    const { username = '', password = '', email= '', name= '', location= ''} = action.params;
    try {
        const response = yield call(
            registerUser,
            username,
            password,
            email,
            name,
            location
        );
        const data = response.data;
        
        if(data.status == 'ok'){
            yield put({ type: SignUpConstants.REGISTER_USER_SUCCESS, params: data });
        }else{
            yield put({ type: SignUpConstants.REGISTER_USER_FAILURE, params: data });
        }

    } catch (error) {
        yield put({ type: SignUpConstants.REGISTER_USER_FAILURE, params: error });
    }
}

export function* fetchRegisterUserAsync() {
    yield takeEvery(SignUpConstants.REGISTER_USER, registerUsers);
}