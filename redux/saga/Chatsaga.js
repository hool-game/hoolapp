import { call, put, select, takeEvery } from "redux-saga/effects";
import { ChatConstants } from "../actions/ChatActions";
import { getChatDataForUser, getFriendsList, getList, getUserList, setUsers } from "../api/config";

export function* fetchChatForUser() {
    try {
        const response = yield call(getChatDataForUser);
        const data = response.data;

        yield put({ type: ChatConstants.FETCHED_CHAT_INFO_SUCCESS, data });

    } catch (error) {
        yield put({ type: ChatConstants.FETCHED_CHAT_INFO_FAILED, error });
    }
}
export function* setNotificationList() {
    try{
      const data = yield call(getList)
      if( data.newList !== null){
      yield put({ type: ChatConstants.UPDATE_NOTIFICATION_LIST , params: { updatedList: data.newList}})
      }
      if( data.count ){
        yield put({ type: ChatConstants.SET_NOTIFICATION_COUNT , params: { notificationCount : data.count}}) 
      }
    }
    catch(error){
       console.log(error)
    }
} 

export function* setUsersList() {
  try{
    const response = yield call(getUserList)
    console.log(response.data)
    if( response.data.status == 'ok'){
        let usersList = yield call(setUsers,response.data.users)
        yield put({ type: ChatConstants.FETCH_USERS_LIST_SUCCESS , params: { usersList: usersList}})
    }
  }
  catch(error){
    yield put({ type: ChatConstants.FETCH_USERS_LIST_SUCCESS , params: error})
  }
}

export function* fetchFriendsList() {
    try {
        const state = yield select();
        console.log("WL: ",state.chatActions.waitingList)
        const response = yield call(getFriendsList,state.signInActions.username,state.chatActions.friendsList,state.chatActions.waitingList);
        console.log(response.friendsList)
        if(response){
            yield put({ type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS, params: {data: response.friendsList}});
            yield put({ type: ChatConstants.UPDATE_CHATLIST, params: {messages: response.messages}})
            yield put({ type: ChatConstants.UPDATE_WAITING_LIST, params: { waitingList: response.waitingList}})
        }else{
            yield put({ type: ChatConstants.FETCH_FRIENDS_LIST_FAILURE, params: error });  
        }

    } catch (error) {
        yield put({ type: ChatConstants.FETCH_FRIENDS_LIST_FAILURE, params: error });
    }
}

export function* fetchChatAsync() {
    yield takeEvery(ChatConstants.FETCH_CHAT_INFO, fetchChatForUser);
}
export function* fetchFriendsListAsync() {
    yield takeEvery(ChatConstants.FETCH_FRIENDS_LIST, fetchFriendsList);
}

export function* fetchNotificationList(){
    yield takeEvery(ChatConstants.FETCH_NOTIFICATION_LIST,setNotificationList)
}

export function* fetchUsersList(){
    yield takeEvery(ChatConstants.FETCH_USERS_LIST,setUsersList)
}