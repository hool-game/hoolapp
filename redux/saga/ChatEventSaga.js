import {take,put,call,fork} from 'redux-saga/effects'
import {eventChannel} from 'redux-saga'
import { SignInConstants } from '../actions/SignInAction'
import { HOOL_CLIENT } from '../../shared/util'
import { HOOL_EVENTS } from '../../constants/hoolEvents'
import AsyncStorage from '@react-native-async-storage/async-storage';
const { jid } = require('@xmpp/client');
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import Toast from 'react-native-toast-message';
import { ChatConstants } from '../actions/ChatActions'
import { store } from '../../App'

const notificationObject = {
  "name": "",
  "jid" : "",
  "isGameInvite": false, //Change it to true when game-invite sent   //Last change
  "isFriendRequest": true,
  "avatarImage" : "profile1",
  "isInviteAccepted" : false,
  "isInviteDeclined" : false,
  "isGameInviteAccepted": false,   //Last change
  "isGameInviteDeclined": false,
  "isGameInviteMissed" : false,
  "timeOfRequest": "3m ago"
}

export function* listener(){
   const channel = yield call(subscribe)
   while(true){
    const action = yield take(channel)
    for( const item of action){
      yield put({ type: item.type , params: item.params})

    }
   }
}


export function* subscribe(){
 return new eventChannel( emit => {
    HOOL_CLIENT.addListener(HOOL_EVENTS.ROSTER_SUBSCRIBE,async (e) => {
      let rosterList = await HOOL_CLIENT.xmppGetRoster()
      let preApproveSubscription = false
      rosterList.forEach( (element) => {
        if(element.jid === e.user){
          if( element.subscription === "both"){
            preApproveSubscription = true
          }else if( element.subscription === "none"){
            preApproveSubscription = false
          }else if( element.subscription === "from"){
            preApproveSubscription = false
          }else if( element.subscription === "to"){
            HOOL_CLIENT.rosterSubscribed(e.user)
            preApproveSubscription = true
          }
        }
      })
      if(!preApproveSubscription){
        let user = jid(e.user);
        let notificationCount
        let data = await AsyncStorage.getItem('notificationList')
        let List = JSON.parse(data)
        let flag = true
        if(List !== null){
          List.forEach( element => {
            if( element.name === user._local)
            {
              flag = false
              return
            }
          })
        }else{
          await AsyncStorage.setItem('notificationList',JSON.stringify([]))
        }
        if( flag ){
        notificationCount = await AsyncStorage.getItem('notificationCount')
        let count = JSON.parse(notificationCount)
        if(count === null){
          count = 1
        }else{
          count = count + 1
        }
        await AsyncStorage.setItem('notificationCount',JSON.stringify(count))
        List = JSON.parse(await AsyncStorage.getItem('notificationList'))
        let notificationItem = {...notificationObject}
        notificationItem.name = user._local
        notificationItem.jid = e.user
        notificationItem.timeOfRequest = new Date()
        notificationItem._id = uuidv4();
        List.unshift(notificationItem)
        await AsyncStorage.setItem('notificationList',JSON.stringify(List))
        
        emit([
          { type: ChatConstants.NOTIFICATION_LIST , params: {newItem: notificationItem}},
          { type: ChatConstants.SET_NOTIFICATION_COUNT , params: {notificationCount: count}}
          ])
        }
      }else{
        emit([
          { type: ChatConstants.FETCH_USERS_LIST}
        ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CONTACT_ONLINE ,async e => {
      console.log(`Online User ${e.user}`)
      const state =  store.getState();
      let user = jid(e.user)

      let list = [...state.chatActions.friendsList]
      
      let onlineUser = list.find( element => element.name === user._local)
      // Check The friendsList Length . if its empty we can add it to the empty array and then we process it in the fetch friendsList
      if( onlineUser !== undefined){
      list.forEach((element,idx) => {
       if(element.name === user._local)
       list[idx].isOnline = true
      })

      emit([{ type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: list}},]) 
      }else{
        let waitingList = [...state.chatActions.waitingList]
        waitingList.push({ name: jid(e.user)})
        
        console.log("Waiting List :",waitingList)
        emit([
          { type: ChatConstants.UPDATE_WAITING_LIST , params: {waitingList: waitingList}}
        ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CONTACT_OFFLINE ,async e => {
      const state =  store.getState();
      let user = jid(e.user)
 
      let list = [...state.chatActions.friendsList]
      let offlineUser = list.find( element => element.name === user._local)
 
      if( offlineUser !== undefined){
        list.forEach((element,idx) => {
          if(element.name === user._local)
          list[idx].isOnline = false
         })
   
         emit(
           [
           { type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: list}},
           ]) 
      }else{
        let waitingList = [...state.chatActions.waitingList]
        waitingList.push({ name: jid(e.user)})
        
        console.log("Waiting List :",waitingList)
        emit([
          { type: ChatConstants.UPDATE_WAITING_LIST , params: {waitingList: waitingList}}
        ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CHAT,async (e) => {
        let user = jid(e.from)._local
        const state =  store.getState();
        
        let friendsList = [...state.chatActions.friendsList]
        let count = state.chatActions.friendsNotificationCount
        
        const chatObject = {
          _id: uuidv4(),
          text: e.text,
          createdAt: new Date(),
          user: {
            _id: uuidv4(),
            name: user
          }
        }
        let oldMessages = [...state.chatActions.messages[user]]
        if( oldMessages.length >= 500){
          oldMessages.pop()
        }
        oldMessages.unshift(chatObject)
        await AsyncStorage.setItem(user,JSON.stringify(oldMessages))
    
        let messages = {...state.chatActions.messages}
        messages[user] = oldMessages
        if(state.chatActions.lastClickedUser !== user){
          friendsList.forEach( (element,idx) => {
            if(element.name === user){
              friendsList[idx].unreadMessages =  friendsList[idx].unreadMessages + 1
              friendsList[idx].isNewMessages = true
              count = count + 1
            }
        })
        Toast.show({
          type: 'info',
          text1: `message from ${user}`,
          position: 'bottom',
          bottomOffset: 50,
          text2: e.text
        });
        }
        
        emit([
           {type: ChatConstants.UPDATE_CHATLIST, params: {messages: messages }},
           {type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: friendsList }},
           {type: ChatConstants.SET_CHAT_NOTIFICATION_COUNT , params: {friendsNotificationCount: count}}
        ])
    })

    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_SUBSCRIBED , e => {
      console.log(`${e.user} Accepted Your FriendRequest`)
      emit([{ type: ChatConstants.FETCH_FRIENDS_LIST }])
    })

    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_UNSUBSCRIBE ,async e => {
      console.log(`${e.user} has no longer following your presence`)
      HOOL_CLIENT.rosterUnsubscribed(e.user)
      let rosterList = await HOOL_CLIENT.xmppGetRoster() 
      let oldUser = false
      rosterList.forEach( element => {
        if( element.jid === e.user){
          oldUser = true
        }
      })
      if( oldUser ){
        HOOL_CLIENT.rosterRemove(e.user)
        emit([{ type: ChatConstants.FETCH_FRIENDS_LIST }])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_UNSUBSCRIBED, async (e) => {
      console.log(`${e.user} has removed You from the friendsList`)
    })
    HOOL_CLIENT.on(HOOL_EVENTS.GAME_INVITED, async (e) => {
      // let gameCall = true
      console.log(`${e.invitor} called you to play `)
      console.info(`I have been invited by ${e.invitor} to play on some table `)
      // let user = jid(e.user);
      // let notificationCount
      // let data = await AsyncStorage.getItem('notificationList')
      // let GameList = JSON.parse(data)
      // // let flag = true
      // if(GameList !== null){
      //   GameList.forEach( element => {
      //     if( element.name === user._local)
      //     {
      //       flag = false
      //       return
      //     }
      //   })
      // }else{
      //   await AsyncStorage.setItem('notificationList',JSON.stringify([]))
      // }      
      // notificationCount = await AsyncStorage.getItem('notificationCount')
      // let count = JSON.parse(notificationCount)
      // if(count === null){
      //   count = 1
      // }else{
      //   count = count + 1
      // }
      // await AsyncStorage.setItem('notificationCount',JSON.stringify(count))
      // GameList = JSON.parse(await AsyncStorage.getItem('notificationList'))
      // let notificationItem = {...notificationObject}
      // notificationItem.name = user._local
      // notificationItem.jid = e.user
      // notificationItem.isGameInvite = gameCall
      // notificationItem.timeOfRequest = new Date()
      // notificationItem._id = uuidv4();
      // GameList.unshift(notificationItem)
      // await AsyncStorage.setItem('notificationList',JSON.stringify(GameList))
      
      // emit([
      //   { type: ChatConstants.NOTIFICATION_LIST , params: {newItem: notificationItem}},
      //   { type: ChatConstants.SET_NOTIFICATION_COUNT , params: {notificationCount: count}}
      // ])
      
    })
    
    HOOL_CLIENT.on(HOOL_EVENTS.GAME_DECLINED, async e => {
      console.info(`Game invite is declined by ${e.invitor}`)
    })
    
    return () => {}
 })
}


export function* initiateEventEmitter(){
yield take(SignInConstants.START_APP)
yield fork(listener)
}