import { call, put, takeEvery } from "redux-saga/effects";
import { SignInConstants } from "../actions/SignInAction";
import { registerUser, setAuthorizationToken, signInUser } from "../api/config";

export function* signIn(action) {
    const { username = '', password = ''} = action.params;
    try {
        const response = yield call(
            signInUser,
            username,
            password
        );
        console.log("Response :",response.data)
        const data = response.data;
        if(data.status == 'ok'){
            yield call(setAuthorizationToken, data.jwt)
            yield put({ type: SignInConstants.SIGN_IN_SUCCESS, params: data });
        }else{
            yield put({ type: SignInConstants.SIGN_IN_FAILURE, params: data });
        }

    } catch (error) {
        yield put({ type: SignInConstants.SIGN_IN_FAILURE, params: error });
    }
}

export function* registerUsers(action) {
    try {
        const response = yield call(registerUser);
        const data = response.data;
        
        yield put({ type: SignInConstants.REGISTER_USER_SUCCESS, data });

    } catch (error) {
        yield put({ type: SignInConstants.REGISTER_USER_FAILURE, error });
    }
}

export function* fetchSignInAsync() {
    yield takeEvery(SignInConstants.SIGN_IN, signIn);
}

export function* fetchRegisterUserAsync() {
    yield takeEvery(SignInConstants.REGISTER_USER, registerUsers);
}