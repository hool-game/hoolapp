import env from 'react-native-config'
/**
 * Config.JS
 * Configuration for environment based
 */
const config = {
  api: {
    host: env.APP_BASE_URL,
    xmppHool: env.XMPP_END_POINT,
    xmppTable: env.XMPP_BENCHES_END_POINT,
    userSuffix: env.USER_NAME_HOOL_SUFFIX,
    botSuffix: env.BOT_EXTENSION,
    timeout: 20000,
    users: env.APP_BASE_URL_USER
  }
};

const API_ENV = config.api;

export {
    API_ENV
}

export default config