import axios from 'axios'
import * as ApiEndPoints from './ApiEndPoints'

const instance = axios.create({
    baseURL: ApiEndPoints.BASE_URL
});
instance.defaults.headers.post['Content-Type'] = 'application/json';
instance.defaults.timeout = 30000;
instance.interceptors.response.use(response => {
    return response;
}, error => {
    return Promise.reject(error);
});

export default instance;