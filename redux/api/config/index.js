import AxiosConfig from '../config/AxiosConfig';
import * as ApiEndPoints from '../config/ApiEndPoints';
import { HOOL_CLIENT } from '../../../shared/util';
const { jid } = require('@xmpp/client');
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setUsersList } from '../../saga/Chatsaga';

const friendObject = {
  name: '',
  avatarImage: 'avatar',
  status: 1,
  indication: false,
  indicatorType: '',
  showProfileIcon: false,
  isOnline: false,
  isFriend: false,
  isRequestPending: false,
  isMessageEnabled: false,
  isHomeUser: false,
  isNewMessages: false,
  unreadMessages: 0
};

const userObjecct = {
  name: '',
  avatarImage: 'avatar',
  status: 1,
  indication: false,
  indicatorType: '',
  showProfileIcon: false,
  isOnline: false,
  isFriend: true,
  isRequestPending: false,
  isMessageEnabled: false,
  isHomeUser: false,
  isNewMessages: false,
}

class HoolAPI {
  constructor(name) {
    this.name = name;
  }

  setAuthorizationToken(accessToken) {
    AxiosConfig.defaults.headers.common['Authorization'] =
      'bearer ' + accessToken;
  }

  signInUser(username = '', password = '') {
    const payload = {
      username: username,
      password: password
    };
    return AxiosConfig.post(ApiEndPoints.SIGN_IN, payload);
  }

  registerUser(
    username = '',
    password = '',
    email = '',
    name = '',
    location = ''
  ) {
    const payload = {
      username: username,
      password: password,
      email: email,
      name: name,
      location: location
    };

    return AxiosConfig.post(ApiEndPoints.REGISTER, payload);
  }

  async getList (){
    let data = await AsyncStorage.getItem('notificationList')
    let count = await AsyncStorage.getItem('notificationCount')
    let List = JSON.parse(data)
    let newList = []
    // To clear old notifications 
    List.forEach( (element) => {
      if( element.isInviteAccepted !== true && element.isInviteDeclined !== true){
        newList.push(element)
      }
    })
    count = JSON.parse(count)

    // update the notificationList
    await AsyncStorage.setItem('notificationList',JSON.stringify(newList))
    return { newList, count}
  }


  async getFriendsList(username = '',oldFriendsList = [] , waitingList) {

    // Check the username if the username is different from the previous one.He logged in from different account
    let oldUser = await AsyncStorage.getItem('username')
    if( username !== oldUser){
      await AsyncStorage.clear();
      await AsyncStorage.setItem('username',username)
    }
    let list = await HOOL_CLIENT.xmppGetRoster();
    let friendsList = []
    let messages = {}

    // Lets check the old friendsList
    if( oldFriendsList.length > 0){
     friendsList = [...oldFriendsList]
     
     let newFriend
     // Also check the list (roster list fetch from the server)
     list.forEach((element,idx) => {
       let oldUser = friendsList.find( oldItem => element.jid === oldItem.jid )
       if( oldUser === undefined && element.subscription === "both"){
          newFriend = element.jid
       }
     })

     if( newFriend !== undefined){
      let name = jid(newFriend)
      let friendItem = {...friendObject}

      friendItem.name = name._local
      friendItem.jid = newFriend
      friendsList.push(friendItem)
     }
    }else{
      list.forEach(async element => {
        if( element.subscription === "both"){
          let name = jid(element.jid)
          let friendItem = {...friendObject}
    
          friendItem.name = name._local
          friendItem.jid = element.jid
          friendsList.push(friendItem)
        }
      })
    }
    
    friendsList.forEach(async (element) => {
      let message = await AsyncStorage.getItem(element.name)
      console.log(message)
      if( message === null){
        await AsyncStorage.setItem(element.name,JSON.stringify([]))
        messages[element.name] = []
      }else{
        messages[element.name] = JSON.parse(message)
      }
    })
    
    console.log("WaitingList :",waitingList)
   
    friendsList.forEach( (element,idx) => {
      let index
      let removeItem = waitingList.find( (item,i) => {
        if(item.name._local === element.name){
          index  = i
          return item.name._local
        }
      })
      if( removeItem !== undefined){
        friendsList[idx].isOnline = true
        waitingList.splice(index,1)
      }
    })

    friendsList.sort( (a,b) => ( (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0 ))
    return { friendsList , messages, waitingList}
  }

  async getUserList(){
    return AxiosConfig.get(ApiEndPoints.USERS_LIST)
  }

  setUsers(data = []){
    let usersList = []

    data.forEach( (element) => {
      let userItem = {...userObjecct}
      userItem.name = element.jid._local
      userItem.jid = `${element.jid._local}@${element.jid._domain}`
      userItem.subName = element.name
      userItem.flagText = element.location
      usersList.push(userItem)
    })
    usersList.sort( (a,b) => ( (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0 ))

    return usersList
  }
  getChatDataForUser() {
    const payload = {};

    return AxiosConfig.post(ApiEndPoints.GET_CHATA_DATA_USER, payload);
  }
}

export const {
  setAuthorizationToken,
  getChatDataForUser,
  signInUser,
  registerUser,
  getFriendsList,
  getList,
  getUserList,
  setUsers
} = new HoolAPI('instance');
