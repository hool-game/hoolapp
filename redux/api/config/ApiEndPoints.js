import { API_ENV } from "./config"

export const BASE_URL = API_ENV.host

export const SIGN_IN = 'signIn'

export const REGISTER = 'register'

export const USER_PROFILE = 'profile'

export const USERS_LIST = 'users'

export const GET_CHATA_DATA_USER = ''

export const USER_LIST = API_ENV.users

