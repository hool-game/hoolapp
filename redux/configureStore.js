import {createStore, applyMiddleware, combineReducers} from 'redux';
import {all} from 'redux-saga/effects';
import {composeWithDevTools} from '@redux-devtools/extension';
import createSagaMiddleware from 'redux-saga';
import chatActions from './actions/ChatActions';
import joinTableActions from './actions/JoinaTableActions';
import signInActions from './actions/SignInAction';
import signUpActions from './actions/SignUpAction';
import {fetchChatAsync, fetchFriendsListAsync, fetchNotificationList, fetchUsersList } from './saga/Chatsaga'
import {fetchSignInAsync } from './saga/SignInSaga'
import {fetchRegisterUserAsync } from './saga/SignUpSaga'
import { initiateEventEmitter } from './saga/ChatEventSaga';


const appReducer = combineReducers({chatActions:chatActions, joinTableActions: joinTableActions, signInActions: signInActions, 
  signUpActions: signUpActions});
const rootReducer = (state, action) => {
  return appReducer(state, action)
}
const sagaMiddleware = createSagaMiddleware()
function* rootSaga() {
  yield all([fetchChatAsync(), fetchSignInAsync(), fetchRegisterUserAsync(), fetchFriendsListAsync(), fetchNotificationList() ,initiateEventEmitter(), fetchUsersList()]);
}
const configureStore = () => {
  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export default configureStore;
