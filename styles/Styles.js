import { StyleSheet } from 'react-native';
import { Colors } from './Colors';
import { Fonts } from './Fonts';

export const commonStyles = StyleSheet.create({
  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: Colors.color_C000FF,
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: Colors.color_FF0C3E,
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: Colors.color_04AEFF,
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: Colors.color_79E62B,
  },
  ContractTextNT: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: Colors.color_FFE81D,
  }
});
