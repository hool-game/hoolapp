import React from 'react';
import {
  GiftedChat,
  Send,
  InputToolbar,
  Bubble,
  Time
} from 'react-native-gifted-chat';
import {
  View,
  Text
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { normalize } from '../../styles/global';
import { Colors } from '../../styles/Colors';
import svgImages from '../../API/SvgFiles';
import { moderateScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { HOOL_CLIENT } from '../../shared/util';
import AsyncStorage from '@react-native-async-storage/async-storage';

class FriendChat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
    };
  }

  async onSend(messages = []) {
    if( this.props.lastUserChat === "Table Messages" ){
      HOOL_CLIENT.groupChat(this.props.gameTableId,messages[0].text)

      let oldTableMessages = [...this.props.tableMessages]
      let newTableMessages = GiftedChat.append(oldTableMessages , messages)
      this.props.setTableMessages({
        tableMessages: newTableMessages
      })
    }else{
    let JID
    this.props.friendsList.forEach( element => {
      if(element.name === this.props.lastUserChat){
        JID = element.jid
      }
    })
    HOOL_CLIENT.chat(JID,messages[0].text)
    let oldMessages = [...this.props.messages[this.props.lastUserChat]]
    let updatedList = GiftedChat.append(oldMessages ,messages)
    if( updatedList.length >= 500){
      updatedList.pop();
    }
    await AsyncStorage.setItem(this.props.lastUserChat,JSON.stringify(updatedList))

    updatedMessages = {...this.props.messages}
    updatedMessages[this.props.lastUserChat] = updatedList
    this.props.updateChatList({
      messages: updatedMessages
    })
  }
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }


  render() {
    return (
      <View style={{...styles.viewContainer}}>
          <GiftedChat
            alwaysShowSend
            scrollToBottom
            renderAvatar={null}
            renderUsernameOnMessage={true}
            isTyping={true}
            multiline={false}
            renderInputToolbar={props => (
              <InputToolbar
                {...props}
                containerStyle={[{...styles.inputToolbarBackground}
                ]}
              />
            )}
            renderTime={props => {
              return (
                <Time
                  {...props}
                  textStyle={{
                    right: {
                      color: Colors.white,
                    },
                    left: {
                      color: Colors.white,
                    },
                  }}
                />
              );
            }}
            renderBubble={props => {
              return (
              <View style={{padding: moderateScale(5)}}>
                <Bubble
                  {...props}
                  textStyle={{
                    right: {
                      color: Colors.white,
                      fontFamily: 'Roboto-Regular',
                      fontSize: normalize(13),
                    },
                    left: {
                      color: Colors.white,
                      fontFamily: 'Roboto-Regular',
                      fontSize: normalize(13),
                    },
                  }}
                  wrapperStyle={{
                    left: {
                      backgroundColor: Colors.chatInputBackground,
                      borderRadius: moderateScale(15)
                    },
                    right: {
                      backgroundColor: Colors.chatRightBackground,
                      borderRadius: moderateScale(15)
                    }, 
                    height: moderateScale(25)
                  }}
                />
                <SvgXml style={[styles.triangle, props.position === 'left' ? styles.leftTriangle : styles.rightTriangle]}
                  xml={props.position === 'left' ? svgImages.leftChat : svgImages.rightChat} />
              </View>
              );
            }}
            renderSend={props => {
              return (
                <Send {...props}>
                  <View style={{...styles.sendButton, height: scale(45), width: scale(45)}}>
                    <SvgXml height={scale(16)} width={scale(16)} xml={svgImages.sendButton} />
                  </View>
                </Send>
              );
            }}
            messagesContainerStyle={{ backgroundColor: 'transparent', }}
            messages={
              ( this.props.lastUserChat === "Table Messages" ) ? this.props.tableMessages : this.props.messages[this.props.lastUserChat]
            }
            inverted={true}
            onSend={messages => this.onSend(messages)}
            user={{
              _id: 1,
              name: this.props.lastUserChat,
            }}
            textInputStyle={styles.inputText}
          />
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  viewContainer: {
    backgroundColor: Colors.blackButton,
    height: '88%',
    width: '100%',
    flexDirection: 'row'
  },
  inputText: {
    backgroundColor: Colors.chatInputBackground,
    color: Colors.white,
    borderRadius: '22@ms',
    paddingStart: '11@ms',
    paddingTop: '8@ms',
  },
  sendButton: {
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  inputToolbarBackground: {
    backgroundColor: Colors.searchBackGround,
    height: '45@s',
    paddingLeft: '12@ms',
    paddingLeft: '6@ms',
  },
  triangle: {
    top: '-6@ms',
  },
  leftTriangle: {
    left: '0@ms',
  },
  rightTriangle: {
    alignSelf: 'flex-end',
    right: '0@ms',
  },
});

const mapStateToProps = (state) => ({
  lastUserChat: state.chatActions.lastUserChat,
  messages: state.chatActions.messages,
  friendsList: state.chatActions.friendsList,
  gameTableId: state.joinTableActions.tableId,
  tableMessages: state.chatActions.tableMessages
});

const mapDispatchToProps = (dispatch) => ({
  changeShowChatWindow: (params) =>
    dispatch({ type: ChatConstants.SHOW_CHAT_WINDOW, params: params }),
  updateChatList: (params) => 
    dispatch({ type: ChatConstants.UPDATE_CHATLIST, params: params}),
  setTableMessages: (params) =>
    dispatch({ type: ChatConstants.SET_TABLE_MESSAGES, params: params})
});

export default connect(mapStateToProps, mapDispatchToProps)(FriendChat);