import * as React from 'react';
import {
  View,
  TouchableOpacity,
  Animated,
  FlatList,
  SafeAreaView,
  Text,
} from 'react-native';
import ChatItem from '../../shared/ChatItem';
import { TabView, SceneMap } from 'react-native-tab-view';
import { Colors } from '../../styles/Colors';
import SearchFilter from '../../shared/SearchFilter';
import { Strings } from '../../styles/Strings';
import { HOOL_CLIENT, listKey } from '../../shared/util';
import { SvgXml, Use } from 'react-native-svg';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import FriendsChat from '../chat/FriendChat'
import { moderateScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { connect } from 'react-redux';
import EmptyChatView from './EmptyChatView';
import  UserProfile  from './UserProfile';
import { JoinTableConstants } from '../../redux/actions/JoinaTableActions';
import { isEmulator } from 'react-native-device-info';
import { API_ENV } from '../../redux/api/config/config';

let botObj = {
  "avatarImage": 'bot',
  "indication": false, 
  "indicatorType": "", 
  "isOnline": true, 
  "name": Strings.addBot, 
}

class ChatInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      index: this.props.index,
      routes: [
        { key: 'first', title: 'Lobby', count: '5' },
        { key: 'second', title: 'Table', count: this.props.tableNotificationCount },
        { key: 'third', title: 'Friends', count: this.props.friendsNotificationCount },
        { key: 'four', title: 'Notifications', count: this.props.notificationCount }
      ],
    }
  }

  Lobby = () => (
    <View style={[styles.container]}>
      <FlatList
        style={styles.setListStyle}
        data={this.props.searchQuery == '' ? this.props.lobbyList : this.props.searchList}
        ListEmptyComponent={this.loadEmptyChat()}
        refreshing={false}
        listKey={listKey}
        ItemSeparatorComponent={null}
        keyExtractor={item => item.name}
        // renderItem={itemData => (
        //   <SafeAreaView forceInset={{ bottom: 'never', top: 'never' }}>
        //     <ChatItem
        //       ChatItem={itemData.item}
        //       view={Strings.lobby}
        //       onTouch={() => {
        //       }}
        //     />
        //     <View style={{ backgroundColor: Colors.searchBackGround }}>
        //       <View style={styles.listSeparator} />
        //     </View>
        //   </SafeAreaView>
        // )}
      />
    </View>
  );

  loadEmptyChat = (item) =>(
    <View>
      <EmptyChatView
        isRequestPending={false}
        addFriend={true}//false
        isFirstMessage={false}
      />
    </View>
  );
  
  Table = () => (
    <View style={{...styles.container}}>
      <FlatList
        style={{...styles.setListStyle}}
        data={this.props.searchQuery == '' ? this.props.tableList : this.props.searchList}
        ListEmptyComponent={this.loadEmptyChat()}
        refreshing={false}
        listKey={listKey}
        keyExtractor={item => item.name}
        renderItem={itemData => (
          <SafeAreaView forceInset={{ bottom: 'never', top: 'never',}}>
            <ChatItem
              ChatItem={itemData.item}
              view={Strings.table}
              onTouch={() => {
                this.onTouchChat(itemData.item)
                this.props.setUser({lastClickedUser: itemData.item.name});
                this.readTableMessages(itemData.item.name)
              }}
            />
            <View style={{ backgroundColor: Colors.searchBackGround }}>
              <View style={styles.listSeparator} />
            </View>
          </SafeAreaView>
        )}
      />
    </View>
  );
  
  Notifications = () => (
    <View style={[styles.container]}>
      <FlatList
        style={styles.setListStyle}
        data={this.props.notificationList}
        // ListEmptyComponent={this.loadEmptyChat()}
        refreshing={false}
        keyExtractor={item => item._id}
        listKey={listKey}
        renderItem={itemData => (
          <SafeAreaView forceInset={{ bottom: 'never', top: 'never' }}>
            <ChatItem
              ChatItem={itemData.item}
              view={Strings.notification}
              onTouch={() => {
                // this.onTouchChat(itemData.item)
              }}
              // onUserProfile={
              //   () =>{
              //     console.log("Clicked Name :",itemData.item.name)
              //     this.userClicked(itemData.item)
              //   }
              // }
            />
            <View style={{ backgroundColor: Colors.searchBackGround }}>
              <View style={styles.listSeparator} />
            </View>
          </SafeAreaView>
        )}
      />
    </View>
  );
  readMessages = ( name ) => {
     let friendsList = [...this.props.friendsList]

     let count = this.props.friendsNotificationCount
     friendsList.forEach( (element, idx) => {
      if( element.name === name){
        let updatedCount = count - friendsList[idx].unreadMessages
        this.props.setChatCount({
          friendsNotificationCount: updatedCount
        })
        friendsList[idx].isNewMessages = false
        friendsList[idx].unreadMessages = 0
      }
     })

     this.props.setFriendsList({
      data: friendsList
     })
  }

  readTableMessages = ( name )=> {
    let tableList = [...this.props.tableList]
    let tableChatCount = this.props.tableNotificationCount

    tableList.forEach( (element,idx) => {
      if( element.name === name){
        let updatedCount = tableChatCount - tableList[idx].unreadMessages
        this.props.setTableChatCount({
          tableNotificationCount: updatedCount
        })
        tableList[idx].unreadMessages = 0
        tableList[idx].isNewMessages = false
      }
    })

    this.props.setTableList({
      tableList: tableList
    })
  }
  onTouchChat = (item) => {
    
    console.log('item  ' + item.name )
    this.props.setSelectedUser({
      selectedUser: item.name
    })
    if (item.isFriend) {
      this.props.setAddFriend({
        isFriend: true
      })
      if(item.isMessageEnabled){
        this.props.setIsMessageEnabled({
          isMessageEnabled: true
        })
      }else{
        this.props.setIsMessageEnabled({
          isMessageEnabled: false
        })
      }
      if(item.isHomeUser){
        this.props.setHomeUser({
          homeUser: true
        })
      }else{
        this.props.setHomeUser({
          homeUser: false
        })
      }
    } else if (item.isRequestPending) {
      this.props.setRequestPending({
        isRequestPending: true
      })
    } else {
      this.readMessages(item.name)
      this.props.showFriendsList({
        showFriendsList: true
      })
      if (!this.props.isFirstTime) {
        this.props.setFirstTimeAndUserInfo({
          lastUserChat: item.name,
          isFirstTime: true
        })
      } else {
        this.props.setLastUserChat({
          lastUserChat: item.name,
        })
      }
    }
  }



  Friends = () => (
    <View style={[styles.container]}>
      <FlatList
        style={styles.setListStyle}
        data={this.props.searchQuery === '' ? (this.props.addFriend ? [botObj, ...this.props.friendsList] : this.props.friendsList) : this.props.searchList}
        refreshing={false}
        keyExtractor={item => item.name}
        listKey={listKey}
        renderItem={itemData => (
          <SafeAreaView forceInset={{ bottom: 'never', top: 'never' }}>
            <ChatItem
              ChatItem={itemData.item}
              view={Strings.friends}
              addFriend={this.props.addFriend}
              onTouch={() => {
                  this.props.setUser({lastClickedUser: itemData.item.name});
                  (!this.props.addFriend &&
                  this.onTouchChat(itemData.item) )
              }}
              // onUserProfile={
              //   () =>{
              //     this.userClicked(itemData.item)
              //   }
              // }
              onGameInv={()=>{
                this.triggerGameInvite(itemData.item)
                console.log(`Clicked the particular username ${itemData.item.name} for game invite`)
              }}
            />
            <View style={{ backgroundColor: Colors.searchBackGround }}>
              <View style={styles.listSeparator} />
            </View>
          </SafeAreaView>
        )}
      />
    </View>
  );
  
  // When we hit user invite,
  // Then it'll trigger this function
  triggerGameInvite = (item) => {
    var userjid;
    console.log("Triggered game invite!", item.name)
    if(item.name === 'ROBOT'){
      userjid = API_ENV.botSuffix
    } else {
      userjid = `${item.name}${API_ENV.userSuffix}`
    }
    console.log('tableid: ', this.props.tablesId, userjid, this.props.direction )
    HOOL_CLIENT.invite(this.props.tablesId, userjid, this.props.direction)
  }
  
  userClicked = (item) => {
    this.props.setClick({
      setClicked: true
    })
    this.props.setClickedUser({
      clickedUser: item.name
    })
    this.props.setUserProfile({
      profile: item
    })
    this.props.setAddFriend({
      isFriend: this.props.isFriend
    })
  }

  onIndexChange = () => {
    this.props.setIndex({
      index: this.props.index,
    });
    this.props.searchChatQuery({
      searchQuery: ''
    })
  };

  changeIndex = (i) =>{
    this.setState({ index: i })
    this.props.setIndex({ index: i })
    this.props.searchChatQuery({
      searchQuery: ''
    })
  }

  

  _renderTabBarTable = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    console.warn("RENDER TABEL ", inputRange)
    return (
      <View style={this.props.isTableUser ? styles.tabBar: {...styles.tabBar, width: scale(220)}}>
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(inputIndex =>
              inputIndex === i ? 1 : 0.5
            ),
          });
          return (
            <TouchableOpacity
              style={styles.tabItem}
              onPress={() => {
                if(i == 1){
                  this.changeIndex(i)
                }
              }}
              key={i}
            >
              {i !== this.props.index ? (
                <Animated.Text style={{ ...styles.tabTextInActive, opacity }}>
                  {route.title}
                </Animated.Text>
              ) : (
                <Animated.Text style={{ ...styles.tabTextActive, opacity }}>
                  {route.title}
                </Animated.Text>
              )}
              {i === 3 && i !== this.props.index  ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml height={this.props.notificationCount > 99? scale(14): scale(12)} width={this.props.notificationCount > 99? scale(14):scale(20)} xml={svgImages.notificationBG} />
                  <View style={styles.centerView}>
                    <Animated.Text style={{ ...styles.countText, opacity }}>
                    {this.props.notificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : i === 3 && i === this.props.index  ? (
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)}}>
                  <SvgXml height={this.props.notificationCount > 99? scale(14): scale(12)} width={this.props.notificationCount > 99? scale(14):scale(20)} xml={svgImages.active}/>
                  <View style={styles.centerView}>
                    <Animated.Text style={{ ...styles.countText, opacity }}>
                    {this.props.notificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : i === 2 && i === this.props.index ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml height={this.props.friendsNotificationCount < 99? scale(12):scale(14)} width={this.props.friendsNotificationCount < 99? scale(20):scale(14)} xml={svgImages.blue} />
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.friendsNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ): i === 2 && i !== this.props.index ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml height={this.props.friendsNotificationCount < 99? scale(12):scale(14)} width={this.props.friendsNotificationCount < 99? scale(20):scale(14)} xml={svgImages.otherBG} />
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.friendsNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ): i === 1 && i === this.props.index ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4),  }}>
                  <SvgXml height={this.props.tableNotificationCount < 99? scale(12): scale(14)} width={this.props.tableNotificationCount < 99? scale(20):scale(14)} xml={svgImages.green}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.tableNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : i === 1 && i !== this.props.index ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4),  }}>
                  <SvgXml height={this.props.tableNotificationCount < 99? scale(12): scale(14)} width={this.props.tableNotificationCount < 99? scale(20):scale(14)} xml={svgImages.otherBG}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.tableNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ): i === 0 && i === this.props.index ?(
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4),  }}>
                  <SvgXml height={route.count < 99? scale(12): scale(14)} width={route.count  < 99? scale(20):scale(14)} xml={svgImages.green}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {route.count}
                    </Animated.Text>
                  </View>
                </View>
              ) : (
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml  height={route.count < 99? scale(12):scale(14)} width={route.count  < 99? scale(20):scale(20)} xml={svgImages.otherBG}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {route.count}
                    </Animated.Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  _renderTabBarHome = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={this.props.isTableUser ? styles.tabBar: {...styles.tabBar, width: scale(220)}}>
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(inputIndex =>
              inputIndex === i ? 1 : 0.5
            ),
          });
          return (
            <TouchableOpacity
              style={styles.tabItem}
              onPress={() => this.changeIndex(i)}
              key={i}
            >
              {i !== this.props.index ? (
                <Animated.Text style={{ ...styles.tabTextInActive, opacity }}>
                  {route.title}
                </Animated.Text>
              ) : (
                <Animated.Text style={{ ...styles.tabTextActive, opacity }}>
                  {route.title}
                </Animated.Text>
              )}
              {i === 1 && i !== this.props.index  ? (
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml height={this.props.notificationCount > 99? scale(14): scale(12)} width={this.props.notificationCount > 99? scale(14):scale(20)} xml={svgImages.notificationBG} />
                  <View style={styles.centerView}>
                    <Animated.Text style={{ ...styles.countText, opacity }}>
                      {this.props.notificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : i === 1 && i === this.props.index  ? (
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)}}>
                  <SvgXml height={this.props.notificationCount > 99? scale(14): scale(12)} width={this.props.notificationCount > 99? scale(14):scale(20)} xml={svgImages.active}/>
                  <View style={styles.centerView}>
                    <Animated.Text style={{ ...styles.countText, opacity }}>
                    {this.props.notificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : i === 0 && i === this.props.index ?(
                <View style={{  flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4),  }}>
                  <SvgXml height={this.props.friendsNotificationCount < 99? scale(12): scale(14)} width={this.props.friendsNotificationCount  < 99? scale(20):scale(14)} xml={svgImages.blue}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.friendsNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              ) : (
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginEnd: moderateScale(4)  }}>
                  <SvgXml  height={this.props.friendsNotificationCount < 99? scale(12):scale(14)} width={this.props.friendsNotificationCount < 99? scale(20):scale(20)} xml={svgImages.otherBG}/>
                  <View style={styles.centerView}>
                    <Animated.Text
                      style={{ ...styles.otherCountText, opacity }}
                    >
                      {this.props.friendsNotificationCount}
                    </Animated.Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };


  _renderFriendTab = props => {
    return (
        <View style={this.props.isTableUser ? styles.tabBar : {...styles.tabBar, width: scale(100)}}>
          {props.navigationState.routes.map((route, i) => {
            return (
              <TouchableOpacity
                style={{...styles.tabItem,  height: scale(0)}}
                key={i}
              >
                  <Animated.Text style={{ ...styles.tabTextInActive, opacity: 1 }}>
                    {'      '}
                    {console.log('route.title'+route.title)}
                  </Animated.Text>
              </TouchableOpacity>
            );
          })}
        </View>
      )
    //)
  };

  _renderFriendsScene = SceneMap({
    first: this.Friends,
  });

  _renderScene = SceneMap({
    first: this.Lobby,
    second: this.Table,
    third: this.Friends,
    four: this.Notifications,
  });

  componentDidMount() {
    console.log("chatinfo component mounted  "+ this.props.fetchUsersList() )
    if(this.props.addFriend){
      this.props.fetchFriendsList()
      this.props.fetchUsersList()
      console.log(this.props.fetchUsersList())
      // For users
      console.log("Fetch user lsit"+this.props.friendsList)
      this.setState({routes: [
        { key: 'first', title: 'Friends', count: this.props.friendsNotificationCount},
      ]})
    }else{
      if(this.props.isTableUser){
        this.setState({routes: [
          { key: 'first', title: 'Lobby', count: '0' },
          { key: 'second', title: 'Table', count: this.props.tableNotificationCount },
          { key: 'third', title: 'Friends', count: this.props.friendsNotificationCount },
          { key: 'four', title: 'Notifications', count: this.props.notificationCount }
        ]})
      }else{
        if( this.props.index === 2){
          this.props.setIndex({
            index: 0
          })
          this.setState({
            index: 0
          })
        }
        if( this.props.index === 3){
          this.props.setIndex({
            index: 1
          })
          this.setState({
            index: 1
          })
        }
        this.setState({routes: [
          { key: 'third', title: 'Friends', count: this.props.friendsNotificationCount },
          { key: 'four', title: 'Notifications', count: this.props.friendsNotificationCount }
        ]})
      }
      {console.log("count" + this.props.friendsList.length)}
      if(this.props.lastUserChat == undefined || !this.props.isTableUser){
        this.props.showFriendsList({
          showFriendsList: false
        })
      }else{
        this.props.showFriendsList({
          showFriendsList: true
        })
      }
    }
  }

  componentWillUnmount() {}

  //Get Users function
  // async getUsers() {
  //   const response = await fetch(
  //     'http://hool.org:5000/api/users/'
  //   )
  //   let actualData = await response.json()
  //   setData(actualData)
  //   console.log('actual'+actualData)
  // }

  // getUsers = () => {
  //   console.log("called getusers")
  //   let data = fetch('http://hool.org:5000/api/users/',{
  //     method:'GET',
  //     headers:{
  //       Accept: 'application.json'
  //     }
  //   })
  //     .then(response => response.json())
  //     .then(item => { 
  //       console.log(item)
  //     })
  //       // let userOnly = data.users
  //       // let key = 'jid'
  //       // for(key in userOnly){
  //       //   console.log(`Userjid - ${key} : ${userOnly[key]}`);
        
  //       //console.log("data.users,freinds  "+data.users.friends)
  //   .catch(err => console.error(err))
  //   //console.log('...........Response.status  '+users)
  //   console.log('data '+data)
  // }
 
  getUsername = (userID) => { 
    const notList = [];
    for(let i=0;i<this.props.friendsList.length;i++){
        notList.push(this.props.friendsList[i].name)
    }
    console.log(`""""Getname is ${userID}`)
    console.log('"""Get User ID are'+ notList)
    return notList
    //return this.getUsername
  }


  render() {
    return (
      <>
        {/* <Spinner
            visible={this.props.spinner}
        /> */}
        
        {
            (this.props.lastUserChat !== undefined && this.props.isFriendsListShown) ?
            <>
              {console.log('----this.props.lastUserChat '+ this.props.lastUserChat)}
              <View style={styles.topContainer} >
                {
                  this.props.lastUserChat == 'Table Messages' ?
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                      <View style={{ flexDirection: 'row',}}>
                        <SvgXml height={scale(17)} width={scale(17)} xml={svgImages.profile2} 
                        style={{backgroundColor: 'transparent', marginRight: moderateScale(3), }} />
                        <SvgXml height={scale(17)} width={scale(17)} xml={svgImages.profile1}
                        style={{backgroundColor: 'transparent'}} />
                      </View>
                      <SvgXml height={scale(17)} width={scale(17)} xml={svgImages.kibitzerProfile} 
                       style={{backgroundColor: 'transparent'}}/>
                    </View>
                  :
                    <SvgXml height={scale(28)} width={scale(28)} style={{alignSelf: 'center', margin: moderateScale(5)}} xml={svgImages.profile1} /> 
                }
                <Text style={styles.userNameStyle}>{this.props.lastUserChat}</Text>                
                  <TouchableOpacity
                    style={{
                      position: 'absolute',
                      height: scale(45),
                      width: scale(45),
                      right: 0,
                    }}
                    onPress={() => 
                      {
                        this.props.setLastUserChat({lastUserChat: undefined})
                        this.props.setUser({lastClickedUser: undefined})
                        this.props.showFriendsList({
                          showFriendsList: false
                        })
                        console.log("this is clicked when after invite sent")
                      }
                    }
                  >
                    <SvgXml height={scale(40)} width={scale(40)} xml={svgImages.back} />
                  </TouchableOpacity>
              </View>
              <FriendsChat style={{backgroundColor: 'green'}}/>
            </>
          :
            (this.props.setClicked) ?
            <UserProfile 
              name={this.props.clickedUser} 
              isFriend={true}
            />
          :
            (this.props.isFriend || this.props.isRequestPending) ?
              <EmptyChatView
                isFirstMessage={false}
              />
            :
            <>
              <SearchFilter />
              {!this.props.addFriend ?
              <TabView
                style={styles.tabStyle}
                activeTabStyle={styles.tabTextInActive}
                swipeEnabled={false}
                navigationState={this.state}
                renderScene={ this.props.addFriend ? this._renderFriendsScene:this._renderScene}
                renderTabBar={this.props.addFriend ? this._renderFriendTab : this.state.routes.length === 2 ? this._renderTabBarHome : this._renderTabBarTable}
                onIndexChange={this.onIndexChange}
              /> : 
               <TabView
                style={styles.tabStyleAdd}
                activeTabStyle={styles.tabTextInActive}
                swipeEnabled={false}
                navigationState={this.state}
                renderScene={ this.props.addFriend ? this._renderFriendsScene:this._renderScene}
                renderTabBar={this.props.addFriend ? this._renderFriendTab : this.state.routes.length === 2 ? this._renderTabBarHome : this._renderTabBarTable}
                onIndexChange={this.onIndexChange}
              /> 
              }
            </> 
        }
        
      </>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1
  },
  setListStyle: {
    flex: 1
  },
  tabBar: {
    flexDirection: 'row',
    color: 'white',
    width: '440@s',
  },
  tabStyle: {
    marginStart: '17@ms',
    marginEnd: '18@ms',
  },
  tabStyleAdd: {
    marginStart: '17@ms',
    marginEnd: '55@ms',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '13@ms',
    marginEnd: '5@ms',
    height: '21@s',
    backgroundColor: Colors.searchBackGround,
    paddingStart: '10@ms',
    borderTopRightRadius: '5@ms',
    borderTopLeftRadius: '5@ms',
  },
  tabTextActive: {
    color: Colors.tabTextColor,
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(13),
    flex: 1
  },
  tabTextInActive: {
    color: Colors.searchPlaceHolder,
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(13),
    flex: 1
  },
  centerView: {
    position: 'absolute',
    alignSelf: 'center',
  },
  countText: {
    color: Colors.black,
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(8),
    marginStart: '1@ms',
  },
  otherCountText: {
    color: Colors.black,
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(8),
  },
  listSeparator: {
    backgroundColor: Colors.tabBackground,
    height: '1@s',
    marginStart: '54@ms',
    marginEnd: '15@ms'
  },
  topContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: Colors.searchBackGround
  },
  userNameStyle: {
    color: Colors.white,
    fontFamily: 'Roboto-Bold',
    fontSize: normalize(15),
    marginStart: '10@ms',
    marginTop: '10@ms',
    marginBottom: '11@ms',
    alignSelf: 'center',
  },
});

const mapStateToProps = (state) => ({
  isTableUser: state.chatActions.isTableUser,
  isFirstTime: state.chatActions.isFirstTime,
  lastUserChat: state.chatActions.lastUserChat,
  isFriendsListShown: state.chatActions.showFriendsList,
  friendsList: state.chatActions.friendsList,
  usersList: state.chatActions.usersList,
  lobbyList: state.chatActions.lobbyList,
  tableList: state.chatActions.tableList,
  notificationList: state.chatActions.notificationList,
  spinner: state.chatActions.showSpinner,
  searchList: state.chatActions.searchList,
  searchQuery:  state.chatActions.searchQuery,
  index: state.chatActions.index,
  isFriend: state.chatActions.isFriend,
  isRequestPending: state.chatActions.isRequestPending,
  direction: state.joinTableActions.direction,
  addFriend: state.joinTableActions.addFriend,
  selectedUser: state.chatActions.selectedUser,
  notificationCount: state.chatActions.notificationCount,
  friendsNotificationCount: state.chatActions.friendsNotificationCount,
  setClicked: state.chatActions.setClicked,
  clickedUser: state.chatActions.clickedUser,
  usersList: state.chatActions.usersList,
  userFriend: state.chatActions.userFriend,
  tablesId: state.joinTableActions.tablesId,
  direction: state.joinTableActions.direction,
  tableList: state.chatActions.tableList,
  tableNotificationCount: state.chatActions.tableNotificationCount,  
});

const mapDispatchToProps = (dispatch) => ({
  showFriendsList: (params) =>
    dispatch({ type: ChatConstants.SHOW_FRIENDS_LIST, params: params }),
  setFirstTimeAndUserInfo: (params) =>
    dispatch({ type: ChatConstants.IS_FIRST_TIME_USER, params: params }),
  setLastUserChat: (params) =>
    dispatch({ type: ChatConstants.LAST_USER_CHATTED, params: params }),
  setIndex: (params) =>
    dispatch({ type: ChatConstants.SET_CHAT_INDEX, params: params }),
  searchChatQuery: (params) =>
    dispatch({ type: ChatConstants.SEARCH_CHAT_QUERY, params: params }),
  setAddFriend: (params) =>
    dispatch({ type: ChatConstants.SET_IS_FRIEND, params: params }),
  setRequestPending: (params) =>
    dispatch({ type: ChatConstants.SET_REQUEST_PENDING, params: params }),
  setIsMessageEnabled: (params) =>
    dispatch({ type: ChatConstants.SET_IS_MESSAGE_ENABLED, params: params }),
  setHomeUser: (params) =>
    dispatch({ type: ChatConstants.SET_HOME_USER, params: params }),
  fetchFriendsList: (params) =>
    dispatch({ type: ChatConstants.FETCH_FRIENDS_LIST, params: params }),
  fetchUsersList: (params) => 
    dispatch({ type: ChatConstants.FETCH_USER_LIST, params: params}),
  setSelectedUser: (params) => 
    dispatch({ type: ChatConstants.SELECTED_USER, params: params}),
  setUser: (params) =>
    dispatch({ type: ChatConstants.SET_LAST_CLICKED_USER, params: params}),
  setFriendsList: (params) =>
   dispatch({ type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS, params: params}),
  setChatCount: (params) =>
   dispatch({ type: ChatConstants.SET_CHAT_NOTIFICATION_COUNT, params: params}),
  setClick: (params) => 
   dispatch({ type: ChatConstants.SET_CLICKED, params: params}),
  setClickedUser: (params) => 
    dispatch({ type: ChatConstants.CLICKED_USER, params: params}),
  triggerGameInvite: (params) => 
    dispatch({ type: JoinTableConstants.GAME_INVITE, params: params}),
  addFriendPlayer: (params) =>
    dispatch({ type: JoinTableConstants.FRIENDS_LIST, params: params }),
  setTableId: (params) =>
    dispatch({ type: JoinTableConstants.SET_TABLE_ID, params: params }),
  setTableList: (params) =>
    dispatch({ type: ChatConstants.SET_TABLE_LIST , params: params}),
  setTableChatCount: (params) =>
    dispatch({ type: ChatConstants.TABLE_NOTIFICATION_COUNT, params: params}),
  setUserProfile: (params) => 
    dispatch({ type: ChatConstants.SET_LAST_CLICKED_USER_PROFILE, params: params}),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatInfo);


