/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import Modal from 'react-native-modal';
import { SvgXml} from 'react-native-svg';
import { Strings } from '../../styles/Strings';
import { Colors } from '../../styles/Colors';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import { moderateScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { HOOL_CLIENT } from '../../shared/util';


class EmptyChatView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstMessage: props.isFirstMessage,
      modalVisible: false,
      modalShow:false,
      textValue: Strings.addFriends
    }
  }
  
  componentDidMount() { }
  componentWillUnmount() { }

  closeWindow(){
    if(this.props.isFriend){
      this.props.setAddFriend({
        isFriend: false
      })
    }else if(this.props.isRequestPending){
      this.props.setRequestPending({
        isRequestPending: false
      })
    }else if(this.props.isHomeUser){
      this.props.setHomeUser({
        homeUser: false
      })
    }
  }

  // After hitting Add Friend Button, 
  // That state of button should be Changed to Request Pending 

  // toggleSwitch = () => {
  //   console.log('toggle called   '+ this.state.isAddFriend)
  //   // Set the isAddFriend as true
  //   // for the first time
  //   this.state.isAddFriend = true
  //   if(this.state.isAddFriend) {
  //     this.props.setRequestPending({
  //       isRequestPending: true
  //     })
  //   }
  //   if(this.props.isRequestPending) {
  //     this.props.setRequestPending({
  //       isRequestPending: false
  //     })
  //   }
  // }

  // For adding as a Friend, 
  // We should get his username and put it in the array list
  // Then we send a Friend Request to his ID

  addFriendReqFor = () => {
    let userId = `${this.props.selectedUser}@hool.org`
      HOOL_CLIENT.rosterSubscribe(userId)
      HOOL_CLIENT.rosterSubscribed(userId)
      console.log("add friend req called!");
      
    console.log("Add friend called from :"+ HOOL_CLIENT.jid +" to :"+ userId )
  }

  // indvidualFrie = () => {
  //   let userId = `${this.props.selectedUser}@hool.org`
  //   var userFriend = []
  //   var usrFrd = userFriend.push(userId)
  //   if(this.props.isRequestPending){
  //     this.props.setUserFriend({
  //       userFriend: usrFrd
  //     })
  //   }
  //   console.log("Add friend called from :"+ HOOL_CLIENT.jid +" to :"+ userId )
  // }

  setReqVal(){
    this.props.setRequestPending({
      isRequestPending: true
    })
    this.setState({
      textValue: Strings.requestPending
    })
    this.addFriendReqFor()
    // this.indvidualFrie()
    console.log("add friend req called!");
  }



  // showRemOrBlockModal(){
  //   this.setState({modalShow:true})
  // }
  render() {
    return (
      <View style={styles.containerStyle}>
        <>
        <TouchableOpacity 
          style={{ right: 0, position: 'absolute' }}
          onPress={() => {this.closeWindow()}}
        >
          <SvgXml 
            width={scale(45)} 
            height={scale(45)} 
            xml={svgImages.back} 
          />
        </TouchableOpacity>
        <View 
          style={{ 
            width: scale(213), 
            height: scale(110), 
            alignItems: 'center', 
            justifyContent: 'center', 
            alignSelf: 'center', 
            top: moderateScale(20)
          }}
        >
          <SvgXml 
            height={moderateScale(58)} 
            width={moderateScale(58)} 
            xml={svgImages.profile1} 
            style={styles.avatarStyle}
          />
              <Text style={styles.nameStyle}>
                  {this.props.selectedUser}
              </Text>
              <Text style={styles.suNameStyle}>{'Durga'}</Text>
              <Text style={{...styles.suNameStyle, paddingTop: 0}}>{Strings.flagText}</Text>
        </View>
          
          {
            this.props.isFriend &&
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',top: moderateScale(20)}}>
              <TouchableOpacity style={{ ...styles.rowContiner , top: moderateScale(10)}} 
                disabled={this.props.isHomeUser}
                onPress={()=> this.setReqVal()}
              >
                <SvgXml height={scale(32)} width={scale(176)} xml={svgImages.addFriend} />
                <View 
                  style={{
                    ...styles.centerView,
                    width: scale(150), 
                    height: scale(25), 
                    justifyContent:"center"
                  }}
                >
                  <Text style={{...styles.textStyle, paddingBottom: moderateScale(3)}}>
                    {this.state.textValue}
                  </Text>
                    {/* {
                      !this.props.isHomeUser  && 
                      <SvgXml xml={svgImages.dropDown} style={{...styles.dropDown, left: moderateScale(120)}} />
                    } */}
                </View>
              </TouchableOpacity>
              {
                this.props.isMessageEnabled &&
                  <TouchableOpacity style={{ ...styles.rowContiner, top: moderateScale(10) }}>
                    <SvgXml height={scale(32)} width={scale(176)} xml={svgImages.addFriend} />
                    <View style={
                      styles.centerView
                    }>
                      <Text style={
                        styles.textStyle
                      }>
                        {Strings.message}
                      </Text>
                    </View>
                  </TouchableOpacity>
              }
            </View>
          }
          {
            this.state.isFirstMessage &&
            <View style={{...styles.rowContiner,top: moderateScale(28)}}>
                
              <TouchableOpacity 
                style={{ ...styles.rowContiner, flexDirection: 'row' }}
                onPress={()=> this.showRemOrBlockModal()}
              >
                <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.message} />
                <View style={
                  {...styles.centerView,}
                }>
                  <Text style={{...styles.textStyle}}>{Strings.friends}</Text>
                  <SvgXml xml={svgImages.dropDown} style={{...styles.dropDown}} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ ...styles.rowContiner }}>
                  <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.message} />
                  <View style={
                    styles.centerView
                  }>
                    <Text style={
                      styles.textStyle
                    }>{Strings.message}</Text>
                  </View>
                </TouchableOpacity>
              <TouchableOpacity style={{ ...styles.rowContiner }}
              >
                <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.leave} />
                <View style={
                  styles.centerView
                }>
                  <Text style={
                    {...styles.textStyle}
                  }>{Strings.vacateKick}</Text>
                  <SvgXml xml={svgImages.dropDown} style={{...styles.dropDown, left: moderateScale(77)}} />
                </View>
              </TouchableOpacity>
            </View>
          } 
          {
          /*
            this.props.isRequestPending &&
            <TouchableOpacity style={{...styles.rowContiner, top: moderateScale(20)}} onPress={()=> this.showAddaFriendorBlockModal()}>
              <SvgXml height={scale(32)} width={scale(176)} xml={svgImages.addFriend} />
              <View style={
                {...styles.centerView, width: scale(150), height: scale(25), justifyContent:"center"}
              }>
                <Text style={{
                  ...
                  styles.textStyle, opacity: 0.3, paddingBottom: moderateScale(3)
                }
                }>{Strings.requestPending}</Text>
              </View>
            </TouchableOpacity>
          */
          }
          <View style={{ width: scale(461), height: scale(62), alignSelf: 'center', justifyContent: 'center', top: moderateScale(20)}}>
            <Text style={styles.descriptionStyle}>
              {Strings.emptyChat}
            </Text>
            <Text style={styles.subDescriptionStyle}>
              {Strings.emptyChat1}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',top: moderateScale(20)}}>
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} style={styles.centerEllipseStyle} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
          </View>
          <View style={{flexDirection: 'row', bottom: moderateScale(-20), alignItems: 'center', justifyContent: 'center'}}>
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.halfCircle} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.halfCircle} style={styles.centerEllipseStyle} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.halfCircle} />
          </View>

          {
            // This is a modal for pop up, when Add friend 
            // Or Blocking...... and also for Removing a friend .....
          }

          {/* <Modal
            style={{
              alignItems: 'center', 
              justifyContent: 'center', 
              alignSelf: 'center', 
              marginLeft: scale(150)
            }}
            animationType="slide"
            visible={this.state.modalVisible}
          >
            <View 
              style={{
                width: scale(110),
                height: scale(30), 
                backgroundColor: 'red', 
                borderColor: '#1E1E1E',
                borderWidth: 0.5,
                paddingBottom: scale(5),
                justifyContent: 'center', 
                alignItems: 'center'
              }}
            >
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false });
                    this.addFriendReqFor();
                    this.toggleSwitch()
                  }}
                >
                  <Text style= {{...styles.textStyle,}}>
                    {
                      this.props.isRequestPending ? (Strings.cancelRequest) : (Strings.addFriends)
                    }
                  </Text>
                </TouchableOpacity>
            </View>
          </Modal>
          <Modal
            style={{
              alignItems: 'center', 
              justifyContent: 'center', 
              alignSelf: 'center', 
              marginLeft: moderateScale(25)
            }}
            animationType="slide"
            visible={this.state.modalShow}
          >
            <View
              style={{
                width: scale(120),
                height: scale(40), 
                backgroundColor: 'red', 
                borderColor: '#1E1E1E',
                borderWidth: 0.5,
                paddingBottom: scale(5),
                justifyContent: 'center', 
                alignItems: 'center'
              }}
            >
              <TouchableOpacity
                onPress={()=>{
                  this.setState({ modalShow: false });
                }}
              >
                <Text style={{...styles.textStyle}}>
                  {Strings.removeFriend}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  onPress={() => {
                    this.setState({modalShow:false});
                  }}>
                  <Text style= {{...styles.textStyle}}>{Strings.block}</Text>
                </TouchableOpacity>
            </View>
          </Modal> */}
        </>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  coverImage: {
    width: '100%',
    height: '200@s',
  },
  rowContiner: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
      backgroundColor: Colors.blackButton,
      height: '100%',
  width: '100%',
  },
  nameStyle: {
    color: Colors.white,
    fontSize: normalize(16),
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    paddingTop: '4@ms',
  },
  suNameStyle: {
    color: Colors.white,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    fontWeight: 'normal',
    paddingTop: '4@ms',
  },
  descriptionStyle: {
    color: Colors.emptyDescription,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Italic',
    paddingTop: '10@ms',
  },
  subDescriptionStyle: {
    color: Colors.emptyDescription,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Italic',
    paddingBottom: '5@ms',
  },
  centerEllipseStyle: {
    marginStart: '60@ms',
    marginEnd: '60@ms',
  },
  avatarStyle: {
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  centerView: {
    position: 'absolute',
    width: '73@s',
    height: '25@s',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textStyle: {
    color: Colors.white,
    fontFamily: 'Roboto',
    fontSize: normalize(13),
    marginTop: '3@ms'
  },
  dropDown: {
    position: 'absolute',
    alignSelf: 'flex-end',
    width: '2@s',
  }
});

const mapStateToProps = (state) => ({
  isFriend: state.chatActions.isFriend,
  isRequestPending: state.chatActions.isRequestPending,
  isMessageEnabled: state.chatActions.isMessageEnabled,
  isHomeUser: state.chatActions.homeUser,
  users: state.chatActions.users,
  selectedUser: state.chatActions.selectedUser,
  lastUserChat: state.chatActions.lastUserChat
});

const mapDispatchToProps = (dispatch) => ({
  setAddFriend: (params) =>
    dispatch({ type: ChatConstants.SET_IS_FRIEND, params: params }),
  setRequestPending: (params) =>
    dispatch({ type: ChatConstants.SET_REQUEST_PENDING, params: params }),
  setHomeUser: (params) =>
    dispatch({ type: ChatConstants.SET_HOME_USER, params: params }),
  setUsers:(params) => 
    dispatch({ type: ChatConstants.SET_USERS , params: params}),
  setSelectedUser: (params) => 
    dispatch({ type: ChatConstants.SELECTED_USER, params: params}),
  // setUserFriend: (params) => 
    // dispatch({ type: ChatConstants.USER_FRIEND, params: params})
});

export default connect(mapStateToProps, mapDispatchToProps)(EmptyChatView);

/**
 * showFriendsList: (params) =>
    dispatch({ type: ChatConstants.SHOW_FRIENDS_LIST, params: params }),
  fetchFriendsList: (params) =>
    dispatch({ type: ChatConstants.FETCH_FRIENDS_LIST, params: params }),
 */

    //Very imp:
    /**
     * let notificationList = [];
      notificationList.unshift(this.props.dataUsername); 
      const notifyList = notificationList.map((userID) => { userID })
      console.log('notify list '+notificationList)
      return notifyList
     * 
     */