import React from 'react';
import {
  View,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import ChatJson from '../../assets/data/chat.json';
import Back from '../../assets/Svg/Back.svg';
import EmptyChatView from './EmptyChatView';
import { handleBackButton } from '../../shared/util';
import { SvgXml } from 'react-native-svg';
import { Colors } from '../../styles/Colors';
import ChatInfo from './ChatInfo';
import { connect } from 'react-redux';

class Chat extends React.Component {
  
  handleCurrentBackButton = () => {
    this.clearListeners();
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    this.props.navigation.goBack();
    return true;
  };

  goBack() {
    this.props.navigation.goBack();
  }

  componentDidMount() {
    console.log("chat component did mount")
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  componentWillUnmount() {
    this.clearListeners();
  }

  clearListeners() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  render() {
    return (
      <View style={styles.viewContainer}>
        <View style={this.props.isTableUser ? {...styles.rootContainer} : {...styles.rootContainer, backgroundColor: '#0a0a0a'}}>
            <ChatInfo />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: Colors.blackButton,
    height: '100%',
    width: '100%',
    backgroundColor: 'red',
    flexDirection: 'row',
  },
  rootContainer: {
    flex: 1,
    backgroundColor: Colors.black,
  },
  backPosition: {
    alignItems: 'flex-end'
  }
});

const mapStateToProps = (state) => ({
  isTableUser: state.chatActions.isTableUser
});

const mapDispatchToProps = (dispatch) => ({
  
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
