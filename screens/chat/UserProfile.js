/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
import React,{ useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { SvgXml} from 'react-native-svg';
import { Strings } from '../../styles/Strings';
import { Colors } from '../../styles/Colors';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import { moderateScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import { ScrollView } from 'react-native-gesture-handler';
import { HOOL_CLIENT, restrictUserNameToEightChar } from '../../shared/util';
import { SCREENS } from '../../constants/screens';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { connect } from 'react-redux';
import getUnicodeFlagIcon from 'country-flag-icons/unicode'


const UserProfile = (props) => {

  const [textValue, setValue] = useState(Strings.addFriends)
  const closeWindow = () => {
    // props.updateDeskStateValues({
    //   showUserProfile: false,
    //   profileName: '',
    //   profileSubname: ''
    // })
  }

  const KickOutUserFromTable = () =>{
    let seat = (props.kickoutUserDirection == Strings.north) ? 
      Strings.directionNorth : (props.kickoutUserDirection == Strings.south) ?
      Strings.directionSouth : (props.kickoutUserDirection == Strings.east) ?
      Strings.directionEast : (props.kickoutUserDirection == Strings.west)
      Strings.directionWest
    HOOL_CLIENT.kickoutUser(props.tableId, seat)
    closeWindow()
  }
  
  const changeVal = () => {
    setValue(Strings.requestPending)
  }
  // const changeShow = () => {
  //   setShowUserProfile(true)
  // }


  return (
    //{console.log('users')}
    <View style={{ ...styles.containerStyle, height: '100%' }}>
      <>
        <ScrollView>
          <View style={{ width: scale(213), height: scale(110), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', top: moderateScale(20)}}>
            <SvgXml height={moderateScale(58)} width={moderateScale(58)} xml={svgImages.profile1} style={{...styles.avatarStyle}} />
            <Text style={styles.nameStyle}>{restrictUserNameToEightChar(props.userProfile.name)}</Text>
            {
              props.userProfile.subName != '' &&
              <Text style={styles.suNameStyle}>{props.userProfile.subName}</Text>
            }
            {/* <Text style={{ ...styles.suNameStyle, paddingTop: 0 }}>
              { props.userProfile.flagText != '' &&
              getUnicodeFlagIcon(props.userProfile.flagText)
              }
            </Text> */}
          </View>
          <View style={{ ...styles.rowContiner, top: moderateScale(17)}}>
            {
              !props.isFriend &&
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',top: moderateScale(0)}}>
                <TouchableOpacity 
                  style={{ ...styles.rowContiner , top: moderateScale(10)}} 
                  onPress={()=> changeVal()}
                >
                  <SvgXml height={scale(32)} width={scale(176)} xml={svgImages.addFriend} />
                  <View 
                    style={{
                      ...styles.centerView,
                      width: scale(150), 
                      height: scale(25), 
                      justifyContent:"center"
                    }}
                  >
                    <Text style={{...styles.textStyle, paddingBottom: moderateScale(3)}}>
                      {textValue}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            }
          </View>
            {
              props.isFriend &&
              <View style={{ ...styles.rowContiner, top: moderateScale(28) }}>
                {props.currentScreen !== SCREENS.JOIN_TABLE &&
                  <TouchableOpacity style={{ ...styles.rowContiner, flexDirection: 'row' }}>
                    <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.message} />
                    <View style={
                      { ...styles.centerView, }
                    }>
                      <Text style={{ ...styles.textStyle }}>{Strings.friends}</Text>
                      <SvgXml xml={svgImages.dropDown} style={{ ...styles.dropDown, top: moderateScale(13) }} />
                    </View>
                  </TouchableOpacity>
                }
                {
                  props.currentScreen !== SCREENS.JOIN_TABLE &&
                  <TouchableOpacity style={{ ...styles.rowContiner }}>
                    <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.message} />
                    <View style={
                      styles.centerView
                    }>
                      <Text style={
                        styles.textStyle
                      }>{Strings.message}</Text>
                    </View>
                  </TouchableOpacity>
                }
                {props.currentScreen !== SCREENS.JOIN_TABLE && !props.kickedUserisHost && props.isHost&&
                  <TouchableOpacity style={{ ...styles.rowContiner }}
                    onPress={() => {KickOutUserFromTable()}}
                  >
                    <SvgXml height={scale(32)} width={scale(124)} xml={svgImages.leave} />
                    <View style={
                      styles.centerView
                    }>
                      <Text style={
                        { ...styles.textStyle }
                      }>{Strings.removePlayer}</Text>
                      <SvgXml width= {scale=(5)} xml={svgImages.dropDown} style={{ ...styles.dropDown, left: moderateScale(77) }} />
                    </View>
                  </TouchableOpacity>
                }
              </View>
            }
          <View style={{ width: scale(461), height: scale(62), alignSelf: 'center', justifyContent: 'center', top: moderateScale(20) }}>
            <Text style={styles.descriptionStyle}>
              {}
            </Text>
            <Text style={styles.subDescriptionStyle}>
              {}
            </Text>
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', top: moderateScale(20) }}>
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} style={styles.centerEllipseStyle} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
          </View>
          <View style={{ flexDirection: 'row', top: moderateScale(40), alignItems: 'center', justifyContent: 'center' }}>
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} style={styles.centerEllipseStyle} />
            <SvgXml height={scale(84)} width={scale(84)} xml={svgImages.ellipse} />
          </View>
        </ScrollView>
        {/* <Text style={styles.nameStyle}>Userjid</Text> */}
        <TouchableOpacity 
          style={{ right: 0, position: 'absolute',}} 
          onPress={() => {
            props.setClick({
              setClicked: false
            })
            props.setUserProfile({
              profile: undefined
            })
            // props.clickedUserProfile({
            //   clickedProf: false
            // })
            closeWindow()
            // changeShow()
          }}>
          <SvgXml width={scale(45)} height={scale(45)} xml={svgImages.back} />
        </TouchableOpacity>
      </>
    </View>
  );
}



const styles = ScaledSheet.create({
  containerStyle: {
    backgroundColor: Colors.black,
    flexDirection: 'row',
    width:'100%'
  },
  rowContiner: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameStyle: {
    color: Colors.white,
    fontSize: normalize(16),
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    paddingTop: '4@ms',
  },
  suNameStyle: {
    color: Colors.white,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    fontWeight: 'normal',
    paddingTop: '4@ms',
  },
  descriptionStyle: {
    color: Colors.emptyDescription,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Italic',
    paddingTop: '10@ms',
  },
  subDescriptionStyle: {
    color: Colors.emptyDescription,
    fontSize: normalize(13),
    textAlign: 'center',
    fontFamily: 'Roboto-Italic',
    paddingBottom: '5@ms',
  },
  centerEllipseStyle: {
    marginStart: '60@ms',
    marginEnd: '60@ms',
  },
  avatarStyle: {
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  textStyle: {
    color: Colors.white,
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(11),
    marginTop: '3@ms'
  },
  dropDown: {
    position: 'absolute',
    alignSelf: 'flex-end',
    width: '10@s',
    height: '5@s',
    right: '12@s'
  },

  centerView: {
    position: 'absolute',
    width: '83@s',
    height: '25@s',
    alignItems: 'center',
    justifyContent: 'center'
  },
});

const mapStateToProps = (state) => ({
  setClicked: state.chatActions.setClicked,
  clickedProf: state.chatActions.clickedProf,
  userProfile: state.chatActions.lastClickedUserProfile
})

const mapDispatchToProps =  (dispatch) => ({
setClick: (params) => 
  dispatch({ type: ChatConstants.SET_CLICKED, params: params}),
clickedUserProfile: (params) => 
  dispatch({ type: ChatConstants.CLICKED, params: params}),
setUserProfile: (params) =>
  dispatch({ type: ChatConstants.SET_LAST_CLICKED_USER_PROFILE, params: params})
})

export default connect(mapStateToProps, mapDispatchToProps) (UserProfile)