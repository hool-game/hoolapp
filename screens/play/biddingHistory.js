import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import SwipeableViews from 'react-swipeable-views-native';

import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { SCREENS } from '../../constants/screens';
import { scale, verticalScale, ScaledSheet, moderateScale, moderateVerticalScale } from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import DeviceInfo, { isTablet } from 'react-native-device-info';
import { formatSharedValue } from '../../shared/util';
import CrownHost from '../../shared/CrownHost';
import Directions from '../../shared/Directions';

var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height;
var totalCardWidth = (Platform.OS == 'ios')? ScreenWidth - verticalScale(120):ScreenWidth - scale(90)
var carMarginWidth = ScreenWidth/54
const SIDES = new Map([
  ['N', 'North'],
  ['E', 'East'],
  ['S', 'South'],
  ['W', 'West']
]);
export default class History extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isTablet: DeviceInfo.isTablet(),
      mySeat: 'S',
      myLHOSeat: 'W',
      myPartnerSeat: 'N',
      myRHOSeat: 'E',
      index: 0,
      updatedTricksValues: {'E':[], 'N': [], 'S':[], 'W':[]},
      emptyCardPlaceHolder: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
      emptyBiddingPlaceHolder: [1, 2, 3, 4, 5]
    };
  }

  componentDidMount() {
    console.log("bidding history mounted")
    this.loadInitialScreenValues();
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.tableID !== prevProps.tableID) {
      this.updateStateValues({ tableID: this.props.tableID });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
    if ( this.props.chatScreen !== prevProps.chatScreen) {
      this.updateStateValues({ chatScreen: this.props.chatScreen})
    }
    if (this.props.northAvtar !== prevProps.northAvtar) {
      this.updateStateValues({ northAvtar: this.props.northAvtar });
    }
    if (this.props.eastAvtar !== prevProps.eastAvtar) {
      this.updateStateValues({ eastAvtar: this.props.eastAvtar });
    }
    if (this.props.southAvtar !== prevProps.southAvtar) {
      this.updateStateValues({ southAvtar: this.props.southAvtar });
    }
    if (this.props.westAvtar !== prevProps.westAvtar) {
      this.updateStateValues({ westAvtar: this.props.westAvtar });
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.updateStateValues({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.updateStateValues({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.updateStateValues({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.updateStateValues({ westUser: this.props.westUser });
    }
    if (this.props.iskibitzer !== prevProps.iskibitzer) {
      this.updateStateValues({ iskibitzer: this.props.iskibitzer });
    }
    if (this.props.infoScreen !== prevProps.infoScreen) {
      this.updateStateValues({ infoScreen: this.props.infoScreen });
    }
    if (this.props.biddings !== prevProps.biddings) {
      this.updateStateValues({ biddings: this.props.biddings });
    }
    if (this.props.lastBidRound !== prevProps.lastBidRound) {
      this.updateStateValues({ lastBidRound: this.props.lastBidRound });
    }
    if (this.props.tricks !== prevProps.tricks) {
      this.updateStateValues({ tricks: this.props.tricks });
    }
    if (this.props.initialPlayer !== prevProps.initialPlayer) {
      this.updateStateValues({ initialPlayer: this.props.initialPlayer });
    }
    if (this.props.arrangedSides !== prevProps.arrangedSides) {
      this.updateStateValues({ arrangedSides: this.props.arrangedSides });
    }
    if (this.props.currentScreen !== prevProps.currentScreen) {
      this.updateStateValues({ currentScreen: this.props.currentScreen });
      if (this.props.currentScreen === SCREENS.SHARE_INFO){
        this.setState({ index: 0 });
      }else if(this.props.currentScreen === SCREENS.BID_INFO){
        this.setState({ index: 1 });
      }else if(this.props.currentScreen === SCREENS.PLAY_SCREEN){
        this.setState({ index: 2 });
      }else{
        this.setState({ index: 0 });
      }
    }
  
  }

  componentWillUnmount() {
    console.log('history closed');
  }

  loadInitialScreenValues() {
    this.setState({ ...this.props, isValueInitialized: true }, () => {
      let sideKeys = [...SIDES.keys()];
      let indexOfMySeat = sideKeys.indexOf(this.state.gamestate.mySeat);
      console.log(indexOfMySeat);
      let remainingSeat = 4 - (indexOfMySeat + 1);
      let splicedArray = sideKeys.splice(indexOfMySeat + 1, remainingSeat);
      console.log('spliced', splicedArray);
      let arrangedSides = [...splicedArray, ...sideKeys];
      console.log('arrangedSides', arrangedSides);
      let number = ["Nu"]
      arrangedSides = [...number, ...arrangedSides];
      if (
        this.state.currentScreen === SCREENS.SHARE_INFO
      ){
        this.setState({ index: 0 });
      }else if(this.state.currentScreen === SCREENS.BID_INFO){
        this.setState({ index: 1 });
      }else if(this.props.currentScreen === SCREENS.PLAY_SCREEN){
        this.setState({ index: 2 });
      }else{
        this.setState({ index: 0 });
      }
      this.props.updateDeskStateValues({ arrangedSides }, () => {});
    });
  }

  updateStateValues(stateObj, callback) {
    this.setState(stateObj, callback);
  }

  getChatMessage(seat) {
    if( seat === 'N'){
      if(this.state.chatScreen.isNorthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'S'){
      if(this.state.chatScreen.isSouthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'W'){
      if(this.state.chatScreen.isWestSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'E'){
      if(this.state.chatScreen.isEastSentMsg){
         return false
      }else{
         return true
      }
    }else{
      return true
    }
  }

  getBGStyleByBid(bid) {
    if (bid.type == 'pass') 
      return styles.biddingHistoryBodyRowPassBg;
    else if (bid.type == 'level') {
      if (bid.suit == 'S')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowSpadeCardBgSolid
          : styles.biddingHistoryBodyRowSpadeCardBg;
      else if (bid.suit == 'D')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowDiamondCardBgSolid
          : styles.biddingHistoryBodyRowDiamondCardBg;
      else if (bid.suit == 'H')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowHeartCardBgSolid
          : styles.biddingHistoryBodyRowHeartCardBg;
      else if (bid.suit == 'C')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddindHistoryBodyRowClubsCardBgSolid
          : styles.biddindHistoryBodyRowClubsCardBg;
      else if (bid.suit == 'NT')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowNtCardBgSolid
          : styles.biddingHistoryBodyRowNtCardBg;
    } else if (bid.type == 'double') {
      return bid.force_won
        ? styles.biddingHistoryBodyRowRXXCardBgSolid
        : styles.biddingHistoryBodyRowRXXCardBgBdr;
    } else if (bid.type == 'redouble') {
      return styles.biddingHistoryBodyRowRXXCardBgSolid;
    } else {
      return styles.biddingHistoryBodyRowBlankBg;
    }
  }
  getTextBGStyleByBid(bid) {
    if (bid.type == 'pass') return styles.biddingHistoryBodyRowPassCardBgText;
    else if (bid.type == 'level') {
      if (bid.suit == 'S')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowSpadeCardBgText;
      else if (bid.suit == 'D')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowDiamondCardBgText;
      else if (bid.suit == 'H')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowHeartsCardBgText;
      else if (bid.suit == 'C')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowClubsCardBgText;
      else if (bid.suit == 'NT')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowNtCardBgText;
    } else if (bid.type == 'double') {
      return bid.force_won
        ? styles.biddingHistoryBlackText
        : styles.biddingHistoryTextWhite;
    } else if (bid.type == 'redouble') {
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? styles.biddingHistoryBlackText
        : styles.biddingHistoryTextWhite;
    }
  }
  getSuitSVG(bid) {
    if (bid.suit == 'S')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.spade_black
        : svgImages.spade;
    else if (bid.suit == 'D')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.diamond_black
        : svgImages.diamond;
    else if (bid.suit == 'H')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.hearts_black
        : svgImages.hearts;
    else if (bid.suit == 'C')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.clubs_black
        : svgImages.clubs;
    else if (bid.suit == 'NT')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.nt_black
        : svgImages.nt_yellow;
  }
  getBidText(bid) {
    if (bid.type == 'pass') return 'P';
    else if (bid.type == 'level') {
      return bid.level;
    } else if (bid.type == 'double') {
      return 'x2';
    } else if (bid.type == 'redouble') {
      return 'x4';
    }
  }
  
  getCircleView(bid) {
    return (
      <View
        style={[
          this.getBGStyleByBid(bid),
          { width: scale(28), height: scale(28),
          }
        ]}
      >
        <Text style={this.getTextBGStyleByBid(bid)}>
          {this.getBidText(bid)}
        </Text>
        {bid.suit && (
          <SvgXml
            style={{...styles.biddingCardSize, maxWidth:scale(8), maxHeight: scale(8)}}
            xml={this.getSuitSVG(bid)}
          />
        )}
      </View>
    );
  }

  getCardBgStyle(card) {
    if (card.suit == 'S') return styles.playHistoryBodyRowSpadeCardBg;
    else if (card.suit == 'D') return styles.playHistoryBodyRowDiamondCardBg;
    else if (card.suit == 'H') return styles.playHistoryBodyRowHeartCardBg;
    else if (card.suit == 'C') return styles.playHistoryBodyRowClubsCardBg;
  }

  getSVGByCard(card) {
    if (card.suit == 'S') return svgImages.spade_black;
    else if (card.suit == 'D') return svgImages.diamond_black;
    else if (card.suit == 'H') return svgImages.hearts_black;
    else if (card.suit == 'C') return svgImages.clubs_black;
  }

  getEmptyCardView(idx, side) {
    return (
      side == 'Nu' ?
      <View
        key={'card' + side + idx}
        style={{ flexDirection: 'row', alignItems: 'center', marginTop: moderateScale(15), marginBottom:moderateScale(15) }}
      >
        {
          idx == 1 &&
          <View style={{width:(carMarginWidth)}} />
        }
          <View
            style={[
              {
                width: scale(24),
              }
            ]}
          >
            <Text style={{...styles.playHistoryNmbText, textAlign: 'center'}}>{idx}</Text>
          </View>
          <View
            style={[
              { width: (carMarginWidth), height: scale(10)}
            ]}
          ></View>
        </View>
        :
        <View style={{
            flexDirection: 'row',
          }}>
            <View
              style={
                this.state.isTablet && Platform.OS == 'ios' ?
                {width: carMarginWidth, height: scale(35), marginBottom: moderateScale(15)}
                :
                {width: carMarginWidth, height: scale(35), marginBottom: moderateScale(15)}
              }
            />
            <SvgXml width={scale(24)} height={scale(35)} xml={svgImages.emptyCard} style={{}} />
        </View>)
  }

  getCardView(card, side, idx) {
    return (
      side != 'Nu' &&
      <View
        key={'card' + side + idx}
        style={{ flexDirection: 'row', alignItems: 'center',}}
      >
        {card.initialCard ? (
          card.startingCard ? (
              <SvgXml width={carMarginWidth} height={scale(35)} xml={svgImages.winner_dealer_line} style={{ 
                marginBottom: carMarginWidth
              }}/>
          ) : (
              <View width={carMarginWidth} height={scale(35)} />
          )
        ) : null}
        {
          card.suit === 'E' ?
          <SvgXml width={scale(24)} height={scale(35)} xml={svgImages.emptyCard} style={{marginBottom: moderateScale(15)}}/>
          :
            <View
              style={[
                this.getCardBgStyle(card),
                {
                  width: scale(24),
                  height: scale(35),
                  paddingBottom: moderateScale(4),
                  marginBottom: moderateScale(15)
                }
              ]}
            >
              <Text style={styles.playHistoryTotalCardNumberText}>{card.rank}</Text>
              <SvgXml width={scale(10)} height={scale(10)} xml={this.getSVGByCard(card)} />
            </View>
        }
        {card.won ? (
          <View
            style={[
              { width: carMarginWidth, height: scale(35),paddingHorizontal:1, marginBottom: carMarginWidth, alignItems: 'center', justifyContent: 'center'}
            ]}
          >
            <SvgXml xml={svgImages.winner_dealer_line} style={{justifyContent: 'center'}}/>
          </View>
        ) : (
          <View
            style={[
              { width: carMarginWidth, height: scale(35),  }
            ]}
          ></View>
        )}
      </View>
    );
  }

  getSideRow(side) {
    return (
      <View
        style={{...
          styles.playHistoryBodyRowCardTotalArea,
          width: totalCardWidth,
          }
        }
      >
        <View
          style={
            (this.state.isTablet && Platform.OS == 'ios')?
            {...
              styles.playHistoryBodyRowCardDvFstBox,
                width: verticalScale(20),
                height: scale(22),
                alignItems: 'center',
                marginLeft: moderateVerticalScale(20),
                marginTop: moderateScale(6)
            }:
            {...
            styles.playHistoryBodyRowCardDvFstBox,
              width: scale(40),
              height: scale(22),
              alignItems: 'center',
              marginLeft: moderateVerticalScale(20),
              marginTop: moderateScale(6)
            }
          }
        >
          <Text style={
              {
                ...styles.playHistoryDirectionText,
                color: this.getColor(side),
                textAlign: 'center',
              }}>
            {side != 'Nu' && side}
          </Text>
        </View>
          {(this.state.tricks[side] != undefined &&
            this.state.tricks[side].length != 0) &&
            this.setEmptyCardToList(side)
          }
          {this.state.tricks[side] != undefined &&
            this.state.tricks[side].length != 0
            ? this.state.tricks[side].map((card, idx) => {
              return this.getCardView(card, side, idx);
            })
            : this.state.emptyCardPlaceHolder.map((idx) => {
                {return this.getEmptyCardView(idx, side)}
            })}
      </View>
    );
  }

  handleChangeIndex = index => {
    this.setState({
      index
    });
  };

  setEmptyCardToList(side){
    this.state.updatedTricksValues[side] = this.state.tricks[side].filter(data => data.suit != 'E')
    var updateBidding = 13 - (this.state.updatedTricksValues[side].length)
    if(this.state.updatedTricksValues[side].length < 13){
      for (let index = 0; index < updateBidding; index++) {
        this.state.updatedTricksValues[side].push(
          {"initialCard": "", "rank": "", "side": "", "startingCard": "", "suit": "E"}
        )
      }
      this.state.tricks[side] = this.state.updatedTricksValues[side]
    }
  }

  setEmptyToList(side){
    if(side != 'Nu'){
      var updateBidding = 5 - this.state.biddings[side].length
      if(this.state.biddings[side].length < 5){
        for (let index = 0; index < updateBidding; index++) {
          this.state.biddings[side].push({})
        }
      }
    }
  }

  getColor = (side) =>{
    if (this.props.currentScreen == SCREENS.SHARE_INFO) {
      if (side == "W") {
        return this.props.infoScreen.DirectionTextLHO
      } else if (side == "E") {
        return this.props.infoScreen.DirectionTextRHO
      } else if (side == "N") {
        return this.props.infoScreen.DirectionTextPartner
      } else if (side == "S") {
        return this.props.infoScreen.DirectionTextMySeat
      }
    }else if(this.props.currentScreen == SCREENS.BID_INFO){
      if(side == 'N'){
        return this.props.bidInfoScreen.DirectionTextPartner
      }else if(side == 'S'){
        return this.props.bidInfoScreen.DirectionTextMySeat
      }else if(side == 'E'){
        return this.props.bidInfoScreen.DirectionTextRHO
      }else if(side == 'W'){
        return this.props.bidInfoScreen.DirectionTextLHO
      }
    }else{
      if(side == 'N'){
        return this.props.playScreen.DirectionTextPartner.color
      }else if(side == 'S'){
        return this.props.playScreen.DirectionTextMySeat.color
      }else if(side == 'E'){
        return this.props.playScreen.DirectionTextRHO.color
      }else if(side == 'W'){
        return this.props.playScreen.DirectionTextLHO.color
      }
    }
    
  }
  
  render() {
    if(this.state.isTablet && Platform.OS == 'ios'){
      carMarginWidth = carMarginWidth - 3
    }
    console.log("the bidding historu ", this.state.isValueInitialized)
    return this.state.isValueInitialized ? (
      <>
        <SwipeableViews
          index={this.state.index}
          onSwitching={(index, type) => {
            if(type == 'end' && index ==2){
              this.handleChangeIndex(0)
            }else if(type == 'end' && index == 0){
              this.handleChangeIndex(2)
            }
          }}
          onChangeIndex={this.handleChangeIndex}
          style={styles.slideContainer}
        >
          <View style={[styles.slide]}>
            <View style={{ width: '100%', alignSelf: 'center', top: moderateVerticalScale(14),
                  height: scale(19),
                  width: scale(118), justifyContent: 'center',}}>
              <Text
                style={{
                  ...styles.biddingHistoryText,
                  marginTop: 0
                }}
              >
                Share History
              </Text>
            </View>
            <View style={{
              width: scale(346),
              alignSelf: 'center',
              top: moderateVerticalScale(20),
              bottom: moderateScale(13),
              height: scale(270)
            }}>
              <Directions
                currentScreen={this.props.currentScreen}
                bidInfoScreen={this.props.bidInfoScreen}
                playScreen={this.props.playScreen}
                infoScreen={this.props.infoScreen}
                gamestate={this.props.gamestate}
              />
               <View
                style={{
                  width: '100%',
                  position: 'absolute',
                  top: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <View style={{ flexDirection: 'column' }}>                
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', borderRightWidth: 1 }}
                  >
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.northSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(this.state.infoScreen.northSharedValue1)}
                    </Text>
                  </View>
                  <View
                    style={{ width: scale(90), height: scale(45), backgroundColor: '#151515' }}
                  >
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.northSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(this.state.infoScreen.northSharedValue2)}
                    </Text>
                  </View>
                </View>
                <View style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  alignItems:'center',
                  justifyContent:'center',
                  width:scale(180),
                  height: scale(23) ,
                }}>
                  <TouchableOpacity
                    disabled={this.getChatMessage(this.state.gamestate.myPartnerSeat)}
                    onPress={() => {
                        this.props.showTableMessages()
                    }}
                  >
                    <View style={{ height:scale(23),width:scale(180),alignSelf:'center'}}>
                      <CrownHost
                        hostName={this.props.hostName}
                        shareBidPlay={false}
                        rightSide={false}
                        textStyle={{
                          ...styles.UserNameText, color: this.state.infoScreen.northUserText, letterSpacing: 0.7,
                        }}
                        userName={this.state.gamestate.myPartnername}
                        reduceChar={this.getChatMessage(this.state.gamestate.myPartnerSeat)}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              </View>
              <View
                style={{
                  height: '100%',
                  position: 'absolute',
                  left: moderateVerticalScale(2),
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    height: scale(180)
                  }}
                >
                  <View
                    style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: 1 }}
                  >
                    <View style={{
                      height: scale(45), transform: [{ rotate: '-90deg' }],
                      alignItems: 'flex-end',
                    }}>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                        {this.state.infoScreen.westSharedType2}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                        {formatSharedValue(this.state.infoScreen.westSharedValue2)}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      width: scale(45), height: scale(90), backgroundColor: '#151515', zIndex: 1
                    }}
                  >
                    <View style={{
                      height: scale(45), transform: [{ rotate: '-90deg' }],
                      alignItems: 'flex-end',
                    }}>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                        {this.state.infoScreen.westSharedType1}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                        {formatSharedValue(this.state.infoScreen.westSharedValue1)}
                      </Text>
                    </View>
                  </View>
                </View>
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.gamestate.myLHOSeat)}
                    onPress={() => {
                        this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    height: scale(180), width: scale(23),
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column'
                  }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={true}
                      rightSide={false}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.westUserText, letterSpacing: 0.7,
                      }}
                      crownStyle={{
                        alignSelf:'center'
                      }}
                      userName={this.state.gamestate.myLHOname}
                      reduceChar={this.getChatMessage(this.state.gamestate.myLHOSeat)}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: '100%',
                  position: 'absolute',
                  right: moderateVerticalScale(2),
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.gamestate.myRHOSeat)}
                    onPress={() => {
                        this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    height: scale(180), width: verticalScale(23),
                    alignItems: 'center', justifyContent: 'center',
                    alignSelf:'center'
                  }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={true}
                      rightSide={true}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.eastUserText, letterSpacing: 0.7,
                      }}
                      crownStyle={{
                      alignSelf:'center'
                      }}
                      userName={this.state.gamestate.myRHOname}
                      reduceChar={this.getChatMessage(this.state.gamestate.myRHOSeat)}
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: 1 }}
                  >
                    <View style={{
                      height: scale(45), transform: [{ rotate: '90deg' }],
                      alignItems: 'flex-start',
                    }}>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                        {this.state.infoScreen.eastSharedType1}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                        {formatSharedValue(this.state.infoScreen.eastSharedValue1)}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', zIndex: 1 }}
                  >
                    <View style={{
                      height: scale(45), transform: [{ rotate: '90deg' }],
                      alignItems: 'flex-start',
                    }}>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                        {this.state.infoScreen.eastSharedType2}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                        : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                        {formatSharedValue(this.state.infoScreen.eastSharedValue2)}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  position: 'absolute',
                  bottom: 1,
                  alignSelf: 'center',
                }}
              >
                <View style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: scale(23),
                  width: scale(180),
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                  <TouchableOpacity
                    disabled={this.getChatMessage(this.state.gamestate.mySeat)}
                    onPress={() => {
                        this.props.showTableMessages()
                    }}
                  >
                    <View style={{ height:scale(23),width:scale(180),alignSelf:'center'}}>
                      <CrownHost
                        hostName={this.props.hostName}
                        shareBidPlay={false}
                        rightSide={false}
                        textStyle={{
                          ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,
                          alignSelf: 'center'
                        }}
                        userName={this.state.gamestate.myUsername}
                        reduceChar={this.getChatMessage(this.state.gamestate.mySeat)}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', borderRightWidth: 1 }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.southSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(this.state.infoScreen.southSharedValue1)}
                    </Text>
                  </View>
                  <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515' }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.southSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } :
                      { ...styles.PaternText, width: scale(90), height: scale(23) }}>
                      {formatSharedValue(this.state.infoScreen.southSharedValue2)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.slide]}>
            <View style={{width: '100%', alignItems: 'center'}}>
              <Text
                style={{
                  ...styles.biddingHistoryText,
                  height: scale(19),
                  width: scale(118)
                }}
              >
                Bidding History
              </Text>
            </View>
            <View
              style={this.state.isTablet? {...
                styles.biddingHistoryBodyTotal,
                  width: '100%',
                  height: scale(268),
                  justifyContent: 'space-around'
                }:{...
                styles.biddingHistoryBodyTotal,
                  width: '100%',
                  height: scale(248),

                }
              }
            >
                {this.state.biddings != undefined
                  ? [...this.state.arrangedSides].map(side => {
                    return (
                      <View
                        style={{
                          ...
                          styles.biddingHistoryBodyRowCardTotalArea,
                          paddingVertical: moderateVerticalScale(8),
                          
                        }
                        }
                      >
                        <View
                          style={[
                            styles.biddingHistoryBodyRowCardDvFstBox,
                            {
                              width: scale(76),
                              height: scale(22),
                              marginLeft: moderateVerticalScale(20),
                              
                            }
                          ]}
                        >
                          <Text style={
                            {
                              ...styles.biddingHistoryDirectionText,
                              color: this.getColor(side),
                              textAlign: 'center',
                            }}>
                            {side != 'Nu'&& side} 
                          </Text>
                        </View>
                        <View
                          style={{width: verticalScale(388), height: '100%' , flexDirection: 'row',alignItems: 'center'
                        }}
                        >
                          {this.setEmptyToList(side)}
                          {side != 'Nu'?
                            this.state.biddings[side].length != 0
                            ? this.state.biddings[side].map((bid, idx) => {
                              return (
                                <>
                                  {this.getCircleView(bid)}
                                  {idx <
                                    this.state.biddings[side].length - 1 ? (
                                    (this.state.biddings[side][idx].won ||
                                      this.state.biddings[side][idx]
                                        .force_line) &&
                                      this.state.biddings[side][idx].type ==
                                      'level' ? (
                                          <View
                                            style={{
                                              ...
                                              styles.biddingHistoryBodyRowNxtLine,
                                              width: moderateVerticalScale(66),
                                              height: scale(28),
                                              alignItems: 'center',
                                              justifyContent: 'center',
                                            }
                                            }
                                          >
                                            <SvgXml
                                              width={moderateVerticalScale(66)}
                                              height={scale(4)}
                                              xml={svgImages.bidding_line}
                                            />
                                          </View>
                                    ) :  <View
                                    style={{...
                                        styles.biddingHistoryBodyRowNxtLine,
                                          width: moderateVerticalScale(66),
                                          height: scale(35),
                                        }
                                    }
                                    >
                                    </View>) : 
                                    null}
                                </>
                              );
                            })
                            : this.state.emptyBiddingPlaceHolder.map((idx) => {
                              return(
                                <View
                                  style={{
                                    height: '100%', flexDirection: 'row',
                                    marginRight: moderateVerticalScale(66),
                                  }}
                                >
                                  <SvgXml xml={svgImages.emptyBidding} width={scale(28)} height={scale(28)}
                                  />
                                </View>
                              )
                            })    
                            :(
                              this.state.emptyBiddingPlaceHolder.map((idx) => {
                               return(
                                <View
                                style={{
                                  flexDirection: 'row',
                                  marginRight: moderateVerticalScale(66),
                                  width: scale(28),
                                  height: scale(28),
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                }}
                              >
                                <Text style={{...styles.biddingHistoryNmbText, textAlign: 'center'}}>{idx}</Text>
                              </View>
                              )               
                            }))
                          }
                        </View>
                      </View>
                    );
                  })
                  : null}
            </View>
          </View>
          <View style={[styles.slide]}>
            <View style={{ width: '100%', alignItems: 'center'}}>
              <Text
                style={{
                  ...styles.playHistoryText,
                  height: scale(19),
                  width: scale(118),
                }}
              >
                Play History
              </Text>
            </View>
            <View
              style={this.state.isTablet ? {...
                styles.playHistoryBody,
                  width: '100%',
                  marginTop: moderateVerticalScale(50), 
                }:{...
                  styles.playHistoryBody,
                    width: '100%',
                    marginTop: moderateScale(28), 
                  }
              }
            >
              {this.state.tricks != undefined
                ? [...this.state.arrangedSides].map(side => {
                    return this.getSideRow(side);
                  })
                : null}
            </View>
          </View>
        </SwipeableViews>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            left: moderateScale(20),
            height: scale(5),
            bottom: moderateVerticalScale(30)
          }}>
          <View style={[styles.biddingHistoryBodySliderIcons]}>
            <TouchableOpacity onPress={() => this.setState({ index: 0 })}>
              <SvgXml
                width={scale(15)}
                xml={
                  this.state.index === 0
                    ? svgImages.play_history_slider_icon1
                    : svgImages.play_history_slider_icon2
                }
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ index: 1 })}>
              <SvgXml
                width={scale(15)}
                xml={
                  this.state.index === 1
                    ? svgImages.play_history_slider_icon1
                    : svgImages.play_history_slider_icon2
                }
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ index: 2 })}>
              <SvgXml
                width={scale(15)}
                xml={
                  this.state.index === 2
                    ? svgImages.play_history_slider_icon1
                    : svgImages.play_history_slider_icon2
                }
              />
            </TouchableOpacity>
          </View>
        </View>
      </>
    ) : null;
  }
}

const styles = ScaledSheet.create({
  slideContainer: {
    width: '100%',
    height: '100%'
  },
  slide: {
    width: '100%',
    height: '100%',
    overflow: 'hidden'
  },
  biddingHistoryTextWhite: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#FFFFFF'
  },
  biddingHistoryBodyRowRXXCardBgBdr: {
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowRXXCardBgSolid: {
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBlackText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#0a0a0a'
  },
  biddingHistoryBodyRowHeartCardBgSolid: {
    alignItems: 'center',
    borderColor: '#FF0C3E',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#FF0C3E',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowNtCardBgSolid: {
    alignItems: 'center',
    borderColor: '#FFE81D',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#FFE81D',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowNtCardBg: {
    alignItems: 'center',
    borderColor: '#FFE81D',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowNtCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#FFE81D'
  },
  biddingHistoryBodyRowPassCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#B9B9B9'
  },
  biddingHistoryBodyRowPassBg: {
    alignItems: 'center',
    borderColor: '#B9B9B9',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingCardSize: {
    marginLeft: 1
  },
  biddingHistoryBodyRowClubsCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#79E62B'
  },
  biddingHistoryBodyRowHeartsCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#FF0C3E'
  },
  biddingHistoryBodyRowDiamondCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#04AEFF'
  },
  biddingHistoryBodyRowSpadeCardBgText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '300',
    color: '#C000FF'
  },
  biddingHistoryBodyRowNumberDveWidth: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  biddingHistoryBodyRowBlankBg: {
    alignItems: 'center',
    borderColor: '#151515',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#151515',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowSpadeCardBgSolid: {
    alignItems: 'center',
    borderColor: '#C000FF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#C000FF',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowSpadeCardBg: {
    alignItems: 'center',
    borderColor: '#C000FF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddindHistoryBodyRowClubsCardBg: {
    alignItems: 'center',
    borderColor: '#79E62B',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddindHistoryBodyRowClubsCardBgSolid: {
    alignItems: 'center',
    borderColor: '#79E62B',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#79E62B',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowDiamondCardBg: {
    alignItems: 'center',
    borderColor: '#04AEFF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowDiamondCardBgSolid: {
    alignItems: 'center',
    borderColor: '#04AEFF',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#04AEFF',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowHeartCardBg: {
    alignItems: 'center',
    borderColor: '#FF0C3E',
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#0a0a0a',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowNxtLine: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  biddingHistoryBodyTotal: {
    flexDirection: 'column',
    marginTop: '24@ms',
  },
  biddingHistoryBodyRowCardTotalArea: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  biddingHistoryBodyRowCardDvFstBox: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  biddingHistoryBodyRowNumberTotalArea: {
    flexDirection: 'row',
  },
  biddingHistoryBodyRowNumberDve: {
    flexDirection: 'row',
  },
  biddingHistoryText: {
    textAlign: 'center',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    marginTop: '14@ms',
    fontWeight: '300',
    color: '#6D6D6D'
  },
  biddingHistoryNmbText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(9),
    fontWeight: '300',
    color: '#6D6D6D'
  },
  biddingHistoryDirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#6D6D6D',
  },
  biddingHistoryBodySliderIcons: {
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  /******************************************** */

  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#C000FF'
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#79E62B'
  },
  bidPlayText: {
    top: '5@s',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D',
    fontWeight: 'normal'
  },
  bidPlayTextBlack: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#0a0a0a',
    fontWeight: 'normal'
  },

  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D'
  },
  ContractText: {
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    top: '8@s'
  },
  ContractTextBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#0a0a0a'
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingRight: '4@s'
  },
  ShareText: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    fontWeight: 'normal',
    color: '#6D6D6D',
    paddingTop: '10@s'
  },
  playHistoryBodySliderIcons: {
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  playHistoryTotalCardNumberText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#0a0a0a'
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },
  playHistoryBodyRowClubsCardBg: {
    alignItems: 'center',
    borderColor: '#0a0a0a',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#79E62B',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  playHistoryBodyRowDiamondCardBg: {
    alignItems: 'center',
    borderColor: '#0a0a0a',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#04AEFF',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  playHistoryBodyRowHeartCardBg: {
    alignItems: 'center',
    borderColor: '#0a0a0a',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#FF0C3E',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  playHistoryBodyRowSpadeCardBg: {
    alignItems: 'center',
    borderColor: '#0a0a0a',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#C000FF',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  playHistoryBodyRowBlankCardTotalAreaBgAss: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#151515',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#151515'
  },
  playHistoryDirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#6D6D6D'
  },
  playHistoryBodyRowCardDvFstBox: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  playHistoryBodyRowCardTotalArea: {
    flexDirection: 'row'
  },
  playHistoryNmbText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(9),
    fontWeight: '300',
    color: '#6D6D6D',
  },
  playHistoryBodyRowNumberDveWidth: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  playHistoryBodyRowNumberDve: {
    flexDirection: 'row',
  },
  playHistoryBodyRowNumberDvf: {
    position: 'relative'
  },
  playHistoryBodyRowNumberTotalArea: {
    flexDirection: 'row',
  },
  playHistoryBody: {
    flexDirection: 'column',
  },
  playHistoryText: {
    fontFamily: 'Roboto-Light',
    marginTop: '14@ms',
    textAlign: 'center',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#6D6D6D'
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
   // textTransform: 'capitalize',
    textAlign: 'center',
    paddingTop: moderateScale(3)
  },
  UserNameText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#676767',
    textAlign: 'center',
    paddingTop: 3
  },
  LHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  RHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  Partner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center',
    letterSpacing: normalize(1.92)
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: 4
  },
});
