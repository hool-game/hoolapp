import React,{Component} from 'react';
import {View, Text, StyleSheet, LogBox, Image, Dimensions,Alert} from 'react-native';
import { scale } from 'react-native-size-matters/extend';
import { HOOL_EVENTS } from '../../constants/hoolEvents';
import { HOOL_CLIENT } from '../../shared/util';
import { normalize } from '../../styles/global';



    var ScreenWidth=  Dimensions.get("window").width;
    var ScreenHeight=  Dimensions.get("window").height;//-30;

    var HeightBoxTop=(225/360*ScreenHeight);
    var MenuBodyWidth=(91/360*ScreenHeight);
    var ShareHehightWidth=(90/360*ScreenHeight);
    var ContractTextHeight=(22/360*ScreenHeight);
    var ContractDwnBoxHeight=(23/360*ScreenHeight);
    var ContractDwnMdlBoxWidth=(44/360*ScreenHeight);
    var TotalCardWidth=(532/360*ScreenHeight);
    var HeightEastWestPalyCard=(360/360*ScreenHeight);
    var EastWestPalyCardArea=(304/360*ScreenHeight);
    var NorthCardWithKibitzerArea=(400/640*ScreenWidth);

    var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
    var HeightWidthBoxes=(45/360*ScreenHeight);
    
    var SampleNameArray = [ "K", "Q", "3", "K", "8", "4", "A", "9", "7", "6", "Q", "J", "7" ];
    var hands = [
        {"rank": "K","img": require('../../assets/images/spade.png'),"suit":"spade","key":"K_spade","SuiteColor":"#C000FF"},
        {"rank": "Q","img": require('../../assets/images/spade.png'),"suit":"spade","key":"Q_spade","SuiteColor":"#C000FF"},
        {"rank": "10","img": require('../../assets/images/spade.png'),"suit":"spade","key":"10_spade","SuiteColor":"#C000FF"},
        {"rank": "K","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"K_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "8","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"8_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "4","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"4_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "A","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"A_diamond","SuiteColor":"#04AEFF"},
        {"rank": "9","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"9_diamond","SuiteColor":"#04AEFF"},
        {"rank": "7","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"7_diamond","SuiteColor":"#04AEFF"},
        {"rank": "6","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"6_diamond","SuiteColor":"#04AEFF"},
        {"rank": "Q","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"Q_clubs","SuiteColor":"#79E62B"},
        {"rank": "J","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"J_clubs","SuiteColor":"#79E62B"},
        {"rank": "7","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"7_clubs","SuiteColor":"#79E62B"}
      ] 
      var eastHands=hands;
    var westHands=hands;
    var northHands=hands;
    var southHands=hands;
    
      export default class Bidding extends React.Component {
        constructor(props) {
            ///navigation.na
          super(props);
          this.state = {              
            NSStyle:styles.ContractTextActive,
            NSBIDStyle: styles.BidTextActive,
            EWStyle:styles.ContractTextActive,
            EWBIDStyle: styles.BidTextActive,
          }; 
          LogBox.ignoreAllLogs();    
        }

        InitializeState(){
            this.setState ({
                output: '',
                tableID:'',
                mySeat: '',
                myLHOSeat: '', 
                myRHOSeat: '',
                myPartnerSeat: '',
                northUser: '',
                eastUser: '',
                southUser: '',
                westUser: '', 
                northSharedType1: '',
                northSharedValue1 :'',
                northSharedType2: '',
                northSharedValue2 :'',
                eastSharedType1: '',
                eastSharedValue1 :'',
                eastSharedType2: '',
                eastSharedValue2 :'',
                southSharedType1: '',
                southSharedValue1 :'',
                southSharedType2: '',
                southSharedValue2 :'',
                westSharedType1: '',
                westSharedValue1 :'',
                westSharedType2: '',
                westSharedValue2 :'', 
                cardHCPTextColor :'#FFFFFF',
                cardPatternTextColor :'#FFFFFF',
                cardSuitTextColor :'#FFFFFF',
                DirectionTextPartner:'#6D6D6D',
                DirectionTextLHO:'#6D6D6D',
                DirectionTextRHO:'#6D6D6D',
                DirectionTextMySeat:'#6D6D6D',
                northUserText: '#6D6D6D',
                eastUserText: '#6D6D6D',
                southUserText: '#6D6D6D',
                westUserText: '#6D6D6D',
                showBiddingBox:false,
                Trick_Pass_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_1_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_2_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_3_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_7_bg:styles.bidBoxWidthHeightAreaActive,
                Trick_Pass_Text:styles.bidBoxWidthHeightTextClor,
                Trick_1_Text:styles.bidBoxWidthHeightTextClor,
                Trick_2_Text:styles.bidBoxWidthHeightTextClor,
                Trick_3_Text:styles.bidBoxWidthHeightTextClor,
                Trick_4_Text:styles.bidBoxWidthHeightTextClor,
                Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                Double_bg:styles.bidBoxWidthHeightArea,
                Redouble_bg:styles.bidBoxWidthHeightArea,
                Double_Text:styles.bidDblTextClordisabled,
                Redouble_Text:styles.bidRedblTextClordisabled,
                Suit_Club_bg:styles.bidBoxWidthHeightArea,
                Suit_Diamond_bg:styles.bidBoxWidthHeightArea,
                Suit_Heart_bg:styles.bidBoxWidthHeightArea,
                Suit_Spade_bg:styles.bidBoxWidthHeightArea,
                NT_bg:styles.bidBoxWidthHeightArea,
                NT_Text:styles.bidBoxDisabledLevel,
                Suit_Club:require('../../assets/images/clubs_black.png'),
                Suit_Diamond:require('../../assets/images/diamond_black.png'),
                Suit_Heart:require('../../assets/images/hearts_black.png'),
                Suit_Spade:require('../../assets/images/spade_black.png'),
                Make_Bid:styles.bidBoxWidthHeightArea,
                TricksSelected:'',
                TrumpSelected:'',
                EnableTrump_Club:true,
                EnableTrump_Diamond:true,
                EnableTrump_Heart:true,
                EnableTrump_Spade:true,
                EnableTrump_NT:true,
                EnableDouble:true,
                EnableRedouble:true,
                EnableBid:true,
                bid_type:'',
                bid_level:'',
                bid_suit:'',
                my_bid_level:'',
                my_bid_suit:'',
                my_bid_suit_color:styles.bidCircleTextAss, 
                my_bid_suit_text_color:'',
                my_bid_suit_image:'',
                my_bid_suit_border_color:styles.bidCirclePositionBodrAss,
                my_partner_bid_level:'',
                my_partner_bid_suit:'',
                my_partner_bid_suit_color:styles.bidCircleTextAssActive, 
                my_partner_bid_suit_text_color:'',
                my_partner_bid_suit_image:'',
                my_partner_bid_suit_border_color:styles.bidCirclePositionBodrAss,
                my_LHO_bid_level:'',
                my_LHO_bid_suit:'',
                my_LHO_bid_suit_color:styles.bidCircleTextAssActive, 
                my_LHO_bid_suit_text_color:'',
                my_LHO_bid_suit_image:'',
                my_LHO_bid_suit_border_color:styles.bidCirclePositionBodrAss,
                my_RHO_bid_level:'',
                my_RHO_bid_suit:'',
                my_RHO_bid_suit_color:styles.bidCircleTextAssActive, 
                my_RHO_bid_suit_text_color:'',
                my_RHO_bid_suit_image:'',
                my_RHO_bid_suit_border_color:styles.bidCirclePositionBodrAss,
                winnerSide:'',
                winnerLevel:'',
                winnerSuit:'',
                winnerSuitImage:'',
                winnerTextColor:'',
                winnerdblRedbl:'',
                Trick_Pass_Disabled:true,
                Trick_1_Disabled:true,
                Trick_2_Disabled:true,
                Trick_3_Disabled:true,
                Trick_4_Disabled:true,
                Trick_5_Disabled:true,
                Trick_6_Disabled:true,
                Trick_7_Disabled:true,
                bid_raised_cnt:1,
            });
        }
    
        async getTableInfo() {
            const { navigation } = this.props;
            const hool = navigation.getParam('xmppconn'); 
            const tableID = navigation.getParam('tableID');
            const gamestate = navigation.getParam('gameState'); 
            console.log(' gamestate as below');
            console.log(gamestate);
            let myHand = gamestate.hands;
            //console.log(myHand); 
            myHand.forEach((h,key) => {
                console.log('Side :' + key);
                if(key==='S'){
                    southHands= this.displayCards(h.cards);
                }
                else if(key==='W'){
                    westHands= this.displayCards(h.cards);
                }
                else if(key==='N'){
                    northHands= this.displayCards(h.cards);
                }
                else if(key==='E'){
                    eastHands= this.displayCards(h.cards);
                }
            }); 
            
            
          }
        componentWillUnmount()
        {
            this.InitializeState();
            console.log('bidding closed');
        }  
    
        componentDidMount() {
            this.InitializeState();
          const { navigation } = this.props;
          const hool = navigation.getParam('xmppconn');
          const gamestate =navigation.getParam('gameState'); 
          //console.log(gamestate.side); 
          if(gamestate.contractinfo !== undefined){ 
                var bid_suit_image='';
                var bid_suit_color='';
                var bid_suit_text_color='';
                var bid_suit_border_color='';
                var bid_winnerTextColor='';
                var bid_level=gamestate.contractinfo.level;
                if(gamestate.contractinfo.suit==='C')
                {
                    bid_suit_image=require('../../assets/images/clubs.png');
                    bid_suit_color=styles.clubsImageActive;
                    bid_suit_text_color=styles.bidCircleTextClubsActive;
                    bid_suit_border_color=styles.bidCirclePositionClubsActive;
                    bid_winnerTextColor='#79E62B';
                }
                else if(gamestate.contractinfo.suit==='D')
                {
                    bid_suit_image=require('../../assets/images/diamond.png');
                    bid_suit_color=styles.diamondImageActive;
                    bid_suit_text_color=styles.bidCircleTextDiamondActive;
                    bid_suit_border_color=styles.bidCirclePositionDiamondActive;
                    bid_winnerTextColor='#04AEFF';
                }
                else if(gamestate.contractinfo.suit==='H')
                {
                    bid_suit_image=require('../../assets/images/hearts.png');
                    bid_suit_color=styles.heartsImageActive;
                    bid_suit_text_color=styles.bidCircleTextHeartsActive;
                    bid_suit_border_color=styles.bidCirclePositionHeartsActive;
                    bid_winnerTextColor='#FF0C3E';
                }
                else if(gamestate.contractinfo.suit==='S')
                {
                    bid_suit_image=require('../../assets/images/spade.png');
                    bid_suit_color=styles.spadeImageActive;
                    bid_suit_text_color=styles.bidCircleTextSpadeActive;
                    bid_suit_border_color=styles.bidCirclePositionSpadeActive;
                    bid_winnerTextColor='#C000FF';
                }
                else if(gamestate.contractinfo.suit==='NT')
                {
                    bid_suit_image='';
                    bid_suit_color=styles.bidCircleTextAssActive;
                    bid_suit_text_color=styles.bidCircleTextNTActive;
                    bid_suit_border_color=styles.bidCirclePositionNTActive;
                    bid_level=gamestate.contractinfo.level+'NT';
                    bid_winnerTextColor='#FFE81D';
                } 
                             
                this.setState({
                    winnerLevel:bid_level,
                    winnerSide:gamestate.contractinfo.declarer,
                    winnerSuit:gamestate.contractinfo.suit,
                    winnerSuitImage:bid_suit_image, 
                    winnerTextColor:bid_winnerTextColor,
                }); 
                if(gamestate.contractinfo.double==='true')
                {
                    this.setState({winnerdblRedbl:'x2'});
                } 
                else if(gamestate.contractinfo.redouble==='true')
                {
                    this.setState({winnerdblRedbl:'x4'});
                } 
        }
          this.getTableInfo();
           
           
            this.setState({mySeat:'S',myLHOSeat:'W',myPartnerSeat:'N',myRHOSeat:'E'});
            this.setState({southUser:gamestate.southUser,westUser:gamestate.westUser,northUser:gamestate.northUser,eastUser:gamestate.eastUser})
            this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00',DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00'});
         
            HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_JOIN_ERROR, (e) => {
            Alert.alert(`Failed to join table: ${e.tag || 'cancel'} (${suitType || 'unknown-error'}): ${e.text || 'unknown error'}`)
          });
    
          HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_LEAVE, (e) => {
            console.info(`${e.user} has (temporarily) disconnected from ${e.table}`)
          });
    
          HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_EXIT, (e) => {
            console.info(`${e.user} has (permanently) left ${e.table}`)
          });
    
            HOOL_CLIENT.addListener(HOOL_EVENTS.BID, (bid) => {
                 var bid_suit_image='';
                 var bid_suit_color='';
                 var bid_suit_text_color='';
                 var bid_suit_border_color='';
                 var bid_winnerTextColor='';
                 var bid_level='';
                 var wonString='';
                 console.log('bid');
                 console.log(bid);
                    if(!!bid.type) {
                        wonString = bid.won ? ' and won' : ''
                        if (bid.type == 'level') {
                          console.info(`${bid.side} has bid: ${bid.level}${bid.suit}${wonString}`)
                        } else {
                          console.info(`${bid.side} has bid: ${bid.type}${wonString}`)
                        }  
                         
                        
                        if (bid.type == 'level') {
                          // update 'won' status if necessary
                          bid_level=bid.level;
                          
                          if(bid.suit==='C')
                            {
                                bid_suit_image=require('../../assets/images/clubs.png');
                                bid_suit_color=styles.clubsImageActive;
                                bid_suit_text_color=styles.bidCircleTextClubsActive;
                                bid_suit_border_color=styles.bidCirclePositionClubsActive;
                                bid_winnerTextColor='#79E62B';
                            }
                            else if(bid.suit==='D')
                            {
                                bid_suit_image=require('../../assets/images/diamond.png');
                                bid_suit_color=styles.diamondImageActive;
                                bid_suit_text_color=styles.bidCircleTextDiamondActive;
                                bid_suit_border_color=styles.bidCirclePositionDiamondActive;
                                bid_winnerTextColor='#04AEFF';
                            }
                            else if(bid.suit==='H')
                            {
                                bid_suit_image=require('../../assets/images/hearts.png');
                                bid_suit_color=styles.heartsImageActive;
                                bid_suit_text_color=styles.bidCircleTextHeartsActive;
                                bid_suit_border_color=styles.bidCirclePositionHeartsActive;
                                bid_winnerTextColor='#FF0C3E';
                            }
                            else if(bid.suit==='S')
                            {
                                bid_suit_image=require('../../assets/images/spade.png');
                                bid_suit_color=styles.spadeImageActive;
                                bid_suit_text_color=styles.bidCircleTextSpadeActive;
                                bid_suit_border_color=styles.bidCirclePositionSpadeActive;
                                bid_winnerTextColor='#C000FF';
                            }
                            else if(bid.suit==='NT')
                            {
                                bid_suit_image='';
                                bid_suit_color=styles.bidCircleTextAssActive;
                                bid_suit_text_color=styles.bidCircleTextNTActive;
                                bid_suit_border_color=styles.bidCirclePositionNTActive;
                                bid_level=bid.level+'NT';
                                bid_winnerTextColor='#FFE81D';
                            } 
                            
                            if (bid.won) { 
                                this.setState({
                                 winnerLevel:bid_level,
                                 winnerSide:bid.side,
                                 winnerSuit:bid.suit,
                                 winnerSuitImage:bid_suit_image,
                                 bid_raised_cnt:Number(this.state.bid_raised_cnt) + 1 ,
                                 winnerTextColor:bid_winnerTextColor,
                                 });
                            }
                            if(bid.side===this.state.myRHOSeat)
                            { 
                                this.setState({            
                                    my_RHO_bid_level:bid_level,
                                    my_RHO_bid_suit_image:bid_suit_image,
                                    my_RHO_bid_suit_border_color:bid_suit_border_color,
                                    my_RHO_bid_suit_color: bid_suit_color,
                                    my_RHO_bid_suit_text_color:bid_suit_text_color, 
                                    my_RHO_bid_type:'level',
                                    DirectionTextRHO:'#6D6D6D',eastUserText:'#6D6D6D'
                                });
                            }
                            else if(bid.side===this.state.mySeat)
                            { 
                                this.setState({            
                                    my_bid_level:bid_level,
                                    my_bid_suit_image:bid_suit_image,
                                    my_bid_suit_border_color:bid_suit_border_color,
                                    my_bid_suit_color: bid_suit_color,
                                    my_bid_suit_text_color:bid_suit_text_color, 
                                    bid_type:'level',
                                    DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D'
                                });
                            }
                            else if(bid.side===this.state.myPartnerSeat )
                            { 
                                this.setState({            
                                    my_partner_bid_level:bid_level, 
                                    my_partner_bid_suit_border_color:bid_suit_border_color,
                                    my_partner_bid_suit_color: bid_suit_color,
                                    my_partner_bid_suit_text_color:bid_suit_text_color,
                                    my_partner_bid_suit_image:bid_suit_image,
                                    my_partner_bid_type:'level',
                                    DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D'
                                });
                            }
                            else if(bid.side===this.state.myLHOSeat)
                            { 
                                this.setState({            
                                    my_LHO_bid_level:bid_level,
                                    my_LHO_bid_suit_image:bid_suit_image,
                                    my_LHO_bid_suit_border_color:bid_suit_border_color,
                                    my_LHO_bid_suit_color: bid_suit_color,
                                    my_LHO_bid_suit_text_color:bid_suit_text_color, 
                                    my_LHO_bid_type:'level',
                                    DirectionTextLHO:'#6D6D6D',westUserText:'#6D6D6D'
                                });
                            }
                             if (bid.won) { 
                                if(Number(this.state.bid_raised_cnt)<4){ 
                                    if( this.state.myLHOSeat===bid.side || this.state.myRHOSeat===bid.side)
                                    {
                                        //console.log('2');
                                        // this.showEligibleBids(bid.level,bid.suit); 
                                        // this.setState({showBiddingBox:true}); 
                                        this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',
                                        DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00',
                                        DirectionTextRHO:'#6D6D6D',eastUserText:'#6D6D6D',
                                            DirectionTextLHO:'#6D6D6D',westUserText:'#6D6D6D', 
                                             });
                                        //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'}); 
                                    } 
                                    else if(gamestate.side===bid.side || this.state.myPartnerSeat===bid.side )
                                    {
                                        //highlight oppent bid chance
                                        this.setState({
                                            DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',
                                            DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00', 
                                            DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D',
                                            DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D',
                                        }); 
                                    }
                                }
                                else if(Number(this.state.bid_raised_cnt)>=3){
                                    //EnableOnlyDoubleorRedouble
                                    if( this.state.myLHOSeat===bid.side || this.state.myRHOSeat===bid.side)
                                    {
                                       // console.log('3');
                                        //his.showDblOrRedbl('dbl');
                                        this.setState({
                                            DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',
                                            DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00',
                                            bid_raised_cnt:Number(this.state.bid_raised_cnt)+1,showBiddingBox:true});        
                                    }
                                    if(gamestate.side===bid.side || this.state.myPartnerSeat===bid.side )
                                    {
                                        this.setState({
                                            DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',
                                            DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00',                                            
                                        });
                                          
                                    }
                                }
                             }
                        } else if (bid.type === 'redouble') {
                          //bidSpan.querySelector('span').innerHTML += ' XX'
                          this.showTextBids('XX',bid.side);
                          this.setState({ 
                            winnerdblRedbl:'x4',
                            });
                           this.gotoPlayScreen();  
                        } else if (bid.type === 'double') {
                          //bidSpan.querySelector('span').innerHTML = 'X'
                          //console.log('inside double');
                          this.showTextBids('X',bid.side);
                          // highlight the double
                          
                            if (bid.won) {
                                if(gamestate.side===this.state.winnerSide)  
                                {
                                    // this.showDblOrRedbl('Redbl');
                                    // this.setState({
                                    //     showBiddingBox:true,
                                    //     EnableRedouble :false,
                                    //     EnableDouble:true,
                                    // });
                                    //showOnlyRedouble 
                                }
                                else if( this.state.myLHOSeat===this.state.winnerSide)
                                { 
                                    this.setState({DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00', 
                                                    DirectionTextRHO:'#6D6D6D',eastUserText:'#6D6D6D', 
                                                    DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D',
                                                    DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D',
                                        });
                                    //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'}); 
                                }
                                else if(this.state.myRHOSeat===this.state.winnerSide)
                                { 
                                    this.setState({DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',                                                 
                                            DirectionTextLHO:'#6D6D6D',westUserText:'#6D6D6D', 
                                            DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D',
                                            DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D',
                                        });
                                    //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'}); 
                                }
                                
                                    //winnerSide:bid.side,
                                this.setState({
                                    winnerdblRedbl:'x2',
                                });
                                 
                            }
                        } else if (bid.type === 'pass') {
                            //bidSpan.querySelector('span').innerHTML = 'P'
                          this.showTextBids('P',bid.side); 
                          if (bid.won) {
                              console.log('pass won');
                              if(this.state.winnerSide==='')
                              {
                                  //RedrawtheDeal
                              }
                              else{
                                  //pass won
                                console.log('pass won go to play');
                                this.gotoPlayScreen();
                              }
                          }         
                          if(this.state.winnerSide!=='' && this.state.winnerdblRedbl==='' && bid.won)
                            {
                                if( this.state.myLHOSeat===this.state.winnerSide || this.state.myRHOSeat===this.state.winnerSide)
                                { 
                                    //console.log('5');
                                    this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',
                                    DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00',
                                        
                                        });
                                    //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'}); 
                                }
                                if(this.state.mySeat===this.state.winnerSide || this.state.myPartnerSeat===this.state.winnerSide )
                                {
                                    this.setState({
                                        DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',
                                        DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00',
                                        
                                    }); 
                                }
                            }
                        } else {
                          //bidSpan.querySelector('span').innerHTML = bid.type
                        }
                    } 
                    else {
                      console.info(`${bid.side} has made a bid;`)
                      if(bid.side===this.state.myRHOSeat)
                      {
                          this.setState({DirectionTextRHO:'#6D6D6D',eastUserText:'#6D6D6D'});                         
                      }
                      else if(bid.side===this.state.mySeat)
                      { 
                          this.setState({DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D'});
                      }
                      else if(bid.side===this.state.myPartnerSeat)
                      { 
                          this.setState({DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D'});
                      }
                      else if(bid.side===this.state.myLHOSeat)
                      {
                          this.setState({DirectionTextLHO:'#6D6D6D',westUserText:'#6D6D6D'});                           
                      }

                      if((this.state.DirectionTextMySeat==='#6D6D6D' && this.state.DirectionTextPartner==='#6D6D6D') || (this.state.DirectionTextRHO==='#6D6D6D' && this.state.DirectionTextLHO==='#6D6D6D')){
                        if(bid.side==='N' || bid.side==='S'){
                            this.setState({NSStyle:styles.ContractText,NSBIDStyle: styles.BidText,
                                EWStyle:styles.ContractTextActive,EWBIDStyle: styles.BidTextActive,});
                        }
                        else if(bid.side==='E' || bid.side==='W'){
                            this.setState({NSStyle:styles.ContractTextActive,NSBIDStyle: styles.BidTextActive,
                                EWStyle:styles.ContractText,EWBIDStyle: styles.BidText,});
                        }
                      }
                      
                    } 
                   
               
             });    
          }

          displayCards(hand) {
            
            var cardSide=[];  
            var idx=0;
             hand.forEach((card) =>{   
              //56   console.log(card.suit);          
              if(card.suit==="S")
                {
                    cardSide.push({"rank":card.rank,"img": require('../../assets/images/spade.png'),"suit":"S","key": card.rank + "S","SuiteColor":"#C000FF","idx":idx}) 
                    
                }else if(card.suit==='D')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/diamond.png'),"suit":"D","key": card.rank + "D","SuiteColor":"#04AEFF","idx":idx})
                     
                }else if(card.suit==='H')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/hearts.png'),"suit":"H","key": card.rank + "H","SuiteColor":"#FF0C3E","idx":idx});
                     
                }else if(card.suit==='C')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/clubs.png'),"suit":"C","key": card.rank + "C","SuiteColor":"#79E62B","idx":idx},)
                    
                }  
                idx++;  
            });
           // this.drawCardsLHO();
           
           return cardSide;
        }
 
          showTextBids(Txt,bid_side)
          {
            const { navigation } = this.props; 
            const gamestate = navigation.getParam('gameState'); 
            if(bid_side===this.state.myRHOSeat)
            { 
                this.setState({            
                    my_RHO_bid_level:Txt,
                    my_RHO_bid_suit_image:'',
                    my_RHO_bid_suit_border_color:styles.bidCirclePositionBodrAssActive,
                    my_RHO_bid_suit_color: styles.bidCircleTextAssActive,
                    my_RHO_bid_suit_text_color:styles.bidCircleTextAssActive,
                    my_RHO_bid_suit_image:'',
                    DirectionTextRHO:'#6D6D6D',eastUserText:'#6D6D6D'
                });
            }
            else if(bid_side===this.state.mySeat)
            { 
                this.setState({            
                    my_bid_level:Txt,
                    my_bid_suit_image:'',
                    my_bid_suit_border_color:styles.bidCirclePositionBodrAssActive,
                    my_bid_suit_color: styles.bidCircleTextAssActive,
                    my_bid_suit_text_color:styles.bidCircleTextAssActive,
                    my_bid_suit_image:'',
                    DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D'
                });
            }
            else if(bid_side===this.state.myPartnerSeat)
            { 
                this.setState({            
                    my_partner_bid_level:Txt, 
                    my_partner_bid_suit_border_color:styles.bidCirclePositionBodrAssActive,
                    my_partner_bid_suit_color: styles.bidCircleTextAssActive,
                    my_partner_bid_suit_text_color:styles.bidCircleTextAssActive,
                    my_partner_bid_suit_image:'',
                    DirectionTextPartner:'#6D6D6D',northUserText:'#6D6D6D'
                });
            }
            else if(bid_side===this.state.myLHOSeat)
            { 
                this.setState({            
                    my_LHO_bid_level:Txt,
                    my_LHO_bid_suit_image:'',
                    my_LHO_bid_suit_border_color:styles.bidCirclePositionBodrAssActive,
                    my_LHO_bid_suit_color: styles.bidCircleTextAssActive,
                    my_LHO_bid_suit_text_color:styles.bidCircleTextAssActive,
                    my_LHO_bid_suit_image:'',
                    DirectionTextLHO:'#6D6D6D',westUserText:'#6D6D6D'
                });
            }
            
          }

          gotoPlayScreen()
          {
            const { navigation } = this.props; 
            const gamestate = navigation.getParam('gameState');
            const hool = navigation.getParam('xmppconn'); 
            if(this.state.winnerSide==='N')
            { 
                gamestate.StartSeat='E';
                gamestate.DummySeat='S';
            }
            else if(this.state.winnerSide==='E')
            { 
                gamestate.StartSeat='S';
                gamestate.DummySeat='W';
            }
            else if(this.state.winnerSide==='S')
            { 
                gamestate.StartSeat='W';
                gamestate.DummySeat='N';
            }
            else if(this.state.winnerSide==='W')
            { 
                gamestate.StartSeat='N';
                gamestate.DummySeat='E';
            }
            gamestate.winnerSuit=this.state.winnerSuit;
            gamestate.winnerLevel=this.state.winnerLevel;
            gamestate.winnerSide=this.state.winnerSide;
            gamestate.winnerSuitImage=this.state.winnerSuitImage;
            gamestate.winnerdblRedbl=this.state.winnerdblRedbl;
            gamestate.mySeat=this.state.mySeat;
            gamestate.myPartnerSeat=this.state.myPartnerSeat, 
            gamestate.myLHOSeat=this.state.myLHOSeat;
            gamestate.myRHOSeat=this.state.myRHOSeat;
            gamestate.myUsername=this.state.southUser;
            gamestate.myPartnername=this.state.northUser;
            gamestate.myLHOname=this.state.westUser;
            gamestate.myRHOname=this.state.eastUser;
             console.log('goto to play from bidding:');
            //console.log(gamestate);
            this.props.navigation.navigate('CardPlayKibz',{xmppconn : hool,tableID:this.state.tableID,gameState:gamestate});
          }

        showEligibleBids(level,suit)
        { 
            console.log('inside eligiblebids');
            if(suit==='NT')
            {
                level=level+1;
            }
            this.setState({
                Trick_Pass_Disabled:false,
                Trick_1_Disabled:true,
                Trick_2_Disabled:true,
                Trick_3_Disabled:true,
                Trick_4_Disabled:true,
                Trick_5_Disabled:true,
                Trick_6_Disabled:true,
                Trick_7_Disabled:true,
                Trick_1_bg:styles.bidBoxWidthHeightArea,
                Trick_2_bg:styles.bidBoxWidthHeightArea,
                Trick_3_bg:styles.bidBoxWidthHeightArea,
                Trick_4_bg:styles.bidBoxWidthHeightArea,
                Trick_5_bg:styles.bidBoxWidthHeightArea,
                Trick_6_bg:styles.bidBoxWidthHeightArea,
                Trick_7_bg:styles.bidBoxWidthHeightArea, 
                Trick_1_Text:styles.bidBoxDisabledLevel,
                Trick_2_Text:styles.bidBoxDisabledLevel,
                Trick_3_Text:styles.bidBoxDisabledLevel,
                Trick_4_Text:styles.bidBoxDisabledLevel,
                Trick_5_Text:styles.bidBoxDisabledLevel,
                Trick_6_Text:styles.bidBoxDisabledLevel,
                Trick_7_Text:styles.bidBoxDisabledLevel,
                Suit_Club:require('../../assets/images/clubs_black.png'),
                Suit_Diamond:require('../../assets/images/diamond_black.png'),
                Suit_Heart:require('../../assets/images/hearts_black.png'),
                Suit_Spade:require('../../assets/images/spade_black.png'),
                Make_Bid:styles.bidBoxWidthHeightArea, 
                Trick_Pass_bg:styles.bidBoxWidthHeightAreaActive, 
                Trick_Pass_Text:styles.bidBoxWidthHeightTextClor,
                EnableDouble:false,
                Double_bg:styles.bidBoxWidthHeightAreaActive, 
                Double_Text:styles.bidBoxWidthHeightTextClor,
            });
            if(this.state.winnerLevel!==''){ 
                    switch (level){
                       case '1':{
                        this.setState({ 
                            Trick_1_Disabled:false,
                            Trick_2_Disabled:false,
                            Trick_3_Disabled:false,
                            Trick_4_Disabled:false,
                            Trick_5_Disabled:false,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false, 
                            Trick_1_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_2_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_3_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive, 
                            Trick_1_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_2_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_3_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_4_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                            });
                           break;
                       }
                       case '2':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:false,
                            Trick_3_Disabled:false,
                            Trick_4_Disabled:false,
                            Trick_5_Disabled:false,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false, 
                            Trick_2_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_3_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive, 
                            Trick_2_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_3_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_4_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                       case '3':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:true,
                            Trick_3_Disabled:false,
                            Trick_4_Disabled:false,
                            Trick_5_Disabled:false,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false, 
                            Trick_3_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive, 
                            Trick_3_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_4_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                       case '4':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:true,
                            Trick_3_Disabled:true,
                            Trick_4_Disabled:false,
                            Trick_5_Disabled:false,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false, 
                            Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive, 
                            Trick_4_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                       case '5':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:true,
                            Trick_3_Disabled:true,
                            Trick_4_Disabled:true,
                            Trick_5_Disabled:false,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false, 
                            Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive, 
                            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                       case '6':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:true,
                            Trick_3_Disabled:true,
                            Trick_4_Disabled:true,
                            Trick_5_Disabled:true,
                            Trick_6_Disabled:false,
                            Trick_7_Disabled:false,  
                            Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive,  
                            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                       case '7':{
                        this.setState({ 
                            Trick_1_Disabled:true,
                            Trick_2_Disabled:true,
                            Trick_3_Disabled:true,
                            Trick_4_Disabled:true,
                            Trick_5_Disabled:true,
                            Trick_6_Disabled:true,
                            Trick_7_Disabled:false, 
                            Trick_7_bg:styles.bidBoxWidthHeightAreaActive,   
                            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
                        });
                           break;
                       }
                    }
                   
               }
        }  

        showEligibleSuit(level)
        { 
             
            if(this.state.winnerLevel!==''){
                
             if(Number(level)===Number(this.state.winnerLevel)){
                 switch (this.state.winnerSuit){
                    case 'C':{
                        this.setState({
                        Suit_Club:require('../../assets/images/clubs_black.png'),
                        Suit_Diamond:require('../../assets/images/diamond_inactive.png'),
                        Suit_Heart:require('../../assets/images/hearts_inactive.png'),
                        Suit_Spade:require('../../assets/images/spade_inactive.png'),
                        EnableTrump_Club:true,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                    }
                    case 'D':{
                        this.setState({
                        Suit_Club:require('../../assets/images/clubs_black.png'),
                        Suit_Diamond:require('../../assets/images/diamond_black.png'),
                        Suit_Heart:require('../../assets/images/hearts_inactive.png'),
                        Suit_Spade:require('../../assets/images/spade_inactive.png'),
                        EnableTrump_Club:true,
                        EnableTrump_Diamond:true,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                    }
                    case 'H':{
                        this.setState({
                        Suit_Club:require('../../assets/images/clubs_black.png'),
                        Suit_Diamond:require('../../assets/images/diamond_black.png'),
                        Suit_Heart:require('../../assets/images/hearts_black.png'),
                        Suit_Spade:require('../../assets/images/spade_inactive.png'),
                        EnableTrump_Club:true,
                        EnableTrump_Diamond:true,
                        EnableTrump_Heart:true,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                    }
                    case 'S':{
                        this.setState({
                        Suit_Club:require('../../assets/images/clubs_black.png'),
                        Suit_Diamond:require('../../assets/images/diamond_black.png'),
                        Suit_Heart:require('../../assets/images/hearts_black.png'),
                        Suit_Spade:require('../../assets/images/spade_black.png'),
                        EnableTrump_Club:true,
                        EnableTrump_Diamond:true,
                        EnableTrump_Heart:true,
                        EnableTrump_Spade:true,
                        EnableTrump_NT:false,
                        });
                        break;
                    }
                 }
             }   
            }
        }

        showDblOrRedbl(both)
        { 
            this.setState({
                Trick_Pass_Disabled:false,
                Trick_1_Disabled:true,
                Trick_2_Disabled:true,
                Trick_3_Disabled:true,
                Trick_4_Disabled:true,
                Trick_5_Disabled:true,
                Trick_6_Disabled:true,
                Trick_7_Disabled:true,
                Trick_1_bg:styles.bidBoxWidthHeightArea,
                Trick_2_bg:styles.bidBoxWidthHeightArea,
                Trick_3_bg:styles.bidBoxWidthHeightArea,
                Trick_4_bg:styles.bidBoxWidthHeightArea,
                Trick_5_bg:styles.bidBoxWidthHeightArea,
                Trick_6_bg:styles.bidBoxWidthHeightArea,
                Trick_7_bg:styles.bidBoxWidthHeightArea, 
                Trick_1_Text:styles.bidBoxDisabledLevel,
                Trick_2_Text:styles.bidBoxDisabledLevel,
                Trick_3_Text:styles.bidBoxDisabledLevel,
                Trick_4_Text:styles.bidBoxDisabledLevel,
                Trick_5_Text:styles.bidBoxDisabledLevel,
                Trick_6_Text:styles.bidBoxDisabledLevel,
                Trick_7_Text:styles.bidBoxDisabledLevel,
                Suit_Club:require('../../assets/images/clubs_black.png'),
                Suit_Diamond:require('../../assets/images/diamond_black.png'),
                Suit_Heart:require('../../assets/images/hearts_black.png'),
                Suit_Spade:require('../../assets/images/spade_black.png'),
                NT_bg:styles.bidBoxWidthHeightArea,
                NT_Text:styles.bidBoxDisabledLevel,
                EnableTrump_Club:true,
                EnableTrump_Diamond:true,
                EnableTrump_Heart:true,
                EnableTrump_Spade:true,
                EnableTrump_NT:true,
                Make_Bid:styles.bidBoxWidthHeightArea, 
                Double_bg:styles.bidBoxWidthHeightArea,
                Redouble_bg:styles.bidBoxWidthHeightArea,
                Double_Text:styles.bidDblTextClordisabled,
                Redouble_Text:styles.bidRedblTextClordisabled,
            });
            if(both==='dbl')
            {
                this.setState({
                    Trick_Pass_bg:styles.bidBoxWidthHeightAreaActive, 
                    Trick_Pass_Text:styles.bidBoxWidthHeightTextClor,
                    Double_bg:styles.bidBoxWidthHeightAreaActive, 
                    Double_Text:styles.bidBoxWidthHeightTextClor,
                });
            }
            else{
                this.setState({
                    Trick_Pass_bg:styles.bidBoxWidthHeightAreaActive, 
                    Trick_Pass_Text:styles.bidBoxWidthHeightTextClor,
                    Redouble_bg:styles.bidBoxWidthHeightAreaActive, 
                    Redouble_Text:styles.bidBoxWidthHeightTextClor, 
                });
            }
        }
    
         SelectedTrick(trick)
         {
          this.setState({Trick_Pass_bg:styles.bidBoxWidthHeightArea,
            Trick_1_bg:styles.bidBoxWidthHeightArea,
            Trick_2_bg:styles.bidBoxWidthHeightArea,
            Trick_3_bg:styles.bidBoxWidthHeightArea,
            Trick_4_bg:styles.bidBoxWidthHeightArea,
            Trick_5_bg:styles.bidBoxWidthHeightArea,
            Trick_6_bg:styles.bidBoxWidthHeightArea,
            Trick_7_bg:styles.bidBoxWidthHeightArea,
            Trick_Pass_Text:styles.bidBoxWidthHeightTextClor,
            Trick_1_Text:styles.bidBoxWidthHeightTextClor,
            Trick_2_Text:styles.bidBoxWidthHeightTextClor,
            Trick_3_Text:styles.bidBoxWidthHeightTextClor,
            Trick_4_Text:styles.bidBoxWidthHeightTextClor,
            Trick_5_Text:styles.bidBoxWidthHeightTextClor,
            Trick_6_Text:styles.bidBoxWidthHeightTextClor,
            Trick_7_Text:styles.bidBoxWidthHeightTextClor,
            Make_Bid:styles.bidBoxWidthHeightArea,
            });  
            if(this.state.winnerLevel!=='')
            {
                this.showEligibleBids(this.state.winnerLevel,this.state.winnerSuit);
            } 
            switch (trick) {
                case 'P': {
                    this.setState({Trick_Pass_bg:styles.bidBoxWidthHeightAreaActive,
                    Trick_Pass_Text:styles.bidBoxWidthHeightTextClorActive,
                    Suit_Club:require('../../assets/images/clubs_black.png'),
                    Suit_Diamond:require('../../assets/images/diamond_black.png'),
                    Suit_Heart:require('../../assets/images/hearts_black.png'),
                    Suit_Spade:require('../../assets/images/spade_black.png'),
                    NT_bg:styles.bidBoxWidthHeightArea,
                    NT_Text:styles.bidBoxDisabledLevel,
                    EnableTrump_Club:true,
                    EnableTrump_Diamond:true,
                    EnableTrump_Heart:true,
                    EnableTrump_Spade:true,
                    EnableTrump_NT:true,
                    EnableBid:false,
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    bid_level:'',
                    bid_type:'pass',
                    });
                    break;
                }
                case '1': {
                        this.setState({
                        Trick_1_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_1_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'1',
                        bid_type:'level', 
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '2': {
                        this.setState({Trick_2_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_2_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'2',
                        bid_type:'level',
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '3': {
                        this.setState({Trick_3_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_3_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'3',
                        bid_type:'level',
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '4': {
                        this.setState({Trick_4_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_4_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'4',
                        bid_type:'level',
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '5': {
                        this.setState({Trick_5_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_5_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'5',
                        bid_type:'level',
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '6': {
                        this.setState({Trick_6_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_6_Text:styles.bidBoxWidthHeightTextClorActive,
                        bid_level:'6',
                        bid_type:'level',
                        EnableTrump_Club:false,
                        EnableTrump_Diamond:false,
                        EnableTrump_Heart:false,
                        EnableTrump_Spade:false,
                        EnableTrump_NT:false,
                        });
                        break;
                }
                case '7': {
                        this.setState({Trick_7_bg:styles.bidBoxWidthHeightAreaActive,
                        Trick_7_Text:styles.bidBoxWidthHeightTextClorActive,
                         bid_level:'7',
                        bid_type:'level',
                        
                        });
                        break;
                }
            }
            if(trick!=='P'){
                this.setState({ Suit_Club:require('../../assets/images/clubs_inactive.png'),
                        Suit_Diamond:require('../../assets/images/diamond_inactive.png'),
                        Suit_Heart:require('../../assets/images/hearts_inactive.png'),
                        Suit_Spade:require('../../assets/images/spade_inactive.png'),
                        NT_bg:styles.bidBoxWidthHeightArea,
                        NT_Text:styles.bidBoxWidthHeightTextClor,
            });
              
            this.showEligibleSuit(trick);          
            }
         } 

         SelectedTrump(trump){
             if(this.state.EnableTrump_Club)
             {
                this.setState({Suit_Club:require('../../assets/images/clubs_black.png')});
             }
             else{
                this.setState({Suit_Club:require('../../assets/images/clubs_inactive.png')});
             }
             if(this.state.EnableTrump_Diamond)
             {
                this.setState({Suit_Diamond:require('../../assets/images/diamond_black.png')});
             }
             else{
                this.setState({Suit_Diamond:require('../../assets/images/diamond_inactive.png')});
             }
             if(this.state.EnableTrump_Heart)
             {
                this.setState({Suit_Heart:require('../../assets/images/hearts_black.png')});
             }
             else{
                this.setState({Suit_Heart:require('../../assets/images/hearts_inactive.png')});
             }
             if(this.state.EnableTrump_Spade)
             {
                this.setState({Suit_Spade:require('../../assets/images/spade_black.png')});
             }
             else{
                this.setState({Suit_Spade:require('../../assets/images/spade_inactive.png')});
             }
            switch (trump) {
                case 'NT': {
                    this.setState({ 
                    NT_bg:styles.bidBoxWidthHeightAreaActive,
                    NT_Text:styles.bidBoxWidthHeightTextClorNTActive,
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    bid_suit:'NT',
                    EnableBid:false,
                    });
                    break;
                }
                case 'C': {
                    this.setState({
                    Suit_Club:require('../../assets/images/clubs.png'), 
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    Suit_Club_bg:styles.bidBoxWidthHeightAreaActive,
                    NT_bg:styles.bidBoxWidthHeightArea,
                    NT_Text:styles.bidBoxWidthHeightTextClor,
                    bid_suit:'C',
                    EnableBid:false,
                    });
                    break;
                }
                case 'D': {
                    this.setState({ 
                    Suit_Diamond:require('../../assets/images/diamond.png'), 
                    NT_bg:styles.bidBoxWidthHeightArea,
                    NT_Text:styles.bidBoxWidthHeightTextClor,
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    Suit_Diamond_bg:styles.bidBoxWidthHeightAreaActive,
                    bid_suit:'D',
                    EnableBid:false,
                    });
                    break;
                }
                case 'H': {
                    this.setState({ 
                    Suit_Heart:require('../../assets/images/hearts.png'), 
                    NT_bg:styles.bidBoxWidthHeightArea,
                    NT_Text:styles.bidBoxWidthHeightTextClor,
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    Suit_Heart_bg:styles.bidBoxWidthHeightAreaActive,
                    bid_suit:'H',
                    EnableBid:false,
                    });
                    break;
                }
                case 'S': {
                    this.setState({ 
                    Suit_Spade:require('../../assets/images/spade.png'),
                    NT_bg:styles.bidBoxWidthHeightArea,
                    NT_Text:styles.bidBoxWidthHeightTextClor,
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    Suit_Spade_bg:styles.bidBoxWidthHeightAreaActive,
                    bid_suit:'S',
                    EnableBid:false,
                    });
                    break;
                }
                case 'X': {
                    this.setState({ 
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    bid_suit:'',
                    bid_level:'',
                    bid_type:'double',
                    EnableBid:false,
                    Double_Text:styles.bidBoxWidthHeightTextClorActive, 
                    });
                    break;
                }
                case 'XX': {
                    this.setState({ 
                    Make_Bid:styles.bidBoxWidthHeightAreaBodrActive,
                    bid_suit:'',
                    bid_level:'',
                    bid_type:'redouble',
                    EnableBid:false,
                    Redouble_Text:styles.bidBoxWidthHeightTextClorActive,
                    });
                    break;
                }
            }     
         }
           
         makeBid(){ 
            const { navigation } = this.props;
            const hool = navigation.getParam('xmppconn');
            console.log(this.state.bid_type +','+this.state.bid_level + ','+this.state.bid_suit);
            if(this.state.bid_type==='pass')
            {
                this.setState({            
                my_bid_level:'P',
                my_bid_suit_image:'',
                my_bid_suit_border_color:styles.bidCirclePositionBodrAssActive,
                my_bid_suit_color: styles.bidCircleTextAssActive,
                my_bid_suit_text_color:styles.bidCircleTextAssActive,
                my_bid_suit_image:'',
                });
            }
            else if(this.state.bid_type==='level')
            {
                this.setState({            
                my_bid_level:this.state.bid_level,
                });
                if(this.state.bid_suit==='C')
                {
                    this.setState({
                        my_bid_suit_image:require('../../assets/images/clubs.png'),
                        my_bid_suit_color:styles.clubsImageActive,
                        my_bid_suit_text_color:styles.bidCircleTextClubsActive,
                        my_bid_suit_border_color:styles.bidCirclePositionClubsActive,
                    });
                }
                else if(this.state.bid_suit==='D')
                {
                    this.setState({
                        my_bid_suit_image:require('../../assets/images/diamond.png'),
                        my_bid_suit_color:styles.diamondImageActive,
                        my_bid_suit_text_color:styles.bidCircleTextDiamondActive,
                        my_bid_suit_border_color:styles.bidCirclePositionDiamondActive,
                    })
                }
                else if(this.state.bid_suit==='H')
                {
                    this.setState({
                        my_bid_suit_image:require('../../assets/images/hearts.png'),
                        my_bid_suit_color:styles.heartsImageActive,
                        my_bid_suit_text_color:styles.bidCircleTextHeartsActive,
                        my_bid_suit_border_color:styles.bidCirclePositionHeartsActive,
                    })
                }
                else if(this.state.bid_suit==='S')
                {
                    this.setState({
                        my_bid_suit_image:require('../../assets/images/spade.png'),
                        my_bid_suit_color:styles.spadeImageActive,
                        my_bid_suit_text_color:styles.bidCircleTextSpadeActive,
                        my_bid_suit_border_color:styles.bidCirclePositionSpadeActive,
                    });
                    //bidCircleTextNTActive
                }
                else if(this.state.bid_suit==='NT')
                {
                    this.setState({
                        my_bid_suit_image:'',
                        my_bid_suit_color:styles.bidCircleTextAssActive,
                        my_bid_suit_text_color:styles.bidCircleTextNTActive,
                        my_bid_suit_border_color:styles.bidCirclePositionNTActive,
                        my_bid_level:this.state.bid_level+'NT',
                    });
                    //
                }
            }
            else if(this.state.bid_type==='double')
            {
                this.setState({            
                my_bid_level:'X',
                my_bid_suit:'',
                });
            }
            else if(this.state.bid_type==='redouble')
            {
                this.setState({            
                my_bid_level:'XX',
                my_bid_suit:'',
                });
            }
        // TODO: autocalculate info value and share that as well 
        this.setState({showBiddingBox:false});
        this.setState({DirectionTextMySeat:'#6D6D6D',southUserText:'#6D6D6D'});
        //console.log(this.state.tableID +',' +  this.state.mySeat +',' + this.state.bid_type +','+this.state.bid_level + ','+this.state.bid_suit);
       // hool.makeBid(this.state.tableID, this.state.mySeat, this.state.bid_type, this.state.bid_level, this.state.bid_suit); 
    }
      
    render(){ 
        const infoShare = (infoType) => {
         console.log('touchable clikced') ;
         if(infoType==='HCP')  
         {
             this.setState({infoHCPClicked:true});
             this.setState({cardHCPTextColor:'#6D6D6D',HCPCardBorder:'#6D6D6D'})
         }
         else if(infoType==='pattern')  
         {
             this.setState({infoPatternClicked:true});
             this.setState({cardPatternTextColor:'#6D6D6D',PatternCardBorder:'#6D6D6D'});
         }
         else if(infoType==='C' || infoType==='D' || infoType==='H' || infoType==='S')  
         {         
             
             this.setState({infoSuitClicked:true});
             this.setState({showSuitCards:false});
         }
         
          const { navigation } = this.props;
          const hool = navigation.getParam('xmppconn');
          const gamestate = navigation.getParam('gameState'); 
          const tableID = navigation.getParam('tableID');
        //   console.log(tableID +', ' + gamestate.side +','+ infoType);
        //   hool.shareInfo(tableID, gamestate.side,infoType);
        //  this.setState({showShareCards : false});
        }  
        const drawCards = () => {
            return southHands.map((element) => {
              return (
                <View style={styles.cardDisplayBody} key={element.key} >
                    <View style={styles.cardPosition}> 
                        <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                        <Image source={element.img} />   
                    </View>
                </View>
              );
            });
          };

          const drawCardsEast = () => {
            return eastHands.map((element) => {
              return (
                <View style={styles.cardDisplayBodyEast} key={element.key} >
                    <View style={styles.cardPositionE}>
                        <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                        <Image source={element.img} />
                    </View>      
                </View>
              );
            });
          };
          const drawCardsWest = () => {
            return westHands.map((element) => {
              return (
                <View style={styles.cardDisplayBodyNorth} key={element.key} >
                    <View style={styles.cardPositionW}>
                        <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                        <Image source={element.img} />
                    </View>      
                </View>
              );
            });
          };
          const drawCardsNorthKibitzer = () => {
        return northHands.map((element) => {
          return (
            <View style={styles.cardDisplayBodyNorth} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      
    return ( 
    
<View style={{backgroundColor:"#0a0a0a",height:"100%"}}>
    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", width:"100%", height:HeightBoxTop, backgroundColor:"#151515"}}>
        <View style={{width:"100%", height:ContractTextHeight, borderBottomWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
            <Text style={styles.ContractTextHeader}>Contract</Text>
        </View>
        <View style={{width:"100%",flexDirection:"row", borderBottomWidth:1, borderBottomColor:"#0a0a0a"}}>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                <Text style={[styles.ContractTextHeader,{color:this.state.winnerTextColor}]}>{this.state.winnerSide}</Text>
            </View>
            <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                <Text style={[styles.ContractMdlText,{color:this.state.winnerTextColor}]}>{this.state.winnerLevel}</Text>
                
                <Image source={this.state.winnerSuitImage} />
            </View>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, alignItems:"center", justifyContent:"center"}}>
                <Text style={[styles.ContractTextHeader,{color:this.state.winnerTextColor}]}>{this.state.winnerdblRedbl}</Text>
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, alignItems:"center"}}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
                 <Text style={styles.ContractTextActive}>N • S</Text>
            
                <Text style={this.state.NSBIDStyle}>BID</Text>
            </View> 
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, alignItems:"center"}}>
             <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
            
            <Text style={styles.ContractText}>E • W</Text>
            <Text adjustsFontSizeToFit numberOfLines={1} style={this.state.EWBIDStyle}>BID</Text>
            </View>
        </View>
    </View>

    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/settings.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/flag.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/revers.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/chat.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/on.png')} />
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", justifyContent:"space-between"}}>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
        <View style={styles.BiddinghanNorthTotalWidthHeightArea}>
               

                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.northSharedType1}</Text>
                            <Text style={styles.PaternText}>{this.state.northSharedValue1}</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.northSharedType2}</Text>
                            <Text style={styles.PaternText}>{this.state.northSharedValue2}</Text>
                        </View>
                    </View>
                </View>   */}

                <View style={{width:"100%", alignItems:"center", justifyContent:"center"}}>
                    <View style={{width:NorthCardWithKibitzerArea, height:45, flexDirection:"row", justifyContent:"space-around", alignItems:"center",}}>
                        { drawCardsNorthKibitzer() }  
                    </View>
                </View>
                
                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.UserNameText,{color:this.state.northUserText}]}>{this.state.northUser}</Text>
                </View>
                <View style={styles.bidCirclePositionArea}>
                    <View style={this.state.my_partner_bid_suit_border_color}>
                        <Text style={this.state.my_partner_bid_suit_text_color}>{this.state.my_partner_bid_level}</Text>
                        {
                        (this.state.my_partner_bid_type==='level') ?
                            <Image style={this.state.my_partner_bid_suit_color} source={this.state.my_partner_bid_suit_image} />
                        : null
                        }
                    </View> 
                </View>    
            </View>
        </View>

        {/* <View style={{width:"100%", height:81, justifyContent:"center", alignItems:"center"}}>
            <View style={{width:197, height:81, flexDirection:"row", justifyContent:"space-between"}}>
                <View style={styles.SharePatternCard}>
                    <Text style={styles.PatternText}>HCP</Text>
                </View>
                <View style={styles.SharePatternCard}>
                    <Text style={styles.PatternText}>Pattern</Text>
                </View>
                <View style={styles.SharePatternCard}>
                    <Text style={styles.PatternText}>No. of</Text>
                    <View style={{flexDirection:"row", width:"100%", justifyContent:"space-evenly", alignItems:"center"}}>
                        <Image source={require('../../assets/images/spade_white.png')} />
                        <Image source={require('../../assets/images/hearts_white.png')} />
                        <Image source={require('../../assets/images/diamond_white.png')} />
                        <Image source={require('../../assets/images/clubs_white.png')} />
                    </View>
                </View>
            </View>
        </View> */}

        <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}>
            {/* <View style={{flexDirection:"column", width:181, height:96, marginLeft:-38, transform: [{ rotate: '-90deg' }]}}> */}
            <View style={{position:"absolute",left:3, borderWidth:0, borderColor:"red", width:96, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center", }}>
             <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:96,  transform: [{ rotate: '-90deg' }]}}>
               
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.westSharedType1}</Text>
                            <Text style={styles.PaternText}>{this.state.westSharedValue1}</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.westSharedType2}</Text>
                            <Text style={styles.PaternText}>{this.state.westSharedValue2}</Text>
                        </View>
                    </View>
                </View>   */}

                <View style={{width:EastWestPalyCardArea, height:45, marginLeft:75, flexDirection:"row", justifyContent:"space-around"}}>
                { drawCardsWest() }  
                </View>   

                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.UserNameText,{color:this.state.westUserText}]}>{this.state.westUser}</Text>
                </View>

                <View style={styles.bidCirclePositionArea}>
                    <View style={this.state.my_LHO_bid_suit_border_color}>
                        <Text style={this.state.my_LHO_bid_suit_text_color}>{this.state.my_LHO_bid_level}</Text>
                        {
                        (this.state.my_LHO_bid_type==='level') ?
                            <Image style={this.state.my_LHO_bid_suit_color} source={this.state.my_LHO_bid_suit_image} />
                        : null
                        }
                    </View> 
                </View>    
            </View>
            </View>

            
       

        <View style={styles.bidBoxPositionDirectionArea}>
            <View style={{width:61, height:61, justifyContent:"space-between", flexDirection:"column"}}>
                <Text style={[styles.DirectionText,{color: this.state.DirectionTextPartner}]}>{this.state.myPartnerSeat}</Text>
                <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center", paddingLeft:3, paddingRight:3}}>
                    <Text style={[styles.DirectionText,{color: this.state.DirectionTextLHO}]}>{this.state.myLHOSeat}</Text>
                    <Image source={require('../../assets/images/center_circle.png')} />
                    <Text style={[styles.DirectionText,{color: this.state.DirectionTextRHO}]}>{this.state.myRHOSeat}</Text>
                </View>
                <Text style={[styles.DirectionText,{color: this.state.DirectionTextMySeat}]}>{this.state.mySeat}</Text>
            </View>
        </View>

    <View style={{position:"absolute",right:0, width:96, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center",  }}>
        <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:96,  transform: [{ rotate: '90deg' }]}}> 
               
                 {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                        <Text style={styles.shareHandText}>{this.state.eastSharedType1}</Text>
                        <Text style={styles.PaternText}>{this.state.eastSharedValue1}</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                        <Text style={styles.shareHandText}>{this.state.eastSharedType2}</Text>
                        <Text style={styles.PaternText}>{this.state.eastSharedValue2}</Text>
                        </View>
                    </View>
                </View>   */}
            
                <View style={{width:EastWestPalyCardArea, marginRight:-17, height:45,justifyContent:"space-around", flexDirection:"row-reverse"}}>
                    { drawCardsEast() }  
                </View> 

                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.UserNameText,{color:this.state.eastUserText}]}>{this.state.eastUser}</Text>
                </View>

                <View style={styles.bidCirclePositionArea}>
                    <View style={this.state.my_RHO_bid_suit_border_color}>
                        <Text style={this.state.my_RHO_bid_suit_text_color}>{this.state.my_RHO_bid_level}</Text>
                        {
                        (this.state.my_RHO_bid_type==='level') ?
                            <Image style={this.state.my_RHO_bid_suit_color} source={this.state.my_RHO_bid_suit_image} />
                        : null
                        }
                    </View> 
                </View>
            </View>
        </View>
        
    </View>

        {/* <View style={{width:"100%", height:81, flexDirection:"column", alignItems:"center"}}>
            
            <View style={{width:181, height:45, flex:1, justifyContent:"flex-end"}}>
                <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}>
                <View style={{width:90, height:45, backgroundColor:"#151515"}}>
                    <Text style={styles.shareHandText}>Pattern</Text>
                    <Text style={styles.PaternText}>4,4,3,2</Text>
                </View>
                <View style={{width:90, height:45, backgroundColor:"#151515"}}>
                    <Text style={styles.shareHandText}>Spades</Text>
                    <Text style={styles.PaternText}>4</Text>
                </View>
                </View>
            </View>
             <View style={{width:268, height:81, flexDirection:"row", justifyContent:"space-between", position:"absolute"}}>
                <View style={styles.SharePatternCardSpade}>
                    <Image source={require('../../assets/images/spade.png')} />
                </View>
                <View style={styles.SharePatternCardHearts}>
                    <Image source={require('../../assets/images/hearts.png')} />
                </View>
                <View style={styles.SharePatternCardDiamond}>
                    <Image source={require('../../assets/images/diamond.png')} />
                </View>
                <View style={styles.SharePatternCardClubs}>
                    <Image source={require('../../assets/images/clubs.png')} />
                </View>   
            </View>

        </View> */}

        <View style={{width:"100%", height:96, alignItems:"flex-end"}}>
            <View style={styles.bidCirclePositionArea}>
                <View style={this.state.my_bid_suit_border_color}>
                    <Text style={this.state.my_bid_suit_text_color}>{this.state.my_bid_level}</Text>
                    {
                    (this.state.bid_type==='level') ?
                         <Image style={this.state.my_bid_suit_color} source={this.state.my_bid_suit_image} />
                    : null
                    }
                </View> 
            </View>
            <View style={styles.hanNameWidthHeightArea}>
                <Text style={[styles.UserNameText,{color:this.state.southUserText}]}>{this.state.southUser}</Text>
            </View>
            <View style={{width:TotalCardWidth, height:45,  flexDirection:"row", justifyContent:"space-around",alignSelf:"center"}}>
                { drawCards() }  
            </View>
        </View>
   </View>
</View>

  );
};
      }


      const styles = StyleSheet.create({
        hanNameWidthHeightArea:{
            width:"100%", 
            height:23,
        },
        BiddinghanNorthTotalWidthHeightArea:{
            width:"100%", 
            height:96, 
            alignItems:"flex-end",
            flexDirection:"column",
        },
        patternWidthHeightArea:{
            width:90, 
            height:45, 
            backgroundColor:"#151515",
        },
        patternTotalRowArea:{
            flexDirection:"row",
            width:181, 
            height:45, 
            justifyContent:"space-between", 
            alignItems:"center",
        },
        patternTotalColumnArea:{
            flexDirection:"column", 
            width:"100%", 
            height:45,  
            alignItems:"center",
        },
        cardDisplayBodyNorth:{
            width:61,
            height:45,
            borderBottomLeftRadius:4,
            borderBottomRightRadius:4,
            borderWidth:1,
            borderColor:"#0a0a0a",
            backgroundColor:"#151515",
        },
        cardDisplayBodyEast:{
            width:61,
            height:45,
            borderBottomLeftRadius:4,
            borderBottomRightRadius:4,
            borderWidth:1,
            borderColor:"#0a0a0a",
            backgroundColor:"#151515",
            flexDirection:"row-reverse",
        },
        cardPositionW:{
            width:22,
            marginTop:3,
            marginLeft:1,
            alignItems:"center",
            justifyContent:"center",
         },
         cardPositionE:{
            width:22,
            marginTop:-4,
            marginLeft:2,
            alignItems:"center",
            justifyContent:"center",
         },
    
        /************************************* */
        ContractTextActive: {
            position:"absolute",
            top:8,
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "400",
            color:"#6D6D6D",
        },
        BidTextActive: {
            top:5,
            fontFamily:"Roboto-Thin",
            fontSize: normalize(44),
            fontWeight: "normal",
            color:"#6D6D6D",
          },
          BidText: {
            top:5,
            fontFamily:"Roboto-Thin",
            fontSize: normalize(44),
            fontWeight: "normal",
            color:"#353535",
          },
        bidCirclePositionArea:{
            width:"100%", 
            height:28, 
            alignItems:"center",
            justifyContent:"center",
        },
        spadeImageActive:{
            width: 8.5,
            height: 9,
            marginLeft:2,
        },
        heartsImageActive:{
            width: 8,
            height: 7,
            marginLeft:2,
        },
        diamondImageActive:{
            width: 8,
            height: 9,
            marginLeft:2,
        },
        clubsImageActive:{
            width: 8,
            height: 8,
            marginLeft:2,
        },
        bidCircleTextAssActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#B9B9B9",
        },
        bidCircleTextAss:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#676767",
        },
        bidCircleTextHeartsActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#FF0C3E",
        },
        bidCircleTextClubsActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#79E62B",
        },
        bidCircleTextDiamondActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#04AEFF",
        },
        bidCircleTextSpadeActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#C000FF",
        },
        bidCircleTextNTActive:{
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "200",
            color:"#FFE81D",
        },
        bidCirclePositionNTActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#0a0a0a",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#FFE81D",
            flexDirection:"row",
        },
        bidCirclePositionSpadeActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#0a0a0a",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#C000FF",
            flexDirection:"row",
        },
        bidCirclePositionHeartsActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#0a0a0a",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#FF0C3E",
            flexDirection:"row",
        },
        bidCirclePositionClubsActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#0a0a0a",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#79E62B",
            flexDirection:"row",
        },
        bidCirclePositionDiamondActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#0a0a0a",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#04AEFF",
            flexDirection:"row",
        },
        bidCirclePositionBodrAssActive:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#151515",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"#B9B9B9",
            flexDirection:"row",
        },
        bidCirclePositionBodrAss:{
            width:scale(28), 
            height:scale(28), 
            backgroundColor:"#151515",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
            borderWidth:1,
            borderColor:"black",//#676767
            flexDirection:"row",
        },
        bidCirclePosition:{
            width:28, 
            height:28, 
            backgroundColor:"#151515",
            borderRadius:50,
            alignItems:"center",
            justifyContent:"center",
        },
        bidBoxPosition:{
            flexDirection:"column",
            width:"100%", 
            borderWidth:0,
            borderColor:"red",
            position:"absolute",
            alignItems:"center",
            justifyContent:"center",
            zIndex:2,
            
        },
        bidBoxPositionDirectionArea:{
            flexDirection:"column",
            width:"100%", 
            borderWidth:0,
            borderColor:"red",
            alignItems:"center",
            justifyContent:"center",
            position:"absolute",
            zIndex:1,
        },
        bidBoxWidthHeightArea:{
            width:43, 
            height:43,
            backgroundColor:"#151515",
            borderBottomWidth:1,
            borderBottomColor:"#0a0a0a",
            borderRightWidth:1,
            borderRightColor:"#0a0a0a",
            alignItems:"center",
            justifyContent:"center",
        },
        bidBoxWidthHeightAreaBodrActive:{
            width:43, 
            height:43,
            backgroundColor:"#151515",
            borderWidth:1,
            borderColor:"#DBFF00",
            alignItems:"center",
            justifyContent:"center",
        },
        bidBoxRorPosition:{
            width:343,
            flexDirection:"row",
        },
        bidBoxWidthHeightAreaActive:{
            width:43, 
            height:43,
            backgroundColor:"#1C1C1C",
            borderBottomWidth:1,
            borderBottomColor:"#0a0a0a",
            borderRightWidth:1,
            borderRightColor:"#0a0a0a",
            alignItems:"center",
            justifyContent:"center",
        },
        bidBoxSelectedTrickTextbg:{
            width:43, 
            height:43,
            backgroundColor:"#1C1C1C",
            borderBottomWidth:1,
            borderBottomColor:"#0a0a0a",
            borderRightWidth:1,
            borderRightColor:"#0a0a0a",
            alignItems:"center",
            justifyContent:"center",
        },
        bidBoxSelectedTrickText:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"#DBFF00",
        },
        bidBoxWidthHeightTextClorActive:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"#DBFF00",
        },
        bidBoxWidthHeightTextClorNTActive:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"#FFE81D",
        },
        bidBoxWidthHeightTextClor:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"#676767",
        },
        bidBoxDisabledLevel:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"black",
        },
        bidDblTextClordisabled:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"black",
        },
        bidRedblTextClordisabled:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "200",
            color:"black",
        },
    
    
    /*************************************** */
    
        SharePatternCardSpade:{
            flexDirection:"column",
            width:55,
            height:81,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#C000FF",
            alignItems:"center",
            justifyContent:"center",
            backgroundColor:"#151515",
        },
        SharePatternCardHearts:{
            flexDirection:"column",
            width:55,
            height:81,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#FF0C3E",
            alignItems:"center",
            justifyContent:"center",
            backgroundColor:"#151515",
        },
        SharePatternCardDiamond:{
            flexDirection:"column",
            width:55,
            height:81,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#04AEFF",
            alignItems:"center",
            justifyContent:"center",
            backgroundColor:"#151515",
        },
        SharePatternCardClubs:{
            flexDirection:"column",
            width:55,
            height:81,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#79E62B",
            alignItems:"center",
            justifyContent:"center",
            backgroundColor:"#151515",
        },
        PatternText:{
            fontFamily:"Roboto-Light",
            fontSize: 14,
            fontWeight: "400",
            color:"#FFFFFF",
        },
        SharePatternCard:{
            flexDirection:"column",
            width:55,
            height:81,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#DBFF00",
            alignItems:"center",
            justifyContent:"center",
            backgroundColor:"#151515",
        },
        cardDisplayBody:{
            width:61,
            height:91,
            borderRadius:4,
            borderWidth:1,
            borderColor:"#0a0a0a",
            backgroundColor:"#151515"
        },
        cardPosition:{
            width:22,
            marginTop:3,
            marginLeft:4,
            alignItems:"center",
            justifyContent:"center",
         },
         SpadeCardNumber:{
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#C000FF",
         },
         heartsCardNumber:{
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#FF0C3E",
         },
         clubsCardNumber:{
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#79E62B",
         },
         diamondCardNumber:{
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#04AEFF",
         },
    
        ContractText: {
            position:"absolute",
            top:8,
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "400",
            color:"#353535",
        },
        ContractTextHeader: {
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "400",
            color:"#353535",
        },
        ContractMdlText:{
            fontFamily:"Roboto-Light",
            fontSize: 13,
            fontWeight: "400",
            color:"#6D6D6D",
            paddingRight:4,
        },
        ShareText: {
            top:5,
            fontFamily:"Roboto-Thin",
            fontSize: 27,
            fontWeight: "normal",
            color:"#6D6D6D",
          },
          shareHandText: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center",
            paddingTop:3,
          },
          PaternText: {
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#FFFFFF",
            textAlign:"center",
          },
        DirectionText: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#DBFF00",
            textAlign:"center", 
          }, 
         
        UserNameText: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center",
            paddingTop:3,
        },
        LHOName: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center",
            paddingTop:3,
        },
        RHOName: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center",
            paddingTop:3,
        },
        Partner: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center",
            paddingTop:3,
        },
          PaternText: {
            fontFamily:"Roboto-Light",
            fontSize: 16,
            fontWeight: "400",
            color:"#FFFFFF",
            textAlign:"center",
          },
        OpenText: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#DBFF00",
            textAlign:"center", 
            padding:4,
        },
        DirectionTextMySeat: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center", 
          },
        DirectionTextLHO: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center", 
            },
        DirectionTextRHO: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center", 
        },
        DirectionTextPartner: {
            fontFamily:"Roboto-Light",
            fontSize: 11,
            fontWeight: "400",
            color:"#6D6D6D",
            textAlign:"center", 
        }, 
      });