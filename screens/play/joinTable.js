import React from 'react';
import { View, Text, LogBox, Platform } from 'react-native';
const { jid } = require('@xmpp/client');
import { HOOL_CLIENT } from '../../shared/util';
import { SvgXml } from 'react-native-svg';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import DeviceInfo from 'react-native-device-info';
import { Strings } from '../../styles/Strings';
import CrownHost from '../../shared/CrownHost';
import { API_ENV } from '../../redux/api/config/config';
import { s } from 'react-native-size-matters';

export default class JoinTable extends React.Component {
  constructor(props) {
    ///navigation.na
    console.log('Join table contructor called');
    super(props);


    this.state = {
      isTablet: DeviceInfo.isTablet(),
      isValueInitialized: false,
      eyeIndicatorClicked: false,
      southEyeClicked: false,
      northEyeClicked: false,
      eastEyeClicked: false,
      westEyeClicked: false,
      northUser: Strings.open,
      eastUser: Strings.open,
      southUser: Strings.open,
      westUser: Strings.open,
      botsDirection: ['S','N', 'W', 'E']
    };
    LogBox.ignoreAllLogs();
  }

  componentDidMount() {
    console.log('JoinTable componentDidMount called');
    this.loadInitialScreenValues();
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.tableID !== prevProps.tableID) {
      this.updateStateValues({ tableID: this.props.tableID });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
    if ( this.props.chatScreen !== prevProps.chatScreen) {
      this.updateStateValues({ chatScreen: this.props.chatScreen})
    }
    if (this.props.northAvtar !== prevProps.northAvtar) {
      this.updateStateValues({ northAvtar: this.props.northAvtar });
    }
    if (this.props.eastAvtar !== prevProps.eastAvtar) {
      this.updateStateValues({ eastAvtar: this.props.eastAvtar });
    }
    if (this.props.southAvtar !== prevProps.southAvtar) {
      this.updateStateValues({ southAvtar: this.props.southAvtar });
    }
    if (this.props.westAvtar !== prevProps.westAvtar) {
      this.updateStateValues({ westAvtar: this.props.westAvtar });
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.updateStateValues({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.updateStateValues({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.updateStateValues({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.updateStateValues({ westUser: this.props.westUser });
    }
    if (this.props.iskibitzer !== prevProps.iskibitzer) {
      this.updateStateValues({ iskibitzer: this.props.iskibitzer });
    }
    if (this.props.eyeIndicatorClicked !== prevProps.eyeIndicatorClicked) {
      this.updateStateValues({
        eyeIndicatorClicked: this.props.eyeIndicatorClicked,
      });
      if (!this.props.eyeIndicatorClicked) {
        this.setState({
          eastEyeClicked: false,
          northEyeClicked: false,
          westEyeClicked: false,
          southEyeClicked: false,
        });
      }
    }
  }

  componentWillUnmount() {
    console.log('JoinTable closed');
  }

  loadInitialScreenValues() {
    console.log('Loading Table Screen values ' + this.props.tableID);
    this.setState({ ...this.props, isValueInitialized: true }, () => {
      //Initialize Screen after values loaded
      // this.joinTable();
    });

    if(this.props.isFromScreen != undefined && this.props.isFromScreen){
      this.state.botsDirection.forEach((direction, i) =>{
        setTimeout(() => {
          if(direction == 'S'){
            HOOL_CLIENT.joinTable(this.props.tableID, direction);
          }else{
            HOOL_CLIENT.invite(this.props.tableID, API_ENV.botSuffix, direction)
          }
        }, i*2000)
      })
    }
  }

  updateStateValues(stateObj) {
    this.setState(stateObj);
  }

  changeSeat(seat, isHostKibitzer, isHost) {
    if(isHostKibitzer){
      this.props.onAddPlayerClick(seat === Strings.east? Strings.directionEast
        :seat === Strings.north?
        Strings.directionNorth
        :seat === Strings.west?
        Strings.directionWest
        :seat === Strings.south&&
        Strings.directionSouth
        , this.state.tableID)
    }else{
      if (this.state.eastUser === Strings.open && seat === Strings.east) {
        HOOL_CLIENT.joinTable(this.state.tableID, Strings.directionEast);
      } else if (this.state.northUser === Strings.open && seat === Strings.north) {
        HOOL_CLIENT.joinTable(this.state.tableID, Strings.directionNorth);
      } else if (this.state.southUser === Strings.open && seat === Strings.south) {
        HOOL_CLIENT.joinTable(this.state.tableID, Strings.directionSouth);
      } else if (this.state.westUser === Strings.open && seat === Strings.west) {
        HOOL_CLIENT.joinTable(this.state.tableID, Strings.directionWest);
      } else if (seat === 'all') {
        HOOL_CLIENT.joinTable(this.state.tableID, 'kibitzer');
      }else if (this.state.northUser !== Strings.open && seat === Strings.north ||
        this.state.southUser !== Strings.open && seat === Strings.south||
        this.state.eastUser !== Strings.open && seat === Strings.east||
        this.state.westUser !== Strings.open && seat === Strings.west){
          let profileName = ''
          if(seat === Strings.north){
            profileName = this.state.northUser
          }else if(seat === Strings.south){
            profileName = this.state.southUser
          }else if(seat === Strings.east){
            profileName = this.state.eastUser
          }else if(seat === Strings.west){
            profileName = this.state.westUser
          }
          let showProfile = !this.props.showUserProfile
          this.props.updateDeskStateValues({
            showUserProfile: showProfile,
            profileName: profileName,
            profileSubname: '',
            kickoutUserDirection: seat,
            kickedUserisHost: isHost
          })
      }
    }
  }

  _getBlurViewStyle = () => {
    if (this.state.northEyeClicked) {
      return {
        top: 0,
        position: 'absolute',
        alignItems: 'center',
        width: '100%',
      };
    } else if (this.state.southEyeClicked) {
      return {
        position: 'absolute',
        width: '100%',
        alignItems: 'center',
        bottom: 0
      };
    } else if (this.state.eastEyeClicked) {
      return {
        position: 'absolute',
        right: 0,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScale(12),
      };
    } else if (this.state.westEyeClicked) {
      return {
        position: 'absolute',
        left: 0,
        height: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row-reverse'
      };
    }
  };

  _indicatorImageStyle = () => {
    return {
      marginStart: moderateScale(11)
    };
  };

  _AKibitzButton = (direction) => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginStart: moderateScale(5),
          // marginTop: moderateScale(3),
          marginBottom: moderateScale(8)
        }}
      >
        <TouchableOpacity
          disabled={true}
          onPress={() =>
            this.props.updateDeskStateValues(
              { eyeIndicatorClicked: false },
              () => {
                this.setState({
                  eastEyeClicked: false,
                  northEyeClicked: false,
                  westEyeClicked: false,
                  southEyeClicked: false,
                });
              }
            )
          }
        >
          <View style={{...styles.centerStyle}}>
            <Text style={{...styles.textStyle, color: '#353535'}}>{direction == 'E'?'Kibitz East': 'Kibitz West'}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ ...styles.centerStyle }}
          onPress={() => this.changeSeat('all',this.state.iskibitzer)}
        >
          <Text style={{ ...styles.textStyle }}>{'Kibitz All'}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  _northClicked = () => {
    return (
      <View
        style={{
          position: 'absolute',
          top: moderateVerticalScale(5),
          alignItems: 'center',
          flexDirection: 'row'
        }}
      >
        <View
          style={{
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{
                marginStart: moderateScale(4),
                marginEnd: moderateScale(8),
              }}
              onPress={() => {
                this.props.updateDeskStateValues(
                  { eyeIndicatorClicked: false },
                  () => {
                    this.setState({
                      eastEyeClicked: false,
                      northEyeClicked: false,
                      westEyeClicked: false,
                      southEyeClicked: false,
                    });
                  }
                );
              }}
            >
              <SvgXml
                height={scale(41)}
                width={scale(41)}
                xml={
                  this.state.iskibitzer
                    ? svgImages.kibitzerProfile
                    : svgImages.addPlayer
                }
              />
            </TouchableOpacity>
            <TouchableOpacity
              disabled={true}
              onPress={() =>
                this.props.updateDeskStateValues(
                  { eyeIndicatorClicked: false },
                  () => {
                    this.setState({
                      eastEyeClicked: false,
                      northEyeClicked: false,
                      westEyeClicked: false,
                      southEyeClicked: false,
                    });
                  }
                )
              }
            >
              <View style={styles.centerStyle}>
                <Text style={{...styles.textStyle, color: '#353535'}}>{'Kibitz North'}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.centerStyle}
              onPress={() => this.changeSeat('all',this.state.iskibitzer)}
            >
              <View style={styles.centerView}>
                <Text style={styles.textStyle}>{'Kibitz All'}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  _southClicked = () => {
    return (
      <View
        style={{
          position: 'absolute',
          bottom: moderateScale(0),
          alignItems: 'center',
          flexDirection: 'row'
        }}
      >
        <View
          style={{
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{
                marginStart: moderateScale(4),
                marginEnd: moderateScale(8),
                marginTop: moderateVerticalScale(2)
              }}
              onPress={() => {
                this.props.updateDeskStateValues(
                  { eyeIndicatorClicked: false },
                  () => {
                    this.setState({
                      eastEyeClicked: false,
                      northEyeClicked: false,
                      westEyeClicked: false,
                      southEyeClicked: false,
                    });
                  }
                );
              }}
            >
              <SvgXml
                height={scale(41)}
                width={scale(41)}
                xml={
                  this.state.iskibitzer
                    ? svgImages.kibitzerProfile
                    : svgImages.addPlayer
                }
              />
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', paddingBottom: moderateVerticalScale(5) }}>
              <TouchableOpacity
              disabled={true}
                onPress={() =>
                  this.props.updateDeskStateValues(
                    { eyeIndicatorClicked: false },
                    () => {
                      this.setState({
                        eastEyeClicked: false,
                        northEyeClicked: false,
                        westEyeClicked: false,
                        southEyeClicked: false,
                      });
                    }
                  )
                }
              >
                <View style={styles.centerStyle}>
                  <Text style={{...styles.textStyle, color: '#353535'}}>{'Kibitz South'}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.centerStyle}
                onPress={() => this.changeSeat('all',this.state.iskibitzer)}
              >
                <View style={styles.centerView}>
                  <Text style={styles.textStyle}>{'Kibitz All'}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  _westClicked = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: moderateVerticalScale(18),
          marginStart: (!this.state.isTablet) ? scale(53) : scale(49)
        }}
      >
        <TouchableOpacity
          onPress={() =>
            this.props.updateDeskStateValues(
              { eyeIndicatorClicked: false },
              () => {
                this.setState({
                  eastEyeClicked: false,
                  northEyeClicked: false,
                  westEyeClicked: false,
                  southEyeClicked: false
                });
              }
            )
          }
          style={{ marginEnd: moderateScale(5) }}
        >
          <SvgXml
            height={scale(41)}
            width={scale(41)}
            xml={
              this.state.iskibitzer
                ? svgImages.kibitzerProfile
                : svgImages.addPlayer
            }
          />
        </TouchableOpacity>
        {this._AKibitzButton('W')}
      </View>
    );
  };

  render() {
    return this.state.isValueInitialized ? (
      <View style={{ ...styles.container }}>
        <View
          style={{
            position: 'absolute',
            top: moderateScale(5),
            alignItems: 'center',
          }}
        >
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <View
              style={{ flexDirection: 'row'
            }}
            >
              <TouchableOpacity
                disabled={this.props.fromPlayBot}
                onPress={() => this.changeSeat(Strings.north, this.state.iskibitzer, this.props.hostName == this.state.northUser)}
              >
                <SvgXml height={scale(41)} width={scale(41)} xml={
                  this.state.iskibitzer && this.state.northUser === Strings.open 
                    ? svgImages.addPlayer :
                    this.state.northUser === Strings.open 
                    ? this.state.northAvtar 
                    : this.state.northAvtar 
                } />
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginStart: moderateScale(7) }}
                onPress={() => {
                  this.props.onAddPlayerClick(Strings.directionNorth, this.state.tableID)
                }}
              >
                <SvgXml
                  height={scale(41)}
                  width={scale(41)}
                  xml={
                    this.state.iskibitzer
                      ? svgImages.kibitzerProfile
                      : svgImages.addPlayer
                  }
                />
              </TouchableOpacity>
            </View>
            <View style={{marginBottom: moderateVerticalScale(5)}}>
            {
              <TouchableOpacity
                    disabled={!this.state.chatScreen.isNorthSentMsg}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
              > 
                <View style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  marginTop: moderateScale(3),
                  width:scale(86),
                  justifyContent:'center',alignItems:'center', alignSelf:'center'
                }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      userName={this.state.northUser}
                      textStyle={this.state.chatScreen.isNorthSentMsg ? {...styles.OpenText}: styles.OpenText}
                      reduceChar={this.state.chatScreen.isNorthSentMsg ?  false : true }
                    />
                </View>
              </TouchableOpacity> 
            }
            </View>
          </View>  
        </View>

        <View
          style={{
            position: 'absolute',
            left: moderateScale(5),
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center', 
              justifyContent: 'center',
              alignSelf:'center',
            }}
          >
            {
              <TouchableOpacity
              disabled={!this.state.chatScreen.isWestSentMsg}
              onPress={() => {
                 this.props.showTableMessages();
              }}
              >
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' ,width:scale(86)}}>
                   <CrownHost
                    hostName={this.props.hostName}
                    userName={this.state.westUser}
                    textStyle={styles.OpenText}
                    reduceChar={this.state.chatScreen.isWestSentMsg ? false : true}
                   />
                </View>
              </TouchableOpacity>
            }

            <View style={{ flexDirection: 'row', marginTop: moderateVerticalScale(5)}}>
              <TouchableOpacity
                disabled={this.props.fromPlayBot}
                onPress={() => this.changeSeat(Strings.west,this.state.iskibitzer, this.props.hostName == this.state.westUser)}
              >
                <SvgXml
                  height={scale(41)} width={scale(41)}
                  xml={
                    this.state.iskibitzer && this.state.westUser === Strings.open 
                    ? svgImages.addPlayer :
                    this.state.westUser === Strings.open 
                    ? this.state.westAvtar 
                    : this.state.westAvtar }
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginStart: moderateVerticalScale(7) }}
                onPress={() => {
                  this.props.onAddPlayerClick(Strings.directionWest, this.state.tableID)
                }}
              >
                <SvgXml
                  height={scale(41)}
                  width={scale(41)}
                  xml={
                    this.state.iskibitzer
                      ? svgImages.kibitzerProfile
                      : svgImages.addPlayer
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'column',
            alignSelf: 'center',
            justifyContent: 'center',
            position: 'absolute',
          }}
        >
          <Text
            style={{
              ...styles.DirectionText,
              color: this.state.northUser === Strings.open ? '#DBFF00' : '#6D6D6D',
              alignSelf: 'center',
              textAlign: 'center',
              marginBottom: moderateScale(3.89),
            }}
          >
            {Strings.directionNorth}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <View style={{marginRight: moderateScale(2.59)}}>
              <Text
                style={{
                  ...
                  styles.DirectionText,
                  color: this.state.westUser === Strings.open ? '#DBFF00' : '#6D6D6D',
                  textAlign: 'left',
                }
                }
              >
                {Strings.directionWest}
              </Text>
            </View>
            <View style={{ width: scale(27), height: scale(27), alignItems: 'center', justifyContent: 'center', }}>
              <SvgXml
                height={scale(21)} width={scale(21)}
                style={{margin: moderateScale(3)}}
                xml={svgImages.centerCircle}
              />
            </View>
            <View style={{marginLeft: moderateScale(5)}}>
              <Text
                style={{
                  ...
                  styles.DirectionText,

                  color: this.state.eastUser === Strings.open ? '#DBFF00' : '#6D6D6D',
                  letterSpacing: 0.1,
                  textAlign: 'left',
                }
                }
              >
                {Strings.directionEast}
              </Text>
            </View>
          </View>
          <Text
            style={[
              styles.DirectionText,
              {
                color: this.state.southUser === Strings.open ? '#DBFF00' : '#6D6D6D',
                letterSpacing: 0.4,
                marginTop: moderateScale(3),
              }
            ]}
          >
            {Strings.directionSouth}
          </Text>
        </View>

        <View
          style={{
            right: moderateScale(5),
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute'
          }}
        >
          <View
            style={{
              flexDirection: 'column',
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            {
              <TouchableOpacity
              style={{flexDirection:'row'}}
              disabled={!this.state.chatScreen.isEastSentMsg}
              onPress={() => {
                 this.props.showTableMessages();
              }}
              >
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' ,width:scale(86)}}>
                  <CrownHost
                    hostName={this.props.hostName}
                    userName={this.state.eastUser}
                    textStyle={styles.OpenText}
                    reduceChar={this.state.chatScreen.isEastSentMsg ? false : true}
                  />
                </View>
              </TouchableOpacity> 
            }
            <View
              style={{
                flexDirection: 'row',
                marginStart: moderateScale(5),
                marginTop: moderateScale(5),
              }}
            >
              <TouchableOpacity
                disabled={this.props.fromPlayBot}
                onPress={() => this.changeSeat(Strings.east,this.state.iskibitzer,  this.props.hostName == this.state.eastUser)}
              >
                <SvgXml height={scale(41)} width={scale(41)} xml={
                    this.state.iskibitzer && this.state.eastUser === Strings.open 
                    ? svgImages.addPlayer :
                    this.state.eastUser === Strings.open 
                    ? this.state.eastAvtar 
                    : this.state.eastAvtar 
                  } />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.onAddPlayerClick(Strings.directionEast, this.state.tableID)
                }}
              >
                <SvgXml
                  height={scale(41)} width={scale(41)}
                  xml={
                    this.state.iskibitzer
                      ? svgImages.kibitzerProfile
                      : svgImages.addPlayer
                  }
                  style={{ marginStart: moderateScale(7) }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            position: 'absolute',
            bottom: moderateScale(5),
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent:'center'
          }}
        >
          <TouchableOpacity
                disabled={!this.state.chatScreen.isSouthSentMsg}
                onPress={() => {
                  this.props.showTableMessages();
                }}
              >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center' , marginBottom: moderateVerticalScale(5) ,width:scale(86)}}>
              {
                <CrownHost
                  hostName={this.props.hostName}
                  userName={this.state.southUser}
                  textStyle={styles.OpenText}
                  reduceChar={this.state.chatScreen.isSouthSentMsg ? false : true}
                />
              }
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', marginBottom: moderateScale(1), }}>
            <TouchableOpacity
              onPress={() => this.changeSeat(Strings.south,this.state.iskibitzer, this.props.hostName == this.state.southUser)}
            >
              <SvgXml height={scale(41)} width={scale(41)} xml={
                   this.state.iskibitzer && this.state.southUser === Strings.open 
                   ? svgImages.addPlayer :
                   this.state.southUser === Strings.open 
                   ? this.state.southAvtar 
                   : this.state.southAvtar 
                    } />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.onAddPlayerClick(Strings.directionSouth, this.state.tableID)
              }}
            >
              <SvgXml
                height={scale(41)} width={scale(41)}
                xml={
                  this.state.iskibitzer
                    ? svgImages.kibitzerProfile
                    : svgImages.addPlayer}
                style={{ marginStart: moderateScale(8) }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    ) : null;
  }
}

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  clmText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingLeft: '10@ms'
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(12),
    color: '#DBFF00',
    textAlign: 'center',
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    color: '#DBFF00',
    textAlign: 'center',
  },
  blurView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  centerStyle: {
    width: '99@vs',
    height: '43@s',
    borderColor: '#1B1B25',
    borderWidth: '0.5@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textStyle: {
    color: '#FFFFFF',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    textAlign: 'center',
    letterSpacing: 0.4
  }
});
