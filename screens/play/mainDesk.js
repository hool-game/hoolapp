import React, { Component } from 'react';
import SafeArea, { type, SafeAreaInsets } from 'react-native-safe-area';
import {
  BackHandler,
  AppRegistry,
  View,
  Text,
  LogBox,
  Image,
  Dimensions,
  StatusBar,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform
} from 'react-native';
const { jid } = require('@xmpp/client');
import { nextSide, getUserNameBasedOnHost, HOOL_CLIENT, showInviteBotOption, showOptionWhenRobotInTable } from '../../shared/util';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SCREENS } from '../../constants/screens';
import JoinTable from './joinTable';
import ShareInfo from './shareInfo';
import BidInfo from './biddingScreen';
import PlayScreen from './playScreen';
import ScoreBoard from './scoreBoard';
import History from './biddingHistory';
import Svg, { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import Spinner from 'react-native-loading-spinner-overlay';
import SettingsDisabled from '../../assets/Svg/icon_Settings_Disable.svg';
import Flag from '../../assets/Svg/icon_flag.svg';
import HistoryMenu from '../../assets/Svg/icon_history.svg';
import ChatScreen from '../chat/chat';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale,
  moderateVerticalScale
} from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import Toast from 'react-native-toast-message';
import { SafeAreaView } from 'react-native-safe-area-context';

const SIDES = new Map([
  ['N', 'North'],
  ['E', 'East'],
  ['S', 'South'],
  ['W', 'West']
]);
import KeepAwake from 'react-native-keep-awake';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { JoinTableConstants } from '../../redux/actions/JoinaTableActions';
import { Strings } from '../../styles/Strings';
import CheatSheet from '../../shared/cheatsheet';
import UserProfile  from '../chat/UserProfile'
import { HOOL_EVENTS } from '../../constants/hoolEvents';
import EncryptedStorage from 'react-native-encrypted-storage';
import { API_ENV } from '../../redux/api/config/config';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import { CommonActions, ThemeProvider } from '@react-navigation/native';

var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height;
var isTablet = DeviceInfo.isTablet()
var bottomHeight = (Platform.OS == 'ios' && isTablet) ? scale(139) : scale(134)
var totalHeight = 0
var isFromScreen = false
console.log(ScreenWidth, ScreenHeight);
var hostChange = ''
//For landscape 44* 2
// var safeAreaWidth = 44 * 2;
// var safeAreaHeight = 24;
var safeAreaHeight = 0;
var MenuBodyWidth = scale(90);
var PlayBodyWidth = ScreenWidth - MenuBodyWidth;

const cardDimension = 41;
var cardWidth = scale(cardDimension);
class MainDesk extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...initialState };
    this.updateDeskStateValues = this.updateDeskStateValues.bind(this);
    this.updateBiddingBox = this.updateBiddingBox.bind(this);
    this.showTableMessages = this.showTableMessages.bind(this);
    this.shareInfoComponent = React.createRef();
    this.bidComponent = React.createRef();
    this.playComponent = React.createRef();
    this.scoreBoardComponent = React.createRef();
    this.presenceInterval = null;
    
  }

  componentDidMount() {
    console.log('Main Desk Loaded');
    isFromScreen = false
    
    KeepAwake.activate();
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );

    SafeArea.getSafeAreaInsetsForRootView().then(result => {
      // safeAreaBottom = result.safeAreaInsets.bottom;
      if(Platform.OS == 'android'){
          bottomHeight = bottomHeight+result.safeAreaInsets.bottom - ((StatusBar.currentHeight*2))
      }else{
        bottomHeight = bottomHeight+result.safeAreaInsets.bottom - verticalScale(15)
      }
      totalHeight = ScreenHeight - bottomHeight
      PlayBodyWidth =
        ScreenWidth -
        (MenuBodyWidth +
          result.safeAreaInsets.left +
          result.safeAreaInsets.right);
      safeAreaHeight = result.safeAreaInsets.bottom;
      // { safeAreaInsets: { top: 44, left: 0, bottom: 34, right: 0 } }
      this.loadInitialScreen();
    });
    var userName = ''
    this.setState({username: userName}, async () => {
      userName = await EncryptedStorage.getItem('username')
      this.setState({ username:  userName});
    });
  }



  componentWillUnmount() {
    this.setState({ ...initialState });
    this.clearListeners();
    // this.leaveTable();
    KeepAwake.deactivate();
  }

  cancelRequest = (direction) => {
    this.props.removePlayerOnTable({
      removePlayer: true,
      addPlayer: true,
      removePlayerDirection: direction
    })
  }

  removePlayerFromTable = () => {
    this.props.cancelDirectionRequest({
      direction: this.props.removePlayerDirection
    })
    this.props.cancelInvitedRequest({
      direction: this.props.removePlayerDirection
    })
  }

  onAddPlayerClick = (direction) => {
    this.props.addPlayers({
      addPlayer: true,
      direction: direction
    })
  }

  getDirectionValue = () =>{
    if(this.props.currentDirection == Strings.directionEast){
      return Strings.east
    }else if(this.props.currentDirection == Strings.directionNorth){
      return Strings.north
    }else if(this.props.currentDirection == Strings.directionSouth){
      return Strings.south
    }else if(this.props.currentDirection == Strings.directionWest){
      return Strings.west
    }
  }

  loadProfileOrVaccatePlayerScreen = (seat) => {
    if( this.props.hostName == this.state.gamestate.mySeat){
      this.props.setHomeUser({
        homeUser: true,
        host: seat == host,
      })
    }
    //this.state.showUserProfile = true
  }

  clearListeners() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
    this.emitterTableJoinError.removeAllListeners();
    //this.emitterNotificationList.removeAllListeners();
    this.emitterTableExit.removeAllListeners();
    this.emitterTableLeave.removeAllListeners();
    this.emitterTablePresence.removeAllListeners();
    this.emitterStatusUpdate.removeAllListeners();
    this.emitterInfoShare.removeAllListeners();
    this.emitterBid.removeAllListeners();
    this.emitterCardPlay.removeAllListeners();
    this.emitterCardPlayError.removeAllListeners();
    this.emitterUndo.removeAllListeners();
    this.emitterUndoError.removeAllListeners();
    this.emitterClaim.removeAllListeners();
    this.emitterClaimError.removeAllListeners();
    this.emitterChangeDeal.removeAllListeners();
    clearInterval(this.presenceInterval);
    if (this.currentPresence) {
      clearInterval(this.currentPresence);
    }
  }

  handleCurrentBackButton = () => {
    Alert.alert(
      'Exit Table',
      'Do you want to exit the table?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'OK',
          onPress: () => {
            this.leaveTable();
          }
        }
      ],
      {
        cancelable: false
      }
    );
    return true;
  };

  navigateToScreen(screenName, params) {
    this.clearListeners();
    this.props.navigation.navigate(screenName, params);
  }

  loadInitialScreen() {
    const { route } = this.props;
    const currentScreen = route.params.loadScreen;
    const tableID = route.params.tableID;
    const role = route.params.role;
    isFromScreen = route.params.isFromScreen;
    const fromPlayBot = route.params.fromPlayBot;
    console.log('Loading Initital Screen -> ' + currentScreen);
    let gamestate = {};
    gamestate.hands = new Map();
    gamestate.info = new Map();
    console.log('enter table screen');
    this.setState({ tableID, role, gamestate,fromPlayBot }, async () => {
      await this.initiateEventEmitter();
      if (currentScreen == SCREENS.JOIN_TABLE) {
        await this.joinTable();
        this.setState({ currentScreen });
      }
    });
  }

  removeStorage = async () => {
    const tableID = await AsyncStorage.getItem('cheatsheetTableID')
    const value = await AsyncStorage.getItem('values')
    if( tableID !== undefined &&  value !== undefined ){
     await AsyncStorage.multiRemove(['cheatsheetTableID','values'])
    }
  }

  redirectToShareInfoScreen(gamestate) {
    //Clearing the cheatSheet data
    this.removeStorage();
    const interval = setInterval(() => {
      let prevGamestate = gamestate
      if( ! this.state.iskibitzer ){
      for(const value of prevGamestate.hands){
        if(value[0] !== gamestate.mySeat){
          prevGamestate.hands.delete(value[0])
        }
      }}
      let infoScreen = this.state.infoScreen;
      infoScreen.northSharedType1 = '';
      infoScreen.eastSharedType1 = '';
      infoScreen.southSharedType1 = '';
      infoScreen.westSharedType1 = '';
      infoScreen.northSharedType2 = '';
      infoScreen.eastSharedType2 = '';
      infoScreen.southSharedType2 = '';
      infoScreen.westSharedType2 = '';
      infoScreen.northSharedValue1 = '';
      infoScreen.eastSharedValue1 = '';
      infoScreen.southSharedValue1 = '';
      infoScreen.westSharedValue1 = '';
      infoScreen.northSharedValue2 = '';
      infoScreen.eastSharedValue2 = '';
      infoScreen.southSharedValue2 = '';
      infoScreen.westSharedValue2 = '';
      infoScreen.infoHCPClicked = false;
      infoScreen.infoPatternClicked = false;
      infoScreen.infoSuitClicked = false;
      infoScreen.numCardsShared = 0;
      infoScreen.numSpShared = false;
      infoScreen.numHtShared = false;
      infoScreen.numDmShared = false;
      infoScreen.numCbShared = false;
      let bidInfoScreen = { ...initialBiddingInfo };
      let playScreen = { ...inititalPlayInfo };
      playScreen.TotalCardWidthNorth = PlayBodyWidth;
      playScreen.TotalCardWidthSouth = PlayBodyWidth;
      //Resetting Game after Scoreboard
      this.setState((prevState) => (
        { roundCount: prevState.roundCount++ }
      ))
      this.setState({
        gamestate: prevGamestate,
        infoScreen,
        bidInfoScreen,
        playScreen,
        TrickScoreNS: '00',
        TrickScoreEW: '00',
        remainingTricks: 13,
        claimType: 'CLAIM',
        claimRequested: false,
        showSubMenuPopup: false,
        showCheatSheet:false,
        biddings: { N: [], E: [], S: [], W: [] },
        bidInfoList: [],
        lastBidRound: 1,
        tricks: { N: [], E: [], S: [], W: [] },
        currentScreen: SCREENS.SHARE_INFO
      },()=>{
        this.resetUndo();
      });
      clearInterval(interval);
      if (this.currentPresence) {
        clearInterval(this.currentPresence);
      }
    }, 2000);
  }
  async initiateEventEmitter() {
    console.log("emitterTableJoinError");
    this.emitterTableJoinError = HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_JOIN_ERROR, e => {
      Alert.alert(
        `Failed to join table: ${e.tag || 'cancel'} (${
          e.type || 'unknown-error'
        }): ${e.text || 'unknown error'}`
      );
    });

    HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_PRESENCE, async (e) => {
      console.warn("Event TABLE_PRESENCE received.")
    })
    HOOL_CLIENT.addListener(HOOL_EVENTS.ROSTER_SUBSCRIBE,async (e) => {
      console.log('E',e)
      let rosterList = await HOOL_CLIENT.xmppGetRoster()
      let preApproveSubscription = false
      rosterList.forEach( (element) => {
        if(element.jid === e.user){
          if( element.subscription === "both"){
            preApproveSubscription = true
          }else if( element.subscription === "none"){
            preApproveSubscription = false
          }else if( element.subscription === "from"){
            preApproveSubscription = false
          }else if( element.subscription === "to"){
            HOOL_CLIENT.rosterSubscribed(e.user)
            preApproveSubscription = true
          }
        }
      })
      if(!preApproveSubscription){
        let user = jid(e.user);
        let notificationCount
        let data = await AsyncStorage.getItem('notificationList')
        let List = JSON.parse(data)
        let flag = true
        if(List !== null){
          List.forEach( element => {
            if( element.name === user._local)
            {
              flag = false
              return
            }
          })
        }else{
          await AsyncStorage.setItem('notificationList',JSON.stringify([]))
        }
        if( flag ){
        notificationCount = await AsyncStorage.getItem('notificationCount')
        let count = JSON.parse(notificationCount)
        if(count === null){
          count = 1
        }else{
          count = count + 1
        }
        await AsyncStorage.setItem('notificationCount',JSON.stringify(count))
        List = JSON.parse(await AsyncStorage.getItem('notificationList'))
        let notificationItem = {...notificationObject}
        notificationItem.name = user._local
        notificationItem.jid = e.user
        notificationItem.timeOfRequest = new Date()
        notificationItem._id = uuidv4();
        List.unshift(notificationItem)
        await AsyncStorage.setItem('notificationList',JSON.stringify(List))
        
        // emit([
        //   { type: ChatConstants.NOTIFICATION_LIST , params: {newItem: notificationItem}},
        //   { type: ChatConstants.SET_NOTIFICATION_COUNT , params: {notificationCount: count}}
        //   ])
        }
      }else{
        // emit([
        //   { type: ChatConstants.FETCH_USERS_LIST}
        // ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CONTACT_ONLINE ,async (e) => {
      console.log(`Online User ${e.user}`)
      const state =  store.getState();
      let user = jid(e.user)

      let list = [...state.chatActions.friendsList]
      
      let onlineUser = list.find( element => element.name === user._local)
      // Check The friendsList Length . if its empty we can add it to the empty array and then we process it in the fetch friendsList
      if( onlineUser !== undefined){
      list.forEach((element,idx) => {
       if(element.name === user._local)
       list[idx].isOnline = true
      })

      // emit([{ type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: list}},]) 
      }else{
        let waitingList = [...state.chatActions.waitingList]
        waitingList.push({ name: jid(e.user)})
        
        // console.log("Waiting List :",waitingList)
        // emit([
        //   { type: ChatConstants.UPDATE_WAITING_LIST , params: {waitingList: waitingList}}
        // ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CONTACT_OFFLINE, (e) => {
      console.info("CONTACT_OFFLINE fired for:")
      const state =  store.getState();
      let user = jid(e.user)
 
      let list = [...state.chatActions.friendsList]
      let offlineUser = list.find( element => element.name === user._local)
 
      if( offlineUser !== undefined){
        list.forEach((element,idx) => {
          if(element.name === user._local)
          list[idx].isOnline = false
         })
   
        //  emit(
        //    [
        //    { type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: list}},
        //    ]) 
      }else{
        let waitingList = [...state.chatActions.waitingList]
        waitingList.push({ name: jid(e.user)})
        
        // console.log("Waiting List :",waitingList)
        // emit([
        //   { type: ChatConstants.UPDATE_WAITING_LIST , params: {waitingList: waitingList}}
        // ])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.CHAT,async (e) => {
        let user = jid(e.from)._local
        const state =  store.getState();
        
        let friendsList = [...state.chatActions.friendsList]
        let count = state.chatActions.friendsNotificationCount
        
        const chatObject = {
          _id: uuidv4(),
          text: e.text,
          createdAt: new Date(),
          user: {
            _id: uuidv4(),
            name: user
          }
        }
        let oldMessages = [...state.chatActions.messages[user]]
        if( oldMessages.length >= 500){
          oldMessages.pop()
        }
        oldMessages.unshift(chatObject)
        await AsyncStorage.setItem(user,JSON.stringify(oldMessages))
    
        let messages = {...state.chatActions.messages}
        messages[user] = oldMessages
        if(state.chatActions.lastClickedUser !== user){
          friendsList.forEach( (element,idx) => {
            if(element.name === user){
              friendsList[idx].unreadMessages =  friendsList[idx].unreadMessages + 1
              friendsList[idx].isNewMessages = true
              count = count + 1
            }
        })
        Toast.show({
          type: 'info',
          text1: `message from ${user}`,
          position: 'bottom',
          bottomOffset: 50,
          text2: e.text
        });
        }
        
        // emit([
        //    {type: ChatConstants.UPDATE_CHATLIST, params: {messages: messages }},
        //    {type: ChatConstants.FETCH_FRIENDS_LIST_SUCCESS , params: {data: friendsList }},
        //    {type: ChatConstants.SET_CHAT_NOTIFICATION_COUNT , params: {friendsNotificationCount: count}}
        // ])
    })

    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_SUBSCRIBED , (e) => {
      console.log(`${e.user} Accepted Your FriendRequest`)
      //emit([{ type: ChatConstants.FETCH_FRIENDS_LIST }])
    })

    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_UNSUBSCRIBE ,async (e) => {
      console.log(`${e.user} has no longer following your presence`)
      HOOL_CLIENT.rosterUnsubscribed(e.user)
      let rosterList = await HOOL_CLIENT.xmppGetRoster() 
      let oldUser = false
      rosterList.forEach( element => {
        if( element.jid === e.user){
          oldUser = true
        }
      })
      if( oldUser ){
        HOOL_CLIENT.rosterRemove(e.user)
        //emit([{ type: ChatConstants.FETCH_FRIENDS_LIST }])
      }
    })
    HOOL_CLIENT.on(HOOL_EVENTS.ROSTER_UNSUBSCRIBED, async (e) => {
      console.log(`${e.user} has removed You from the friendsList`)
    })
    HOOL_CLIENT.on(HOOL_EVENTS.GAME_INVITED, async (e) => {
      // let gameCall = true
      console.log(`${e.invitor} called you to play `)
      console.info(`I have been invited by ${e.invitor} to play on some table `)
      // let user = jid(e.user);
      // let notificationCount
      // let data = await AsyncStorage.getItem('notificationList')
      // let GameList = JSON.parse(data)
      // // let flag = true
      // if(GameList !== null){
      //   GameList.forEach( element => {
      //     if( element.name === user._local)
      //     {
      //       flag = false
      //       return
      //     }
      //   })
      // }else{
      //   await AsyncStorage.setItem('notificationList',JSON.stringify([]))
      // }      
      // notificationCount = await AsyncStorage.getItem('notificationCount')
      // let count = JSON.parse(notificationCount)
      // if(count === null){
      //   count = 1
      // }else{
      //   count = count + 1
      // }
      // await AsyncStorage.setItem('notificationCount',JSON.stringify(count))
      // GameList = JSON.parse(await AsyncStorage.getItem('notificationList'))
      // let notificationItem = {...notificationObject}
      // notificationItem.name = user._local
      // notificationItem.jid = e.user
      // notificationItem.isGameInvite = gameCall
      // notificationItem.timeOfRequest = new Date()
      // notificationItem._id = uuidv4();
      // GameList.unshift(notificationItem)
      // await AsyncStorage.setItem('notificationList',JSON.stringify(GameList))
      
      // emit([
      //   { type: ChatConstants.NOTIFICATION_LIST , params: {newItem: notificationItem}},
      //   { type: ChatConstants.SET_NOTIFICATION_COUNT , params: {notificationCount: count}}
      // ])
      
    })
    
    HOOL_CLIENT.on(HOOL_EVENTS.GAME_DECLINED, async (e) => {
      console.log(`${e.user} is declined `)
    })

    this.emitterTableLeave = HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_LEAVE, e => {
      var jidlocal = jid(e.user);
      let message = `${jidlocal._resource} has disconnected from table`;
      console.log("-----------TABLE_LEAVE", jidlocal);
      if ((this.state.northUser === jidlocal._resource || this.state.eastUser === jidlocal._resource ||
        this.state.southUser === jidlocal._resource || this.state.westUser === jidlocal._resource)
        && !this.state.iskibitzer
      ) {
        Toast.show({
          type: 'info',
          text1: 'Player Disconneted',
          position: 'bottom',
          bottomOffset: 50,
          text2: message
        });
      }
      let gamestate = this.state.gamestate;
      if (this.state.northUser === jidlocal._resource) {
        this.setState({
          northUser: Strings.open,
          northAvtar: svgImages.joinSymbol
        });
        gamestate.northUser = '';
      } else if (this.state.eastUser === jidlocal._resource) {
        this.setState({
          eastUser: Strings.open,
          eastAvtar: svgImages.joinSymbol
        });
        gamestate.eastUser = '';
      } else if (this.state.southUser === jidlocal._resource) {
        this.setState({
          southUser: Strings.open,
          southAvtar: svgImages.joinSymbol
        });
        gamestate.southUser = '';
      } else if (this.state.westUser === jidlocal._resource) {
        this.setState({
          westUser: Strings.open,
          westAvtar: svgImages.joinSymbol
        });
        gamestate.westUser = '';
      }
      gamestate = this.updateGameStateSeat(gamestate);
      this.setState({ gamestate });
    });

    this.emitterTableExit = HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_EXIT, e => {
      var jidlocal = jid(e.user);
      let message = `${jidlocal._resource} has left table`;
      console.log("-----------TABLE_EXIT", jidlocal);
      if ((this.state.northUser === jidlocal._resource || this.state.eastUser === jidlocal._resource ||
        this.state.southUser === jidlocal._resource || this.state.westUser === jidlocal._resource)
        && !this.state.iskibitzer
      ) {
        Toast.show({
          type: 'error',
          text1: 'Player Exit',
          position: 'bottom',
          bottomOffset: 50,
          text2: message
        });
      }
    
      let gamestate = this.state.gamestate;
      if (this.state.northUser === jidlocal._resource) {
        this.setState({
          northUser: Strings.open,
          northAvtar: svgImages.joinSymbol
        });
        gamestate.northUser = '';
      } else if (this.state.eastUser === jidlocal._resource) {
        this.setState({
          eastUser: Strings.open,
          eastAvtar: svgImages.joinSymbol
        });
        gamestate.eastUser = '';
      } else if (this.state.southUser === jidlocal._resource) {
        this.setState({
          southUser: Strings.open,
          southAvtar: svgImages.joinSymbol
        });
        gamestate.southUser = '';
      } else if (this.state.westUser === jidlocal._resource) {
        this.setState({
          westUser: Strings.open,
          westAvtar: svgImages.joinSymbol
        });
        gamestate.westUser = '';
      }
      gamestate = this.updateGameStateSeat(gamestate);
      this.setState({ gamestate });
    });

    this.emitterTablePresence = HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_PRESENCE, e => {
      let role = !e.role || e.role == 'none' ? 'kibitzer' : e.role;
      let affiliation = e.affiliation;
      this.state.affiliation = e.affiliation
      let message = `${e.user} has joined ${e.table} as ${role}`;
      console.log("---------TABLE_PRESENCE------------", e);
      var jidlocal = jid(e.user);
      if (role === 'N') {
        if (this.state.eastUser === jidlocal._local) {
          this.setState({
            eastUser: Strings.open,
            eastAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.eastUser = '';
        } else if (this.state.southUser === jidlocal._local) {
          this.setState({
            southUser: Strings.open,
            southAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.southUser = '';
        } else if (this.state.westUser === jidlocal._local) {
          this.setState({
            westUser: Strings.open,
            westAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.westUser = '';
        }
        this.setState({ northUser: jidlocal._local });
        this.setState({
          northAvtar: svgImages.profile1
        });
        this.state.gamestate.northUser = jidlocal._local;
        if (affiliation === 'host') this.state.gamestate.host = 'N';
      } else if (role === 'E') {
        if (this.state.northUser === jidlocal._local) {
          this.setState({
            northUser: Strings.open,
            northAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.northUser = '';
        } else if (this.state.southUser === jidlocal._local) {
          this.setState({
            southUser: Strings.open,
            southAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.southUser = '';
        } else if (this.state.westUser === jidlocal._local) {
          this.setState({
            westUser: Strings.open,
            westAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.westUser = '';
        }
        this.setState({ eastUser: jidlocal._local });
        this.setState({
          eastAvtar: svgImages.profile2
        });
        this.state.gamestate.eastUser = jidlocal._local;
        if (affiliation === 'host') this.state.gamestate.host = 'E';
      } else if (role === 'S') {
        if (this.state.northUser === jidlocal._local) {
          this.setState({
            northUser: Strings.open,
            northAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.northUser = '';
        } else if (this.state.eastUser === jidlocal._local) {
          this.setState({
            eastUser: Strings.open,
            eastAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.eastUser = '';
        } else if (this.state.westUser === jidlocal._local) {
          this.setState({
            westUser: Strings.open,
            westAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.westUser = '';
        }
        this.setState({ southUser: jidlocal._local });
        this.setState({
          southAvtar: svgImages.profile1
        });
        this.state.gamestate.southUser = jidlocal._local;
        if (affiliation === 'host') this.state.gamestate.host = 'S';
      } else if (role === 'W') {
        if (this.state.northUser === jidlocal._local) {
          this.setState({
            northUser: Strings.open,
            northAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.northUser = '';
        } else if (this.state.eastUser === jidlocal._local) {
          this.setState({
            eastUser: Strings.open,
            eastAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.eastUser = '';
        } else if (this.state.southUser === jidlocal._local) {
          this.setState({
            southUser: Strings.open,
            southAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.southUser = '';
        }
        this.setState({ westUser: jidlocal._local });
        this.setState({
          westAvtar: svgImages.profile2
        });
        this.state.gamestate.westUser = jidlocal._local;
        if (affiliation === 'host') this.state.gamestate.host = 'W';
      } else if (role === 'kibitzer') {
        if (this.state.northUser === jidlocal._local) {
          this.setState({
            northUser: Strings.open,
            northAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.northUser = '';
        } else if (this.state.eastUser === jidlocal._local) {
          this.setState({
            eastUser: Strings.open,
            eastAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.eastUser = '';
        } else if (this.state.southUser === jidlocal._local) {
          this.setState({
            southUser: Strings.open,
            southAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.southUser = '';
        } else if (this.state.westUser === jidlocal._local) {
          this.setState({
            westUser: Strings.open,
            westAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.westUser = '';
        }
        this.setState({ eyeIndicatorClicked: false });
        if (affiliation === 'host') this.state.gamestate.host = 'kibitzer';
      } else if (role === 'rover') {
        if (this.state.northUser === jidlocal._local) {
          this.setState({
            northUser: Strings.open,
            northAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.northUser = '';
        } else if (this.state.southUser === jidlocal._local) {
          this.setState({
            southUser: Strings.open,
            southAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.southUser = '';
        } else if (this.state.westUser === jidlocal._local) {
          this.setState({
            westUser: Strings.open,
            westAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.westUser = '';
        } else if (this.state.eastUser === jidlocal._local) {
          this.setState({
            eastUser: Strings.open,
            eastAvtar: svgImages.joinSymbol
          });
          this.state.gamestate.eastUser = '';
        }
      }
      if (role !== 'rover') {
        let gamestate = this.state.gamestate;
        gamestate = this.updateGameStateSeat(gamestate);
        this.setState({ gamestate });
      }
      if (this.state.username === jidlocal._local) {
        let iskibitzer = false;
        if (role === 'kibitzer') iskibitzer = true;
        this.setState({ currentRole: role, role, iskibitzer });
      }
      // we're doing this because too many alerts() messes the flow
      // console.info(message)
      // if it's us, update side.
      if (HOOL_CLIENT.jid.bare().equals(jid(e.user).bare())) {
        this.state.gamestate.side = role;
      }
      if(affiliation == "owner"){
        this.props.setHostName({
          hostName: jidlocal._local
        })
      }
    });

    this.emitterStatusUpdate = HOOL_CLIENT.addListener(HOOL_EVENTS.STATE_UPDATE, async e => {
      console.log('stateUpdate', e);
      if (this.currentPresence) {
        clearInterval(this.currentPresence);
      }
      if(e?.tableinfo?.host){
        var hostName = jid(e.tableinfo.host)
        this.props.setHostName({
          hostName: hostName._local
        })
      }
      // this.setState({ spinner: false });
      // console.log(this.state.gamestate.side, e.hands);
      let gamestate = { ...this.state.gamestate };
      // console.log('Score');
      let contractInfo = e.contractinfo ? e.contractinfo : null;
      if(e.tableinfo && e.tableinfo.stage === undefined){
        gamestate.host = e.tableinfo.host
        this.setState({gamestate: gamestate}) 
      }
      //Update Bidding History
      if (!e.tableinfo && e.bidInfo) {
        let bidInfoScreen = { ...this.state.bidInfoScreen };
        bidInfoScreen.showDblOrRedbl = 'none';
        let currentBidRound = 1;
        e.bidInfo.forEach(bid => {
          bidInfoScreen = this.updateBiddingDetails(
            bid,
            bidInfoScreen,
            gamestate
          );
          currentBidRound = bid.round;
        });
        let bidInfoList = this.state.bidInfoList.filter(
          b => b.round !== currentBidRound.toString()
        );
        e.bidInfo.forEach(bid => {
          bidInfoList.push(bid);
        });
        this.setState({ bidInfoScreen, bidInfoList }, () => {
          this.updateBiddingHistory(bidInfoList, contractInfo);
          if (e.bidInfo) {
            if (bidInfoScreen.showEligibleBids) {
              const interval = setInterval(() => {
                if (this.bidComponent && this.bidComponent.current) {
                  this.bidComponent.current.showEligibleBids(
                    bidInfoScreen.winnerLevel,
                    bidInfoScreen.winnerSuit
                  );
                }
                clearInterval(interval);
              }, 500);
            }
            if (bidInfoScreen.showDblOrRedbl !== 'none') {
              if (this.bidComponent && this.bidComponent.current) {
                this.bidComponent.current.showDblOrRedbl(
                  bidInfoScreen.showDblOrRedbl
                );
              }
            }
          }
        });
      }
      if (e.tableinfo && e.bidInfo && e.bidInfo.length) {
        console.log('bidd');
        let bidInfoList = [];
        e.bidInfo.forEach(bid => {
          if (bid.type) bidInfoList.push(bid);
        });
        this.setState({ bidInfoList });
        this.updateBiddingHistory(e.bidInfo, contractInfo);
        console.log('lastBidRound');
      }
      // console.log(e.score);
      // console.log(e.turn);
      //Update Card Play History
      if (e.score && e.turn) {
        let tricks = this.state.tricks;
        // console.log(tricks, [...SIDES.keys()]);
        [...SIDES.keys()].forEach(side => {
          if (tricks[side].length !== 0) {
            if (side === e.turn) {
              tricks[side][tricks[side].length - 1].won = 'true';
            } else {
              tricks[side][tricks[side].length - 1].won = null;
            }
          }
        });
        // tricks[e.turn][tricks[e.turn].length - 1].won = 'true';
        this.setState({ tricks });
      }
      console.log('trick score pass');
      if (e.tricks && e.tricks.length) {
        console.log('tricks');
        let contractInfo = e.contractinfo ? e.contractinfo : null;
        // console.log(contractInfo);
        let tricks = { N: [], E: [], S: [], W: [] };
        let lastBidRound = 1;

        e.tricks.forEach((trick, idx) => {
          if (trick.cards && trick.cards.length) {
            trick.cards.forEach((card, cidx) => {
              if (idx == 0) {
                card.initialCard = 'true';
                if (cidx == 0) {
                  card.startingCard = 'true';
                }
              }
              if (cidx == parseInt(trick.winner)) {
                card.won = 'true';
              }
              tricks[card.side].push(card);
            });
          }
        });
        console.log('tricks');
        // console.log(tricks);
        this.setState({ tricks });
        let keys = [...SIDES.keys()];
        keys.forEach(side => {});
      }
      console.log('Test123');
      //Select the required screen based on the Table stage
      if (gamestate.side && gamestate.side != 'none' && e.hands) {
        console.log('Test');
        for (let h of e.hands) {
          gamestate.hands.set(h.side, h);
        }
        // if (gamestate.side && gamestate.side !== 'none' && e.hands) {
        //   e.hands.forEach(h => {
        //     this.displayCardsDummy(h);
        //     if (gamestate.StartSeat === this.state.myLHOSeat) {
        //       this.setState({
        //         disabledummyCards_C: false,
        //         disabledummyCards_D: false,
        //         disabledummyCards_H: false,
        //         disabledummyCards_S: false
        //       });
        //     }
        //   });
        // }
        if (e.contractinfo !== undefined) {
          gamestate.contractinfo = e.contractinfo;
        }
        if (e.bidInfo !== undefined) {
          gamestate.bidInfo = e.bidInfo;
        }
        if (e.tricks !== undefined) {
          gamestate.tricks = e.tricks;
        }
        if (e.tableinfo !== undefined) {
          gamestate.tableinfo = e.tableinfo;
          gamestate.dealer = e.tableinfo.dealer;
          gamestate.host =
            e.tableinfo.host && e.tableinfo.host.seat != undefined
              ? e.tableinfo.host.seat
              : gamestate.host;
        } else {
          gamestate.dealer = 'N';
        }

        try {
          // AsyncStorage.setItem('currentGameState', 'ShareInfo');
          // AsyncStorage.setItem('gameInfo', JSON.stringify([...gamestate]));
        } catch (e) {
          console.log(e);
        }
        gamestate = this.updateGameStateSeat(gamestate);
        if (e.tableinfo !== undefined) {
          console.log('e.tableinfo.stage ' + e.tableinfo.stage);
          console.log('enter play screen');
          if (e.tableinfo.stage === 'play') {
            const interval = setInterval(() => {
              this.setState({ gamestate }, () => {
                this.gotoPlayScreen(e);
              });
              clearInterval(interval);
            }, 500);
          } else if (e.tableinfo.stage === 'bidding') {
            const interval = setTimeout(() => {
              this.setState(
                {
                  gamestate,
                  currentScreen: SCREENS.BID_INFO
                },
                () => {
                  setTimeout(() => {
                    let bidInfoScreen = { ...this.state.bidInfoScreen };
                    bidInfoScreen.showDblOrRedbl = 'none';
                    let bidInfoList = e.bidInfo;
                    let lastBidRound = 1;
                    let fulltruth = false;
                    e.bidInfo.forEach(bidDetail => {
                      if (parseInt(bidDetail.round) > lastBidRound) {
                        lastBidRound = parseInt(bidDetail.round);
                      }
                    });
                    bidInfoScreen.showBiddingBox = false;
                    bidInfoScreen.showEligibleBids = false;
                    bidInfoScreen.showDblOrRedbl = 'none';
                    let firstRoundCheck = true;
                    for (let index = 0; index < lastBidRound; index++) {
                      fulltruth = false;
                      let prevshowBiddingBox = bidInfoScreen.showBiddingBox;
                      let prevshowEligibleBids = bidInfoScreen.showEligibleBids;
                      let prevshowDblOrRedbl = bidInfoScreen.showDblOrRedbl;
                      bidInfoScreen.showBiddingBox = false;
                      bidInfoScreen.showEligibleBids = false;
                      bidInfoScreen.showDblOrRedbl = 'none';
                      e.bidInfo.forEach(bid => {
                        console.log(bid.type, 'bidtype');
                        if (
                          bid.type &&
                          bid.type != null &&
                          bid.type != undefined &&
                          parseInt(bid.round) === index + 1
                        ) {
                          fulltruth = true;
                          bidInfoScreen = this.updateBiddingDetails(
                            bid,
                            bidInfoScreen,
                            gamestate
                          );
                        }
                      });
                      if (!fulltruth) {
                        bidInfoScreen.showBiddingBox = prevshowBiddingBox;
                        bidInfoScreen.showEligibleBids = prevshowEligibleBids;
                        bidInfoScreen.showDblOrRedbl = prevshowDblOrRedbl;
                      }
                    }
                    if (
                      e.bidInfo.length === 0 ||
                      (lastBidRound == 1 && !fulltruth)
                    )
                      bidInfoScreen.showBiddingBox = true;
                    if (e.bidInfo.length !== 0 && fulltruth) {
                      bidInfoScreen.showBiddingBox = false;
                      if (
                        bidInfoScreen.showEligibleBids ||
                        bidInfoScreen.showDblOrRedbl !== 'none'
                      ) {
                        bidInfoScreen.showBiddingBox = true;
                      }
                    }

                    this.setState({ bidInfoScreen }, () => {
                      if (e.bidInfo) {
                        if (bidInfoScreen.showEligibleBids) {
                          const interval = setInterval(() => {
                            if (
                              this.bidComponent &&
                              this.bidComponent.current
                            ) {
                              this.bidComponent.current.showEligibleBids(
                                bidInfoScreen.winnerLevel,
                                bidInfoScreen.winnerSuit
                              );
                            }
                            clearInterval(interval);
                          }, 500);
                        }
                        if (bidInfoScreen.showDblOrRedbl !== 'none') {
                          if (this.bidComponent && this.bidComponent.current) {
                            this.bidComponent.current.showDblOrRedbl(
                              bidInfoScreen.showDblOrRedbl
                            );
                          }
                        }
                      }
                    });
                  }, 1000);
                }
              );
              clearInterval(interval);
            }, 2000);
          } else if (e.tableinfo.stage === 'infosharing') {
            this.setState({ currentScreen: '' });
            this.redirectToShareInfoScreen(gamestate);
          }
        } else {
          if (this.state.currentScreen == SCREENS.JOIN_TABLE) {
            this.redirectToShareInfoScreen(gamestate);
          }
        }
      }
      if (
        this.state.currentScreen === SCREENS.SCOREBOARD &&
        gamestate.side &&
        gamestate.side != 'none' &&
        e.hands
      ) {
        gamestate.hands = new Map();
        console.log('inside if');
        // console.log(gamestate);
        for (let h of e.hands) {
          gamestate.hands.set(h.side, h);
        }
        gamestate.dealaer = e.dealer;
        console.log('before setinterval');
        this.redirectToShareInfoScreen(gamestate);
      } else if (
        this.state.currentScreen === SCREENS.PLAY_SCREEN &&
        gamestate.side &&
        gamestate.side !== 'none' &&
        e.hands
      ) {
        this.setState({ gamestate }, () => {
          setTimeout(() => {
            if (e.hands.length != 0) {
              e.hands.forEach((h, key) => {
                console.log('Key side', key);
                let dummySide = '';
                if (!this.state.iskibitzer) {
                  if (
                    h.side === 'S' &&
                    gamestate.contractinfo.declarer === 'N'
                  ) {
                    if (
                      this.playComponent &&
                      this.playComponent.current != null
                    )
                      this.playComponent.current.displayCardsDummy(h);
                    dummySide = h.side;
                  } else if (
                    h.side === 'W' &&
                    gamestate.contractinfo.declarer === 'E'
                  ) {
                    if (
                      this.playComponent &&
                      this.playComponent.current != null
                    )
                      this.playComponent.current.displayCardsDummy(h);
                    dummySide = h.side;
                  } else if (
                    h.side === 'N' &&
                    gamestate.contractinfo.declarer === 'S'
                  ) {
                    if (
                      this.playComponent &&
                      this.playComponent.current != null
                    )
                      this.playComponent.current.displayCardsDummy(h);
                    dummySide = h.side;
                  } else if (
                    h.side === 'E' &&
                    gamestate.contractinfo.declarer === 'W'
                  ) {
                    if (
                      this.playComponent &&
                      this.playComponent.current != null
                    )
                      this.playComponent.current.displayCardsDummy(h);
                    dummySide = h.side;
                  }
                }
                let playScreen = { ...this.state.playScreen };
                if (dummySide === gamestate.myLHOSeat) {
                  playScreen.DummySeat = 'LHO';
                } else if (dummySide === gamestate.myRHOSeat) {
                  playScreen.DummySeat = 'RHO';
                } else if (dummySide === gamestate.myPartnerSeat) {
                  playScreen.DummySeat = 'Partner';
                }
                if (dummySide === '') {
                  if (
                    this.playComponent &&
                    this.playComponent.current != null
                  ) {
                    let claimCardData = this.displayClaimCards(h.cards);
                    if (h.side === this.state.gamestate.myRHOSeat) {
                      playScreen.eastHands = claimCardData.cardSide;
                      playScreen.eastCardsShown = claimCardData.cardsShown;
                    } else if (h.side === this.state.gamestate.myLHOSeat) {
                      playScreen.westHands = claimCardData.cardSide;
                      playScreen.westCardsShown = claimCardData.cardsShown;
                    } else if (h.side === this.state.gamestate.myPartnerSeat) {
                      playScreen.northHands = claimCardData.cardSide;
                      playScreen.northCardsShown = claimCardData.cardsShown;
                    }
                    console.log('east west claim hands');
                  }
                }
                console.log(
                  gamestate.StartSeat,
                  gamestate.myLHOSeat,
                  'start and lho'
                );
                if (
                  gamestate.StartSeat === gamestate.myLHOSeat &&
                  dummySide !== ''
                ) {
                  playScreen.disabledummyCards_C = true;
                  playScreen.disabledummyCards_D = true;
                  playScreen.disabledummyCards_H = true;
                  playScreen.disabledummyCards_S = true;
                  if (playScreen.trick_Suit === 'C') {
                    console.log(
                      'NumberofDummy_C ' + playScreen.NumberofDummy_C
                    );
                    if (Number(playScreen.NumberofDummy_C) > 0) {
                      playScreen.disabledummyCards_C = false;
                    } else {
                      playScreen.disabledummyCards_C = false;
                      playScreen.disabledummyCards_D = false;
                      playScreen.disabledummyCards_H = false;
                      playScreen.disabledummyCards_S = false;
                    }
                  } else if (playScreen.trick_Suit === 'D') {
                    console.log(
                      'NumberofDummy_D ' + playScreen.NumberofDummy_D
                    );
                    if (Number(playScreen.NumberofDummy_D) > 0) {
                      playScreen.disabledummyCards_D = false;
                    } else {
                      playScreen.disabledummyCards_C = false;
                      playScreen.disabledummyCards_D = false;
                      playScreen.disabledummyCards_H = false;
                      playScreen.disabledummyCards_S = false;
                    }
                  } else if (playScreen.trick_Suit === 'H') {
                    console.log(
                      'NumberofDummy_H ' + playScreen.NumberofDummy_H
                    );
                    if (Number(playScreen.NumberofDummy_H) > 0) {
                      playScreen.disabledummyCards_H = false;
                    } else {
                      playScreen.disabledummyCards_C = false;
                      playScreen.disabledummyCards_D = false;
                      playScreen.disabledummyCards_H = false;
                      playScreen.disabledummyCards_S = false;
                    }
                  } else if (playScreen.trick_Suit === 'S') {
                    console.log(
                      'NumberofDummy_S ' + playScreen.NumberofDummy_S
                    );
                    if (Number(playScreen.NumberofDummy_S) > 0) {
                      playScreen.disabledummyCards_S = false;
                    } else {
                      playScreen.disabledummyCards_C = false;
                      playScreen.disabledummyCards_D = false;
                      playScreen.disabledummyCards_H = false;
                      playScreen.disabledummyCards_C = false;
                    }
                  }
                }
                this.setState({ playScreen }, () => {
                  if (this.playComponent && this.playComponent.current != null)
                    playScreen = this.playComponent.current.updateCardPlayArea(
                      this.state.playScreen
                    );
                  this.setState({ playScreen });
                });
              });
            }

            // this.playComponent.current.setDirection(e.turn);
          }, 500);
        });
      }

      if (e.score) {
        if (e.score.trick) {
          let playScreen = { ...this.state.playScreen };
          var scoreString = e.score.trick
            .map(s => `${s.side}-${s.value}`)
            .join(' ');

          e.score.trick.map(s => {
            var val = s.value.padStart(2, '0');
            if (s.side.indexOf('NS') !== -1) {
              this.setState({ TrickScoreNS: val });
            } else if (s.side.indexOf('EW') !== -1) {
              this.setState({ TrickScoreEW: val });
            }
          });
          console.info(`Trick turn: ${e.turn}`);
          if (e.turn != undefined) {
            playScreen.DirectionTextPartner = styles.DirectionText;
            playScreen.DirectionTextLHO = styles.DirectionText;
            playScreen.DirectionTextRHO = styles.DirectionText;
            playScreen.DirectionTextMySeat = styles.DirectionText;
            playScreen.northUserText = '#6D6D6D';
            playScreen.eastUserText = '#6D6D6D';
            playScreen.southUserText = '#6D6D6D';
            playScreen.westUserText = '#6D6D6D';
            playScreen.CardBorderS = '#676767';
            playScreen.CardBorderN = '#676767';
            playScreen.CardBorderE = '#676767';
            playScreen.CardBorderW = '#676767';
            playScreen.trick_Suit = '';
            playScreen.cardsCnt = 0;
            playScreen.directionImage = svgImages.pointer;
            playScreen.playTurn = '';
            this.setState({ playScreen });
            const interval = setInterval(() => {
              playScreen.MyCard = false;
              playScreen.PartnerPlayed = false;
              playScreen.LHOPlayed = false;
              playScreen.RHOPlayed = false;
              playScreen.playTurn = e.turn;
              console.log('play turn from interval', e.turn);
              this.setState({ playScreen },  () => {
                if (this.playComponent && this.playComponent.current != null) {
                   this.playComponent.current.setDirection(e.turn);
                }
              });
              // this.playComponent.current.setDirection(e.turn);
              clearInterval(interval);
            }, 2000);
            this.setState({ playScreen })
          }
        }
        if (e.score.deal) {
          var scoreString = e.score.deal
            .map(s => `${s.side}-${s.value}`)
            .join(' ');
          console.info(`Deal score: ${scoreString}`);
          e.score.deal.map(s => {
            var val = s.value;
            if (s.side.indexOf('NS') !== -1) {
              this.setState({ dealScoreNS: val });
              gamestate.dealScoreNS = val;
            } else if (s.side.indexOf('EW') !== -1) {
              this.setState({ dealScoreEW: val });
              gamestate.dealScoreEW = val;
            }
          });
          //this.props.navigation.navigate('Home',{xmppconn : hool});
        }
        if (e.score.running) {
          var scoreString = e.score.running
            .map(s => `${s.side}-${s.value}`)
            .join(' ');
          console.info(`Running score: ${scoreString}`);
          e.score.running.map(s => {
            var val = s.value;
            if (s.side.indexOf('NS') !== -1) {
              this.setState({ runningScoreNS: val });
              gamestate.runningScoreNS = val;
            } else if (s.side.indexOf('EW') !== -1) {
              this.setState({ runningScoreEW: val });
              gamestate.runningScoreEW = val;
            }
          });
        }
        if (
          (e.tableinfo && e.tableinfo.stage === 'scoreboard') ||
          (e.score.running && e.score.deal)
        ) {
          console.log('Inside Scoreboard Display');
          if (Number(this.state.dealScoreEW) > Number(this.state.dealScoreNS)) {
            gamestate.dealWinner = 'EW';
          } else {
            gamestate.dealWinner = 'NS';
          }
          gamestate.trickScoreNS = this.state.TrickScoreNS;
          gamestate.trickScoreEW = this.state.TrickScoreEW;
          let playScreen = { ...this.state.playScreen };
          playScreen.reqUndoimg = svgImages.undo;
          const interval = setInterval(() => {
            this.setState({
              gamestate,
              playScreen,
              showClaimPopup: false,
              currentScreen: SCREENS.SCOREBOARD
            });
            clearInterval(interval);
          }, 2000);
          return;
        }
      }
    });
    
    this.emitterGroupChat = HOOL_CLIENT.addListener(HOOL_EVENTS.GROUP_CHAT,  e => {
      const chatObject = {
        _id: uuidv4(),
        text: `${e.text}`,
        createdAt: new Date(),
        user: {
          _id: uuidv4(),
          name: `${(jid(e.from)._resource)}`
        }
      }
      let user = jid(e.from)._resource
      // Update Messages in the tableMessages Array
      let messages = [...this.props.tableMessages]

      messages.unshift(chatObject)
      this.props.setTableMessages({
        tableMessages: messages
      })
      
      //Add Table NotificationCount
      if( this.props.lastClickedUser !== "Table Messages"){
         let count = this.props.tableNotificationCount
         count = count + 1
         this.props.setTableChatCount({
          tableNotificationCount : count
         })
         let tableList = [...this.props.tableList]

         tableList[0].isNewMessages = true
         let tableChatCount = tableList[0].unreadMessages
         tableChatCount = tableChatCount + 1
         tableList[0].unreadMessages = tableChatCount

         this.props.setTableList({
          tableList: tableList
         })
      }

      // To Change the username to chatScreen
      if( !this.state.showChatWindow ){
      if( this.state.currentScreen != SCREENS.JOIN_TABLE && this.state.currentScreen != SCREENS.SCOREBOARD ){
        console.log("Current Screen",)
        if( user === this.state.gamestate.myPartnername ){
          let username = this.state.gamestate.myPartnername
          let gamestate = {...this.state.gamestate}
          gamestate.myPartnername = e.text
          this.setState({
            gamestate: gamestate
          })
          let userSeat = this.changeUsernameToChat(user,e.text)
          const interval = setInterval(() => {
            gamestate.myPartnername = username
            this.setState({
              gamestate: gamestate,
              northUser: username
            })
            
            this.changeChatToUsername(userSeat,username)
            clearTimeout(interval)
          },7000) 
        }else if( user === this.state.gamestate.myLHOname){
          let username = this.state.gamestate.myLHOname
          let gamestate = {...this.state.gamestate}
          gamestate.myLHOname = e.text
          this.setState({
            gamestate: gamestate
          })
          let userSeat = this.changeUsernameToChat(user,e.text)
          const interval = setInterval(() => {
            gamestate.myLHOname = username
            this.setState({
              gamestate: gamestate
            })
            this.changeChatToUsername(userSeat,username)
            clearTimeout(interval)
          },7000) 
        }else if( user === this.state.gamestate.myRHOname){
          let username = this.state.gamestate.myRHOname
          let gamestate = {...this.state.gamestate}
          gamestate.myRHOname = e.text
          this.setState({
            gamestate: gamestate
          })
          let userSeat = this.changeUsernameToChat(user,e.text)
          const interval = setInterval(() => {
            gamestate.myRHOname = username
            this.setState({
              gamestate: gamestate
            })
            this.changeChatToUsername(userSeat,username)
            clearTimeout(interval)
          },7000) 
        }else if( user === this.state.gamestate.myUsername){
          let username = this.state.gamestate.myUsername
          let gamestate = {...this.state.gamestate}
          gamestate.myUsername = e.text
          this.setState({
            gamestate: gamestate
          })
          let userSeat = this.changeUsernameToChat(user,e.text)
          const interval = setInterval(() => {
            gamestate.myUsername = username
            this.setState({
              gamestate: gamestate
            })
            this.changeChatToUsername(userSeat,username)
            clearTimeout(interval)
          },7000) 
        }
      }else if(this.state.currentScreen == SCREENS.JOIN_TABLE || this.state.currentScreen == SCREENS.SCOREBOARD ){
        let username = user
        let userSeat = this.changeUsernameToChat(user,e.text)
        console.log(" User Seat:",userSeat)
        const interval = setInterval(() => {
          this.changeChatToUsername(userSeat,username)
          clearTimeout(interval)
        },7000)     
      }
    }
    })

    this.emitterInfoShare = HOOL_CLIENT.addListener(HOOL_EVENTS.INFO_SHARE, e => {
      console.log("----infoshare------e", e);      
      // re-sort pattern
      if (e.type === 'pattern') {
        let pat = e.value.split(',').sort().reverse();

        // make sure it has four values
        while (pat.length < 4) {
          pat.push('0');
        }

        // put them together
        e.value = pat.join(',');
      }

      let s = this.state.gamestate.info.get(e.side);
      if (!s) {
        s = [];
      }
      s.push({ type: e.type, value: e.value });
      this.state.gamestate.info.set(e.side, s);
      var suitType = e.type;
      if (suitType === 'D') {
        suitType = 'Diamonds';
      } else if (suitType === 'C') {
        suitType = 'Clubs';
      } else if (suitType === 'H') {
        suitType = 'Hearts';
      } else if (suitType === 'S') {
        suitType = 'Spades';
      } else if (suitType === 'pattern'){
        suitType = 'Pattern';
      }
      let infoScreen = this.state.infoScreen;
      if (e.side === this.state.gamestate.myPartnerSeat) {
        if (!infoScreen.northSharedType1) {
          infoScreen.northSharedType1 = suitType;
          infoScreen.northSharedValue1 = e.value;
          infoScreen.DirectionTextPartner = '#6D6D6D';
          infoScreen.northUserText = '#6D6D6D';
        } else if (!infoScreen.northSharedType2) {
          infoScreen.northSharedType2 = suitType;
          infoScreen.northSharedValue2 = e.value;
          infoScreen.DirectionTextPartner = '#6D6D6D';
          infoScreen.northUserText = '#6D6D6D';
        }
        if (!infoScreen.eastSharedType2) {
          infoScreen.DirectionTextRHO = '#DBFF00';
          infoScreen.eastUserText = '#DBFF00';
          infoScreen.directionPointer = require('../../assets/images/direction_pointer_e.png');
        }
      } else if (e.side === this.state.gamestate.myRHOSeat) {
        if (!infoScreen.eastSharedType1) {
          infoScreen.eastSharedType1 = suitType;
          infoScreen.eastSharedValue1 = e.value;
          infoScreen.DirectionTextRHO = '#6D6D6D';
          infoScreen.eastUserText = '#6D6D6D';
        } else if (!infoScreen.eastSharedType2) {
          infoScreen.eastSharedType2 = suitType;
          infoScreen.eastSharedValue2 = e.value;
          infoScreen.DirectionTextRHO = '#6D6D6D';
          infoScreen.eastUserText = '#6D6D6D';
        }
        if (!infoScreen.southSharedType2) {
          infoScreen.DirectionTextMySeat = '#DBFF00';
          infoScreen.southUserText = '#DBFF00';
          infoScreen.directionPointer = require('../../assets/images/direction_pointer_s.png');
          infoScreen.showShareCards = true;
        }
      } else if (e.side === this.state.gamestate.mySeat) {
        if (!infoScreen.southSharedType1) {
          infoScreen.southSharedType1 = suitType;
          infoScreen.southSharedValue1 = e.value;
          infoScreen.DirectionTextMySeat = '#6D6D6D';
          infoScreen.southUserText = '#6D6D6D';
        } else if (!infoScreen.southSharedType2) {
          infoScreen.southSharedType2 = suitType;
          infoScreen.southSharedValue2 = e.value;
          infoScreen.DirectionTextMySeat = '#6D6D6D';
          infoScreen.southUserText = '#6D6D6D';
        }
        if (!infoScreen.westSharedType2) {
          infoScreen.DirectionTextLHO = '#DBFF00';
          infoScreen.westUserText = '#DBFF00';
          infoScreen.directionPointer = require('../../assets/images/direction_pointer_w.png');
        }
      } else if (e.side === this.state.gamestate.myLHOSeat) {
        if (!infoScreen.westSharedType1) {
          infoScreen.westSharedType1 = suitType;
          infoScreen.westSharedValue1 = e.value;
          infoScreen.DirectionTextLHO = '#6D6D6D';
          infoScreen.westUserText = '#6D6D6D';
        } else if (!infoScreen.westSharedType2) {
          infoScreen.westSharedType2 = suitType;
          infoScreen.westSharedValue2 = e.value;
          infoScreen.DirectionTextLHO = '#6D6D6D';
          infoScreen.westUserText = '#6D6D6D';
        }
        if (!infoScreen.northSharedType2) {
          infoScreen.DirectionTextPartner = '#DBFF00';
          infoScreen.northUserText = '#DBFF00';
          infoScreen.directionPointer = require('../../assets/images/direction_pointer_n.png');
        }
      }
      infoScreen = this.shareInfoComponent.current.toggleSideSelection(
        nextSide(e.side),
        infoScreen
      );
      // if (e.side === 'N' || e.side === 'S') {
      //   infoScreen.NSStyle = styles.ContractText;
      //   infoScreen.NSSharingStyle = styles.ShareText;
      //   infoScreen.EWStyle = styles.ContractTextActive;
      //   infoScreen.EWSharingStyle = styles.ShareTextActive;
      // } else if (e.side === 'E' || e.side === 'W') {
      //   infoScreen.NSStyle = styles.ContractTextActive;
      //   infoScreen.NSSharingStyle = styles.ShareTextActive;
      //   infoScreen.EWStyle = styles.ContractText;
      //   infoScreen.EWSharingStyle = styles.ShareText;
      // }
      this.setState({ infoScreen }, () => {
        let result = this.checkBidding(
          this.state.infoScreen,
          this.state.gamestate
        );
        if (result.redirect) {
          let bidInfoScreen = this.state.bidInfoScreen;
          bidInfoScreen.showBiddingBox = true;
          this.setState({ gamestate: result.gamestate, bidInfoScreen }, () => {
            const interval = setInterval(() => {
              this.setState({ currentScreen: SCREENS.BID_INFO });
              clearInterval(interval);
            }, 2000);
          });
        }
      });
    });

    this.emitterBid = HOOL_CLIENT.addListener(HOOL_EVENTS.BID, bid => {
      let gamestate = this.state.gamestate;
      let bidInfoScreen = { ...this.state.bidInfoScreen };
      bidInfoScreen.showDblOrRedbl = 'none';
      bidInfoScreen.showEligibleBids = false;
      if (bid.type) {
        bidInfoScreen = this.updateBiddingDetails(
          bid,
          bidInfoScreen,
          gamestate
        );
        //Update Live Bidding at the bid response of round end
        console.log('------------------------------------------->bidd', bid);
        let contractInfo = this.state.contractinfo;
        let biddings = this.state.biddings;
        let lastBidRound = parseInt(bid.round);
        console.log('existing bids');
        console.log(biddings[bid.side].filter(b => b.round === bid.round));
        if (
          biddings[bid.side].filter(
            b => b.round === bid.round && b.type === 'dummy' && !b.dummy_won
          ).length !== 0
        ) {
          biddings[bid.side].splice(biddings[bid.side].length - 1);
        }
        if (
          biddings[bid.side].filter(b => b.round === bid.round).length === 0
        ) {
          biddings[bid.side].push(bid);
          let keys = [...SIDES.keys()];
          keys.forEach(side => {
            let updatedBiddings = [];
            let currentSideRound = 1;
            let lastRoundBidObj = null;
            biddings[side].forEach((bid, idx) => {
              if (updatedBiddings.length != 0) {
                lastRoundBidObj = updatedBiddings[updatedBiddings.length - 1];
                let lastRound = parseInt(lastRoundBidObj.round);
                let roundDiff = parseInt(bid.round) - (lastRound + 1);

                if (roundDiff == 0) {
                  updatedBiddings.push(bid);
                  lastRoundBidObj = bid;
                } else {
                  for (let index = 1; index <= roundDiff; index++) {
                    let dummyBid = {
                      type: 'dummy',
                      round: (lastRound + index).toString()
                    };
                    if (lastRoundBidObj.won) {
                      dummyBid.type = lastRoundBidObj.type;
                      dummyBid.level = lastRoundBidObj.level;
                      dummyBid.side = lastRoundBidObj.side;
                      dummyBid.suit = lastRoundBidObj.suit;
                      dummyBid.dummy_won = lastRoundBidObj.won;
                    }

                    updatedBiddings.push(dummyBid);
                  }
                  updatedBiddings.push(bid);
                  lastRoundBidObj = bid;
                }
              } else {
                updatedBiddings.push(bid);
                lastRoundBidObj = bid;
              }
              if (currentSideRound < parseInt(bid.round)) {
                currentSideRound = parseInt(bid.round);
              }
            });
            console.log(currentSideRound, lastBidRound, side);
            if (currentSideRound < lastBidRound) {
              let roundDiff = lastBidRound - currentSideRound;
              for (let index = 1; index <= roundDiff; index++) {
                let dummyBid = {
                  type: 'dummy',
                  round: (currentSideRound + index).toString()
                };
                console.log(lastRoundBidObj);
                if (lastRoundBidObj.type != 'double' && lastRoundBidObj.won) {
                  dummyBid.type = lastRoundBidObj.type;
                  dummyBid.level = lastRoundBidObj.level;
                  dummyBid.side = lastRoundBidObj.side;
                  dummyBid.suit = lastRoundBidObj.suit;
                  dummyBid.dummy_won = lastRoundBidObj.won;
                }

                updatedBiddings.push(dummyBid);
              }
            }
            updatedBiddings.forEach((updatedBid, idx) => {
              let nextRoundBid = null;
              if (idx < updatedBiddings.length - 1) {
                nextRoundBid = updatedBiddings[idx + 1];
              }
              console.log(nextRoundBid);
              if (nextRoundBid != null && nextRoundBid.type == 'redouble') {
                updatedBid.force_won = 'true';
              }
              if (nextRoundBid != null && nextRoundBid.type == 'redouble') {
                updatedBid.force_line = 'true';
              }
              if (
                updatedBid.type == 'double' &&
                contractInfo != null &&
                contractInfo.double &&
                contractInfo.redouble == 'false'
              ) {
                updatedBid.force_won = 'true';
              }
              if (
                updatedBid.type == 'level' &&
                updatedBid.dummy_won &&
                contractInfo != null &&
                contractInfo.suit == updatedBid.suit &&
                contractInfo.level == updatedBid.level
              ) {
                updatedBid.force_won = 'true';
              }
            });
            biddings[side] = updatedBiddings;
          });
          console.log(biddings);
          this.setState({ biddings, lastBidRound });
          console.log(lastBidRound);
        }
      } else {
        console.info(`${bid.side} has made a bid;`);
        if (bid.side === this.state.gamestate.myRHOSeat) {
          bidInfoScreen.DirectionTextRHO = '#6D6D6D';
          bidInfoScreen.eastUserText = '#6D6D6D';
        } else if (bid.side === this.state.gamestate.mySeat) {
          bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
          bidInfoScreen.southUserText = '#6D6D6D';
        } else if (bid.side === this.state.gamestate.myPartnerSeat) {
          console.log('part 2');
          bidInfoScreen.DirectionTextPartner = '#6D6D6D';
          bidInfoScreen.northUserText = '#6D6D6D';
        } else if (bid.side === this.state.gamestate.myLHOSeat) {
          bidInfoScreen.DirectionTextLHO = '#6D6D6D';
          bidInfoScreen.westUserText = '#6D6D6D';
        }

        if (
          bidInfoScreen.DirectionTextMySeat === '#6D6D6D' &&
          bidInfoScreen.DirectionTextPartner === '#6D6D6D' &&
          (bid.side === this.state.gamestate.mySeat ||
            bid.side === this.state.gamestate.myPartnerSeat)
        ) {
          bidInfoScreen = this.bidComponent.current.toggleSideSelection(
            bid.side,
            bidInfoScreen,
            false,
            false
          );
        }
        if (
          bidInfoScreen.DirectionTextRHO === '#6D6D6D' &&
          bidInfoScreen.DirectionTextLHO === '#6D6D6D' &&
          (bid.side === this.state.gamestate.myLHOSeat ||
            bid.side === this.state.gamestate.myRHOSeat)
        ) {
          bidInfoScreen = this.bidComponent.current.toggleSideSelection(
            bid.side,
            bidInfoScreen,
            false,
            false
          );
          // if (bid.side === 'N' || bid.side === 'S') {
          //   bidInfoScreen.NSStyle = styles.ContractText;
          //   bidInfoScreen.NSBIDStyle = styles.BidText;
          //   bidInfoScreen.EWStyle = styles.ContractTextActive;
          //   bidInfoScreen.EWBIDStyle = styles.BidTextActive;
          // } else if (bid.side === 'E' || bid.side === 'W') {
          //   bidInfoScreen.NSStyle = styles.ContractTextActive;
          //   bidInfoScreen.NSBIDStyle = styles.BidTextActive;
          //   bidInfoScreen.EWStyle = styles.ContractText;
          //   bidInfoScreen.EWBIDStyle = styles.BidText;
          // }
        }
      }
      console.log(bidInfoScreen);
      this.setState({ bidInfoScreen }, () => {
        if (bidInfoScreen.showEligibleBids) {
          const interval = setInterval(() => {
            console.log('Bid Data', bid);
            this.bidComponent.current.showEligibleBids(bid.level, bid.suit);
            clearInterval(interval);
          }, 500);
        }
        if (bidInfoScreen.showDblOrRedbl !== 'none') {
          this.bidComponent.current.showDblOrRedbl(
            bidInfoScreen.showDblOrRedbl
          );
        }
      });
    });

    this.emitterCardPlay = HOOL_CLIENT.addListener(HOOL_EVENTS.CARD_PLAY, e => {
      // hide the card, if necessary
      // this.resetUndo();
      let playScreen = this.state.playScreen;
      let gamestate = this.state.gamestate;
      let showHistory = this.state.showHistory;
      let showCheatSheet = this.state.showCheatSheet;
      playScreen.directionImage = svgImages.pointer;
      console.log(e.side, 'Side Received');
      if (e.side === 'N' || e.side === 'S') {
        playScreen.NSStyle = styles.ContractText;
        playScreen.NSScoreStyle = styles.ScoreText;
        playScreen.NSSharingBottom = '#6d6d6d';
        playScreen.EWStyle = styles.ContractTextActive;
        playScreen.EWScoreStyle = styles.ScoreTextActive;
        playScreen.EWSharingBottom = '#DBFF00';
        if (
          gamestate.mySeat !== playScreen.DummySeat &&
          (playScreen.DummySeat === 'Partner' ||
            (playScreen.DummySeat !== 'Partner' &&
              ((e.side === 'N' && gamestate.mySeat === 'E') ||
                (e.side === 'S' && gamestate.mySeat === 'W'))))
        ) {
          showHistory = false;
          showCheatSheet = false
        }
      } else if (e.side === 'E' || e.side === 'W') {
        playScreen.NSStyle = styles.ContractTextActive;
        playScreen.NSScoreStyle = styles.ScoreTextActive;
        playScreen.NSSharingBottom = '#DBFF00';
        playScreen.EWStyle = styles.ContractText;
        playScreen.EWScoreStyle = styles.ScoreText;
        playScreen.EWSharingBottom = '#6d6d6d';
        console.log(gamestate.myPartnerSeat, playScreen.DummySeat);
        if (
          gamestate.mySeat !== playScreen.DummySeat &&
          (playScreen.DummySeat === 'Partner' ||
            (playScreen.DummySeat !== 'Partner' &&
              ((e.side === 'E' && gamestate.mySeat === 'S') ||
                (e.side === 'W' && gamestate.mySeat === 'N'))))
        ) {
          showHistory = false;
          showCheatSheet= false;
        }
      }
      let tricks = this.state.tricks;
      if (e.side) {
        if (tricks[e.side].length === 0) {
          e.initialCard = 'true';
        }
        if (
          tricks.N.length === 0 &&
          tricks.S.length === 0 &&
          tricks.W.length === 0 &&
          tricks.E.length === 0
        ) {
          e.startingCard = 'true';
        }
        tricks[e.side].push(e);
      } 
      this.setState({ playScreen, showHistory, tricks, showCheatSheet },  () => {
        if (this.playComponent && this.playComponent.current != null) {
           this.playComponent.current.cardsPlayed(e);
        }
      });
    });

    this.emitterCardPlayError = HOOL_CLIENT.addListener(HOOL_EVENTS.CARD_PLAY_ERROR, e => {
      let line = `Failed to play ${e.rank}${e.suit}: ${
        e.tag || 'unknown error'
      }: ${e.text || 'unknown error'}`;
      console.warn(line);
      let playScreen = { ...this.state.playScreen };
      playScreen.directionImage = svgImages.pointerSouth;
      playScreen.DirectionTextMySeat = styles.DirectionTextActive;
      playScreen.southUserText = '#DBFF00';
      playScreen.disablemyCards_C = false;
      playScreen.disablemyCards_D = false;
      playScreen.disablemyCards_H = false;
      playScreen.disablemyCards_S = false;
      this.setState({ playScreen });
    });

    this.emitterUndo = HOOL_CLIENT.addListener(HOOL_EVENTS.UNDO, e => {
      let playScreen = { ...this.state.playScreen };
      let gamestate = this.state.gamestate;
      console.log(e.mode, e);
      if (e.mode === 'accept' && e.status === 'accepted') {
        console.log(playScreen.cardsCnt, playScreen.trick_Suit);
        let timer = 0;
        if (playScreen.cardsCnt === 0 && playScreen.trick_Suit === '') {
          timer = 2000;
          //for(var ir=0 ;ir<e.cards.length;ir++){
          console.log(e.cards);
          for (let trickcard of e.cards) {
            console.log(trickcard);
            if (this.playComponent && this.playComponent.current != null) {
              this.playComponent.current.cardsPlayed(trickcard);
            }
          }
          // }
          playScreen.cardsCnt = 4;
          // this.setState({ playScreen });
        }
        setTimeout(() => {
          playScreen = { ...this.state.playScreen };
          if (e.side === gamestate.mySeat) {
            var val = e.rank + e.suit;
            var index = playScreen.hands.findIndex(function (item, i) {
              return item.key === val;
            });
            playScreen.TotalCardWidthSouth =
              playScreen.TotalCardWidthSouth + cardWidth;
            playScreen.cardsShown[index] = 'flex';
            playScreen.MyCard = false;
            playScreen.DirectionTextLHO = styles.DirectionText;
            playScreen.westUserText = '#6D6D6D';
            playScreen.CardBorderW = '#676767';
            playScreen.DirectionTextMySeat = styles.DirectionTextActive;
            playScreen.southUserText = '#DBFF00';
            playScreen.CardBorderS = '#DBFF00';
            playScreen.directionImage = svgImages.pointerSouth;
            playScreen.cardsCnt = playScreen.cardsCnt - 1;
            playScreen.playTurn = gamestate.mySeat;

            if (playScreen.cardsCnt < 1) {
              playScreen.trick_Suit = '';
            }

            if (e.suit === 'C') {
              playScreen.Numberof_C = playScreen.Numberof_C + 1;
            } else if (e.suit === 'D') {
              playScreen.Numberof_D = playScreen.Numberof_D + 1;
            } else if (e.suit === 'H') {
              playScreen.Numberof_H = playScreen.Numberof_H + 1;
            } else if (e.suit === 'S') {
              playScreen.Numberof_S = playScreen.Numberof_S + 1;
            }

            if (!playScreen.isSouthDummy) {
              if (playScreen.trick_Suit === 'C') {
                //  console.log('Numberof_C ' +Numberof_C);
                if (Number(playScreen.Numberof_C) > 0) {
                  playScreen.disablemyCards_C = false;
                  playScreen.disablemyCards_D = true;
                  playScreen.disablemyCards_H = true;
                  playScreen.disablemyCards_S = true;
                } else {
                  playScreen.disablemyCards_C = false;
                  playScreen.disablemyCards_D = false;
                  playScreen.disablemyCards_H = false;
                  playScreen.disablemyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'D') {
                console.log('Numberof_D ' + playScreen.Numberof_D);
                if (Number(playScreen.Numberof_D) > 0) {
                  playScreen.disablemyCards_D = false;
                  playScreen.disablemyCards_C = true;
                  playScreen.disablemyCards_H = true;
                  playScreen.disablemyCards_S = true;
                } else {
                  playScreen.disablemyCards_C = false;
                  playScreen.disablemyCards_D = false;
                  playScreen.disablemyCards_H = false;
                  playScreen.disablemyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'H') {
                console.log('Numberof_H ' + playScreen.Numberof_H);
                if (Number(playScreen.Numberof_H) > 0) {
                  playScreen.disablemyCards_H = false;
                  playScreen.disablemyCards_D = true;
                  playScreen.disablemyCards_C = true;
                  playScreen.disablemyCards_S = true;
                } else {
                  playScreen.disablemyCards_C = false;
                  playScreen.disablemyCards_D = false;
                  playScreen.disablemyCards_H = false;
                  playScreen.disablemyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'S') {
                console.log('Numberof_S ' + playScreen.Numberof_S);
                if (Number(playScreen.Numberof_S) > 0) {
                  playScreen.disablemyCards_S = false;
                  playScreen.disablemyCards_D = true;
                  playScreen.disablemyCards_H = true;
                  playScreen.disablemyCards_C = true;
                } else {
                  playScreen.disablemyCards_C = false;
                  playScreen.disablemyCards_D = false;
                  playScreen.disablemyCards_H = false;
                  playScreen.disablemyCards_S = false;
                }
              } else if (playScreen.trick_Suit === '') {
                playScreen.disablemyCards_C = false;
                playScreen.disablemyCards_D = false;
                playScreen.disablemyCards_H = false;
                playScreen.disablemyCards_S = false;
              }
            }
          } else if (e.side === gamestate.myPartnerSeat) {
            if (playScreen.DummySeat === 'Partner') {
              var val = e.rank + e.suit;
              var index = playScreen.dummyHands.findIndex(function (item, i) {
                return item.key === val;
              });
              playScreen.TotalCardWidthNorth =
                playScreen.TotalCardWidthNorth + cardWidth;
              playScreen.dummycardsShown[index] = 'flex';
            }
            //this.setDirection(this.state.myPartnerSeat);
            playScreen.PartnerPlayed = false;
            playScreen.DirectionTextRHO = styles.DirectionText;
            playScreen.eastUserText = '#6D6D6D';
            playScreen.CardBorderE = '#676767';
            playScreen.DirectionTextPartner = styles.DirectionTextActive;
            playScreen.northUserText = '#DBFF00';
            playScreen.CardBorderN = '#DBFF00';
            playScreen.directionImage = svgImages.pointerNorth;
            playScreen.cardsCnt = playScreen.cardsCnt - 1;
            playScreen.playTurn = gamestate.myPartnerSeat;
            if (playScreen.cardsCnt < 1) {
              playScreen.trick_Suit = '';
            }

            if (
              playScreen.DummySeat === 'Partner' &&
              !playScreen.isSouthDummy
            ) {
              if (e.suit === 'C') {
                playScreen.NumberofDummy_C = playScreen.NumberofDummy_C + 1;
              } else if (e.suit === 'D') {
                playScreen.NumberofDummy_D = playScreen.NumberofDummy_D + 1;
              } else if (e.suit === 'H') {
                playScreen.NumberofDummy_H = playScreen.NumberofDummy_H + 1;
              } else if (e.suit === 'S') {
                playScreen.NumberofDummy_S = playScreen.NumberofDummy_S + 1;
              }
              if (playScreen.trick_Suit === 'C') {
                console.log('NumberofDummy_C ' + playScreen.NumberofDummy_C);
                if (Number(playScreen.NumberofDummy_C) > 0) {
                  playScreen.disabledummyCards_C = false;
                  playScreen.disabledummyCards_D = true;
                  playScreen.disabledummyCards_H = true;
                  playScreen.disabledummyCards_S = true;
                } else {
                  playScreen.disabledummyCards_C = false;
                  playScreen.disabledummyCards_D = false;
                  playScreen.disabledummyCards_H = false;
                  playScreen.disabledummyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'D') {
                console.log('NumberofDummy_D ' + playScreen.NumberofDummy_D);
                if (Number(playScreen.NumberofDummy_D) > 0) {
                  playScreen.disabledummyCards_D = false;
                  playScreen.disabledummyCards_C = true;
                  playScreen.disabledummyCards_H = true;
                  playScreen.disabledummyCards_S = true;
                } else {
                  playScreen.disabledummyCards_C = false;
                  playScreen.disabledummyCards_D = false;
                  playScreen.disabledummyCards_H = false;
                  playScreen.disabledummyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'H') {
                console.log('NumberofDummy_H ' + playScreen.NumberofDummy_H);
                if (Number(playScreen.NumberofDummy_H) > 0) {
                  playScreen.disabledummyCards_H = false;
                  playScreen.disabledummyCards_D = true;
                  playScreen.disabledummyCards_C = true;
                  playScreen.disabledummyCards_S = true;
                } else {
                  playScreen.disabledummyCards_C = false;
                  playScreen.disabledummyCards_D = false;
                  playScreen.disabledummyCards_H = false;
                  playScreen.disabledummyCards_S = false;
                }
              } else if (playScreen.trick_Suit === 'S') {
                console.log('NumberofDummy_S ' + playScreen.NumberofDummy_S);
                if (Number(playScreen.NumberofDummy_S) > 0) {
                  playScreen.disabledummyCards_S = false;
                  playScreen.disabledummyCards_D = true;
                  playScreen.disabledummyCards_H = true;
                  playScreen.disabledummyCards_C = true;
                } else {
                  playScreen.disabledummyCards_C = false;
                  playScreen.disabledummyCards_D = false;
                  playScreen.disabledummyCards_H = false;
                  playScreen.disabledummyCards_C = false;
                }
              } else if (playScreen.trick_Suit === '') {
                playScreen.disabledummyCards_C = false;
                playScreen.disabledummyCards_D = false;
                playScreen.disabledummyCards_H = false;
                playScreen.disabledummyCards_S = false;
              }
            }
          } else if (e.side === gamestate.myLHOSeat) {
            if (playScreen.DummySeat === 'LHO') {
              var val = e.rank + e.suit;
              var index = playScreen.dummyHands.findIndex(function (item, i) {
                return item.key === val;
              });
              playScreen.EastWestPalyCardArea =
                playScreen.EastWestPalyCardArea + 23;
              playScreen.dummycardsShown[index] = 'flex';
            }
            //this.setDirection(this.state.myLHOSeat);
            playScreen.LHOPlayed = false;
            playScreen.DirectionTextPartner = styles.DirectionText;
            playScreen.northUserText = '#6D6D6D';
            playScreen.CardBorderN = '#676767';
            playScreen.DirectionTextLHO = styles.DirectionTextActive;
            playScreen.westUserText = '#DBFF00';
            playScreen.CardBorderW = '#DBFF00';
            playScreen.directionImage = svgImages.pointerLeft;
            playScreen.cardsCnt = playScreen.cardsCnt - 1;
            playScreen.playTurn = gamestate.myLHOSeat;
            if (playScreen.cardsCnt < 1) {
              playScreen.trick_Suit = '';
            }
          } else if (e.side === gamestate.myRHOSeat) {
            console.log('Right Side');
            if (playScreen.DummySeat === 'RHO') {
              var val = e.rank + e.suit;
              var index = playScreen.dummyHands.findIndex(function (item, i) {
                return item.key === val;
              });
              playScreen.EastWestPalyCardArea =
                playScreen.EastWestPalyCardArea + 23;
              playScreen.dummycardsShown[index] = 'flex';
            }
            //this.setDirection(this.state.myRHOSeat);
            playScreen.RHOPlayed = false;
            playScreen.DirectionTextMySeat = styles.DirectionText;
            playScreen.southUserText = '#6D6D6D';
            playScreen.DirectionTextRHO = styles.DirectionTextActive;
            playScreen.eastUserText = '#DBFF00';
            playScreen.CardBorderE = '#DBFF00';
            playScreen.CardBorderS = '#676767';
            playScreen.directionImage = svgImages.pointerRight;
            playScreen.cardsCnt = playScreen.cardsCnt - 1;
            playScreen.playTurn = gamestate.myRHOSeat;
            if (playScreen.cardsCnt < 1) {
              playScreen.trick_Suit = '';
            }
            console.log(playScreen);
          }
          playScreen.undoMySeatimg = require('../../assets/images/takeback_black.png');
          playScreen.undoLHOSeatimg = require('../../assets/images/takeback_black.png');
          playScreen.undoPartnerSeatimg = require('../../assets/images/takeback_black.png');
          playScreen.undoRHOSeatimg = require('../../assets/images/takeback_black.png');
          if (playScreen.cardsCnt < 0) {
            playScreen.cardsCnt = 0;
          }
          let tricks = this.state.tricks;
          tricks[e.side].splice(this.state.tricks[e.side].length - 1);
          this.setState({ playScreen, tricks }, () => {
            this.resetUndo();
          });
        }, timer);
      }

      if (e.mode === 'reject' || e.mode === 'cancel') {
        this.resetUndo();
      }
      console.log(e.mode);
      if (e.mode === 'request') {
        playScreen = this.undoRequestState(e, playScreen, gamestate);
        this.setState({ playScreen });
      }
    });

    this.emitterUndoError = HOOL_CLIENT.addListener(HOOL_EVENTS.UNDO_ERROR, e => {
      alert(
        `Failed to make undo on ${e.table || 'the table'}: ${
          e.tag || 'unknown error'
        }: ${e.text || 'unknown error'}`
      );
    });

    this.emitterClaim = HOOL_CLIENT.addListener(HOOL_EVENTS.CLAIM, e => {
      let playScreen = { ...this.state.playScreen };
      let gamestate = this.state.gamestate;
      if (e.mode === 'accept' && e.status === 'accepted') {
        playScreen.undoMySeatimg = require('../../assets/images/takeback_black.png');
        playScreen.undoLHOSeatimg = require('../../assets/images/takeback_black.png');
        playScreen.undoPartnerSeatimg = require('../../assets/images/takeback_black.png');
        playScreen.undoRHOSeatimg = require('../../assets/images/takeback_black.png');
        this.setState({ playScreen });
      }

      if (e.mode === 'reject' || e.mode === 'withdraw') {
        this.resetUndo();
      }
      console.log(e.mode);
      if (e.mode === 'request') {
        playScreen = this.claimRequestState(e, playScreen, gamestate);
        this.setState({ playScreen });
      }
    });

    this.emitterClaimError = HOOL_CLIENT.addListener(HOOL_EVENTS.CLAIM_ERROR, e => {
      alert(
        `Failed to make claim on ${e.table || 'the table'}: ${
          e.tag || 'unknown error'
        }: ${e.text || 'unknown error'}`
      );
    });

    this.emitterChangeDeal = HOOL_CLIENT.addListener(HOOL_EVENTS.CHANGE_DEAL, e => {
      let playScreen = { ...this.state.playScreen };
      let gamestate = this.state.gamestate;
      console.log(e.mode, e);
      let changeDealUser = '';
      if (e.requestee) {
        changeDealUser = jid(e.requestee)._local.toLowerCase();
      }
      if (e.status === 'reject') {
        this.setState({
          changeDealResponed: true,
          changeDealResponseText: changeDealUser + ' rejected request'
        });
        setTimeout(() => {
          this.resetChangeDeal();
        }, 3000);
      } else if (e.status === 'catchup') {
        this.resetChangeDeal();
        this.checkStateUpdatePlay();
      } else if (e.status === 'request') {
        let isChangeDeal = true;
        let changeDealType = e.type;
        this.setState({ isChangeDeal, changeDealType, changeDealUser });
      }
    });

    this.emitterScoreboard = HOOL_CLIENT.addListener(HOOL_EVENTS.SCORE_BOARD, e => {
      console.log('scoreboard');
      console.log(e);
      var jidlocal = jid(e.user);
      console.log(jidlocal);
      var textColor = styles.shareHandTextHandName;
      if (e.mode === 'ready') {
        textColor = styles.shareHandTextHandNameActive;
      } else {
        textColor = styles.shareHandTextHandNameRed;
      }
      if (e.side === this.state.myPartnerSeat || e.side === this.state.mySeat) {
        if (e.side === this.state.myPartnerSeat) {
          this.setState({
            northUser: jidlocal._resource,
            northText: textColor
          });
        } else if (e.side === this.state.mySeat) {
          this.setState({
            southUser: jidlocal._resource,
            southText: textColor
          });
        }
      } else if (
        e.side === this.state.myRHOSeat ||
        e.side === this.state.myLHOSeat
      ) {
        if (e.side === this.state.myRHOSeat) {
          this.setState({ eastUser: jidlocal._resource, eastText: textColor });
        } else if (e.side === this.state.myLHOSeat) {
          this.setState({ westUser: jidlocal._resource, westText: textColor });
        }
      }
    });

    this.emitterScoreboardError = HOOL_CLIENT.addListener(HOOL_EVENTS.SCORE_BOARD_ERROR, e => {
      alert(
        `Failed to make scoreboard request on ${e.table || 'the table'}: ${
          e.tag || 'unknown error'
        }: ${e.text || 'unknown error'}`
      );
    });
  }

  updateGameStateSeat(gamestate) {
    if (this.state.role !== 'kibitzer') {
      if (gamestate.side === 'N') {
        ////this.setState({mySeat:'N',myLHOSeat:'E',myPartnerSeat:'S',myRHOSeat:'W'});
        // this.setState({southUser:gamestate.northUser,westUser:gamestate.eastUser,northUser:gamestate.southUser,eastUser:gamestate.westUser})
        gamestate.mySeat = 'N';
        gamestate.myPartnerSeat = 'S';
        gamestate.myLHOSeat = 'E';
        gamestate.myRHOSeat = 'W';
        gamestate.myUsername = this.state.northUser;
        gamestate.myPartnername = this.state.southUser;
        gamestate.myLHOname = this.state.eastUser;
        gamestate.myRHOname = this.state.westUser;
      } else if (gamestate.side === 'E') {
        //this.setState({mySeat:'E',myLHOSeat:'S',myPartnerSeat:'W',myRHOSeat:'N'});
        //this.setState({southUser:gamestate.eastUser,westUser:gamestate.southUser,northUser:gamestate.westUser,eastUser:gamestate.northUser})
        gamestate.mySeat = 'E';
        gamestate.myPartnerSeat = 'W';
        gamestate.myLHOSeat = 'S';
        gamestate.myRHOSeat = 'N';
        gamestate.myUsername = this.state.eastUser;
        gamestate.myPartnername = this.state.westUser;
        gamestate.myLHOname = this.state.southUser;
        gamestate.myRHOname = this.state.northUser;
      } else if (gamestate.side === 'S') {
        // this.setState({mySeat:'S',myLHOSeat:'W',myPartnerSeat:'N',myRHOSeat:'E'});
        // this.setState({southUser:gamestate.southUser,westUser:gamestate.westUser,northUser:gamestate.northUser,eastUser:gamestate.eastUser})
        gamestate.mySeat = 'S';
        gamestate.myPartnerSeat = 'N';
        gamestate.myLHOSeat = 'W';
        gamestate.myRHOSeat = 'E';
        gamestate.myUsername = this.state.southUser;
        gamestate.myPartnername = this.state.northUser;
        gamestate.myLHOname = this.state.westUser;
        gamestate.myRHOname = this.state.eastUser;
      } else if (gamestate.side === 'W') {
        // this.setState({mySeat:'W',myLHOSeat:'N',myPartnerSeat:'E',myRHOSeat:'S'});
        //  this.setState({southUser:gamestate.westUser,westUser:gamestate.northUser,northUser:gamestate.eastUser,eastUser:gamestate.southUser})
        gamestate.mySeat = 'W';
        gamestate.myPartnerSeat = 'E';
        gamestate.myLHOSeat = 'N';
        gamestate.myRHOSeat = 'S';
        gamestate.myUsername = this.state.westUser;
        gamestate.myPartnername = this.state.eastUser;
        gamestate.myLHOname = this.state.northUser;
        gamestate.myRHOname = this.state.southUser;
      }
    } else {
      gamestate.mySeat = 'S';
      gamestate.myPartnerSeat = 'N';
      gamestate.myLHOSeat = 'W';
      gamestate.myRHOSeat = 'E';
      gamestate.side = 'S';
      gamestate.myUsername = this.state.southUser;
      gamestate.myPartnername = this.state.northUser;
      gamestate.myLHOname = this.state.westUser;
      gamestate.myRHOname = this.state.eastUser;
    }
    return gamestate;
  }

  undoRequestState(undo, playScreen, gamestate) {
    if (undo.side === gamestate.myLHOSeat) {
      playScreen.undoLHOSeatimg = require('../../assets/images/takeback.png');
    } else if (undo.side === gamestate.myRHOSeat) {
      playScreen.undoRHOSeatimg = require('../../assets/images/takeback.png');
    } else if (undo.side === gamestate.mySeat) {
      playScreen.undoMySeatimg = require('../../assets/images/takeback.png');
    } else if (undo.side === gamestate.myPartnerSeat) {
      playScreen.undoPartnerSeatimg = require('../../assets/images/takeback.png');
    }
    if (this.state.role == 'kibitzer') {
      Toast.show({
        type: 'info',
        text1: 'Undo Request',
        position: 'bottom',
        bottomOffset: 50,
        text2: 'Undo Requested on ' + undo.side
      });
    } else {
      if (undo.side === gamestate.myLHOSeat) {
        playScreen.undoLHO = true;
        playScreen.cancelAnimateImg = false;
        // this.playComponent.current.animateImg();
      } else if (undo.side === gamestate.myRHOSeat) {
        playScreen.undoRHO = true;
        playScreen.cancelAnimateImg = false;
        // this.playComponent.current.animateImg();
      }
    }

    playScreen.isClaimInitiated = false;
    console.log(playScreen.undoLHO, playScreen.undoRHO);
    return playScreen;
  }

  claimRequestState(claim, playScreen, gamestate) {
    if (claim.side === gamestate.myLHOSeat) {
      playScreen.undoLHOSeatimg = require('../../assets/images/claim_flag.png');
    } else if (claim.side === gamestate.myRHOSeat) {
      playScreen.undoRHOSeatimg = require('../../assets/images/claim_flag.png');
    } else if (claim.side === gamestate.mySeat) {
      playScreen.undoMySeatimg = require('../../assets/images/claim_flag.png');
    } else if (claim.side === gamestate.myPartnerSeat) {
      playScreen.undoPartnerSeatimg = require('../../assets/images/claim_flag.png');
    }
    playScreen.isClaimInitiated = true;
    playScreen.claimConcedeNumber = parseInt(claim.amount);
    playScreen.isConcedeTrick = false;
    if (claim.amount != undefined && parseInt(claim.amount) === 0) {
      let remainingTricks = this.getRemainingTricks();
      playScreen.isConcedeTrick = true;
      playScreen.claimConcedeNumber = remainingTricks;
    }
    if (this.state.role == 'kibitzer') {
      let type = playScreen.isConcedeTrick ? 'Concede' : 'Claim';
      Toast.show({
        type: 'info',
        text1: type + ' Request',
        position: 'bottom',
        bottomOffset: 50,
        text2: type + 'Requested on ' + claim.side
      });
    } else {
      if (claim.side === gamestate.myLHOSeat) {
        playScreen.undoLHO = true;
        playScreen.cancelAnimateImg = false;
        // this.playComponent.current.animateImg();
      } else if (claim.side === gamestate.myRHOSeat) {
        playScreen.undoRHO = true;
        playScreen.cancelAnimateImg = false;
        // this.playComponent.current.animateImg();
      }
    }
    console.log(playScreen.undoLHO, playScreen.undoRHO);
    return playScreen;
  }
  resetUndo() {
    let playScreen = { ...this.state.playScreen };
    playScreen.undoMySeatimg = require('../../assets/images/takeback_black.png');
    playScreen.undoLHOSeatimg = require('../../assets/images/takeback_black.png');
    playScreen.undoPartnerSeatimg = require('../../assets/images/takeback_black.png');
    playScreen.undoRHOSeatimg = require('../../assets/images/takeback_black.png');
    playScreen.disableTkBk = true;
    playScreen.reqUndoimg = svgImages.undo;
    playScreen.undoLHO = false;
    playScreen.undoRHO = false;
    playScreen.undoAsked = false;
    playScreen.isClaimInitiated = false;
    playScreen.isConcedeTrick = false;
    playScreen.claimConcedeNumber = 0;
    this.setState({ playScreen, claimRequested: false });
  }

  toggleHistory() {
    let showHistory = this.state.showHistory;
    showHistory = !showHistory;

    console.log("!History ")
    this.props.addPlayers({
      addPlayer: false,
      direction: ''
    })
    this.setState({ showHistory : showHistory,
      showCheatSheet: false, 
      showSubMenuPopup: false,
      showChatWindow: false,
      // showClaimPopup: false,
      showChangeDealPopup: false
    }); 
  }

  toggleChat() {
    let showChatWindow = this.state.showChatWindow;
    showChatWindow = !showChatWindow;
    this.props.isTableUser({
      isTableUser: showChatWindow
    })
    this.props.searchChatQuery({
      searchQuery: ''
    })
    this.props.setLastClickedUser({
      lastClickedUser: undefined
    })
    this.props.setLastUserChat({
      lastUserChat: undefined,
    })
    if(!this.props.isFirstTime){
      this.props.setFirstTimeAndUserInfo({
        //  lastUserChat: 'Table Messages',
        isFirstTime: true
      })
    }else{
      
    }
    if(!showChatWindow){
      // this.props.showFriendsList({
      //   showFriendsList: false
      // })
    }
    this.setState({ showChatWindow });
  }

  toggleSubMenuPopup() {
    let showSubMenuPopup = this.state.showSubMenuPopup;
    let showClaimPopup = this.state.playScreen.isClaimInitiated
    if(showClaimPopup){
      showClaimPopup = false
    }
    if(showClaimPopup && this.state.showClaimPopup){
      showClaimPopup = false
    }else if(showClaimPopup && !this.state.showClaimPopup){
      showClaimPopup = true
    }
    showSubMenuPopup =
        this.state.showClaimPopup || this.state.showChangeDealPopup
        ? false
        : !showSubMenuPopup;
    
    this.props.addPlayers({
      addPlayer: false,
      direction: ''
    })
    this.setState({
      showSubMenuPopup,
      showClaimPopup: showClaimPopup,
      showChangeDealPopup: false,
      showChatWindow: false,
      showHistory: false, 
      showCheatSheet: false
    });
  }

  toggleCheatSheet(){
    let cheatSheet = this.state.showCheatSheet
    cheatSheet  = !cheatSheet
    
    this.props.addPlayers({
      addPlayer: false,
      direction: ''
    })
    
    this.setState({
      showCheatSheet: cheatSheet, 
      showChatWindow: false,
      showHistory: false, 
      showSubMenuPopup: false,
      // showClaimPopup: false,
      showChangeDealPopup: false})

  }
  // To Change the UserName to Chat Message
  changeUsernameToChat(user,chat){
    let chatScreen = {...this.state.chatScreen}
    if( user === this.state.westUser){
      chatScreen.isWestSentMsg = true
      this.setState({
        westUser: chat,
        chatScreen: chatScreen
      })
      return 'w'
    }else if( user === this.state.eastUser){
      chatScreen.isEastSentMsg = true
      this.setState({
        eastUser: chat,
        chatScreen: chatScreen
      })
      return 'e'
    }else if( user === this.state.southUser){
      chatScreen.isSouthSentMsg = true
      this.setState({
        southUser: chat,
        chatScreen: chatScreen
      })
      return 's'
    }else if( user === this.state.northUser){
      chatScreen.isNorthSentMsg = true
      this.setState({
        northUser: chat,
        chatScreen: chatScreen
      })
      return 'n'
    }
  }
  // To Change the userName to name from Chat Message
  changeChatToUsername(user,name){
    let chatScreen = {...this.state.chatScreen}
    if( user === 'w'){
      chatScreen.isWestSentMsg = false
      this.setState({
        westUser: name,
        chatScreen: chatScreen
      })
    }else if( user === 'e'){
      chatScreen.isEastSentMsg = false
      this.setState({
        eastUser: name,
        chatScreen: chatScreen
      })
    }else if( user === 's'){
      chatScreen.isSouthSentMsg = false
      this.setState({
        southUser: name,
        chatScreen: chatScreen
      })
    }else if( user === 'n'){
      chatScreen.isNorthSentMsg = false
      this.setState({
        northUser: name,
        chatScreen: chatScreen
      })
    }
  }
  getRemainingTricks() {
    return (
      13 -
      (parseInt(this.state.TrickScoreNS) + parseInt(this.state.TrickScoreEW))
    );
  }

  enableClaimPopup() {
    console.log(this.state.TrickScoreNS, this.state.TrickScoreEW);
    let remainingTricks = this.getRemainingTricks();
    console.log(remainingTricks);
    this.setState(
      {
        remainingTricks,
        showSubMenuPopup: false,
        showClaimPopup: true,
      },
      () => {
        this.selectClaimType('CLAIM');
      }
    );
  }

  enableChangeDealPopup() {
    this.setState({
      showSubMenuPopup: false,
      showChangeDealPopup: true,
    });
  }

  isClaimDisabled() {
    let flag = false;
    //Disable for Kibitzer
    if (this.state.role === 'kibitzer') return true;
    //Disable if not Play screen
    if (this.state.currentScreen !== SCREENS.PLAY_SCREEN) return true;
    if (
      this.state.playScreen.isClaimInitiated &&
      !this.state.playScreen.claimRequested
    )
      flag = true;
    if (this.state.playScreen.isSouthDummy) flag = true;
    return flag;
  }

  isChangeDealDisabled() {
    //Disable for Kibitzer
    if (this.state.role === 'kibitzer') return true;
    if (
      this.state.playScreen.isClaimInitiated ||
      this.state.playScreen.claimRequested
    )
      return true;
    //Disable on Join table
    return this.state.currentScreen === SCREENS.JOIN_TABLE;
  }

  undoRequest() {
    console.info("UNDO MAINDISK requested " )
    let playScreen = { ...this.state.playScreen };
    if (playScreen.undoAsked === false) {
      playScreen.undoAsked = true;
      playScreen.reqUndoimg = svgImages.iconCancel;
      HOOL_CLIENT.undo(this.state.tableID, 'request', {
        side: this.state.playScreen.undoCardSide,
        rank: this.state.playScreen.undoCardRank,
        suit: this.state.playScreen.undoCardSuit
      });
    } else {
      playScreen.disableTkBk = true;
      playScreen.undoAsked = false;
      playScreen.reqUndoimg = svgImages.undo;
      HOOL_CLIENT.undo(this.state.tableID, 'cancel', {
        side: this.state.playScreen.undoCardSide
      });
    }
    this.setState({ playScreen });
  }

  changeDealResponse(type) {
    HOOL_CLIENT.changedeal(this.state.tableID, this.state.gamestate.mySeat, type);
    let changeDealResponseText = "Waiting for other players' response...";
    if (type === 'no') {
      changeDealResponseText = 'Rejected Change Deal';
      setTimeout(() => {
        this.resetChangeDeal();
      }, 3000);
    }
    this.setState({ changeDealResponed: true, changeDealResponseText });
  }
  resetChangeDeal() {
    this.setState({
      isChangeDeal: false,
      changeDealResponed: false,
      changeDealResponseText: ''
    });
  }
  checkShareInfo() {
    console.log('north : ' + this.state.infoScreen.northSharedType2);
    console.log('east : ' + this.state.infoScreen.eastSharedType2);
    console.log('south : ' + this.state.infoScreen.southSharedType2);
    console.log('west : ' + this.state.infoScreen.westSharedType2);
    if (
      this.state.infoScreen.northSharedType2 !== '' &&
      this.state.infoScreen.eastSharedType2 !== '' &&
      this.state.infoScreen.southSharedType2 !== '' &&
      this.state.infoScreen.westSharedType2 !== ''
    ) {
    }
  }

  checkBidding(infoScreen, gamestate) {
    if (infoScreen != undefined) {
      console.log('north : ' + infoScreen.northSharedType2);
      console.log('east : ' + infoScreen.eastSharedType2);
      console.log('south : ' + infoScreen.southSharedType2);
      console.log('west : ' + infoScreen.westSharedType2);
      if (
        infoScreen.northSharedType2 !== '' &&
        infoScreen.eastSharedType2 !== '' &&
        infoScreen.southSharedType2 !== '' &&
        infoScreen.westSharedType2 !== '' &&
        infoScreen.northSharedType2 !== undefined &&
        infoScreen.eastSharedType2 !== undefined &&
        infoScreen.southSharedType2 !== undefined &&
        infoScreen.westSharedType2 !== undefined
      ) {
        // let gamestate = this.state.gamestate;
        gamestate.northSharedType1 = infoScreen.northSharedType1;
        gamestate.northSharedValue1 = infoScreen.northSharedValue1;
        gamestate.northSharedType2 = infoScreen.northSharedType2;
        gamestate.northSharedValue2 = infoScreen.northSharedValue2;
        gamestate.eastSharedType1 = infoScreen.eastSharedType1;
        gamestate.eastSharedValue1 = infoScreen.eastSharedValue1;
        gamestate.eastSharedType2 = infoScreen.eastSharedType2;
        gamestate.eastSharedValue2 = infoScreen.eastSharedValue2;
        gamestate.southSharedType1 = infoScreen.southSharedType1;
        gamestate.southSharedValue1 = infoScreen.southSharedValue1;
        gamestate.southSharedType2 = infoScreen.southSharedType2;
        gamestate.southSharedValue2 = infoScreen.southSharedValue2;
        gamestate.westSharedType1 = infoScreen.westSharedType1;
        gamestate.westSharedValue1 = infoScreen.westSharedValue1;
        gamestate.westSharedType2 = infoScreen.westSharedType2;
        gamestate.westSharedValue2 = infoScreen.westSharedValue2;
        return { gamestate, redirect: true };
      } else {
        console.log('not all info shared');
        return { gamestate, redirect: false };
      }
    }
    console.log('info not defined');
    return { gamestate, redirect: false };
  }
  checkStateUpdatePlay() {
    HOOL_CLIENT.catchup(this.state.tableID, 'catchup', this.state.gamestate.mySeat);
  }
  gotoPlayScreen(e) {
    let gamestate = this.state.gamestate;
    console.log(this.state.bidInfoScreen.winnerSuitImage);
    console.log(gamestate.winnerSuitImage);
    console.log('gamestate.winnnersuit -- '+gamestate.winnerSuit)
    if (this.state.bidInfoScreen.winnerSide === 'N') {
      gamestate.StartSeat = 'E';
      gamestate.DummySeat = 'S';
    } else if (this.state.bidInfoScreen.winnerSide === 'E') {
      gamestate.StartSeat = 'S';
      gamestate.DummySeat = 'W';
    } else if (this.state.bidInfoScreen.winnerSide === 'S') {
      gamestate.StartSeat = 'W';
      gamestate.DummySeat = 'N';
    } else if (this.state.bidInfoScreen.winnerSide === 'W') {
      gamestate.StartSeat = 'N';
      gamestate.DummySeat = 'E';
    }
    gamestate.winnerSuit = this.state.bidInfoScreen.winnerSuit;
    gamestate.winnerLevel = this.state.bidInfoScreen.winnerLevel;
    gamestate.winnerSide = this.state.bidInfoScreen.winnerSide;
    gamestate.winnerSuitImage = this.state.bidInfoScreen.winnerSuitImage;
    gamestate.winnerdblRedbl = this.state.bidInfoScreen.winnerdblRedbl;
    let playScreen = { ...this.state.playScreen };
    playScreen.southUser = gamestate.myUsername;
    playScreen.northUser = gamestate.myPartnername;
    playScreen.westUser = gamestate.myLHOname;
    playScreen.eastUser = gamestate.myRHOname;
    playScreen.TotalCardWidthNorth = PlayBodyWidth;
    playScreen.TotalCardWidthSouth = PlayBodyWidth;
    console.log('goto to play from bidding:');
    console.log(gamestate);
    let myHand = gamestate.hands;
    console.log(myHand);
    let eastHands = [];
    let northHands = [];
    let westHands = [];
    let eastCardsShown = [];
    let northCardsShown = [];
    let westCardsShown = [];
    let isCurrentDummy = gamestate.DummySeat == gamestate.mySeat ? true : false;
    myHand.forEach((h, key) => {
      let dummySide = false;
      if (key === 'S' && gamestate.contractinfo.declarer === 'N') {
        dummySide = true;
      } else if (key === 'W' && gamestate.contractinfo.declarer === 'E') {
        dummySide = true;
      } else if (key === 'N' && gamestate.contractinfo.declarer === 'S') {
        dummySide = true;
      } else if (key === 'E' && gamestate.contractinfo.declarer === 'W') {
        dummySide = true;
      }
      if (this.state.iskibitzer || !dummySide) {
        let claimCardData = this.displayClaimCards(h.cards);
        if (h.side === gamestate.myRHOSeat) {
          eastHands = claimCardData.cardSide;
          eastCardsShown = claimCardData.cardsShown;
        } else if (h.side === gamestate.myLHOSeat) {
          westHands = claimCardData.cardSide;
          westCardsShown = claimCardData.cardsShown;
        } else if (h.side === gamestate.myPartnerSeat) {
          northHands = claimCardData.cardSide;
          northCardsShown = claimCardData.cardsShown;
        }
      }
    });
    playScreen.eastHands = eastHands;
    playScreen.westHands = westHands;
    playScreen.northHands = northHands;
    playScreen.eastCardsShown = eastCardsShown;
    playScreen.northCardsShown = northCardsShown;
    playScreen.westCardsShown = westCardsShown;
    this.setState(
      { gamestate, playScreen, currentScreen: SCREENS.PLAY_SCREEN },
      () => {
        setTimeout(() => {
          let playScreen = { ...this.state.playScreen };
          //Check for any Undo Request
          if (e.undo) {
            if (
              !isCurrentDummy &&
              (e.undo.side === gamestate.mySeat ||
                (e.undo.side === gamestate.myPartnerSeat &&
                  gamestate.DummySeat === gamestate.myPartnerSeat))
            ) {
              playScreen.disableTkBk = false;
              playScreen.undoAsked = true;
              playScreen.reqUndoimg = svgImages.iconCancel;
            }
            playScreen = this.undoRequestState(e.undo, playScreen, gamestate);
          }
          if (e.claim) {
            if (e.claim.side === gamestate.mySeat) {
              this.setState({
                claimRequested: true,
                showClaimPopup: true,
                showSubMenuPopup: false
              });
            }
            playScreen = this.claimRequestState(e.claim, playScreen, gamestate);
          }
          if (e.changedeal) {
            let changeDealUser = '';
            if (e.changedeal.requestee) {
              changeDealUser = jid(e.changedeal.requestee)._local.toLowerCase();
            }
            let isChangeDeal = true;
            let changeDealType = e.changedeal.type;
            this.setState({ isChangeDeal, changeDealType, changeDealUser });
            if (e.changedeal.status === 'active') {
              let changeDealResponseText = "Waiting for other players' response...";
              this.setState({
                changeDealResponed: true,
                changeDealResponseText
              });
            }
          }
          this.setState({ playScreen });
          //   gamestate.hands.forEach(h => {
          //     this.playComponent.current.displayCardsDummy(h);
          //     if (gamestate.StartSeat === gamestate.myLHOSeat) {
          //       let playScreen = { ...this.state.playScreen };
          //       playScreen.disabledummyCards_C = false;
          //       playScreen.disabledummyCards_D = false;
          //       playScreen.disabledummyCards_H = false;
          //       playScreen.disabledummyCards_S = false;
          //     }
          //   });
          //   clearInterval(interval);
        }, 1500);
      }
    );
  }

  displayClaimCards(hand) {
    var cardSide = [];
    let cardsShown = [];
    var idx = 0;
    console.log('claimHand');
    hand.forEach(card => {
      //56   console.log(card.suit);
      if (card.suit === 'S') {
        cardSide.push({
          rank: card.rank,
          suit: 'S',
          key: card.rank + 'S',
          SuiteColor: '#C000FF',
          idx: idx
        });
        //Numberof_S = Numberof_S + 1;
      } else if (card.suit === 'D') {
        cardSide.push({
          rank: card.rank,
          suit: 'D',
          key: card.rank + 'D',
          SuiteColor: '#04AEFF',
          idx: idx
        });
        //Numberof_D = Numberof_D + 1;
      } else if (card.suit === 'H') {
        cardSide.push({
          rank: card.rank,
          suit: 'H',
          key: card.rank + 'H',
          SuiteColor: '#FF0C3E',
          idx: idx
        });
        // Numberof_H = Numberof_H + 1;
      } else if (card.suit === 'C') {
        cardSide.push({
          rank: card.rank,
          suit: 'C',
          key: card.rank + 'C',
          SuiteColor: '#79E62B',
          idx: idx
        });
        // Numberof_C = Numberof_C + 1;
      }
      cardsShown[idx] = 'flex';
      idx++;
    });
    return { cardSide, cardsShown };
  }

  showTextBids(Txt, bid_side, bidInfoScreen, gamestate) {
    // console.log(Txt, bid_side, bidInfoScreen, gamestate);
    if (bid_side === gamestate.myRHOSeat) {
      bidInfoScreen.my_RHO_bid_level = Txt;
      bidInfoScreen.my_RHO_bid_type = Txt;
      bidInfoScreen.my_RHO_bid_suit_image = '';
      bidInfoScreen.my_RHO_bid_suit_border_color =
        styles.bidCirclePositionBodrAssActive;
      bidInfoScreen.my_RHO_bid_suit_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_RHO_bid_suit_text_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_RHO_bid_suit_image = '';
    } else if (bid_side === gamestate.mySeat) {
      bidInfoScreen.my_bid_level = Txt;
      bidInfoScreen.my_bid_type = Txt;
      bidInfoScreen.my_bid_suit_image = '';
      bidInfoScreen.my_bid_suit_border_color =
        styles.bidCirclePositionBodrAssActive;
      bidInfoScreen.my_bid_suit_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_bid_suit_image = '';
    } else if (bid_side === gamestate.myPartnerSeat) {
      bidInfoScreen.my_partner_bid_level = Txt;
      bidInfoScreen.my_partner_bid_type = Txt;
      bidInfoScreen.my_partner_bid_suit_image = '';
      bidInfoScreen.my_partner_bid_suit_border_color =
        styles.bidCirclePositionBodrAssActive;
      bidInfoScreen.my_partner_bid_suit_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_partner_bid_suit_text_color =
        styles.bidCircleTextAssActive;
      bidInfoScreen.my_partner_bid_suit_image = '';
    } else if (bid_side === gamestate.myLHOSeat) {
      bidInfoScreen.my_LHO_bid_level = Txt;
      bidInfoScreen.my_LHO_bid_type = Txt;
      bidInfoScreen.my_LHO_bid_suit_image = '';
      bidInfoScreen.my_LHO_bid_suit_border_color =
        styles.bidCirclePositionBodrAssActive;
      bidInfoScreen.my_LHO_bid_suit_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_LHO_bid_suit_text_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_LHO_bid_suit_image = '';
    }
    return bidInfoScreen;
  }

  updateDeskStateValues(value, callback) {
    this.setState(value, callback);
  }

  updateBiddingBox(value, callback) {
    let bidInfoScreen = { ...value };
    this.setState({ bidInfoScreen }, callback);
  }

  async joinTable() {
    this.props.setTableId({
      tableId: this.state.tableID
    })
    HOOL_CLIENT.joinTable(this.state.tableID, this.state.role);
    try {
      AsyncStorage.setItem('currentGameState', 'JoinTable');
      AsyncStorage.setItem('TableID', this.state.tableID);
      AsyncStorage.setItem('Role', this.state.role);
    } catch (e) {
      console.log(e);
    }
  }

  // to go back to home screen
  leaveTable() {
    HOOL_CLIENT.leaveTable(this.state.tableID);
    this.clearListeners();
    this.props.navigation.dispatch(CommonActions.navigate({
      name: "Home"
    }));
  }

  leaveSeat() {
    let tableId = this.state.tableID;
    console.log("leave seat in table id ", tableId)
    this.clearListeners();

    // this.props.navigation.navigate('JoinTable', {tableId: tableId, role: 'rover'});
    this.props.navigation.dispatch(CommonActions.navigate({
      name: "JoinTable",
      params: {
        // loadScreen: SCREENS.JOIN_TABLE,
        tableID: tableId,
        role: 'rover'
      }
    }));
  }

  loadActiveNormalStateBasedOnWinner = (bid_suit_image, bidSide, bidSuit, gameState) =>{
    var suitImage = ''
    gameState.bidInfo.map(state => {
      if(state.side === bidSide && state.won != undefined && state.won){
        if (state.suit === bidSuit) {
          suitImage =  svgImages.clubs
        }else if (state.suit === bidSuit) {
          suitImage = svgImages.diamond
        }else if (state.suit === bidSuit) {
          suitImage = svgImages.hearts
        }else if (state.suit === bidSuit) {
          suitImage = svgImages.spade
        }
      }else{
        if (bidSuit === 'C') {
          suitImage =  svgImages.clubsWhite
        }else if (bidSuit === 'D') {
          suitImage = svgImages.diamondsWhite
        }else if (bidSuit === 'H') {
          suitImage = svgImages.heartsWhite
        }else if (bidSuit === 'S') {
          suitImage = svgImages.spadesWhite
        }
      }
    });
    return suitImage
  }

  getStylesBasedonWon = (bidSide, bidSuit, gameState) =>{
    var bid_suit_text_color = ''
    gameState.bidInfo.map(side => {
      if(side.side === bidSide){
        if (bidSuit === 'C') {
          bid_suit_text_color =  styles.bidCircleTextClubsActive
        }else if (bidSuit === 'D') {
          bid_suit_text_color =  styles.bidCircleTextDiamondActive
        }else if (bidSuit === 'H') {
          bid_suit_text_color =  styles.bidCircleTextHeartsActive
        }else if (bidSuit === 'S') {
          bid_suit_text_color =  styles.bidCircleTextSpadeActive
        }
      }else{
        if (bidSuit === 'C') {
          bid_suit_text_color =  styles.bidCircleTextClubsInActive
        }else if (bidSuit === 'D') {
          bid_suit_text_color =  styles.bidCircleTextDiamondInActive
        }else if (bidSuit === 'H') {
          bid_suit_text_color =  styles.bidCircleTextHeartsInActive
        }else if (bidSuit === 'S') {
          bid_suit_text_color =  styles.bidCircleTextSpadeInActive
        }
      }
    });
    return bid_suit_text_color
  }

  getTextColor= (bidSide, bidSuit, gameState) =>{
    var bid_winnerTextColor = ''
    gameState.bidInfo.map(side => {
      if(side.side === bidSide){
        if (bidSuit === 'C') {
          bid_winnerTextColor = styles.bidCircleTextClubsActive
        }else if (bidSuit === 'D') {
          bid_winnerTextColor =  styles.bidCircleTextDiamondActive
        }else if (bidSuit === 'H') {
          bid_winnerTextColor = styles.bidCircleTextHeartsActive
        }else if (bidSuit === 'S') {
          bid_winnerTextColor =   styles.bidCircleTextSpadeActive
        }
      }else{
        if (bidSuit === 'C') {
          bid_winnerTextColor =  styles.bidCircleTextClubsInActive
        }else if (bidSuit === 'D') {
          bid_winnerTextColor =  styles.bidCircleTextDiamondInActive
        }else if (bidSuit === 'H') {
          bid_winnerTextColor =  styles.bidCircleTextHeartsInActive
        }else if (bidSuit === 'S') {
          bid_winnerTextColor =  styles.bidCircleTextSpadeInActive
        }
      }
    });
    return bid_winnerTextColor
  }

  getBorderColor= (bidSide, bidSuit, gameState) =>{
    var bid_suit_border_color = ''
    console.log("----gameState-------", gameState.bidInfo);
    gameState.bidInfo.map(side => {
      if(side.side === bidSide){
        if (bidSuit === 'C') {
          bid_suit_border_color =  styles.bidCirclePositionClubsActive
        }else if (bidSuit === 'D') {
          bid_suit_border_color =  styles.bidCirclePositionDiamondActive
        }else if (bidSuit === 'H') {
          bid_suit_border_color =  styles.bidCirclePositionHeartsActive
        }else if (bidSuit === 'S') {
          bid_suit_border_color =  styles.bidCirclePositionSpadeActive
        }
      }else{
        if (bidSuit === 'C') {
          bid_suit_border_color =  styles.bidCirclePositionClubsInActive
        }else if (bidSuit === 'D') {
          bid_suit_border_color =  styles.bidCirclePositionDiamondInActive
        }else if (bidSuit === 'H') {
          bid_suit_border_color =  styles.bidCirclePositionHeartsInActive
        }else if (bidSuit === 'S') {
          bid_suit_border_color =  styles.bidCirclePositionSpadeInActive
        }
      }
    });
    return bid_suit_border_color
  }

  updateBiddingDetails(bid, bidInfoScreen, gamestate) {
    var bid_suit_image = '';
    var bid_suit_color = '';
    var bid_suit_text_color = '';
    var bid_suit_border_color = '';
    var bid_winnerTextColor = '';
    var bid_level = '';
    if (bid.type == 'level') {
      // update 'won' status if necessary
      bid_level = bid.level;
      if (bid.suit === 'C') {
        bid_suit_image = svgImages.clubs
        bid_suit_color = styles.clubsImageActive;
        bid_suit_text_color = styles.bidCircleTextClubsActive;
        bid_suit_border_color = styles.bidCirclePositionClubsActive;
        bid_winnerTextColor = '#79E62B';
      }else if (bid.suit === 'D') {
        bid_suit_image = svgImages.diamond
        bid_suit_color = styles.diamondImageActive;
        bid_suit_text_color = styles.bidCircleTextDiamondActive;
        bid_suit_border_color = styles.bidCirclePositionDiamondActive;
        bid_winnerTextColor = '#04AEFF';
      }else if (bid.suit === 'H') {
        bid_suit_image = svgImages.hearts
        bid_suit_color = styles.heartsImageActive;
        bid_suit_text_color = styles.bidCircleTextHeartsActive;
        bid_suit_border_color = styles.bidCirclePositionHeartsActive;
        bid_winnerTextColor = '#FF0C3E';
      }else if (bid.suit === 'S') {
        bid_suit_image = svgImages.spade;
        bid_suit_color = styles.spadeImageActive;
        bid_suit_text_color = styles.bidCircleTextSpadeActive;
        bid_suit_border_color = styles.bidCirclePositionSpadeActive;
        bid_winnerTextColor = '#C000FF';
      }else if (bid.suit === 'NT') {
        bid_suit_image = svgImages.ntActive;
        bid_suit_color = styles.bidCircleTextAssActive;
        bid_suit_text_color = styles.bidCircleTextNTActive;
        bid_suit_border_color = styles.bidCirclePositionNTActive;
        //bid_level = bid.level + 'NT';
        bid_winnerTextColor = '#FFE81D';
      }

      if (bid.won) {
        bidInfoScreen.winnerLevel = bid.level;
        bidInfoScreen.winnerSide = bid.side;
        bidInfoScreen.winnerSuit = bid.suit;
        console.log("console.log --- winnersuit ---"+bidInfoScreen.winnerSuit);
        bidInfoScreen.winnerSuitImage = bid_suit_image;
        console.log("console.log --- winnersuit_image ---"+bidInfoScreen.winnerSuitImage);
        bidInfoScreen.bid_raised_cnt = Number(bidInfoScreen.bid_raised_cnt) + 1;
        bidInfoScreen.winnerTextColor = bid_winnerTextColor;
      }
      if (bid.side === gamestate.myRHOSeat) {
        bidInfoScreen.my_RHO_bid_level = bid_level;
        bidInfoScreen.my_RHO_bid_suit_image = bid_suit_image;
        bidInfoScreen.my_RHO_bid_suit_border_color = bid_suit_border_color;
        bidInfoScreen.my_RHO_bid_suit_color = bid_suit_color;
        bidInfoScreen.my_RHO_bid_suit_text_color = bid_suit_text_color;
        bidInfoScreen.my_RHO_bid_type = 'level';
      } else if (bid.side === gamestate.mySeat) {
        bidInfoScreen.my_bid_level = bid_level;
        bidInfoScreen.my_bid_suit_image = bid_suit_image;
        bidInfoScreen.my_bid_suit_border_color = bid_suit_border_color;
        bidInfoScreen.my_bid_suit_color = bid_suit_color;
        bidInfoScreen.my_bid_suit_text_color = bid_suit_text_color;
        bidInfoScreen.bid_type = 'level';
      } else if (bid.side === gamestate.myPartnerSeat) {
        bidInfoScreen.my_partner_bid_level = bid_level;
        bidInfoScreen.my_partner_bid_suit_border_color = bid_suit_border_color;
        bidInfoScreen.my_partner_bid_suit_color = bid_suit_color;
        bidInfoScreen.my_partner_bid_suit_text_color = bid_suit_text_color;
        bidInfoScreen.my_partner_bid_suit_image = bid_suit_image;
        bidInfoScreen.my_partner_bid_type = 'level';
      } else if (bid.side === gamestate.myLHOSeat) {
        bidInfoScreen.my_LHO_bid_level = bid_level;
        bidInfoScreen.my_LHO_bid_suit_image = bid_suit_image;
        bidInfoScreen.my_LHO_bid_suit_border_color = bid_suit_border_color;
        bidInfoScreen.my_LHO_bid_suit_color = bid_suit_color;
        bidInfoScreen.my_LHO_bid_suit_text_color = bid_suit_text_color;
        bidInfoScreen.my_LHO_bid_type = 'level';
      }
      if (bid.won) {
        console.log('Entered into won');
        if (Number(bidInfoScreen.bid_raised_cnt) < 4) {
          if (
            gamestate.myLHOSeat === bid.side ||
            gamestate.myRHOSeat === bid.side
          ) {
            console.log('Enable Bid');
            bidInfoScreen.showBiddingBox = true;
            bidInfoScreen.DirectionTextMySeat = '#DBFF00';
            bidInfoScreen.southUserText = '#DBFF00';
            bidInfoScreen.DirectionTextPartner = '#DBFF00';
            bidInfoScreen.northUserText = '#FFE81D';
            bidInfoScreen.DirectionTextRHO = '#6D6D6D';
            bidInfoScreen.eastUserText = '#6D6D6D';
            bidInfoScreen.DirectionTextLHO = '#6D6D6D';
            bidInfoScreen.westUserText = '#6D6D6D';
            bidInfoScreen.EnableDouble = false;
            bidInfoScreen.showEligibleBids = true;
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                nextSide(bid.side),
                bidInfoScreen,
                true,
                false
              );
            }
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                bid.side,
                bidInfoScreen,
                false,
                true
              );
            }
            //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'});
          } else if (
            gamestate.side === bid.side ||
            gamestate.myPartnerSeat === bid.side
          ) {
            //highlight oppent bid chance
            bidInfoScreen.DirectionTextRHO = '#DBFF00';
            bidInfoScreen.eastUserText = '#DBFF00';
            bidInfoScreen.DirectionTextLHO = '#DBFF00';
            bidInfoScreen.westUserText = '#DBFF00';
            bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
            bidInfoScreen.southUserText = '#6D6D6D';
            bidInfoScreen.DirectionTextPartner = '#6D6D6D';
            bidInfoScreen.northUserText = '#6D6D6D';
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                nextSide(bid.side),
                bidInfoScreen,
                true,
                false
              );
            }
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                bid.side,
                bidInfoScreen,
                false,
                true
              );
            }
          } else {
            console.log('no action')
            // console.log(gamestate, bid.side);
          }
        } else if (Number(bidInfoScreen.bid_raised_cnt) >= 3) {
          //EnableOnlyDoubleorRedouble
          if (
            gamestate.myLHOSeat === bid.side ||
            gamestate.myRHOSeat === bid.side
          ) {
            console.log('33e3423 ->' + bidInfoScreen.bid_raised_cnt);
            bidInfoScreen.showDblOrRedbl = 'dbl';
            bidInfoScreen.EnableDouble = false;
            bidInfoScreen.DirectionTextMySeat = '#DBFF00';
            bidInfoScreen.southUserText = '#DBFF00';
            bidInfoScreen.DirectionTextPartner = '#DBFF00';
            bidInfoScreen.northUserText = '#DBFF00';
            bidInfoScreen.DirectionTextRHO = '#6D6D6D';
            bidInfoScreen.eastUserText = '#6D6D6D';
            bidInfoScreen.DirectionTextLHO = '#6D6D6D';
            bidInfoScreen.westUserText = '#6D6D6D';
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                nextSide(bid.side),
                bidInfoScreen,
                true,
                false
              );
            }
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                bid.side,
                bidInfoScreen,
                false,
                true
              );
            }
            bidInfoScreen.bid_raised_cnt =
              Number(bidInfoScreen.bid_raised_cnt) + 1;
            bidInfoScreen.showBiddingBox = true;
            bidInfoScreen.isOnlyDblOrReDbl = true;
          }
          if (
            gamestate.side === bid.side ||
            gamestate.myPartnerSeat === bid.side
          ) {
            bidInfoScreen.DirectionTextRHO = '#DBFF00';
            bidInfoScreen.eastUserText = '#DBFF00';
            bidInfoScreen.DirectionTextLHO = '#DBFF00';
            bidInfoScreen.westUserText = '#DBFF00';
            bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
            bidInfoScreen.southUserText = '#6D6D6D';
            bidInfoScreen.DirectionTextPartner = '#6D6D6D';
            bidInfoScreen.northUserText = '#6D6D6D';
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                nextSide(bid.side),
                bidInfoScreen,
                true,
                false
              );
            }
            if (this.bidComponent && this.bidComponent.current) {
              bidInfoScreen = this.bidComponent.current.toggleSideSelection(
                bid.side,
                bidInfoScreen,
                false,
                true
              );
            }
          }
        } else {
          console.log('Entered into else part');
        }
      }
    } else if (bid.type === 'redouble') {
      //bidSpan.querySelector('span').innerHTML += ' XX'
      bidInfoScreen = this.showTextBids(
        'XX',
        bid.side,
        bidInfoScreen,
        gamestate
      );
      // winnerSide:bid.side,
      bidInfoScreen.winnerdblRedbl = 'x4';
      this.checkStateUpdatePlay();
    } else if (bid.type === 'double') {
      //bidSpan.querySelector('span').innerHTML = 'X'
      //console.log('inside double');
      bidInfoScreen = this.showTextBids(
        'X',
        bid.side,
        bidInfoScreen,
        gamestate
      );
      // highlight the double
      if (bid.won) {
        if (gamestate.side === bidInfoScreen.winnerSide) {
          bidInfoScreen.showDblOrRedbl = 'Redbl';
          bidInfoScreen.showBiddingBox = true;
          bidInfoScreen.EnableRedouble = false;
          bidInfoScreen.EnableDouble = true;
          bidInfoScreen.isOnlyDblOrReDbl = true;
          //showOnlyRedouble
        } else if (gamestate.myPartnerSeat === bidInfoScreen.winnerSide) {
          bidInfoScreen.DirectionTextLHO = '#6D6D6D';
          bidInfoScreen.westUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextRHO = '#6D6D6D';
          bidInfoScreen.eastUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
          bidInfoScreen.southUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextPartner = '#DBFF00';
          bidInfoScreen.northUserText = '#DBFF00';
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              nextSide(bid.side),
              bidInfoScreen,
              true,
              false
            );
          }
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              bid.side,
              bidInfoScreen,
              false,
              true
            );
          }
          //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'});
        } else if (gamestate.myLHOSeat === bidInfoScreen.winnerSide) {
          bidInfoScreen.DirectionTextLHO = '#DBFF00';
          bidInfoScreen.westUserText = '#DBFF00';
          bidInfoScreen.DirectionTextRHO = '#6D6D6D';
          bidInfoScreen.eastUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
          bidInfoScreen.southUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextPartner = '#6D6D6D';
          bidInfoScreen.northUserText = '#6D6D6D';
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              nextSide(bid.side),
              bidInfoScreen,
              true,
              false
            );
          }
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              bid.side,
              bidInfoScreen,
              false,
              true
            );
          }
          //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'});
        } else if (gamestate.myRHOSeat === bidInfoScreen.winnerSide) {
          bidInfoScreen.DirectionTextRHO = '#DBFF00';
          bidInfoScreen.eastUserText = '#DBFF00';
          bidInfoScreen.DirectionTextLHO = '#6D6D6D';
          bidInfoScreen.westUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
          bidInfoScreen.southUserText = '#6D6D6D';
          bidInfoScreen.DirectionTextPartner = '#6D6D6D';
          bidInfoScreen.northUserText = '#6D6D6D';
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              nextSide(bid.side),
              bidInfoScreen,
              true,
              false
            );
          }
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              bid.side,
              bidInfoScreen,
              false,
              true
            );
          }
          //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'});
        }
        // winnerSide:bid.side,
        bidInfoScreen.winnerdblRedbl = 'x2';
      }
    } else if (bid.type === 'pass') {
      //bidSpan.querySelector('span').innerHTML = 'P'
      bidInfoScreen = this.showTextBids(
        'P',
        bid.side,
        bidInfoScreen,
        gamestate
      );
      if (bid.won) {
        console.log('pass won' + ' winner ' + bidInfoScreen.winnerSide);
        if (bidInfoScreen.winnerSide === '') {
          console.log('redrew deal', gamestate);
          //RedrawtheDeal
          gamestate.trickScoreNS = this.state.TrickScoreNS;
          gamestate.trickScoreEW = this.state.TrickScoreEW;
          console.log(gamestate);
          setTimeout(() => {
            console.log('redrew deal timer');
            this.setState({ gamestate, currentScreen: SCREENS.SCOREBOARD });
          }, 2000);
        } else {
          //pass won
          console.log('pass won go to play');
          this.checkStateUpdatePlay();
        }
      }
      if (
        bidInfoScreen.winnerSide !== '' &&
        bidInfoScreen.winnerdblRedbl === '' &&
        bid.won
      ) {
        if (
          gamestate.myLHOSeat === bidInfoScreen.winnerSide ||
          gamestate.myRHOSeat === bidInfoScreen.winnerSide
        ) {
          //console.log('5');
          bidInfoScreen.DirectionTextMySeat = '#DBFF00';
          bidInfoScreen.southUserText = '#DBFF00';
          bidInfoScreen.DirectionTextPartner = '#DBFF00';
          bidInfoScreen.northUserText = '#DBFF00';
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              bidInfoScreen.winnerSide,
              bidInfoScreen,
              false,
              false
            );
          }
          //​this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00'});
        }
        if (
          gamestate.mySeat === bidInfoScreen.winnerSide ||
          gamestate.myPartnerSeat === bidInfoScreen.winnerSide
        ) {
          bidInfoScreen.DirectionTextRHO = '#DBFF00';
          bidInfoScreen.eastUserText = '#DBFF00';
          bidInfoScreen.DirectionTextLHO = '#DBFF00';
          bidInfoScreen.westUserText = '#DBFF00';
          if (this.bidComponent && this.bidComponent.current) {
            bidInfoScreen = this.bidComponent.current.toggleSideSelection(
              bidInfoScreen.winnerSide,
              bidInfoScreen,
              false,
              false
            );
          }
        }
      }
    } else {
      //bidSpan.querySelector('span').innerHTML = bid.type
    }
    console.log("------5------------->", bidInfoScreen);
    return bidInfoScreen;
  }
  updateBiddingHistory(bidInfo, contractInfo) {
    // console.log(contractInfo);
    let biddings = { N: [], E: [], S: [], W: [] };
    let lastBidRound = 1;

    bidInfo.forEach(bidDetail => {
      biddings[bidDetail.side].push(bidDetail);
      if (parseInt(bidDetail.round) > lastBidRound) {
        lastBidRound = parseInt(bidDetail.round);
      }
    });
    // console.log(biddings);

    let keys = [...SIDES.keys()];
    keys.forEach(side => {
      let updatedBiddings = [];
      let currentSideRound = 1;
      let lastRoundBidObj = null;
      biddings[side].forEach((bid, idx) => {
        if (updatedBiddings.length != 0) {
          lastRoundBidObj = updatedBiddings[updatedBiddings.length - 1];
          let lastRound = parseInt(lastRoundBidObj.round);
          let roundDiff = parseInt(bid.round) - (lastRound + 1);

          if (roundDiff == 0) {
            updatedBiddings.push(bid);
            lastRoundBidObj = bid;
          } else {
            for (let index = 1; index <= roundDiff; index++) {
              let dummyBid = {
                type: 'dummy',
                round: (lastRound + index).toString()
              };
              if (lastRoundBidObj.won) {
                dummyBid.type = lastRoundBidObj.type;
                dummyBid.level = lastRoundBidObj.level;
                dummyBid.side = lastRoundBidObj.side;
                dummyBid.suit = lastRoundBidObj.suit;
                dummyBid.dummy_won = lastRoundBidObj.won;
              }

              updatedBiddings.push(dummyBid);
            }
            updatedBiddings.push(bid);
            lastRoundBidObj = bid;
          }
        } else {
          updatedBiddings.push(bid);
          lastRoundBidObj = bid;
        }
        if (currentSideRound < parseInt(bid.round)) {
          currentSideRound = parseInt(bid.round);
        }
      });
      // console.log(currentSideRound, lastBidRound, side);
      if (currentSideRound < lastBidRound) {
        let roundDiff = lastBidRound - currentSideRound;
        for (let index = 1; index <= roundDiff; index++) {
          let dummyBid = {
            type: 'dummy',
            round: (currentSideRound + index).toString()
          };
          // console.log(lastRoundBidObj);
          if (lastRoundBidObj.type != 'double' && lastRoundBidObj.won) {
            dummyBid.type = lastRoundBidObj.type;
            dummyBid.level = lastRoundBidObj.level;
            dummyBid.side = lastRoundBidObj.side;
            dummyBid.suit = lastRoundBidObj.suit;
            dummyBid.dummy_won = lastRoundBidObj.won;
          }

          updatedBiddings.push(dummyBid);
        }
      }
      updatedBiddings.forEach((updatedBid, idx) => {
        let nextRoundBid = null;
        if (idx < updatedBiddings.length - 1) {
          nextRoundBid = updatedBiddings[idx + 1];
        }
        // console.log(nextRoundBid);
        if (nextRoundBid != null && nextRoundBid.type == 'redouble') {
          updatedBid.force_won = 'true';
        }
        if (nextRoundBid != null && nextRoundBid.type == 'redouble') {
          updatedBid.force_line = 'true';
        }
        if (
          updatedBid.type == 'double' &&
          contractInfo != null &&
          contractInfo.double &&
          contractInfo.redouble == 'false'
        ) {
          updatedBid.force_won = 'true';
        }
        if (
          updatedBid.type == 'level' &&
          updatedBid.dummy_won &&
          contractInfo != null &&
          contractInfo.suit == updatedBid.suit &&
          contractInfo.level == updatedBid.level
        ) {
          updatedBid.force_won = 'true';
        }
      });
      biddings[side] = updatedBiddings;
    });
    console.log(biddings);
    this.setState({ biddings, lastBidRound });
  }
  playBodyStyle() {
    let bodyStyles = {
      right: 0,
      marginLeft: scale(90),
      height: '100%'
    };
    if (
      this.state.currentScreen == SCREENS.BID_INFO ||
      this.state.currentScreen == SCREENS.SCOREBOARD
    ) {
      bodyStyles = {
        ...bodyStyles,
        flexDirection: 'column',
        justifyContent: 'space-between'
      };
    } else {
      bodyStyles = {
        ...bodyStyles,
        justifyContent: 'center',
        alignItems: 'center'
      };
    }
    return bodyStyles;
  }

  showTableMessages(){
    this.updateDeskStateValues({ showChatWindow: true})
    this.props.isTableUser({
      isTableUser: true
    })
    this.props.setIndex({
      index: 1
    })
    this.props.setLastUserChat({
      lastUserChat: 'Table Messages'
    })
  }
  popupViewBodyStyle() {
    let bodyStyles = {
      position: 'absolute',
      right: 0,
      marginLeft: scale(90),
      width: '100%',
      height: '100%',
      zIndex: 9999
    };
    if (this.state.showHistory || this.state.showChatWindow || this.state.showCheatSheet ){ //|| this.state.showUserProfile
      bodyStyles = {
        ...bodyStyles,
        marginLeft: scale(0),
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0A0A0A'
      };
    } else if (
      this.state.showSubMenuPopup ||
      this.state.showChangeDealPopup ||
      this.state.isChangeDeal || 
      this.props.addPlayer ||
      this.props.addFriend
    ) {
      bodyStyles = {
        ...bodyStyles,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0a0a0acf',
        zIndex: 99
      };
    } else {
      bodyStyles = {
        ...bodyStyles,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: '#0a0a0acf'
      };
    }
    return bodyStyles;
  }

  onPlayerSelect = (playerType, tableID) => {
    console.log('this table id :', tableID, this.props.currentDirection)
    if(playerType == 'Bots'){
      HOOL_CLIENT.invite(tableID, API_ENV.botSuffix,this.props.currentDirection)
      this.props.addBotPlayer({
          addPlayer: false,
          addBot: true,
          direction: this.props.currentDirection
      })
    }else if(playerType == 'Kibitz All'){
      HOOL_CLIENT.joinTable(this.state.tableID, 'kibitzer')
      this.props.addBotPlayer({
        addPlayer: false,
        addBot: false,
        direction: ''
      })
    }else{
      this.props.addFriendPlayer({
        addFriend: true,
        tablesId: tableID,
        direction: this.props.currentDirection
      })
      // this.props.setFriendsList({
      //   friendsList: this.props.friendsList
      // })
    }
  }

  onCancelPlayerSelect = (closeFriendsList) => {
    if(closeFriendsList){
      this.props.closeFriendsList({
        addFriend: false,
      })
    }else{
      this.props.cancelPlayerSelect({
        addPlayer: false,
        direction: undefined
      })
    }
  }

  getClaimNumberStyle(currentNumber) {
    let textStyles =
      (this.state.claimType === 'CLAIM' &&
        currentNumber <= this.state.remainingTricks) ||
      (this.state.claimType === 'CONCEDE' &&
        currentNumber === this.state.remainingTricks)
        ? styles.claimTxtWhite
        : styles.claimTxtBlack;
    if (this.state.playScreen.claimConcedeNumber === currentNumber) {
      textStyles = styles.claimTxtBlack;
    } else if (this.state.claimRequested) return styles.claimTxtBlack;
    return textStyles;
  }

  getClaimBoxStyle(currentNumber) {
    return this.state.playScreen.claimConcedeNumber === currentNumber
      ? styles.claimRequestNumberBodyRowBoxGreen
      : styles.claimRequestNumberBodyRowBox;
  }

  getClaimBoxRequestStyle() {
    return this.state.claimRequested
      ? styles.claimRequestNumberBodyRowBoxGreen
      : styles.claimRequestNumberBodyRowBox;
  }

  disabledClaimButton(currentNumber) {
    if (this.state.claimRequested) return true;
    return (this.state.claimType === 'CLAIM' &&
      currentNumber <= this.state.remainingTricks) ||
      (this.state.claimType === 'CONCEDE' &&
        currentNumber === this.state.remainingTricks)
      ? false
      : true;
  }

  getClaimCancelButton() {
    return this.state.claimRequested
      ? svgImages.cancelRed
      : svgImages.cancelBlack;
  }

  getClaimRequestButton() {
    return this.state.claimRequested
      ? svgImages.lock
      : this.state.playScreen.claimConcedeNumber === 0
      ? svgImages.bidNonActive
      : svgImages.bidActive;
  }

  changeDealRequest(type) {
    this.setState({ changeRequest: true, changeDealType: type });
    HOOL_CLIENT.changedeal(this.state.tableID, this.state.gamestate.mySeat, type);
    let changeDealResponseText = "Waiting for other players' response...";
    this.setState({
      showSubMenuPopup: false,
      showChangeDealPopup: false,
      changeDealResponed: true,
      changeDealResponseText
    });
  }

  claimConcedeRequest(type) {
    if (type === 'request') {
      this.setState({ claimRequested: true });
    } else {
      this.setState({ claimRequested: false });
    }
    let claimData = {
      side: this.state.gamestate.mySeat,
      amount:
        this.state.claimType === 'CLAIM'
          ? this.state.playScreen.claimConcedeNumber
          : 0
    };
    HOOL_CLIENT.claim(this.state.tableID, type, claimData);
    if (type !== 'request')
      this.setState({ showClaimPopup: false, showSubMenuPopup: false });
  }

  selectClaimNumber(currentNumber) {
    let playScreen = this.state.playScreen;
    playScreen.claimConcedeNumber = currentNumber;
    this.setState({ playScreen });
  }

  updateChatInfo(key, value){
    this.props.setState({key: value})
  }

  selectClaimType(type) {
    if (!this.state.claimRequested && this.state.claimType != type) {
      let playScreen = this.state.playScreen;
      playScreen.claimConcedeNumber = 0;
      if (type === 'CONCEDE')
        playScreen.claimConcedeNumber = this.state.remainingTricks;
      this.setState({ playScreen, claimType: type });
    }
  }

  getClaimPopupTitle(type) {
    return this.state.claimType === type
      ? styles.claimTxtGreen
      : styles.claimTxtWhite;
  }

  changeSeat(seat, tableID) {
      if (seat === Strings.directionEast) {
        HOOL_CLIENT.joinTable(tableID, Strings.directionEast);
      } else if (seat === Strings.directionNorth) {
        HOOL_CLIENT.joinTable(tableID, Strings.directionNorth);
      } else if (seat === Strings.directionSouth) {
        HOOL_CLIENT.joinTable(tableID, Strings.directionSouth);
      } else if (seat === Strings.directionWest) {
        HOOL_CLIENT.joinTable(tableID, Strings.directionWest);
      }
      this.props.addPlayers({
        addPlayer: false,
        direction: ''
      })
}

  render() {   
    return (
      <SafeAreaView edges={['top', 'left']} style={{ backgroundColor: '#0a0a0a', flex:1}}>
        <View style={{ height: '100%'}}>
          <View
            style={{
              position: 'absolute',
              left: 0,
              width: scale(90),
              backgroundColor: '#151515',
              height: '100%',
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                backgroundColor: '#151515',
                width: scale(90),
                height: scale(135),
                bottom: 0,
                position: 'absolute',
                zIndex: 2
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  width: scale(90),
                  height: scale(45),
                }}
              >
                <View style={this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO ?
                  { ...styles.MenuBox, borderTopWidth: 0}: { ...styles.MenuBox}}>
                  <SvgXml height={scale(45)} width={scale(45)} xml={SettingsDisabled} />
                </View>
                <TouchableHighlight
                  style={this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO ?
                    { ...styles.MenuBox, borderTopWidth: 0, borderRightWidth: 0}: { ...styles.MenuBox, borderRightWidth: 0}}
                  onPress={() => this.toggleSubMenuPopup()}
                >
                  {this.state.showSubMenuPopup ||
                    this.state.showClaimPopup ||
                    this.state.showChangeDealPopup ? (
                    <SvgXml
                      height={scale(45)}
                      width={scale(44.95)}
                      xml={svgImages.iconCancel}
                    />
                  ) : (
                    <SvgXml height={scale(45)} width={scale(44.95)} xml={Flag} />
                  )}
                </TouchableHighlight>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: scale(45)
                }}
              >
                <View style={styles.MenuBox}>
                  <TouchableOpacity onPress={() => this.toggleCheatSheet()}
                    disabled={this.state.currentScreen == SCREENS.JOIN_TABLE}
                  >
                  {
                      (this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                        || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD) ?
                        !this.state.showCheatSheet ? (
                          <SvgXml
                            height={scale(45)}
                            width={scale(45)}
                            xml={svgImages.cheat_Sheet_white}
                          />
                        ) : (
                          <SvgXml
                            height={scale(42)}
                            width={scale(44.95)}
                            xml={svgImages.iconCancel}
                          />
                        )
                        :
                        (<SvgXml
                          height={scale(45)}
                          width={scale(45)}
                          xml={svgImages.cheat_Sheet_diabled}
                        />)
                    }
                  </TouchableOpacity>
                </View>
                <View style={{...styles.MenuBox, borderRightWidth: 0}}>
                <TouchableOpacity
                    onPress={() => this.undoRequest()}
                    disabled={this.state.playScreen.disableTkBk || this.props.addPlayer || this.state.showCheatSheet ||
                      this.state.showHistory || this.state.showChatWindow || this.state.showSubMenuPopup || showOptionWhenRobotInTable(this.state.gamestate) || this.state.gamestate.myPartnername.includes(Strings.bot)}
                  >
                    <SvgXml
                      height={scale(45)}
                      width={scale(44.95)}
                      xml={this.state.playScreen.reqUndoimg}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              {/* <View style={{ ...styles.MenuBox }}>
                <TouchableOpacity onPress={() => this.toggleChat()} 
                  //disabled={true}
                  disabled={this.state.currentScreen == SCREENS.JOIN_TABLE}
                  // disabled={this.props.addPlayer}
                  // disabled={this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                  //   || this.state.currentScreen == SCREENS.JOIN_TABLE
                  //   || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD}
                >
                    {
                      (this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                        || this.state.currentScreen == SCREENS.JOIN_TABLE
                        || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD) ?
                      !this.state.showChatWindow ? (
                        <SvgXml
                          height={scale(45)}
                          width={scale(45)}
                          xml={svgImages.chatDisbaled}
                        />
                      ) : (
                        <SvgXml
                          height={scale(42)}
                          width={scale(45)}
                          xml={svgImages.iconCancel}
                        />
                      )
                      :
                      (<SvgXml
                        height={scale(45)}
                        width={scale(44.95)}
                        xml={svgImages.chatDisbaled}
                      />)
                    }
                </TouchableOpacity>
              </View> */}

              <View
                style={{
                  flexDirection: 'row',
                  height: scale(45)
                }}
              > 
                <TouchableOpacity 
                  // onPress={() => this.toggleChat()} 
                  // style={{opacity: '0.3'}}
                    disabled={true}
                    // disabled={this.props.addPlayer}
                    // disabled={this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                    //   || this.state.currentScreen == SCREENS.JOIN_TABLE
                    //   || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD}
                >
                  <View style={{ ...styles.MenuBox }}>
                    {
                      // (this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                      //   || this.state.currentScreen == SCREENS.JOIN_TABLE
                      //   || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD) ?
                      !this.state.showChatWindow ? (
                        <SvgXml
                          height={scale(45)}
                          width={scale(45)}
                          xml={svgImages.chatDisbaled}
                        />
                      ) : (
                        <SvgXml
                          height={scale(42)}
                          width={scale(45)}
                          xml={svgImages.iconCancel}
                        />
                      )
                      
                    }
                  </View>
                </TouchableOpacity>
                <View style={{...styles.MenuBox, borderRightWidth: 0}}>
                  <TouchableOpacity onPress={() => this.toggleHistory()}
                    disabled={this.state.currentScreen == SCREENS.JOIN_TABLE}>
                    {
                      (this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                        || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD) ?
                        !this.state.showHistory ? (
                          <SvgXml
                            height={scale(45)}
                            width={scale(44.95)}
                            xml={HistoryMenu}
                          />
                        ) : (
                          <SvgXml
                            height={scale(42)}
                            width={scale(44.95)}
                            xml={svgImages.iconCancel}
                          />
                        )
                        :
                        (<SvgXml
                          height={scale(45)}
                          width={scale(44.95)}
                          xml={svgImages.historySvgDisabled}
                        />)
                    }
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            {this.state.currentScreen == SCREENS.JOIN_TABLE ? (
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  height: totalHeight,
                  backgroundColor: '#151515',
                }}
              >
                {/* <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={require('../../assets/images/fast.png')} />
                  <Text style={styles.clmText}>Fast</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={require('../../assets/images/open.png')} />
                  <Text style={styles.clmText}>OPEN</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={require('../../assets/images/on.png')} />
                  <Text style={styles.clmText}>On</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={require('../../assets/images/player_ass.png')}
                  />
                  <Text style={styles.clmText}>OPEN</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={require('../../assets/images/cheat_sheet_ass.png')}
                  />
                  <Text style={styles.clmText}>OPEN</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={require('../../assets/images/public.png')} />
                  <Text style={styles.clmText}>Public</Text>
                </View> */}
              </View>
            ) : null}
            {this.state.currentScreen == SCREENS.SHARE_INFO||
            this.state.currentScreen == '' ? (
              <View
                style={isTablet?{
                  flexDirection: 'column',
                  width: '100%',
                  height: "62.5%",
                  backgroundColor: '#151515',
                }:{
                  flexDirection: 'column',
                  width: '100%',
                  height: "62.5%",
                  backgroundColor: '#151515',
                }}
              >
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(34),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }:{
                    width: '100%',
                    height: scale(21),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text style={styles.ContractTextHeader}>Contract</Text>
                </View>
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(33),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }:{width: '100%',
                    height: scale(23),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }}
                >
                  <View
                    style={{
                      width: scale(23),
                      height: '100%',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text style={styles.ContractTextHeader} />
                  </View>
                  <View
                    style={{
                      width: scale(44),
                      height: '100%',
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text style={styles.ContractMdlText} />
                  </View>
                  <View
                    style={{
                      width: scale(23),
                      height: '100%',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text style={styles.ContractTextHeader} />
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                    }}
                  >
                    <Text
                      style={(isTablet && Platform.OS == 'ios')? {
                        ...this.state.infoScreen.EWStyle,
                        marginTop: moderateVerticalScale(20),
                        letterSpacing: 0.1,
                      }:{
                        ...this.state.infoScreen.NSStyle,
                        marginTop: moderateVerticalScale(8),
                        letterSpacing: 0.4
                      }}
                    >
                      N • S
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={(isTablet && Platform.OS == 'ios')? {
                        ...this.state.infoScreen.NSSharingStyle,
                        marginTop: moderateVerticalScale(60),
                        letterSpacing: 0.1,
                      }:{
                        ...this.state.infoScreen.NSSharingStyle,
                        paddingTop: moderateVerticalScale(30),
                        letterSpacing: 0.1,
                      }}
                    >
                      SHARE
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom,
                        // borderRightWidth: 1
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center'
                    }}
                  >
                    <Text
                      style={(isTablet && Platform.OS == 'ios')? {
                        ...this.state.infoScreen.EWStyle,
                        marginTop: moderateVerticalScale(20),
                        letterSpacing: 0.1,
                      }:{
                        ...this.state.infoScreen.EWStyle,
                        marginTop: moderateVerticalScale(8),
                        letterSpacing: 0.4
                      }}
                    >
                      E • W
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={(isTablet && Platform.OS == 'ios')? {
                        ...this.state.infoScreen.EWSharingStyle,
                        marginTop: moderateVerticalScale(60),
                        letterSpacing: 0.1,
                      }:{
                        ...this.state.infoScreen.EWSharingStyle,
                        marginTop: moderateVerticalScale(30),
                        letterSpacing: 0.1,
                      }}
                    >
                      SHARE
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: verticalScale(1),
                    }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom,
                        // borderRightWidth: 1
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom
                      }}
                    />
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.currentScreen == SCREENS.BID_INFO  ? (
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  height: "62.5%",
                  zIndex: 1,
                  backgroundColor: '#151515'
                }}
              >
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(33),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }:{
                    width: '100%',
                    height: scale(21),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text
                    style={{
                      ...styles.ContractTextHeader,
                      letterSpacing: 0.7,
                      
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSide === ''
                      ? 'Contract'
                      : 'Leading Bid'}
                  </Text>
                </View>
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(34),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'//#0a0a0a
                  }:{
                    width: '100%',
                    height: scale(23),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }}
                >
                  <View
                    style={isTablet?{
                      width: scale(25),
                      height: scale(34),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }:{
                      width: scale(23),
                      height: scale(23),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'

                    }}
                  >
                    <Text
                      style={[
                        styles.ContractTextHeader,
                        { color: this.state.bidInfoScreen.winnerTextColor }
                      ]}
                    >
                      {this.state.bidInfoScreen.winnerSide}
                    </Text>
                  </View>
                  <View
                    style={isTablet?{
                      width: scale(42),
                      height: scale(34),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      paddingLeft: 22.5,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }:{
                      width: scale(44),
                      height: scale(23),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextSpade,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.spade}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextHearts,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.hearts}
                        />
                      </View>
                    ) : null}

                    {this.state.bidInfoScreen.winnerSuit === 'D' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextDiamond,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.diamond}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextClubs,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.clubs}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                        }}
                      >
                        <Text style={{...styles.ContractTextNT, marginEnd: moderateScale(5)}}>
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(15)}
                          height={scale(15)}
                          xml={svgImages.ntActive}
                        />
                      </View>
                    ) : null}
                  </View>
                  <View
                    style={{
                      width: scale(23),
                      height: scale(23),
                      //flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text
                      style={ isTablet ? {
                        fontFamily: 'Roboto-Light',
                        fontSize: normalize(13),
                        fontWeight: '400',
                        color: '#353535',
                        letterSpacing: 0.7,
                        color: this.state.bidInfoScreen.winnerTextColor, 
                        paddingTop: 10 
                        }:{
                          fontFamily: 'Roboto-Light',
                          fontSize: normalize(13),
                          fontWeight: '400',
                          color: '#353535',
                          letterSpacing: 0.7,
                          color: this.state.bidInfoScreen.winnerTextColor, 
                      }
                    }
                    >
                      {this.state.bidInfoScreen.winnerdblRedbl}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text
                      style={{
                        ...this.state.bidInfoScreen.NSStyle,
                        letterSpacing: 0.1,
                      }}
                    >
                      N • S
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{
                        ...this.state.bidInfoScreen.NSBIDStyle,
                      }}
                    >
                      BID
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom,
                        // borderRightWidth: 2
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom
                      }}
                    />
                  </View>
                </View>
                <View
                  style={isTablet ? {
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }:{
                    height: scale(90), //Needed a fix here
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <Text
                      style={{
                        ...this.state.bidInfoScreen.EWStyle,
                      }}
                    >
                      E • W
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{
                        ...this.state.bidInfoScreen.EWBIDStyle,
                      }}
                    >
                      BID
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1)}}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.bidInfoScreen.EWBIDBottom,
                        // borderRightWidth: 1
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.bidInfoScreen.EWBIDBottom,
                        // borderLeftWidth: 1
                      }}
                    />
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.currentScreen === SCREENS.PLAY_SCREEN ? (
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  height: '62.5%',
                  zIndex: 1,
                  backgroundColor: '#151515'
                }}
              >
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(33),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }:{
                    width: '100%',
                    height: scale(21),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text style={styles.ContractTextHeader}>Contract</Text>
                </View>
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(34),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }:{
                    width: '100%',
                    height: scale(23),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }}
                >
                  <View
                    style={isTablet?{
                      width: scale(25),
                      height: scale(33),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }:{
                      width: scale(23),
                      height: scale(23),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' ? (
                      <Text style={styles.ContractTextSpade}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' ? (
                      <Text style={styles.ContractTextHearts}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'D' ? (
                      <Text style={styles.ContractTextDiamond}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' ? (
                      <Text style={styles.ContractTextClubs}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' ? (
                      <Text style={styles.ContractTextNT}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                  </View>
                  <View
                    style={isTablet?{
                      width: scale(42),
                      height: scale(33),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                      paddingLeft: moderateScale(6),
                      paddingRight: moderateScale(6)
                    }:{
                      width: scale(44),
                      height: scale(23),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                      paddingLeft: moderateScale(6),
                      paddingRight: moderateScale(6)
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextSpade,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.spade}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextHearts,
                            marginEnd: moderateScale(4)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.hearts}
                        />
                      </View>
                    ) : null}

                    {this.state.bidInfoScreen.winnerSuit === 'D' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextDiamond,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.diamond}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextClubs,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.clubs}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                        }}
                      >
                        <Text style={{...styles.ContractTextNT, marginEnd: moderateScale(5)}}>
                          {this.state.bidInfoScreen.winnerLevel[0]}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.ntActive}
                        />
                      </View>
                    ) : null}
                  </View>
                  <View
                    style={{
                      width: scale(23),
                      height: scale(23),
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextSpade}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextHearts}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}

                    {this.state.bidInfoScreen.winnerSuit === 'D' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextDiamond}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextClubs}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextNT}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                    //,(this.state.TrickScoreNS > this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null] 
                  >
                    <Text
                      style={{
                        ...this.state.playScreen.NSStyle,
                        marginTop: moderateScale(4)
                      }}
                    >
                      N • S
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{...this.state.playScreen.NSScoreStyle,
                        marginTop: moderateScale(14)
                      }}
                    >
                      {this.state.TrickScoreNS}
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom,
                        // borderRightWidth: 2
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center',
                  }}
                >
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    //,(this.state.TrickScoreNS < this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null]
                  >
                    <Text
                      style={{
                        ...this.state.playScreen.EWStyle,
                        marginTop: moderateScale(4)

                      }}
                    >
                      E • W
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{...this.state.playScreen.EWScoreStyle,
                        marginTop: moderateScale(14)
                      }}
                    >
                      {this.state.TrickScoreEW}
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom,
                        // borderRightWidth: 2
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom
                      }}
                    />
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.currentScreen === SCREENS.SCOREBOARD ? (
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  height: '62.5%',
                  zIndex: 1,
                  backgroundColor: '#151515'
                }}
              >
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(33),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }:{
                    width: '100%',
                    height: scale(21),
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text style={styles.ContractTextHeader}>Contract</Text>
                </View>
                <View
                  style={isTablet?{
                    width: '100%',
                    height: scale(34),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }:{
                    width: '100%',
                    height: scale(23),
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: '#0a0a0a'
                  }}
                >
                  <View
                    style={isTablet?{
                      width: scale(25),
                      height: scale(33),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }:{
                      width: scale(23),
                      height: scale(23),
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' ? (
                      <Text style={styles.ContractTextSpade}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' ? (
                      <Text style={styles.ContractTextHearts}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'D' ? (
                      <Text style={styles.ContractTextDiamond}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' ? (
                      <Text style={styles.ContractTextClubs}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' ? (
                      <Text style={styles.ContractTextNT}>
                        {this.state.bidInfoScreen.winnerSide}
                      </Text>
                    ) : null}
                  </View>
                  <View
                    style={isTablet?{
                      width: scale(42),
                      height: scale(33),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                      paddingLeft: moderateScale(6),
                      paddingRight: moderateScale(6)
                    }:{
                      width: scale(44),
                      height: scale(23),
                      flexDirection: 'row',
                      borderRightWidth: 1,
                      borderBottomColor: '#0a0a0a',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                      paddingLeft: moderateScale(6),
                      paddingRight: moderateScale(6)
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextSpade,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.spade}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextHearts,
                            marginEnd: moderateScale(4)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.hearts}
                        />
                      </View>
                    ) : null}

                    {this.state.bidInfoScreen.winnerSuit === 'D' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextDiamond,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.diamond}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                          paddingLeft: moderateScale(6),
                          paddingRight: moderateScale(6)
                        }}
                      >
                        <Text
                          style={{
                            ...styles.ContractTextClubs,
                            marginEnd: moderateScale(5)
                          }}
                        >
                          {this.state.bidInfoScreen.winnerLevel}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.clubs}
                        />
                      </View>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderBottomColor: '#0a0a0a',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                        }}
                      >
                        <Text style={{...styles.ContractTextNT, marginEnd: moderateScale(5)}}>
                          {this.state.bidInfoScreen.winnerLevel[0]}
                        </Text>
                        <SvgXml
                          width={scale(11)}
                          height={scale(10)}
                          xml={svgImages.ntActive}
                        />
                      </View>
                    ) : null}
                  </View>
                  <View
                    style={{
                      width: scale(23),
                      height: scale(23),
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.bidInfoScreen.winnerSuit === 'S' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextSpade}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'H' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextHearts}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}

                    {this.state.bidInfoScreen.winnerSuit === 'D' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextDiamond}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'C' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextClubs}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                    {this.state.bidInfoScreen.winnerSuit === 'NT' &&
                    this.state.bidInfoScreen.winnerdblRedbl !== '' ? (
                      <Text style={styles.ContractTextNT}>
                        {this.state.bidInfoScreen.winnerdblRedbl}
                      </Text>
                    ) : null}
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={[{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center'
                    },(this.state.TrickScoreNS > this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null]}
                    //,(this.state.TrickScoreNS > this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null] 
                  >
                    <Text
                      style={{
                        ...this.state.playScreen.NSStyle,
                        marginTop: moderateScale(4)
                      }}
                    >
                      N • S
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{...this.state.playScreen.NSScoreStyle,
                        marginTop: moderateScale(14)
                      }}
                    >
                      {this.state.TrickScoreNS}
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom,
                        // borderRightWidth: 2
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.NSSharingBottom
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    height: scale(90),
                    width: scale(90),
                    alignItems: 'center',
                  }}
                >
                  <View
                    style={[{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                    },(this.state.TrickScoreNS < this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null]}
                    //,(this.state.TrickScoreNS < this.state.TrickScoreEW) ? {backgroundColor:'#DBFF00'}:null]
                  >
                    <Text
                      style={{
                        ...this.state.playScreen.EWStyle,
                        marginTop: moderateScale(4)

                      }}
                    >
                      E • W
                    </Text>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{...this.state.playScreen.EWScoreStyle,
                        marginTop: moderateScale(14)
                      }}
                    >
                      {this.state.TrickScoreEW}
                    </Text>
                  </View>
                  <View
                    style={{ flexDirection: 'row', height: verticalScale(1) }}
                  >
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom,
                        // borderRightWidth: 2
                      }}
                    />
                    <View
                      style={{
                        width: '50%',
                        backgroundColor: this.state.infoScreen.EWSharingBottom
                      }}
                    />
                  </View>
                </View>
              </View>
            ) : null}
          </View>

          <View style={this.playBodyStyle()}>
            {this.state.showHistory ? (
              <View style={this.popupViewBodyStyle()}>
                {this.state.showHistory ? (
                  <History
                    ref={this.historyComponent}
                    tableID={this.state.tableID}
                    gamestate={this.state.gamestate}
                    northAvtar={this.state.northAvtar}
                    eastAvtar={this.state.eastAvtar}
                    southAvtar={this.state.southAvtar}
                    westAvtar={this.state.westAvtar}
                    northUser={this.state.northUser}
                    eastUser={this.state.eastUser}
                    southUser={this.state.southUser}
                    westUser={this.state.westUser}
                    iskibitzer={this.state.iskibitzer}
                    infoScreen={this.state.infoScreen}
                    biddings={this.state.biddings}
                    bidInfoScreen={this.state.bidInfoScreen}
                    playScreen={this.state.playScreen}
                    lastBidRound={this.state.lastBidRound}
                    tricks={this.state.tricks}
                    currentScreen={this.state.currentScreen}
                    initialPlayer={this.state.initialPlayer}
                    arrangedSides={this.state.arrangedSides}
                    showTableMessages={this.showTableMessages}
                    chatScreen={this.state.chatScreen}
                    updateDeskStateValues={this.updateDeskStateValues}
                  />
                ) : null}
              </View>
            ) : null}
            {
              this.state.showCheatSheet? (
                <View style={this.popupViewBodyStyle()}>
                  <CheatSheet
                    tableID={this.state.tableID}
                    gamestate={this.state.gamestate}
                    northAvtar={this.state.northAvtar}
                    eastAvtar={this.state.eastAvtar}
                    southAvtar={this.state.southAvtar}
                    westAvtar={this.state.westAvtar}
                    northUser={this.state.northUser}
                    eastUser={this.state.eastUser}
                    southUser={this.state.southUser}
                    westUser={this.state.westUser}
                    iskibitzer={this.state.iskibitzer}
                    biddings={this.state.biddings}
                    playScreen={this.state.playScreen}
                    infoScreen={this.state.infoScreen}
                    username={this.state.username}
                    bidInfoScreen={this.state.bidInfoScreen}
                    lastBidRound={this.state.lastBidRound}
                    tricks={this.state.tricks}
                    currentScreen={this.state.currentScreen}
                    initialPlayer={this.state.initialPlayer}
                    arrangedSides={this.state.arrangedSides}
                    showTableMessages={this.showTableMessages}
                    updateDeskStateValues={this.updateDeskStateValues}
                    roundCount={this.state.roundCount}
                    chatScreen={this.state.chatScreen}
                  />
                </View>
              ): null
            }
            {this.state.showChatWindow ? (
              <View style={this.popupViewBodyStyle()}>
                {this.state.showChatWindow ? (
                  <ChatScreen
                    ref={this.historyComponent}
                    tableID={this.state.tableID}
                    gamestate={this.state.gamestate}
                    northAvtar={this.state.northAvtar}
                    eastAvtar={this.state.eastAvtar}
                    southAvtar={this.state.southAvtar}
                    westAvtar={this.state.westAvtar}
                    northUser={this.state.northUser}
                    eastUser={this.state.eastUser}
                    southUser={this.state.southUser}
                    westUser={this.state.westUser}
                    iskibitzer={this.state.iskibitzer}
                    infoScreen={this.state.infoScreen}
                    biddings={this.state.biddings}
                    bidInfoScreen={this.state.bidInfoScreen}
                    playScreen={this.state.playScreen}
                    lastBidRound={this.state.lastBidRound}
                    tricks={this.state.tricks}
                    currentScreen={this.state.currentScreen}
                    initialPlayer={this.state.initialPlayer}
                    // arrangedSides={this.state.arrangedSides}
                    updateDeskStateValues={this.updateDeskStateValues}
                  />
                ) : null}
              </View>
            ) : null}
            {this.state.currentScreen == '' ? (
              <Spinner
                visible={true}
                textContent={''}
                textStyle={styles.spinnerTextStyle}
              />
            ) : null}
            {this.state.currentScreen == SCREENS.JOIN_TABLE ? (
              <JoinTable
                tableID={this.state.tableID}
                isFromScreen={isFromScreen}
                fromPlayBot={this.state.fromPlayBot}
                gamestate={this.state.gamestate}
                northAvtar={this.state.northAvtar}
                eastAvtar={this.state.eastAvtar}
                southAvtar={this.state.southAvtar}
                westAvtar={this.state.westAvtar}
                northUser={this.state.northUser}
                eastUser={this.state.eastUser}
                southUser={this.state.southUser}
                westUser={this.state.westUser}
                affiliation={this.state.affiliation}
                iskibitzer={this.state.iskibitzer}
                username={this.state.username}
                chatScreen={this.state.chatScreen}
                showTableMessages={this.showTableMessages}
                hostName = {this.props.hostName}
                isNorthInvited={this.props.isNorthInvited}
                isSouthInvited={this.props.isSouthInvited}
                isWestInvited={this.props.isWestInvited}
                isEastInvited={this.props.isEastInvited}
                directionWest={this.props.directionWest}
                directionNorth={this.props.directionNorth}
                directionEast={this.props.directionEast}
                directionSouth={this.props.directionSouth}
                northUserName={this.props.northUserName}
                southUserName={this.props.southUserName}
                eastUserName={this.props.eastUserName}
                westUserName={this.props.westUserName}
                isHomeUser={this.props.homeUser}
                // showUserProfile={this.state.showUserProfile}
                cancelRequest={this.cancelRequest}
                onAddPlayerClick={this.onAddPlayerClick}
                eyeIndicatorClicked={this.state.eyeIndicatorClicked}
                updateDeskStateValues={this.updateDeskStateValues}
              />
            ) : null}

            {this.state.currentScreen == SCREENS.SHARE_INFO ? (
              <ShareInfo
                ref={this.shareInfoComponent}
                tableID={this.state.tableID}
                gamestate={this.state.gamestate}
                northAvtar={this.state.northAvtar}
                eastAvtar={this.state.eastAvtar}
                southAvtar={this.state.southAvtar}
                westAvtar={this.state.westAvtar}
                northUser={this.state.northUser}
                eastUser={this.state.eastUser}
                southUser={this.state.southUser}
                westUser={this.state.westUser}
                chatScreen={this.state.chatScreen}
                isNorthInvited={this.props.isNorthInvited}
                isSouthInvited={this.props.isSouthInvited}
                isWestInvited={this.props.isWestInvited}
                isEastInvited={this.props.isEastInvited}
                directionWest={this.props.directionWest}
                directionNorth={this.props.directionNorth}
                directionEast={this.props.directionEast}
                directionSouth={this.props.directionSouth}
                northUserName={this.props.northUserName}
                southUserName={this.props.southUserName}
                eastUserName={this.props.eastUserName}
                westUserName={this.props.westUserName}
                isHomeUser={this.props.homeUser}
                iskibitzer={this.state.iskibitzer}
                infoScreen={this.state.infoScreen}
                hostName = {this.props.hostName}
                username={this.state.username}
                showTableMessages={this.showTableMessages}
                updateDeskStateValues={this.updateDeskStateValues}
                cancelRequest={this.cancelRequest}
                onAddPlayerClick={this.onAddPlayerClick}
                checkBidding={this.checkBidding}
                navigation={this.props.navigation}
              />
            ) : null}

            {this.state.currentScreen == SCREENS.BID_INFO ? (
              <BidInfo
                ref={this.bidComponent}
                tableID={this.state.tableID}
                gamestate={this.state.gamestate}
                northAvtar={this.state.northAvtar}
                eastAvtar={this.state.eastAvtar}
                southAvtar={this.state.southAvtar}
                westAvtar={this.state.westAvtar}
                northUser={this.state.northUser}
                eastUser={this.state.eastUser}
                southUser={this.state.southUser}
                westUser={this.state.westUser}
                iskibitzer={this.state.iskibitzer}
                infoScreen={this.state.infoScreen}
                username={this.state.username}
                chatScreen={this.state.chatScreen}
                hostName = {this.props.hostName}
                bidInfoScreen={this.state.bidInfoScreen}
                isNorthInvited={this.props.isNorthInvited}
                isSouthInvited={this.props.isSouthInvited}
                isWestInvited={this.props.isWestInvited}
                isEastInvited={this.props.isEastInvited}
                directionWest={this.props.directionWest}
                directionNorth={this.props.directionNorth}
                northUserName={this.props.northUserName}
                southUserName={this.props.southUserName}
                eastUserName={this.props.eastUserName}
                westUserName={this.props.westUserName}
                directionEast={this.props.directionEast}
                directionSouth={this.props.directionSouth}
                isHomeUser={this.props.homeUser}
                showTableMessages={this.showTableMessages}
                cancelRequest={this.cancelRequest}
                onAddPlayerClick={this.onAddPlayerClick}
                updateDeskStateValues={this.updateDeskStateValues}
                updateBiddingBox={this.updateBiddingBox}
                navigation={this.props.navigation}
              />
            ) : null}
            {this.state.currentScreen == SCREENS.PLAY_SCREEN ? (
              <PlayScreen
                ref={this.playComponent}
                tableID={this.state.tableID}
                gamestate={this.state.gamestate}
                northAvtar={this.state.northAvtar}
                eastAvtar={this.state.eastAvtar}
                southAvtar={this.state.southAvtar}
                westAvtar={this.state.westAvtar}
                northUser={this.state.northUser}
                eastUser={this.state.eastUser}
                southUser={this.state.southUser}
                westUser={this.state.westUser}
                iskibitzer={this.state.iskibitzer}
                infoScreen={this.state.infoScreen}
                hostName = {this.props.hostName}
                bidInfoScreen={this.state.bidInfoScreen}
                affiliation={this.state.affiliation}
                playScreen={this.state.playScreen}
                tricks={this.state.tricks}
                chatScreen={this.state.chatScreen}
                isNorthInvited={this.props.isNorthInvited}
                isSouthInvited={this.props.isSouthInvited}
                isWestInvited={this.props.isWestInvited}
                isEastInvited={this.props.isEastInvited}
                directionWest={this.props.directionWest}
                directionNorth={this.props.directionNorth}
                directionEast={this.props.directionEast}
                directionSouth={this.props.directionSouth}
                isHomeUser={this.props.homeUser}
                host={this.props.host}
                username={this.state.username}
                // showUserProfile={this.state.showUserProfile}
                showTableMessages={this.showTableMessages}
                cancelRequest={this.cancelRequest}
                onAddPlayerClick={this.onAddPlayerClick}
                updateDeskStateValues={this.updateDeskStateValues}
                loadProfileOrVaccatePlayerScreen={this.loadProfileOrVaccatePlayerScreen}
                navigation={this.props.navigation}
              />
            ) : null}
            {/* {this.state.showUserProfile ? (
                <View style={this.popupViewBodyStyle()}>
                  {this.state.showUserProfile ? (
                    
                    // {console.log("User pro called from main desk", this.props.hostName)}
                    <UserProfile 
                      name={this.state.profileName}
                      subName={this.state.profileSubname}
                      currentScreen={this.state.currentScreen}
                      isHost={this.props.hostName == this.state.gamestate.myUsername}
                      kickoutUserDirection={this.state.kickoutUserDirection}
                      tableId = {this.state.tableID}
                      kickedUserisHost = {this.state.kickedUserisHost}
                      updateDeskStateValues={this.updateDeskStateValues}
                    />
                  ): null
                }
                </View>
              ): null} */}

            {this.state.currentScreen == SCREENS.SCOREBOARD ? (
              <ScoreBoard
                ref={this.scoreBoardComponent}
                tableID={this.state.tableID}
                gamestate={this.state.gamestate}
                northAvtar={this.state.northAvtar}
                eastAvtar={this.state.eastAvtar}
                southAvtar={this.state.southAvtar}
                westAvtar={this.state.westAvtar}
                northUser={this.state.northUser}
                eastUser={this.state.eastUser}
                southUser={this.state.southUser}
                westUser={this.state.westUser}
                chatScreen={this.state.chatScreen}
                hostName = {this.props.hostName}
                iskibitzer={this.state.iskibitzer}
                infoScreen={this.state.infoScreen}
                bidInfoScreen={this.state.bidInfoScreen}
                playScreen={this.state.playScreen}
                scoreScreen={this.state.scoreScreen}
                showTableMessages={this.showTableMessages}
                updateDeskStateValues={this.updateDeskStateValues}
              />
            ) : null}
            {/* {this.state.currentScreen == SCREENS.USERPROFILE ? (
              <UserProfile 
                name={this.state.profileName}
                subName={this.state.profileSubname}
                currentScreen={this.state.currentScreen}
                isHost={this.props.hostName == this.state.gamestate.myUsername}
                kickoutUserDirection={this.state.kickoutUserDirection}
                tableId = {this.state.tableID}
                kickedUserisHost = {this.state.kickedUserisHost}
                updateDeskStateValues={this.updateDeskStateValues}
              />
            ) : null} */}
           
            {this.state.showSubMenuPopup ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                  position: 'absolute',
                  opacity: this.state.showChatWindow ? 0.9 : 1
                }}
              >
                <View style={[styles.claimRequestBody]}>
                  <View
                    style={[
                      styles.claimRequestTextBody,
                      { width: scale(424),}
                    ]}
                  >
                    {
                      (!this.isClaimDisabled() && !showOptionWhenRobotInTable(this.state.gamestate))&&
                      <TouchableOpacity
                        onPress={() => this.enableClaimPopup()}
                        style={{justifyContent: 'center',height: verticalScale(46)}}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems
                          ]}
                        >
                          Claim/Concede
                        </Text>
                      </TouchableOpacity>
                    }
                    {
                      (!this.isChangeDealDisabled() && !showOptionWhenRobotInTable(this.state.gamestate))&&
                      <TouchableOpacity
                        onPress={() => this.enableChangeDealPopup()}
                        style={{justifyContent: 'center',height: verticalScale(46)}}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems
                          ]}
                        >
                          Change deal
                        </Text>
                      </TouchableOpacity>
                    }
                    {
                      this.state.currentRole !== 'rover' &&
                      <TouchableOpacity
                        // onPress={() => this.leaveSeat()}
                        disabled={true}
                        style={{justifyContent: 'center',height: verticalScale(46)}}
                      >
                        <Text
                          style={{
                            ...styles.claimRequestTextItems,
                            color: '#6D6D6D'
                          }}
                        >
                          Leave seat
                        </Text>
                      </TouchableOpacity>
                    }
                   
                    <TouchableOpacity onPress={() => this.leaveTable()}
                       style={{height: verticalScale(46), justifyContent: 'center'}}>
                      <Text style={styles.claimRequestTextItems}>
                        Leave table
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.showChangeDealPopup ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                  position: 'absolute',
                  opacity: this.state.showChatWindow ? 0.9 : 1
                }}
              >
                <View style={[styles.claimRequestBody]}>
                  <View
                    style={[
                      styles.claimRequestTextBody,
                      { width: scale(424),}
                    ]}
                  >
                    <TouchableOpacity
                      onPress={() => this.changeDealRequest('redeal')}
                      style={{ height: scale(40) }}
                    >
                      <Text style={[styles.claimRequestTextItems]}>Next Deal</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.changeDealRequest('replay')}
                      style={{ height: scale(40) }}
                    >
                      <Text style={[styles.claimRequestTextItems]}>
                        Replay Deal
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.isChangeDeal ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                  position: 'absolute',
                  opacity: this.state.showChatWindow ? 0.9 : 0.7,
                  zIndex: 9
                }}
              >
                <View style={[styles.claimRequestNextBody]}>
                  <View
                    style={[
                      styles.claimRequestBodyClm,
                      { width: scale(215), height: verticalScale(167) }
                    ]}
                  >
                    {this.state.changeDealResponed ? (
                      <View style={styles.claimRequestTopRow}>
                        <Text style={styles.claimTxtWhite}>
                          {this.state.changeDealResponseText}
                        </Text>
                      </View>
                    ) : (
                      this.state.role == 'kibitzer' ? (
                        <View style={styles.claimRequestTopRow}>
                          <Text style={styles.claimTxtWhite}>
                            {this.state.changeDealUser +
                              ' requests ' +
                              this.state.changeDealType}
                          </Text>
                          <Text style={styles.claimTxtWhite}>
                            Waiting for Player response..
                          </Text>
                        </View>
                      ) : (
                      <>
                        <View style={styles.claimRequestTopRow}>
                          <Text style={styles.claimTxtWhite}>
                            {this.state.changeDealUser +
                              ' requests ' +
                              this.state.changeDealType +
                              '?'}
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.claimRequestNumberBodyRow,
                            { width: scale(215), height: verticalScale(129) }
                          ]}
                        >
                          <TouchableHighlight
                            style={[
                              styles.claimRequestTopRowBox,
                              { width: scale(108), height: verticalScale(24) }
                            ]}
                            onPress={() =>
                              this.changeDealResponse(this.state.changeDealType)
                            }
                          >
                            <Text style={styles.claimTxtGreen}>Yes</Text>
                          </TouchableHighlight>
                          <TouchableHighlight
                            style={[
                              styles.claimRequestTopRowBox,
                              { width: scale(108), height: verticalScale(24) }
                            ]}
                            onPress={() => this.changeDealResponse('no')}
                          >
                            <Text style={styles.claimTxtWhite}>No</Text>
                          </TouchableHighlight>
                        </View>
                      </>
                    ))}
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.showClaimPopup ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                  position: 'absolute',
                }}
              >
                <View
                  style={[
                    styles.claimRequestNextBody,
                    {
                      width: scale(215),
                      height: verticalScale(167),
                      top: moderateScale(89)
                    }
                  ]}
                >
                  <View style={[styles.claimRequestBodyClm]}>
                    <View style={styles.claimRequestTopRow}>
                      <TouchableHighlight
                        style={[
                          styles.claimRequestTopRowBox,
                          { width: scale(108), height: verticalScale(24) }
                        ]}
                        onPress={() => this.selectClaimType('CLAIM')}
                      >
                        <Text style={this.getClaimPopupTitle('CLAIM')}>
                          Claim
                        </Text>
                      </TouchableHighlight>
                      <TouchableHighlight
                        style={[
                          styles.claimRequestTopRowBox,
                          { width: scale(108), height: verticalScale(24) }
                        ]}
                        onPress={() => this.selectClaimType('CONCEDE')}
                      >
                        <Text style={this.getClaimPopupTitle('CONCEDE')}>
                          Concede
                        </Text>
                      </TouchableHighlight>
                    </View>
                    <View
                      style={[
                        styles.claimRequestNumberTotalBody,
                        { marginTop: moderateScale(14) }
                      ]}
                    >
                      <View style={styles.claimRequestNumberBodyRow}>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(1),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(1)}
                          disabled={this.disabledClaimButton(1)}
                        >
                          <Text style={this.getClaimNumberStyle(1)}>1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(2),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(2)}
                          disabled={this.disabledClaimButton(2)}
                        >
                          <Text style={this.getClaimNumberStyle(2)}>2</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(3),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(3)}
                          disabled={this.disabledClaimButton(3)}
                        >
                          <Text style={this.getClaimNumberStyle(3)}>3</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(4),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(4)}
                          disabled={this.disabledClaimButton(4)}
                        >
                          <Text style={this.getClaimNumberStyle(4)}>4</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(5),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(5)}
                          disabled={this.disabledClaimButton(5)}
                        >
                          <Text style={this.getClaimNumberStyle(5)}>5</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.claimRequestNumberBodyRow}>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(6),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(6)}
                          disabled={this.disabledClaimButton(6)}
                        >
                          <Text style={this.getClaimNumberStyle(6)}>6</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(7),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(7)}
                          disabled={this.disabledClaimButton(7)}
                        >
                          <Text style={this.getClaimNumberStyle(7)}>7</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(8),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(8)}
                          disabled={this.disabledClaimButton(8)}
                        >
                          <Text style={this.getClaimNumberStyle(8)}>8</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(9),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(9)}
                          disabled={this.disabledClaimButton(9)}
                        >
                          <Text style={this.getClaimNumberStyle(9)}>9</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(10),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(10)}
                          disabled={this.disabledClaimButton(10)}
                        >
                          <Text style={this.getClaimNumberStyle(10)}>10</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.claimRequestNumberBodyRow}>
                        <TouchableOpacity
                          style={[
                            styles.claimRequestNumberBodyRowBox,
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.claimConcedeRequest('withdraw')}
                          disabled={!this.state.claimRequested}
                        >
                          <SvgXml
                            height={verticalScale(9.2)}
                            width={scale(9.8)}
                            xml={this.getClaimCancelButton()}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(11),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(11)}
                          disabled={this.disabledClaimButton(11)}
                        >
                          <Text style={this.getClaimNumberStyle(11)}>11</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(12),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(12)}
                          disabled={this.disabledClaimButton(12)}
                        >
                          <Text style={this.getClaimNumberStyle(12)}>12</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            this.getClaimBoxStyle(13),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.selectClaimNumber(13)}
                          disabled={this.disabledClaimButton(13)}
                        >
                          <Text style={this.getClaimNumberStyle(13)}>13</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          style={[
                            this.getClaimBoxRequestStyle(),
                            {
                              width: scale(43),
                              height: verticalScale(43)
                            }
                          ]}
                          onPress={() => this.claimConcedeRequest('request')}
                          disabled={this.state.claimRequested}
                        >
                          <SvgXml
                            height={verticalScale(12)}
                            width={scale(12)}
                            xml={this.getClaimRequestButton()}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ) : null}
            {(this.props.addPlayer && !this.props.addFriend) ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                  position: 'absolute',
                  opacity: this.state.showChatWindow ? 0.9 : 1
                }}
              >
                <TouchableOpacity
                  onPress={() => { this.onCancelPlayerSelect(false) }}
                  style={{ position: 'absolute', right: moderateScale(10), top: 0 }}
                >
                  <SvgXml
                    height={scale(45)}
                    width={scale(45)}
                    xml={svgImages.back}
                  />
                </TouchableOpacity>
                <View style={{ ...styles.claimRequestBody }}>
                  <View
                    style={[
                      styles.claimRequestTextBody,
                      { width: scale(424)}, 
                    ]}
                  >
                    {
                      this.state.iskibitzer &&
                      <TouchableOpacity
                        onPress={() => {this.changeSeat(this.props.currentDirection, this.state.tableID)}}
                        style={{ height: scale(40) }}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems,
                          ]}
                        >
                          Play
                        </Text>
                      </TouchableOpacity>
                    }
                    {
                      this.props.removePlayer &&
                      <TouchableOpacity
                        onPress={() => {this.removePlayerFromTable()}}
                        style={{ height: scale(40) }}
                        disabled={true}
                      >
                        <Text
                          style={[
                            styles.disbaledState,
                          ]}
                        >
                          Remove player
                        </Text>
                      </TouchableOpacity>
                    }
                    {
                      // this.props.removePlayer && 
                      //i want to remove this
                      getUserNameBasedOnHost(this.state.username, this.props.hostName) &&
                      showInviteBotOption(this.state.gamestate, this.props.currentDirection) && 
                      <TouchableOpacity
                        onPress={() => { this.onPlayerSelect('Bots', this.state.tableID) }}
                        style={{ height: scale(40) }}
                        //disabled={true}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems,
                          ]}
                        >
                          Invite robot
                        </Text>
                      </TouchableOpacity> 
                    }
                    {/* {
                      !this.props.removePlayer &&
                      <TouchableOpacity
                        onPress={() => { this.onPlayerSelect('Player',this.state.tableID) }}
                        style={{ height: scale(40) }}
                        //disabled={true}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems,
                          ]}
                        >
                          Invite
                        </Text>
                      </TouchableOpacity>
                    } */}
                    {
                      (!this.state.iskibitzer)&&
                      <TouchableOpacity
                        onPress={() => { this.onPlayerSelect('Player', this.state.tableID) }}
                        style={{ height: scale(40) }}
                        disabled={true}
                      >
                        <Text
                          style={[
                            styles.disbaledState,
                          ]}
                        >
                          {'Kibitz'+ ' '+this.getDirectionValue()}
                        </Text>
                      </TouchableOpacity>
                    }
                    {
                      (!this.state.iskibitzer) &&
                      <TouchableOpacity
                        onPress={() => {this.onPlayerSelect('Kibitz All', this.state.tableID)}}
                        style={{ height: scale(40) }}
                        // disabled={true}
                      >
                        <Text
                          style={[
                            styles.claimRequestTextItems,
                          ]}
                        >
                          {'Kibitz All'}
                        </Text>
                      </TouchableOpacity>
                    }
                  </View>
                </View>
              </View>
            ) : null}
            {this.props.addFriend ? (
              <View
                style={{
                  ...this.popupViewBodyStyle(),
                }}
              >
                <TouchableOpacity
                  onPress={() => { this.onCancelPlayerSelect(true) }}
                  style={{ position: 'absolute', right: moderateScale(10), top: 0 }}
                >
                  <SvgXml
                    height={scale(45)}
                    width={scale(45)}
                    xml={svgImages.back}
                  />
                </TouchableOpacity>
                <View style={{width: '100%', height: '100%',}}>
                  <ChatScreen />
                </View>
              </View>
            ) : null}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  clmText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingLeft: '10@ms'
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: '4@ms'
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#676767',
    textAlign: 'center'
  },
  DirectionTextActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },
  bidCirclePositionBodrAssActive: {
    width: '28@s',
    height: '28@s',
    backgroundColor: '#151515',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#B9B9B9',
    flexDirection: 'row'
  },
  bidCircleTextAssActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#B9B9B9'
  },
  BidText: {
    top: '3@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    fontWeight: 'normal',
    color: '#353535'
  },
  bidCirclePosition: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#151515',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  ContractTextActive: {
    top: '4@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
  },
  ContractText: {
    top: '4@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
  },
  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
    letterSpacing: 0.7
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    //paddingRight: '4@ms'
  },
  ShareTextActive: {
    top: '8@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(27),
    fontWeight: 'normal',
    color: '#6D6D6D'
  },
  ShareText: {
    top: '8@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(27),
    fontWeight: 'normal',
    color: '#353535'
  },
  ScoreText: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(45),
    color: '#353535'
  },
  ScoreTextActive: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(45),
    color: '#6D6D6D'
  },
  BidTextActive: {
    top: '3@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D',
  },
  bidCirclePositionArea: {
    width: '100%',
    height: '28@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  spadeImageActive: {
    width: '8.5@s',
    height: '9@vs',
    marginLeft: '2@ms'
  },
  heartsImageActive: {
    width: '8@s',
    height: '7@vs',
    marginLeft: '2@ms'
  },
  diamondImageActive: {
    width: '8@s',
    height: '9@vs',
    marginLeft: '2@ms'
  },
  clubsImageActive: {
    width: '8@s',
    height: '8@vs',
    marginLeft: '2@ms'
  },
  bidCircleTextAss: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#676767'
  },
  bidCircleTextHeartsActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FF0C3E'
  },
  bidCircleTextHeartsInActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFF'
  },
  bidCircleTextClubsActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#79E62B'
  },
  bidCircleTextClubsInActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFF'
  },
  bidCircleTextDiamondActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#04AEFF'
  },
  bidCircleTextDiamondInActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFF'
  },
  bidCircleTextSpadeActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#C000FF'
  },
  bidCircleTextSpadeInActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFF'
  },
  bidCircleTextNTActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFE81D'
  },
  bidCirclePositionNTActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFE81D',
    flexDirection: 'row'
  },
  bidCirclePositionSpadeActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#C000FF',
    flexDirection: 'row'
  },
  bidCirclePositionSpadeInActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFF',
    flexDirection: 'row'
  },
  bidCirclePositionHeartsActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FF0C3E',
    flexDirection: 'row'
  },
  bidCirclePositionHeartsInActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFF',
    flexDirection: 'row'
  },
  bidCirclePositionClubsActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#79E62B',
    flexDirection: 'row'
  },
  bidCirclePositionClubsInActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFF',
    flexDirection: 'row'
  },
  bidCirclePositionDiamondActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#04AEFF',
    flexDirection: 'row'
  },
  bidCirclePositionDiamondInActive: {
    width: '28@s',
    height: '28@vs',
    backgroundColor: '#0a0a0a',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFF',
    flexDirection: 'row'
  },
  bidCirclePositionBodrAss: {
    width: '33@s',
    height: '34@s',
    backgroundColor: '#151515',
    borderRadius: '50@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black', //#676767
    flexDirection: 'row'
  },

  bidBoxPosition: {
    flexDirection: 'column',
    width: '100%',
    borderWidth: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2
  },
  bidBoxPositionDirectionArea: {
    flexDirection: 'column',
    width: '100%',
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 1
  },
  bidBoxWidthHeightArea: {
    width: '43@s',
    height: '43@vs',
    backgroundColor: '#151515',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bidBoxWidthHeightAreaBodrActive: {
    width: '43@s',
    height: '43@vs',
    backgroundColor: '#151515',
    borderWidth: 1,
    borderColor: '#DBFF00',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bidBoxRorPosition: {
    width: '343@s',
    flexDirection: 'row'
  },
  bidBoxWidthHeightAreaActive: {
    width: '43@s',
    height: '43@vs',
    backgroundColor: '#1C1C1C',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bidBoxSelectedTrickTextbg: {
    width: '43@s',
    height: '43@vs',
    backgroundColor: '#1C1C1C',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bidBoxSelectedTrickText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#DBFF00'
  },
  bidBoxWidthHeightTextClorActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#DBFF00'
  },
  bidBoxWidthHeightTextClorNTActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#FFE81D'
  },
  bidBoxWidthHeightTextClor: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#676767'
  },
  bidBoxDisabledLevel: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },
  bidDblTextClordisabled: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },
  bidRedblTextClordisabled: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },
  MenuBox: {
    backgroundColor: '#151515',
    borderColor: '#0A0A0A',
    borderWidth: 0.5,
    borderLeftWidth: 0,
    borderBottomWidth: 0
  },

  /*************************************** */

  SharePatternCardSpade: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#C000FF',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardHearts: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#FF0C3E',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardDiamond: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#04AEFF',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardClubs: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#79E62B',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  PatternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#FFFFFF'
  },
  SharePatternCard: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#DBFF00',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  cardDisplayBody: {
    width: '90@s',
    height: '45@vs',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardPosition: {
    width: '22@s',
    marginTop: '3@ms',
    marginLeft: '4@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },

  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center'
  },

  UserNameText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  LHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  RHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  Partner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#C000FF'
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#79E62B'
  },
  ContractTextNT: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FFE81D'
  },
  bidPlayText: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D'
  },
  CardPositionBorderAss: {
    // width:55,
    // height:81,
    borderRadius: '4@ms',
    borderWidth: '1@ms',
    borderColor: '#676767'
  },
  northCardPosition: {
    // width:55,
    // height:81,
    position: 'relative'
  },
  sauthCardPosition: {
    // width:55,
    // height:81,
    position: 'relative'
  },
  westCardPosition: {
    // width:55,
    // height:81,
    marginLeft: 12,
    transform: [{ rotate: '-90deg' }]
  },
  eastCardPosition: {
    // width:55,
    // height:81,
    transform: [{ rotate: '90deg' }]
  },
  playCardNumberPosition: {
    position: 'absolute',
    // width:55,
    // height:81,
    borderWidth: 0,
    // paddingTop:2,
    // paddingLeft:6,
  },
  playCardNumberDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  playCardDiamond: {
    // width:55,
    // height:81,
    borderRadius: '4@ms',
    borderWidth: '1@ms',
    borderColor: '#04AEFF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  playCardHearts: {
    // width:55,
    // height:81,
    borderRadius: '4@ms',
    borderWidth: '1@ms',
    borderColor: '#FF0C3E',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  playCardClubs: {
    // width:55,
    // height:81,
    borderRadius: '4@ms',
    borderWidth: '1@ms',
    borderColor: '#79E62B',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  playCardSpade: {
    // width:55,
    // height:81,
    borderRadius: '4@ms',
    borderWidth: '1@ms',
    borderColor: '#C000FF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  claimRequestNumberBodyRowBoxGreen: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: '1@ms',
    borderRightColor: '#0a0a0a',
    borderBottomColor: '#0a0a0a',
    borderBottomWidth: '1@ms',
    backgroundColor: '#DBFF00'
  },
  claimRequestNumberBodyRowBox: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: '1@ms',
    borderRightColor: '#0a0a0a',
    borderBottomColor: '#0a0a0a',
    borderBottomWidth: '1@ms',
    backgroundColor: '#151515'
  },
  claimRequestNumberBodyRow: {
    flexDirection: 'row'
  },
  claimRequestNumberTotalBody: {
    flexDirection: 'column'
  },
  claimTxtBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#0a0a0a',
    textAlign: 'center'
  },
  claimTxtWhite: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300',
    color: '#FFFFFF',
    textAlign: 'center'
  },
  claimTxtGreen: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },
  claimRequestTopRowBox: {
    borderRightWidth: '1@ms',
    borderRightColor: '#0a0a0a',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  claimRequestTopRow: {
    flexDirection: 'row'
  },
  claimRequestBodyClm: {
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  claimRequestTextItems: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#ffffff',
    textAlign: 'center'
  },
  disbaledState: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  claimRequestTextItemsDisabled: {
    color: '#323232'
  },
  claimRequestTextBody: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  claimRequestBody: {
    position: 'absolute',
    borderWidth: 0
  },
  claimRequestNextBody: {
    position: 'absolute',
    borderWidth: 0,
    alignSelf: 'center'
  }

  /************** CLAIM CLOSE ******************* */
});

const initialShareInfo = {
  showShareCards: false,
  DirectionTextPartner: '#6D6D6D',
  DirectionTextLHO: '#6D6D6D',
  DirectionTextRHO: '#6D6D6D',
  DirectionTextMySeat: '#6D6D6D',
  northUserText: '#6D6D6D',
  eastUserText: '#6D6D6D',
  southUserText: '#6D6D6D',
  westUserText: '#6D6D6D',
  NSStyle: styles.ContractText,
  NSSharingStyle: styles.ShareText,
  EWStyle: styles.ContractText,
  EWSharingStyle: styles.ShareText,
  EWSharingBottom: '#6D6D6D',
  NSSharingBottom: '#6D6D6D',
  infoHCPClicked: false,
  infoPatternClicked: false,
  infoSuitClicked: false,
  numCardsShared: 0,
  numSpShared: false,
  numHtShared: false,
  numDmShared: false,
  numCbShared: false,
};

const initialChatUnfo = {
  isNorthSentMsg : false,
  isSouthSentMsg : false,
  isWestSentMsg: false,
  isEastSentMsg: false,
}

const initialBiddingInfo = {
  showBiddingBox: false,
  winnerSide: '',
  winnerLevel: '',
  winnerSuit: '',
  bid_raised_cnt: 1,
  EnableDouble: true,
  EnableRedouble: true,
  isOnlyDblOrReDbl: false,
  DirectionTextPartner: '#DBFF00',
  DirectionTextLHO: '#DBFF00',
  DirectionTextRHO: '#DBFF00',
  DirectionTextMySeat: '#DBFF00',
  northUserText: '#DBFF00',
  eastUserText: '#DBFF00',
  southUserText: '#DBFF00',
  westUserText: '#DBFF00',
  NSStyle: styles.ContractTextActive,
  NSBIDStyle: styles.BidTextActive,
  NSBIDBottom: '#DBFF00',
  EWStyle: styles.ContractTextActive,
  EWBIDStyle: styles.BidTextActive,
  EWBIDBottom: '#DBFF00',
  my_bid_level: '',
  current_bid_level: '',
  my_bid_suit: '',
  my_bid_suit_color: styles.bidCircleTextAss,
  my_bid_suit_text_color: '',
  my_bid_suit_image: '',
  my_bid_suit_border_color: styles.bidCirclePositionBodrAss,
  my_partner_bid_level: '',
  my_partner_bid_suit: '',
  my_partner_bid_suit_color: styles.bidCircleTextAssActive,
  my_partner_bid_suit_text_color: '',
  my_partner_bid_suit_image: '',
  my_partner_bid_suit_border_color: styles.bidCirclePositionBodrAss,
  my_LHO_bid_level: '',
  my_LHO_bid_suit: '',
  my_LHO_bid_suit_color: styles.bidCircleTextAssActive,
  my_LHO_bid_suit_text_color: '',
  my_LHO_bid_suit_image: '',
  my_LHO_bid_suit_border_color: styles.bidCirclePositionBodrAss,
  my_RHO_bid_level: '',
  my_RHO_bid_suit: '',
  my_RHO_bid_suit_color: styles.bidCircleTextAssActive,
  my_RHO_bid_suit_text_color: '',
  my_RHO_bid_suit_image: '',
  my_RHO_bid_suit_border_color: styles.bidCirclePositionBodrAss
};
const inititalPlayInfo = {
  disablemyCards_C: true,
  disablemyCards_D: true,
  disablemyCards_H: true,
  disablemyCards_S: true,
  disabledummyCards_C: true,
  disabledummyCards_D: true,
  disabledummyCards_H: true,
  disabledummyCards_S: true,
  Numof_C: 0,
  Numberof_D: 0,
  Numberof_H: 0,
  Numof_S: 0,
  trick_Suit: '',
  cardsCnt: 0,
  output: '',
  tableID: '',
  DummySeat: '',
  DummyPlayed: false,
  directionImage: svgImages.pointerSouth,
  northUser: '',
  eastUser: '',
  southUser: '',
  westUser: '',
  northSharedType1: '',
  northSharedValue1: '',
  northSharedType2: '',
  northSharedValue2: '',
  eastSharedType1: '',
  eastSharedValue1: '',
  eastSharedType2: '',
  eastSharedValue2: '',
  southSharedType1: '',
  southSharedValue1: '',
  southSharedType2: '',
  southSharedValue2: '',
  westSharedType1: '',
  westSharedValue1: '',
  westSharedType2: '',
  westSharedValue2: '',
  cardHCPTextColor: '#FFFFFF',
  cardPatternTextColor: '#FFFFFF',
  cardSuitTextColor: '#FFFFFF',
  DirectionTextPartner: styles.DirectionText,
  DirectionTextLHO: styles.DirectionText,
  DirectionTextRHO: styles.DirectionText,
  DirectionTextMySeat: styles.DirectionText,
  northUserText: '#6D6D6D',
  eastUserText: '#6D6D6D',
  southUserText: '#6D6D6D',
  westUserText: '#6D6D6D',
  winnerSide: '',
  winnerLevel: '',
  winnerSuit: '',
  winnerSuitImage: '',
  winnerTextColor: '',
  winnerdblRedbl: '',
  PartnerPlayed: false,
  LHOPlayed: false,
  RHOPlayed: false,
  MyCard: false,
  undoLHO: false,
  undoRHO: false,
  disableTkBk: true,
  undoAsked: false,
  undoCardSide: '',
  undoCardSuit: '',
  undoCardRank: '',
  undoMySeatimg: require('../../assets/images/takeback_black.png'),
  undoLHOSeatimg: require('../../assets/images/takeback_black.png'),
  undoPartnerSeatimg: require('../../assets/images/takeback_black.png'),
  undoRHOSeatimg: require('../../assets/images/takeback_black.png'),
  reqUndo: svgImages.undo,
  reqUndoimg: svgImages.undo,
  isClaimInitiated: false,
  claimConcedeNumber: 0,
  LHORank: '',
  RHORank: '',
  PartnerRank: '',
  myCardStyle: '',
  myCardNumberstyle: '',
  myRank: '',
  mySuit: svgImages.cheat_Sheet,
  PartnerSuit: svgImages.cheat_Sheet,
  LHOSuit: svgImages.cheat_Sheet,
  RHOSuit: svgImages.cheat_Sheet,
  dummycardsdrawn: false,
  isSouthDummy: false,
  NumberofDummy_C: 0,
  NumberofDummy_D: 0,
  NumberofDummy_H: 0,
  NumberofDummy_S: 0,
  CardBorderS: '#676767',
  CardBorderN: '#676767',
  CardBorderE: '#676767',
  CardBorderW: '#676767',
  NSStyle: styles.ContractText,
  EWStyle: styles.ContractText,
  NSScoreStyle: styles.ScoreText,
  EWScoreStyle: styles.ScoreText,
  playTurn: '',
  cancelAnimateImg: false,
  playTimer: require('../../assets/images/crxanimation_0.png'),
  hands: [],
  dummyHands: [],
  eastHands: [],
  westHands: [],
  northHands: [],
  eastCardsShown: [],
  westCardsShown: [],
  northCardsShown: [],
  cardsShown: [
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex'
  ],
  dummycardsShown: [
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex',
    'flex'
  ]
};

var initialState = {
  stage: null,
  currentScreen: null,
  role: null,
  tableID: null,
  gamestate: {},
  infoScreen: { ...initialShareInfo },
  bidInfoScreen: { ...initialBiddingInfo },
  playScreen: { ...inititalPlayInfo },
  chatScreen: {...initialChatUnfo},
  output: '',
  northAvtar: svgImages.joinSymbol,
  eastAvtar: svgImages.joinSymbol,
  southAvtar: svgImages.joinSymbol,
  westAvtar: svgImages.joinSymbol,
  northUser: Strings.open,
  eastUser: Strings.open,
  southUser: Strings.open,
  westUser: Strings.open,
  iskibitzer: false,
  eyeIndicatorClicked: false,
  isChangeDeal: false,
  changeDealResponed: false,
  changeDealResponseText: '',
  changeDealType: '',
  changeDealUser: '',
  TrickScoreNS: '00',
  TrickScoreEW: '00',
  dealScoreNS: '00',
  dealScoreEW: '00',
  runningScoreNS: '00',
  runningScoreEW: '00',
  showHistory: false,
  showChatWindow: true,
  showSubMenuPopup: false,
  showCheatSheet:false,
  showClaimPopup: false,
  // showUserProfile: false,
  profileName: '',
  profileSubname: '',
  kickoutUserDirection: '',
  kickedUserisHost: '',
  remainingTricks: 13,
  claimType: 'CLAIM',
  claimRequested: false,
  biddings: { N: [], E: [], S: [], W: [] },
  bidInfoList: [],
  lastBidRound: 1,
  tricks: { N: [], E: [], S: [], W: [] },
  initialPlayer: '',
  arrangedSides: [],
  reqUndoimg: svgImages.undo,
  showChatWindow: false,
  showAddaFriend: false,
  affiliation: null,
  roundCount: 0,
  fromPlayBot: false,
};


const mapStateToProps = (state) => ({
  chatScreen: state.chatActions.showChatWindow,
  isFirstTime: state.chatActions.isFirstTime,
  lastUserChat: state.chatActions.lastUserChat,
  addPlayer: state.joinTableActions.addPlayer,
  direction: state.joinTableActions.direction,
  addFriend: state.joinTableActions.addFriend,
  isNorthInvited: state.chatActions.isNorthInvited,
  isSouthInvited: state.chatActions.isSouthInvited,
  isWestInvited: state.chatActions.isWestInvited,
  isEastInvited: state.chatActions.isEastInvited,
  directionWest: state.joinTableActions.directionWest,
  directionNorth: state.joinTableActions.directionNorth,
  directionEast: state.joinTableActions.directionEast,
  directionSouth: state.joinTableActions.directionSouth,
  currentDirection: state.joinTableActions.currentDirection,
  northUserName: state.chatActions.northUserName,
  southUserName: state.chatActions.southUserName,
  eastUserName: state.chatActions.eastUserName,
  westUserName: state.chatActions.westUserName,
  isHomeUser: state.chatActions.homeUser,
  host: state.chatActions.host,
  gameTableId: state.joinTableActions.tableId,
  removePlayer: state.joinTableActions.removePlayer,
  removePlayerDirection: state.joinTableActions.removePlayerDirection,
  addBot: state.joinTableActions.addBot,
  hostName: state.chatActions.hostName,
  tableMessages: state.chatActions.tableMessages,
  tableList: state.chatActions.tableList,
  tableNotificationCount: state.chatActions.tableNotificationCount,
  lastClickedUser: state.chatActions.lastClickedUser
});

const mapDispatchToProps = (dispatch) => ({
  isTableUser: (params) =>
    dispatch({ type: ChatConstants.SET_IS_TABLE_USER_TYPE, params: params }),
  setFirstTimeAndUserInfo: (params) =>
    dispatch({ type: ChatConstants.IS_FIRST_TIME_USER, params: params }),
  setLastUserChat: (params) =>
    dispatch({ type: ChatConstants.LAST_USER_CHATTED, params: params }),
  searchChatQuery: (params) =>
    dispatch({ type: ChatConstants.SEARCH_CHAT_QUERY, params: params }),
  cancelPlayerSelect: (params) =>
    dispatch({ type: JoinTableConstants.CANCEL_PLAYER_SELECT, params: params }),
  addBotPlayer: (params) =>
    dispatch({ type: JoinTableConstants.ADD_BOT, params: params }),
  addFriendPlayer: (params) =>
    dispatch({ type: JoinTableConstants.FRIENDS_LIST, params: params }),
  closeFriendsList: (params) =>
    dispatch({ type: JoinTableConstants.CLOSE_FRIEND_LIST, params: params }),
  showFriendsList: (params) =>
    dispatch({ type: ChatConstants.SHOW_FRIENDS_LIST, params: params }),
  addPlayers: (params) =>
    dispatch({ type: JoinTableConstants.ADD_PLAYER, params: params }),
  cancelDirectionRequest: (params) =>
    dispatch({ type: JoinTableConstants.CANCEL_REQUEST, params: params }),
  cancelInvitedRequest: (params) =>
    dispatch({ type: JoinTableConstants.CANCEL_REQUEST, params: params }),
  setHomeUser: (params) =>
    dispatch({ type: ChatConstants.SET_HOME_USER_WITH_HOST, params: params }),
  removePlayerOnTable: (params) =>
    dispatch({ type: JoinTableConstants.SHOW_REMOVE_PLAYER, params: params }),
  setTableId: (params) =>
    dispatch({ type: JoinTableConstants.SET_TABLE_ID, params: params }),
  setHostName: (params) =>
    dispatch({ type: ChatConstants.SET_HOST_NAME, params: params }),
  setTableMessages: (params) =>
    dispatch({ type: ChatConstants.SET_TABLE_MESSAGES, params: params}),
  setTableChatCount: (params) =>
    dispatch({ type: ChatConstants.TABLE_NOTIFICATION_COUNT, params: params}),
  setLastClickedUser: (params) =>
    dispatch({ type: ChatConstants.SET_LAST_CLICKED_USER , params: params}),
  setTableList: (params) =>
    dispatch({ type: ChatConstants.SET_TABLE_LIST , params: params}),
  setIndex: (params) =>
    dispatch({ type: ChatConstants.SET_CHAT_INDEX, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainDesk);




/**
 * <View style={{ ...styles.MenuBox }}>
                <TouchableOpacity onPress={() => this.toggleChat()} 
                  //disabled={true}
                  disabled={this.state.currentScreen == SCREENS.JOIN_TABLE}
                  // disabled={this.props.addPlayer}
                  // disabled={this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                  //   || this.state.currentScreen == SCREENS.JOIN_TABLE
                  //   || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD}
                >
                    {
                      (this.state.currentScreen == SCREENS.SHARE_INFO || this.state.currentScreen == SCREENS.BID_INFO
                        || this.state.currentScreen == SCREENS.JOIN_TABLE
                        || this.state.currentScreen == SCREENS.PLAY_SCREEN || this.state.currentScreen == SCREENS.SCOREBOARD) ?
                      !this.state.showChatWindow ? (
                        <SvgXml
                          height={scale(45)}
                          width={scale(45)}
                          xml={svgImages.chatDisbaled}
                        />
                      ) : (
                        <SvgXml
                          height={scale(42)}
                          width={scale(45)}
                          xml={svgImages.iconCancel}
                        />
                      )
                      :
                      (<SvgXml
                        height={scale(45)}
                        width={scale(44.95)}
                        xml={svgImages.chatDisbaled}
                      />)
                    }
                </TouchableOpacity>
                </View>

                {
                  
                  this.state.hitProfile && 
                  <UserProfile 
                    name={this.props.lastUserChat} 
                    currentScreen={'JOIN_TABLE'} 
                    isHost={true}
                    kickedUserisHost={false}
                  /> 
                  
                }
 */
