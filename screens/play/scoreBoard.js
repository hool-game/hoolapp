import * as React from 'react';
import {
  Animated,
  View,
  Text,
  Image,
  Dimensions,
  Easing,
  TouchableOpacity
} from 'react-native';
import { HOOL_CLIENT } from '../../shared/util';
const { jid } = require('@xmpp/client');
import { normalize } from '../../styles/global';
import { APPLABELS } from '../../constants/applabels';
import {
  scale,
  verticalScale,
  ScaledSheet,
  moderateScale
} from 'react-native-size-matters/extend';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { HOOL_EVENTS } from '../../constants/hoolEvents';

var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height;

var NorthCardWithKibitzerArea = (376 / 640) * ScreenWidth;
var EastWestPalyCardArea = (345 / 360) * ScreenHeight;
var HeightEastWestPalyCard = (360 / 360) * ScreenHeight;

const cardDimension = 35;
var cardWidth = scale(cardDimension);
var kibitzerCardWidth = scale(29);
var TotalCardWidthSouth = (533 / 640) * ScreenWidth;
var eastWestcardWidth = scale(25);
var KibitzerEastWestcardWidth = scale(25);

export default class ScoreBoard extends React.Component {
  constructor(props) {
    ///navigation.na
    super(props);
    this.state = {
      runningScoreEWFinal: 0,
      runningScoreEW: 0,
      scoreEW: styles.scoreText,
      animatedScore: new Animated.Value(0),
      runningScoreNS: '0',
      scoreNS: styles.scoreText,
      trickScore: 0,
      dealScoreEW: '',
      trickScoreEW: '00',
      trickScoreNS: '00',
      runningScoreNSFinal: 0,
      dealScoreNS: '',
      dealWinner: '',
      playTimer: require('../../assets/images/animation_0.png'),
      cancelAnimateImg: false,
      northText: styles.shareHandTextHandName,
      eastText: styles.shareHandTextHandName,
      southText: styles.shareHandTextHandName,
      westText: styles.shareHandTextHandName,
      bgNS: '#151515',
      bgEW: '#151515',
      eastHands: [],
      westHands: [],
      northHands: [],
      southHands: []
    };
  }

  componentDidUpdate(prevProps){
    if ( this.props.chatScreen !== prevProps.chatScreen) {
      this.setState({ chatScreen: this.props.chatScreen})
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.setState({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.setState({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.setState({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.setState({ westUser: this.props.westUser });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    const gamestate = this.props.gamestate;
    // console.log('all hands');
    console.log(gamestate);
    this.setState({ ...this.props, isValueInitialized: true }, () => {});
    this.setState({
      northSharedType1: gamestate.northSharedType1,
      northSharedValue1: gamestate.northSharedValue1,
      northSharedType2: gamestate.northSharedType2,
      northSharedValue2: gamestate.northSharedValue2,
      eastSharedType1: gamestate.eastSharedType1,
      eastSharedValue1: gamestate.eastSharedValue1,
      eastSharedType2: gamestate.eastSharedType2,
      eastSharedValue2: gamestate.eastSharedValue2,
      southSharedType1: gamestate.southSharedType1,
      southSharedValue1: gamestate.southSharedValue1,
      southSharedType2: gamestate.southSharedType2,
      southSharedValue2: gamestate.southSharedValue2,
      westSharedType1: gamestate.westSharedType1,
      westSharedValue1: gamestate.westSharedValue1,
      westSharedType2: gamestate.westSharedType2,
      westSharedValue2: gamestate.westSharedValue2,
      winnerSuit: gamestate.winnerSuit,
      winnerLevel: gamestate.winnerLevel,
      winnerSide: gamestate.winnerSide,
      winnerSuitImage: gamestate.winnerSuitImage,
      winnerdblRedbl: gamestate.winnerdblRedbl,
      mySeat: gamestate.mySeat,
      myLHOSeat: gamestate.myLHOSeat,
      myPartnerSeat: gamestate.myPartnerSeat,
      myRHOSeat: gamestate.myRHOSeat,
      southUser: gamestate.myUsername,
      northUser: gamestate.myPartnername,
      westUser: gamestate.myLHOname,
      eastUser: gamestate.myRHOname,
      trickScoreNS: gamestate.trickScoreNS,
      trickScoreEW: gamestate.trickScoreEW,
      dealWinner: gamestate.dealWinner
    });
    console.log(gamestate.dealWinner);

    this.getTableInfo();

    this.emitterScoreboard = HOOL_CLIENT.addListener(
      HOOL_EVENTS.SCORE_BOARD,
      e => {
        console.log('scoreboard');
        console.log(e);
        var jidlocal = jid(e.user);
        console.log(jidlocal);
        var textColor = styles.shareHandTextHandName;
        if (e.mode === 'ready') {
          textColor = styles.shareHandTextHandNameActive;
        } else {
          textColor = styles.shareHandTextHandNameRed;
        }
        if (
          e.side === this.state.myPartnerSeat ||
          e.side === this.state.mySeat
        ) {
          if (e.side === this.state.myPartnerSeat) {
            this.setState({
              northUser: jidlocal._resource,
              northText: textColor
            });
          } else if (e.side === this.state.mySeat) {
            this.setState({
              southUser: jidlocal._resource,
              southText: textColor
            });
          }
        } else if (
          e.side === this.state.myRHOSeat ||
          e.side === this.state.myLHOSeat
        ) {
          if (e.side === this.state.myRHOSeat) {
            this.setState({
              eastUser: jidlocal._resource,
              eastText: textColor
            });
          } else if (e.side === this.state.myLHOSeat) {
            this.setState({
              westUser: jidlocal._resource,
              westText: textColor
            });
          }
        }
      }
    );

    this.emitterScoreboardError = HOOL_CLIENT.addListener(
      HOOL_EVENTS.SCORE_BOARD_ERROR,
      e => {
        alert(
          `Failed to make scoreboard request on ${e.table || 'the table'}: ${
            e.tag || 'unknown error'
          }: ${e.text || 'unknown error'}`
        );
      }
    );
  }

  getChatMessage(seat) {
    if( seat === 'N'){
      if(this.state.chatScreen.isNorthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'S'){
      if(this.state.chatScreen.isSouthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'W'){
      if(this.state.chatScreen.isWestSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'E'){
      if(this.state.chatScreen.isEastSentMsg){
         return false
      }else{
         return true
      }
    }else{
      return true
    }
  }

  updateScore() {
    console.log('inside updatescore');
    const { navigation } = this.props;
    const gamestate = this.props.gamestate;
    //// console.log(this.state.animatedScore);
    if (gamestate.dealWinner === 'EW') {
      this.state.animatedScore.addListener(({ value }) => {
        const dealScore = Number(this.state.runningScoreEWFinal) - value;
        //console.log('dealScore');
        // console.log(dealScore + ', '+value +' ,'+this.state.runningScoreEWFinal);
        this.setState({ runningScoreEW: Math.round(value) });
        this.setState({ dealScoreEW: '+' + Math.round(dealScore) });
        if (dealScore === 0) {
          setTimeout(() => {
            this.setState({ dealScoreEW: '' });
          }, 100);
        }
      });
      Animated.timing(this.state.animatedScore, {
        delay: 100,
        toValue: Number(this.state.runningScoreEWFinal),
        duration: 1000,
        useNativeDriver: true,
        easing: Easing.out(Easing.ease)
      }).start();
    } else if (gamestate.dealWinner === 'NS') {
      this.state.animatedScore.addListener(({ value }) => {
        const dealScore = Number(this.state.runningScoreNSFinal) - value;
        //console.log('dealScore');
        // console.log(dealScore + ', '+value +' ,'+this.state.runningScoreEWFinal);
        this.setState({ runningScoreNS: Math.round(value) });
        this.setState({ dealScoreNS: Math.round(dealScore) });
        if (dealScore === 0) {
          setTimeout(() => {
            this.setState({ dealScoreNS: '' });
          }, 100);
        }
      });
      Animated.timing(this.state.animatedScore, {
        delay: 100,
        toValue: Number(this.state.runningScoreNSFinal),
        duration: 1000,
        useNativeDriver: true,
        easing: Easing.out(Easing.ease)
      }).start();
    }
  }

  animateImg() {
    var arrImg = [
      { img: require('../../assets/images/animation_1.png') },
      { img: require('../../assets/images/animation_2.png') },
      { img: require('../../assets/images/animation_3.png') },
      { img: require('../../assets/images/animation_4.png') },
      { img: require('../../assets/images/animation_5.png') },
      { img: require('../../assets/images/animation_6.png') },
      { img: require('../../assets/images/animation_7.png') },
      { img: require('../../assets/images/animation_8.png') },
      { img: require('../../assets/images/animation_9.png') },
      { img: require('../../assets/images/animation_10.png') },
      { img: require('../../assets/images/animation_11.png') },
      { img: require('../../assets/images/animation_12.png') },
      { img: require('../../assets/images/animation_13.png') },
      { img: require('../../assets/images/animation_14.png') },
      { img: require('../../assets/images/animation_15.png') },
      { img: require('../../assets/images/animation_16.png') }
    ];
    console.log('arrImg');
    //console.log(arrImg);
    var ir = 0;
    const interv = setInterval(() => {
      if (ir > 15 || this.state.cancelAnimateImg) {
        clearInterval(interv);
        if (ir > 15) {
          this.scoreboardResponse(1);
        }
      } else {
        this.setState({ playTimer: arrImg[ir].img });
        ir++;
      }
    }, 1000);
  }

  scoreboardResponse(resp) {
    const gamestate = this.props.gamestate;

    this.setState({ cancelAnimateImg: true });

    gamestate.northSharedType1 = '';
    gamestate.northSharedValue1 = '';
    gamestate.northSharedType2 = '';
    gamestate.northSharedValue2 = '';
    gamestate.eastSharedType1 = '';
    gamestate.eastSharedValue1 = '';
    gamestate.eastSharedType2 = '';
    gamestate.eastSharedValue2 = '';
    gamestate.southSharedType1 = '';
    gamestate.southSharedValue1 = '';
    gamestate.southSharedType2 = '';
    gamestate.southSharedValue2 = '';
    gamestate.westSharedType1 = '';
    gamestate.westSharedValue1 = '';
    gamestate.westSharedType2 = '';
    gamestate.westSharedValue2 = '';
    gamestate.winnerSuit = '';
    gamestate.winnerLevel = '';
    gamestate.winnerSide = '';
    gamestate.winnerSuitImage = '';
    gamestate.winnerdblRedbl = '';
    gamestate.contractinfo = [];
    gamestate.bidInfo = [];
    gamestate.tricks = [];
    gamestate.tableinfo = [];
    gamestate.DummySeat = [];
    console.log(gamestate);

    if (resp === 1) {
      if (this.state.iskibitzer) return;
      HOOL_CLIENT.scoreboard(this.state.tableID, 'ready', gamestate.mySeat);
    } else {
      this.setState({
        playTimer: require('../../assets/images/play_green.png')
      });
      // hool.scoreboard(this.state.tableID,'notready', gamestate.mySeat)
    }
  }

  getTableInfo() {
    const { navigation } = this.props;
    const tableID = this.props.tableID;
    this.setState({ tableID: tableID });
    //If the device length is less than total card size then use the vertical scale proportion
    if (TotalCardWidthSouth < 13 * scale(cardDimension))
      cardWidth = verticalScale(cardDimension);
    TotalCardWidthSouth =
      13 * (this.state.iskibitzer ? kibitzerCardWidth : cardWidth);

    const gamestate = this.props.gamestate;
    console.log(gamestate);
    let myHand = gamestate.hands;
    let southHands = [];
    let northHands = [];
    let westHands = [];
    let eastHands = [];
    console.log('myHand', JSON.stringify(myHand));
    myHand.forEach((h, key) => {
      console.log(h);
      if (h.side === gamestate.mySeat) {
        southHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myPartnerSeat) {
        northHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myLHOSeat) {
        westHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myRHOSeat) {
        eastHands = this.displayCards(h.cards);
        eastHands = [...eastHands].reverse();
        //console.log('eastHands');
        // console.log(eastHands);
      }
    });
    this.setState({ southHands, northHands, westHands, eastHands });
    setTimeout(() => {
      if (gamestate.dealWinner === 'EW') {
        console.log(
          'inside EW ' + gamestate.runningScoreEW + ', ' + gamestate.dealScoreEW
        );
        this.setState({
          runningScoreEWFinal: gamestate.runningScoreEW,
          runningScoreEW:
            Number(gamestate.runningScoreEW) - Number(gamestate.dealScoreEW),
          animatedScore: new Animated.Value(
            Number(gamestate.runningScoreEW) - Number(gamestate.dealScoreEW)
          ),
          runningScoreNS: gamestate.runningScoreNS,
          dealScoreEW: '+' + gamestate.dealScoreEW,
          scoreEW: styles.scoreTextGreen,
          bgEW: '#DBFF00'
        });
      } else if (gamestate.dealWinner === 'NS') {
        this.setState({
          runningScoreNSFinal: gamestate.runningScoreNS,
          runningScoreNS:
            Number(gamestate.runningScoreNS) - Number(gamestate.dealScoreNS),
          animatedScore: new Animated.Value(
            Number(gamestate.runningScoreNS) - Number(gamestate.dealScoreNS)
          ),
          runningScoreEW: gamestate.runningScoreEW,
          dealScoreNS: '+' + gamestate.dealScoreNS,
          bgNS: '#DBFF00'
        });
      }
      console.log(
        'animatedScore ' +
          this.state.runningScoreEW +
          ' ,' +
          this.state.runningScoreEWFinal
      );
      setTimeout(() => {
        this.updateScore();
        //this.animateImg();
      }, 500);

      setTimeout(() => {
        this.animateImg();
      }, 3000);
    }, 5);
  }

  displayCards(hand) {
    var cardSide = [];
    var idx = 0;
    hand.forEach(card => {
      //56   console.log(card.suit);
      if (card.suit === 'S') {
        cardSide.push({
          rank: card.rank,
          xml: svgImages.spade,
          suit: 'S',
          key: card.rank + 'S',
          SuiteColor: '#C000FF',
          idx: idx
        });
        //Numberof_S = Numberof_S + 1;
      } else if (card.suit === 'D') {
        cardSide.push({
          rank: card.rank,
          xml: svgImages.diamond,
          suit: 'D',
          key: card.rank + 'D',
          SuiteColor: '#04AEFF',
          idx: idx
        });
        //Numberof_D = Numberof_D + 1;
      } else if (card.suit === 'H') {
        cardSide.push({
          rank: card.rank,
          xml: svgImages.hearts,
          suit: 'H',
          key: card.rank + 'H',
          SuiteColor: '#FF0C3E',
          idx: idx
        });
        // Numberof_H = Numberof_H + 1;
      } else if (card.suit === 'C') {
        cardSide.push({
          rank: card.rank,
          xml: svgImages.clubs,
          suit: 'C',
          key: card.rank + 'C',
          SuiteColor: '#79E62B',
          idx: idx
        });
        // Numberof_C = Numberof_C + 1;
      }
      idx++;
    });
    return cardSide;
  }

  componentWillUnmount() {
    this.state.animatedScore.removeAllListeners();
  }
  drawCards = (hands, handsDirection) => {
    var svgImg;
    return hands.map(element => {
      if (element.suit === 'S') {
        svgImg = svgImages.spade;
      } else if (element.suit === 'D') {
        svgImg = svgImages.diamond;
      } else if (element.suit === 'H') {
        svgImg = svgImages.hearts;
      } else if (element.suit === 'C') {
        svgImg = svgImages.clubs;
      }
      var cardSize = kibitzerCardWidth;
      if (handsDirection == 'E' || handsDirection == 'W') {
        cardSize = this.state.iskibitzer
          ? KibitzerEastWestcardWidth
          : eastWestcardWidth;
      }
      return (
        <View
          style={[
            styles.cardDisplayBodyNorth,
            {
              height: scale(45),
              width: cardSize
            }
          ]}
          key={element.key}
        >
          <View
            style={{
              width: scale(20),
              marginTop: moderateScale(3),
              marginLeft: moderateScale(1),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <SvgXml height={scale(10)} width={scale(9)} xml={svgImg} />
          </View>
        </View>
      );
    });
  };

  render() {
    return this.state.isValueInitialized ? (
      <>
         <View
            style={{
              position: 'absolute',
              left: 0,
              width: scale(68),
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                width: HeightEastWestPalyCard,
                height: scale(68),
                transform: [{ rotate: '-90deg' }],
                alignItems: 'center',
                alignSelf:'center',
                justifyContent: 'center',}}
            >
              <View
                style={{
                  width: EastWestPalyCardArea,
                  height: scale(45),
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignSelf: 'center',
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                  }}
                >
                {this.drawCards(this.state.westHands, 'W')}
                </View>
                </View>
                <TouchableOpacity
                    disabled={!this.state.chatScreen.isWestSentMsg}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                <View style={styles.hanNameWidthHeightArea} >
                  <Text style={this.state.westText}  numberOfLines={1} ellipsizeMode={'tail'}>{this.state.westUser}</Text>
                </View>
                </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              position: 'absolute',
              right: 0,
              width: scale(68),
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf:'center'
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                width: HeightEastWestPalyCard,
                height: scale(68),
                transform: [{ rotate: '90deg' }],
                alignSelf:'center',
                alignItems: 'center',
                justifyContent: 'center',}}
            >
              <View
                style={{
                  width: EastWestPalyCardArea,
                  height: scale(45),
                  justifyContent: 'space-around',
                  alignSelf:'center'
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                  }}
                >
                  {this.drawCards(this.state.eastHands, 'E')}
                </View>
              </View>
                <TouchableOpacity
                    disabled={!this.state.chatScreen.isEastSentMsg}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                  <View style={styles.hanNameWidthHeightArea}>
                    <Text style={this.state.eastText}  numberOfLines={1} ellipsizeMode={'tail'}>{this.state.eastUser}</Text>
                  </View>
                </TouchableOpacity> 
            </View>
        </View>

        <View style={{ ...styles.scoreTotalBody }}>
          <View style={styles.nsewAreaBodyAndScoreTotalWidth}>
            <View style={styles.nsewAreaBodyAndScore}>
              <View style={styles.nsewAreaBody}>
                <Text style={styles.scoreContractText}>N • S</Text>
              </View>
              <Text style={this.state.scoreNS}>
                {this.state.runningScoreNS}
              </Text>
              <Text style={styles.scoreTextGreenPlus}>
                {this.state.dealScoreNS}
              </Text>
            </View>
            <View style={styles.nsewAreaBodyAndScore}>
              <View style={styles.nsAreaBody}>
                <Text style={styles.scoreContractTextGreen}>E • W</Text>
              </View>
              <Text style={this.state.scoreEW}>
                {this.state.runningScoreEW}
              </Text>
              <Text style={styles.scoreTextGreenPlus}>
                {this.state.dealScoreEW}
              </Text>
            </View>
          </View>
          <View style={styles.nextDealAreaWidth}>
            <View>
              <TouchableOpacity onPress={() => this.scoreboardResponse(0)}>
                <SvgXml
                  width={scale(41)}
                  height={scale(41)}
                  xml={svgImages.joinSymbolPlay}
                />
              </TouchableOpacity>
            </View>
            <SvgXml width={scale(33)} xml={svgImages.scoreLine} />
            <Text style={{ ...styles.scoreNextDealText }}>
              {APPLABELS.TEXT_NEXT_DEAL}
            </Text>
            <SvgXml width={scale(33)} xml={svgImages.scoreLine} />
            <View>
              <TouchableOpacity onPress={() => this.scoreboardResponse(1)}>
                <Image
                  style={{ width: scale(41), height: scale(41) }}
                  source={this.state.playTimer}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            width: NorthCardWithKibitzerArea,
            position: 'absolute',
            top: 0,
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf:'center',
          }}
        >
          <View
            style={{
              width: NorthCardWithKibitzerArea,
              height: scale(45),
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center'
            }}
          >
            {this.drawCards(this.state.northHands, 'N')}
          </View>
        </View>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            top: scale(45),
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf:'center'
          }}
        >
          <TouchableOpacity
            disabled={!this.state.chatScreen.isNorthSentMsg}
            onPress={() => {
                this.props.showTableMessages()
            }}
          >
          <View
            style={{
              ...styles.hanNameWidthHeightArea,
              height: scale(23),
            }}
          >
            <Text style={this.state.northText}  numberOfLines={1} ellipsizeMode={'tail'}>{this.state.northUser}</Text>
          </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            bottom: scale(45),
            alignSelf:'center'
          }}
        >
          <TouchableOpacity
            disabled={!this.state.chatScreen.isSouthSentMsg}
            onPress={() => {
                this.props.showTableMessages()
            }}
          >
            <View
              style={{
                ...styles.hanNameWidthHeightArea,
              }}
            >
              <Text style={this.state.southText}  numberOfLines={1} ellipsizeMode={'tail'} >{this.state.southUser}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: NorthCardWithKibitzerArea,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              width: NorthCardWithKibitzerArea,
              height: scale(45),
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center'
            }}
          >
            {this.drawCards(this.state.southHands, 'S')}
          </View>
        </View>
      </>
    ) : null;
  }
}

const styles = ScaledSheet.create({
  tkbImgMrgTp: {
    marginTop: '4@ms',
    marginLeft: '8@ms',
    marginRight: '8@ms'
  },
  shareHandTextHandName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandTextHandNameActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandTextHandNameBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#0a0a0a',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandTextHandNameRed: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#FF0C3E',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },

  /********************************* */
  nsewAreaBodyAndScore: {
    width: scale(190),
    flexDirection: 'column',
    alignItems: 'center'
  },
  nsewAreaBody: {
    marginTop: '8@ms',
    width: scale(30),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center'
  },
  nsAreaBody: {
    marginTop: '8@ms',
    width: scale(32),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center'
  },
  scoreNextDealText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: 'normal',
    color: '#b8b8b8'
  },
  scoreContractText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: 'normal',
    color: '#b8b8b8'
  },
  scoreContractTextGreen: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: 'normal',
    color: '#DBFF00'
  },
  scoreText: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(41),
    fontWeight: 'normal',
    color: '#b8b8b8'
  },
  scoreTextGreen: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(41),
    fontWeight: 'normal',
    color: '#DBFF00'
  },
  scoreTextGreenPlus: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: 'normal',
    color: '#DBFF00',
    paddingTop: '8@ms'
  },
  nsewAreaBodyAndScoreTotalWidth: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  scoreTotalBody: {
    top:scale(93),
    position: 'absolute',
    width:  '380@s',
    height: '180@s',
    borderWidth: 0,
    alignSelf:'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  nextDealAreaWidth: {
    top:scale(134),
    position: 'absolute',
    width: '235@s',
    height: '41@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center'
  },
  /************************************/

  patternTotalRowArea: {
    flexDirection: 'row',
    width: '181@s',
    height: '45@s',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  hanSauthTotalWidthHeightArea: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '68@s',
    flexDirection: 'column'
  },
  hanNorthTotalWidthHeightArea: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '68@s',
    flexDirection: 'column'
  },
  hanNameWidthHeightArea: {
    width: scale(180),
    height: '23@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'center'
  },
  patternWidthHeightArea: {
    width: '90@s',
    height: '45@s',
    backgroundColor: '#151515'
  },

  cardDisplayBodyNorth: {
    borderBottomLeftRadius: '4@ms',
    borderBottomRightRadius: '4@ms',
    borderWidth: '1@s',
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardDisplayBodyEast: {
    width: '40@s',
    height: '45@s',
    borderBottomLeftRadius: '4@ms',
    borderBottomRightRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515',
    flexDirection: 'row-reverse'
  },
  cardPosition: {
    width: '22@s',
    marginTop: '3@ms',
    marginLeft: '1@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardPositionW: {
    width: '22@s',
    marginTop: '3@ms',
    marginLeft: '1@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardPositionE: {
    width: '22@s',
    marginTop: -4,
    marginLeft: '2@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  directionPointerPosition: {
    width: '27@s',
    height: '27@s',
    borderWidth: 0,
    borderColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#C000FF'
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#79E62B'
  },
  bidPlayText: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D',
    fontWeight: 'normal'
  },
  bidPlayTextBlack: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#0a0a0a',
    fontWeight: 'normal'
  },
  CardPositionBorderAss: {
    width: '55@s',
    height: '81@s',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#676767'
  },
  northCardPosition: {
    width: '55@s',
    height: '81@s'
  },
  sauthCardPosition: {
    width: '55@s',
    height: '81@s'
  },
  westCardPosition: {
    width: '55@s',
    height: '81@s',
    marginLeft: '12@ms',
    transform: [{ rotate: '-90deg' }]
  },
  eastCardPosition: {
    width: '55@s',
    height: '81@s',
    marginRight: '12@ms',
    transform: [{ rotate: '90deg' }]
  },
  playCardNumberPosition: {
    position: 'absolute',
    width: '55@s',
    height: '81@s',
    borderWidth: 0,
    borderColor: 'red',
    paddingTop: '2@ms',
    paddingLeft: '6@ms'
  },
  playCardNumberDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  playCardDiamond: {
    width: '55@s',
    height: '81@s',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#04AEFF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  playCardHearts: {
    width: '55@s',
    height: '81@s',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#FF0C3E',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  playCardClubs: {
    width: '55@s',
    height: '81@s',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#79E62B',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  playCardSpade: {
    width: '55@s',
    height: '81@s',
    borderRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#C000FF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardDirectionPosition: {
    width: '60@s',
    height: '60@s',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#676767',
    textAlign: 'center'
  },
  DirectionTextActive: {
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },

  /*************************************** */

  cardDisplayBody: {
    height: '45@s',
    borderTopLeftRadius: '4@ms',
    borderTopRightRadius: '4@ms',
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D'
  },
  ContractText: {
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    top: '8@ms'
  },
  ContractTextBlack: {
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#0a0a0a',
    top: '8@ms'
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingRight: '4@ms'
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center'
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: '4@ms'
  }
});
