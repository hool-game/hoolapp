import React, { Component } from 'react';
import SafeArea, { type, SafeAreaInsets } from 'react-native-safe-area';
import {
  View,
  Text,
  Image,
  Dimensions,
  LogBox,
  TouchableWithoutFeedback,
  Platform
} from 'react-native';
import {
  formatSharedValue,
  loadTextOrAddOption,
  getUserNameBasedOnHost,
  HOOL_CLIENT
} from '../../shared/util';
import { normalize } from '../../styles/global';
//import global from './shared/global';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import {
  ScaledSheet,
  moderateScale,
  moderateVerticalScale,
  scale,
  verticalScale
} from 'react-native-size-matters/extend';
import DeviceInfo, { isTablet } from 'react-native-device-info';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Strings } from '../../styles/Strings';
import AddPlayer from '../../shared/AddPlayer';
import { NavigationActions, StackActions } from '@react-navigation/native';
import CrownHost from '../../shared/CrownHost';
var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height; //-30;

const cardDimension = 35;
var cardWidth = scale(cardDimension);
var kibitzerCardWidth = scale(29);
var TotalCardWidthSouth = (533 / 640) * ScreenWidth;
var eastWestcardWidth = scale(25);
var KibitzerEastWestcardWidth = scale(25);
var hostLeft = 0;
var hostRight = 0;
var BidWidth

export default class BidInfo extends React.Component {
  constructor(props) {
    ///navigation.na
    super(props);
    this.state = {
      NSStyle: styles.ContractTextActive,
      NSBIDStyle: styles.BidTextActive,
      EWStyle: styles.ContractTextActive,
      EWBIDStyle: styles.BidTextActive,
      isTablet: DeviceInfo.isTablet(),
      eastHands: [],
      westHands: [],
      northHands: [],
      southHands: [],
      hands: [],
      northBidInfo: false,
      southBidInfo: false,
      westBidInfo: false,
      eastBidInfo: false,

    };
    LogBox.ignoreAllLogs();
  }

  componentDidMount() {
    console.log('BidScreen componentDidMount called');
    this.state.isTablet?BidWidth=ScreenWidth/2:BidWidth=ScreenWidth/1.8;
    SafeArea.getSafeAreaInsetsForRootView().then(result => {
      console.log(result);
      TotalCardWidth =
        ScreenWidth -
        (MenuBodyWidth +
          result.safeAreaInsets.left +
          result.safeAreaInsets.right);
      // { safeAreaInsets: { top: 44, left: 0, bottom: 34, right: 0 } }
      this.loadInitialScreenValues();
    });
    this.loadInitialScreenValues();
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.tableID !== prevProps.tableID) {
      this.updateStateValues({ tableID: this.props.tableID });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
    if ( this.props.chatScreen !== prevProps.chatScreen) {
      this.updateStateValues({ chatScreen: this.props.chatScreen})
    }
    if (this.props.northAvtar !== prevProps.northAvtar) {
      this.updateStateValues({ northAvtar: this.props.northAvtar });
    }
    if (this.props.eastAvtar !== prevProps.eastAvtar) {
      this.updateStateValues({ eastAvtar: this.props.eastAvtar });
    }
    if (this.props.southAvtar !== prevProps.southAvtar) {
      this.updateStateValues({ southAvtar: this.props.southAvtar });
    }
    if (this.props.westAvtar !== prevProps.westAvtar) {
      this.updateStateValues({ westAvtar: this.props.westAvtar });
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.updateStateValues({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.updateStateValues({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.updateStateValues({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.updateStateValues({ westUser: this.props.westUser });
    }
    if (this.props.iskibitzer !== prevProps.iskibitzer) {
      this.updateStateValues({ iskibitzer: this.props.iskibitzer });
    }
    if (this.props.infoScreen !== prevProps.infoScreen) {
      this.updateStateValues({ infoScreen: this.props.infoScreen });
    }
    if (this.props.bidInfoScreen !== prevProps.bidInfoScreen) {
      console.log('BidScreen Values Updated', this.props.bidInfoScreen);
      this.updateStateValues({ bidInfoScreen: this.props.bidInfoScreen });
    }

    if (this.props.eastUser == Strings.open && this.props.westUser == Strings.open
      && this.props.northUser == Strings.open && this.props.southUser == Strings.open 
      && this.props.iskibitzer) {
        HOOL_CLIENT.leaveTable(this.state.tableID);
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })]
        });
        this.props.navigation.dispatch(resetAction);
    }
    hostLeft = (this.props.gamestate.myLHOname.length+2) * 2
    hostRight = (this.props.gamestate.myRHOname.length+2) * 2
  }

  componentWillUnmount() {
    console.log('Bidding Screen closed');
  }

  loadInitialScreenValues() {
    console.log('Loading Table Screen values ' + this.props.tableID);
    this.setState(
      {
        Trick_Pass_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_1_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_2_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_3_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
        Trick_1_Text: styles.bidBoxWidthHeightTextClor,
        Trick_2_Text: styles.bidBoxWidthHeightTextClor,
        Trick_3_Text: styles.bidBoxWidthHeightTextClor,
        Trick_4_Text: styles.bidBoxWidthHeightTextClor,
        Trick_5_Text: styles.bidBoxWidthHeightTextClor,
        Trick_6_Text: styles.bidBoxWidthHeightTextClor,
        Trick_7_Text: styles.bidBoxWidthHeightTextClor,
        Double_bg: styles.bidBoxWidthHeightArea,
        Redouble_bg: styles.bidBoxWidthHeightArea,
        Double_Text: styles.bidDblTextClordisabled,
        Redouble_Text: styles.bidRedblTextClordisabled,
        Suit_Club_bg: styles.bidBoxWidthHeightArea,
        Suit_Diamond_bg: styles.bidBoxWidthHeightArea,
        Suit_Heart_bg: styles.bidBoxWidthHeightArea,
        Suit_Spade_bg: styles.bidBoxWidthHeightArea,
        NT_bg: styles.bidBoxWidthHeightArea,
        NT_Text: styles.bidBoxDisabledLevel,
        Suit_Club: svgImages.clubsBlack,
        Suit_Diamond: svgImages.diamondBlack,
        Suit_Heart: svgImages.heartsBlack,
        Suit_Spade: svgImages.spadesBlack,
        Suit_NT: svgImages.nt_black,
        Make_Bid: styles.bidBoxWidthHeightArea,
        Make_Bid_img: svgImages.bidNonActive,
        TricksSelected: '',
        TrumpSelected: '',
        EnableTrump_Club: true,
        EnableTrump_Diamond: true,
        EnableTrump_Heart: true,
        EnableTrump_Spade: true,
        EnableTrump_NT: true,
        EnableBid: true,
        Trick_Pass_Disabled: true,
        Trick_1_Disabled: true,
        Trick_2_Disabled: true,
        Trick_3_Disabled: true,
        Trick_4_Disabled: true,
        Trick_5_Disabled: true,
        Trick_6_Disabled: true,
        Trick_7_Disabled: true,
        ...this.props,
        isValueInitialized: true
      },
      () => {
        //Initialize Screen after values loaded
        // this.joinTable();

        let gamestate = this.state.gamestate;
        let bidInfoScreen = this.state.bidInfoScreen;
        let stateUpdate = {};
        bidInfoScreen = { ...bidInfoScreen };
        if (gamestate.side === 'N') {
          stateUpdate = {
            mySeat: 'N',
            myLHOSeat: 'E',
            myPartnerSeat: 'S',
            myRHOSeat: 'W',
            southUser: gamestate.northUser,
            westUser: gamestate.eastUser,
            northUser: gamestate.southUser,
            eastUser: gamestate.westUser,
            bidInfoScreen: {
              ...bidInfoScreen,
              DirectionTextMySeat: '#DBFF00',
              southUserText: '#DBFF00',
              DirectionTextRHO: '#DBFF00',
              eastUserText: '#DBFF00',
              DirectionTextPartner: '#DBFF00',
              northUserText: '#DBFF00',
              DirectionTextLHO: '#DBFF00',
              westUserText: '#DBFF00'
            }
          };
        } else if (gamestate.side === 'E') {
          stateUpdate = {
            mySeat: 'E',
            myLHOSeat: 'S',
            myPartnerSeat: 'W',
            myRHOSeat: 'N',
            southUser: gamestate.eastUser,
            westUser: gamestate.southUser,
            northUser: gamestate.westUser,
            eastUser: gamestate.northUser,
            bidInfoScreen: {
              ...bidInfoScreen,
              DirectionTextMySeat: '#DBFF00',
              southUserText: '#DBFF00',
              DirectionTextRHO: '#DBFF00',
              eastUserText: '#DBFF00',
              DirectionTextPartner: '#DBFF00',
              northUserText: '#DBFF00',
              DirectionTextLHO: '#DBFF00',
              westUserText: '#DBFF00'
            }
          };
        } else if (gamestate.side === 'S') {
          stateUpdate = {
            mySeat: 'S',
            myLHOSeat: 'W',
            myPartnerSeat: 'N',
            myRHOSeat: 'E',
            southUser: gamestate.southUser,
            westUser: gamestate.westUser,
            northUser: gamestate.northUser,
            eastUser: gamestate.eastUser,
            bidInfoScreen: {
              ...bidInfoScreen,
              DirectionTextMySeat: '#DBFF00',
              southUserText: '#DBFF00',
              DirectionTextRHO: '#DBFF00',
              eastUserText: '#DBFF00',
              DirectionTextPartner: '#DBFF00',
              northUserText: '#DBFF00',
              DirectionTextLHO: '#DBFF00',
              westUserText: '#DBFF00'
            }
          };
        } else if (gamestate.side === 'W') {
          stateUpdate = {
            mySeat: 'W',
            myLHOSeat: 'N',
            myPartnerSeat: 'E',
            myRHOSeat: 'S',
            southUser: gamestate.westUser,
            westUser: gamestate.northUser,
            northUser: gamestate.eastUser,
            eastUser: gamestate.southUser,
            bidInfoScreen: {
              ...bidInfoScreen,
              DirectionTextMySeat: '#DBFF00',
              southUserText: '#DBFF00',
              DirectionTextRHO: '#DBFF00',
              eastUserText: '#DBFF00',
              DirectionTextPartner: '#DBFF00',
              northUserText: '#DBFF00',
              DirectionTextLHO: '#DBFF00',
              westUserText: '#DBFF00'
            }
          };
        }
        stateUpdate = {
          ...stateUpdate,
          Trick_Pass_Disabled: false,
          Trick_1_Disabled: false,
          Trick_2_Disabled: false,
          Trick_3_Disabled: false,
          Trick_4_Disabled: false,
          Trick_5_Disabled: false,
          Trick_6_Disabled: false,
          Trick_7_Disabled: false
        };
        this.updateStateValues(stateUpdate, () => {
          this.getTableInfo();
        });
      }
    );
    hostLeft = (this.props.gamestate.myLHOname.length+2) * 2
    hostRight = (this.props.gamestate.myRHOname.length+2) * 2
  }

  updateStateValues(stateObj, callback) {
    this.setState(stateObj, callback);
  }

  getChatMessage(seat) {
    if( seat === 'N'){
      if(this.state.chatScreen.isNorthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'S'){
      if(this.state.chatScreen.isSouthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'W'){
      if(this.state.chatScreen.isWestSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'E'){
      if(this.state.chatScreen.isEastSentMsg){
         return false
      }else{
         return true
      }
    }else{
      return true
    }
  }

  async getTableInfo() {
    let gamestate = this.state.gamestate;
    let infoScreen = this.state.infoScreen;
    console.log(' gamestate as below');
    let myHand = gamestate.hands;
    let hands = [];
    let southHands = [];
    let northHands = [];
    let westHands = [];
    let eastHands = [];
    console.log('myHand', JSON.stringify(myHand));
    if (TotalCardWidthSouth < 13 * scale(cardDimension))
      cardWidth = verticalScale(cardDimension);
    TotalCardWidthSouth = 13 * (this.state.iskibitzer ? kibitzerCardWidth : cardWidth);
    myHand.forEach((h, key) => {
      console.log(h);
      if (h.side === gamestate.mySeat) {
        southHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myPartnerSeat) {
        northHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myLHOSeat) {
        westHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myRHOSeat) {
        eastHands = this.displayCards(h.cards);
        eastHands = [...eastHands].reverse();
        //console.log('eastHands');
        // console.log(eastHands);
      }
    });
    this.setState({ southHands, northHands, westHands, eastHands });
    myHand.forEach((h, key) => {
      console.log(h.infoShared);
      if (key === this.state.mySeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          //console.log(suitType);
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.southSharedType1 = suitType;
          infoScreen.southSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.southSharedType2 = suitType;
          infoScreen.southSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.southSharedType1 = suitType;
          infoScreen.southSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.myLHOSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          console.log(suitType);
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.westSharedType1 = suitType;
          infoScreen.westSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.westSharedType2 = suitType;
          infoScreen.westSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.westSharedType1 = suitType;
          infoScreen.westSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.myPartnerSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          console.log(suitType);
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.northSharedType1 = suitType;
          infoScreen.northSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.northSharedType2 = suitType;
          infoScreen.northSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.northSharedType1 = suitType;
          infoScreen.northSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.myRHOSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          console.log(suitType);
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType1 = suitType;
          infoScreen.eastSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType2 = suitType;
          infoScreen.eastSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === 'D') {
            suitType = 'Diamonds';
          } else if (suitType === 'C') {
            suitType = 'Clubs';
          } else if (suitType === 'H') {
            suitType = 'Hearts';
          } else if (suitType === 'S') {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType1 = suitType;
          infoScreen.eastSharedValue1 = h.infoShared[0].value;
        }
      }
    });

    this.setState({ infoScreen , hands });
  }

  showEligibleBids(level, suit) {
    console.log('inside eligiblebids');
    console.log(level, suit);
    if (suit === 'NT') {
      level = (parseInt(level) + 1).toString();
    }
    level = level.toString();
    console.log(level, suit);
    this.setState({
      Trick_Pass_Disabled: false,
      Trick_1_Disabled: true,
      Trick_2_Disabled: true,
      Trick_3_Disabled: true,
      Trick_4_Disabled: true,
      Trick_5_Disabled: true,
      Trick_6_Disabled: true,
      Trick_7_Disabled: true,
      Trick_1_bg: styles.bidBoxWidthHeightArea,
      Trick_2_bg: styles.bidBoxWidthHeightArea,
      Trick_3_bg: styles.bidBoxWidthHeightArea,
      Trick_4_bg: styles.bidBoxWidthHeightArea,
      Trick_5_bg: styles.bidBoxWidthHeightArea,
      Trick_6_bg: styles.bidBoxWidthHeightArea,
      Trick_7_bg: styles.bidBoxWidthHeightArea,
      Trick_1_Text: styles.bidBoxDisabledLevel,
      Trick_2_Text: styles.bidBoxDisabledLevel,
      Trick_3_Text: styles.bidBoxDisabledLevel,
      Trick_4_Text: styles.bidBoxDisabledLevel,
      Trick_5_Text: styles.bidBoxDisabledLevel,
      Trick_6_Text: styles.bidBoxDisabledLevel,
      Trick_7_Text: styles.bidBoxDisabledLevel,
      Suit_Club: svgImages.clubsBlack,
      Suit_Diamond: svgImages.diamondBlack,
      Suit_Heart: svgImages.heartsBlack,
      Suit_Spade: svgImages.spadesBlack,
      Suit_NT: svgImages.nt_black,
      NT_bg: styles.bidBoxWidthHeightArea,
      NT_Text: styles.bidBoxDisabledLevel,
      Make_Bid: styles.bidBoxWidthHeightArea,
      Make_Bid_img: svgImages.bidNonActive,
      Trick_Pass_bg: styles.bidBoxWidthHeightAreaActive,
      EnableBid: true,
      Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
      Double_bg: styles.bidBoxWidthHeightAreaActive,
      Double_Text: !this.state.bidInfoScreen.EnableDouble
        ? styles.bidBoxWidthHeightTextClor
        : styles.bidDblTextClordisabled,
      Redouble_Text: !this.state.bidInfoScreen.EnableRedouble
        ? styles.bidBoxWidthHeightTextClor
        : styles.bidRedblTextClordisabled,
      EnableTrump_Club: true,
      EnableTrump_Diamond: true,
      EnableTrump_Heart: true,
      EnableTrump_Spade: true,
      EnableTrump_NT: true
    });
    console.log('winnerlevel', this.state.bidInfoScreen.winnerLevel);
    if (this.state.bidInfoScreen.isOnlyDblOrReDbl) return;
    if (this.state.bidInfoScreen.winnerLevel !== 'level') {
      switch (level) {
        case '1': {
          this.setState({
            Trick_1_Disabled: false,
            Trick_2_Disabled: false,
            Trick_3_Disabled: false,
            Trick_4_Disabled: false,
            Trick_5_Disabled: false,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_1_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_2_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_3_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_1_Text: styles.bidBoxWidthHeightTextClor,
            Trick_2_Text: styles.bidBoxWidthHeightTextClor,
            Trick_3_Text: styles.bidBoxWidthHeightTextClor,
            Trick_4_Text: styles.bidBoxWidthHeightTextClor,
            Trick_5_Text: styles.bidBoxWidthHeightTextClor,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '2': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: false,
            Trick_3_Disabled: false,
            Trick_4_Disabled: false,
            Trick_5_Disabled: false,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_2_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_3_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_2_Text: styles.bidBoxWidthHeightTextClor,
            Trick_3_Text: styles.bidBoxWidthHeightTextClor,
            Trick_4_Text: styles.bidBoxWidthHeightTextClor,
            Trick_5_Text: styles.bidBoxWidthHeightTextClor,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '3': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: true,
            Trick_3_Disabled: false,
            Trick_4_Disabled: false,
            Trick_5_Disabled: false,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_3_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_3_Text: styles.bidBoxWidthHeightTextClor,
            Trick_4_Text: styles.bidBoxWidthHeightTextClor,
            Trick_5_Text: styles.bidBoxWidthHeightTextClor,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '4': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: true,
            Trick_3_Disabled: true,
            Trick_4_Disabled: false,
            Trick_5_Disabled: false,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_4_Text: styles.bidBoxWidthHeightTextClor,
            Trick_5_Text: styles.bidBoxWidthHeightTextClor,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '5': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: true,
            Trick_3_Disabled: true,
            Trick_4_Disabled: true,
            Trick_5_Disabled: false,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_5_Text: styles.bidBoxWidthHeightTextClor,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '6': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: true,
            Trick_3_Disabled: true,
            Trick_4_Disabled: true,
            Trick_5_Disabled: true,
            Trick_6_Disabled: false,
            Trick_7_Disabled: false,
            Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_6_Text: styles.bidBoxWidthHeightTextClor,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
        case '7': {
          this.setState({
            Trick_1_Disabled: true,
            Trick_2_Disabled: true,
            Trick_3_Disabled: true,
            Trick_4_Disabled: true,
            Trick_5_Disabled: true,
            Trick_6_Disabled: true,
            Trick_7_Disabled: false,
            Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
            Trick_7_Text: styles.bidBoxWidthHeightTextClor
          });
          break;
        }
      }
    }
  }

  showEligibleSuit(level) {
    if (this.state.bidInfoScreen.winnerLevel !== '') {
      if (Number(level) === Number(this.state.bidInfoScreen.winnerLevel)) {
        console.log(
          this.state.bidInfoScreen.winnerSuit,
          this.state.bidInfoScreen.winnerLevel,
          level
        );
        switch (this.state.bidInfoScreen.winnerSuit) {
          case 'C': {
            this.setState({
              Suit_Club: svgImages.clubsBlack,
              Suit_Diamond: svgImages.diamondInactive,
              Suit_Heart: svgImages.heartsInactive,
              Suit_Spade: svgImages.spadesInactive,
              //for nt svg
              Suit_NT: svgImages.ntIn_Active,
              EnableTrump_Club: true,
              EnableTrump_Diamond: false,
              EnableTrump_Heart: false,
              EnableTrump_Spade: false,
              EnableTrump_NT: false
            });
            break;
          }
          case 'D': {
            this.setState({
              Suit_Club: svgImages.clubsBlack,
              Suit_Diamond: svgImages.diamondBlack,
              Suit_Heart: svgImages.heartsInactive,
              Suit_Spade: svgImages.spadesInactive,
              Suit_NT: svgImages.ntIn_Active,
              EnableTrump_Club: true,
              EnableTrump_Diamond: true,
              EnableTrump_Heart: false,
              EnableTrump_Spade: false,
              EnableTrump_NT: false
            });
            break;
          }
          case 'H': {
            this.setState({
              Suit_Club: svgImages.clubsBlack,
              Suit_Diamond: svgImages.diamondBlack,
              Suit_Heart: svgImages.heartsBlack,
              Suit_Spade: svgImages.spadesInactive,
              Suit_NT: svgImages.ntIn_Active,
              EnableTrump_Club: true,
              EnableTrump_Diamond: true,
              EnableTrump_Heart: true,
              EnableTrump_Spade: false,
              EnableTrump_NT: false
            });
            break;
          }
          case 'S': {
            this.setState({
              Suit_Club: svgImages.clubsBlack,
              Suit_Diamond: svgImages.diamondBlack,
              Suit_Heart: svgImages.heartsBlack,
              Suit_Spade: svgImages.spadesBlack,
              Suit_NT: svgImages.ntIn_Active,
              EnableTrump_Club: true,
              EnableTrump_Diamond: true,
              EnableTrump_Heart: true,
              EnableTrump_Spade: true,
              EnableTrump_NT: false
            });
            break;
          }
          case 'NT': {
            this.setState({
              Suit_Club: svgImages.clubsBlack,
              Suit_Diamond: svgImages.diamondBlack,
              Suit_Heart: svgImages.heartsBlack,
              Suit_Spade: svgImages.spadesBlack,
              Suit_NT: svgImages.nt_black,
              EnableTrump_Club: true,
              EnableTrump_Diamond: true,
              EnableTrump_Heart: true,
              EnableTrump_Spade: true,
              EnableTrump_NT: true
            });
            break;
          }
        }
      }
    }
  }

  showDblOrRedbl(both) {
    this.setState({
      Trick_Pass_Disabled: false,
      Trick_1_Disabled: true,
      Trick_2_Disabled: true,
      Trick_3_Disabled: true,
      Trick_4_Disabled: true,
      Trick_5_Disabled: true,
      Trick_6_Disabled: true,
      Trick_7_Disabled: true,
      Trick_1_bg: styles.bidBoxWidthHeightArea,
      Trick_2_bg: styles.bidBoxWidthHeightArea,
      Trick_3_bg: styles.bidBoxWidthHeightArea,
      Trick_4_bg: styles.bidBoxWidthHeightArea,
      Trick_5_bg: styles.bidBoxWidthHeightArea,
      Trick_6_bg: styles.bidBoxWidthHeightArea,
      Trick_7_bg: styles.bidBoxWidthHeightArea,
      Trick_1_Text: styles.bidBoxDisabledLevel,
      Trick_2_Text: styles.bidBoxDisabledLevel,
      Trick_3_Text: styles.bidBoxDisabledLevel,
      Trick_4_Text: styles.bidBoxDisabledLevel,
      Trick_5_Text: styles.bidBoxDisabledLevel,
      Trick_6_Text: styles.bidBoxDisabledLevel,
      Trick_7_Text: styles.bidBoxDisabledLevel,
      Suit_Club: svgImages.clubsBlack,
      Suit_Diamond: svgImages.diamondBlack,
      Suit_Heart: svgImages.heartsBlack,
      Suit_Spade: svgImages.spadesBlack,
      Suit_NT: svgImages.nt_black,
      NT_bg: styles.bidBoxWidthHeightArea,
      NT_Text: styles.bidBoxDisabledLevel,
      EnableTrump_Club: true,
      EnableTrump_Diamond: true,
      EnableTrump_Heart: true,
      EnableTrump_Spade: true,
      EnableTrump_NT: true,
      Make_Bid: styles.bidBoxWidthHeightArea,
      EnableBid: true,
      Make_Bid_img: svgImages.bidNonActive,
      Double_bg: styles.bidBoxWidthHeightArea,
      Redouble_bg: styles.bidBoxWidthHeightArea,
      Double_Text: styles.bidDblTextClordisabled,
      Redouble_Text: styles.bidRedblTextClordisabled
    });
    if (both === 'dbl') {
      this.setState({
        Trick_Pass_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
        Double_bg: styles.bidBoxWidthHeightAreaActive,
        Double_Text: styles.bidBoxWidthHeightTextClor
      });
    } else {
      this.setState({
        Trick_Pass_bg: styles.bidBoxWidthHeightAreaActive,
        Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
        Redouble_bg: styles.bidBoxWidthHeightAreaActive,
        Redouble_Text: styles.bidBoxWidthHeightTextClor
      });
    }
  }

  SelectedTrick(trick) {
    let bidInfoScreen = this.state.bidInfoScreen;
    this.setState({
      Trick_Pass_bg: styles.bidBoxWidthHeightArea,
      Trick_1_bg: styles.bidBoxWidthHeightArea,
      Trick_2_bg: styles.bidBoxWidthHeightArea,
      Trick_3_bg: styles.bidBoxWidthHeightArea,
      Trick_4_bg: styles.bidBoxWidthHeightArea,
      Trick_5_bg: styles.bidBoxWidthHeightArea,
      Trick_6_bg: styles.bidBoxWidthHeightArea,
      Trick_7_bg: styles.bidBoxWidthHeightArea,
      Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
      Trick_1_Text: styles.bidBoxWidthHeightTextClor,
      Trick_2_Text: styles.bidBoxWidthHeightTextClor,
      Trick_3_Text: styles.bidBoxWidthHeightTextClor,
      Trick_4_Text: styles.bidBoxWidthHeightTextClor,
      Trick_5_Text: styles.bidBoxWidthHeightTextClor,
      Trick_6_Text: styles.bidBoxWidthHeightTextClor,
      Trick_7_Text: styles.bidBoxWidthHeightTextClor,
      Double_Text: !bidInfoScreen.EnableDouble
        ? styles.bidBoxWidthHeightTextClor
        : styles.bidDblTextClordisabled,
      Redouble_Text: !bidInfoScreen.EnableRedouble
        ? styles.bidBoxWidthHeightTextClor
        : styles.bidRedblTextClordisabled,
      Make_Bid: styles.bidBoxWidthHeightArea,
      Make_Bid_img: svgImages.bidNonActive,
      EnableBid: true,
      Suit_Club_bg: styles.bidBoxWidthHeightArea,
      Suit_Diamond_bg: styles.bidBoxWidthHeightArea,
      Suit_Heart_bg: styles.bidBoxWidthHeightArea,
      Suit_Spade_bg: styles.bidBoxWidthHeightArea,
      NT_bg: styles.bidBoxWidthHeightArea,
      NT_Text: styles.bidBoxDisabledLevel
    });

    if (bidInfoScreen.winnerLevel !== '') {
      this.showEligibleBids(
        bidInfoScreen.winnerLevel,
        bidInfoScreen.winnerSuit
      );
    }
    switch (trick) {
      case 'P': {
        bidInfoScreen.bid_level = '';
        bidInfoScreen.bid_type = 'pass';
        this.setState({
          Trick_Pass_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_Pass_Text: styles.bidBoxWidthHeightTextClorActive,
          Suit_Club: svgImages.clubsBlack,
          Suit_Diamond: svgImages.diamondBlack,
          Suit_Heart: svgImages.heartsBlack,
          Suit_Spade: svgImages.spadesBlack,
          Suit_NT: svgImages.nt_black,
          NT_bg: styles.bidBoxWidthHeightArea,
          NT_Text: styles.bidBoxDisabledLevel,
          EnableTrump_Club: true,
          EnableTrump_Diamond: true,
          EnableTrump_Heart: true,
          EnableTrump_Spade: true,
          EnableTrump_NT: true,
          EnableBid: false,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive
        });
        break;
      }
      case '1': {
        bidInfoScreen.bid_level = '1';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_1_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_1_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '2': {
        bidInfoScreen.bid_level = '2';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_2_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_2_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '3': {
        bidInfoScreen.bid_level = '3';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_3_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_3_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '4': {
        bidInfoScreen.bid_level = '4';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_4_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_4_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '5': {
        bidInfoScreen.bid_level = '5';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_5_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_5_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '6': {
        bidInfoScreen.bid_level = '6';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_6_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_6_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
      case '7': {
        bidInfoScreen.bid_level = '7';
        bidInfoScreen.bid_type = 'level';
        this.setState({
          Trick_7_bg: styles.bidBoxWidthHeightAreaActive,
          Trick_7_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: false,
          EnableTrump_Diamond: false,
          EnableTrump_Heart: false,
          EnableTrump_Spade: false,
          EnableTrump_NT: false
        });
        break;
      }
    }
    if (trick !== 'P') {
      this.setState({
        Suit_Club: svgImages.clubsInactive,
        Suit_Diamond: svgImages.diamondInactive,
        Suit_Heart: svgImages.heartsInactive,
        Suit_Spade: svgImages.spadesInactive,
        Suit_NT: svgImages.ntIn_Active,
        //NT_bg: styles.bidBoxWidthHeightArea,
        //NT_Text: styles.bidBoxWidthHeightTextClor
      });

      this.showEligibleSuit(trick);
    }
    this.props.updateDeskStateValues({ bidInfoScreen }, () => {});
  }

  SelectedTrump(trump) {
    let bidInfoScreen = this.state.bidInfoScreen;
    if (this.state.EnableTrump_Club) {
      this.setState({
        Suit_Club: svgImages.clubsBlack
      });
    } else {
      this.setState({
        Suit_Club: svgImages.clubsInactive
      });
    }
    if (this.state.EnableTrump_Diamond) {
      this.setState({
        Suit_Diamond: svgImages.diamondBlack
      });
    } else {
      this.setState({
        Suit_Diamond: svgImages.diamondInactive
      });
    }
    if (this.state.EnableTrump_Heart) {
      this.setState({
        Suit_Heart: svgImages.heartsBlack
      });
    } else {
      this.setState({
        Suit_Heart: svgImages.heartsInactive
      });
    }
    if (this.state.EnableTrump_Spade) {
      this.setState({
        Suit_Spade: svgImages.spadesBlack
      });
    } else {
      this.setState({
        Suit_Spade: svgImages.spadesInactive
      });
    }
    this.setState({
      Suit_Club_bg: styles.bidBoxWidthHeightArea,
      Suit_Diamond_bg: styles.bidBoxWidthHeightArea,
      Suit_Heart_bg: styles.bidBoxWidthHeightArea,
      Suit_Spade_bg: styles.bidBoxWidthHeightArea,
      NT_bg: styles.bidBoxWidthHeightArea,
      NT_Text: styles.bidBoxDisabledLevel
    });
    switch (trump) {
      case 'NT': {
        bidInfoScreen.bid_suit = 'NT';
        this.setState({
          Suit_NT: svgImages.ntActive,
          NT_bg: styles.bidBoxWidthHeightAreaActive,
          NT_Text: styles.bidBoxWidthHeightTextClorNTActive,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          EnableBid: false
        });
        break;
      }
      case 'C': {
        bidInfoScreen.bid_suit = 'C';
        this.setState({
          Suit_Club: svgImages.clubs,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          Suit_Club_bg: styles.bidBoxWidthHeightAreaActive,
          NT_bg: styles.bidBoxWidthHeightArea,
          NT_Text: styles.bidBoxWidthHeightTextClor,
          EnableBid: false
        });
        break;
      }
      case 'D': {
        bidInfoScreen.bid_suit = 'D';
        this.setState({
          Suit_Diamond: svgImages.diamond,
          NT_bg: styles.bidBoxWidthHeightArea,
          NT_Text: styles.bidBoxWidthHeightTextClor,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          Suit_Diamond_bg: styles.bidBoxWidthHeightAreaActive,
          EnableBid: false
        });
        break;
      }
      case 'H': {
        bidInfoScreen.bid_suit = 'H';
        this.setState({
          Suit_Heart: svgImages.hearts,
          NT_bg: styles.bidBoxWidthHeightArea,
          NT_Text: styles.bidBoxWidthHeightTextClor,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          Suit_Heart_bg: styles.bidBoxWidthHeightAreaActive,
          EnableBid: false
        });
        break;
      }
      case 'S': {
        bidInfoScreen.bid_suit = 'S';
        this.setState({
          Suit_Spade: svgImages.spade,
          NT_bg: styles.bidBoxWidthHeightArea,
          NT_Text: styles.bidBoxWidthHeightTextClor,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          Suit_Spade_bg: styles.bidBoxWidthHeightAreaActive,
          EnableBid: false
        });
        break;
      }
      case 'X': {
        bidInfoScreen.bid_suit = '';
        bidInfoScreen.bid_level = '';
        bidInfoScreen.bid_type = 'double';
        this.setState({
          Trick_1_Text: styles.bidBoxDisabledLevel,
          Trick_2_Text: styles.bidBoxDisabledLevel,
          Trick_3_Text: styles.bidBoxDisabledLevel,
          Trick_4_Text: styles.bidBoxDisabledLevel,
          Trick_5_Text: styles.bidBoxDisabledLevel,
          Trick_6_Text: styles.bidBoxDisabledLevel,
          Trick_7_Text: styles.bidBoxDisabledLevel,
          Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          EnableBid: false,
          Trick_Pass_Disabled: false,
          Double_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: true,
          EnableTrump_Diamond: true,
          EnableTrump_Heart: true,
          EnableTrump_Spade: true,
          EnableTrump_NT: true
        });
        break;
      }
      case 'XX': {
        bidInfoScreen.bid_suit = '';
        bidInfoScreen.bid_level = '';
        bidInfoScreen.bid_type = 'redouble';
        this.setState({
          Make_Bid: styles.bidBoxWidthHeightAreaBodrActive,
          Make_Bid_img: svgImages.bidActive,
          EnableBid: false,
          Trick_Pass_Text: styles.bidBoxWidthHeightTextClor,
          Redouble_Text: styles.bidBoxWidthHeightTextClorActive,
          EnableTrump_Club: true,
          EnableTrump_Diamond: true,
          EnableTrump_Heart: true,
          EnableTrump_Spade: true,
          EnableTrump_NT: true
        });
        break;
      }
    }
    this.props.updateDeskStateValues({ bidInfoScreen }, () => {});
  }

  makeBid() {
    let bidInfoScreen = this.state.bidInfoScreen;
    console.log(
      bidInfoScreen.bid_type +
        ',' +
        bidInfoScreen.bid_level +
        ',' +
        bidInfoScreen.bid_suit
    );
    bidInfoScreen.makeBidd = false;
    if (bidInfoScreen.bid_type === 'pass') {
      bidInfoScreen.my_bid_level = 'P';
      bidInfoScreen.my_bid_suit_image = '';
      bidInfoScreen.my_bid_suit_border_color =
        styles.bidCirclePositionBodrAssActive;
      bidInfoScreen.my_bid_suit_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextAssActive;
      bidInfoScreen.my_bid_suit_image = '';
    } else if (bidInfoScreen.bid_type === 'level') {
      bidInfoScreen.my_bid_level = bidInfoScreen.bid_level;
      if (bidInfoScreen.bid_suit === 'C') {
        bidInfoScreen.my_bid_suit_image = svgImages.clubs;
        bidInfoScreen.my_bid_suit_color = styles.clubsImageActive;
        bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextClubsActive;
        bidInfoScreen.my_bid_suit_border_color =
          styles.bidCirclePositionClubsActive;
      } else if (bidInfoScreen.bid_suit === 'D') {
        bidInfoScreen.my_bid_suit_image = svgImages.diamond;
        bidInfoScreen.my_bid_suit_color = styles.diamondImageActive;
        bidInfoScreen.my_bid_suit_text_color =
          styles.bidCircleTextDiamondActive;
        bidInfoScreen.my_bid_suit_border_color =
          styles.bidCirclePositionDiamondActive;
      } else if (bidInfoScreen.bid_suit === 'H') {
        bidInfoScreen.my_bid_suit_image = svgImages.hearts;
        bidInfoScreen.my_bid_suit_color = styles.heartsImageActive;
        bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextHeartsActive;
        bidInfoScreen.my_bid_suit_border_color =
          styles.bidCirclePositionHeartsActive;
      } else if (bidInfoScreen.bid_suit === 'S') {
        bidInfoScreen.my_bid_suit_image = svgImages.spade;
        bidInfoScreen.my_bid_suit_color = styles.spadeImageActive;
        bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextSpadeActive;
        bidInfoScreen.my_bid_suit_border_color =
          styles.bidCirclePositionSpadeActive;
        //bidCircleTextNTActive
      } else if (bidInfoScreen.bid_suit === 'NT') {
        bidInfoScreen.my_bid_suit_image = svgImages.nt;
        bidInfoScreen.my_bid_suit_color = styles.bidCircleTextAssActive;
        bidInfoScreen.my_bid_suit_text_color = styles.bidCircleTextNTActive;
        bidInfoScreen.my_bid_suit_border_color =
          styles.bidCirclePositionNTActive;
        //bidInfoScreen.my_bid_level = bidInfoScreen.bid_level + 'NT';
        //
      }
      bidInfoScreen.makeBidd = true;
    } else if (bidInfoScreen.bid_type === 'double') {
      bidInfoScreen.my_bid_level = 'X';
      bidInfoScreen.my_bid_suit = '';
    } else if (bidInfoScreen.bid_type === 'redouble') {
      bidInfoScreen.my_bid_level = 'XX';
      bidInfoScreen.my_bid_suit = '';
    }
    // TODO: autocalculate info value and share that as well
    console.log("After bidding, --  "+ bidInfoScreen.bid_level+ "and bid suit val --  "+ bidInfoScreen.bid_suit)
    bidInfoScreen.showBiddingBox = false;  
    bidInfoScreen.DirectionTextMySeat = '#6D6D6D';
    bidInfoScreen.southUserText = '#6D6D6D';
    this.props.updateDeskStateValues({ bidInfoScreen }, () => {
      HOOL_CLIENT.makeBid(
        this.state.tableID,
        this.state.mySeat,
        this.state.bidInfoScreen.bid_type,
        this.state.bidInfoScreen.bid_level,
        this.state.bidInfoScreen.bid_suit
      );
    });
    //console.log(this.state.tableID +',' +  this.state.mySeat +',' + this.state.bid_type +','+this.state.bid_level + ','+this.state.bid_suit);
  }

  displayCards(hand) {
    var cardSide = [];
    var idx = 0;
    hand.forEach(card => {
      //56   console.log(card.suit);
      if (card.suit === 'S') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.spade,
          suit: 'S',
          key: card.rank + 'S',
          SuiteColor: '#C000FF',
          idx: idx
        });
      } else if (card.suit === 'D') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.diamond,
          suit: 'D',
          key: card.rank + 'D',
          SuiteColor: '#04AEFF',
          idx: idx
        });
      } else if (card.suit === 'H') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.hearts,
          suit: 'H',
          key: card.rank + 'H',
          SuiteColor: '#FF0C3E',
          idx: idx
        });
      } else if (card.suit === 'C') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.clubs,
          suit: 'C',
          key: card.rank + 'C',
          SuiteColor: '#79E62B',
          idx: idx
        });
      }
      idx++;
    });
    return cardSide;
  }

  drawCards = (hands, handsDirection) => {
    var svgImg;
    var width, height, top, left, textLeft
    return hands.map(element => {
      if (element.suit === 'S') {
        svgImg = svgImages.spade;
        width = scale(11)
        height = scale(10.24)
        top = moderateVerticalScale(1)
        left = element.rank == '10' ? moderateScale(7): 0 
      } else if (element.suit === 'D') {
        svgImg = svgImages.diamond;
        width = scale(10)
        height = scale(11)
        top = moderateVerticalScale(1)
        left = element.rank == '10' ? moderateScale(7): 0 
      } else if (element.suit === 'H') {
        svgImg = svgImages.hearts;
        width = scale(11)
        height = scale(10.19)
        top = moderateVerticalScale(2)
        left = element.rank == '10' ? moderateScale(7): 0 
      } else if (element.suit === 'C') {
        svgImg = svgImages.clubs;
        width = scale(11.05)
        height = scale(11.08)
        top = moderateVerticalScale(1)
        left = element.rank == '10' ? moderateScale(7): 0 
      } /*else if (element.suit === 'NT') {
        svgImg = svgImages.nt;
        width = scale(11.05)
        height = scale(11.08)
        top = moderateVerticalScale(1)
        left = element.rank == '10' ? moderateScale(7): 0 
      }*/
      if(this.state.isTablet && Platform.OS == 'ios'){
        textLeft = moderateScale(3)
      }else{
        textLeft = 0
      }
      var cardSize = this.state.iskibitzer? kibitzerCardWidth:cardWidth
      if(handsDirection == 'E' || handsDirection == 'W'){
        cardSize = this.state.iskibitzer? KibitzerEastWestcardWidth:eastWestcardWidth
      }
      return (
        <View style={(this.state.isTablet && Platform.OS == 'ios') ? {...styles.cardDisplayBody, width: (cardSize)} : 
            {...styles.cardDisplayBody, width: (cardSize), paddingLeft: element.rank == 10 ? 4 : 8,}} key={element.key}>
            <Text
              numberOfLines={1}
              style={{...styles.SpadeCardNumber, color: element.SuiteColor, marginStart: textLeft,
              }}
            >
              {element.rank}
            </Text>
            <SvgXml height={height} width={width} xml={svgImg} style={{marginTop: top, marginStart: left}} />
        </View>
      );
    });
  };

  toggleSideSelection(side, bidInfoScreen, forceActivate, forceDisable) {
    console.log('side', side);
    if (side === 'E' || side === 'W') {
      console.log(bidInfoScreen.EWBIDBottom, forceActivate, forceDisable);
      if (
        (forceActivate || bidInfoScreen.EWBIDBottom === '#0a0a0a') &&
        !forceDisable
      ) {
        bidInfoScreen.EWStyle = styles.ContractTextActive;
        bidInfoScreen.EWBIDStyle = styles.BidTextActive;
        bidInfoScreen.EWBIDBottom = '#DBFF00';
      } else {
        bidInfoScreen.EWStyle = styles.ContractText;
        bidInfoScreen.EWBIDStyle = styles.BidText;
        bidInfoScreen.EWBIDBottom = '#0a0a0a';
      }
    } else if (side === 'N' || side === 'S') {
      console.log(bidInfoScreen.NSBIDBottom, forceActivate);
      if (
        (forceActivate || bidInfoScreen.NSBIDBottom === '#0a0a0a') &&
        !forceDisable
      ) {
        bidInfoScreen.NSStyle = styles.ContractTextActive;
        bidInfoScreen.NSBIDStyle = styles.BidTextActive;
        bidInfoScreen.NSBIDBottom = '#DBFF00';
      } else {
        bidInfoScreen.NSStyle = styles.ContractText;
        bidInfoScreen.NSBIDStyle = styles.BidText;
        bidInfoScreen.NSBIDBottom = '#0a0a0a';
      }
    }
    return bidInfoScreen;
  }

  toggleBidInfoSection(side){
    if(side == 'N'){
      this.updateStateValues({northBidInfo: true})
      setInterval(() => this.setState({ northBidInfo: false}), 5000);
    }else if(side == 'S'){
      this.updateStateValues({southBidInfo: true})
      setInterval(() => this.setState({ southBidInfo: false}), 5000);
    } else if(side == 'E'){
      this.updateStateValues({eastBidInfo: true})
      setInterval(() => this.setState({ eastBidInfo: false}), 5000);
    } else if(side == 'W'){
      this.updateStateValues({westBidInfo: true})
      setInterval(() => this.setState({ westBidInfo: false}), 5000);
    }
  }

  loadNormalImageForLoser = (imageType) => {
    if (imageType == 'C'){
      return svgImages.clubsWhite
    }else if (imageType == 'D'){
      return svgImages.diamondsWhite
    }else if (imageType == 'H'){
      return svgImages.heartsWhite
    }else if (imageType == 'S'){
      return svgImages.spadesWhite
    }
  }

  render() {
    return this.state.isValueInitialized ? (
      <View style={{width: '100%', height: '100%'}}>
        {
          // !this.state.bidInfoScreen.showBiddingBox || this.state.iskibitzer &&
          <View style={{
            height: '100%',
            alignSelf: 'center',
            justifyContent: 'center',
            position: 'absolute'
          }}>
            
              <Text
                style={{
                  ...styles.DirectionText,
                  color: this.state.bidInfoScreen.DirectionTextPartner,
                  marginBottom: moderateScale(3.89),
                }
                }
              >
                {this.state.myPartnerSeat}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >                
                <View style={{width:scale(10)}}>
                  <Text
                    style={{
                      ...
                      styles.DirectionText, color: this.state.bidInfoScreen.DirectionTextLHO,right: 3
                    }}
                  >
                    {this.state.myLHOSeat}
                  </Text>
                </View>
                <View style={{ width: scale(27), height: scale(27), alignItems: 'center', justifyContent: 'center'}}>
                  <SvgXml
                    width={scale(21)}
                    height={scale(21)} 
                    xml={svgImages.centerCircle}
                  />
                </View>
                <View style={{width:scale(10)}}>
                  <Text
                    style={[
                      styles.DirectionText,
                      { color: this.state.bidInfoScreen.DirectionTextRHO, left: 3 }
                    ]}
                  >
                    {this.state.myRHOSeat}
                  </Text>
                </View>
              </View>
              <Text
                style={{
                  ...styles.DirectionText,
                  color: this.state.bidInfoScreen.DirectionTextMySeat,
                  marginTop: moderateScale(3),
                }
                }
              >
                {this.state.mySeat}
              </Text>
            </View>
        }
         {/* BIDDING START */}
         {this.state.bidInfoScreen.showBiddingBox && !this.state.iskibitzer ? (
             <View style={{ ...styles.bidBoxPosition, 
                width: BidWidth, // for ex : 800
                height: '100%',
                alignSelf: 'center',
                alignItems: 'center',
                marginLeft: moderateScale(3),
                justifyContent: 'center',
                }}>
              <View style={{ ...styles.bidBoxRorPosition,               
                borderColor: this.state.EnableBid ? '#DBFF00' : '#0A0A0A', borderTopWidth: 1,
                borderLeftWidth:1, borderRightWidth:1,
                backgroundColor: '#0a0a0a',
                }}>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('P')}
                  disabled={this.state.Trick_Pass_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_Pass_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_Pass_Text}>Pass</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('1')}
                  disabled={this.state.Trick_1_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_1_bg,
                      ...styles.centerItems,
                      width: (BidWidth / 8)
                    }}
                  >
                    <Text style={{ ...this.state.Trick_1_Text }}>1</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('2')}
                  disabled={this.state.Trick_2_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_2_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_2_Text}>2</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('3')}
                  disabled={this.state.Trick_3_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_3_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_3_Text}>3</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('4')}
                  disabled={this.state.Trick_4_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_4_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_4_Text}>4</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('5')}
                  disabled={this.state.Trick_5_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_5_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_5_Text}>5</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('6')}
                  disabled={this.state.Trick_6_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_6_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_6_Text}>6</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrick('7')}
                  disabled={this.state.Trick_7_Disabled}
                >
                  <View
                    style={{
                      ...this.state.Trick_7_bg,
                      ...styles.centerItems,
                      width: BidWidth / 8
                    }}
                  >
                    <Text style={this.state.Trick_7_Text}>7</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{ ...styles.bidBoxRorPosition, borderColor: this.state.EnableBid ? '#DBFF00' : '#0A0A0A',
               borderBottomWidth: 1,
                borderLeftWidth:1, borderRightWidth:1,backgroundColor: '#0a0a0a',}}>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('X')}
                  disabled={this.state.bidInfoScreen.EnableDouble}
                >
                  <View style={{ ...this.state.Double_bg, width: BidWidth / 8 }}>
                    <Text
                      style={{
                        ...this.state.Double_Text
                      }}
                    >
                      x2
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('XX')}
                  disabled={this.state.bidInfoScreen.EnableRedouble}
                >
                  <View style={{ ...this.state.Redouble_bg, width: BidWidth / 8 }}>
                    <Text
                      style={{
                        ...this.state.Redouble_Text
                      }}
                    >
                      x4
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('C')}
                  disabled={this.state.EnableTrump_Club}
                >
                  <View style={{ ...this.state.Suit_Club_bg, width: BidWidth / 8 }}>
                    <SvgXml
                      width={scale(11.08)}
                      height={scale(15)}
                      xml={this.state.Suit_Club}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('D')}
                  disabled={this.state.EnableTrump_Diamond}
                >
                  <View style={{ ...this.state.Suit_Diamond_bg, width: BidWidth / 8 }}>
                    <SvgXml
                      width={scale(13)}
                      height={scale(13)}
                      xml={this.state.Suit_Diamond}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('H')}
                  disabled={this.state.EnableTrump_Heart}
                >
                  <View style={{ ...this.state.Suit_Heart_bg, width: BidWidth / 8 }}>
                    <SvgXml
                      width={scale(13)}
                      height={scale(13)}
                      xml={this.state.Suit_Heart}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('S')}
                  disabled={this.state.EnableTrump_Spade}
                >
                  <View style={{ ...this.state.Suit_Spade_bg, width: BidWidth / 8 }}>
                    <SvgXml
                      width={scale(13)}
                      height={scale(13)}
                      xml={this.state.Suit_Spade}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.SelectedTrump('NT')}
                  disabled={this.state.EnableTrump_NT}
                >
                  <View
                    style={{
                      ...this.state.NT_bg,
                      width: BidWidth / 8
                    }}
                  >
                    <SvgXml
                      width={scale(17)}
                      height={scale(17)}
                      xml={this.state.Suit_NT}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.makeBid()}
                  disabled={this.state.EnableBid}
                >
                  <View style={{ ...this.state.Make_Bid, width: BidWidth / 8 }}>
                    <SvgXml
                      width={scale(16.25)}
                      height={scale(16.08)}
                      xml={this.state.Make_Bid_img}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
           ) : null} 
          {/* BIDDING END */}
        <View
          style={{
            flexDirection: 'column',
            width: '100%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            top: moderateVerticalScale(1),
          }}
        >
          {
            (this.state.iskibitzer && !this.state.northBidInfo) &&
              <TouchableOpacity
                onPress={() => this.toggleBidInfoSection("N")}
                style={{
                  height: scale(45),
                  justifyContent: 'space-evenly',
                  alignSelf: 'center',
                }}
              >
                <View style={{flexDirection: 'row',}}>
                  {this.drawCards(this.state.northHands, 'N')}
                </View>
            </TouchableOpacity>
          }
          {
            (!this.state.iskibitzer || this.state.northBidInfo) &&
            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center'
                }}
              >
                <View
                  style={{
                    width: scale(90), height: scale(45), backgroundColor: '#151515', alignItems: 'center',
                    justifyContent: 'center',
                    borderRightWidth: moderateVerticalScale(1)
                  }}
                >
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.northSharedType1}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {this.state.infoScreen.northSharedValue1}
                  </Text>
                </View>
                <View
                  style={{
                    width: scale(90), height: scale(45),
                    justifyContent: 'center',
                    backgroundColor: '#151515',
                  }}
                >
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.northSharedType2}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {this.state.infoScreen.northSharedValue2}
                  </Text>
                </View>
              </View>
                {
                  !this.state.northBidInfo ?
                    this.state.gamestate.myPartnername.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username, this.props.hostName) && this.getChatMessage(this.state.myPartnerSeat)
                    ?
                    <AddPlayer
                    isNorthInvited={this.props.isNorthInvited}
                    direction={Strings.directionNorth}
                    northUserName={this.props.northUserName}
                    directionNorth={this.props.directionNorth}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    cancelRequest={this.props.cancelRequest}
                    tableId={this.props.tableID} />
                   :
                   <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myPartnerSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                    >
                      <View style={{
                        height: scale(23), 
                        width: scale(180),
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf:'center'
                        }}>
                          <CrownHost
                            hostName={this.props.hostName}
                            shareBidPlay={false}
                            rightSide={false}
                            textStyle={{ ...styles.UserNameText, 
                              color: this.state.infoScreen.northUserText, 
                            }}
                            userName={this.state.gamestate.myPartnername}
                            reduceChar={this.getChatMessage(this.state.myPartnerSeat)}
                          />
                        </View>
                      </TouchableOpacity>
                    :null
                }
            </View>
          }
          {
            this.state.iskibitzer &&
            <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myPartnerSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
            >
              <View style={{ height: scale(23), width: scale(180), justifyContent: 'center',alignItems:'center',alignSelf:'center', flexDirection: 'row' }}>
                <CrownHost
                  hostName={this.props.hostName}
                  shareBidPlay={false}
                  rightSide={false}
                  textStyle={{ ...styles.UserNameText, color: this.state.infoScreen.northUserText, alignSelf: 'center' }}
                  userName={this.state.gamestate.myPartnername}
                  reduceChar={this.getChatMessage(this.state.myPartnerSeat)}
                />
              </View>
            </TouchableOpacity>
          }
          <View style={{...styles.bidCirclePositionArea,}}>
            <View
              style={
                {...this.state.bidInfoScreen.my_partner_bid_suit_border_color,  width: scale(28), height: scale(28)}
              }
            >
              <Text
                style={
                  {...this.state.bidInfoScreen.my_partner_bid_suit_text_color}
                }
              >
                {this.state.bidInfoScreen.my_partner_bid_level}
              </Text>
              {console.log("-- partner side suit val --- "+ this.state.bidInfoScreen.my_partner_bid_suit)}
              {this.state.bidInfoScreen.my_partner_bid_type === 'level' ? (
                <SvgXml
                  //style={this.state.bidInfoScreen.my_partner_bid_suit_color}
                  width={scale(10)}
                  height={scale(10)}
                  xml={this.state.bidInfoScreen.my_partner_bid_suit_image}
                />
              ) : null}
            </View>
          </View>
        </View>

        {/* <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: '100%', 
          backgroundColor: 'blue'}}> */}
          <View
            style={{
              height: '100%',
              left: moderateScale(2),
              justifyContent: 'center',
              position: 'absolute',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center',
              }}
            >
              {
                (this.state.iskibitzer && !this.state.westBidInfo) &&
                <View style={{
                  height: '100%'
                }}>
                  <TouchableOpacity
                    onPress={() => this.toggleBidInfoSection('W')}
                    style={{
                      width: scale(45),
                      height: '100%',
                      justifyContent: 'space-evenly',
                    }}
                  >
                    <View
                      style={{
                        transform: [{ rotate: '-90deg' }],
                        flexDirection: 'row',
                        alignSelf: 'center'
                      }}>
                      {this.drawCards(this.state.westHands, 'W')}
                    </View>
                  </TouchableOpacity>
                </View>
              }
              {
                (!this.state.iskibitzer || this.state.westBidInfo) &&
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: moderateScale(1) }}
                  >
                    <View style={{
                      height: scale(90),
                      width: scale(45),
                      alignItems: 'center',
                      transform: [{ rotate: '-90deg' }],
                      justifyContent: 'center',
                    }}>

                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(45), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(71), height: scale(21) }}>
                        {this.state.infoScreen.westSharedType1}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } :
                        { ...styles.PaternText, width: scale(71), height: scale(23) }}>
                        {this.state.infoScreen.westSharedValue1}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{ width: scale(45), height: scale(90), backgroundColor: '#151515' }}
                  >
                    <View style={{
                      height: scale(90),
                      width: scale(45),
                      alignItems: 'center',
                      transform: [{ rotate: '-90deg' }],
                      justifyContent: 'center',
                    }}>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(71), height: verticalScale(21) } :
                        { ...styles.shareHandText, width: scale(71), height: scale(21) }}>
                        {this.state.infoScreen.westSharedType2}
                      </Text>
                      <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(71), height: verticalScale(23) } :
                        { ...styles.PaternText, width: scale(71), height: scale(23), }}>
                        {this.state.infoScreen.westSharedValue2}
                      </Text>
                    </View>
                  </View>
                </View>

              }   
              
              {!this.state.iskibitzer ?
              this.state.gamestate.myLHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username, this.props.hostName) && this.getChatMessage(this.state.myLHOSeat)
              ?
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <AddPlayer
                  isWestInvited={this.props.isWestInvited}
                  direction={Strings.directionWest}
                  directionWest={this.props.directionWest}
                  westUserName={this.props.westUserName}
                  onAddPlayerClick={this.props.onAddPlayerClick}
                  cancelRequest={this.props.cancelRequest}
                  tableId={this.props.tableID} />
              </View> 
              :
              <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myLHOSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
              >
                <View style={{
                  height: scale(180), width: scale(23),
                  alignItems: 'center', justifyContent: 'center',
                  alignSelf:'center'
                }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={true}
                    rightSide={false}
                    textStyle={{
                      ...styles.UserNameText, color: this.state.infoScreen.westUserText,
                    }}
                    crownStyle={{ alignSelf:'center'}}
                    userName={this.state.gamestate.myLHOname}
                    reduceChar={this.getChatMessage(this.state.myLHOSeat)}
                  />
                </View>
              </TouchableOpacity>
              : null
              }
              {this.state.iskibitzer &&
              <TouchableOpacity
                disabled={this.getChatMessage(this.state.myLHOSeat)}
                onPress={() => {
                  this.props.showTableMessages()
                }}
              >
                <View style={{
                  height: scale(180), width: scale(23),
                  alignItems: 'center', justifyContent: 'center',alignSelf:'center'
                }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={true}
                    rightSide={false}
                    textStyle={{
                      ...styles.UserNameText, color: this.state.infoScreen.westUserText,
                    }}
                    crownStyle={{ alignSelf:'center' }}
                    userName={this.state.gamestate.myLHOname}
                    reduceChar={this.getChatMessage(this.state.myLHOSeat)}
                  />
                </View>
              </TouchableOpacity>}

                <View style={(Platform.OS == 'ios' && DeviceInfo.isTablet()) ? {
                  ...styles.bidCirclePositionArea, height: scale(180), width: verticalScale(23),
                  alignItems: 'center', justifyContent: 'center', marginLeft: moderateScale(10)
                } : (Platform.OS == 'android' && DeviceInfo.isTablet()) ? {
                  ...styles.bidCirclePositionArea, height: scale(180), width: verticalScale(23),
                  alignItems: 'center', justifyContent: 'center', marginLeft: moderateScale(5)
                } : {
                  ...styles.bidCirclePositionArea, height: scale(180),
                  alignItems: 'center', justifyContent: 'center', left: moderateScale(2),
                }}>
                  <View
                    style={
                      { ...this.state.bidInfoScreen.my_LHO_bid_suit_border_color, width: scale(28), height: scale(28), transform: [{ rotate: '-90deg' }] }}>
                    <Text
                      style={{ ...this.state.bidInfoScreen.my_LHO_bid_suit_text_color }}
                    >
                      {this.state.bidInfoScreen.my_LHO_bid_level}
                    </Text>
                    {console.log("-- LHO side suit val --- "+ this.state.bidInfoScreen.my_LHO_bid_suit +" , type level  "+this.state.bidInfoScreen.my_LHO_bid_type)}
                    {this.state.bidInfoScreen.my_LHO_bid_type === 'level' ? (
                      <SvgXml
                      //style={this.state.bidInfoScreen.my_LHO_bid_suit_color}
                      width={scale(10)}
                      height={scale(10)}
                      xml={this.state.bidInfoScreen.my_LHO_bid_suit_image}
                    />
                    ) : null}
                  </View>
                </View>
            </View>
          </View>
          
          <View
            style={{
              height: '100%',
              right: scale(1),
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center',
              }}
            >
              <View style={{
                ...styles.bidCirclePositionArea, height: scale(180), width: scale(23),
                  alignItems: 'center', justifyContent: 'center',alignSelf: 'center',
                  right: moderateScale(2)
              }}>
                <View
                  style={
                    {...this.state.bidInfoScreen.my_RHO_bid_suit_border_color,  width: scale(28), height: scale(28), transform: [{ rotate: '90deg' }]}}>
                  <Text
                  
                  style={{ ...this.state.bidInfoScreen.my_RHO_bid_suit_text_color }}
                  >
                    {this.state.bidInfoScreen.my_RHO_bid_level}
                  </Text>
                  {this.state.bidInfoScreen.my_RHO_bid_type === 'level' ? (
                  <SvgXml
                  //style={this.state.bidInfoScreen.my_RHO_bid_suit_color}
                  width={scale(10)}
                  height={scale(10)}
                  xml={this.state.bidInfoScreen.my_RHO_bid_suit_image}
                />
                  ) : null}
                </View>
              </View>
              {
                this.state.iskibitzer &&
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myRHOSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    height: scale(180), width: scale(23),
                    alignItems: 'center', justifyContent: 'center',alignSelf:'center'
                  }}>
                    <CrownHost
                        hostName={this.props.hostName}
                        shareBidPlay={true}
                        rightSide={true}
                        textStyle={{
                          ...styles.UserNameText, color: this.state.infoScreen.westUserText, letterSpacing: 0.7,
                        }}
                        crownStyle={{
                          alignSelf:'center'
                        }}
                        userName={this.state.gamestate.myRHOname}
                        reduceChar={this.getChatMessage(this.state.myRHOSeat)}
                      />
                  </View>
                </TouchableOpacity>
              }
              {
                !this.state.iskibitzer ?
                this.state.gamestate.myRHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName) && this.getChatMessage(this.state.myRHOSeat)
                ?
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <AddPlayer
                    isEastInvited={this.props.isEastInvited}
                    direction={Strings.directionEast}
                    directionEast={this.props.directionEast}
                    eastUserName={this.props.eastUserName}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    cancelRequest={this.props.cancelRequest}
                    tableId={this.props.tableID} />
                </View>:
                <TouchableOpacity
                  disabled={this.getChatMessage(this.state.myRHOSeat)}
                  onPress={() => {
                    this.props.showTableMessages()
                  }}
                >
                  <View style={{
                    height: scale(180), width: scale(23),
                    alignItems: 'center', justifyContent: 'center',alignSelf:'center'
                  }}>
                    <CrownHost
                        hostName={this.props.hostName}
                        shareBidPlay={true}
                        rightSide={true}
                        textStyle={{
                          ...styles.UserNameText, color: this.state.infoScreen.westUserText, letterSpacing: 0.7,
                        }}
                        crownStyle={{
                          alignSelf:'center'
                        }}
                        userName={this.state.gamestate.myRHOname}
                        reduceChar={this.getChatMessage(this.state.myRHOSeat)}
                      />
                  </View>
                </TouchableOpacity>
                : null
              }
              { (!this.state.iskibitzer || this.state.eastBidInfo) &&
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  alignSelf: 'center'
                }}
              >
                <View
                  style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: 1 }}
                >
                  <View style={{
                    height: scale(90),
                    width: scale(45),
                    alignItems: 'center',
                    transform: [{ rotate: '90deg' }],
                    justifyContent: 'center',
                  }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(71), height: scale(21) }}>
                      {this.state.infoScreen.eastSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(71), height: verticalScale(23) } :
                      { ...styles.PaternText, width: scale(71), height: scale(23), }}>
                      {this.state.infoScreen.eastSharedValue1}
                    </Text>
                  </View>
                </View>
                <View
                  style={{ width: scale(45), height: scale(90), backgroundColor: '#151515' }}
                >
                  <View style={{
                    height: scale(90), transform: [{ rotate: '90deg' }],
                    alignItems: 'center',
                    transform: [{ rotate: '90deg' }],
                    justifyContent: 'center',
                  }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(71), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(71), height: scale(21) }}>
                      {this.state.infoScreen.eastSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {this.state.infoScreen.eastSharedValue2}
                    </Text>
                  </View>
                </View>
              </View>
              }
              {
                (this.state.iskibitzer && !this.state.eastBidInfo) &&
                <View style={{
                  height: '100%'
                }}>
                  <TouchableOpacity
                    onPress={() => this.toggleBidInfoSection('E')}
                    style={{
                      width: scale(45),
                      height: '100%',
                      justifyContent: 'space-evenly',
                    }}
                  >
                    <View
                      style={{
                        transform: [{ rotate: '90deg' }],
                        flexDirection: 'row',
                        alignSelf: 'center'
                      }}>
                      {this.drawCards(this.state.eastHands, 'E')}
                    </View>
                  </TouchableOpacity>
                </View>
              }
            </View>
          </View>
         
        {/* </View> */}
        <View style={{ width: '100%', alignItems: 'center',bottom: 0, position: 'absolute',}}>
          <View style={{
            ...styles.bidCirclePositionArea,
          }}>
            <View
              style={{ ...this.state.bidInfoScreen.my_bid_suit_border_color, width: scale(28), height: scale(28) }}>
              <Text style={{ ...this.state.bidInfoScreen.my_bid_suit_text_color }}>
                {this.state.bidInfoScreen.my_bid_level}
              </Text>
              {
                this.state.bidInfoScreen.my_bid_level !== 'P' &&
                this.state.bidInfoScreen.my_bid_level !== 'X' &&
                this.state.bidInfoScreen.my_bid_level !== 'XX' && (
                  <SvgXml
                    width={scale(10)}
                    height={scale(10)}
                    xml={
                      (this.state.bidInfoScreen.bid_suit === 'S') ? svgImages.spade 
                      : (this.state.bidInfoScreen.bid_suit === 'H') ? svgImages.hearts
                      : (this.state.bidInfoScreen.bid_suit === 'C') ? svgImages.clubs
                      : (this.state.bidInfoScreen.bid_suit === 'D') ? svgImages.diamond
                      : (this.state.bidInfoScreen.bid_suit === 'NT') ? svgImages.ntActive : " "
                    }
                  />
                )
               }
            </View>
          </View>
          {
            this.state.iskibitzer &&
            <TouchableOpacity
                    disabled={this.getChatMessage(this.state.mySeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
            >
              <View style={{flexDirection: 'row',width:scale(180), height: scale(23) ,alignItems:'center',justifyContent:'center',alignSelf:'center'}}>
                <CrownHost
                  hostName={this.props.hostName}
                  shareBidPlay={false}
                  rightSide={false}
                  textStyle={this.getChatMessage(this.state.mySeat) ? {
                    ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,
                    alignSelf: 'center',
                  }: {...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,
                    alignSelf: 'center',width:scale(180)}}
                  crownStyle={{
                    alignSelf: 'center',
                    left: moderateScale(-15),
                    alignContent: 'center',
                    bottom: moderateScale(5),
                    position: 'absolute'
                  }}
                  userName={this.state.gamestate.myUsername}
                  reduceChar={this.getChatMessage(this.state.mySeat)}
                />
             </View>
            </TouchableOpacity>
          }
          
          {
            !this.state.iskibitzer ?
            this.state.gamestate.myUsername.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName) && this.getChatMessage(this.state.mySeat)
                ?
            loadTextOrAddOption(Strings.directionSouth) :
            <TouchableOpacity
              disabled={this.getChatMessage(this.state.mySeat)}
              onPress={() => {
                  this.props.showTableMessages()
              }}
            >
              <View style={{ 
                  alignItems: 'center', 
                  justifyContent:'center', 
                  height: scale(23),      
                  width: scale(180),
                  alignSelf:'center',
                  flexDirection: 'row'
                  }}>
                <CrownHost
                  hostName={this.props.hostName}
                  shareBidPlay={false}
                  rightSide={false}
                  textStyle={{
                    ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,
                    alignSelf: 'center', 
                  }}
                  crownStyle={{alignSelf: 'center',
                  left: moderateScale(-15),
                  alignContent: 'center',
                  bottom: moderateScale(8),
                  position: 'absolute'}}
                  userName={this.state.gamestate.myUsername}
                  reduceChar={this.getChatMessage(this.state.mySeat)}
                />
              </View>
            </TouchableOpacity>
            : null
          }
          
          {this.state.iskibitzer ? (
            this.state.southBidInfo ? (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <View
                style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', borderRightWidth: 1, justifyContent: 'center' }}
              >
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21)} : 
                    { ...styles.shareHandText, width: scale(90), height: scale(21)}}>
                  {this.state.infoScreen.southSharedType1}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                  {formatSharedValue(this.state.infoScreen.southSharedValue1)}
                </Text>
              </View>
              <View
                style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', justifyContent: 'center'  }}
              >
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21)} : 
                    { ...styles.shareHandText, width: scale(90), height: scale(21)}}>
                  {this.state.infoScreen.southSharedType2}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                  {formatSharedValue(this.state.infoScreen.southSharedValue2)}
                </Text>
              </View>
            </View>
            ):(
                <TouchableOpacity
                  onPress={() => this.toggleBidInfoSection("S")}
                  >
                  <View
                    style={{
                      height: scale(45),
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      alignSelf: 'center',
                    }}
                  >
                    {this.drawCards(this.state.southHands, 'S')}
                  </View>
                </TouchableOpacity>
            )
          ) : (
            <View
              style={{
                height: scale(45),
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignSelf: 'center',
              }}
            >
                {this.drawCards(this.state.hands, 'NT')}
            </View>
          )}
        </View>
      </View>
    ) : null;
  }
}

const styles = ScaledSheet.create({
  ContractText: {
    position: 'absolute',
    top: 8,
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535'
  },
  ContractTextActive: {
    position: 'absolute',
    top: 8,
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D'
  },
  BidText: {
    top: '3@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    fontWeight: 'normal',
    color: '#353535'
  },
  BidTextActive: {
    top: '3@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D',
  },

  bidCirclePositionArea: {
    width: scale(28),
    height: scale(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  spadeImageActive: {
    width: scale(8),
    height: scale(8),
    marginLeft: 2
  },
  heartsImageActive: {
    width: scale(8),
    height: scale(8),
    marginLeft: 2
  },
  diamondImageActive: {
    width: scale(8),
    height: scale(8),
    marginLeft: 2
  },
  clubsImageActive: {
    width: scale(8),
    height: scale(8),
    marginLeft: 2
  },
  bidCircleTextAssActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#B9B9B9'
  },
  bidCircleTextAss: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#676767'
  },
  bidCircleTextHeartsActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FF0C3E'
  },
  bidCircleTextClubsActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#79E62B'
  },
  bidCircleTextDiamondActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#04AEFF'
  },
  bidCircleTextSpadeActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#C000FF'
  },
  bidCircleTextNTActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '200',
    color: '#FFE81D'
  },
  bidCirclePositionNTActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#0a0a0a',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FFE81D',
    flexDirection: 'row'
  },
  bidCirclePositionSpadeActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#0a0a0a',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#C000FF',
    flexDirection: 'row'
  },
  bidCirclePositionHeartsActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#0a0a0a',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#FF0C3E',
    flexDirection: 'row'
  },
  bidCirclePositionClubsActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#0a0a0a',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#79E62B',
    flexDirection: 'row'
  },
  bidCirclePositionDiamondActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#0a0a0a',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#04AEFF',
    flexDirection: 'row'
  },
  bidCirclePositionBodrAssActive: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#151515',
    backgroundColor: 'blue',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#B9B9B9',
    flexDirection: 'row'
  },
  bidCirclePositionBodrAss: {
    width: scale(28),
    height: verticalScale(28),
    backgroundColor: '#151515',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black', //#676767
    flexDirection: 'row'
  },

  bidBoxPosition: {
    flexDirection: 'column',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  bidBoxPositionDirectionArea: {
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 1
  },
  bidBoxWidthHeightArea: {
    width: scale(43),
    height: scale(43),
    backgroundColor: '#151515',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  centerItems: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  bidBoxWidthHeightAreaBodrActive: {
    width: scale(43),
    height: scale(43),
    backgroundColor: '#151515',
    borderWidth: 1,
    borderColor: '#DBFF00',
    alignItems: 'center',
    justifyContent: 'center'
  },

  bidBoxRorPosition: {
    flexDirection: 'row',
  },
  bidBoxWidthHeightAreaActive: {
    width: '43@s',
    height: '43@s',
    backgroundColor: '#1C1C1C',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bidBoxSelectedTrickTextbg: {
    width: '43@s',
    height: '43@s',
    backgroundColor: '#1C1C1C',
    borderBottomWidth: 1,
    borderBottomColor: '#0a0a0a',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    alignItems: 'center'
  },
  bidBoxSelectedTrickText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#DBFF00'
  },
  bidBoxWidthHeightTextClorActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#DBFF00'
  },
  bidBoxWidthHeightTextClorNTActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#FFE81D'
  },
  bidBoxWidthHeightTextClor: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: '#676767'
  },
  bidBoxDisabledLevel: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },
  bidDblTextClordisabled: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },
  bidRedblTextClordisabled: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '200',
    color: 'black'
  },

  /*************************************** */

  SharePatternCardSpade: {
    flexDirection: 'column',
    width: verticalScale(55),
    height: scale(81),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#C000FF',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardHearts: {
    flexDirection: 'column',
    width: verticalScale(55),
    height: scale(81),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#FF0C3E',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardDiamond: {
    flexDirection: 'column',
    width: verticalScale(55),
    height: scale(81),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#04AEFF',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  SharePatternCardClubs: {
    flexDirection: 'column',
    width: verticalScale(55),
    height: scale(81),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#79E62B',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  PatternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#FFFFFF'
  },
  SharePatternCard: {
    flexDirection: 'column',
    width: verticalScale(55),
    height: scale(81),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#DBFF00',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#151515'
  },
  cardDisplayBody: {
    width: scale(45),
    height: scale(45),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    paddingLeft: 8,
    paddingTop: 3,
    backgroundColor: '#151515'
  },
  cardPosition: {
    width: '22@ms',
    alignItems: 'center',
    justifyContent: 'center'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    color: '#C000FF'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  bidLoserColor:{
    color: "#FFF",
    fontFamily: "Roboto-Light",
    fontSize: 11,
    fontWeight: "200",
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center',
    letterSpacing: normalize(1.92)
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },

  UserNameText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
  },
  LHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  bidBorderLoserColor: {
    alignItems: "center",
    backgroundColor: "#0a0a0a",
    borderColor: "#FFF",
    borderRadius: 51.04166666666667,
    borderWidth: 1,
    flexDirection: "row",
    height: '35.525@s',
    justifyContent: "center",
    width: '29.166666666666668@s'
  },
  RHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  Partner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: 4
  },
  DirectionTextMySeat: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextLHO: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextRHO: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextPartner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  }
});
