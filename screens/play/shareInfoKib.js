import React,{Component} from 'react';
import {AppRegistry, View, Text, StyleSheet, Image, Dimensions,TouchableOpacity} from 'react-native';
import { normalize } from '../../styles/global';
import { HOOL_CLIENT } from '../../shared/util';
import { HOOL_EVENTS } from '../../constants/hoolEvents';
//import global from './shared/global'; 

 
var ScreenWidth=  Dimensions.get("window").width;
var ScreenHeight=  Dimensions.get("window").height;//-30;

var HeightBoxTop=(225/360*ScreenHeight);
var MenuBodyWidth=(91/360*ScreenHeight);
var ShareHehightWidth=(90/360*ScreenHeight);
var ContractTextHeight=(22/360*ScreenHeight);
var ContractDwnBoxHeight=(23/360*ScreenHeight);
var ContractDwnMdlBoxWidth=(44/360*ScreenHeight);
var TotalCardWidth=(532/360*ScreenHeight);
var NorthCardWithKibitzerArea=(532/360*ScreenHeight);

var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
var HeightWidthBoxes=(45/360*ScreenHeight);
var HeightEastWestPalyCard=(360/360*ScreenHeight);
var EastWestPalyCardArea=(304/360*ScreenHeight); 
    var SampleNameArray = [ "K", "Q", "3", "K", "8", "4", "A", "9", "7", "6", "Q", "J", "7" ];
    var hands = [
        {"rank": "K","img": require('../../assets/images/spade.png'),"suit":"S","key":"KS","SuiteColor":"#C000FF","idx":0},
        {"rank": "Q","img": require('../../assets/images/spade.png'),"suit":"S","key":"QS","SuiteColor":"#C000FF","idx":1},
        {"rank": "10","img": require('../../assets/images/spade.png'),"suit":"S","key":"10S","SuiteColor":"#C000FF","idx":2},
        {"rank": "K","img": require('../../assets/images/hearts.png'),"suit":"H","key":"KH","SuiteColor":"#FF0C3E","idx":3},
        {"rank": "8","img": require('../../assets/images/hearts.png'),"suit":"H","key":"8H","SuiteColor":"#FF0C3E","idx":4},
        {"rank": "4","img": require('../../assets/images/hearts.png'),"suit":"H","key":"4H","SuiteColor":"#FF0C3E","idx":5},
        {"rank": "A","img": require('../../assets/images/diamond.png'),"suit":"D","key":"AD","SuiteColor":"#04AEFF","idx":6},
        {"rank": "9","img": require('../../assets/images/diamond.png'),"suit":"D","key":"9D","SuiteColor":"#04AEFF","idx":7},
        {"rank": "7","img": require('../../assets/images/diamond.png'),"suit":"D","key":"7D","SuiteColor":"#04AEFF","idx":8},
        {"rank": "6","img": require('../../assets/images/diamond.png'),"suit":"D","key":"6D","SuiteColor":"#04AEFF","idx":9},
        {"rank": "Q","img": require('../../assets/images/clubs.png'),"suit":"C","key":"QC","SuiteColor":"#79E62B","idx":10},
        {"rank": "J","img": require('../../assets/images/clubs.png'),"suit":"C","key":"JC","SuiteColor":"#79E62B","idx":11},
        {"rank": "7","img": require('../../assets/images/clubs.png'),"suit":"C","key":"7C","SuiteColor":"#79E62B","idx":12}
      ]
    var eastHands=hands;
    var westHands=hands;
    var northHands=hands;
    var southHands=[];//hands;
      
    
      export default class shareInfoKib extends React.Component {
        constructor(props) {
            ///navigation.na
          super(props);
           this.state = { 
               dummycardsdrawn:false,    
               disablemyCards_C:true,
                disablemyCards_D:true,
                disablemyCards_H:true,
                disablemyCards_S:true,
                disabledummyCards_C:true,
                disabledummyCards_D:true,
                disabledummyCards_H:true,
                disabledummyCards_S:true,
                Numof_C:0,
               Numberof_D:0,
               Numberof_H:0,
               Numof_S:0,
                trick_Suit:'',
                output: '',
                tableID:'',
                mySeat: 'S',
                myLHOSeat: 'W', 
                myRHOSeat: 'E',
                myPartnerSeat: 'N',
                DummySeat:'',
                DummyPlayed:false, 
                directionPointer: require('../../assets/images/center_circle.png'),
                northUser: 'North',
                eastUser: '',
                southUser: '',
                westUser: '', 
                northSharedType1: '',
                northSharedValue1 :'',
                northSharedType2: '',
                northSharedValue2 :'',
                eastSharedType1: '',
                eastSharedValue1 :'',
                eastSharedType2: '',
                eastSharedValue2 :'',
                southSharedType1: '',
                southSharedValue1 :'',
                southSharedType2: '',
                southSharedValue2 :'',
                westSharedType1: '',
                westSharedValue1 :'',
                westSharedType2: '',
                westSharedValue2 :'', 
                cardHCPTextColor :'#FFFFFF',
                cardPatternTextColor :'#FFFFFF',
                cardSuitTextColor :'#FFFFFF',
                DirectionTextPartner:styles.DirectionText,
                DirectionTextLHO:styles.DirectionText,
                DirectionTextRHO:styles.DirectionText,
                DirectionTextMySeat:styles.DirectionText,
                northUserText: '#6D6D6D',
                eastUserText: '#6D6D6D',
                southUserText: '#6D6D6D',
                westUserText: '#6D6D6D',
                winnerSide:'',
                winnerLevel:'',
                winnerSuit:'',
                winnerSuitImage:'',
                winnerTextColor:'',
                winnerdblRedbl:'', 
                PartnerPlayed:false,
                LHOPlayed:false,
                RHOPlayed:false,
                MyCard:false,
                LHORank:'',
                RHORank:'',
                PartnerRank:'',
                myCardStyle:'',
                myCardNumberstyle:'',
                myRank:'',
                mySuit:'', 
                NSStyle:styles.ContractText,
                NSSharingStyle: styles.ShareText,
                EWStyle:styles.ContractText,
                EWSharingStyle: styles.ShareText,             
            }; 
          hands=[];
          
        }
 
           getTableInfo() {
            const { navigation } = this.props; 
            const gamestate = navigation.getParam('gameState'); 
             console.log('gamestate');
            // console.log(gamestate);
            let myHand = gamestate.hands;
            //console.log(myHand); 
            myHand.forEach((h,key) => { 
                if(key==='S'){
                    southHands= this.displayCards(h.cards);
                   
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({southSharedType1:suitType,southSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({southSharedType2:suitType,southSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({southSharedType1:suitType,southSharedValue1:h.infoShared[0].value});
                    }
                }
                else if(key==='W'){
                    westHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({westSharedType1:suitType,westSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({westSharedType2:suitType,westSharedValue2:h.infoShared[1].value}) ;
                        
                        const interval = setInterval(() => {
                            console.log(this.state.northSharedType2 +' ,'+  this.state.eastSharedType2 + ', ' + this.state.southSharedType2 +', '+  this.state.westSharedType2);
                            this.checkBidding();  
                            clearInterval(interval);
                         }, 1000); 
                       
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({westSharedType1:suitType,westSharedValue1:h.infoShared[0].value});
                    }

                }
                else if(key==='N'){
                    northHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({northSharedType1:suitType,northSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({northSharedType2:suitType,northSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({northSharedType1:suitType,northSharedValue1:h.infoShared[0].value});
                    }
                }
                else if(key==='E'){
                    eastHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({eastSharedType1:suitType,eastSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({eastSharedType2:suitType,eastSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({eastSharedType1:suitType,eastSharedValue1:h.infoShared[0].value});
                    }
                }
            });   
            this.setState({southUser:gamestate.southUser,westUser:gamestate.westUser,northUser:gamestate.northUser,eastUser:gamestate.eastUser})
            this.setState({DirectionTextPartner:styles.DirectionTextActive,northUserText:'#DBFF00'});
           
          }
        
        
      componentDidMount() {
        console.log('componentDidMount');
        this.getTableInfo();
        this.checkBidding();
            HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_JOIN_ERROR, (e) => {
                Alert.alert(`Failed to join table: ${e.tag || 'cancel'} (${suitType || 'unknown-error'}): ${e.text || 'unknown error'}`)
              });
        
              HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_LEAVE, (e) => {
                console.info(`${e.user} has (temporarily) disconnected from ${e.table}`)
              });
        
              HOOL_CLIENT.addListener(HOOL_EVENTS.TABLE_EXIT, (e) => {
                console.info(`${e.user} has (permanently) left ${e.table}`)
              });
        
                HOOL_CLIENT.addListener(HOOL_EVENTS.INFO_SHARE, (e) => {
                    console.info(`${e.side} has shared ${e.type} info: ${e.value}`)
                    // re-sort pattern
                    if (e.type === 'pattern') {
                        let pat = e.value.split(',').sort().reverse();
            
                        // make sure it has four values
                        while (pat.length < 4) {
                        pat.push('0')
                        }
            
                        // put them together
                        e.value = pat.join(',')
                    }
          
                    let s = gamestate.info.get(e.side)
                    if (!s) s = []
                    s.push({type: e.type, value: e.value})
                    gamestate.info.set(e.side, s)  
                    var suitType=e.type;
                    if(suitType==='D')
                    {
                        suitType ='Diamonds';
                    }
                    else if(suitType==='C')
                    {
                        suitType ='Clubs';
                    }
                    else if(suitType==='H')
                    {
                        suitType ='Hearts';
                    }
                    else if(suitType==='S')
                    {
                        suitType ='Spades';
                    } 
                       
                    this.setState({DirectionTextRHO:styles.DirectionText,eastUserText:'#6D6D6D'});
                    this.setState({DirectionTextMySeat:styles.DirectionText,southUserText:'#6D6D6D'});
                    this.setState({DirectionTextPartner:styles.DirectionText,northUserText:'#6D6D6D'});
                    this.setState({DirectionTextLHO:styles.DirectionText,westUserText:'#6D6D6D'}); //DBFF00
                    if(e.side==="N")
                    {
                        if(this.state.northSharedType1===''){
                            this.setState({northSharedType1:suitType,northSharedValue1:e.value});
                        }else if(this.state.northSharedType2===''){
                            this.setState({northSharedType2:suitType,northSharedValue2:e.value});
                        }

                        if(this.state.eastSharedType2===''){
                            this.setState({DirectionTextRHO:styles.DirectionTextActive,eastUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_e.png'),});
                        }
                    }else if(e.side==="E")
                    {
                        if(this.state.eastSharedType1===''){
                            this.setState({eastSharedType1:suitType,eastSharedValue1:e.value});
                        }else if(this.state.eastSharedType2===''){
                            this.setState({eastSharedType2:suitType,eastSharedValue2:e.value});
                        }

                        if(this.state.southSharedType2===''){
                            this.setState({DirectionTextMySeat:styles.DirectionTextActive,southUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_s.png'),});
                        }
                    }else if(e.side==="S")
                    {
                        if(this.state.southSharedType1 ===''){
                            this.setState({southSharedType1:suitType,southSharedValue1 :e.value});
                        }else if(this.state.southSharedType2===''){
                            this.setState({southSharedType2:suitType,southSharedValue2:e.value});
                        }
                        if(this.state.westSharedType2===''){
                            this.setState({DirectionTextLHO:styles.DirectionTextActive,westUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_w.png'),});
                        }
                    }else if(e.side==="W")
                    {
                        if(this.state.westSharedType1===''){
                            this.setState({westSharedType1:suitType,westSharedValue1:e.value});
                        }else if(this.state.westSharedType2===''){
                            this.setState({westSharedType2:suitType,westSharedValue2:e.value});
                            this.checkBidding();
                        }
                        
                        if(this.state.northSharedType2===''){
                            this.setState({DirectionTextPartner:styles.DirectionTextActive,northUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_n.png'),});
                        }
                    }

                    if(e.side==='N' || e.side==='S'){
                        this.setState({NSStyle:styles.ContractText,NSSharingStyle: styles.ShareText,
                            EWStyle:styles.ContractTextActive,EWSharingStyle: styles.ShareTextActive,});
                    }
                    else if(e.side==='E' || e.side==='W'){
                        this.setState({NSStyle:styles.ContractTextActive,NSSharingStyle: styles.ShareTextActive,
                            EWStyle:styles.ContractText,EWSharingStyle: styles.ShareText,});
                    } 
                    
                    
                 });  
 
        }  

        componentWillUnmount()
        { 
            this.setState({
                northSharedType1: '',
                northSharedValue1 :'',
                northSharedType2: '',
                northSharedValue2 :'',
                eastSharedType1: '',
                eastSharedValue1 :'',
                eastSharedType2: '',
                eastSharedValue2 :'',
                southSharedType1: '',
                southSharedValue1 :'',
                southSharedType2: '',
                southSharedValue2 :'',
                westSharedType1: '',
                westSharedValue1 :'',
                westSharedType2: '',
                westSharedValue2 :'', 
            });
        }

        checkBidding()
       {
    //     console.log('north : ' + this.state.northSharedType2);
    //     console.log('east : ' +  this.state.eastSharedType2);
    //     console.log('south : ' +  this.state.southSharedType2 );
    //     console.log('west : ' +  this.state.westSharedType2);
       if(this.state.northSharedType2 !=='' &&  this.state.eastSharedType2 !=='' && this.state.southSharedType2 !=='' &&  this.state.westSharedType2 !=='')
          {
            console.log('go to bidding screen');
            const { navigation } = this.props;
            const hool = navigation.getParam('xmppconn');
            const gamestate = navigation.getParam('gameState'); 
            const tableID = navigation.getParam('tableID');
            gamestate.northSharedType1=this.state.northSharedType1;
            gamestate.northSharedValue1=this.state.northSharedValue1;
            gamestate.northSharedType2=this.state.northSharedType2;
            gamestate.northSharedValue2=this.state.northSharedValue2;
            gamestate.eastSharedType1=this.state.eastSharedType1;
            gamestate.eastSharedValue1=this.state.eastSharedValue1;
            gamestate.eastSharedType2=this.state.eastSharedType2;
            gamestate.eastSharedValue2=this.state.eastSharedValue2;
            gamestate.southSharedType1=this.state.southSharedType1;
            gamestate.southSharedValue1=this.state.southSharedValue1;
            gamestate.southSharedType2=this.state.southSharedType2;
            gamestate.southSharedValue2=this.state.southSharedValue2;
            gamestate.westSharedType1=this.state.westSharedType1;
            gamestate.westSharedValue1=this.state.westSharedValue1;
            gamestate.westSharedType2=this.state.westSharedType2;
            gamestate.westSharedValue2=this.state.westSharedValue2;
            const interval = setInterval(() => {
                this.props.navigation.navigate('BiddingKibz',{xmppconn : hool,tableID:tableID,gameState:gamestate});
                clearInterval(interval);
             }, 2000);
            
          }
          else{
            console.log('not all info shared');  
          }
      }

    
        
         displayCards(hand) {
            
            var cardSide=[];  
            var idx=0;
             hand.forEach((card) =>{   
              //56   console.log(card.suit);          
              if(card.suit==="S")
                {
                    cardSide.push({"rank":card.rank,"img": require('../../assets/images/spade.png'),"suit":"S","key": card.rank + "S","SuiteColor":"#C000FF","idx":idx}) 
                    
                }else if(card.suit==='D')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/diamond.png'),"suit":"D","key": card.rank + "D","SuiteColor":"#04AEFF","idx":idx})
                     
                }else if(card.suit==='H')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/hearts.png'),"suit":"H","key": card.rank + "H","SuiteColor":"#FF0C3E","idx":idx});
                     
                }else if(card.suit==='C')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/clubs.png'),"suit":"C","key": card.rank + "C","SuiteColor":"#79E62B","idx":idx},)
                    
                }  
                idx++;  
            });
           // this.drawCardsLHO();
          
           return cardSide;
        }
 

          
         
     render(){   
            var drawmycards = (
                southHands.map((element) => {  
                    //console.log('drawCards'+element.suit +' , '+ element.rank); 
                  return ( 
                    <View style={[styles.cardDisplayBody]} key={element.key} > 
                            <TouchableOpacity onPress={()=>this.cardPlayed(element.idx,element.suit, element.rank,element.img,element.SuiteColor)} disabled={ (element.suit==="C") ? this.state.disablemyCards_C:(element.suit==="D") ? this.state.disablemyCards_D:(element.suit==="H") ? this.state.disablemyCards_H:(element.suit==="S") ? this.state.disablemyCards_S:null}>
                            <View style={styles.cardPosition}>
                                <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                                <Image source={element.img} />
                            </View> 
                            </TouchableOpacity> 
                    </View>                     
                  );                  
                })
              );
        
      const drawCardsPartner = () => {
        return northHands.map((element) => {
            console.log('drawCardsPartner');
          return (
            <View style={[styles.cardDisplayBodyNorth]} key={element.key}>
                 <TouchableOpacity onPress={()=>this.dummycardPlayed(element.idx,element.suit, element.rank,element.img,element.SuiteColor)} disabled={ (element.suit==="C") ? this.state.disabledummyCards_C:(element.suit==="D") ? this.state.disabledummyCards_D:(element.suit==="H") ? this.state.disabledummyCards_H:(element.suit==="S") ? this.state.disabledummyCards_S:null}> 
                 
                    <View style={styles.cardPosition}>
                        <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                        <Image source={element.img} />
                    </View>   
                </TouchableOpacity>   
            </View>
          );
        });
      };
      const drawCardsLHO = () => {
        return westHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBodyNorth]} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsRHO = () => {
        return eastHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBodyEast]} key={element.key} >
                <View style={styles.cardPositionE}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };


return (
<View style={{backgroundColor:"#0a0a0a",height:"100%"}}>
    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", width:"100%", height:HeightBoxTop, backgroundColor:"#151515"}}>
        <View style={{width:"100%", height:ContractTextHeight, borderBottomWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
            <Text style={styles.ContractTextHeader}>Contract</Text>
        </View>
        <View style={{width:"100%",flexDirection:"row", borderBottomWidth:1, borderBottomColor:"#0a0a0a"}}>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                {/* <Text style={styles.ContractText}>4</Text>  */}
                {this.state.winnerSuit==='S' ?  <Text style={styles.ContractTextSpade}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='H' ?  <Text style={styles.ContractTextHearts}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='D' ?  <Text style={styles.ContractTextDiamond}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='C' ?  <Text style={styles.ContractTextClubs}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='NT' ?  <Text style={styles.ContractTextNT}>{this.state.winnerSide}</Text> :null }
                 
            </View>
            <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                {/* <Text style={styles.ContractMdlText}>4</Text>
                <Image source={require('../../assets/images/hearts_inactive.png')} /> */}
                {
                this.state.winnerSuit==='S' ?
                <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                    <Text style={styles.ContractTextSpade}>{this.state.winnerLevel}</Text>
                    <Image source={require('../../assets/images/spade.png')} />
                </View>
                :null
                }
                {
                this.state.winnerSuit==='H' ?
                <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                    <Text style={styles.ContractTextHearts}>{this.state.winnerLevel}</Text>
                    <Image source={require('../../assets/images/hearts.png')} /> 
                </View>
                :null
                }
                
                {
                this.state.winnerSuit==='D' ?
                <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                 <Text style={styles.ContractTextDiamond}>{this.state.winnerLevel}</Text>
                <Image source={require('../../assets/images/diamond.png')} /> 
                </View>
                :null
                }
                {
                this.state.winnerSuit==='C' ?
                <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                 <Text style={styles.ContractTextClubs}>{this.state.winnerLevel}</Text>
                <Image source={require('../../assets/images/clubs.png')} /> 
                </View>
                :null
                }
                {
                this.state.winnerSuit==='NT' ?
                <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                 <Text style={styles.ContractTextNT}>{this.state.winnerLevel}</Text> 
                </View>
                :null
                }
            </View>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, alignItems:"center", justifyContent:"center"}}>
                {/* <Text style={styles.ContractText}>x4</Text> */}
                 
                {
                    (this.state.winnerSuit==='S' && this.state.winnerdblRedbl !=='') ?
                 
                    <Text style={styles.ContractTextSpade}>{this.state.winnerdblRedbl}</Text> 
                :null
                }
                {
                    (this.state.winnerSuit==='H' && this.state.winnerdblRedbl !=='')  ?
                
                    <Text style={styles.ContractTextHearts}>{this.state.winnerdblRedbl}</Text> 
                :null
                }
                
                {
                    (this.state.winnerSuit==='D' && this.state.winnerdblRedbl !=='') ? 
                 <Text style={styles.ContractTextDiamond}>{this.state.winnerdblRedbl}</Text> 
                :null
                }
                {
                    (this.state.winnerSuit==='C' && this.state.winnerdblRedbl !=='')  ?                 
                 <Text style={styles.ContractTextClubs}>{this.state.winnerdblRedbl}</Text>  
                :null
                }
                {
                    (this.state.winnerSuit==='NT' && this.state.winnerdblRedbl !=='')  ?                 
                 <Text style={styles.ContractTextNT}>{this.state.winnerdblRedbl}</Text>  
                :null
                }
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, alignItems:"center"}}>
        <View style={{width:"100%",height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
                <Text style={this.state.NSStyle}>N • S</Text>
            
            <Text adjustsFontSizeToFit numberOfLines={1}  style={this.state.NSSharingStyle}>SHARE</Text>
            </View>
  
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, alignItems:"center"}}>
        <View style={{width:"100%",height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
                <Text style={this.state.EWStyle}>E • W</Text>
            
            <Text adjustsFontSizeToFit numberOfLines={1}  style={this.state.EWSharingStyle}>SHARE</Text>
            </View>
        </View>
    </View>

    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/settings.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/flag.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/revers.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/chat.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/on.png')} />
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", justifyContent:"space-between"}}>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <View style={styles.hanNorthSauthTotalWidthHeightArea}>
                <View style={{width:"100%", alignItems:"center", justifyContent:"center"}}>
                    <View style={{width:NorthCardWithKibitzerArea, height:45, flexDirection:"row", justifyContent:"space-around"}}>
                        { drawCardsPartner() }  
                    </View>
                </View> 

                 <View style={[styles.hanNameWidthHeightArea,{zIndex:200}]}>
                    <Text style={[styles.shareHandText,{color:this.state.northUserText}]}>{this.state.northUser}</Text>
                </View>

                <View style={[styles.patternTotalColumnArea]}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.northSharedType1}</Text>
                            <Text style={styles.PaternText}>{this.state.northSharedValue1}</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.northSharedType2}</Text>
                            <Text style={styles.PaternText}>{this.state.northSharedValue2}</Text>
                        </View>
                    </View>
                </View>               
                   
            </View>
        </View>


        <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
            <View style={{position:"absolute",left:3, borderWidth:0, borderColor:"red",  width:113, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center", }}>
             <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:113,  transform: [{ rotate: '-90deg' }]}}>
                <View style={{width:EastWestPalyCardArea, height:45, marginLeft:75, flexDirection:"row", justifyContent:"space-around"}}>
                    { drawCardsLHO() }  
                </View>
                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.shareHandText,{color:this.state.westUserText}]}>{this.state.westUser}</Text>
                </View> 

                 <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.westSharedType1}</Text>
                            <Text style={styles.PaternText}>{this.state.westSharedValue1}</Text>
                        </View>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.westSharedType2}</Text>
                            <Text style={styles.PaternText}>{this.state.westSharedValue2}</Text>
                        </View>
                    </View>
                </View>   
                
               
            </View>
            </View>

        <View style={{position:"absolute",right:0, width:113, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center",  }}>
            <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:113,  transform: [{ rotate: '90deg' }]}}>
                    
                    <View style={{width:EastWestPalyCardArea, marginRight:-17, height:45,justifyContent:"space-around", flexDirection:"row-reverse"}}>
                        { drawCardsRHO() }  
                    </View>

                    <View style={styles.hanNameWidthHeightArea}>
                        <Text style={[styles.shareHandText,{color:this.state.eastUserText}]}>{this.state.eastUser}</Text>
                    </View> 

                    <View style={styles.patternTotalColumnArea}>
                        <View style={styles.patternTotalRowArea}>
                            <View style={styles.patternWidthHeightArea}>
                                <Text style={styles.shareHandText}>{this.state.eastSharedType1}</Text>
                                <Text style={styles.PaternText}>{this.state.eastSharedValue1}</Text>
                            </View>
                            <View style={styles.patternWidthHeightArea}>
                                <Text style={styles.shareHandText}>{this.state.eastSharedType2}</Text>
                                <Text style={styles.PaternText}>{this.state.eastSharedValue2}</Text>
                            </View>
                        </View>
                    </View> 
                  
                
            </View>
        </View>


            <View style={styles.playingCardTotalWidthHeightArea}>
                <View style={styles.northCardPosition}>
                 
                {
                    (this.state.PartnerPlayed) ?
                        <View style={this.state.PartnerCardStyle}>
                            <View style={styles.playCardNumberPosition}>
                                <Text style={this.state.PartnerCardNumberstyle}>{this.state.PartnerRank}</Text>
                            </View>
                            <Image source={this.state.PartnerSuit} /> 
                        </View>
                    :
                    null//<View style={styles.CardPositionBorderAss}></View>
                }
                </View>

                <View style={styles.playingEastWestCardTotalWidthHeightArea}>
                    <View style={styles.westCardPosition}>
                        {
                            (this.state.LHOPlayed) ?
                            <View style={this.state.LHOCardStyle}>
                                <View style={styles.playCardNumberPosition}>
                                    <Text style={this.state.LHOCardNumberstyle}>{this.state.LHORank}</Text>
                                </View>
                                <Image source={this.state.LHOSuit} /> 
                             </View>
                            :
                            null//<View style={styles.CardPositionBorderAss}></View> 
                        }
                        
                        
                    </View>
                    <View style={styles.eastCardPosition}>
                        
                        {
                        (this.state.RHOPlayed) ?
                            <View style={this.state.RHOCardStyle}>
                                <View style={styles.playCardNumberPosition}>
                                    <Text style={this.state.RHOCardNumberstyle}>{this.state.RHORank}</Text>
                                </View>
                                <Image source={this.state.RHOSuit} /> 
                            </View>
                        :
                        null//<View style={styles.CardPositionBorderAss}></View>
                        }
                    </View>
                </View>
                
                <View style={styles.sauthCardPosition}>
                     
                    {
                      (this.state.MyCard) ?
                        <View style={this.state.myCardStyle}>
                            <View style={styles.playCardNumberPosition}>
                                <Text style={this.state.myCardNumberstyle}>{this.state.myRank}</Text>
                            </View>
                            <Image source={this.state.mySuit} /> 
                        </View>
                    :
                     null//  <View style={styles.CardPositionBorderAss}></View>
                    }
                </View>
                
                 <View style={styles.totalWidthheightPositionDirectionArea}>
                    <View style={styles.playCardDirectionPosition}>
                        <Text style={this.state.DirectionTextPartner}>{this.state.myPartnerSeat}</Text>
                        <View style={styles.pointerEastWestAreaPosition}>
                            <Text style={this.state.DirectionTextLHO}>{this.state.myLHOSeat}</Text>
                            <View style={styles.directionPointerPosition}>
                                <Image source={this.state.directionPointer} />
                            </View>
                            <Text style={this.state.DirectionTextRHO}>{this.state.myRHOSeat}</Text>
                        </View>
                        <Text style={this.state.DirectionTextMySeat}>{this.state.mySeat}</Text>
                    </View>
                </View> 

                 
            </View>
        </View>

        <View style={{width:"100%", height:113, alignItems:"flex-end"}}>
            <View style={{width:"100%", flexDirection:"column", alignItems:"center"}}>

            <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.southSharedType1}</Text>
                            <Text style={styles.PaternText}>{this.state.southSharedValue1}</Text>
                        </View>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>{this.state.southSharedType2}</Text>
                            <Text style={styles.PaternText}>{this.state.southSharedValue2}</Text>
                        </View>
                    </View>
                </View>            
            </View>
            <View style={styles.hanNameWidthHeightArea}>
                <Text style={[styles.shareHandText,{color:this.state.southUserText}]}>{this.state.southUser}</Text>
            </View>
            <View style={{width:TotalCardWidth, height:45,  flexDirection:"row", justifyContent:"space-around",alignSelf:"center"}}>
                 {drawmycards}
            </View>
        </View>

   </View>
</View>

  );
};
}
 

const styles = StyleSheet.create({
    playingEastWestCardTotalWidthHeightArea:{
        flexDirection:"row", 
        width:"100%", 
        height:55, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    playingCardTotalWidthHeightArea:{
        position:'absolute',
        width:225, 
        height:225,
        flexDirection:"column", 
        alignItems:"center",
        justifyContent:"space-between",
    },
    totalWidthheightPositionDirectionArea:{
        position:"absolute",
        flexDirection:"row", 
        width:"100%", 
        height:225, 
        justifyContent:"center", 
        alignItems:"center",
    },
    pointerEastWestAreaPosition:{
        flexDirection:"row", 
        justifyContent:"space-around", 
        alignItems:"center",  
        paddingRight:4,
    },
    patternTotalColumnArea:{
        flexDirection:"column", 
        width:"100%", 
        height:45,  
        alignItems:"center",
    },
    patternTotalRowArea:{
        flexDirection:"row",
        width:181, 
        height:45, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    hanNorthSauthTotalWidthHeightArea:{
        width:"100%", 
        height:113, 
        alignItems:"flex-end",
        flexDirection:"column",
    },
    hanNameWidthHeightArea:{
        width:"100%", 
        height:23,
    },
    patternWidthHeightArea:{
        width:90, 
        height:45, 
        backgroundColor:"#151515",
    },
    cardDisplayBodyNorth:{
        width:61,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
    },
    cardDisplayBodyEast:{
        width:61,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
        flexDirection:"row-reverse",
    },
    cardPositionW:{
        width:22,
        marginTop:3,
        marginLeft:1,
        alignItems:"center",
        justifyContent:"center",
     },
     cardPositionE:{
        width:22,
        marginTop:-4,
        marginLeft:2,
        alignItems:"center",
        justifyContent:"center",
     },
    directionPointerPosition:{
        width:27,
        height:27,
        borderWidth:0,
        borderColor:"red",
        alignItems:"center",
        justifyContent:"center",
        top:1,
    },
    ContractTextSpade: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#C000FF",
    },
    ContractTextHearts: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    ContractTextDiamond: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#04AEFF",
    },
    ContractTextClubs: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#79E62B",
    },
    ContractTextNT: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#FFE81D",
    },
    bidPlayText: {
		fontFamily:"Roboto-Thin",
        fontSize: 55,
        fontWeight: "normal",
        color:"#6D6D6D",
      },
    CardPositionBorderAss:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#676767",
    },
    northCardPosition:{
        width:55,
        height:81,
    },
    sauthCardPosition:{
        width:55,
        height:81,
    },
    westCardPosition:{
        width:55,
        height:81,
        marginLeft: 12, 
        transform: [{ rotate: '-90deg' }],
    },
    eastCardPosition:{
        width:55,
        height:81,
        marginRight: 12, 
        transform: [{ rotate: '90deg' }],
    },
    playCardNumberPosition:{
        position:"absolute",
        width:55,
        height:81,
        borderWidth:0,
        borderColor:"red",
        paddingTop:2,
        paddingLeft:6,

    },
    playCardNumberDiamond:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
    },
    playCardDiamond:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberHearts:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    playCardHearts:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberClubs:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
    },
    playCardClubs:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberSpade:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
    },
    playCardSpade:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardDirectionPosition:{
        width:60, 
        height:60, 
        justifyContent:"space-between", 
        flexDirection:"column",
    },
    DirectionText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#676767",
        textAlign:"center", 
      }, 
      DirectionTextActive: {
		  fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
      }, 


/*************************************** */

    SharePatternCardSpade:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardHearts:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardDiamond:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardClubs:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    PatternText:{
		fontFamily:"Roboto-Light",
        fontSize: 14,
        fontWeight: "400",
        color:"#FFFFFF",
    },
    SharePatternCard:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#DBFF00",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    cardDisplayBody:{
        width:61,
        // height:91,
        height:45,
        borderTopLeftRadius:4,
        borderTopRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515"
    },
    
    cardPosition:{
        width:22,
        marginTop:3,
        marginLeft:4,
        alignItems:"center",
        justifyContent:"center",
     },
     SpadeCardNumber:{
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
     },
     heartsCardNumber:{
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
     },
     clubsCardNumber:{
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
     },
     diamondCardNumber:{
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
     },

    ContractText: {
        position:"absolute",
        top:8,
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
    },
    ContractTextHeader: {
        
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
    },
    ContractTextActive: {
        position:"absolute",
        top:8,
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
    },
    ContractMdlText:{
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
        paddingRight:4,
    },
    ShareText: {
        top:5,
		fontFamily:"Roboto-Thin",
        fontSize: normalize(27),
        fontWeight: "normal",
        color:"#353535",
      },
      ShareTextActive: {
        top:5,
		fontFamily:"Roboto-Thin",
        fontSize: normalize(27),
        fontWeight: "normal",
        color:"#6D6D6D",
        paddingTop:10,
      },
      shareHandText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#6D6D6D",
        textAlign:"center",
        paddingTop:3,
      },
      PaternText: {
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FFFFFF",
        textAlign:"center",
      },
    OpenText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
        padding:4,
    },
   
  });
