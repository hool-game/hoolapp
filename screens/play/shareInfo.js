import React from 'react';
import SafeArea from 'react-native-safe-area';
import {
  AppState,
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {
  HOOL_CLIENT,
  formatSharedValue,
  getUserNameBasedOnHost
} from '../../shared/util';
import { SCREENS } from '../../constants/screens';
import { normalize } from '../../styles/global';
//import SvgUri from 'react-native-svg-uri';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { scale, verticalScale, ScaledSheet, moderateScale, moderateVerticalScale } from 'react-native-size-matters/extend';
import DeviceInfo, { isTablet } from 'react-native-device-info';
import { Strings } from '../../styles/Strings';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AddPlayer from '../../shared/AddPlayer';
import CrownHost from '../../shared/CrownHost';
//import { hostCrown, hasSpaceForCrown } from '../../shared/util';

var ScreenWidth = Dimensions.get('window').width;
const cardDimension = 35;
var cardWidth = scale(cardDimension);
var kibitzerCardWidth = scale(29);
var TotalCardWidthSouth = (533 / 640) * ScreenWidth;
var eastWestcardWidth = scale(25);
var KibitzerEastWestcardWidth = scale(25);

export default class ShareInfo extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      isTablet: DeviceInfo.isTablet(),
      hands: [],
      eastHands: [],
      westHands: [],
      northHands: [],
      southHands: [],
      output: '',
      mySeat: '',
      myLHOSeat: '',
      myRHOSeat: '',
      myPartnerSeat: '',
      northUser: '',
      eastUser: '',
      southUser: '',
      westUser: '',
      showShareCards: false,
      showSuitCards: false,
      northSharedType1: '',
      northSharedValue1: '',
      northSharedType2: '',
      northSharedValue2: '',
      eastSharedType1: '',
      eastSharedValue1: '',
      eastSharedType2: '',
      eastSharedValue2: '',
      southSharedType1: '',
      southSharedValue1: '',
      southSharedType2: '',
      southSharedValue2: '',
      westSharedType1: '',
      westSharedValue1: '',
      westSharedType2: '',
      westSharedValue2: '',
      urlSp: svgImages.spade,
      urlHt: svgImages.hearts,
      urlDm: svgImages.diamond,
      urlCb: svgImages.clubs,
      inactSp: styles.SharePatternCardSpade.borderColor,
      inactHt: styles.SharePatternCardHearts.borderColor,
      inactDm: styles.SharePatternCardDiamond.borderColor,
      inactCb: styles.SharePatternCardClubs.borderColor,
      cardHCPTextColor: '#FFFFFF',
      cardPatternTextColor: '#FFFFFF',
      cardSuitTextColor: '#FFFFFF',
      directionPointer: svgImages.centerCircle,
      DirectionTextPartner: '#6D6D6D',
      DirectionTextLHO: '#6D6D6D',
      DirectionTextRHO: '#6D6D6D',
      DirectionTextMySeat: '#6D6D6D',
      northUserText: '#6D6D6D',
      eastUserText: '#6D6D6D',
      southUserText: '#6D6D6D',
      westUserText: '#6D6D6D',
      HCPCardBorder: '#DBFF00',
      PatternCardBorder: '#DBFF00',
      SuitCardBorder: '#DBFF00',
      NSStyle: styles.ContractText,
      NSSharingStyle: styles.ShareText,
      EWStyle: styles.ContractText,
      EWSharingStyle: styles.ShareText,
      textColor: '#FFFFFF',
      boxBorderColor: '#DBFF00',
      inactiveColor: '#6D6D6D'
    };
  }

  componentDidMount() {
    console.log('ShareInfo componentDidMount called');
    SafeArea.getSafeAreaInsetsForRootView().then(result => {     
      this.loadInitialScreenValues();
    });
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.tableID !== prevProps.tableID) {
      this.updateStateValues({ tableID: this.props.tableID });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
    if ( this.props.chatScreen !== prevProps.chatScreen) {
      this.updateStateValues({ chatScreen: this.props.chatScreen})
    }
    if (this.props.northAvtar !== prevProps.northAvtar) {
      this.updateStateValues({ northAvtar: this.props.northAvtar });
    }
    if (this.props.eastAvtar !== prevProps.eastAvtar) {
      this.updateStateValues({ eastAvtar: this.props.eastAvtar });
    }
    if (this.props.southAvtar !== prevProps.southAvtar) {
      this.updateStateValues({ southAvtar: this.props.southAvtar });
    }
    if (this.props.westAvtar !== prevProps.westAvtar) {
      this.updateStateValues({ westAvtar: this.props.westAvtar });
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.updateStateValues({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.updateStateValues({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.updateStateValues({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.updateStateValues({ westUser: this.props.westUser });
    }
    if (this.props.iskibitzer !== prevProps.iskibitzer) {
      this.updateStateValues({ iskibitzer: this.props.iskibitzer });
    }
    if (this.props.infoScreen !== prevProps.infoScreen) {
      this.updateStateValues({ infoScreen: this.props.infoScreen });
    }

    if (this.props.eastUser == Strings.open && this.props.westUser == Strings.open
      && this.props.northUser == Strings.open && this.props.southUser == Strings.open 
      && this.props.iskibitzer) {
        HOOL_CLIENT.leaveTable(this.state.tableID);
        this.props.navigation.dispatch(CommonActions.navigate({
          name: "Home"
        }));
    }
  }

  componentWillUnmount() {
    // AppState.removeEventListener('change', this._handleAppStateChange);
    // // Remove the event listener
    // if (this.focusListener != null && this.focusListener.remove) {
    //   this.focusListener.remove();
    // }
    // if (this.infosharelsn !== undefined) {
    //   this.infosharelsn.remove();
    //   console.log('shareinfo lsn removed');
    // }
    console.log('shareinfo closed');
  }

  loadInitialScreenValues() {
    console.log('Loading Table Screen values ' + this.props.tableID);
    //console.log("ChatScreen :",this.props.chatScreen)
    this.setState(
      {
        appState: AppState.currentState,
        output: '',
        mySeat: '',
        myLHOSeat: '',
        myRHOSeat: '',
        myPartnerSeat: '',
        northUser: '',
        eastUser: '',
        southUser: '',
        westUser: '',
        showShareCards: false,
        showSuitCards: false,
        urlSp: svgImages.spade,
        urlHt: svgImages.hearts,
        urlDm: svgImages.diamond,
        urlCb: svgImages.clubs,
        inactSp: styles.SharePatternCardSpade.borderColor,
        inactHt: styles.SharePatternCardHearts.borderColor,
        inactDm: styles.SharePatternCardDiamond.borderColor,
        inactCb: styles.SharePatternCardClubs.borderColor,
        cardHCPTextColor: '#FFFFFF',
        cardPatternTextColor: '#FFFFFF',
        cardSuitTextColor: '#FFFFFF',
        HCPCardBorder: '#DBFF00',
        PatternCardBorder: '#DBFF00',
        SuitCardBorder: '#DBFF00',
        NSStyle: styles.ContractText,
        NSSharingStyle: styles.ShareText,
        EWStyle: styles.ContractText,
        EWSharingStyle: styles.ShareText,
        ...this.props,
        isValueInitialized: true
      },
      () => {
        this.updateScreen();
      }
    );
  }

  updateStateValues(stateObj) {
    this.setState(stateObj);
  }

  async getTableInfo() {
    let gamestate = this.state.gamestate;
    let infoScreen = this.state.infoScreen;
    let checkForBidding = false;
    console.log(' gamestate as below');
    console.log(gamestate);
    let myHand = gamestate.hands;
    let hands = [];
    let southHands = [];
    let northHands = [];
    let westHands = [];
    let eastHands = [];
    //If the device length is less than total card size then use the vertical scale proportion
    if (TotalCardWidthSouth < 13 * scale(cardDimension))
      cardWidth = verticalScale(cardDimension);
    TotalCardWidthSouth = 13 * (this.state.iskibitzer? kibitzerCardWidth: cardWidth);
    console.log('myHand', JSON.stringify(myHand));
    myHand.forEach((h, key) => {
      console.log(h);
      if (h.side === gamestate.mySeat) {
        southHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myPartnerSeat) {
        northHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myLHOSeat) {
        westHands = this.displayCards(h.cards);
      } else if (h.side === gamestate.myRHOSeat) {
        eastHands = this.displayCards(h.cards);
        eastHands = [...eastHands].reverse();
        //console.log('eastHands');
        // console.log(eastHands);
      }
    });
    this.setState({ southHands, northHands, westHands, eastHands });
    myHand.forEach((h, key) => {
      console.log("---------->",h.infoShared);
      let suitType1 = null;
      let suitType2 = null;
      let suitValue1 = null;
      let suitValue2 = null;
      if (h.infoShared.length > 0) {
        suitType1 = this.getSuitType(h.infoShared[0].type);
        suitValue1 = h.infoShared[0].value;
        if (h.infoShared.length > 1) {
          suitType2 = this.getSuitType(h.infoShared[1].type);
          suitValue2 = h.infoShared[1].value;
        }
      }
      if (key === this.state.mySeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);
        if (suitType1 !== null) {
          infoScreen.southSharedType1 = suitType1;
          infoScreen.southSharedValue1 = suitValue1;
          if (suitType1.toLowerCase() === 'hcp') {
            infoScreen.infoHCPClicked = true;
          } else if (suitType1.toLowerCase() === 'pattern') {
            infoScreen.infoPatternClicked = true;
          } else if (suitType1.toLowerCase() === 'spades') {
            infoScreen.numSpShared = true;
          } else if (suitType1.toLowerCase() === 'hearts') {
            infoScreen.numHtShared = true;
          } else if (suitType1.toLowerCase() === 'diamonds') {
            infoScreen.numDmShared = true;
          } else if (suitType1.toLowerCase() === 'clubs') {
            infoScreen.numCbShared = true;
          }
        }
        if (suitType2 !== null) {
          infoScreen.southSharedType2 = suitType2;
          infoScreen.southSharedValue2 = suitValue2;
        }
      } else if (key === this.state.myLHOSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);
        if (suitType1 !== null) {
          infoScreen.westSharedType1 = suitType1;
          infoScreen.westSharedValue1 = suitValue1;
        }
        if (suitType2 !== null) {
          infoScreen.westSharedType2 = suitType2;
          infoScreen.westSharedValue2 = suitValue2;
        }
      } else if (key === this.state.myPartnerSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);
        if (suitType1 !== null) {
          infoScreen.northSharedType1 = suitType1;
          infoScreen.northSharedValue1 = suitValue1;
        }
        if (suitType2 !== null) {
          infoScreen.northSharedType2 = suitType2;
          infoScreen.northSharedValue2 = suitValue2;
        }
      } else if (key === this.state.myRHOSeat) {
        if (key === gamestate.side) hands = this.displayCards(h.cards);
        if (suitType1 !== null) {
          infoScreen.eastSharedType1 = suitType1;
          infoScreen.eastSharedValue1 = suitValue1;
        }
        if (suitType2 !== null) {
          infoScreen.eastSharedType2 = suitType2;
          infoScreen.eastSharedValue2 = suitValue2;
        }
      }
    });
    this.setState({ hands });
    if (gamestate.tableinfo !== undefined) {
      infoScreen.DirectionTextPartner = '#6D6D6D';
      infoScreen.DirectionTextLHO = '#6D6D6D';
      infoScreen.DirectionTextRHO = '#6D6D6D';
      infoScreen.DirectionTextMySeat = '#6D6D6D';
      infoScreen.northUserText = '#6D6D6D';
      infoScreen.eastUserText = '#6D6D6D';
      infoScreen.southUserText = '#6D6D6D';
      infoScreen.westUserText = '#6D6D6D';
      infoScreen.showShareCards = false;
      console.log(gamestate.tableinfo.turn, 'Turn');
      if (gamestate.tableinfo.turn === this.state.mySeat) {
        infoScreen.showShareCards = true;
        infoScreen.DirectionTextMySeat = '#DBFF00';
        infoScreen.southUserText = '#DBFF00';
        infoScreen.directionPointer = svgImages.southDirectionPointer;
        infoScreen = this.toggleSideSelection(this.state.mySeat, infoScreen);
      } else if (gamestate.tableinfo.turn === this.state.myRHOSeat) {
        infoScreen.DirectionTextRHO = '#DBFF00';
        infoScreen.eastUserText = '#DBFF00';
        infoScreen.directionPointer = svgImages.eastDirectionPointer;
        infoScreen = this.toggleSideSelection(this.state.myRHOSeat, infoScreen);
      } else if (gamestate.tableinfo.turn === this.state.myPartnerSeat) {
        infoScreen.DirectionTextPartner = '#DBFF00';
        infoScreen.northUserText = '#DBFF00';
        infoScreen.directionPointer = svgImages.northDirectionPointer;
        infoScreen = this.toggleSideSelection(
          this.state.myPartnerSeat,
          infoScreen
        );
      } else if (gamestate.tableinfo.turn === this.state.myLHOSeat) {
        infoScreen.DirectionTextLHO = '#DBFF00';
        infoScreen.westUserText = '#DBFF00';
        infoScreen.directionPointer = svgImages.westDirectionPointer;
        infoScreen = this.toggleSideSelection(this.state.myLHOSeat, infoScreen);
      }
    }

    this.props.updateDeskStateValues({ infoScreen }, () => {
      if (checkForBidding) {
        let result = this.props.checkBidding(
          this.state.infoScreen,
          this.state.gamestate
        );
        if (result.redirect) {
          this.props.updateDeskStateValues(
            { gamestate: result.gamestate },
            () => {
              const interval = setInterval(() => {
                this.props.updateDeskStateValues({
                  currentScreen: SCREENS.BID_INFO
                });
                clearInterval(interval);
              }, 2000);
            }
          );
        }
      }
    });
  }

  getSuitType(suitType) {
    let suitVal = null;
    if (suitType === 'D') {
      suitVal = 'Diamonds';
    } else if (suitType === 'C') {
      suitVal = 'Clubs';
    } else if (suitType === 'H') {
      suitVal = 'Hearts';
    } else if (suitType === 'S') {
      suitVal = 'Spades';
    } else {
      suitVal = suitType;
    }
    return suitVal;
  }

  getChatMessage(seat) {
    if( seat === 'N'){
      if(this.state.chatScreen.isNorthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'S'){
      if(this.state.chatScreen.isSouthSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'W'){
      if(this.state.chatScreen.isWestSentMsg){
         return false
      }else{
         return true
      }
    }else if( seat === 'E'){
      if(this.state.chatScreen.isEastSentMsg){
         return false
      }else{
         return true
      }
    }else{
      return true
    }
  }
  updateScreen() {
    let gamestate = this.state.gamestate;
    let infoScreen = this.state.infoScreen;
    infoScreen.showShareCards = false;
    console.log('share info mount');
    // AppState.addEventListener('change', this._handleAppStateChange);
    // if(gamestate.side==="N")
    // {
    //     this.setState({mySeat:'N',myLHOSeat:'E',myPartnerSeat:'S',myRHOSeat:'W'});
    //     this.setState({southUser:gamestate.northUser,westUser:gamestate.eastUser,northUser:gamestate.southUser,eastUser:gamestate.westUser})
    //     if(this.state.northSharedType1==='')
    //     {
    //         this.setState({showShareCards:true})
    //     }
    //     this.setState({DirectionTextMySeat:'#DBFF00',southUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_s.png')});
    // }else if(gamestate.side==="E")
    // {
    //   this.setState({mySeat:'E',myLHOSeat:'S',myPartnerSeat:'W',myRHOSeat:'N'});
    //   this.setState({southUser:gamestate.eastUser,westUser:gamestate.southUser,northUser:gamestate.westUser,eastUser:gamestate.northUser})
    //   this.setState({DirectionTextRHO:'#DBFF00',eastUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_e.png')});
    // }else if(gamestate.side==="S")
    // {
    //   this.setState({mySeat:'S',myLHOSeat:'W',myPartnerSeat:'N',myRHOSeat:'E'});
    //   this.setState({southUser:gamestate.southUser,westUser:gamestate.westUser,northUser:gamestate.northUser,eastUser:gamestate.eastUser})
    //   this.setState({DirectionTextPartner:'#DBFF00',northUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_n.png')});
    // }else if(gamestate.side==="W")
    // {
    //   this.setState({mySeat:'W',myLHOSeat:'N',myPartnerSeat:'E',myRHOSeat:'S'});
    //     this.setState({southUser:gamestate.westUser,westUser:gamestate.northUser,northUser:gamestate.eastUser,eastUser:gamestate.southUser})
    //     this.setState({DirectionTextLHO:'#DBFF00',westUserText:'#DBFF00',directionPointer:require('../../assets/images/direction_pointer_w.png')});
    // }

    this.setState({
      mySeat: gamestate.mySeat,
      myLHOSeat: gamestate.myLHOSeat,
      myPartnerSeat: gamestate.myPartnerSeat,
      myRHOSeat: gamestate.myRHOSeat
    });
    this.setState({
      southUser: gamestate.myUsername,
      westUser: gamestate.myLHOname,
      northUser: gamestate.myPartnername,
      eastUser: gamestate.myRHOname
    });
    console.log('gamestate.dealer ' + gamestate.dealer);
    if (
      gamestate.dealer === gamestate.mySeat &&
      ((gamestate.tableinfo !== undefined &&
        gamestate.tableinfo.turn === gamestate.mySeat) ||
        gamestate.tableinfo === undefined)
    ) {
      if (this.state.southSharedType1 === '') {
        infoScreen.showShareCards = true;
      }
      console.log('entered');
      infoScreen.DirectionTextMySeat = '#DBFF00';
      infoScreen.southUserText = '#DBFF00';
      infoScreen.directionPointer = svgImages.southDirectionPointer
      infoScreen = this.toggleSideSelection(gamestate.mySeat, infoScreen);
    } else if (gamestate.dealer === gamestate.myRHOSeat) {
      infoScreen.DirectionTextRHO = '#DBFF00';
      infoScreen.eastUserText = '#DBFF00';
      infoScreen.directionPointer = svgImages.eastDirectionPointer
      infoScreen = this.toggleSideSelection(gamestate.myRHOSeat, infoScreen);
    } else if (gamestate.dealer === gamestate.myPartnerSeat) {
      infoScreen.DirectionTextPartner = '#DBFF00';
      infoScreen.northUserText = '#DBFF00';
      infoScreen.directionPointer = svgImages.northDirectionPointer
      infoScreen = this.toggleSideSelection(
        gamestate.myPartnerSeat,
        infoScreen
      );
    } else if (gamestate.dealer === gamestate.myLHOSeat) {
      infoScreen.DirectionTextLHO = '#DBFF00';
      infoScreen.westUserText = '#DBFF00';
      infoScreen.directionPointer = svgImages.westDirectionPointer
      infoScreen = this.toggleSideSelection(gamestate.myLHOSeat, infoScreen);
    }

    this.props.updateDeskStateValues({ infoScreen }, () => {
      setTimeout(() => {
        this.getTableInfo();
      }, 500);
    });
  }

  toggleSideSelection(side, infoScreen) {
    console.log('side', side);
    if (side === 'E' || side === 'W') {
      infoScreen.NSStyle = styles.ContractText;
      infoScreen.NSSharingStyle = styles.ShareText;
      infoScreen.NSSharingBottom = '#0a0a0a';
      infoScreen.EWStyle = styles.ContractTextActive;
      infoScreen.EWSharingStyle = styles.ShareTextActive;
      infoScreen.EWSharingBottom = '#DBFF00';
    } else if (side === 'N' || side === 'S') {
      infoScreen.NSStyle = styles.ContractTextActive;
      infoScreen.NSSharingStyle = styles.ShareTextActive;
      infoScreen.NSSharingBottom = '#DBFF00';
      infoScreen.EWStyle = styles.ContractText;
      infoScreen.EWSharingStyle = styles.ShareText;
      infoScreen.EWSharingBottom = '#0a0a0a';
    }
    return infoScreen;
  }
  displayCards(hand) {
    var cardSide = [];
    var idx = 0;
    console.log(hand);
    hand.forEach(card => {
      //56   console.log(card.suit);
      if (card.suit === 'S') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.spade,
          suit: 'S',
          key: card.rank + 'S',
          SuiteColor: '#C000FF',
          idx: idx
        });
      } else if (card.suit === 'D') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.diamond,
          suit: 'D',
          key: card.rank + 'D',
          SuiteColor: '#04AEFF',
          idx: idx
        });
      } else if (card.suit === 'H') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.hearts,
          suit: 'H',
          key: card.rank + 'H',
          SuiteColor: '#FF0C3E',
          idx: idx
        });
      } else if (card.suit === 'C') {
        cardSide.push({
          rank: card.rank,
          img: svgImages.clubs,
          suit: 'C',
          key: card.rank + 'C',
          SuiteColor: '#79E62B',
          idx: idx
        });
      }
      idx++;
    });
    return cardSide;
  }

  infoShare = infoType => {
    console.log('info tapped');
    let infoScreen = this.state.infoScreen;
    if (infoType === 'HCP') {
      infoScreen.infoHCPClicked = true;
      this.setState({
        cardHCPTextColor: '#353535',
        HCPCardBorder: '#151515'
      });
    } else if (infoType === 'pattern') {
      infoScreen.infoPatternClicked = true;
      this.setState({
        cardPatternTextColor: '#353535',
        PatternCardBorder: '#151515'
      });
    } else if (
      infoType === 'C' ||
      infoType === 'D' ||
      infoType === 'H' ||
      infoType === 'S'
    ) {
      if (infoScreen.numCardsShared == 2) {
        infoScreen.infoSuitClicked = true;
        this.setState({
          cardSuitTextColor: '#353535',
          SuitCardBorder: '#151515'
        });
      }
      this.setState({ showSuitCards: false });
    }
    if(!infoScreen.infoHCPClicked){
      this.setState({
        HCPCardBorder : '#DBFF00' 
      })
    }
    if(!infoScreen.infoPatternClicked){
      this.setState({PatternCardBorder : '#DBFF00' })
    }
    // console.log(tableID +', ' + gamestate.side +','+ infoType);
    HOOL_CLIENT.shareInfo(this.state.tableID, this.state.gamestate.side, infoType);
    infoScreen.showShareCards = false;
    this.setState({ infoScreen, showSuitCards: false });
  };

  NumberShareClicked = () => {
    let infoScreen = this.state.infoScreen;
    if(!infoScreen.infoHCPClicked){
      this.setState((prevState) => {
         if(prevState.HCPCardBorder === '#DBFF00'){ 
          return {HCPCardBorder : '#353535'}
        }else{
          return { HCPCardBorder : '#DBFF00' }
        }
      })
    }
    if(!infoScreen.infoPatternClicked){
      this.setState((prevState) => {
        if(prevState.PatternCardBorder === '#DBFF00'){ 
         return {PatternCardBorder : '#353535'}
       }else{
         return { PatternCardBorder : '#DBFF00' }
       }
     })
    }
    this.setState((prevState) => (
      { showSuitCards: !prevState.showSuitCards}
    ));
    infoScreen.numCardsShared = infoScreen.numCardsShared + 1;
    this.props.updateDeskStateValues({ infoScreen });
  }

  drawCards = (hands, handsDirection) => {
    var svgImg;
    return hands.map((element) => {
      if (element.suit === 'S') {
        svgImg = svgImages.spade;
      } else if (element.suit === 'D') {
        svgImg = svgImages.diamond;
      } else if (element.suit === 'H') {
        svgImg = svgImages.hearts;
      } else if (element.suit === 'C') {
        svgImg = svgImages.clubs;
      }
      var cardSize = this.state.iskibitzer? kibitzerCardWidth:cardWidth
      if(handsDirection == 'E' || handsDirection == 'W'){
        cardSize = this.state.iskibitzer? KibitzerEastWestcardWidth:eastWestcardWidth
      }
      return (
        <View
          style={[
            styles.cardDisplayBodyNorth,
            {
              height: scale(45),
              width: cardSize
            }
          ]}
          key={element.key}
        >
          <View style={
            {
              width: scale(20),
              marginTop: moderateScale(3),
              marginLeft: moderateScale(1),
              alignItems: 'center',
              justifyContent: 'center'
            }
          }>
            <Text style={[styles.SpadeCardNumber, { color: element.SuiteColor}]}>
              {element.rank}
            </Text>
            <SvgXml height={scale(10)} width={scale(9)} xml={svgImg}/>
          </View>
        </View>
      );
    });
  };

  render() {
    return this.state.isValueInitialized ? (
      <>
        <View
          style={{
            width: '100%',
            height: '100%',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              width: scale(61),
              height: scale(61),
              flexDirection: 'column',
            }}
          >
            <Text
              style={[
                styles.DirectionText,
                {
                  alignItems: 'center',
                  color: this.state.infoScreen.DirectionTextPartner,
                  marginBottom: moderateScale(3)
                }
              ]}
            >
              {this.state.myPartnerSeat}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text
                style={[
                  styles.DirectionText,
                  {flex: 0.5, color: this.state.infoScreen.DirectionTextLHO, textAlign: 'center'}
                ]}
              >
                {this.state.myLHOSeat}
              </Text>
              <View style={{width: scale(27), height: scale(27), alignItems: 'center', justifyContent: 'center'}}>
                <SvgXml
                  width={scale(21)}
                  height={scale(21)}
                  xml={this.state.directionPointer}
                />
              </View>
              <Text
                style={[
                  styles.DirectionText,
                  {flex: 0.5, color: this.state.infoScreen.DirectionTextRHO ,}
                ]}
              >
                {this.state.myRHOSeat}
              </Text>
            </View>
            <Text
              style={[
                styles.DirectionText,
                {
                  alignItems: 'center',
                  color: this.state.infoScreen.DirectionTextMySeat,
                  marginTop: moderateScale(3)
                }
              ]}
            >
              {this.state.mySeat}
            </Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            position: 'absolute',
            top: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#0A0A0A',
          }}
        >
          {
            this.state.iskibitzer && 
            <View
              style={{
                height: scale(45),
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignSelf: 'center',
              }}
            >
              {this.drawCards(this.state.northHands, 'N')}
            </View>
          }
          <View style={{ flexDirection: 'column' }}>
            {
              this.state.iskibitzer &&
              <TouchableOpacity
                disabled={this.getChatMessage(this.state.myPartnerSeat)}
                onPress={() => {
                    this.props.showTableMessages()
                }}
              >
                <View style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  alignItems:'center',
                  justifyContent:'center',
                  width:scale(180),
                  height: scale(23) 
                }}>
                  <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.northUserText, letterSpacing: 0.7,
                        
                      }}
                      userName={this.state.gamestate.myPartnername}                    
                      reduceChar={this.getChatMessage(this.state.myPartnerSeat)}
                  />
                </View>
              </TouchableOpacity>
            }
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <View
                style={{ width: scale(90), height: scale(45), backgroundColor: '#151515',borderRightWidth: 1}}
              >
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                  {this.state.infoScreen.northSharedType1}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                  {formatSharedValue(this.state.infoScreen.northSharedValue1)}
                </Text>
              </View>
              <View
                style={{ width: scale(90), height: scale(45), backgroundColor: '#151515' }}
              >
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                  {this.state.infoScreen.northSharedType2}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                  {formatSharedValue(this.state.infoScreen.northSharedValue2)}
                </Text>
              </View>
            </View>
            {
              !this.state.iskibitzer ?
                this.state.gamestate.myPartnername.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName) && this.getChatMessage(this.state.myPartnerSeat)
                ?
                <AddPlayer
                  isNorthInvited={this.props.isNorthInvited}
                  direction={Strings.directionNorth}
                  northUserName={this.props.northUserName}
                  directionNorth={this.props.directionNorth}
                  onAddPlayerClick={this.props.onAddPlayerClick}
                  tableId={this.props.tableID} />
                :
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myPartnerSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    alignItems:'center',
                    justifyContent:'center',
                    width:scale(180),
                    height:scale(23),
                    bottom: scale(1)
                  }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.northUserText, letterSpacing: 0.7,
                        
                      }}
                      userName={this.state.gamestate.myPartnername}
                      reduceChar={this.getChatMessage(this.state.myPartnerSeat)}
                    />
                  </View>
                </TouchableOpacity>
              : null
            }
          </View>
                 
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              top: moderateScale(3)
            }}
          >
            {this.state.infoScreen.showShareCards && !this.state.iskibitzer ? (
              <View
                style={{
                  flexDirection: 'row',
                }}
              >
                <TouchableWithoutFeedback
                  onPress={() => this.infoShare('HCP')}
                  disabled={this.state.infoScreen.infoHCPClicked}
                >
                  <View
                    style={this.state.isTablet ? {...
                      styles.SharePatternCard,
                      height: scale(75),
                      borderColor: this.state.HCPCardBorder,
                      justifyContent: 'center',
                      marginRight: moderateScale(16)
                      }: {...
                        styles.SharePatternCard,
                        height: scale(75),
                          borderColor: this.state.HCPCardBorder,
                          justifyContent:'center',
                          marginRight: moderateScale(16)
                        }}
                  >
                    <Text
                      style={{
                        ...styles.PatternText,
                        color: this.state.cardHCPTextColor,
                        letterSpacing: 0.2,
                        textAlign: 'center',
                      }}
                    >
                      HCP
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.infoShare('pattern')}
                  disabled={this.state.infoScreen.infoPatternClicked}
                >
                  <View
                    style={this.state.isTablet ? {...
                      styles.SharePatternCard,
                      height: scale(75),
                        borderColor: this.state.PatternCardBorder,
                        justifyContent: 'center',
                        marginRight: moderateScale(16)
                      }:{...
                        styles.SharePatternCard,
                        height: scale(75),
                          borderColor: this.state.PatternCardBorder,
                          justifyContent: 'center',
                          marginRight: moderateScale(16)
                        }
                    }
                  >
                    <Text
                      style={{
                        ...styles.PatternText,
                        color: this.state.cardPatternTextColor,
                        textAlign: 'center',
                        top: -2
                      }}
                    >
                      Pattern
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.NumberShareClicked()
                  }}
                  disabled={this.state.infoScreen.infoSuitClicked}
                >
                  <View
                    style={this.state.isTablet ? {...
                      styles.SharePatternCard,
                        borderColor: this.state.SuitCardBorder,
                        justifyContent: 'center',
                        height: scale(75)
                      }: {...
                        styles.SharePatternCard,
                          borderColor: this.state.SuitCardBorder,
                          height: scale(75),
                          justifyContent: 'center'
                        }
                    }
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        textAlign: 'center',
                      }}
                    >
                      <Text
                        style={{
                          ...styles.PatternText,
                          color: this.state.cardSuitTextColor,
                          width: scale(50),
                          letterSpacing: 0.3,
                          textAlign: 'center',
                        }}
                      >
                        {'No.of'}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: scale(55),
                      }}
                    >
                      <View style={{ marginLeft: scale(3) }}>
                        <SvgXml
                          width={scale(10)}
                          height={verticalScale(10)}
                          xml={svgImages.spadesWhite}
                        />
                      </View>
                      <View style={{ marginLeft: scale(3) }}>
                        <SvgXml
                          width={scale(10)}
                          height={verticalScale(10)}
                          xml={svgImages.heartsWhite}
                        />
                      </View>
                      <View style={{ marginLeft: scale(3) }}>
                        <SvgXml
                          width={scale(10)}
                          height={verticalScale(10)}
                          xml={svgImages.diamondsWhite}
                        />
                      </View>
                      <View style={{ marginLeft: scale(2) }}>
                        <SvgXml
                          width={scale(10)}
                          height={verticalScale(10)}
                          xml={svgImages.clubsWhite}
                        />
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            ) : null}
          </View>
        </View>

        <View
          style={{
            height: '100%',
            position: 'absolute',
            left: moderateVerticalScale(2),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {
            this.state.iskibitzer && 
            <View
              style={
                
              {
                width: scale(45),
                justifyContent: 'space-evenly',
              }}
            >
                <View style={{ 
                  transform: [{ rotate: '-90deg' }], 
                  flexDirection: 'row', 
                  alignSelf: 'center',
                  }}>
                  {this.drawCards(this.state.westHands, 'W')}
                </View>
            </View>
          }
          <View
            style={{
              flexDirection: 'row',
            }}
          >
            {
              this.state.iskibitzer &&
              <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myLHOSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
              >
                <View style={{
                  height: scale(180), width: scale(23),
                  alignItems: 'center',justifyContent: 'center',
                  alignSelf:'center',flexDirection:'row'
                }}>
                  <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={true}
                      rightSide={false}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.westUserText, letterSpacing: 0.7,
                        
                      }}
                      crownStyle={{ alignSelf:'center'}}
                      userName={this.state.gamestate.myLHOname}
                      reduceChar={this.getChatMessage(this.state.myLHOSeat)}
                    />
                </View>
              </TouchableOpacity>
            }
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                height: scale(180)
              }}
            >
              <View
                style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: 1 }}
              >
                <View style={{height: scale(45), transform: [{ rotate: '-90deg' }], 
                        alignItems: 'flex-end',}}>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.westSharedType2}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                  {formatSharedValue(this.state.infoScreen.westSharedValue2)}
                </Text>
                </View>
              </View>
              <View
                style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', zIndex:1
                }}
              >
                <View style={{height: scale(45), transform: [{ rotate: '-90deg' }],
                        alignItems: 'flex-end',}}>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.westSharedType1}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}: { ...styles.PaternText, width: scale(90), height: scale(23),}}>
                    {formatSharedValue(this.state.infoScreen.westSharedValue1)}
                  </Text>
                </View>
              </View>
            </View>
            {
              !this.state.iskibitzer ?
                this.state.gamestate.myLHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName) && this.getChatMessage(this.state.myLHOSeat)
                ?
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <AddPlayer
                    isWestInvited={this.props.isWestInvited}
                    direction={Strings.directionWest}
                    directionWest={this.props.directionWest}
                    westUserName={this.props.westUserName}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    tableId={this.props.tableID} />
                </View>
                :
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myLHOSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    height: scale(180),
                    width:scale(23),
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignSelf:'center'
                  }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={true}
                      rightSide={false}
                      textStyle={{
                        ...styles.UserNameText, color: this.state.infoScreen.westUserText, letterSpacing: 0.7,
                      }}
                      crownStyle={{ alignSelf:'center' , 
                      }}
                      userName={this.state.gamestate.myLHOname}
                      reduceChar={this.getChatMessage(this.state.myLHOSeat)}
                    />
                  </View>
                </TouchableOpacity>
                : null
            }
          </View>
        </View>

        <View 
          style={{
            height: '100%',
            position: 'absolute',
            right: moderateVerticalScale(2),
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: '0A0A0A',
          }}
        >
          <View
            style={{
              flexDirection: 'row',
            }}
          >
            {
              !this.state.iskibitzer ?
                this.state.gamestate.myRHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName) && this.getChatMessage(this.state.myRHOSeat)
                ?
                <View  style={{alignItems: 'center', justifyContent: 'center' }}>
                  <AddPlayer
                    isEastInvited={this.props.isEastInvited}
                    direction={Strings.directionEast}
                    directionEast={this.props.directionEast}
                    eastUserName={this.props.eastUserName}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    tableId={this.props.tableID} />
                </View>
                :
                <TouchableOpacity
                    disabled={this.getChatMessage(this.state.myRHOSeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
                >
                  <View style={{
                    height: scale(180), width: scale(23),
                    alignItems: 'center', justifyContent: 'center',
                    flexDirection:'column',alignSelf:'center'
                  }}>
                      <CrownHost
                        hostName={this.props.hostName}
                        shareBidPlay={true}
                        rightSide={true}
                        textStyle={{
                          ...styles.UserNameText, color: this.state.infoScreen.eastUserText, letterSpacing: 0.7,
                        }}
                        crownStyle={{
                         alignSelf:'center'
                        }}
                        userName={this.state.gamestate.myRHOname}
                        reduceChar={this.getChatMessage(this.state.myRHOSeat)}
                      />
                  </View>
              </TouchableOpacity>
              : null
            }

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <View
                style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', borderBottomWidth: 1 }}
              >
                <View style={{
                  height: scale(45), transform: [{ rotate: '90deg' }],
                  alignItems: 'flex-start',
                }}>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.eastSharedType1}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {formatSharedValue(this.state.infoScreen.eastSharedValue1)}
                  </Text>
                </View>
              </View>
              <View
                style={{ width: scale(45), height: scale(90), backgroundColor: '#151515', zIndex: 1 }}
              >
                <View style={{
                  height: scale(45), transform: [{ rotate: '90deg' }],
                  alignItems: 'flex-start',
                }}>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.eastSharedType2}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                    : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {formatSharedValue(this.state.infoScreen.eastSharedValue2)}
                  </Text>
                </View>
              </View>
            </View>
            {
              this.state.iskibitzer &&
            <TouchableOpacity
                  disabled={this.getChatMessage(this.state.myRHOSeat)}
                  onPress={() => {
                      this.props.showTableMessages()
                  }}
            >
              <View style={{
                height: scale(180), width: scale(23),
                alignItems: 'center', justifyContent: 'center',
                flexDirection:'column',alignSelf:'center'
              }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={true}
                    rightSide={true}
                    textStyle={{
                      ...styles.UserNameText, color: this.state.infoScreen.eastUserText, letterSpacing: 0.7,
                    }}
                    crownStyle={{
                      alignSelf:'center'
                    }}
                    userName={this.state.gamestate.myRHOname}
                    reduceChar={this.getChatMessage(this.state.myRHOSeat)}
                  />
              </View>
            </TouchableOpacity>
            }
          </View>
          {
            this.state.iskibitzer && 
            <View
              style={{
                width: scale(45),
                justifyContent: 'space-evenly',
              }}
            >
              <View style={{
                transform: [{ rotate: '90deg' }],
                flexDirection: 'row',
                alignSelf: 'center',
              }}>
                {this.drawCards(this.state.eastHands, 'E')}
              </View>
            </View>
          }
        </View>

        {
          (!this.state.showSuitCards && this.state.iskibitzer) &&
          <View
            style={{
              width: '100%',
              position: 'absolute',
              bottom: 1,
              alignSelf: 'center',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', borderRightWidth: 1 }}>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                  { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                  {this.state.infoScreen.southSharedType1}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                  {formatSharedValue(this.state.infoScreen.southSharedValue1)}
                </Text>
              </View>
              <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515' }}>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                  { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                  {this.state.infoScreen.southSharedType2}
                </Text>
                <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } :
                  { ...styles.PaternText, width: scale(90), height: scale(23) }}>
                  {formatSharedValue(this.state.infoScreen.southSharedValue2)}
                </Text>
              </View>
            </View>
            <TouchableOpacity
                    disabled={this.getChatMessage(this.state.mySeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
            >
              <View style={{ 
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent:'center',
                alignItems:'center', 
                width:scale(180),
                height: scale(18)  }}
              >
                <CrownHost
                  hostName={this.props.hostName}
                  shareBidPlay={false}
                  rightSide={false}
                  textStyle={this.getChatMessage(this.state.mySeat) ? {
                    ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,
                  } : { ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4,width:scale(180)}}
                  userName={this.state.gamestate.myUsername}
                  reduceChar={this.getChatMessage(this.state.mySeat)}
                />
              </View>
            </TouchableOpacity>
            <View
              style={{
                height: scale(45),
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignSelf: 'center',
              }}
            >
              {this.drawCards(this.state.southHands, 'S')}
            </View>
          </View>
        }

        {(!this.state.iskibitzer) &&
          <View
            style={{
              width: '100%',
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}
          >
            {
              !this.state.showSuitCards &&
              <> 
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515', borderRightWidth: 1 }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.southSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(this.state.infoScreen.southSharedValue1)}
                    </Text>
                  </View>
                  <View style={{ width: scale(90), height: scale(45), backgroundColor: '#151515' }}>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.southSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } :
                      { ...styles.PaternText, width: scale(90), height: scale(23) }}>
                      {formatSharedValue(this.state.infoScreen.southSharedValue2)}
                    </Text>
                  </View>
                </View>
              </>     
            }
            <TouchableOpacity
                    disabled={this.getChatMessage(this.state.mySeat)}
                    onPress={() => {
                       this.props.showTableMessages()
                    }}
            >
              <View style={{ 
                      flexDirection: 'row',
                      alignSelf: 'center',
                      justifyContent:'center',
                      alignItems:'center', 
                      width:scale(180),
                      height: scale(23), }}
              >
                <CrownHost
                  hostName={this.props.hostName}
                  shareBidPlay={false}
                  rightSide={false}
                  textStyle={{
                    ...styles.UserNameText, color: this.state.infoScreen.southUserText, letterSpacing: 0.4, 
                  }}
                  userName={this.state.gamestate.myUsername}
                  reduceChar={this.getChatMessage(this.state.mySeat)}
                />
              </View>
            </TouchableOpacity>
            <View
                  style={{
                    height: scale(45),
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignSelf: 'center',
                  }}
                >
                  {this.drawCards(this.state.southHands, 'S')}
            </View>
          
          </View>
        }

        {this.state.showSuitCards &&
          <View
            style={{
              width: '100%',
              height: scale(90),
              position: 'absolute',
              zIndex: 11,
              bottom: this.state.isTablet ? Platform.OS == 'ios' ? moderateVerticalScale(107) : moderateVerticalScale(97) : moderateVerticalScale(62),
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                height: scale(81),
                flexDirection: 'row',
                position: 'absolute',
                paddingTop: moderateScale(3)
              }}
            >
              <TouchableWithoutFeedback
                onPress={() => {
                  this.infoShare('S');
                  let infoScreen = this.state.infoScreen;
                  this.setState({
                    inactSp : '#151515'
                  })
                  infoScreen.numSpShared = true;
                  this.props.updateDeskStateValues({ infoScreen });
                }}
                disabled={this.state.infoScreen.numSpShared}
              >
                <View
                  style={[
                    styles.SharePatternCardSpade,
                    {
                      borderColor: this.state.inactSp,
                      marginRight: moderateScale(16)
                    }
                  ]}
                >
                  <SvgXml
                    height={scale(11)}
                    width={scale(11)}
                    xml={
                      this.state.infoScreen.numSpShared
                        ? svgImages.spadesInactive
                        : svgImages.spade
                    }
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.infoShare('H');
                  let infoScreen = this.state.infoScreen;
                  this.setState({
                    inactHt : '#151515'
                  })
                  infoScreen.numHtShared = true;
                  this.props.updateDeskStateValues({ infoScreen });
                }}
                disabled={this.state.infoScreen.numHtShared}
              >
                <View
                  style={[
                    styles.SharePatternCardHearts,
                    {
                      borderColor: this.state.inactHt,
                      marginRight: moderateScale(16)
                    }
                  ]}
                >
                  <SvgXml
                    height={scale(11)}
                    width={scale(11)}
                    xml={
                      this.state.infoScreen.numHtShared
                        ? svgImages.heartsInactive
                        : svgImages.hearts
                    }
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.infoShare('D');
                  let infoScreen = this.state.infoScreen;
                  this.setState({
                    inactDm : '#151515'
                  })
                  infoScreen.numDmShared = true;
                  this.props.updateDeskStateValues({ infoScreen });
                }}
                disabled={this.state.infoScreen.numDmShared}
              >
                <View
                  style={[
                    styles.SharePatternCardDiamond,
                    {
                      borderColor: this.state.inactDm,
                      marginRight: moderateScale(16)
                    }
                  ]}
                >
                  <SvgXml
                    height={scale(11)}
                    width={scale(11)}
                    xml={
                      this.state.infoScreen.numDmShared
                        ? svgImages.diamondInactive
                        : svgImages.diamond
                    }
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.infoShare('C');
                  let infoScreen = this.state.infoScreen;
                  this.setState({
                    inactCb : '#151515'
                  })
                  infoScreen.numCbShared = true;
                  this.props.updateDeskStateValues({ infoScreen });
                }}
                disabled={this.state.infoScreen.numCbShared}
              >
                <View
                  style={[
                    styles.SharePatternCardClubs,
                    {
                      borderColor: this.state.inactCb
                    }
                  ]}
                >
                  <SvgXml
                    height={scale(11)}
                    width={scale(11)}
                    xml={
                      this.state.infoScreen.numCbShared
                        ? svgImages.clubsInactive
                        : svgImages.clubs
                    }
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>}
      </>
    ) : null;
  }
}

const styles = ScaledSheet.create({
  SharePatternCardSpade: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#C000FF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  SharePatternCardHearts: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#FF0C3E',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  SharePatternCardDiamond: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#04AEFF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  SharePatternCardClubs: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#79E62B',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  PatternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(12),
    fontWeight: '400',
    color: '#FFFFFF'
  },
  SharePatternCard: {
    flexDirection: 'column',
    width: '55@s',
    height: '86@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#DBFF00',
    backgroundColor: '#151515'
  },
  SharePatternCardDisabled: {
    flexDirection: 'column',
    width: '55@s',
    height: '81@s',
    borderRadius: '4@s',
    borderWidth: '1@s',
    borderColor: '#DBFF00',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardDisplayBody: {
    width: 'gameState@vs',
    height: '@s',
    borderRadius: 4,
    borderWidth: '0.5@ms',
    borderColor: '#0a0a0a',
    paddingLeft: 8,
    paddingTop: 3,
    backgroundColor: '#151515'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextActive: {
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D'
  },
  ContractText: {
    position: 'absolute',
    top: '5@ms',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535'
  },
  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
    letterSpacing: 0.7
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingRight: '4@ms'
  },
  ShareTextActive: {
    top: '8@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(27),
    fontWeight: 'normal',
    color: '#6D6D6D'
  },
  ShareText: {
    top: '8@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(27),
    fontWeight: 'normal',
    color: '#353535'
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: moderateScale(3)
  },
  UserNameText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#676767',
    textAlign: 'center',
  },
  LHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  RHOName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  Partner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: 3
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center',
    letterSpacing: normalize(1.92)
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: 4
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },
  DirectionTextMySeat: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextLHO: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextRHO: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  DirectionTextPartner: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center'
  },
  cardDisplayBodyNorth: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
});
