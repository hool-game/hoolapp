import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { normalize } from '../../styles/global';
import { formatSharedValue, nextSide, getUserNameBasedOnHost, HOOL_CLIENT } from '../../shared/util';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateVerticalScale,
  moderateScale
} from 'react-native-size-matters/extend';
import DeviceInfo, { isTablet } from 'react-native-device-info';
import { Strings } from '../../styles/Strings';
import EmptyChatView from '../chat/EmptyChatView';
import AddPlayer from '../../shared/AddPlayer';
import { StackActions, NavigationActions } from '@react-navigation/native';
import CrownHost from '../../shared/CrownHost';

var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height;

const cardDimension = 35;
var cardWidth = scale(cardDimension);
var kibitzerCardWidth = scale(29);
var minasFourTop = (-4 / 360) * ScreenHeight;

//var TotalCardWidth=(535/640*ScreenWidth);
var TotalCardWidthSouth = (533 / 640) * ScreenWidth;
var eastWestcardWidth = scale(25);
var KibitzerEastWestcardWidth = scale(25);

//var WestPalyCardArea=(304/360*ScreenHeight);
//console.log('ScreenHeight '+ ScreenHeight);
//var subScribe=null;

export default class PlayScreen extends React.Component {
  constructor(props) {
    ///navigation.na
    super(props);
    this.state = {
      isTablet: DeviceInfo.isTablet(),
      disablemyCards_C: true,
      disablemyCards_D: true,
      disablemyCards_H: true,
      disablemyCards_S: true,
      disabledummyCards_C: true,
      disabledummyCards_D: true,
      disabledummyCards_H: true,
      disabledummyCards_S: true,
      Numof_C: 0,
      Numberof_D: 0,
      Numberof_H: 0,
      Numof_S: 0,
      trick_Suit: '',
      cardsCnt: 0,
      output: '',
      tableID: '',
      DummySeat: '',
      DummyPlayed: false,
      directionImage: svgImages.pointerSouth,
      northUser: '',
      eastUser: '',
      southUser: '',
      westUser: '',
      northSharedType1: '',
      northSharedValue1: '',
      northSharedType2: '',
      northSharedValue2: '',
      eastSharedType1: '',
      eastSharedValue1: '',
      eastSharedType2: '',
      eastSharedValue2: '',
      southSharedType1: '',
      southSharedValue1: '',
      southSharedType2: '',
      southSharedValue2: '',
      westSharedType1: '',
      westSharedValue1: '',
      westSharedType2: '',
      westSharedValue2: '',
      cardHCPTextColor: '#FFFFFF',
      cardPatternTextColor: '#FFFFFF',
      cardSuitTextColor: '#FFFFFF',
      DirectionTextPartner: styles.DirectionText,
      DirectionTextLHO: styles.DirectionText,
      DirectionTextRHO: styles.DirectionText,
      DirectionTextMySeat: styles.DirectionText,
      northUserText: '#6D6D6D',
      eastUserText: '#6D6D6D',
      southUserText: '#6D6D6D',
      westUserText: '#6D6D6D',
      winnerSide: '',
      winnerLevel: '',
      winnerSuit: '',
      winnerSuitImage: '',
      winnerTextColor: '',
      winnerdblRedbl: '',
      PartnerPlayed: false,
      LHOPlayed: false,
      RHOPlayed: false,
      MyCard: false,
      undoLHO: false,
      undoRHO: false,
      disableTkBk: true,
      undoAsked: false,
      undoCardSide: '',
      undoCardSuit: '',
      undoCardRank: '',
      reqUndoimg: svgImages.undo,
      undoMySeatimg: require('../../assets/images/takeback_black.png'),
      undoLHOSeatimg: require('../../assets/images/takeback_black.png'),
      undoPartnerSeatimg: require('../../assets/images/takeback_black.png'),
      undoRHOSeatimg: require('../../assets/images/takeback_black.png'),
      reqUndo: svgImages.undo,
      LHORank: '',
      RHORank: '',
      PartnerRank: '',
      myCardStyle: '',
      myCardNumberstyle: '',
      myRank: '',
      mySuit: svgImages.cheat_Sheet,
      PartnerSuit: svgImages.cheat_Sheet,
      LHOSuit: svgImages.cheat_Sheet,
      RHOSuit: svgImages.cheat_Sheet,
      TrickScoreNS: '00',
      TrickScoreEW: '00',
      dealScoreNS: '00',
      dealScoreEW: '00',
      runningScoreNS: '00',
      runningScoreEW: '00',

      dummycardsdrawn: false,
      isSouthDummy: false,
      Numberof_D: 0,
      NumberofDummy_C: 0,
      NumberofDummy_D: 0,
      NumberofDummy_H: 0,
      NumberofDummy_S: 0,
      CardBorderS: '#676767',
      CardBorderN: '#676767',
      CardBorderE: '#676767',
      CardBorderW: '#676767',
      NSStyle: styles.ContractText,
      EWStyle: styles.ContractText,
      NSScoreStyle: styles.ScoreText,
      EWScoreStyle: styles.ScoreText,
      playTurn: '',
      cancelAnimateImg: false,
      playTimer: svgImages.joinSymbolPlay,
      northBidInfo: false,
      southBidInfo: false,
      westBidInfo: false,
      eastBidInfo: false,
    };
    //this.dynamicStates();
  }

  componentDidMount() {
    //  console.log('ShareInfo componentDidMount called');
    this.loadInitialScreenValues();
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.tableID !== prevProps.tableID) {
      this.updateStateValues({ tableID: this.props.tableID });
    }
    if (this.props.gamestate !== prevProps.gamestate) {
      this.updateStateValues({ gamestate: this.props.gamestate });
    }
    if (this.props.northAvtar !== prevProps.northAvtar) {
      this.updateStateValues({ northAvtar: this.props.northAvtar });
    }
    if (this.props.eastAvtar !== prevProps.eastAvtar) {
      this.updateStateValues({ eastAvtar: this.props.eastAvtar });
    }
    if (this.props.southAvtar !== prevProps.southAvtar) {
      this.updateStateValues({ southAvtar: this.props.southAvtar });
    }
    if (this.props.westAvtar !== prevProps.westAvtar) {
      this.updateStateValues({ westAvtar: this.props.westAvtar });
    }
    if (this.props.northUser !== prevProps.northUser) {
      this.updateStateValues({ northUser: this.props.northUser });
    }
    if (this.props.eastUser !== prevProps.eastUser) {
      this.updateStateValues({ eastUser: this.props.eastUser });
    }
    if (this.props.southUser !== prevProps.southUser) {
      this.updateStateValues({ southUser: this.props.southUser });
    }
    if (this.props.westUser !== prevProps.westUser) {
      this.updateStateValues({ westUser: this.props.westUser });
    }
    if (this.props.iskibitzer !== prevProps.iskibitzer) {
      this.updateStateValues({ iskibitzer: this.props.iskibitzer });
    }
    if (this.props.infoScreen !== prevProps.infoScreen) {
      this.updateStateValues({ infoScreen: this.props.infoScreen });
    }
    if (this.props.bidInfoScreen !== prevProps.bidInfoScreen) {
      this.updateStateValues({ bidInfoScreen: this.props.bidInfoScreen });
    }
    if (this.props.playScreen !== prevProps.playScreen) {
      //  console.log('PlayScreen Props Updated');
      // let playScreen = this.props.playScreen;
      // playScreen = this.updateCardPlayArea(playScreen);
      this.updateStateValues({ playScreen: this.props.playScreen });
    }
    if (this.props.tricks !== prevProps.tricks) {
      this.updateStateValues({ tricks: this.props.tricks });
    }

    if (this.props.eastUser == Strings.open && this.props.westUser == Strings.open
      && this.props.northUser == Strings.open && this.props.southUser == Strings.open 
      && this.props.iskibitzer) {
        HOOL_CLIENT.leaveTable(this.state.tableID);
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })]
        });
        this.props.navigation.dispatch(resetAction);
    }
  }

  componentWillUnmount() {
    // AppState.removeEventListener('change', this._handleAppStateChange);
    // // Remove the event listener
    // if (this.focusListener != null && this.focusListener.remove) {
    //   this.focusListener.remove();
    // }
    // if (this.infosharelsn !== undefined) {
    //   this.infosharelsn.remove();
    // //  console.log('shareinfo lsn removed');
    // }
    //  console.log('shareinfo closed');
  }

  updateStateValues(stateObj, callback) {
    this.setState(stateObj, callback);
  }

  async getTableInfo() {
    TotalCardWidthSouth = verticalScale(533);
    let gamestate = this.state.gamestate;
    let infoScreen = this.state.infoScreen;
    let playScreen = this.state.playScreen;
    if(this.state.iskibitzer){
      playScreen.EAST_PLAY_AREA = 13 * KibitzerEastWestcardWidth;
      playScreen.WEST_PLAY_AREA = 13 * KibitzerEastWestcardWidth;
    }else{
      playScreen.EAST_PLAY_AREA = 13 * eastWestcardWidth;
      playScreen.WEST_PLAY_AREA = 13 * eastWestcardWidth;
    }
    //If the device length is less than total card size then use the vertical scale proportion
    if (TotalCardWidthSouth < 13 * scale(cardDimension))
      cardWidth = verticalScale(cardDimension);
    
    playScreen.TotalCardWidthSouth = 13 * (this.state.iskibitzer? kibitzerCardWidth: cardWidth);
    playScreen.TotalCardWidthNorth = 13 * (this.state.iskibitzer? kibitzerCardWidth: cardWidth);
    //  console.log(' gamestate as below');
    console.log(TotalCardWidthSouth);
    //  console.log(gamestate);
    let myHand = gamestate.hands;
    myHand.forEach((h, key) => {
      //  console.log(h.infoShared);
      if (key === this.state.gamestate.mySeat) {
        if (key === gamestate.side)
          playScreen = this.displayCards(h.cards, playScreen, true);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          //console.log(suitType);
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.southSharedType1 = suitType;
          infoScreen.southSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.southSharedType2 = suitType;
          infoScreen.southSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.southSharedType1 = suitType;
          infoScreen.southSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.gamestate.myLHOSeat) {
        // if (key === gamestate.side)
        //   playScreen = this.displayCards(h.cards, playScreen);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          //  console.log(suitType);
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.westSharedType1 = suitType;
          infoScreen.westSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.westSharedType2 = suitType;
          infoScreen.westSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.westSharedType1 = suitType;
          infoScreen.westSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.gamestate.myPartnerSeat) {
        //  console.log('My Partner');
        // if (key === gamestate.side)
        //   playScreen = this.displayCards(h.cards, playScreen);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          //  console.log(suitType);
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.northSharedType1 = suitType;
          infoScreen.northSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.northSharedType2 = suitType;
          infoScreen.northSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.northSharedType1 = suitType;
          infoScreen.northSharedValue1 = h.infoShared[0].value;
        }
      } else if (key === this.state.gamestate.myRHOSeat) {
        // if (key === gamestate.side)
        //   playScreen = this.displayCards(h.cards, playScreen);

        if (h.infoShared.length > 1) {
          var suitType = h.infoShared[0].type;
          //  console.log(suitType);
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType1 = suitType;
          infoScreen.eastSharedValue1 = h.infoShared[0].value;
          suitType = h.infoShared[1].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType2 = suitType;
          infoScreen.eastSharedValue2 = h.infoShared[1].value;
        } else if (h.infoShared.length > 0) {
          var suitType = h.infoShared[0].type;
          if (suitType === Strings.d) {
            suitType = 'Diamonds';
          } else if (suitType === Strings.c) {
            suitType = 'Clubs';
          } else if (suitType === Strings.h) {
            suitType = 'Hearts';
          } else if (suitType === Strings.directionSouth) {
            suitType = 'Spades';
          }
          infoScreen.eastSharedType1 = suitType;
          infoScreen.eastSharedValue1 = h.infoShared[0].value;
        }
      }
    });
    playScreen = this.updateCardPlayArea(playScreen);

    this.props.updateDeskStateValues({ playScreen }, () => {
      this.setDirection('');
    });
  }

  updateCardPlayArea(playScreen) {
    //To Calculate EAST Card Side Play area
    let eastCards = [];
    if ('RHO' === playScreen.DummySeat && playScreen.dummyHands.length !== 0) {
      eastCards = playScreen.dummycardsShown;
    } else if (
      playScreen.eastHands != undefined &&
      playScreen.eastHands.length !== 0
    ) {
      eastCards = playScreen.eastCardsShown;
    }
    let eastCardOccurances = this.cardOccurrences(eastCards);
    console.log(eastCards);
    let eastCardsLength = eastCardOccurances.flex ? eastCardOccurances.flex : 0;
    if(this.state.iskibitzer){
      playScreen.EAST_PLAY_AREA =
      13 * eastWestcardWidth - (13 - eastCardsLength) * KibitzerEastWestcardWidth;
    }else{
      playScreen.EAST_PLAY_AREA =
      13 * eastWestcardWidth - (13 - eastCardsLength) * eastWestcardWidth;
    }    
    //To Calculate WEST Card Side Play area
    let westCards = [];
    if ('RHO' === playScreen.DummySeat && playScreen.dummyHands.length !== 0) {
      westCards = playScreen.dummycardsShown;
    } else if (
      playScreen.westHands != undefined &&
      playScreen.westHands.length !== 0
    ) {
      westCards = playScreen.westCardsShown;
    }
    let westCardOccurances = this.cardOccurrences(westCards);
    console.log(westCards);
    let westCardsLength = westCardOccurances.flex ? westCardOccurances.flex : 0;
    if(this.state.iskibitzer){
      playScreen.WEST_PLAY_AREA =
      13 * eastWestcardWidth - (13 - westCardsLength) * KibitzerEastWestcardWidth;
    }else{
      playScreen.WEST_PLAY_AREA =
      13 * eastWestcardWidth - (13 - westCardsLength) * eastWestcardWidth;
    }
    //To Calculate NORTH Card Side Play area
    let northCards = [];
    if (
      playScreen.DummySeat === 'Partner' &&
      playScreen.dummyHands.length !== 0
    ) {
      northCards = playScreen.dummycardsShown;
    } else if (
      playScreen.northHands != undefined &&
      playScreen.northHands.length !== 0
    ) {
      northCards = playScreen.northCardsShown;
    }
    let northCardOccurances = this.cardOccurrences(northCards);
    console.log(northCardOccurances);
    let northCardsLength = northCardOccurances.flex
      ? northCardOccurances.flex
      : 0;
    if(this.state.iskibitzer){
      playScreen.TotalCardWidthNorth =
      13 * kibitzerCardWidth - (13 - northCardsLength) * kibitzerCardWidth;
    }else{
      playScreen.TotalCardWidthNorth =
      13 * cardWidth - (13 - northCardsLength) * cardWidth;
    }
   
    console.log('TotalCardWidthNorth', playScreen.TotalCardWidthNorth);
    //adjust cards width area
    let southCardOccurances = this.cardOccurrences(playScreen.cardsShown);
    console.log(southCardOccurances);
    let southCardsLength = southCardOccurances.flex
      ? southCardOccurances.flex
      : 0;
    if(this.state.iskibitzer){
      playScreen.TotalCardWidthSouth =
      13 * kibitzerCardWidth - (13 - southCardsLength) * kibitzerCardWidth;
    }else{
      playScreen.TotalCardWidthSouth =
      13 * cardWidth - (13 - southCardsLength) * cardWidth;
    }
    return playScreen;
  }

  displayCards(hand, playScreen, isMySeat) {
    var cardSide = [];
    var idx = 0;
    console.log('south cards');
    console.log(hand);
    playScreen.cardsShown = [];
    if (isMySeat) {
      playScreen.Numberof_S = 0;
      playScreen.Numberof_D = 0;
      playScreen.Numberof_H = 0;
      playScreen.Numberof_C = 0;
    }

    hand.forEach(card => {
      // //  console.log(card.suit);
      if (card.suit === Strings.directionSouth) {
        cardSide.push({
          rank: card.rank,
          suit: Strings.directionSouth,
          key: card.rank + Strings.directionSouth,
          SuiteColor: '#C000FF',
          idx: idx
        });
        if (isMySeat) playScreen.Numberof_S = playScreen.Numberof_S + 1;
      } else if (card.suit === Strings.d) {
        cardSide.push({
          rank: card.rank,
          suit: Strings.d,
          key: card.rank + Strings.d,
          SuiteColor: '#04AEFF',
          idx: idx
        });
        if (isMySeat) playScreen.Numberof_D = playScreen.Numberof_D + 1;
      } else if (card.suit === Strings.h) {
        cardSide.push({
          rank: card.rank,
          suit: Strings.h,
          key: card.rank + Strings.h,
          SuiteColor: '#FF0C3E',
          idx: idx
        });
        if (isMySeat) playScreen.Numberof_H = playScreen.Numberof_H + 1;
      } else if (card.suit === Strings.c) {
        cardSide.push({
          rank: card.rank,
          suit: Strings.c,
          key: card.rank + Strings.c,
          SuiteColor: '#79E62B',
          idx: idx
        });
        if (isMySeat) playScreen.Numberof_C = playScreen.Numberof_C + 1;
      }
      playScreen.cardsShown[idx] = 'flex';
      idx++;
    });
    playScreen.hands = cardSide;
    //  console.log('card showd');
    //  console.log(playScreen.cardsShown);
    return playScreen;
  }

  setDirection(seat) {
    //  console.log(seat, 'seat direction');
    let playScreen = { ...this.state.playScreen };
    playScreen.DirectionTextMySeat = styles.DirectionText;
    playScreen.DirectionTextLHO = styles.DirectionText;
    playScreen.DirectionTextRHO = styles.DirectionText;
    playScreen.DirectionTextPartner = styles.DirectionText;
    if (playScreen.trick_Suit === '') {
      playScreen.disablemyCards_C = true;
      playScreen.disablemyCards_D = true;
      playScreen.disablemyCards_H = true;
      playScreen.disablemyCards_S = true;
      playScreen.disabledummyCards_C = true;
      playScreen.disabledummyCards_D = true;
      playScreen.disabledummyCards_H = true;
      playScreen.disabledummyCards_S = true;
    }
    let gamestate = this.state.gamestate;
    if (seat === '') {
      seat = gamestate.StartSeat;
      playScreen.playTurn = gamestate.StartSeat;
    } else if (seat !== '') {
      playScreen.playTurn = seat;
    }
    console.log('Start Seat');
    console.log(seat, playScreen.playTurn);
    if (seat === gamestate.mySeat) {
      playScreen.DirectionTextMySeat = styles.DirectionTextActive;
      playScreen.southUserText = '#DBFF00';
      playScreen.CardBorderS = '#DBFF00';
      playScreen.directionImage = svgImages.pointerSouth;
      if (!playScreen.isSouthDummy && playScreen.trick_Suit === '') {
        playScreen.disablemyCards_C = false;
        playScreen.disablemyCards_D = false;
        playScreen.disablemyCards_H = false;
        playScreen.disablemyCards_S = false;
      }
    } else if (seat === gamestate.myLHOSeat) {
      playScreen.DirectionTextLHO = styles.DirectionTextActive;
      playScreen.westUserText = '#DBFF00';
      playScreen.CardBorderW = '#DBFF00';
      playScreen.directionImage = svgImages.pointerLeft;
    } else if (seat === gamestate.myRHOSeat) {
      playScreen.DirectionTextRHO = styles.DirectionTextActive;
      playScreen.eastUserText = '#DBFF00';
      playScreen.CardBorderE = '#DBFF00';
      playScreen.directionImage = svgImages.pointerRight;
    } else if (
      seat === gamestate.myPartnerSeat &&
      playScreen.trick_Suit === ''
    ) {
      playScreen.DirectionTextPartner = styles.DirectionTextActive;
      playScreen.northUserText = '#DBFF00';
      playScreen.CardBorderN = '#DBFF00';
      playScreen.directionImage = svgImages.pointerNorth;
      if ('Partner' === playScreen.DummySeat && !playScreen.isSouthDummy) {
        playScreen.disabledummyCards_C = false;
        playScreen.disabledummyCards_D = false;
        playScreen.disabledummyCards_H = false;
        playScreen.disabledummyCards_S = false;
      }
    }
    //  console.log('side', seat);
    if (seat === Strings.directionEast || seat === Strings.directionWest) {
      playScreen.NSStyle = styles.ContractText;
      playScreen.NSScoreStyle = styles.ScoreText;
      playScreen.NSSharingBottom = '#0a0a0a';
      playScreen.EWStyle = styles.ContractTextActive;
      playScreen.EWScoreStyle = styles.ScoreTextActive;
      playScreen.EWSharingBottom = '#DBFF00';
    } else if (seat === Strings.directionNorth || seat === Strings.directionSouth) {
      playScreen.NSStyle = styles.ContractTextActive;
      playScreen.NSScoreStyle = styles.ScoreTextActive;
      playScreen.NSSharingBottom = '#DBFF00';
      playScreen.EWStyle = styles.ContractText;
      playScreen.EWScoreStyle = styles.ScoreText;
      playScreen.EWSharingBottom = '#0a0a0a';
    }
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  loadInitialScreenValues() {
    this.setState({ ...this.props }, () => {
      let gamestate = { ...this.state.gamestate };
      let bidInfoScreen = { ...this.state.bidInfoScreen };
      //console.log(gamestate.side);
      this.getTableInfo();
      if (
        gamestate.contractinfo !== undefined &&
        gamestate.contractinfo !== []
      ) {
        var bid_suit_image = '';
        var bid_suit_color = '';
        var bid_suit_text_color = '';
        var bid_suit_border_color = '';
        var bid_winnerTextColor = '';
        var bid_level = gamestate.contractinfo.level;
        if (gamestate.contractinfo.suit === Strings.c) {
          bid_suit_image = require('../../assets/images/clubs.png');
          bid_suit_color = styles.clubsImageActive;
          bid_suit_text_color = styles.bidCircleTextClubsActive;
          bid_suit_border_color = styles.bidCirclePositionClubsActive;
          bid_winnerTextColor = '#79E62B';
        } else if (gamestate.contractinfo.suit === Strings.d) {
          bid_suit_image = require('../../assets/images/diamond.png');
          bid_suit_color = styles.diamondImageActive;
          bid_suit_text_color = styles.bidCircleTextDiamondActive;
          bid_suit_border_color = styles.bidCirclePositionDiamondActive;
          bid_winnerTextColor = '#04AEFF';
        } else if (gamestate.contractinfo.suit === Strings.h) {
          bid_suit_image = require('../../assets/images/hearts.png');
          bid_suit_color = styles.heartsImageActive;
          bid_suit_text_color = styles.bidCircleTextHeartsActive;
          bid_suit_border_color = styles.bidCirclePositionHeartsActive;
          bid_winnerTextColor = '#FF0C3E';
        } else if (gamestate.contractinfo.suit === Strings.directionSouth) {
          bid_suit_image = require('../../assets/images/spade.png');
          bid_suit_color = styles.spadeImageActive;
          bid_suit_text_color = styles.bidCircleTextSpadeActive;
          bid_suit_border_color = styles.bidCirclePositionSpadeActive;
          bid_winnerTextColor = '#C000FF';
        } else if (gamestate.contractinfo.suit === 'NT') {
          bid_suit_image = '';
          bid_suit_color = styles.bidCircleTextAssActive;
          bid_suit_text_color = styles.bidCircleTextNTActive;
          bid_suit_border_color = styles.bidCirclePositionNTActive;
          bid_level = gamestate.contractinfo.level + 'NT';
          bid_winnerTextColor = '#FFE81D';
        }

        bidInfoScreen.winnerLevel = bid_level;
        bidInfoScreen.winnerSide = gamestate.contractinfo.declarer;
        bidInfoScreen.winnerSuit = gamestate.contractinfo.suit;
        bidInfoScreen.winnerSuitImage = bid_suit_image;
        bidInfoScreen.winnerTextColor = bid_winnerTextColor;

        if (gamestate.contractinfo.double === 'true') {
          bidInfoScreen.winnerdblRedbl = 'x2';
        }
        if (gamestate.contractinfo.redouble === 'true') {
          bidInfoScreen.winnerdblRedbl = 'x4';
        }
        this.props.updateDeskStateValues({ bidInfoScreen }, () => {});
      }
      if (
        gamestate.tricks !== undefined &&
        gamestate.contractinfo !== undefined
      ) {
        const interval = setTimeout(() => {
          let myHand = gamestate.hands;
          if (gamestate.tricks.length > 0) {
            myHand.forEach((h, key) => {
              if (key === Strings.directionSouth) {
                if (gamestate.contractinfo.declarer === Strings.directionNorth) {
                  this.displayCardsDummy(h);
                }
              } else if (key === Strings.directionWest) {
                if (gamestate.contractinfo.declarer === Strings.directionEast) {
                  this.displayCardsDummy(h);
                }
              } else if (key === Strings.directionNorth) {
                if (gamestate.contractinfo.declarer === Strings.directionSouth) {
                  this.displayCardsDummy(h);
                }
              } else if (key === Strings.directionEast) {
                if (gamestate.contractinfo.declarer === Strings.directionWest) {
                  this.displayCardsDummy(h);
                }
              }
            });
          }
          for (var ir = 0; ir < gamestate.tricks.length; ir++) {
            var trickcards = gamestate.tricks[ir].cards;
            if (trickcards.length < 4) {
              for (let trickcard of trickcards) {
                this.cardsPlayed(trickcard);
              }
            } else {
              for (let trickcard of trickcards) {
                if (trickcard.side === this.state.gamestate.mySeat) {
                  // if(trickcard.suit===Strings.c)
                  // {
                  //     Numberof_C = Numberof_C-1;
                  // }
                  // else if(trickcard.suit===Strings.d)
                  // {
                  //     Numberof_D = Numberof_D-1;
                  // }
                  // else if(trickcard.suit===Strings.h)
                  // {
                  //     Numberof_H = Numberof_H-1;
                  // }
                  // else if(trickcard.suit===Strings.directionSouth)
                  // {
                  //     Numberof_S = Numberof_S-1;
                  // }
                  // TotalCardWidthSouth=TotalCardWidthSouth-fortyFiveHeight;
                } else if (
                  trickcard.side === this.state.gamestate.myPartnerSeat
                ) {
                  // if(trickcard.suit===Strings.c)
                  // {
                  //     this.setState({NumberofDummy_C : this.state.NumberofDummy_C-1});
                  // }
                  // else if(trickcard.suit===Strings.d)
                  // {
                  //     this.setState({NumberofDummy_D : this.state.NumberofDummy_D-1});
                  // }
                  // else if(trickcard.suit===Strings.h)
                  // {
                  //     this.setState({NumberofDummy_H : this.state.NumberofDummy_H-1});
                  // }
                  // else if(trickcard.suit===Strings.directionSouth)
                  // {
                  //     this.setState({NumberofDummy_S : this.state.NumberofDummy_S-1});
                  // }
                }
              }
            }
          }
          console.log(gamestate.tableinfo.turn, 'gamestate turn');
          this.setDirection(gamestate.tableinfo.turn);
          clearInterval(interval);
        }, 1000);
      }
      const interval = setInterval(() => {
        this.setState({ isValueInitialized: true });
        clearInterval(interval);
      }, 500);
    });
  }

  cardsPlayed(e) {
    console.log(e.side + ' has played card ' + e.rank + e.suit);
    let playScreen = { ...this.state.playScreen };
    if (playScreen.trick_Suit === '') {
      playScreen.trick_Suit = e.suit;
      playScreen.cardsCnt = 0;
    }

    playScreen.cardsCnt = playScreen.cardsCnt + 1;
    playScreen.disableTkBk = true;
    playScreen.undoCardSide = e.side;
    playScreen.undoCardSuit = e.suit;
    playScreen.undoCardRank = e.rank;
    playScreen.reqUndoimg = svgImages.undo;
    if (e.side === this.state.gamestate.mySeat) {
      playScreen.MyCard = true;
      playScreen.myRank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.myCardStyle = styles.playCardClubs;
        playScreen.myCardNumberstyle = styles.playCardNumberClubs;
        playScreen.mySuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.myCardStyle = styles.playCardDiamond;
        playScreen.myCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.mySuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.myCardStyle = styles.playCardHearts;
        playScreen.myCardNumberstyle = styles.playCardNumberHearts;
        playScreen.mySuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.myCardStyle = styles.playCardSpade;
        playScreen.myCardNumberstyle = styles.playCardNumberSpade;
        playScreen.mySuit = svgImages.spade;
      }
      var val = e.rank + e.suit;
      if (this.state.iskibitzer) {
        var index = playScreen.hands.findIndex(function (item, i) {
          return item.key === val;
        });
        console.log(val, index, playScreen.hands);
        playScreen.cardsShown[index] = 'none';
      }
      if (playScreen.isSouthDummy) {
        var index = playScreen.hands.findIndex(function (item, i) {
          return item.key === val;
        });
        //console.log('south dummy idx ' + index);
        // var arrcddummy=this.state.dummycardsShown;
        playScreen.cardsShown[index] = 'none';
        const occurrences = playScreen.cardsShown.reduce(function (acc, curr) {
          return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
        }, {});
        if(this.state.iskibitzer){
          playScreen.TotalCardWidthSouth =
          playScreen.TotalCardWidthSouth - kibitzerCardWidth;
        }else{
          playScreen.TotalCardWidthSouth =
          playScreen.TotalCardWidthSouth - cardWidth;
        }
      } else {
        playScreen.disableTkBk = false;
        playScreen.reqUndoimg = svgImages.undoActive;
      }
      if (playScreen.cardsCnt < 4) {
        playScreen.DirectionTextLHO = styles.DirectionTextActive;
        playScreen.westUserText = '#DBFF00';
        playScreen.CardBorderW = '#DBFF00';
        playScreen.directionImage = svgImages.pointerLeft;
        playScreen.DirectionTextMySeat = styles.DirectionText;
        playScreen.southUserText = '#6D6D6D';
        playScreen.disablemyCards_C = true;
        playScreen.disablemyCards_D = true;
        playScreen.disablemyCards_H = true;
        playScreen.disablemyCards_S = true;
      }
    } else if (e.side === this.state.gamestate.myPartnerSeat) {
      playScreen.PartnerPlayed = true;
      playScreen.PartnerRank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.PartnerCardStyle = styles.playCardClubs;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberClubs;
        playScreen.PartnerSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.PartnerCardStyle = styles.playCardDiamond;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.PartnerSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.PartnerCardStyle = styles.playCardHearts;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberHearts;
        playScreen.PartnerSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.PartnerCardStyle = styles.playCardSpade;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberSpade;
        playScreen.PartnerSuit = svgImages.spade;
      }
      if (playScreen.cardsCnt < 4) {
        playScreen.DirectionTextRHO = styles.DirectionTextActive;
        playScreen.eastUserText = '#DBFF00';
        playScreen.CardBorderE = '#DBFF00';
        playScreen.directionImage = svgImages.pointerRight;
        playScreen.DirectionTextPartner = styles.DirectionText;
        playScreen.northUserText = '#6D6D6D';
      }
      var val = e.rank + e.suit;
      var index = playScreen.northHands.findIndex(function (item, i) {
        return item.key === val;
      });
      if (this.state.iskibitzer || index !== -1) {
        console.log(val, index, playScreen.northHands);
        playScreen.northCardsShown[index] = 'none';
        playScreen.TotalCardWidthNorth =
          playScreen.TotalCardWidthNorth - kibitzerCardWidth;
      }
      var index = playScreen.dummyHands.findIndex(function (item, i) {
        return item.key === val;
      });
      if (playScreen.isSouthDummy) {
        playScreen.dummycardsShown[index] = 'none';
        const occurrences = playScreen.dummycardsShown.reduce(function (
          acc,
          curr
        ) {
          return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
        },
        {});
      }
      console.log(this.state.gamestate.DummySeat, e.side,'undo check');
      if ('Partner' === playScreen.DummySeat && !playScreen.isSouthDummy) {
        playScreen.disableTkBk = false;
        playScreen.reqUndoimg = svgImages.undoActive;
      }
    } else if (e.side === this.state.gamestate.myLHOSeat) {
      playScreen.LHOPlayed = true;
      playScreen.LHORank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.LHOCardStyle = styles.playCardClubs;
        playScreen.LHOCardNumberstyle = styles.playCardNumberClubs;
        playScreen.LHOSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.LHOCardStyle = styles.playCardDiamond;
        playScreen.LHOCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.LHOSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.LHOCardStyle = styles.playCardHearts;
        playScreen.LHOCardNumberstyle = styles.playCardNumberHearts;
        playScreen.LHOSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.LHOCardStyle = styles.playCardSpade;
        playScreen.LHOCardNumberstyle = styles.playCardNumberSpade;
        playScreen.LHOSuit = svgImages.spade;
      }
      var val = e.rank + e.suit;
      if (this.state.iskibitzer || playScreen.westHands.length !== 0) {
        var index = playScreen.westHands.findIndex(function (item, i) {
          return item.key === val;
        });
        console.log(val, index, playScreen.westHands);
        playScreen.westCardsShown[index] = 'none';
      }
      if ('LHO' === playScreen.DummySeat) {
        var index = playScreen.dummyHands.findIndex(function (item, i) {
          return item.key === val;
        });
        playScreen.dummycardsShown[index] = 'none';
        playScreen.DummyPlayed = true;
      }
      if(this.state.iskibitzer){
        playScreen.WEST_PLAY_AREA = playScreen.WEST_PLAY_AREA - KibitzerEastWestcardWidth;
      }else{
        playScreen.WEST_PLAY_AREA = playScreen.WEST_PLAY_AREA - eastWestcardWidth;
      }
      if (playScreen.cardsCnt < 4) {
        playScreen.DirectionTextPartner = styles.DirectionTextActive;
        playScreen.northUserText = '#DBFF00';
        playScreen.CardBorderN = '#DBFF00';
        playScreen.directionImage = svgImages.pointerNorth;
        playScreen.DirectionTextLHO = styles.DirectionText;
        playScreen.westUserText = '#6D6D6D';
        playScreen.playTurn = this.state.gamestate.myPartnerSeat;
      }
      console.log('LHO Played', playScreen.DummySeat, playScreen.isSouthDummy);
      if ('Partner' === playScreen.DummySeat && !playScreen.isSouthDummy) {
        if (playScreen.trick_Suit === Strings.c) {
          console.log('NumberofDummy_C ' + playScreen.NumberofDummy_C);
          if (Number(playScreen.NumberofDummy_C) > 0) {
            playScreen.disabledummyCards_C = false;
            playScreen.disabledummyCards_D = true;
            playScreen.disabledummyCards_H = true;
            playScreen.disabledummyCards_S = true;
          } else {
            playScreen.disabledummyCards_C = false;
            playScreen.disabledummyCards_D = false;
            playScreen.disabledummyCards_H = false;
            playScreen.disabledummyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.d) {
          console.log('NumberofDummy_D ' + playScreen.NumberofDummy_D);
          if (Number(playScreen.NumberofDummy_D) > 0) {
            playScreen.disabledummyCards_D = false;
            playScreen.disabledummyCards_C = true;
            playScreen.disabledummyCards_H = true;
            playScreen.disabledummyCards_S = true;
          } else {
            playScreen.disabledummyCards_C = false;
            playScreen.disabledummyCards_D = false;
            playScreen.disabledummyCards_H = false;
            playScreen.disabledummyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.h) {
          console.log('NumberofDummy_H ' + playScreen.NumberofDummy_H);
          if (Number(playScreen.NumberofDummy_H) > 0) {
            playScreen.disabledummyCards_H = false;
            playScreen.disabledummyCards_D = true;
            playScreen.disabledummyCards_C = true;
            playScreen.disabledummyCards_S = true;
          } else {
            playScreen.disabledummyCards_C = false;
            playScreen.disabledummyCards_D = false;
            playScreen.disabledummyCards_H = false;
            playScreen.disabledummyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.directionSouth) {
          console.log('NumberofDummy_S ' + playScreen.NumberofDummy_S);
          if (Number(playScreen.NumberofDummy_S) > 0) {
            playScreen.disabledummyCards_S = false;
            playScreen.disabledummyCards_D = true;
            playScreen.disabledummyCards_H = true;
            playScreen.disabledummyCards_C = true;
          } else {
            playScreen.disabledummyCards_C = false;
            playScreen.disabledummyCards_D = false;
            playScreen.disabledummyCards_H = false;
            playScreen.disabledummyCards_C = false;
          }
        }
      }
    } else if (e.side === this.state.gamestate.myRHOSeat) {
      //  console.log('inside rho ' + playScreen.trick_Suit);
      playScreen.RHOPlayed = true;
      playScreen.RHORank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.RHOCardStyle = styles.playCardClubs;
        playScreen.RHOCardNumberstyle = styles.playCardNumberClubs;
        playScreen.RHOSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.RHOCardStyle = styles.playCardDiamond;
        playScreen.RHOCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.RHOSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.RHOCardStyle = styles.playCardHearts;
        playScreen.RHOCardNumberstyle = styles.playCardNumberHearts;
        playScreen.RHOSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.RHOCardStyle = styles.playCardSpade;
        playScreen.RHOCardNumberstyle = styles.playCardNumberSpade;
        playScreen.RHOSuit = svgImages.spade;
      }
      var val = e.rank + e.suit;
      if (this.state.iskibitzer || playScreen.eastHands.length !== 0) {
        var index = playScreen.eastHands.findIndex(function (item, i) {
          return item.key === val;
        });
        console.log(val, index, playScreen.eastHands);
        playScreen.eastCardsShown[index] = 'none';
      }
      if ('RHO' === playScreen.DummySeat) {
        var index = playScreen.dummyHands.findIndex(function (item, i) {
          return item.key === val;
        });
        playScreen.dummycardsShown[index] = 'none';
        playScreen.DummyPlayed = true;
      }
      if(this.state.iskibitzer){
        playScreen.EAST_PLAY_AREA = playScreen.EAST_PLAY_AREA - KibitzerEastWestcardWidth;
      }else{
        playScreen.EAST_PLAY_AREA = playScreen.EAST_PLAY_AREA - eastWestcardWidth;
      }
      if (playScreen.cardsCnt < 4) {
        playScreen.DirectionTextMySeat = styles.DirectionTextActive;
        playScreen.southUserText = '#DBFF00';
        playScreen.CardBorderS = '#DBFF00';
        playScreen.directionImage = svgImages.pointerSouth;
        playScreen.DirectionTextRHO = styles.DirectionText;
        playScreen.eastUserText = '#6D6D6D';
        playScreen.playTurn = this.state.gamestate.mySeat;
      }
      ////  console.log('isSouthDummy ' + this.state.isSouthDummy);
      if (!playScreen.isSouthDummy) {
        if (playScreen.trick_Suit === Strings.c) {
          //  console.log('Numberof_C ' + playScreen.Numberof_C);
          if (Number(playScreen.Numberof_C) > 0) {
            playScreen.disablemyCards_C = false;
            playScreen.disablemyCards_D = true;
            playScreen.disablemyCards_H = true;
            playScreen.disablemyCards_S = true;
          } else {
            playScreen.disablemyCards_C = false;
            playScreen.disablemyCards_D = false;
            playScreen.disablemyCards_H = false;
            playScreen.disablemyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.d) {
          //  console.log('Numberof_D ' + playScreen.Numberof_D);
          if (Number(playScreen.Numberof_D) > 0) {
            //  console.log('enable diamond');
            playScreen.disablemyCards_D = false;
            playScreen.disablemyCards_C = true;
            playScreen.disablemyCards_H = true;
            playScreen.disablemyCards_S = true;
          } else {
            playScreen.disablemyCards_C = false;
            playScreen.disablemyCards_D = false;
            playScreen.disablemyCards_H = false;
            playScreen.disablemyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.h) {
          //  console.log('Numberof_H ' + playScreen.Numberof_H);
          if (Number(playScreen.Numberof_H) > 0) {
            playScreen.disablemyCards_H = false;
            playScreen.disablemyCards_D = true;
            playScreen.disablemyCards_C = true;
            playScreen.disablemyCards_S = true;
          } else {
            playScreen.disablemyCards_C = false;
            playScreen.disablemyCards_D = false;
            playScreen.disablemyCards_H = false;
            playScreen.disablemyCards_S = false;
          }
        } else if (playScreen.trick_Suit === Strings.directionSouth) {
          console.log('Numberof_S ' + playScreen.Numberof_S);
          if (Number(playScreen.Numberof_S) > 0) {
            playScreen.disablemyCards_S = false;
            playScreen.disablemyCards_D = true;
            playScreen.disablemyCards_H = true;
            playScreen.disablemyCards_C = true;
          } else {
            playScreen.disablemyCards_C = false;
            playScreen.disablemyCards_D = false;
            playScreen.disablemyCards_H = false;
            playScreen.disablemyCards_S = false;
          }
        }
      }
    }
    if (this.state.iskibitzer) {
      playScreen.disableTkBk = true;
      playScreen.undoCardSide = e.side;
      playScreen.undoCardSuit = e.suit;
      playScreen.undoCardRank = e.rank;
      playScreen.reqUndoimg = svgImages.undo;
    }
    //Disable Undo if this is the first card in the table
    if (
      e.side === nextSide(this.state.gamestate.contractinfo.declarer) &&
      this.state.tricks &&
      this.state.tricks[e.side].length === 1
    ) {
      playScreen.disableTkBk = true;
      playScreen.reqUndoimg = svgImages.undo;
    }
    this.props.updateDeskStateValues({ playScreen }, () => {});
    //console.log('end of carddisplay');
  }

  lastTrcickCards(e) {
    let playScreen = { ...this.state.playScreen };
    if (playScreen.trick_Suit === '') {
      playScreen.trick_Suit = e.suit;
      playScreen.cardsCnt = 0;
    }

    playScreen.cardsCnt = playScreen.cardsCnt + 1;

    if (e.side === this.state.gamestate.mySeat) {
      playScreen.MyCard = true;
      playScreen.myRank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.myCardStyle = styles.playCardClubs;
        playScreen.myCardNumberstyle = styles.playCardNumberClubs;
        playScreen.mySuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.myCardStyle = styles.playCardDiamond;
        playScreen.myCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.mySuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.myCardStyle = styles.playCardHearts;
        playScreen.myCardNumberstyle = styles.playCardNumberHearts;
        playScreen.mySuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.myCardStyle = styles.playCardSpade;
        playScreen.myCardNumberstyle = styles.playCardNumberSpade;
        playScreen.mySuit = svgImages.spade;
      }
    } else if (e.side === this.state.gamestate.myPartnerSeat) {
      playScreen.PartnerPlayed = true;
      playScreen.PartnerRank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.PartnerCardStyle = styles.playCardClubs;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberClubs;
        playScreen.PartnerSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.PartnerCardStyle = styles.playCardDiamond;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.PartnerSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.PartnerCardStyle = styles.playCardHearts;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberHearts;
        playScreen.PartnerSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.PartnerCardStyle = styles.playCardSpade;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberSpade;
        playScreen.PartnerSuit = svgImages.spade;
      }
    } else if (e.side === this.state.gamestate.myLHOSeat) {
      playScreen.LHOPlayed = true;
      playScreen.LHORank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.LHOCardStyle = styles.playCardClubs;
        playScreen.LHOCardNumberstyle = styles.playCardNumberClubs;
        playScreen.LHOSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.LHOCardStyle = styles.playCardDiamond;
        playScreen.LHOCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.LHOSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.LHOCardStyle = styles.playCardHearts;
        playScreen.LHOCardNumberstyle = styles.playCardNumberHearts;
        playScreen.LHOSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.LHOCardStyle = styles.playCardSpade;
        playScreen.LHOCardNumberstyle = styles.playCardNumberSpade;
        playScreen.LHOSuit = svgImages.spade;
      }
    } else if (e.side === this.state.gamestate.myRHOSeat) {
      playScreen.RHOPlayed = true;
      playScreen.RHORank = e.rank;
      if (e.suit === Strings.c) {
        playScreen.RHOCardStyle = styles.playCardClubs;
        playScreen.RHOCardNumberstyle = styles.playCardNumberClubs;
        playScreen.RHOSuit = svgImages.clubs;
      } else if (e.suit === Strings.d) {
        playScreen.RHOCardStyle = styles.playCardDiamond;
        playScreen.RHOCardNumberstyle = styles.playCardNumberDiamond;
        playScreen.RHOSuit = svgImages.diamond;
      } else if (e.suit === Strings.h) {
        playScreen.RHOCardStyle = styles.playCardHearts;
        playScreen.RHOCardNumberstyle = styles.playCardNumberHearts;
        playScreen.RHOSuit = svgImages.hearts;
      } else if (e.suit === Strings.directionSouth) {
        playScreen.RHOCardStyle = styles.playCardSpade;
        playScreen.RHOCardNumberstyle = styles.playCardNumberSpade;
        playScreen.RHOSuit = svgImages.spade;
      }
    }
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  displayCardsDummy(hand) {
    let playScreen = { ...this.state.playScreen };
    playScreen.dummyHands = [];
    playScreen.dummycardsShown = [];
    playScreen.NumberofDummy_S = 0;
    playScreen.NumberofDummy_H = 0;
    playScreen.NumberofDummy_D = 0;
    playScreen.NumberofDummy_C = 0;
    var idx = 0;
    //  console.log('displayCardsDummy');
    //console.log(hand.cards);
    //console.log(hand.side +  ', ' +this.state.gamestate.myLHOSeat);
    if (hand.side !== this.state.gamestate.mySeat) {
      for (let card of hand.cards) {
        ////  console.log(card.suit+','+card.rank)  ;
        if (card.suit === Strings.directionSouth) {
          playScreen.dummyHands.push({
            rank: card.rank,
            suit: Strings.directionSouth,
            key: card.rank + Strings.directionSouth,
            SuiteColor: '#C000FF',
            idx: idx
          });
          playScreen.NumberofDummy_S = playScreen.NumberofDummy_S + 1;
          //console.log('NumberofDummy_S ' + this.state.NumberofDummy_S)
        } else if (card.suit === Strings.d) {
          playScreen.dummyHands.push({
            rank: card.rank,
            suit: Strings.d,
            key: card.rank + Strings.d,
            SuiteColor: '#04AEFF',
            idx: idx
          });
          playScreen.NumberofDummy_D = playScreen.NumberofDummy_D + 1;
        } else if (card.suit === Strings.h) {
          playScreen.dummyHands.push({
            rank: card.rank,
            suit: Strings.h,
            key: card.rank + Strings.h,
            SuiteColor: '#FF0C3E',
            idx: idx
          });
          playScreen.NumberofDummy_H = playScreen.NumberofDummy_H + 1;
        } else if (card.suit === Strings.c) {
          playScreen.dummyHands.push({
            rank: card.rank,
            suit: Strings.c,
            key: card.rank + Strings.c,
            SuiteColor: '#79E62B',
            idx: idx
          });
          playScreen.NumberofDummy_C = playScreen.NumberofDummy_C + 1;
        }
        playScreen.dummycardsShown[idx] = 'flex';
        idx++;
      }
      if (hand.side === this.state.gamestate.myLHOSeat) {
        playScreen.DummySeat = 'LHO';
      } else if (hand.side === this.state.gamestate.myRHOSeat) {
        playScreen.DummySeat = 'RHO';
      } else if (hand.side === this.state.gamestate.myPartnerSeat) {
        playScreen.DummySeat = 'Partner';
      } else if (hand.side === this.state.gamestate.mySeat) {
        playScreen.isSouthDummy = true;
        playScreen.disablemyCards_C = true;
        playScreen.disablemyCards_D = true;
        playScreen.disablemyCards_H = true;
        playScreen.disablemyCards_S = true;
      }
      playScreen = this.updateCardPlayArea(playScreen);
    }
    if (hand.side === this.state.gamestate.mySeat) {
      playScreen.isSouthDummy = true;
      playScreen.disablemyCards_C = true;
      playScreen.disablemyCards_D = true;
      playScreen.disablemyCards_H = true;
      playScreen.disablemyCards_S = true;
    }
    // this.drawCardsLHO();
    // console.log('NumberofDummy_C '+ this.state.NumberofDummy_C + ' NumberofDummy_H '+this.state.NumberofDummy_H +' NumberofDummy_D '+this.state.NumberofDummy_D + ' NumberofDummy_S '+ this.state.NumberofDummy_H)

    // console.log('dummy seat is ' + playScreen.DummySeat);
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  dummycardPlayed(idx, suit, rank, img, SuiteColor) {
    let playScreen = { ...this.state.playScreen };
    console.log(
      'suit ',
      suit,
      ' rank ',
      rank,
      ' img ',
      img,
      ' SuiteColor ',
      SuiteColor,
      ' idx ',
      idx
    );
    if (suit === Strings.directionSouth) {
      img = svgImages.spade;
    } else if (suit === Strings.d) {
      img = svgImages.diamond;
    } else if (suit === Strings.h) {
      img = svgImages.hearts;
    } else if (suit === Strings.c) {
      img = svgImages.clubs;
    }
    if (playScreen.playTurn === this.state.gamestate.myPartnerSeat) {
      playScreen.PartnerPlayed = true;
      playScreen.PartnerRank = rank;
      playScreen.PartnerSuit = img;
      if (suit === Strings.c) {
        playScreen.NumberofDummy_C = playScreen.NumberofDummy_C - 1;
        playScreen.PartnerCardStyle = styles.playCardClubs;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberClubs;
      } else if (suit === Strings.d) {
        playScreen.NumberofDummy_D = playScreen.NumberofDummy_D - 1;
        playScreen.PartnerCardStyle = styles.playCardDiamond;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberDiamond;
      } else if (suit === Strings.h) {
        playScreen.NumberofDummy_H = playScreen.NumberofDummy_H - 1;
        playScreen.PartnerCardStyle = styles.playCardHearts;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberHearts;
      } else if (suit === Strings.directionSouth) {
        playScreen.NumberofDummy_S = playScreen.NumberofDummy_S - 1;
        playScreen.PartnerCardStyle = styles.playCardSpade;
        playScreen.PartnerCardNumberstyle = styles.playCardNumberSpade;
      }
      //var arrcd=this.state.cardsShown;
      playScreen.dummycardsShown[idx] = 'none';
      const occurrences = playScreen.dummycardsShown.reduce(function (
        acc,
        curr
      ) {
        return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
      },
      {});
      if(this.state.iskibitzer){
        playScreen.TotalCardWidthNorth =
        playScreen.TotalCardWidthNorth - kibitzerCardWidth;
      }else{
        playScreen.TotalCardWidthNorth =
        playScreen.TotalCardWidthNorth - cardWidth;
      }
      playScreen.disabledummyCards_C = true;
      playScreen.disabledummyCards_D = true;
      playScreen.disabledummyCards_H = true;
      playScreen.disabledummyCards_S = true;
      // send the message
      //  console.log('this.state.tableID ' + this.state.tableID);

      HOOL_CLIENT.playCard(
        this.state.tableID,
        this.state.gamestate.myPartnerSeat,
        rank,
        suit
      );
    } else {
      Alert.alert("It is not dummy's turn to play");
    }
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  cardPlayed(idx, suit, rank, img, SuiteColor) {
    if (this.state.iskibitzer) return;
    console.log(
      '@@@ card played ' +
      'idx ' +
        idx +
        ' suit ' +
        suit +
        ' rank ' +
        rank +
        ' img ' +
        img +
        ' SuiteColor ' +
        SuiteColor
    );
    if (suit === Strings.directionSouth) {
      img = svgImages.spade;
    } else if (suit === Strings.d) {
      img = svgImages.diamond;
    } else if (suit === Strings.h) {
      img = svgImages.hearts;
    } else if (suit === Strings.c) {
      img = svgImages.clubs;
    }
    let playScreen = { ...this.state.playScreen };
    console.log(this.state.gamestate.mySeat);
    if (playScreen.playTurn === this.state.gamestate.mySeat) {
      //console.log('suit '+ suit + ' rank '+ rank + ' img ' + img +' SuiteColor '+SuiteColor);
      playScreen.MyCard = true;
      playScreen.myRank = rank;
      playScreen.mySuit = img;
      if (suit === Strings.c) {
        playScreen.Numberof_C = playScreen.Numberof_C - 1;
        playScreen.myCardStyle = styles.playCardClubs;
        playScreen.myCardNumberstyle = styles.playCardNumberClubs;
      } else if (suit === Strings.d) {
        playScreen.Numberof_D = playScreen.Numberof_D - 1;
        playScreen.myCardStyle = styles.playCardDiamond;
        playScreen.myCardNumberstyle = styles.playCardNumberDiamond;
      } else if (suit === Strings.h) {
        playScreen.Numberof_H = playScreen.Numberof_H - 1;
        playScreen.myCardStyle = styles.playCardHearts;
        playScreen.myCardNumberstyle = styles.playCardNumberHearts;
      } else if (suit === Strings.directionSouth) {
        playScreen.Numberof_S = playScreen.Numberof_S - 1;
        playScreen.myCardStyle = styles.playCardSpade;
        playScreen.myCardNumberstyle = styles.playCardNumberSpade;
      }
      //var arrcd=this.state.cardsShown;
      //console.log(cardsShown)
      playScreen.cardsShown[idx] = 'none';
      if(this.state.iskibitzer){
        playScreen.TotalCardWidthSouth =
        playScreen.TotalCardWidthSouth - kibitzerCardWidth;
      }else{
        playScreen.TotalCardWidthSouth =
        playScreen.TotalCardWidthSouth - cardWidth;
      }
      console.log(playScreen.TotalCardWidthSouth + '-');
      playScreen.disablemyCards_C = true;
      playScreen.disablemyCards_D = true;
      playScreen.disablemyCards_H = true;
      playScreen.disablemyCards_S = true;

      // const { navigation } = this.props;
      // const hool = navigation.getParam('xmppconn');
      // send the message
      HOOL_CLIENT.playCard(
        this.state.tableID,
        this.state.gamestate.mySeat,
        rank,
        suit
      );
    } else {
      Alert.alert('It is not your turn to play.');
    }
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  cardOccurrences(cards) {
    let occurrences = cards.reduce(function (acc, curr) {
      return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
    }, {});
    return occurrences;
  }

  undoResponse(responsestr) {
    let playScreen = { ...this.state.playScreen };
    if (playScreen.isClaimInitiated)
    HOOL_CLIENT.claim(this.state.tableID, responsestr, {});
    else HOOL_CLIENT.undo(this.state.tableID, responsestr, {});
    playScreen.undoLHO = false;
    playScreen.undoRHO = false;
    playScreen.cancelAnimateImg = true;
    this.props.updateDeskStateValues({ playScreen }, () => {});
  }

  undoRequest() {
    // const { navigation } = this.props;
    // const hool = navigation.getParam('xmppconn');
    // if (this.state.undoAsked === false) {
    //   this.setState({
    //     undoAsked: true,
    //     reqUndoimg: require('../../assets/images/cross.png')
    //   });
    //   hool.undo(this.state.tableID, 'request', {
    //     side: this.state.undoCardSide,
    //     rank: this.state.undoCardRank,
    //     suit: this.state.undoCardSuit
    //   });
    // } else {
    //   this.setState({
    //     disableTkBk: true,
    //     undoAsked: false,
    //     reqUndoimg: svgImages.undo
    //   });
    //   hool.undo(this.state.tableID, 'cancel', {
    //     side: this.state.undoCardSide
    //   });
    // }
    //hool.undo(this.state.tableID, 'request' , {side:this.state.undoCardSide, rank:this.state.undoCardRank,  suit:this.state.undoCardSuit});
  }

  showHistory() {
    const { navigation } = this.props;
    const hool = navigation.getParam('xmppconn');
    const gamestate = navigation.getParam('gameState');
    this.componentWillUnmount();
    this.props.navigation.navigate('History', {
      xmppconn: hool,
      tableID: this.state.tableID,
      gameState: gamestate,
      cardsCnt: this.state.cardsCnt
    });
  }

  showCheetSheet() {
    // const { navigation } = this.props;
    // const hool = navigation.getParam('xmppconn');
    // const gamestate = navigation.getParam('gameState');
    // this.componentWillUnmount();
    // this.props.navigation.navigate('CheatSheet', {
    //   xmppconn: hool,
    //   tableID: this.state.tableID,
    //   gameState: gamestate,
    //   cardsCnt: this.state.cardsCnt
    // });
  }

  // undoLHOCancel() {
  //   const { navigation } = this.props;
  //   const hool = navigation.getParam('xmppconn');
  //   //hool.undoCancel(this.state.tableID , this.state.gamestate.mySeat)
  //   hool.undo(this.state.tableID, 'cancel', { side: this.state.gamestate.mySeat });
  // }

  animateImg() {
    var arrImg = [
      { img: require('../../assets/images/crxanimation_1.png') },
      { img: require('../../assets/images/crxanimation_2.png') },
      { img: require('../../assets/images/crxanimation_3.png') },
      { img: require('../../assets/images/crxanimation_4.png') },
      { img: require('../../assets/images/crxanimation_5.png') },
      { img: require('../../assets/images/crxanimation_6.png') },
      { img: require('../../assets/images/crxanimation_7.png') },
      { img: require('../../assets/images/crxanimation_8.png') },
      { img: require('../../assets/images/crxanimation_9.png') },
      { img: require('../../assets/images/crxanimation_10.png') },
      { img: require('../../assets/images/crxanimation_11.png') },
      { img: require('../../assets/images/crxanimation_12.png') },
      { img: require('../../assets/images/crxanimation_13.png') },
      { img: require('../../assets/images/crxanimation_14.png') },
      { img: require('../../assets/images/crxanimation_15.png') },
      { img: require('../../assets/images/crxanimation_16.png') }
    ];

    var ir = 0;
    // const interv = setInterval(() => {
    //   if (ir > 15 || this.state.cancelAnimateImg) {
    //     clearInterval(interv);
    //     this.setState({ undoLHO: false, undoRHO: false });
    //   } else {
    //     this.setState({ playTimer: arrImg[ir].img });
    //     ir++;
    //   }
    // }, 1000);
  }
  isPartnerCardEmpty() {
    return (
      this.state.playScreen.DummySeat !== 'Partner' &&
      this.state.playScreen.northHands.length == 0
    );
  }

  drawmycards = () => {
    var svgImg;
    return this.state.playScreen.hands
      ? this.state.playScreen.hands.map(element => {
          if (element.suit === Strings.directionSouth) {
            svgImg = svgImages.spade;
          } else if (element.suit === Strings.d) {
            svgImg = svgImages.diamond;
          } else if (element.suit === Strings.h) {
            svgImg = svgImages.hearts;
          } else if (element.suit === Strings.c) {
            svgImg = svgImages.clubs;
          }
          return (
            <TouchableOpacity
              style={[
                styles.cardDisplayBodyNorth,
                {
                  height: scale(45),
                  width: cardWidth,
                  display: this.state.playScreen.cardsShown[element.idx],
                  backgroundColor: '#151515'
                }
              ]}
              key={element.key}
              onPress={() =>
                this.cardPlayed(
                  element.idx,
                  element.suit,
                  element.rank,
                  svgImg,
                  element.SuiteColor
                )
              }
              disabled={
                element.suit === Strings.c
                  ? this.state.playScreen.disablemyCards_C
                  : element.suit === Strings.d
                  ? this.state.playScreen.disablemyCards_D
                  : element.suit === Strings.h
                  ? this.state.playScreen.disablemyCards_H
                  : element.suit === Strings.directionSouth
                  ? this.state.playScreen.disablemyCards_S
                  : null
              }
            >
              <View
                style={[
                  styles.cardPosition,
                  {
                    width: scale(22),
                    marginTop: moderateScale(3),
                    marginLeft: moderateScale(4)
                  }
                ]}
              >
                <Text
                  style={[
                    styles.SpadeCardNumber,
                    { color: element.SuiteColor }
                  ]}
                >
                  {element.rank}
                </Text>
                <SvgXml xml={svgImg} width={(scale(9))}height={scale(10)}/>
              </View>
            </TouchableOpacity>
          );
        })
      : null;
  };

  drawCardsPartner = () => {
    var svgImg;
    let isNorthHand = this.state.playScreen.northHands.length ? true : false;
    console.log('is North Hand', isNorthHand);
    let hands = this.state.playScreen.northHands.length
      ? this.state.playScreen.northHands
      : this.state.playScreen.dummyHands;
    return hands.map(element => {
      if (element.suit === Strings.directionSouth) {
        svgImg = svgImages.spade;
      } else if (element.suit === Strings.d) {
        svgImg = svgImages.diamond;
      } else if (element.suit === Strings.h) {
        svgImg = svgImages.hearts;
      } else if (element.suit === Strings.c) {
        svgImg = svgImages.clubs;
      }
      return (
        <TouchableOpacity
          style={[
            styles.cardDisplayBodyNorth,
            {
              height: scale(45),
              width: cardWidth,
              display: isNorthHand
                ? this.state.playScreen.northCardsShown[element.idx]
                : this.state.playScreen.dummycardsShown[element.idx]
            }
          ]}
          key={element.key}
          onPress={() =>
            this.dummycardPlayed(
              element.idx,
              element.suit,
              element.rank,
              svgImg,
              element.SuiteColor
            )
          }
          disabled={
            element.suit === Strings.c
              ? this.state.playScreen.disabledummyCards_C
              : element.suit === Strings.d
              ? this.state.playScreen.disabledummyCards_D
              : element.suit === Strings.h
              ? this.state.playScreen.disabledummyCards_H
              : element.suit === Strings.directionSouth
              ? this.state.playScreen.disabledummyCards_S
              : null
          }
        >
          <View
            style={[
              styles.cardPosition,
              {
                width: scale(22),
                marginTop: moderateScale(3),
                marginLeft: moderateScale(4)
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <SvgXml xml={svgImg} width={(scale(9))}height={scale(10)}/>
          </View>
        </TouchableOpacity>
      );
    });
  };
  
  drawCardsLHO = () => {
    var svgImg;
    let isWestHand = this.state.playScreen.westHands.length ? true : false;
    let hands = this.state.playScreen.westHands.length
      ? this.state.playScreen.westHands
      : this.state.playScreen.dummyHands;
    return hands.map(element => {
      if (element.suit === Strings.directionSouth) {
        svgImg = svgImages.spade;
      } else if (element.suit === Strings.d) {
        svgImg = svgImages.diamond;
      } else if (element.suit === Strings.h) {
        svgImg = svgImages.hearts;
      } else if (element.suit === Strings.c) {
        svgImg = svgImages.clubs;
      }
      return (
        <View
          style={[
            styles.cardDisplayBodyNorth,
            {
              height: scale(45),
              width: this.state.iskibitzer? KibitzerEastWestcardWidth:eastWestcardWidth,// (hands.length -1 == element.idx)? scale(40): 
              display: isWestHand
                ? this.state.playScreen.westCardsShown[element.idx]
                : this.state.playScreen.dummycardsShown[element.idx]
            }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPositionW,
              {
                width: scale(20),
                marginTop: moderateScale(3),
                marginLeft: moderateScale(1)
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <SvgXml xml={svgImg} width={(scale(9))}height={scale(10)}/>
          </View>
        </View>
      );
    });
  };
  
  drawCardsRHO = () => {
    var svgImg;
    let isEastHand = this.state.playScreen.eastHands.length ? true : false;
    let hands = this.state.playScreen.eastHands.length
      ? this.state.playScreen.eastHands
      : this.state.playScreen.dummyHands;
    return hands.map(element => {
      if (element.suit === Strings.directionSouth) {
        svgImg = svgImages.spade;
      } else if (element.suit === Strings.d) {
        svgImg = svgImages.diamond;
      } else if (element.suit === Strings.h) {
        svgImg = svgImages.hearts;
      } else if (element.suit === Strings.c) {
        svgImg = svgImages.clubs;
      }
      return (
        <View
          style={[
            styles.cardDisplayBodyEast,
            {
              width: this.state.iskibitzer? KibitzerEastWestcardWidth:eastWestcardWidth, //(element.idx == 0)? scale(40)
              height: scale(45),
              display: isEastHand
                ? this.state.playScreen.eastCardsShown[element.idx]
                : this.state.playScreen.dummycardsShown[element.idx]
            }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPositionE,
              {
                width: eastWestcardWidth,
                marginTop: minasFourTop,
                marginLeft: moderateScale(2)
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <SvgXml xml={svgImg} width={(scale(9))}height={scale(10)}/>
          </View>
        </View>
      );
    });
  };

  toggleBidInfoSection(side){
    if(side == 'N'){
      this.updateStateValues({northBidInfo: true})
      setInterval(() => this.setState({ northBidInfo: false}), 5000);
    }else if(side == 'S'){
      this.updateStateValues({southBidInfo: true})
      setInterval(() => this.setState({ southBidInfo: false}), 5000);
    } else if(side == 'E'){
      this.updateStateValues({eastBidInfo: true})
      setInterval(() => this.setState({ eastBidInfo: false}), 5000);
    } else if(side == 'W'){
      this.updateStateValues({westBidInfo: true})
      setInterval(() => this.setState({ westBidInfo: false}), 5000);
    }
  }

  render() { 
    return this.state.isValueInitialized && !this.props.isHomeUser ? (
      <>
        <View
          style={{
            position: 'absolute',
            top: 1,
            height: scale(68),
            zIndex: 9,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <View
            style={[
              styles.hanNorthSauthTotalWidthHeightArea,
            ]}
          >
            {this.isPartnerCardEmpty() ? (
              <View
                style={[
                  styles.patternTotalColumnArea,
                  {
                    height: scale(45)
                  }
                ]}
              >
                {/* This is for shared values of opponents */}
                <View
                  style={[
                    styles.patternTotalRowArea,
                    { height: scale(45), width: scale(181) }
                  ]}
                >
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.northSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.northSharedValue1
                      )}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.northSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.northSharedValue2
                      )}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              // console.log("kibits, northInfo", this.state.iskibitzer, !this.state.northBidInfo )
              (this.state.iskibitzer && !this.state.northBidInfo) ?
                <TouchableOpacity
                  onPress={() => this.toggleBidInfoSection("N")}
                  style={{
                    width: this.state.playScreen.TotalCardWidthNorth,
                    height: scale(45),
                    marginLeft: scale(1),
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignSelf: 'center',
                    zIndex: 1
                  }}
                >
                  {this.drawCardsPartner()}
                </TouchableOpacity>
                :
                // this is dummy's cards showed explicitly for others
                !this.state.iskibitzer &&
                <View
                  style={{
                    width: this.state.playScreen.TotalCardWidthNorth,
                    height: scale(45),
                    marginLeft: scale(1),
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignSelf: 'center',
                    zIndex: 1
                  }}
                >
                  {this.drawCardsPartner()}
                </View>
            )}
            {
              (this.state.iskibitzer && this.state.northBidInfo) &&
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      width: scale(90), height: scale(45), backgroundColor: '#151515', alignItems: 'center',
                      justifyContent: 'center',
                      borderRightWidth: moderateVerticalScale(1)
                    }}
                  >
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.northSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {this.state.infoScreen.northSharedValue1}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: scale(90), height: scale(45),
                      justifyContent: 'center',
                      backgroundColor: '#151515',
                    }}
                  >
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                      {this.state.infoScreen.northSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {this.state.infoScreen.northSharedValue2}
                    </Text>
                  </View>
                </View>
                {
                  !this.state.northBidInfo &&
                  <View style={{ height: scale(23), width: verticalScale(85), justifyContent: 'center', flexDirection: 'row' }}>
                    <CrownHost 
                        hostName={this.props.hostName}
                        shareBidPlay={false}
                        rightSide={false}
                        textStyle={{...styles.shareHandText, color: this.state.playScreen.northUserText, alignSelf: 'center'}}
                        userName={this.state.gamestate.myPartnername}
                      />
                  </View>
                }
              </View>
            }
            {
              !this.state.iskibitzer ?
                this.state.gamestate.myPartnername.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName)
                ?
                <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(5) }}>
                  <AddPlayer
                    isNorthInvited={this.props.isNorthInvited}
                    direction={Strings.directionNorth}
                    northUserName={this.props.northUserName}
                    directionNorth={this.props.directionNorth}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    cancelRequest={this.props.cancelRequest}
                    tableId={this.props.tableID} />
                </View>
                :
                <View
                  style={[
                    styles.hanNameWidthHeightArea,
                    { height: scale(23), }
                  ]}
                >
                  <Image
                    style={{
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8)
                    }}
                    source={require('../../assets/images/takeback_black.png')}
                  />
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myPartnerSeat) }}>
                    <CrownHost 
                        hostName={this.props.hostName}
                        shareBidPlay={false}
                        rightSide={false}
                        textStyle={{...styles.shareHandText, color: this.state.playScreen.northUserText, alignSelf: 'center'}}
                        userName={this.state.gamestate.myPartnername}
                      />
                  </TouchableOpacity>
                  <Image
                    style={{
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8),
                    }}
                    source={this.state.playScreen.undoPartnerSeatimg}
                  />
                </View>
              : <View
              style={[
                styles.hanNameWidthHeightArea,
                { height: scale(23), }
              ]}
            >
              <Image
                style={{
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8)
                }}
                source={require('../../assets/images/takeback_black.png')}
              />
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myPartnerSeat) }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{ ...styles.shareHandText, color: this.state.playScreen.northUserText, alignSelf: 'center' }}
                      userName={this.state.gamestate.myPartnername}
                    />
              </TouchableOpacity>
              <Image
                style={{
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8),
                }}
                source={this.state.playScreen.undoPartnerSeatimg}
              />
            </View>
            }

          </View>
        </View>

        <View
          style={{
            position: 'absolute',
            left: moderateScale(1),
            width: scale(68),
            height: scale(360),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <View
            style={{
              flexDirection: 'column',
              width: scale(360),
              height: scale(68),
              transform: [{ rotate: '-90deg' }]
            }}
          >
            {
              (this.state.iskibitzer && this.state.westBidInfo) &&
              <View
                style={[
                  styles.patternTotalColumnArea,
                  { height: scale(45) }
                ]}
              >
                <View
                  style={[
                    styles.patternTotalRowArea,
                    { height: scale(45), width: scale(181) }
                  ]}
                >
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.westSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23), paddingTop: moderateScale(3) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23) }}>
                      {formatSharedValue(
                        this.state.infoScreen.westSharedValue1
                      )}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.westSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23) }}>
                      {formatSharedValue(
                        this.state.infoScreen.westSharedValue2
                      )}
                    </Text>
                  </View>
                </View>
              </View>

            } 
            {this.state.playScreen.DummySeat !== 'LHO' &&
            this.state.playScreen.westHands.length == 0 ? (
              <View
                style={[
                  styles.patternTotalColumnArea,
                  { height: scale(45) }
                ]}
              >
                <View
                  style={[
                    styles.patternTotalRowArea,
                    { height: scale(45), width: scale(181) }
                  ]}
                >
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                       style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                       { ...styles.shareHandText, width: scale(90), height: scale(21),paddingTop: moderateScale(3)}}
                    >
                      {this.state.infoScreen.westSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23), paddingTop: moderateScale(3)}
                    : { ...styles.PaternText, width: scale(90), height: scale(23)}}>
                      {formatSharedValue(
                        this.state.infoScreen.westSharedValue1
                      )}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21),paddingTop: moderateScale(3) } :
                      { ...styles.shareHandText, width: scale(90), height: scale(21),paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.westSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23)}
                    : { ...styles.PaternText, width: scale(90), height: scale(23)}}>
                      {formatSharedValue(
                        this.state.infoScreen.westSharedValue2
                      )}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              (this.state.iskibitzer && !this.state.westBidInfo) ?
                <TouchableOpacity
                  onPress={() => this.toggleBidInfoSection("W")}
                  style={{
                    height: scale(45),
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {this.drawCardsLHO()}
                </TouchableOpacity>
                : 
                !this.state.iskibitzer &&
                <View
                  style={{
                    height: scale(45),
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {this.drawCardsLHO()}
                </View>
            )}
            {
              !this.state.iskibitzer ?
                this.state.gamestate.myLHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName)
                ?
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <AddPlayer
                    isWestInvited={this.props.isWestInvited}
                    direction={Strings.directionWest}
                    directionWest={this.props.directionWest}
                    westUserName={this.props.westUserName}
                    onAddPlayerClick={this.props.onAddPlayerClick}
                    cancelRequest={this.props.cancelRequest}
                    tableId={this.props.tableID} />
                </View> 
                :
                <View
                  style={[
                    styles.hanNameWidthHeightArea,
                    { height: scale(23) }
                  ]}
                >
                  <Image
                    style={{
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8)
                    }}
                    source={require('../../assets/images/takeback_black.png')}
                  />
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myLHOSeat) }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{ ...styles.shareHandText, color: this.state.playScreen.westUserText, alignSelf: 'center' }}
                      userName={this.state.gamestate.myLHOname}
                    />
                  </TouchableOpacity>
                  <Image
                    style={{
                      marginTop: moderateScale(4),
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8)
                    }}
                    source={this.state.playScreen.undoLHOSeatimg}
                  />
                </View>
              : <View
              style={[
                styles.hanNameWidthHeightArea,
                { height: scale(23) }
              ]}
            >
              <Image
                style={{
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8)
                }}
                source={require('../../assets/images/takeback_black.png')}
              />
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myLHOSeat) }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{ ...styles.shareHandText, color: this.state.playScreen.westUserText, alignSelf: 'center' }}
                      userName={this.state.gamestate.myLHOname}
                    />
              </TouchableOpacity>
              <Image
                style={{
                  marginTop: moderateScale(4),
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8)
                }}
                source={this.state.playScreen.undoLHOSeatimg}
              />
            </View>
            }
          </View>
        </View>

        {/* East shared values  */}
        <View
          style={{
            position: 'absolute',
            right: 0,
            width: scale(68),
            height: scale(360),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <View
            style={{
              flexDirection: 'column',
              width: scale(360),
              height: scale(68),
              transform: [{ rotate: '90deg' }]
            }}
          >
            {
              (this.state.iskibitzer && this.state.eastBidInfo) &&
              <View
                style={[
                  styles.patternTotalColumnArea,
                  { height: scale(45) }
                ]}
              >
                <View
                  style={[
                    styles.patternTotalRowArea,
                    { height: scale(45), width: scale(181) }
                  ]}
                >
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.eastSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.eastSharedValue1
                      )}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.eastSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.eastSharedValue2
                      )}
                    </Text>
                  </View>
                </View>
              </View>
            }
            {this.state.playScreen.DummySeat !== 'RHO' &&
              this.state.playScreen.eastHands.length == 0 ? (
              <View
                style={[
                  styles.patternTotalColumnArea,
                  { height: scale(45) }
                ]}
              >
                <View
                  style={[
                    styles.patternTotalRowArea,
                    { height: scale(45), width: scale(181) }
                  ]}
                >
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.eastSharedType1}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.eastSharedValue1
                      )}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.patternWidthHeightArea,
                      { height: scale(45), width: scale(90) }
                    ]}
                  >
                    <Text
                      style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21), paddingTop: moderateScale(3) } :
                        { ...styles.shareHandText, width: scale(90), height: scale(21), paddingTop: moderateScale(3) }}
                    >
                      {this.state.infoScreen.eastSharedType2}
                    </Text>
                    <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) }
                      : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                      {formatSharedValue(
                        this.state.infoScreen.eastSharedValue2
                      )}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              (this.state.iskibitzer && !this.state.eastBidInfo) ?
                <TouchableOpacity
                  style={{
                    height: scale(45),
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: scale(45),
                    justifyContent: 'center',
                    flexDirection: 'row-reverse',
                    alignItems: 'center'
                  }}
                  onPress={() => this.toggleBidInfoSection("E")}
                >
                  {this.drawCardsRHO()}
                </TouchableOpacity>
                :
                !this.state.iskibitzer &&
                <View
                  style={{
                    height: scale(45),
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: scale(45),
                    justifyContent: 'center',
                    flexDirection: 'row-reverse',
                    alignItems: 'center'
                  }}
                >
                  {this.drawCardsRHO()}
                </View>
            )}
            {
               !this.state.iskibitzer ?
               this.state.gamestate.myRHOname.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName)
               ?
               <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                 <AddPlayer
                   isEastInvited={this.props.isEastInvited}
                   direction={Strings.directionEast}
                   directionEast={this.props.directionEast}
                   eastUserName={this.props.eastUserName}
                   onAddPlayerClick={this.props.onAddPlayerClick}
                   cancelRequest={this.props.cancelRequest}
                   tableId={this.props.tableID} />
               </View> :
                <View
                  style={[
                    styles.hanNameWidthHeightArea,
                    { height: scale(23) }
                  ]}
                >
                  <Image
                    style={{
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8)
                    }}
                    source={require('../../assets/images/takeback_black.png')}
                  />
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myRHOSeat) }}>
                    <CrownHost
                      hostName={this.props.hostName}
                      shareBidPlay={false}
                      rightSide={false}
                      textStyle={{ ...styles.shareHandText, color: this.state.playScreen.eastUserText, alignSelf: 'center' }}
                      userName={this.state.gamestate.myRHOname}
                    />
                  </TouchableOpacity>
                  <Image
                    style={{
                      marginLeft: moderateScale(8),
                      marginRight: moderateScale(8)
                    }}
                    source={this.state.playScreen.undoRHOSeatimg}
                  />

                </View>
              :  <View
              style={[
                styles.hanNameWidthHeightArea,
                { height: scale(23) }
              ]}
            >
              <Image
                style={{
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8)
                }}
                source={require('../../assets/images/takeback_black.png')}
              />
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.myRHOSeat) }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={false}
                    rightSide={false}
                    textStyle={{ ...styles.shareHandText, color: this.state.playScreen.eastUserText, alignSelf: 'center' }}
                    userName={this.state.gamestate.myRHOname}
                  />
              </TouchableOpacity>
              <Image
                style={{
                  marginLeft: moderateScale(8),
                  marginRight: moderateScale(8)
                }}
                source={this.state.playScreen.undoRHOSeatimg}
              />

            </View>
            }
          </View>
        </View>

        {/* Claim part */}
        {this.state.playScreen.undoLHO &&
        !this.state.playScreen.isSouthDummy ? (
          <View
            style={[
              styles.claimingBodyAreaWest,
              {
                left: Platform.OS == 'android'? moderateScale(70):moderateScale(90),
                width: scale(90),
                height: scale(184),
                zIndex: 3
              }
            ]}
          >
            <TouchableOpacity onPress={() => this.undoResponse('accept')}>
              <SvgXml  xml={svgImages.createPlay} height={scale(41)} width={scale(41)}/> 
            </TouchableOpacity>
            <SvgXml  xml={svgImages.line} height={scale(33)}/> 
            <Text style={styles.claimMdlText}>
              {this.state.playScreen.isClaimInitiated
                ? (this.state.playScreen.isConcedeTrick
                    ? 'Concede '
                    : 'Claim ') + this.state.playScreen.claimConcedeNumber
                : 'Undo Requested'}
            </Text>
            <SvgXml  xml={svgImages.line} height={scale(33)}/> 
            <TouchableOpacity onPress={() => this.undoResponse('reject')}>
                <SvgXml xml={svgImages.joinSymbolPlay} height={scale(41)} width={scale(41)} /> 
            </TouchableOpacity>
          </View>
        ) : null}

        {/* Claim part */}
        {this.state.playScreen.undoRHO &&
         !this.state.playScreen.isSouthDummy ? (
          <View
            style={[
              styles.claimingBodyAreaEast,
              {
                right: Platform.OS == 'android'? moderateScale(70):moderateScale(90),
                width: scale(90),
                height: scale(184),
                zIndex: 3
              }
            ]}
          >
            <TouchableOpacity onPress={() => this.undoResponse('accept')}>
              <SvgXml  xml={svgImages.createPlay} height={scale(41)} width={scale(41)}/> 
            </TouchableOpacity>
            <SvgXml  xml={svgImages.line} height={scale(33)}/> 
            <Text style={styles.claimMdlText}>
              {this.state.playScreen.isClaimInitiated
                ? (this.state.playScreen.isConcedeTrick
                    ? 'Concede '
                    : 'Claim ') + this.state.playScreen.claimConcedeNumber
                : 'Undo Requested'}
            </Text>
            <SvgXml  xml={svgImages.line} height={scale(33)}/> 
            <TouchableOpacity onPress={() => this.undoResponse('reject')}>
              <SvgXml xml={svgImages.joinSymbolPlay} height={scale(41)} width={scale(41)} /> 
            </TouchableOpacity>
          </View>
        ) : null}

        <View
          style={[
            styles.playingCardTotalWidthHeightArea,
            { height: scale(225), width: scale(225)}
          ]}
        >
          <View
            style={[
              styles.northCardPosition,
              { height: scale(81), width: scale(55)}
            ]}
          >
            {this.state.playScreen.PartnerPlayed ? (
              // Partner Card
              <View
                style={[
                  this.state.playScreen.PartnerCardStyle,
                  { height: scale(81), width: scale(55) }
                ]}
              >
                <View
                  style={[
                    styles.playCardNumberPosition,
                    {
                      height: scale(81),
                      width: scale(55),
                      paddingLeft: moderateScale(3)
                    }
                  ]}
                >
                  <Text style={this.state.playScreen.PartnerCardNumberstyle}>
                    {this.state.playScreen.PartnerRank}
                  </Text>
                </View>
                <SvgXml xml={this.state.playScreen.PartnerSuit} width={scale(11)} height={scale(10)}/>
              </View>
            ) : (
              <View
                style={[
                  styles.CardPositionBorderAss,
                  {
                    height: scale(81),
                    width: scale(55),
                    borderColor: this.state.playScreen.CardBorderN
                  }
                ]}
              ></View>
            )}
          </View>
          
          {/* West card play area */}
          <View
            style={[
              styles.playingEastWestCardTotalWidthHeightArea,
              { height: scale(55) }
            ]}
          >
            <View
              style={[
                styles.westCardPosition,
                { height: scale(81), width: scale(55) }
              ]}
            >
              {this.state.playScreen.LHOPlayed ? (
                <View
                  style={[
                    this.state.playScreen.LHOCardStyle,
                    { height: scale(81), width: scale(55),}
                  ]}
                >
                  <View
                    style={[
                      styles.playCardNumberPosition,
                      {
                        height: scale(81),
                        width: scale(55),
                        paddingTop: moderateScale(3),
                        paddingLeft: moderateScale(3)
                      }
                    ]}
                  >
                    <Text style={this.state.playScreen.LHOCardNumberstyle}>
                      {this.state.playScreen.LHORank}
                    </Text>
                  </View>
                  <SvgXml xml={this.state.playScreen.LHOSuit} width={scale(11)} height={scale(10)}/>
                </View>
              ) : (
                <View
                  style={[
                    styles.CardPositionBorderAss,
                    {
                      height: scale(81),
                      width: scale(55),
                      borderColor: this.state.playScreen.CardBorderW
                    }
                  ]}
                ></View>
              )}
            </View>
            <View
              style={[
                styles.eastCardPosition,
                {
                  height: scale(81),
                  width: scale(55),
                  marginRight: moderateScale(12)
                }
              ]}
            >
              {this.state.playScreen.RHOPlayed ? (
                <View
                  style={[
                    this.state.playScreen.RHOCardStyle,
                    ,
                    { height: scale(81), width: scale(55) }
                  ]}
                >
                  <View
                    style={[
                      styles.playCardNumberPosition,
                      {
                        height: scale(81),
                        width: scale(55),
                        paddingTop: moderateScale(2),
                        paddingLeft: moderateScale(6)
                      }
                    ]}
                  >
                    <Text style={this.state.playScreen.RHOCardNumberstyle}>
                      {this.state.playScreen.RHORank}
                    </Text>
                  </View>
                  <SvgXml xml={this.state.playScreen.RHOSuit} width={scale(11)} height={scale(10)}/>
                </View>
              ) : (
                <View
                  style={[
                    styles.CardPositionBorderAss,
                    {
                      height: scale(81),
                      width: scale(55),
                      borderColor: this.state.playScreen.CardBorderE
                    }
                  ]}
                ></View>
              )}
            </View>
          </View>

          {/* MyCard play 4 rects area */}
          <View
            style={[
              styles.sauthCardPosition,
              { height: scale(81), width: scale(55) }
            ]}
          >
            {this.state.playScreen.MyCard ? (
              <View
                style={[
                  this.state.playScreen.myCardStyle,
                  { width: scale(55), height: scale(81) }
                ]}
              >
                <View
                  style={[
                    styles.playCardNumberPosition,
                    {
                      height: scale(81),
                      width: scale(55),
                      paddingTop: moderateScale(2),
                      paddingLeft: moderateScale(6)
                    }
                  ]}
                >
                  <Text style={this.state.playScreen.myCardNumberstyle}>
                    {this.state.playScreen.myRank}
                  </Text>
                </View>
                <SvgXml xml={this.state.playScreen.mySuit} width={scale(11)} height={scale(10)}/>
              </View>
            ) : (
              <View
                style={[
                  styles.CardPositionBorderAss,
                  {
                    height: scale(81),
                    width: scale(55),
                    borderColor: this.state.playScreen.CardBorderS
                  }
                ]}
              ></View>
            )}
          </View>

          {/* Pointer style division */}
          <View
            style={[
              styles.totalWidthheightPositionDirectionArea,
              { height: scale(225)}
            ]}
          >
            <View
              style={[
                styles.playCardDirectionPosition,
                { height: scale(61), width: scale(61),justifyContent: 'center'}
              ]}
            >
              <View style={{ height: scale(17), justifyContent: 'space-around'}}>
                <Text style={{
                  ...this.state.playScreen.DirectionTextPartner,
                  textAlign: 'center',
                }}>
                  {this.state.gamestate.myPartnerSeat}
                </Text>
              </View>
              <View style={{...styles.pointerEastWestAreaPosition}}>
                <Text
                  style={[
                    this.state.playScreen.DirectionTextLHO,
                    { flex: 1, textAlign: 'center'}
                  ]}
                >
                  {this.state.gamestate.myLHOSeat}
                </Text>
                <SvgXml height={scale(27)} width={scale(27)}
                  xml={this.state.playScreen.directionImage}
                  style={[
                    styles.directionPointerPosition,
                    {
                      justifyContent: 'center',
                    }
                  ]} />
                <Text
                  style={[
                    this.state.playScreen.DirectionTextRHO,
                    { flex: 1, textAlign: 'center'}
                  ]}
                >
                  {this.state.gamestate.myRHOSeat}
                </Text>
              </View>
              <View style={{ height: scale(17),justifyContent: 'space-around'}}>
              <Text style={{...this.state.playScreen.DirectionTextMySeat, 
                  textAlign:'center'}}>
                {this.state.gamestate.mySeat}
              </Text>
              </View>
            </View>
          </View>
        </View>

        {/* For myCard distribution */}
        <View
          style={[
            styles.hanNorthSauthTotalWidthHeightArea,
            { height: scale(68), bottom: 0 }
          ]}
        >
          {
            (this.state.southUser.toUpperCase() == Strings.open && getUserNameBasedOnHost(this.props.username,this.props.hostName)) ?
              <AddPlayer 
              isSouthInvited={this.props.isSouthInvited} 
              direction = {Strings.directionSouth}
              directionSouth = {this.props.directionSouth}
              southUserName = {this.props.southUserName}
              onAddPlayerClick={this.props.onAddPlayerClick}
              cancelRequest={this.props.cancelRequest}
              tableId={this.props.tableID}/>:
              <View
                style={[styles.hanNameWidthHeightArea, { height: scale(23) }]}
              >
                <Image
                  style={{
                    marginLeft: moderateScale(8),
                    marginRight: moderateScale(8)
                  }}
                  source={require('../../assets/images/takeback_black.png')}
                />
                <TouchableOpacity
                  style={{ flexDirection: 'row' }}
                  onPress={() => { this.props.loadProfileOrVaccatePlayerScreen(this.state.gamestate.mySeat) }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={false}
                    rightSide={false}
                    textStyle={{ ...styles.shareHandText, color: this.state.playScreen.southUserText, alignSelf: 'center' }}
                    userName={this.state.gamestate.myUsername}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    marginLeft: moderateScale(8),
                    marginRight: moderateScale(8)
                  }}
                  source={this.state.playScreen.undoMySeatimg}
                />
              </View>
          }
          {
            (this.state.iskibitzer && this.state.southBidInfo) &&
            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center'
                }}
              >
                <View
                  style={{
                    width: scale(90), height: scale(45), backgroundColor: '#151515', alignItems: 'center',
                    justifyContent: 'center',
                    borderRightWidth: moderateVerticalScale(1)
                  }}
                >
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.southSharedType1}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : 
                  { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {this.state.infoScreen.southSharedValue1}
                  </Text>
                </View>
                <View
                  style={{
                    width: scale(90), height: scale(45),
                    justifyContent: 'center',
                    backgroundColor: '#151515',
                  }}
                >
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.shareHandText, width: scale(90), height: verticalScale(21) } :
                    { ...styles.shareHandText, width: scale(90), height: scale(21) }}>
                    {this.state.infoScreen.southSharedType2}
                  </Text>
                  <Text style={(this.state.isTablet && Platform.OS == 'ios') ? { ...styles.PaternText, width: scale(90), height: verticalScale(23) } : { ...styles.PaternText, width: scale(90), height: scale(23), }}>
                    {this.state.infoScreen.southSharedValue2}
                  </Text>
                </View>
              </View>
              {
                !this.state.northBidInfo &&
                <View style={{ height: scale(23), width: verticalScale(85), justifyContent: 'center', flexDirection: 'row' }}>
                  <CrownHost
                    hostName={this.props.hostName}
                    shareBidPlay={false}
                    rightSide={false}
                    textStyle={{ ...styles.shareHandText, color: this.state.playScreen.southUserText, alignSelf: 'center' }}
                    userName={this.state.gamestate.myUsername}
                  />
                </View>
              }
            </View>
          }
          {
            (this.state.iskibitzer && !this.state.southBidInfo) ?
            <TouchableOpacity
              style={{
                width: this.state.playScreen.TotalCardWidthSouth,
                height: scale(45),
                marginLeft: scale(1),
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignSelf: 'center'
              }}
              onPress={() => this.toggleBidInfoSection("S")}
            >
              {this.drawmycards()}
            </TouchableOpacity>
            :
            !this.state.iskibitzer &&
            <View
              style={{
                width: this.state.playScreen.TotalCardWidthSouth,
                height: scale(45),
                marginLeft: scale(1),
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignSelf: 'center'
              }}
            >
              {this.drawmycards()}
            </View>
          }
        </View>
      </>
    ) : this.props.isHomeUser ? 
        <View style={{width: '100%'}}>
          <EmptyChatView isFirstMessage={!this.props.host}/>  
        </View>: null
  }
}

const styles = ScaledSheet.create({
  claimMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: 'normal',
    color: '#ffffff'
  },
  claimingBodyAreaWest: {
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  claimingBodyAreaEast: {
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  shareHandTextHandName: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandTextHandNameBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#0a0a0a',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandTextHandNameRed: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#FF0C3E',
    textAlign: 'center',
    paddingTop: '3@ms'
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#6D6D6D',
    textAlign: 'center',
  },
  /************************************************ */
  playingEastWestCardTotalWidthHeightArea: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  playingCardTotalWidthHeightArea: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  totalWidthheightPositionDirectionArea: {
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  pointerEastWestAreaPosition: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  patternTotalColumnArea: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  patternTotalRowArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  hanNorthSauthTotalWidthHeightArea: {
    position: 'absolute',
    width: '100%',
    alignItems: 'center',
    flexDirection: 'column',
  },
  hanNameWidthHeightArea: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  patternWidthHeightArea: {
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  cardDisplayBodyNorth: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardDisplayBodyEast: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515',
    flexDirection: 'row-reverse'
  },
  cardPositionW: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardPositionE: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  directionPointerPosition: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#C000FF'
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#79E62B'
  },
  ContractTextNT: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FFE81D'
  },
  bidPlayText: {
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(55),
    color: '#6D6D6D'
  },
  CardPositionBorderAss: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#676767'
  },
  northCardPosition: {
    position: 'relative'
  },
  sauthCardPosition: {
    position: 'relative'
  },
  westCardPosition: {
    marginLeft: '12@ms',
    transform: [{ rotate: '-90deg' }]
  },
  eastCardPosition: {
    transform: [{ rotate: '90deg' }]
  },
  playCardNumberPosition: {
    position: 'absolute',
  },
  playCardNumberDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  playCardDiamond: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#04AEFF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  playCardHearts: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#FF0C3E',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  playCardClubs: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#79E62B',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardNumberSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  playCardSpade: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#C000FF',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  playCardDirectionPosition: {
    flexDirection: 'column'
  },
  DirectionText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#676767',
    textAlign: 'center'
  },
  DirectionTextActive: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },

  /*************************************** */
  cardPosition: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#C000FF'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FF0C3E'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#79E62B'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#04AEFF'
  },
  ContractTextActive: {
    top: '4@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
  },
  ContractText: {
    top: '4@ms',
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
  },
  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
    letterSpacing: 0.7
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#6D6D6D',
    paddingRight: '4@ms'
  },
  ScoreText: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(45),
    color: '#353535'
  },
  ScoreTextActive: {
    top: '5@ms',
    fontFamily: 'Roboto-Thin',
    fontSize: normalize(45),
    color: '#6D6D6D'
  },

  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(16),
    fontWeight: '400',
    color: '#FFFFFF',
    textAlign: 'center'
  },
  OpenText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(11),
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center',
    padding: 4
  }
});


// export default connect(mapStateToProps, mapDispatchToProps)(PlayScreen);
