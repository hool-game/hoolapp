import React,{Component} from 'react';
import { View, Text, StyleSheet, LogBox, Image, Dimensions, TouchableWithoutFeedback} from 'react-native';
const { jid } = require('@xmpp/client') 
import  { normalize } from '../../styles/global';
import { Strings } from '../../styles/Strings';
import { HOOL_CLIENT } from '../../shared/util';
import { HOOL_EVENTS } from '../../constants/hoolEvents';

var ScreenWidth=  Dimensions.get("window").width;
var ScreenHeight=  Dimensions.get("window").height;//-30;

var HeightBoxTop=(225/360*ScreenHeight);
var MenuBodyWidth=(91/360*ScreenHeight);
var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
var HeightWidthBoxes=(45/360*ScreenHeight);
let gamestate = {};
gamestate.hands = new Map();
gamestate.info = new Map();

export default class Redeal extends React.Component {
    constructor(props) {
        ///navigation.na
      super(props);
      
       this.state = {
          output: '',
          northAvtar: require('../../assets/images/circle_play.png'),
          eastAvtar: require('../../assets/images/circle_play.png'), 
          southAvtar: require('../../assets/images/circle_play.png'),
          westAvtar: require('../../assets/images/circle_play.png'),
          northUser: Strings.open,
          eastUser: Strings.open,
          southUser: Strings.open,
          westUser: Strings.open,
          iskibitzer:false,  
      }     
        LogBox.ignoreAllLogs();
    }
 
    componentDidMount() { 
        const { navigation } = this.props;
        
        const unsubscribe = HOOL_CLIENT.addListener(HOOL_EVENTS.STATE_UPDATE, (e) => {
            unsubscribe.remove();
            const hool = navigation.getParam('xmppconn'); 
            const tableID = navigation.getParam('tableID');  
         const gamestate = navigation.getParam('gameState');
            console.log('stateUpdate redeal');
            console.log(e);

            gamestate.northSharedType1=''; 
            gamestate.northSharedValue1='';
            gamestate.northSharedType2='';
            gamestate.northSharedValue2='';
            gamestate.eastSharedType1='';
            gamestate.eastSharedValue1='';
            gamestate.eastSharedType2='';
            gamestate.eastSharedValue2='';
            gamestate.southSharedType1='';
            gamestate.southSharedValue1='';
            gamestate.southSharedType2='';
            gamestate.southSharedValue2='';
            gamestate.westSharedType1='';
            gamestate.westSharedValue1='';
            gamestate.westSharedType2='';
            gamestate.westSharedValue2='';
            gamestate.winnerSuit='';
            gamestate.winnerLevel='';
            gamestate.winnerSide='';
            gamestate.winnerSuitImage='';
            gamestate.winnerdblRedbl='';

            if (gamestate.side && gamestate.side != 'none' && e.hands) {
            for (let h of e.hands) {  
                        gamestate.hands.set(h.side, h); 
            }  
             
            const interval = setInterval(() => {
                this.props.navigation.navigate('ShareInfo',{xmppconn : hool,tableID:tableID,gameState:gamestate});
                clearInterval(interval);
            }, 2000);  
        
            }
        });
        HOOL_CLIENT.addListener(HOOL_EVENTS.SCORE_BOARD, (e) => {
            console.log("scoreboard");
            console.log(e);
            var jidlocal=jid(e.user); 
            if(e.side==='N' || e.side==='S'){
                if(e.side==='N'){
                    this.setState({northUser:jidlocal._resource});
                    this.setState({northAvtar: require('../../assets/images/Profile-Icon-1.png')});
                }
                else if(e.side==='S')
                {
                    this.setState({southUser:jidlocal._resource});
                    this.setState({southAvtar: require('../../assets/images/Profile-Icon-1.png')});
                }
            }
            else if(e.side==='E' ||e.side==='W'){
                if(e.side==='E')
                {
                    this.setState({eastUser:jidlocal._resource});
                    this.setState({eastAvtar: require('../../assets/images/Profile-Icon-2.png')});
                }
                else if(e.side==='W')
                {
                    this.setState({westUser:jidlocal._resource});
                    this.setState({eastAvtar: require('../../assets/images/Profile-Icon-2.png')});
                }
            }
            
        });

        HOOL_CLIENT.addListener(HOOL_EVENTS.SCORE_BOARD_ERROR, (e) => {
            alert(`Failed to make scoreboard request on ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
        });
        this.scoreboardResponse(1);
      }
     
      scoreboardResponse(resp) {  
        const {navigation} = this.props;     
        console.log('response ' + resp); 
        const hool = navigation.getParam('xmppconn'); 
        const tableID = navigation.getParam('tableID'); 
        const gamestate = navigation.getParam('gameState');  
        hool.scoreboard(tableID, resp==1?'ready':'notready', gamestate.mySeat)
      }

    componentWillUnmount()
    {
        console.log('JoinTable closed');
        const { navigation } = this.props;
        const hool = navigation.getParam('xmppconn'); 
        const tableID = navigation.getParam('tableID');  
    }  

render(){    
return (
<View style={{backgroundColor:"#0a0a0a",height:"100%"}}>
    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", justifyContent:"space-between", width:"100%", height:HeightBoxTop, padding:13, backgroundColor:"#151515"}}>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/fast.png')} />
            <Text style={styles.clmText}>Fast</Text>
        </View>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/open.png')} />
            <Text style={styles.clmText}>{Strings.open}</Text>
        </View>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/on.png')} />
            <Text style={styles.clmText}>On</Text>
        </View>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/player_ass.png')} />
            <Text style={styles.clmText}>{Strings.open}</Text>
        </View>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            <Text style={styles.clmText}>{Strings.open}</Text>
        </View>
        <View style={{flexDirection:"row", alignItems:"center"}}>
            <Image source={require('../../assets/images/public.png')} />
            <Text style={styles.clmText}>Public</Text>
        </View>
    </View>
    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/settings.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/flag.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/revers.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/chat.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/on.png')} />
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", justifyContent:"space-between"}}>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <View style={{width:89, height:63, marginTop:5, alignItems:"center", justifyContent:"center"}}>
                {/* <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}> */}
                    <TouchableWithoutFeedback onPress={() => this.changeSeat('North')} disabled={this.state.iskibitzer}>
                        <Image source={this.state.northAvtar} /> 
                    </TouchableWithoutFeedback>
                {/* </View>                 */}
                <Text style={styles.OpenText}>{this.state.northUser}</Text>
                
            </View>
        </View>
        <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}>
            <View style={{width:70, height:63, marginLeft:5, alignItems:"center", justifyContent:"center"}}>
                <Text style={styles.OpenText}>{this.state.westUser}</Text>
                {/* <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center"}}> */}
                    <TouchableWithoutFeedback onPress={() => this.changeSeat('West')} disabled={this.state.iskibitzer}>
                        <Image source={this.state.westAvtar} /> 
                    </TouchableWithoutFeedback>
                {/* </View> */}
            </View>
            <View style={{width:61, height:61, justifyContent:"space-between", flexDirection:"column"}}>
                <Text style={styles.DirectionText}>N</Text>
                <View style={{flexDirection:"row", justifyContent:"space-between", alignItems:"center", paddingLeft:3, paddingRight:3}}>
                    <Text style={styles.DirectionText}>W</Text>
                    <Image source={require('../../assets/images/center_circle.png')} />
                    <Text style={styles.DirectionText}>E</Text>
                </View>
                <Text style={styles.DirectionText}>S</Text>
            </View>
            <View style={{width:70, height:63, marginLeft:5, alignItems:"center", justifyContent:"center"}}>
                <Text style={styles.OpenText}>{this.state.eastUser}</Text>
                {/* <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center"}}> */}
                    <TouchableWithoutFeedback onPress={() => this.changeSeat('East')} disabled={this.state.iskibitzer}>
                        <Image source={this.state.eastAvtar} /> 
                    </TouchableWithoutFeedback>
                {/* </View> */}
            </View>
        </View>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <View style={{width:89, height:63, marginTop:5, alignItems:"center", justifyContent:"center"}}>
                <Text style={styles.OpenText}>{this.state.southUser}</Text>
                {/* <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center"}}> */}
                    <TouchableWithoutFeedback onPress={() => this.changeSeat('South')} disabled={this.state.iskibitzer}>
                         <Image source={this.state.southAvtar} /> 
                    </TouchableWithoutFeedback>
                {/* </View> */}
            </View>
        </View>

   </View>

</View>

  );
}
}



const styles = StyleSheet.create({
    clmText: {
	  fontFamily:"Roboto-Light",
      fontSize: normalize(11),
      fontWeight: "400",
      color:"#6D6D6D",
      paddingLeft:10,
    },
    OpenText: {
	  fontFamily:"Roboto-Light",
      fontSize: normalize(11),
      fontWeight: "400",
      color:"#DBFF00",
      textAlign:"center", 
      padding:4,
    },
    DirectionText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
      }, 
  }); 
