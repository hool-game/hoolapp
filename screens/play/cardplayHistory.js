import React, {Component} from 'react';
import {View, Text, StyleSheet, Button, Image, Dimensions,TouchableOpacity, Alert} from 'react-native'; 
import { normalize } from 'jest-config';
//import global from '../shared/global';
import { SvgXml } from 'react-native-svg';
 import svgImages from '../../API/SvgFiles';
 import { APPLABELS } from '../../constants/applabels';
 
    var ScreenWidth=  Dimensions.get("window").width;
    var ScreenHeight=  Dimensions.get("window").height;

    var HeightBoxTop=(225/360*ScreenHeight);
    var MenuBodyWidth=(91/360*ScreenHeight);
    var ShareHehightWidth=(90/360*ScreenHeight);
    var ContractTextHeight=(22/360*ScreenHeight);
    var ContractDwnBoxHeight=(23/360*ScreenHeight);
    var ContractDwnMdlBoxWidth=(44/360*ScreenHeight);
    var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
    var HeightWidthBoxes=(45/360*ScreenHeight);
    var fourFiveSixWidth=(456/360*ScreenHeight);
    var TwoOneZeroHeight=(210/360*ScreenHeight);
    var nineHeight=(9/360*ScreenHeight);
    var threeSixWidth=(36/360*ScreenHeight);
    var fourTwoZeroWidth=(420/360*ScreenHeight);
    var twoFourWidth=(24/360*ScreenHeight);
    var oneZeroHeight=(10/360*ScreenHeight);
    var threeFiveHeight=(35/360*ScreenHeight);
    var oneFourWidth=(14/360*ScreenHeight);
    var oneThreeWidth=(13/360*ScreenHeight);
    var fourHeight=(4/360*ScreenHeight);
    var twoHeight=(2/360*ScreenHeight);
    var threeZeroWidth=(30/360*ScreenHeight);
    var twoZeroHeightWidth=(20/360*ScreenHeight);
    var sixHeight=(6/360*ScreenHeight);
    var oneSixHeight=(16/360*ScreenHeight);
    var fiveHeightTop=(5/360*ScreenHeight);
    var eightHeight=(8/360*ScreenHeight);
 
    export default  class History extends Component {
 
        constructor(props) {
          super(props);
          this.state = { 
            mySeat: '',  
            myLHOSeat: '',
            myPartnerSeat:'',
            myRHOSeat:'',
            dealer:'',
            TrickScoreEW: '00',
            TrickScoreNS: '00',
          };
        }
      
        componentDidMount(){
          const { navigation } = this.props;
          const hool = navigation.getParam('xmppconn');
         const gamestate = navigation.getParam('gameState'); 
         const   tableID = navigation.getParam('tableID');
         const cardsCnt=navigation.getParam('cardsCnt');
          this.setState({tableID:tableID, 
          mySeat: gamestate.mySeat,  
          myLHOSeat: gamestate.myLHOSeat,
          myPartnerSeat:gamestate.myPartnerSeat,
          myRHOSeat:gamestate.myRHOSeat,
        }); 
        } 

        componentWillUnmount()
        {
            this.subScribe.remove();  
        } 
       
 
    render() {  
return (
 
    <View style={{backgroundColor:"#0a0a0a",height:"100%"}}>

    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", width:"100%", height:HeightBoxTop, backgroundColor:"#151515"}}>
        <View style={{width:"100%", height:ContractTextHeight, borderBottomWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
            <Text style={styles.ContractTextHeader}>Contract</Text>
        </View>
        <View style={{width:"100%",flexDirection:"row", borderBottomWidth:1, borderBottomColor:"#0a0a0a"}}>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                {/* <Text style={styles.ContractTextHeader}>4</Text>  */}
                <Text style={styles.ContractTextSpade}>4</Text>
                {/*<Text style={styles.ContractTextHearts}>4</Text>
                <Text style={styles.ContractTextDiamond}>4</Text>
               <Text style={styles.ContractTextClubs}>4</Text> */}
            </View>
            <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                {/* <Text style={styles.ContractMdlText}>4</Text>
                <Image source={require('../../assets/images/hearts_inactive.png')} /> */}

                <Text style={styles.ContractTextSpade}>4</Text>
                <Image source={require('../../assets/images/spade.png')} />

                {/* <Text style={styles.ContractTextHearts}>4</Text>
                <Image source={require('../../assets/images/hearts.png')} /> */}

                {/* <Text style={styles.ContractTextDiamond}>4</Text>
                <Image source={require('../../assets/images/diamond.png')} /> */}

                {/* <Text style={styles.ContractTextClubs}>4</Text>
                <Image source={require('../../assets/images/clubs.png')} /> */}
            </View>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, alignItems:"center", justifyContent:"center"}}>
                {/* <Text style={styles.ContractTextHeader}>x4</Text> */}
                <Text style={styles.ContractTextSpade}>x4</Text>
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center" }}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
            <Text style={[styles.ContractText, {top:eightHeight}]}>N • S</Text>
            <Text style={[styles.bidPlayText, {top:fiveHeightTop}]}>{this.state.TrickScoreNS}</Text>
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#DBFF00", borderBottomWidth:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center" }}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
        <Text style={[styles.ContractText, {top:eightHeight}]}>E • W</Text>
            <Text style={[styles.bidPlayText, {top:fiveHeightTop}]}>{this.state.TrickScoreEW} </Text>
            </View>
        </View>
    </View>

    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/settings.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/flag.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/revers_2.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/chat.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/green_crx.png')} />
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
        <Text style={[styles.playHistoryText, {top:oneSixHeight, paddingLeft:threeFiveHeight}]}>{APPLABELS.TEXT_PLAY_HISTORY}</Text>

        <View style={[styles.playHistoryBody, {width:fourFiveSixWidth, height:TwoOneZeroHeight, bottom:oneFourWidth, left:nineHeight}]}>
       
            <View style={[styles.playHistoryBodyRowNumberTotalArea, {width:fourFiveSixWidth, height:nineHeight}]}>
                <View style={[styles.playHistoryBodyRowNumberDvf, {width:threeSixWidth, height:nineHeight}]}></View>
                <View style={[styles.playHistoryBodyRowNumberDve, {width:fourTwoZeroWidth, height:nineHeight}]}>
                    <View style={[styles.playHistoryBodyRowNumberDve, {width:fourTwoZeroWidth, height:nineHeight}]}>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>1</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>2</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>3</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>4</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>5</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>6</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>7</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>8</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>9</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>10</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>11</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>12</Text></View>
                        <View style={[styles.playHistoryBodyRowNumberDveWidth, {width:twoFourWidth, height:nineHeight}]}><Text style={[styles.playHistoryNmbText, {lineHeight:oneZeroHeight}]}>13</Text></View>
                    </View>
                </View>
            </View>

            <View style={[styles.playHistoryBodyRowCardTotalArea, {width:fourFiveSixWidth, height:threeFiveHeight}]}>
                <View style={[styles.playHistoryBodyRowCardDvFstBox, {width:oneFourWidth, height:threeFiveHeight, marginRight:oneThreeWidth,}]}>
                    <Text style={styles.playHistoryDirectionText}>{this.state.myLHOSeat}</Text>
                </View>  
                 <View style={{flexDirection:"row", alignItems:"center"}}>
                    <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}>
                        <SvgXml xml={svgImages.winner_dealer_line}/>  
                    </View>
                    <View style={[styles.playHistoryBodyRowSpadeCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                        <Text style={styles.playHistoryTotalCardNumberText}>A</Text>
                        <Image source={require('../../assets/images/spade_black.png')}/>
                    </View>
                </View>                    
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}>
                    <SvgXml xml={svgImages.winner_dealer_line}/>  
                </View>
                <View style={[styles.playHistoryBodyRowHeartCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:twoHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>A</Text>
                    <Image source={require('../../assets/images/hearts_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
                <View style={[styles.playHistoryBodyRowDiamondCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>A</Text>
                    <Image source={require('../../assets/images/diamond_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>A</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>2</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>  
            </View>

             <View style={[styles.playHistoryBodyRowCardTotalArea, {width:fourFiveSixWidth, height:threeFiveHeight}]}>
             <View style={[styles.playHistoryBodyRowCardDvFstBox, {width:oneFourWidth, height:threeFiveHeight, marginRight:oneThreeWidth,}]}><Text style={styles.playHistoryDirectionText}>{this.state.myPartnerSeat}</Text></View>
              <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}>
                 
            </View>
             <View style={[styles.playHistoryBodyRowSpadeCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>2</Text>
                    <Image source={require('../../assets/images/spade_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowHeartCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:twoHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>3</Text>
                    <Image source={require('../../assets/images/hearts_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowDiamondCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>4</Text>
                    <Image source={require('../../assets/images/diamond_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>5</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>K</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>  
            </View>

             <View style={[styles.playHistoryBodyRowCardTotalArea, {width:fourFiveSixWidth, height:threeFiveHeight}]}>
             <View style={[styles.playHistoryBodyRowCardDvFstBox, {width:oneFourWidth, height:threeFiveHeight, marginRight:oneThreeWidth,}]}><Text style={styles.playHistoryDirectionText}>{this.state.myRHOSeat}</Text></View>
              <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
             <View style={[styles.playHistoryBodyRowSpadeCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>3</Text>
                    <Image source={require('../../assets/images/spade_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowHeartCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:twoHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>3</Text>
                    <Image source={require('../../assets/images/hearts_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowDiamondCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>4</Text>
                    <Image source={require('../../assets/images/diamond_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>5</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>K</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View> 
            </View>

             <View style={[styles.playHistoryBodyRowCardTotalArea, {width:fourFiveSixWidth, height:threeFiveHeight}]}>
             <View style={[styles.playHistoryBodyRowCardDvFstBox, {width:oneFourWidth, height:threeFiveHeight, marginRight:oneThreeWidth,}]}><Text style={styles.playHistoryDirectionText}>{this.state.mySeat}</Text></View>
                  <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
             <View style={[styles.playHistoryBodyRowSpadeCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>4</Text>
                    <Image source={require('../../assets/images/spade_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowHeartCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:twoHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>3</Text>
                    <Image source={require('../../assets/images/hearts_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowDiamondCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>

                    <Text style={styles.playHistoryTotalCardNumberText}>4</Text>
                    <Image source={require('../../assets/images/diamond_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>5</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}><Image source={require('../../assets/images/cnt_line.png')}/></View>
                <View style={[styles.playHistoryBodyRowClubsCardBg, {width:twoFourWidth, height:threeFiveHeight, paddingBottom:fourHeight}]}>
                    <Text style={styles.playHistoryTotalCardNumberText}>K</Text>
                    <Image source={require('../../assets/images/clubs_black.png')}/>
                </View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowCardCntNxtLine, {width:nineHeight, height:threeFiveHeight}]}></View>
                <View style={[styles.playHistoryBodyRowBlankCardTotalAreaBgAss, {width:twoFourWidth, height:threeFiveHeight}]}></View> 
            </View>
        </View>

        <View style={{position:"absolute", width:"100%", left:twoZeroHeightWidth, height:sixHeight, bottom:threeZeroWidth,}}>
        <View style={[styles.playHistoryBodySliderIcons, { width:twoZeroHeightWidth, }]}>
            <Image source={require('../../assets/images/play_history_slider_icon1.png')}/>
            <Image source={require('../../assets/images/play_history_slider_icon2.png')}/>
        </View>
        </View>
   </View>

</View>
 

  );
}
}
 


const styles = StyleSheet.create({
    playHistoryBodySliderIcons:{
        alignSelf:"center",
        justifyContent:"space-between",
        alignItems:"center",
        borderColor:"blue",
        borderWidth:0,
        flexDirection:"row",
    },
    playHistoryTotalCardNumberText:{
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "300",
        color:"#0a0a0a",
    },
    playHistoryBodyRowClubsCardBg:{
        alignItems:"center",
        borderColor:"#0a0a0a",
        borderWidth:1,
        borderRadius:4,
        backgroundColor:"#79E62B",
        flexDirection:"column",
        justifyContent:"space-around",
       
    },
    playHistoryBodyRowDiamondCardBg:{
        alignItems:"center",
        borderColor:"#0a0a0a",
        borderWidth:1,
        borderRadius:4,
        backgroundColor:"#04AEFF",
        flexDirection:"column",
        justifyContent:"space-around", 
    },
    playHistoryBodyRowHeartCardBg:{
        alignItems:"center",
        borderColor:"#0a0a0a",
        borderWidth:1,
        borderRadius:4,
        backgroundColor:"#FF0C3E",
        flexDirection:"column",
        justifyContent:"space-around",
        
    },
    playHistoryBodyRowSpadeCardBg:{
        alignItems:"center",
        borderColor:"#0a0a0a",
        borderWidth:1,
        borderRadius:4,
        backgroundColor:"#C000FF",
        flexDirection:"column",
        justifyContent:"space-around",
        
    },
    playHistoryBodyRowBlankCardTotalAreaBgAss:{
        alignItems:"center",
        justifyContent:"center",
        borderColor:"#151515",
        borderWidth:1,
        borderRadius:4,
        backgroundColor:"#151515",
    },
    playHistoryBodyRowCardCntNxtLine:{
        alignItems:"center",
        justifyContent:"center",
    },
    playHistoryDirectionText:{
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "300",
        color:"#6D6D6D",
    },
    playHistoryBodyRowCardDvFstBox:{
        alignItems:"center",
        justifyContent:"center",
        borderColor:"red",
        borderWidth:0,
    },
    playHistoryBodyRowCardTotalArea:{
        flexDirection:"row",
    },
    playHistoryNmbText:{
        fontFamily:"Roboto-Light",
        fontSize: 9,
        fontWeight: "300",
        color:"#6D6D6D",
    },
    playHistoryBodyRowNumberDveWidth:{
        alignItems:"center",
        justifyContent:"center",
        borderColor:"blue",
        borderWidth:0,
    },
    playHistoryBodyRowNumberDve:{
        borderColor:"blue",
        borderWidth:0,
        flexDirection:"row",
        justifyContent:"space-between",
    },
    playHistoryBodyRowNumberDvf:{
        position:"relative",
    },
    playHistoryBodyRowNumberTotalArea:{
        flexDirection:"row",
        justifyContent:"space-between",
    },
    playHistoryBody:{
        borderColor:"red",
        borderWidth:0,
        flexDirection:"column",
        justifyContent:"space-between",
    },
    playHistoryText:{
        position:"absolute",
        alignSelf:"center",
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "300",
        color:"#6D6D6D",
    },
      

    /********************************************************/

    
    ContractTextSpade: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#C000FF",
    },
    ContractTextHearts: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    ContractTextDiamond: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#04AEFF",
    },
    ContractTextClubs: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#79E62B",
    },
    bidPlayText: {
        // top:5,
        fontFamily:"Roboto-Thin",
        fontSize: 55,
        color:"#6D6D6D",
        fontWeight:"normal",
      },
      

    ContractText: {
        position:"absolute",
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
        // top:8,
    },
    
    ContractTextHeader: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
    },
    ContractTextBlack: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#0a0a0a",
    },
    ContractMdlText:{
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
        paddingRight:4,
    },
   

  });
 