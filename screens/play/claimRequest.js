import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  StatusBar
} from 'react-native';
import { normalize } from 'jest-config';
//import global from './shared/global';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { APPLABELS } from '../../constants/applabels';
const ViewBoxesWithColorAndText = () => {
  var ScreenWidth = Dimensions.get('window').width;
  var ScreenHeight = Dimensions.get('window').height;

  var HeightBoxTop = (225 / 360) * ScreenHeight;
  var MenuBodyWidth = (91 / 360) * ScreenHeight;
  var ShareHehightWidth = (90 / 360) * ScreenHeight;
  var ContractTextHeight = (22 / 360) * ScreenHeight;
  var ContractDwnBoxHeight = (23 / 360) * ScreenHeight;
  var ContractDwnMdlBoxWidth = (44 / 360) * ScreenHeight;
  var TotalCardWidthSouth = (530 / 640) * ScreenWidth;
  var TotalCardWidthNorth = (530 / 640) * ScreenWidth;
  var PlayBodyWidth = ScreenWidth - MenuBodyWidth;
  var HeightWidthBoxes = (45 / 360) * ScreenHeight;
  var HeightEastWestPalyCard = (360 / 360) * ScreenHeight;
  var EastWestPalyCardArea = (304 / 360) * ScreenHeight;
  var eastWestMrgLft = (110 / 360) * ScreenHeight;
  var fourThreeWidthHeight = (43 / 360) * ScreenHeight;
  var twoOneFiveWidth = (215 / 360) * ScreenHeight;
  var oneTwoNineHeight = (129 / 360) * ScreenHeight;
  var oneZeroEightWidth = (108 / 360) * ScreenHeight;
  var twoFourHeight = (24 / 360) * ScreenHeight;
  var oneSixSevenHeight = (167 / 360) * ScreenHeight;
  var threeHeightTop = (3 / 360) * ScreenHeight;
  var oneEightOneWidth = (181 / 360) * ScreenHeight;
  var sixEightHeight = (68 / 360) * ScreenHeight;
  var fiveZeroWidth = (50 / 360) * ScreenHeight;
  var fourNgtTop = (-4 / 360) * ScreenHeight;
  var twoWidthLeft = (2 / 360) * ScreenHeight;
  var fiveHeight = (5 / 360) * ScreenHeight;
  var fourHeight = (4 / 360) * ScreenHeight;
  var eightHeightTop = (8 / 360) * ScreenHeight;
  var oneFiveSevenHeight = (157 / 360) * ScreenHeight;

  var SampleNameArray = [
    'K',
    'Q',
    '3',
    'K',
    '8',
    '4',
    'A',
    '9',
    '7',
    '6',
    'Q',
    'J',
    '7'
  ];
  var hands = [
    {
      rank: 'K',
      img: svgImages.spade,
      suit: 'S',
      key: 'KS',
      SuiteColor: '#C000FF',
      idx: 0
    },
    {
      rank: 'Q',
      img: svgImages.spade,
      suit: 'S',
      key: 'QS',
      SuiteColor: '#C000FF',
      idx: 1
    },
    {
      rank: '10',
      img: svgImages.spade,
      suit: 'S',
      key: '10S',
      SuiteColor: '#C000FF',
      idx: 2
    },
    {
      rank: 'K',
      img: svgImages.hearts,
      suit: 'H',
      key: 'KH',
      SuiteColor: '#FF0C3E',
      idx: 3
    },
    {
      rank: '8',
      img: svgImages.hearts,
      suit: 'H',
      key: '8H',
      SuiteColor: '#FF0C3E',
      idx: 4
    },
    {
      rank: '4',
      img: svgImages.hearts,
      suit: 'H',
      key: '4H',
      SuiteColor: '#FF0C3E',
      idx: 5
    },
    {
      rank: 'A',
      img: svgImages.diamond,
      suit: 'D',
      key: 'AD',
      SuiteColor: '#04AEFF',
      idx: 6
    },
    {
      rank: '9',
      img: svgImages.diamond,
      suit: 'D',
      key: '9D',
      SuiteColor: '#04AEFF',
      idx: 7
    },
    {
      rank: '7',
      img: svgImages.diamond,
      suit: 'D',
      key: '7D',
      SuiteColor: '#04AEFF',
      idx: 8
    },
    {
      rank: '6',
      img: svgImages.diamond,
      suit: 'D',
      key: '6D',
      SuiteColor: '#04AEFF',
      idx: 9
    },
    {
      rank: 'Q',
      img: svgImages.clubs,
      suit: 'C',
      key: 'QC',
      SuiteColor: '#79E62B',
      idx: 10
    },
    {
      rank: 'J',
      img: svgImages.clubs,
      suit: 'C',
      key: 'JC',
      SuiteColor: '#79E62B',
      idx: 11
    },
    {
      rank: '7',
      img: svgImages.clubs,
      suit: 'C',
      key: '7C',
      SuiteColor: '#79E62B',
      idx: 12
    }
  ];

  const drawCards = () => {
    return hands.map(element => {
      return (
        <View
          style={[
            styles.cardDisplayBody,
            { width: ShareHehightWidth, height: HeightWidthBoxes }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPosition,
              {
                width: ContractTextHeight,
                marginTop: threeHeightTop,
                marginLeft: fourHeight
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <Image source={element.img} />
          </View>
        </View>
      );
    });
  };

  const drawCardsPartner = () => {
    return hands.map(element => {
      return (
        <View
          style={[
            styles.cardDisplayBodyNorth,
            { width: ShareHehightWidth, height: HeightWidthBoxes }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPosition,
              {
                width: ContractTextHeight,
                marginTop: threeHeightTop,
                marginLeft: fourHeight
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <Image source={element.img} />
          </View>
        </View>
      );
    });
  };

  const drawCardsLHO = () => {
    return hands.map(element => {
      return (
        <View
          style={[
            styles.cardDisplayBodyLHO,
            { width: ShareHehightWidth, height: HeightWidthBoxes }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPositionW,
              { width: ContractTextHeight, marginTop: threeHeightTop }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <Image source={element.img} />
          </View>
        </View>
      );
    });
  };

  const drawCardsRHO = () => {
    return hands.map(element => {
      return (
        <View
          style={[
            styles.cardDisplayBodyRHO,
            { width: ShareHehightWidth, height: HeightWidthBoxes }
          ]}
          key={element.key}
        >
          <View
            style={[
              styles.cardPositionE,
              {
                width: ContractTextHeight,
                marginTop: fourNgtTop,
                marginLeft: twoWidthLeft
              }
            ]}
          >
            <Text
              style={[styles.SpadeCardNumber, { color: element.SuiteColor }]}
            >
              {element.rank}
            </Text>
            <Image source={element.img} />
          </View>
        </View>
      );
    });
  };

  return (
    <View style={{ backgroundColor: '#0a0a0a', height: '100%' }}>
      <View
        style={{
          position: 'absolute',
          left: 0,
          width: MenuBodyWidth,
          height: '100%'
        }}
      >
        <View
          style={{
            flexDirection: 'column',
            width: '100%',
            height: HeightBoxTop,
            backgroundColor: '#151515'
          }}
        >
          <View
            style={{
              width: '100%',
              height: ContractTextHeight,
              borderBottomWidth: 1,
              borderBottomColor: '#0a0a0a',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text style={styles.ContractTextHeader}>Contract</Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              borderBottomWidth: 1,
              borderBottomColor: '#0a0a0a'
            }}
          >
            <View
              style={{
                width: ContractDwnBoxHeight,
                height: ContractDwnBoxHeight,
                borderRightWidth: 1,
                borderBottomColor: '#0a0a0a',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              {/* <Text style={styles.ContractTextHeader}>4</Text>  */}
              {/* <Text style={styles.ContractTextSpade}>4</Text> */}
              <Text style={styles.ContractTextHearts}>4</Text>
              {/*<Text style={styles.ContractTextDiamond}>4</Text>
               <Text style={styles.ContractTextClubs}>4</Text> */}
            </View>
            <View
              style={{
                width: ContractDwnMdlBoxWidth,
                height: ContractDwnBoxHeight,
                flexDirection: 'row',
                borderRightWidth: 1,
                borderBottomColor: '#0a0a0a',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text
                style={[
                  styles.ContractMdlText,
                  { paddingRight: threeHeightTop }
                ]}
              >
                4
              </Text>
              <Image source={require('./assets/images/hearts_inactive.png')} />
            </View>
            <View
              style={{
                width: ContractDwnBoxHeight,
                height: ContractDwnBoxHeight,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              {/* <Text style={styles.ContractTextHeader}>x4</Text> */}
              <Text style={styles.ContractTextHearts}>x4</Text>
            </View>
          </View>
          <View
            style={{
              height: ShareHehightWidth,
              width: ShareHehightWidth,
              borderBottomColor: '#323232',
              borderBottomWidth: 1,
              backgroundColor: '#151515',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                width: '100%',
                height: '100%',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text style={[styles.ContractText, { top: eightHeightTop }]}>
                N • S
              </Text>
              <Text style={[styles.bidPlayText, { top: fiveHeight }]}>02</Text>
            </View>
          </View>
          <View
            style={{
              height: ShareHehightWidth,
              width: ShareHehightWidth,
              borderBottomColor: '#323232',
              borderBottomWidth: 1,
              alignItems: 'center',
              backgroundColor: '#151515',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                width: '100%',
                height: '100%',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text style={[styles.ContractText, { top: eightHeightTop }]}>
                E • W
              </Text>
              <Text style={[styles.bidPlayText, { top: fiveHeight }]}>04</Text>
            </View>
          </View>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <View
            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 1 }}
          >
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                marginRight: 1,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/settings.png')} />
            </View>
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/green_crx.png')} />
            </View>
          </View>
          <View
            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 1 }}
          >
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                marginRight: 1,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/cheat_sheet_ass.png')} />
            </View>
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/revers_2.png')} />
            </View>
          </View>
          <View
            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 1 }}
          >
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                marginRight: 1,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/chat_claim.png')} />
            </View>
            <View
              style={{
                width: HeightWidthBoxes,
                height: HeightWidthBoxes,
                backgroundColor: '#151515',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image source={require('./assets/images/on.png')} />
            </View>
          </View>
        </View>
      </View>

      <View
        style={{
          position: 'absolute',
          right: 0,
          width: PlayBodyWidth,
          height: '100%',
          flexDirection: 'column',
          justifyContent: 'space-between'
        }}
      >
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <View
            style={[
              styles.hanNorthSauthTotalWidthHeightArea,
              { height: sixEightHeight }
            ]}
          >
            <View
              style={[
                styles.patternTotalColumnArea,
                { height: HeightWidthBoxes }
              ]}
            >
              <View
                style={[
                  styles.patternTotalRowArea,
                  { width: oneEightOneWidth, height: HeightWidthBoxes }
                ]}
              >
                <View
                  style={[
                    styles.patternWidthHeightArea,
                    { width: ShareHehightWidth, height: HeightWidthBoxes }
                  ]}
                >
                  <Text
                    style={[
                      styles.shareHandText,
                      { paddingTop: threeHeightTop }
                    ]}
                  >
                    Pattern
                  </Text>
                  <Text style={styles.PaternText}>4,4,3,2</Text>
                </View>
                <View
                  style={[
                    styles.patternWidthHeightArea,
                    { width: ShareHehightWidth, height: HeightWidthBoxes }
                  ]}
                >
                  <Text
                    style={[
                      styles.shareHandText,
                      { paddingTop: threeHeightTop }
                    ]}
                  >
                    Spades
                  </Text>
                  <Text style={styles.PaternText}>4</Text>
                </View>
              </View>
            </View>

            {/* <View style={{width:TotalCardWidthNorth, height:HeightWidthBoxes,marginLeft:fiveZeroWidth, flexDirection:"row", justifyContent:"space-around",alignSelf:"center",}}>
                    { drawCardsPartner() }  
                </View> */}

            <View
              style={[
                styles.hanNameWidthHeightArea,
                { height: ContractDwnBoxHeight }
              ]}
            >
              <Text style={styles.shareHandTextHandName}>
              {APPLABELS.TEXT_HANSOLOMAN_PARTNER}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <View
            style={{
              position: 'absolute',
              left: threeHeightTop,
              borderWidth: 0,
              borderColor: 'red',
              width: sixEightHeight,
              height: HeightEastWestPalyCard,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                width: HeightEastWestPalyCard,
                height: sixEightHeight,
                transform: [{ rotate: '-90deg' }]
              }}
            >
              {/* <View style={[styles.patternTotalColumnArea, {height:HeightWidthBoxes}]}>
                      <View style={[styles.patternTotalRowArea, {width:oneEightOneWidth, height:HeightWidthBoxes}]}>
                          <View style={[styles.patternWidthHeightArea, {width:ShareHehightWidth, height:HeightWidthBoxes}]}>
                            <Text style={[styles.shareHandText, {paddingTop:threeHeightTop}]}>>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                          <View style={[styles.patternWidthHeightArea, {width:ShareHehightWidth, height:HeightWidthBoxes}]}>
                            <Text style={[styles.shareHandText, {paddingTop:threeHeightTop}]}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>  */}

              <View
                style={{
                  width: EastWestPalyCardArea,
                  height: HeightWidthBoxes,
                  marginLeft: eastWestMrgLft,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignSelf: 'center'
                }}
              >
                {drawCardsLHO()}
              </View>

              <View
                style={[
                  styles.hanNameWidthHeightArea,
                  { height: ContractDwnBoxHeight }
                ]}
              >
                <Text style={styles.shareHandTextHandName}>
                {APPLABELS.TEXT_HANSOLOMAN_LHO}
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              position: 'absolute',
              right: 0,
              width: sixEightHeight,
              height: HeightEastWestPalyCard,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                width: HeightEastWestPalyCard,
                height: sixEightHeight,
                transform: [{ rotate: '90deg' }]
              }}
            >
              {/* <View style={[styles.patternTotalColumnArea, {height:HeightWidthBoxes}]}>
                     <View style={[styles.patternTotalRowArea, {width:oneEightOneWidth, height:HeightWidthBoxes}]}>
                        <View style={[styles.patternWidthHeightArea, {width:ShareHehightWidth, height:HeightWidthBoxes}]}>
                        <Text style={[styles.shareHandText, {paddingTop:threeHeightTop}]}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                        <View style={[styles.patternWidthHeightArea, {width:ShareHehightWidth, height:HeightWidthBoxes}]}>
                        <Text style={[styles.shareHandText, {paddingTop:threeHeightTop}]}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>   */}

              <View
                style={{
                  width: EastWestPalyCardArea,
                  marginRight: -1 * eastWestMrgLft,
                  height: HeightWidthBoxes,
                  justifyContent: 'space-around',
                  flexDirection: 'row-reverse',
                  alignSelf: 'center'
                }}
              >
                {drawCardsRHO()}
              </View>

              <View
                style={[
                  styles.hanNameWidthHeightArea,
                  { height: ContractDwnBoxHeight }
                ]}
              >
                <Text
                  style={[styles.shareHandText, { paddingTop: threeHeightTop }]}
                >
                  {APPLABELS.TEXT_HANSOLO_RHO}
                </Text>
              </View>
            </View>
          </View>

          {/* <View style={[styles.claimRequestBody, {width:twoOneFiveWidth, height:oneSixSevenHeight,}]}>
            <View style={[styles.claimRequestTextBody, { width:twoOneFiveWidth, height:oneFiveSevenHeight}]}>
                <Text style={styles.claimRequestTextItems}>Claim/Concede</Text>
                <Text style={styles.claimRequestTextItems}>Change deal</Text>
                <Text style={styles.claimRequestTextItems}>Leave seat</Text>
                <Text style={styles.claimRequestTextItems}>Leave table</Text>
            </View>
        </View>  */}
        </View>

        <View
          style={[
            styles.claimRequestNextBody,
            {
              width: twoOneFiveWidth,
              height: oneSixSevenHeight,
              top: ShareHehightWidth
            }
          ]}
        >
          <View
            style={[
              styles.claimRequestBodyClm,
              { width: twoOneFiveWidth, height: oneSixSevenHeight }
            ]}
          >
            <View style={styles.claimRequestTopRow}>
              <View
                style={[
                  styles.claimRequestTopRowBox,
                  { width: oneZeroEightWidth, height: twoFourHeight }
                ]}
              >
                <Text style={styles.claimTxtGreen}>Claim</Text>
              </View>
              <View
                style={[
                  styles.claimRequestTopRowBox,
                  { width: oneZeroEightWidth, height: twoFourHeight }
                ]}
              >
                <Text style={styles.claimTxtWhite}>Concede</Text>
              </View>
            </View>
            <View
              style={[
                styles.claimRequestNumberTotalBody,
                { width: twoOneFiveWidth, height: oneTwoNineHeight }
              ]}
            >
              <View style={styles.claimRequestNumberBodyRow}>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>1</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>2</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>3</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtGreen}>4</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>5</Text>
                </View>
              </View>
              <View style={styles.claimRequestNumberBodyRow}>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>6</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>7</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>8</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>9</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtWhite}>10</Text>
                </View>
              </View>
              <View style={styles.claimRequestNumberBodyRow}>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Image source={require('./assets/images/red_crx.png')} />
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtBlack}>11</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBoxGreen,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtBlack}>12</Text>
                </View>
                <View
                  style={[
                    styles.claimRequestNumberBodyRowBox,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Text style={styles.claimTxtBlack}>13</Text>
                </View>

                <View
                  style={[
                    styles.claimRequestNumberBodyRowBoxGreen,
                    {
                      width: fourThreeWidthHeight,
                      height: fourThreeWidthHeight
                    }
                  ]}
                >
                  <Image source={require('./assets/images/black_lock.png')} />
                </View>
              </View>
            </View>
          </View>
        </View>

        <View
          style={[
            styles.hanNorthSauthTotalWidthHeightArea,
            { height: sixEightHeight }
          ]}
        >
          <View
            style={[
              styles.hanNameWidthHeightArea,
              { height: ContractDwnBoxHeight }
            ]}
          >
            <Text
              style={[styles.shareHandText, { paddingTop: threeHeightTop }]}
            >
              My-Han-Solo
            </Text>
          </View>
          <View
            style={{
              width: TotalCardWidthSouth,
              height: HeightWidthBoxes,
              marginLeft: fiveZeroWidth,
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignSelf: 'center'
            }}
          >
            {drawCards()}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  claimRequestNumberBodyRowBoxGreen: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    borderBottomColor: '#0a0a0a',
    borderBottomWidth: 1,
    backgroundColor: '#DBFF00'
  },
  claimRequestNumberBodyRowBox: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    borderBottomColor: '#0a0a0a',
    borderBottomWidth: 1,
    backgroundColor: '#151515'
  },
  claimRequestNumberBodyRow: {
    flexDirection: 'row'
  },
  claimRequestNumberTotalBody: {
    flexDirection: 'column'
  },
  claimTxtBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '300',
    color: '#0a0a0a',
    textAlign: 'center'
  },
  claimTxtWhite: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '300',
    color: '#FFFFFF',
    textAlign: 'center'
  },
  claimTxtGreen: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#DBFF00',
    textAlign: 'center'
  },
  claimRequestTopRowBox: {
    borderRightWidth: 1,
    borderRightColor: '#0a0a0a',
    backgroundColor: '#151515',
    alignItems: 'center',
    justifyContent: 'center'
  },
  claimRequestTopRow: {
    flexDirection: 'row'
  },
  claimRequestBodyClm: {
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  claimRequestTextItems: {
    fontFamily: 'Roboto-Light',
    fontSize: 14,
    fontWeight: '400',
    color: '#ffffff',
    textAlign: 'center'
  },
  claimRequestTextBody: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  claimRequestBody: {
    position: 'absolute',
    borderColor: 'red',
    borderWidth: 0
  },
  claimRequestNextBody: {
    position: 'absolute',
    borderColor: 'red',
    borderWidth: 0,
    alignSelf: 'center'
  },

  /************** CLAIM CLOSE ******************* */
  // tkbImgMrgTp:{
  //     marginTop:4,
  //     marginLeft:8,
  //     marginRight:8,
  // },
  shareHandTextHandName: {
    fontFamily: 'Roboto-Light',
    fontSize: 11,
    fontWeight: '400',
    color: '#323232',
    textAlign: 'center',
    paddingTop: 3
  },
  shareHandTextHandNameBlack: {
    fontFamily: 'Roboto-Light',
    fontSize: 11,
    fontWeight: '400',
    color: '#0a0a0a',
    textAlign: 'center',
    paddingTop: 3
  },
  shareHandTextHandNameRed: {
    fontFamily: 'Roboto-Light',
    fontSize: 11,
    fontWeight: '400',
    color: '#323232',
    textAlign: 'center',
    paddingTop: 3
  },
  shareHandText: {
    fontFamily: 'Roboto-Light',
    fontSize: 11,
    fontWeight: '400',
    color: '#323232',
    textAlign: 'center'
  },

  patternTotalColumnArea: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center'
  },
  patternTotalRowArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  hanNorthSauthTotalWidthHeightArea: {
    width: '100%',
    alignItems: 'flex-end',
    flexDirection: 'column'
  },
  hanNameWidthHeightArea: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  patternWidthHeightArea: {
    backgroundColor: '#151515',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardDisplayBodyNorth: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardDisplayBodyLHO: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardDisplayBodyRHO: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515',
    flexDirection: 'row-reverse'
  },
  cardPositionW: {
    marginLeft: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardPositionE: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  ContractTextSpade: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#C000FF'
  },
  ContractTextHearts: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  ContractTextDiamond: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  ContractTextClubs: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  bidPlayText: {
    fontFamily: 'Roboto-Thin',
    fontSize: 55,
    color: '#323232',
    fontWeight: 'normal'
  },
  cardDisplayBody: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderWidth: 1,
    borderColor: '#0a0a0a',
    backgroundColor: '#151515'
  },
  cardPosition: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  SpadeCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    fontWeight: '400',
    color: '#323232'
  },
  heartsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    fontWeight: '400',
    color: '#323232'
  },
  clubsCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    fontWeight: '400',
    color: '#323232'
  },
  diamondCardNumber: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    fontWeight: '400',
    color: '#323232'
  },
  ContractTextHeader: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  ContractText: {
    position: 'absolute',
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  ContractMdlText: {
    fontFamily: 'Roboto-Light',
    fontSize: 13,
    fontWeight: '400',
    color: '#323232'
  },
  PaternText: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    fontWeight: '400',
    color: '#323232',
    textAlign: 'center'
  }
});

export default ViewBoxesWithColorAndText;
