/*import React, { Component } from 'react';

export class svgImg extends Component {

  getSvg() {
    var svgImg;
    if (bid.suit == 'S') {
        return (bid.won)
    }
  }

  render() {
    return <div>svgImg</div>;
  }
}

export default svgImg;

/*import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';


export default function svgImg() {

getBGStyleByBid(bid) {
    if (bid.type == 'pass') 
      return styles.biddingHistoryBodyRowPassBg;
    else if (bid.type == 'level') {
      if (bid.suit == 'S')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowSpadeCardBgSolid
          : styles.biddingHistoryBodyRowSpadeCardBg;
      else if (bid.suit == 'D')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowDiamondCardBgSolid
          : styles.biddingHistoryBodyRowDiamondCardBg;
      else if (bid.suit == 'H')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowHeartCardBgSolid
          : styles.biddingHistoryBodyRowHeartCardBg;
      else if (bid.suit == 'C')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddindHistoryBodyRowClubsCardBgSolid
          : styles.biddindHistoryBodyRowClubsCardBg;
      else if (bid.suit == 'NT')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBodyRowNtCardBgSolid
          : styles.biddingHistoryBodyRowNtCardBg;
    } else if (bid.type == 'double') {
      return bid.force_won
        ? styles.biddingHistoryBodyRowRXXCardBgSolid
        : styles.biddingHistoryBodyRowRXXCardBgBdr;
    } else if (bid.type == 'redouble') {
      return styles.biddingHistoryBodyRowRXXCardBgSolid;
    } else {
      return styles.biddingHistoryBodyRowBlankBg;
    }
  }

  getTextBGStyleByBid(bid) {
    if (bid.type == 'pass') return styles.biddingHistoryBodyRowPassCardBgText;
    else if (bid.type == 'level') {
      if (bid.suit == 'S')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowSpadeCardBgText;
      else if (bid.suit == 'D')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowDiamondCardBgText;
      else if (bid.suit == 'H')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowHeartsCardBgText;
      else if (bid.suit == 'C')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowClubsCardBgText;
      else if (bid.suit == 'NT')
        return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
          bid.force_won
          ? styles.biddingHistoryBlackText
          : styles.biddingHistoryBodyRowNtCardBgText;
    } else if (bid.type == 'double') {
      return bid.force_won
        ? styles.biddingHistoryBlackText
        : styles.biddingHistoryTextWhite;
    } else if (bid.type == 'redouble') {
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? styles.biddingHistoryBlackText
        : styles.biddingHistoryTextWhite;
    }
  }
  
  getSuitSVG(bid) {
    if (bid.suit == 'S')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.spade_black
        : svgImages.spade;
    else if (bid.suit == 'D')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.diamond_black
        : svgImages.diamond;
    else if (bid.suit == 'H')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.hearts_black
        : svgImages.hearts;
    else if (bid.suit == 'C')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.clubs_black
        : svgImages.clubs;
    else if (bid.suit == 'NT')
      return (bid.won && parseInt(bid.round) == this.state.lastBidRound) ||
        bid.force_won
        ? svgImages.nt_black
        : svgImages.nt_yellow;
  }
  getBidText(bid) {
    if (bid.type == 'pass') return 'P';
    else if (bid.type == 'level') {
      return bid.level;
    } else if (bid.type == 'double') {
      return 'x2';
    } else if (bid.type == 'redouble') {
      return 'x4';
    }
  }
  
  getCircleView(bid) {
    return (
      <View
        style={[
          this.getBGStyleByBid(bid),
          { width: scale(28), height: scale(28),
          }
        ]}
      >
        <Text style={this.getTextBGStyleByBid(bid)}>
          {this.getBidText(bid)}
        </Text>
        {bid.suit && (
          <SvgXml
            style={{...styles.biddingCardSize, maxWidth:scale(8), maxHeight: scale(8)}}
            xml={this.getSuitSVG(bid)}
          />
        )}
      </View>
    );
  }

  
  }
  */
/**
 * {
    "statusCode": 0,
    "errorResponseDTO": {
        "errorCode": 0,
        "errorMessage": null
    },
    "lobbyList": [
        {
            "name": "table1",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "s",
            "showProfileIcon": true
        },
        {
            "name": "table2",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "e",
            "showProfileIcon": true
        },
        {
            "name": "table3",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "w",
            "showProfileIcon": true
        },
        {
            "name": "table4",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "",
            "showProfileIcon": true
        },
        {
            "name": "table5",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false
        },
        {
            "name": "table6",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false
        }
    ],
    "friendsList": [
        {
            "name":"Ganesh",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "",
            "isFriend": true,
            "isRequestPending": false,
            "showProfileIcon": true,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name":"Kumar",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "isFriend": true,
            "isRequestPending": false,
            "showProfileIcon": false,
            "isMessageEnabled": false,
            "isHomeUser": false,
            "isFirstMessage": true
        },
        {
            "name": "Hari",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "",
            "showProfileIcon": true,
            "isFriend": true,
            "isRequestPending": true,
            "isMessageEnabled": true,
            "isHomeUser": false,
            "isFirstMessage": true
        },
        {
            "name": "Saravana",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "e",
            "showProfileIcon": true,
            "isFriend": false,
            "isRequestPending": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Praveen",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "w",
            "showProfileIcon": true,
            "isFriend": false,
            "isRequestPending": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Dharshan",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "",
            "showProfileIcon": true,
            "isFriend": false,
            "isRequestPending": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Deepak",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false,
            "isFriend": false,
            "isRequestPending": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "K23",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false,
            "isFriend": false,
            "isRequestPending": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Guhan",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "isFriend": true,
            "isRequestPending": false,
            "showProfileIcon": false,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Friend Request Pending",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "isFriend": false,
            "isRequestPending": true,
            "showProfileIcon": true,
            "isMessageEnabled": false,
            "isHomeUser": false
        },
        {
            "name": "Profile with Message enabled",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "isFriend": true,
            "isRequestPending": false,
            "showProfileIcon": false,
            "isMessageEnabled": true,
            "isHomeUser": false
        },
        {
            "name": "Home User",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "isFriend": true,
            "isRequestPending": false,
            "showProfileIcon": false,
            "isMessageEnabled": false,
            "isHomeUser": true
        }
    ],
    "tableList": [
        {
            "name": "Table Messages",
            "avatarImage": "all",
            "status": 1,
            "indication": false,
            "indicatorType": "all",
            "showProfileIcon": true
        },
        {
            "name": "north",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "e",
            "showProfileIcon": true
        },
        {
            "name": "south",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "w",
            "showProfileIcon": true
        },
        {
            "name": "east",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "s",
            "showProfileIcon": true
        },
        {
            "name": "west",
            "avatarImage": "avatar",
            "status": 1,
            "indication": true,
            "indicatorType": "h",
            "showProfileIcon": false
        },
        {
            "name": "Kibitzer",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false
        },
        {
            "name": "Kibitzer12",
            "avatarImage": "avatar",
            "status": 1,
            "indication": false,
            "indicatorType": "",
            "showProfileIcon": false
        }
    ],
    "notificationList": [],
    "emptyChat": [],
    "usersList": [
        {
            "name":"Balaji",
            "jid":"balaji@hool.org"
        },
        {
            "name":"Naveenkumar",
            "jid":"naveen@hool.org"
        },
        {
            "name":"Sam",
            "jid":"sami@hool.org"
        },
        {
            "name":"Jeelu",
            "jid":"jeelu23@hool.org"
        },
        {
            "name":"Nand",
            "jid":"nand@hool.org"
        }
    ]
}


 */