import React,{Component} from 'react';
import {View, Text, StyleSheet, Button, Image, Dimensions, TouchableOpacity} from 'react-native';
import svgImages from '../../API/SvgFiles';
import { SvgXml } from 'react-native-svg';
import { HOOL_CLIENT } from '../../shared/util';
import { HOOL_EVENTS } from '../../constants/hoolEvents';

     var ScreenWidth=  Dimensions.get("window").width;
    var ScreenHeight=  Dimensions.get("window").height;//-30;

    var HeightBoxTop=(225/360*ScreenHeight);
    var MenuBodyWidth=(91/360*ScreenHeight);
    var ShareHehightWidth=(90/360*ScreenHeight);
    var ContractTextHeight=(22/360*ScreenHeight);
    var ContractDwnBoxHeight=(23/360*ScreenHeight);
    var ContractDwnMdlBoxWidth=(44/360*ScreenHeight);
    var TotalCardWidthSouth=(530/640*ScreenWidth);
    var TotalCardWidthNorth=(530/640*ScreenWidth);

    var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
    var HeightWidthBoxes=(45/360*ScreenHeight);
    var HeightEastWestPalyCard=(360/360*ScreenHeight);
    var EastPlayCardArea=(304/360*ScreenHeight);
    var WestPlayCardArea=(304/360*ScreenHeight);
    var NorthCardWithKibitzerArea=(400/640*ScreenWidth);
    
    var SampleNameArray = [ "K", "Q", "3", "K", "8", "4", "A", "9", "7", "6", "Q", "J", "7" ];
    var hands = [
        {"rank": "K","img": require('../../assets/images/spade.png'),"suit":"spade","key":"K_spade","SuiteColor":"#C000FF"},
        {"rank": "Q","img": require('../../assets/images/spade.png'),"suit":"spade","key":"Q_spade","SuiteColor":"#C000FF"},
        {"rank": "10","img": require('../../assets/images/spade.png'),"suit":"spade","key":"10_spade","SuiteColor":"#C000FF"},
        {"rank": "K","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"K_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "8","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"8_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "4","img": require('../../assets/images/hearts.png'),"suit":"hearts","key":"4_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "A","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"A_diamond","SuiteColor":"#04AEFF"},
        {"rank": "9","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"9_diamond","SuiteColor":"#04AEFF"},
        {"rank": "7","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"7_diamond","SuiteColor":"#04AEFF"},
        {"rank": "6","img": require('../../assets/images/diamond.png'),"suit":"diamond","key":"6_diamond","SuiteColor":"#04AEFF"},
        {"rank": "Q","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"Q_clubs","SuiteColor":"#79E62B"},
        {"rank": "J","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"J_clubs","SuiteColor":"#79E62B"},
        {"rank": "7","img": require('../../assets/images/clubs.png'),"suit":"clubs","key":"7_clubs","SuiteColor":"#79E62B"}
      ]
       
      var eastHands=hands;
    var westHands=hands;
    var northHands=hands;
    var southHands=hands;
      var Numberof_S=0;
      var Numberof_H=0;
      var Numberof_C=0;
      var Numberof_D=0;
      var southcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
      var northcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
      var eastcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
      var westcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
        export default class Bidding extends React.Component {
          constructor(props) {
              ///navigation.na
            super(props);
             this.state = { 
                 dummycardsdrawn:false, 
                 Numberof_D:0,
                 NumberofDummy_C:0,
                 NumberofDummy_D:0,
                 NumberofDummy_H:0,
                 NumberofDummy_S:0,
                 NSStyle:styles.ContractText, 
                 EWStyle:styles.ContractText,
                 NSScoreStyle:styles.ShareText,
                 EWScoreStyle:styles.ShareText,
                 disablemyCards_C:true,
                  disablemyCards_D:true,
                  disablemyCards_H:true,
                  disablemyCards_S:true,
                  disabledummyCards_C:true,
                  disabledummyCards_D:true,
                  disabledummyCards_H:true,
                  disabledummyCards_S:true,
                  Numof_C:0,
                 Numberof_D:0,
                 Numberof_H:0,
                 Numof_S:0,
                  trick_Suit:'',
                  output: '',
                  tableID:'',
                  mySeat: 'S',
                  myLHOSeat: 'W', 
                  myRHOSeat: 'E',
                  myPartnerSeat: 'N',
                  DummySeat:'RHO',
                  DummyPlayed:false,
                  directionImage:'',
                  northUser: '',
                  eastUser: '',
                  southUser: '',
                  westUser: '', 
                  northSharedType1: '',
                  northSharedValue1 :'',
                  northSharedType2: '',
                  northSharedValue2 :'',
                  eastSharedType1: '',
                  eastSharedValue1 :'',
                  eastSharedType2: '',
                  eastSharedValue2 :'',
                  southSharedType1: '',
                  southSharedValue1 :'',
                  southSharedType2: '',
                  southSharedValue2 :'',
                  westSharedType1: '',
                  westSharedValue1 :'',
                  westSharedType2: '',
                  westSharedValue2 :'', 
                  cardHCPTextColor :'#FFFFFF',
                  cardPatternTextColor :'#FFFFFF',
                  cardSuitTextColor :'#FFFFFF',
                  DirectionTextPartner:styles.DirectionText,
                  DirectionTextLHO:styles.DirectionText,
                  DirectionTextRHO:styles.DirectionText,
                  DirectionTextMySeat:styles.DirectionText,
                  northUserText: '#6D6D6D',
                  eastUserText: '#6D6D6D',
                  southUserText: '#6D6D6D',
                  westUserText: '#6D6D6D',
                  winnerSide:'',
                  winnerLevel:'',
                  winnerSuit:'',
                  winnerSuitImage:'',
                  winnerTextColor:'',
                  winnerdblRedbl:'', 
                  PartnerPlayed:false,
                  LHOPlayed:false,
                  RHOPlayed:false,
                  MyCard:false,
                  LHORank:'',
                  RHORank:'',
                  PartnerRank:'',
                  myCardStyle:'',
                  myCardNumberstyle:'',
                  myRank:'',
                  mySuit:'',
                  TrickScoreNS:'0',
                  TrickScoreEW:'0',
                  
              };
            southcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
            northcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
            eastcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
            westcardsShown=['flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex','flex'];
            //this.dynamicStates(); 
          }
  
          getTableInfo() { 
              const {navigation} = this.props;
              const hool = navigation.getParam('xmppconn'); 
              const tableID = navigation.getParam('tableID');
              this.setState({tableID:tableID});
              const gamestate = navigation.getParam('gameState'); 
              //console.log('goto to play :');
              //console.log(gamestate);   
              let myHand = gamestate.hands;
              //console.log(myHand); 
              myHand.forEach((h,key) => {
                  //console.log('Side :' + key);
                  if(key==='S'){
                    southHands= this.displayCards(h.cards);
                   
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({southSharedType1:suitType,southSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({southSharedType2:suitType,southSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({southSharedType1:suitType,southSharedValue1:h.infoShared[0].value});
                    }
                }
                else if(key==='W'){
                    westHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({westSharedType1:suitType,westSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({westSharedType2:suitType,westSharedValue2:h.infoShared[1].value}) ;                        
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({westSharedType1:suitType,westSharedValue1:h.infoShared[0].value});
                    }

                }
                else if(key==='N'){
                    northHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({northSharedType1:suitType,northSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({northSharedType2:suitType,northSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({northSharedType1:suitType,northSharedValue1:h.infoShared[0].value});
                    }
                }
                else if(key==='E'){
                    eastHands= this.displayCards(h.cards);
                    if(h.infoShared.length>1){
                        var suitType=h.infoShared[0].type;
                        console.log(suitType);
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({eastSharedType1:suitType,eastSharedValue1:h.infoShared[0].value});
                        suitType=h.infoShared[1].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                         this.setState({eastSharedType2:suitType,eastSharedValue2:h.infoShared[1].value})   
                    }
                    else if(h.infoShared.length>0){
                        var suitType=h.infoShared[0].type;
                        if(suitType==='D')
                        {
                            suitType ='Diamonds';
                        }
                        else if(suitType==='C')
                        {
                            suitType ='Clubs';
                        }
                        else if(suitType==='H')
                        {
                            suitType ='Hearts';
                        }
                        else if(suitType==='S')
                        {
                            suitType ='Spades';
                        } 
                        this.setState({eastSharedType1:suitType,eastSharedValue1:h.infoShared[0].value});
                    }
                }
              }); 
               this.setState({
                  northSharedType1:gamestate.northSharedType1,
                  northSharedValue1:gamestate.northSharedValue1,
                  northSharedType2:gamestate.northSharedType2,
                  northSharedValue2:gamestate.northSharedValue2, 
                  eastSharedType1:gamestate.eastSharedType1,
                  eastSharedValue1:gamestate.eastSharedValue1,
                  eastSharedType2:gamestate.eastSharedType2,
                  eastSharedValue2:gamestate.eastSharedValue2, 
                  southSharedType1:gamestate.southSharedType1,
                  southSharedValue1:gamestate.southSharedValue1,
                  southSharedType2:gamestate.southSharedType2,
                  southSharedValue2:gamestate.southSharedValue2, 
                  westSharedType1:gamestate.westSharedType1,
                  westSharedValue1:gamestate.westSharedValue1,
                  westSharedType2:gamestate.westSharedType2,
                  westSharedValue2:gamestate.westSharedValue2, 
                  winnerSuit: gamestate.winnerSuit,
                  winnerLevel:gamestate.winnerLevel,
                  winnerSide:gamestate.winnerSide,
                  winnerSuitImage:gamestate.winnerSuitImage,
                  winnerdblRedbl:gamestate.winnerdblRedbl, 
                  mySeat: gamestate.mySeat,  
                  myLHOSeat: gamestate.myLHOSeat,
                  myPartnerSeat:gamestate.myPartnerSeat,
                  myRHOSeat:gamestate.myRHOSeat,
                  southUser:gamestate.myUsername,  
                  northUser:gamestate.myPartnername,
                  westUser:gamestate.myLHOname,
                  eastUser: gamestate.myRHOname,
              });
              this.setDirection(''); 
              //console.log('Numof_C '+ Numberof_C + 'Numberof_D '+ Numberof_D + 'Numberof_H '+ Numberof_H + 'Numof_S '+ Numberof_S);
              // console.log(this.state.winnerSuit);
            }
  
            setDirection(seat)
            {
              const { navigation } = this.props; 
              const gamestate = navigation.getParam('gameState'); 
              if(seat==='')
              {
                  seat=gamestate.StartSeat;
              } 
              
              if(seat==='S')
              {
                console.log('seat '+seat + ' mySeat '+ gamestate.myRHOSeat);
                  this.setState({
                      DirectionTextMySeat:styles.DirectionTextActive,
                      southUserText:'#DBFF00',
                      directionImage: svgImages.pointerSouth,
                      });
                  this.setState({
                      disablemyCards_C:false,
                      disablemyCards_D:false,
                      disablemyCards_H:false,
                      disablemyCards_S:false,
                  })
  
              }
              else if(seat=== 'W')
              {
                  this.setState({
                      DirectionTextLHO:styles.DirectionTextActive,
                      westUserText:'#DBFF00',
                      directionImage: svgImages.pointerLeft,
                      });
              }
              else if(seat=== 'E')
              {
                console.log('seat '+seat + '  MyRHO '+ gamestate.myRHOSeat);
                  this.setState({
                      DirectionTextRHO:styles.DirectionTextActive,
                      eastUserText:'#DBFF00',
                      directionImage: svgImages.pointerRight,
                      });
              } 
              else if(seat=== 'N')
              {
                  this.setState({
                      DirectionTextPartner:styles.DirectionTextActive,
                      northUserText:'#DBFF00',
                      directionImage: svgImages.pointerNorth,
                      });
  
                  if('Partner'===this.state.DummySeat){
                      this.setState({
                          disabledummyCards_C:false,
                          disabledummyCards_D:false,
                          disabledummyCards_H:false,
                          disabledummyCards_S:false,
                      })
                  }
              }
            }

        showHistory(){
            const {navigation} = this.props;
            const hool = navigation.getParam('xmppconn');
            const gamestate = navigation.getParam('gameState'); 
            this.props.navigation.navigate('History',{xmppconn : hool,tableID:this.state.tableID,gameState:gamestate,cardsCnt:this.state.cardsCnt});
        }    
          
        componentDidMount() {
              const { navigation } = this.props;
              const hool = navigation.getParam('xmppconn');
              const gamestate = navigation.getParam('gameState'); 
              console.log('gamestate');
              console.log(gamestate);
            
              this.getTableInfo();

                 
              if(gamestate.contractinfo !== undefined){ 
                var bid_suit_image='';
                var bid_suit_color='';
                var bid_suit_text_color='';
                var bid_suit_border_color='';
                var bid_winnerTextColor='';
                var bid_level=gamestate.contractinfo.level;
                if(gamestate.contractinfo.suit==='C')
                {
                    bid_suit_image=require('../../assets/images/clubs.png');
                    bid_suit_color=styles.clubsImageActive;
                    bid_suit_text_color=styles.bidCircleTextClubsActive;
                    bid_suit_border_color=styles.bidCirclePositionClubsActive;
                    bid_winnerTextColor='#79E62B';
                }
                else if(gamestate.contractinfo.suit==='D')
                {
                    bid_suit_image=require('../../assets/images/diamond.png');
                    bid_suit_color=styles.diamondImageActive;
                    bid_suit_text_color=styles.bidCircleTextDiamondActive;
                    bid_suit_border_color=styles.bidCirclePositionDiamondActive;
                    bid_winnerTextColor='#04AEFF';
                }
                else if(gamestate.contractinfo.suit==='H')
                {
                    bid_suit_image=require('../../assets/images/hearts.png');
                    bid_suit_color=styles.heartsImageActive;
                    bid_suit_text_color=styles.bidCircleTextHeartsActive;
                    bid_suit_border_color=styles.bidCirclePositionHeartsActive;
                    bid_winnerTextColor='#FF0C3E';
                }
                else if(gamestate.contractinfo.suit==='S')
                {
                    bid_suit_image=require('../../assets/images/spade.png');
                    bid_suit_color=styles.spadeImageActive;
                    bid_suit_text_color=styles.bidCircleTextSpadeActive;
                    bid_suit_border_color=styles.bidCirclePositionSpadeActive;
                    bid_winnerTextColor='#C000FF';
                }
                else if(gamestate.contractinfo.suit==='NT')
                {
                    bid_suit_image='';
                    bid_suit_color=styles.bidCircleTextAssActive;
                    bid_suit_text_color=styles.bidCircleTextNTActive;
                    bid_suit_border_color=styles.bidCirclePositionNTActive;
                    bid_level=gamestate.contractinfo.level+'NT';
                    bid_winnerTextColor='#FFE81D';
                } 
                             
                this.setState({
                    winnerLevel:bid_level,
                    winnerSide:gamestate.contractinfo.declarer,
                    winnerSuit:gamestate.contractinfo.suit,
                    winnerSuitImage:bid_suit_image, 
                    winnerTextColor:bid_winnerTextColor,
                }); 
                if(gamestate.contractinfo.double==='true')
                {
                    this.setState({winnerdblRedbl:'x2'});
                } 
                else if(gamestate.contractinfo.redouble==='true')
                {
                    this.setState({winnerdblRedbl:'x4'});
                } 
            }
            if(gamestate.tricks.length>0){
                for(var ir=0 ;ir<gamestate.tricks.length;ir++){
                    var trickcards=gamestate.tricks[ir].cards; 
                    console.log(gamestate.tricks[ir].cards);
                    if(trickcards.length<4){
                        for (let trickcard of trickcards) { 
                            console.log(trickcard.side +', '+trickcard.suit +', '+trickcard.rank);
                            this.cardsPlayed(trickcard);
                        }
                    }
                }
            }   
            
              HOOL_CLIENT.addListener(HOOL_EVENTS.STATE_UPDATE, (e) => {
                 
                if (e.score) {
                    if (e.score.trick) {
                      scoreString = e.score.trick.map(s => `${s.side}-${s.value}`).join(' ')
                      console.info(`Trick score: ${scoreString}`)

                      e.score.trick.map(s =>{
                          //console.log('s.side.indexOf(NS)');
                         // console.log(s.side.indexOf('NS'));
                        if(s.side.indexOf('NS') !== -1){
                            this.setState({TrickScoreNS:s.value});
                        }
                        else if(s.side.indexOf('EW') !== -1){
                            this.setState({TrickScoreEW:s.value});
                        }
                      })
                      //console.info(`Trick turn: ${e.turn}`);
                      this.setState({ 
                        DirectionTextPartner:styles.DirectionText,
                       DirectionTextLHO:styles.DirectionText,
                       DirectionTextRHO:styles.DirectionText,
                       DirectionTextMySeat:styles.DirectionText,
                       northUserText: '#6D6D6D',
                       eastUserText: '#6D6D6D',
                       southUserText: '#6D6D6D',
                       westUserText: '#6D6D6D',
                       trick_Suit:'',
                       directionImage:'',
                   });
                      const interval = setInterval(() => {
                         this.setState({
                             MyCard:false,PartnerPlayed:false,LHOPlayed:false,RHOPlayed:false,                              
                        });
                        this.setDirection(e.turn);                            
                         clearInterval(interval);
                      }, 3000);
                      
                    }
                    if (e.score.deal) {
                      scoreString = e.score.deal.map(s => `${s.side}-${s.value}`).join(' ')
                      console.info(`Deal score: ${scoreString}`)
                    }
                    if (e.score.running) {
                      scoreString = e.score.running.map(s => `${s.side}-${s.value}`).join(' ')
                      console.info(`Running score: ${scoreString}`)
                    }
                }
              }); 
  
                HOOL_CLIENT.addListener(HOOL_EVENTS.CARD_PLAY, (e) => {
                  // hide the card, if necessary
                  this.setState({directionImage:svgImages.pointer});
                  this.cardsPlayed(e);
                  if(e.side==='N' || e.side==='S'){
                    this.setState({NSStyle:styles.ContractText,NSScoreStyle: styles.ShareText,
                        EWStyle:styles.ContractTextActive,EWScoreStyle: styles.ShareTextActive,});
                    }
                    else if(e.side==='E' || e.side==='W'){
                        this.setState({NSStyle:styles.ContractTextActive,NSScoreStyle: styles.ShareTextActive,
                            EWStyle:styles.ContractText,EWScoreStyle: styles.ShareText,});
                    }
                });
          
                HOOL_CLIENT.addListener(HOOL_EVENTS.CARD_PLAY_ERROR, (e) => {
                  let line = `Failed to play ${e.rank}${e.suit}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`
                  console.warn(line)
                   alert(line)
                });   
          }  
  
          async cardsPlayed(e){
              console.log(e.side +' has played card ' + e.rank + e.suit);
              console.log(e.side +' , ' + this.state.mySeat);
            if(this.state.trick_Suit===''){
                this.setState({trick_Suit:e.suit});
            }
            console.log('this.state.trick_Suit ' + this.state.trick_Suit);
                  if(e.side===this.state.mySeat){
                      this.setState({MyCard:true,myRank:e.rank});
                      if(e.suit==='C')
                      {
                          this.setState({myCardStyle:styles.playCardClubs,myCardNumberstyle:styles.playCardNumberClubs,mySuit:require('../../assets/images/clubs.png')});
                      }
                      else if(e.suit==='D')
                      {
                          this.setState({myCardStyle:styles.playCardDiamond,myCardNumberstyle:styles.playCardNumberDiamond,mySuit:require('../../assets/images/diamond.png')});
                      }
                      else if(e.suit==='H')
                      {
                          this.setState({myCardStyle:styles.playCardHearts,myCardNumberstyle:styles.playCardNumberHearts,mySuit:require('../../assets/images/hearts.png')});
                      }
                      else if(e.suit==='S')
                      {
                          this.setState({myCardStyle:styles.playCardSpade,myCardNumberstyle:styles.playCardNumberSpade,mySuit:require('../../assets/images/spade.png')});
                      } 
                          var val = e.rank + e.suit;
                          var index = southHands.findIndex(function(item, i){
                          return item.key === val
                          }); 
                        //  console.log('index '+ index);
                         TotalCardWidthSouth= TotalCardWidthSouth - 44;
                         southcardsShown[index]='none';            
                      
                      this.setState({
                          DirectionTextLHO:styles.DirectionTextActive,
                          westUserText:'#DBFF00',
                          directionImage:svgImages.pointerLeft,
                          DirectionTextMySeat:styles.DirectionText,
                          southUserText: '#6D6D6D', 
                          disablemyCards_C:true,
                          disablemyCards_D:true,
                          disablemyCards_H:true,
                          disablemyCards_S:true,
                          });
                      
                  }
                  else if(e.side===this.state.myPartnerSeat){
                     // console.log('inside partneer');
                      this.setState({PartnerPlayed:true,PartnerRank:e.rank});
                      if(e.suit==='C')
                      {
                          this.setState({PartnerCardStyle:styles.playCardClubs,PartnerCardNumberstyle:styles.playCardNumberClubs,PartnerSuit:require('../../assets/images/clubs.png')});
                      }
                      else if(e.suit==='D')
                      {
                          this.setState({PartnerCardStyle:styles.playCardDiamond,PartnerCardNumberstyle:styles.playCardNumberDiamond,PartnerSuit:require('../../assets/images/diamond.png')});
                      }
                      else if(e.suit==='H')
                      {
                          this.setState({PartnerCardStyle:styles.playCardHearts,PartnerCardNumberstyle:styles.playCardNumberHearts,PartnerSuit:require('../../assets/images/hearts.png')});
                      }
                      else if(e.suit==='S')
                      {
                          this.setState({PartnerCardStyle:styles.playCardSpade,PartnerCardNumberstyle:styles.playCardNumberSpade,PartnerSuit:require('../../assets/images/spade.png')});
                      } 
                        var val = e.rank + e.suit;
                        var index = northHands.findIndex(function(item, i){
                        return item.key === val
                        }); 
                        TotalCardWidthNorth= TotalCardWidthNorth - 44;
                       northcardsShown[index]='none';            
                    
                      this.setState({
                          DirectionTextRHO:styles.DirectionTextActive,
                          eastUserText:'#DBFF00', 
                          directionImage:svgImages.pointerRight,
                          DirectionTextPartner:styles.DirectionText,
                          northUserText: '#6D6D6D',
                          });
                  }
                  else if(e.side===this.state.myLHOSeat){
                    //  console.log('inside lho');
                      this.setState({LHOPlayed:true,LHORank:e.rank});
                      if(e.suit==='C')
                      {
                          this.setState({LHOCardStyle:styles.playCardClubs,LHOCardNumberstyle:styles.playCardNumberClubs,LHOSuit:require('../../assets/images/clubs.png')});
                      }
                      else if(e.suit==='D')
                      {
                          this.setState({LHOCardStyle:styles.playCardDiamond,LHOCardNumberstyle:styles.playCardNumberDiamond,LHOSuit:require('../../assets/images/diamond.png')});
                      }
                      else if(e.suit==='H')
                      {
                          this.setState({LHOCardStyle:styles.playCardHearts,LHOCardNumberstyle:styles.playCardNumberHearts,LHOSuit:require('../../assets/images/hearts.png')});
                      }
                      else if(e.suit==='S')
                      {
                          this.setState({LHOCardStyle:styles.playCardSpade,LHOCardNumberstyle:styles.playCardNumberSpade,LHOSuit:require('../../assets/images/spade.png')});
                      }
                       
                          var val = e.rank + e.suit;
                          var index = westHands.findIndex(function(item, i){
                          return item.key === val
                          });
                          console.log('index west '+index);
                          WestPlayCardArea = WestPlayCardArea-23;
                          westcardsShown[index]='none';   
                          //this.setState({DummyPlayed:true});     
                       
                      this.setState({
                          DirectionTextPartner:styles.DirectionTextActive,
                          northUserText:'#DBFF00',
                          directionImage:svgImages.pointerNorth,
                          DirectionTextLHO:styles.DirectionText,
                          westUserText: '#6D6D6D',
                          });  
                  }
                  else if(e.side===this.state.myRHOSeat){
                    //  console.log('inside rho');
                      this.setState({RHOPlayed:true,RHORank:e.rank});
                      if(e.suit==='C')
                      {
                          this.setState({RHOCardStyle:styles.playCardClubs,RHOCardNumberstyle:styles.playCardNumberClubs,RHOSuit:require('../../assets/images/clubs.png')});
                      }
                      else if(e.suit==='D')
                      {
                          this.setState({RHOCardStyle:styles.playCardDiamond,RHOCardNumberstyle:styles.playCardNumberDiamond,RHOSuit:require('../../assets/images/diamond.png')});
                      }
                      else if(e.suit==='H')
                      {
                          this.setState({RHOCardStyle:styles.playCardHearts,RHOCardNumberstyle:styles.playCardNumberHearts,RHOSuit:require('../../assets/images/hearts.png')});
                      }
                      else if(e.suit==='S')
                      {
                          this.setState({RHOCardStyle:styles.playCardSpade,RHOCardNumberstyle:styles.playCardNumberSpade,RHOSuit:require('../../assets/images/spade.png')});
                      }
                       
                          var val = e.rank + e.suit;
                          var index = eastHands.findIndex(function(item, i){
                          return item.key === val
                          }); 
                          EastPlayCardArea = EastPlayCardArea-23;
                          eastcardsShown[index]='none';   
                          this.setState({DummyPlayed:true});     
                      
                      this.setState({
                          DirectionTextMySeat:styles.DirectionTextActive,
                          southUserText:'#DBFF00',
                          directionImage:svgImages.pointerSouth,
                          DirectionTextRHO:styles.DirectionText,
                          eastUserText: '#6D6D6D', 
                          });                           
                  } 
          }
   
          displayCards(hand) {
            
            var cardSide=[];  
            var idx=0;
             hand.forEach((card) =>{   
              //56   console.log(card.suit);          
              if(card.suit==="S")
                {
                    cardSide.push({"rank":card.rank,"img": require('../../assets/images/spade.png'),"suit":"S","key": card.rank + "S","SuiteColor":"#C000FF","idx":idx}) 
                    
                }else if(card.suit==='D')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/diamond.png'),"suit":"D","key": card.rank + "D","SuiteColor":"#04AEFF","idx":idx})
                     
                }else if(card.suit==='H')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/hearts.png'),"suit":"H","key": card.rank + "H","SuiteColor":"#FF0C3E","idx":idx});
                     
                }else if(card.suit==='C')
                {
                    cardSide.push({"rank": card.rank ,"img": require('../../assets/images/clubs.png'),"suit":"C","key": card.rank + "C","SuiteColor":"#79E62B","idx":idx},)
                    
                }  
                idx++;  
            });
           // this.drawCardsLHO();
          
           return cardSide;
        }
           render(){  
      
      const drawCards = () => {
        return southHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBody,{display:southcardsShown[element.idx]}]} key={element.key} >
                <View style={styles.cardPosition}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsNorthKibitzer = () => {
        return  northHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBodyNorth,{display:northcardsShown[element.idx]}]} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsWest = () => {
        return westHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBodyNorth,{display:westcardsShown[element.idx]}]} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsEast = () => {
        return eastHands.map((element) => {
          return (
            <View style={[styles.cardDisplayBodyEast,{display:eastcardsShown[element.idx]}]} key={element.key} >
                <View style={styles.cardPositionE}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };


return (
<View style={{backgroundColor:"#0a0a0a",height:"100%"}}>
    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", width:"100%", height:HeightBoxTop, backgroundColor:"#151515"}}>
        <View style={{width:"100%", height:ContractTextHeight, borderBottomWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
            <Text style={styles.ContractTextHeader}>Contract</Text>
        </View>
        <View style={{width:"100%",flexDirection:"row", borderBottomWidth:1, borderBottomColor:"#0a0a0a"}}>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                 {this.state.winnerSuit==='S' ?  <Text style={styles.ContractTextSpade}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='H' ?  <Text style={styles.ContractTextHearts}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='D' ?  <Text style={styles.ContractTextDiamond}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='C' ?  <Text style={styles.ContractTextClubs}>{this.state.winnerSide}</Text> :null }
                {this.state.winnerSuit==='NT' ?  <Text style={styles.ContractTextNT}>{this.state.winnerSide}</Text> :null }

            </View>
            <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
            {
                        this.state.winnerSuit==='S' ?
                        <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                            <Text style={styles.ContractTextSpade}>{this.state.winnerLevel}</Text>
                            <Image source={require('../../assets/images/spade.png')} />
                        </View>
                        :null
                        }
                        {
                        this.state.winnerSuit==='H' ?
                        <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                            <Text style={styles.ContractTextHearts}>{this.state.winnerLevel}</Text>
                            <Image source={require('../../assets/images/hearts.png')} /> 
                        </View>
                        :null
                        }
                        
                        {
                        this.state.winnerSuit==='D' ?
                        <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                         <Text style={styles.ContractTextDiamond}>{this.state.winnerLevel}</Text>
                        <Image source={require('../../assets/images/diamond.png')} /> 
                        </View>
                        :null
                        }
                        {
                        this.state.winnerSuit==='C' ?
                        <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                         <Text style={styles.ContractTextClubs}>{this.state.winnerLevel}</Text>
                        <Image source={require('../../assets/images/clubs.png')} /> 
                        </View>
                        :null
                        }
                        {
                        this.state.winnerSuit==='NT' ?
                        <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                         <Text style={styles.ContractTextNT}>{this.state.winnerLevel}</Text> 
                        </View>
                        :null
                        }
            </View>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, alignItems:"center", justifyContent:"center"}}>
            {
                            (this.state.winnerSuit==='S' && this.state.winnerdblRedbl !=='') ?
                         
                            <Text style={styles.ContractTextSpade}>{this.state.winnerdblRedbl}</Text> 
                        :null
                        }
                        {
                            (this.state.winnerSuit==='H' && this.state.winnerdblRedbl !=='')  ?
                        
                            <Text style={styles.ContractTextHearts}>{this.state.winnerdblRedbl}</Text> 
                        :null
                        }
                        
                        {
                            (this.state.winnerSuit==='D' && this.state.winnerdblRedbl !=='') ? 
                         <Text style={styles.ContractTextDiamond}>{this.state.winnerdblRedbl}</Text> 
                        :null
                        }
                        {
                            (this.state.winnerSuit==='C' && this.state.winnerdblRedbl !=='')  ?                 
                         <Text style={styles.ContractTextClubs}>{this.state.winnerdblRedbl}</Text>  
                        :null
                        }
                        {
                            (this.state.winnerSuit==='NT' && this.state.winnerdblRedbl !=='')  ?                 
                         <Text style={styles.ContractTextNT}>{this.state.winnerdblRedbl}</Text>  
                        :null
                        }
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"red", borderBottomWidth:1, alignItems:"center"}}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
                <Text style={this.state.NSStyle}>N • S</Text>
            
                <Text style={styles.bidPlayText}>{this.state.TrickScoreNS}</Text>
            </View>
  
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"red", borderBottomWidth:1, alignItems:"center"}}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
                <Text style={this.state.EWStyle}>E • W</Text>
            
                <Text style={styles.bidPlayText}>{this.state.TrickScoreEW}</Text>
            </View>
        </View>
    </View>

    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/settings.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/flag.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/cheat_sheet_ass.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/revers.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('../../assets/images/chat.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <TouchableOpacity onPress={() => this.showHistory()}>
                    <Image source={require('../../assets/images/on.png')} />
                </TouchableOpacity> 
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", justifyContent:"space-between"}}>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <View style={styles.hanNorthSauthTotalWidthHeightArea}>
               
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View> */}

                {/* <View style={{width:TotalCardWidth, height:45,  flexDirection:"row", justifyContent:"space-around"}}>
                { drawCardsNorth() }  
                </View> */}
                
               <View style={{width:"100%", alignItems:"center", justifyContent:"center"}}>
                    <View style={{width:TotalCardWidthNorth, height:45, flexDirection:"row", justifyContent:"space-around", alignSelf:"center",}}>
                        { drawCardsNorthKibitzer() }  
                    </View>
                </View>

                
                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.shareHandText,,{color:this.state.northUserText}]}>{this.state.northUser}</Text>
                </View>    
            </View>
        </View>

        <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
            <View style={{position:"absolute",left:3, borderWidth:0, borderColor:"red", width:68, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center", }}>
             <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:68,  transform: [{ rotate: '-90deg' }]}}>
            
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>  */}

               <View style={{width:WestPlayCardArea, height:45, marginLeft:75, flexDirection:"row", justifyContent:"space-around"}}>
                { drawCardsWest() }  
                </View>               

                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.shareHandText,{color:this.state.westUserText}]}>{this.state.westUser}</Text>
                </View> 
            </View>
            </View>

        <View style={{position:"absolute",right:0, width:68, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center",  }}>
            <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:68,  transform: [{ rotate: '90deg' }]}}>
                
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>   */}

                 <View style={{width:EastPlayCardArea, marginRight:-17, height:45,justifyContent:"space-around", flexDirection:"row-reverse"}}>
                { drawCardsEast() }  
                </View>             
                
                
                
                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={[styles.shareHandText,{color:this.state.eastUserText}]}>{this.state.eastUser}</Text>
                </View> 
            </View>
        </View>

            <View style={styles.playingCardTotalWidthHeightArea}>
                <View style={styles.northCardPosition}>
                    {
                        (this.state.PartnerPlayed) ?
                            <View style={this.state.PartnerCardStyle}>
                                <View style={styles.playCardNumberPosition}>
                                    <Text style={this.state.PartnerCardNumberstyle}>{this.state.PartnerRank}</Text>
                                </View>
                                <Image source={this.state.PartnerSuit} /> 
                            </View>
                        :
                        <View style={styles.CardPositionBorderAss}></View>
                    }
                </View>

                <View style={styles.playingEastWestCardTotalWidthHeightArea}>
                    <View style={styles.westCardPosition}>
                        {
                            (this.state.LHOPlayed) ?
                            <View style={this.state.LHOCardStyle}>
                                <View style={styles.playCardNumberPosition}>
                                    <Text style={this.state.LHOCardNumberstyle}>{this.state.LHORank}</Text>
                                </View>
                                <Image source={this.state.LHOSuit} /> 
                                </View>
                            :
                            <View style={styles.CardPositionBorderAss}></View> 
                        }
                    </View>
                    <View style={styles.eastCardPosition}>
                            {
                            (this.state.RHOPlayed) ?
                                <View style={this.state.RHOCardStyle}>
                                    <View style={styles.playCardNumberPosition}>
                                        <Text style={this.state.RHOCardNumberstyle}>{this.state.RHORank}</Text>
                                    </View>
                                    <Image source={this.state.RHOSuit} /> 
                                </View>
                            :
                            <View style={styles.CardPositionBorderAss}></View>
                            }
                    </View>
                </View>
                
                <View style={styles.sauthCardPosition}>
                    {
                        (this.state.MyCard) ?
                        <View style={this.state.myCardStyle}>
                            <View style={styles.playCardNumberPosition}>
                                <Text style={this.state.myCardNumberstyle}>{this.state.myRank}</Text>
                            </View>
                            <Image source={this.state.mySuit} /> 
                        </View>
                    :
                    <View style={styles.CardPositionBorderAss}></View>
                    }
                </View>
                
                <View style={styles.totalWidthheightPositionDirectionArea}>
                    <View style={styles.playCardDirectionPosition}>
                        <Text style={this.state.DirectionTextPartner}>N</Text>
                        <View style={styles.pointerEastWestAreaPosition}>
                            <Text style={this.state.DirectionTextLHO}>W</Text>
                            <View style={styles.directionPointerPosition}>
                                <SvgXml xml={this.state.directionImage} />
                            </View>
                            <Text style={this.state.DirectionTextRHO}>E</Text>
                        </View>
                        <Text style={this.state.DirectionTextMySeat}>S</Text>
                    </View>
                </View>

            </View>
        </View>

        <View style={styles.hanNorthSauthTotalWidthHeightArea}>
            <View style={styles.hanNameWidthHeightArea}>
                <Text style={[styles.shareHandText,{color:this.state.southUserText}]}>{this.state.southUser}</Text>
            </View>
            <View style={{width:TotalCardWidthSouth, height:45, marginLeft:50,  flexDirection:"row", justifyContent:"space-around", alignSelf: 'center',}}>
                { drawCards() }  
            </View>
        </View>

   </View>
</View>

  );
};
        }
 
        

const styles = StyleSheet.create({
    playingEastWestCardTotalWidthHeightArea:{
        flexDirection:"row", 
        width:"100%", 
        height:55, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    playingCardTotalWidthHeightArea:{
        width:225, 
        height:225,
        flexDirection:"column", 
        alignItems:"center",
        justifyContent:"space-between",
    },
    totalWidthheightPositionDirectionArea:{
        position:"absolute",
        flexDirection:"row", 
        width:"100%", 
        height:225, 
        justifyContent:"center", 
        alignItems:"center",
    },
    pointerEastWestAreaPosition:{
        flexDirection:"row", 
        justifyContent:"space-around", 
        alignItems:"center", 
        paddingRight:4,
    },
    patternTotalColumnArea:{
        flexDirection:"column", 
        width:"100%", 
        height:45,  
        alignItems:"center",
    },
    patternTotalRowArea:{
        flexDirection:"row",
        width:181, 
        height:45, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    hanNorthSauthTotalWidthHeightArea:{
        width:"100%", 
        height:68, 
        alignItems:"flex-end",
        flexDirection:"column",
    },
    hanNameWidthHeightArea:{
        width:"100%", 
        height:23,
    },
    patternWidthHeightArea:{
        width:90, 
        height:45, 
        backgroundColor:"#151515",
    },
    cardDisplayBodyNorth:{
        width:90,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
    },
    cardDisplayBodyEast:{
        width:90,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
        flexDirection:"row-reverse",
    },
    cardPositionW:{
        width:22,
        marginTop:3,
        marginLeft:1,
        alignItems:"center",
        justifyContent:"center",
     },
     cardPositionE:{
        width:22,
        marginTop:-4,
        marginLeft:2,
        alignItems:"center",
        justifyContent:"center",
     },
    directionPointerPosition:{
        width:27,
        height:27,
        borderWidth:0,
        borderColor:"red",
        alignItems:"center",
        justifyContent:"center",
        top:1,
    },
    ContractTextSpade: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#C000FF",
    },
    ContractTextHearts: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    ContractTextDiamond: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#04AEFF",
    },
    ContractTextClubs: {
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#79E62B",
    },
    bidPlayText: {
		fontFamily:"Roboto-Thin",
        fontSize: 55,
        fontWeight: "normal",
        color:"#6D6D6D",
      },
    CardPositionBorderAss:{
		
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#676767",
    },
    northCardPosition:{
        width:55,
        height:81,
    },
    sauthCardPosition:{
        width:55,
        height:81,
    },
    westCardPosition:{
        width:55,
        height:81,
        marginLeft: 12, 
        transform: [{ rotate: '-90deg' }],
    },
    eastCardPosition:{
        width:55,
        height:81,
        marginRight: 12, 
        transform: [{ rotate: '90deg' }],
    },
    playCardNumberPosition:{
        position:"absolute",
        width:55,
        height:81,
        borderWidth:0,
        borderColor:"red",
        paddingTop:2,
        paddingLeft:6,

    },
    playCardNumberDiamond:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
    },
    playCardDiamond:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberHearts:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    playCardHearts:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberClubs:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
    },
    playCardClubs:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberSpade:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
    },
    playCardSpade:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardDirectionPosition:{
        width:60, 
        height:60, 
        justifyContent:"space-between", 
        flexDirection:"column",
    },
    DirectionText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#676767",
        textAlign:"center", 
      }, 
      DirectionTextActive: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
      }, 


/*************************************** */

    SharePatternCardSpade:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardHearts:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardDiamond:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardClubs:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    PatternText:{
		fontFamily:"Roboto-Light",
        fontSize: 14,
        fontWeight: "400",
        color:"#FFFFFF",
    },
    SharePatternCard:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#DBFF00",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    cardDisplayBody:{
        width:90,
        // height:91,
        height:45,
        borderTopLeftRadius:4,
        borderTopRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515"
    },
    
    cardPosition:{
        width:22,
        marginTop:3,
        marginLeft:4,
        alignItems:"center",
        justifyContent:"center",
     },
     SpadeCardNumber:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
     },
     heartsCardNumber:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
     },
     clubsCardNumber:{
		fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
     },
     diamondCardNumber:{
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
     },

    ContractText: {
        position:"absolute",
        top:8,
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
    },
    ContractTextHeader: {
       
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
    },
    ContractTextActive: {
        position:"absolute",
        top:8,
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
    },
    ContractMdlText:{
		fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#6D6D6D",
        paddingRight:4,
    },
    ShareText: {
        top:5,
		fontFamily:"Roboto-Thin",
        fontSize: 27,
        fontWeight: "normal",
        color:"#353535",
      },
      ShareTextActive: {
        top:5,
		fontFamily:"Roboto-Thin",
        fontSize: 27,
        fontWeight: "normal",
        color:"#6D6D6D",
      },
      shareHandText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#6D6D6D",
        textAlign:"center",
        paddingTop:3,
      },
      PaternText: {
		 fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FFFFFF",
        textAlign:"center",
      },
    OpenText: {
		fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
        padding:4,
    },
  });
 