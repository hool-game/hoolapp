import React,{Component} from 'react';
import {AppRegistry, View, Text, StyleSheet, Button, Image, Dimensions} from 'react-native';
import { normalize } from 'jest-config';
//import global from './shared/global';


const ViewBoxesWithColorAndText = () => {
    var ScreenWidth=  Dimensions.get("window").width;
    var ScreenHeight=  Dimensions.get("window").height;

    var HeightBoxTop=(225/360*ScreenHeight);
    var MenuBodyWidth=(91/360*ScreenHeight);
    var ShareHehightWidth=(90/360*ScreenHeight);
    var ContractTextHeight=(22/360*ScreenHeight);
    var ContractDwnBoxHeight=(23/360*ScreenHeight);
    var ContractDwnMdlBoxWidth=(44/360*ScreenHeight);
    //var TotalCardWidth=(535/640*ScreenWidth);
    //var TotalCardWidth=(400/640*ScreenWidth);
    // var TotalCardWidth=(376/640*ScreenWidth);

    var PlayBodyWidth=(ScreenWidth-MenuBodyWidth);
    var HeightWidthBoxes=(45/360*ScreenHeight);
    var HeightEastWestPalyCard=(360/360*ScreenHeight);
    //var EastWestPalyCardArea=(304/360*ScreenHeight);
    var EastWestPalyCardArea=(350/360*ScreenHeight);
    //var NorthCardWithKibitzerArea=(400/640*ScreenWidth);
    var NorthCardWithKibitzerArea=(376/640*ScreenWidth);
    
    var SampleNameArray = [ "K", "Q", "3", "I0", "8", "4", "A", "9", "7", "6", "Q", "J", "7" ];
    var hands = [
        {"rank": "K","img": require('./assets/images/spade.png'),"suit":"spade","key":"K_spade","SuiteColor":"#C000FF"},
        {"rank": "Q","img": require('./assets/images/spade.png'),"suit":"spade","key":"Q_spade","SuiteColor":"#C000FF"},
        {"rank": "I0","img": require('./assets/images/spade.png'),"suit":"spade","key":"10_spade","SuiteColor":"#C000FF"},
        {"rank": "K","img": require('./assets/images/hearts.png'),"suit":"hearts","key":"K_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "8","img": require('./assets/images/hearts.png'),"suit":"hearts","key":"8_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "4","img": require('./assets/images/hearts.png'),"suit":"hearts","key":"4_hearts","SuiteColor":"#FF0C3E"},
        {"rank": "A","img": require('./assets/images/diamond.png'),"suit":"diamond","key":"A_diamond","SuiteColor":"#04AEFF"},
        {"rank": "9","img": require('./assets/images/diamond.png'),"suit":"diamond","key":"9_diamond","SuiteColor":"#04AEFF"},
        {"rank": "7","img": require('./assets/images/diamond.png'),"suit":"diamond","key":"7_diamond","SuiteColor":"#04AEFF"},
        {"rank": "6","img": require('./assets/images/diamond.png'),"suit":"diamond","key":"6_diamond","SuiteColor":"#04AEFF"},
        {"rank": "Q","img": require('./assets/images/clubs.png'),"suit":"clubs","key":"Q_clubs","SuiteColor":"#79E62B"},
        {"rank": "J","img": require('./assets/images/clubs.png'),"suit":"clubs","key":"J_clubs","SuiteColor":"#79E62B"},
        {"rank": "7","img": require('./assets/images/clubs.png'),"suit":"clubs","key":"7_clubs","SuiteColor":"#79E62B"}
      ]
    
      const drawCards = () => {
        return hands.map((element) => {
          return (
            <View style={styles.cardDisplayBody} key={element.key} >
                <View style={styles.cardPosition}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsNorthKibitzer = () => {
        return hands.map((element) => {
          return (
            <View style={styles.cardDisplayBodyNorth} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsNorth = () => {
        return hands.map((element) => {
          return (
            <View style={styles.cardDisplayBodyNorth} key={element.key} >
                <View style={styles.cardPosition}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsWest = () => {
        return hands.map((element) => {
          return (
            <View style={styles.cardDisplayBodyNorth} key={element.key} >
                <View style={styles.cardPositionW}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };
      const drawCardsEast = () => {
        return hands.map((element) => {
          return (
            <View style={styles.cardDisplayBodyEast} key={element.key} >
                <View style={styles.cardPositionE}>
                    <Text style={[styles.SpadeCardNumber,{color: element.SuiteColor}]}>{element.rank}</Text>
                    <Image source={element.img} />
                </View>      
            </View>
          );
        });
      };


return (
<View style={{backgroundColor:"#0a0a0a",height:"100%"}}>
    <View style={{position:"absolute", left:0, width:MenuBodyWidth, height:"100%"}}> 
     <View style={{flexDirection:"column", width:"100%", height:HeightBoxTop, backgroundColor:"#151515"}}>
        <View style={{width:"100%", height:ContractTextHeight, borderBottomWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
            <Text style={styles.ContractTextHeader}>Contract</Text>
        </View>
        <View style={{width:"100%",flexDirection:"row", borderBottomWidth:1, borderBottomColor:"#0a0a0a"}}>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"center"}}>
                <Text style={styles.ContractTextHeader}>4</Text> 
                {/*<Text style={styles.ContractTextSpade}>4</Text>
                <Text style={styles.ContractTextHearts}>4</Text>
                <Text style={styles.ContractTextDiamond}>4</Text>
               <Text style={styles.ContractTextClubs}>4</Text> */}
            </View>
            <View style={{width:ContractDwnMdlBoxWidth, height:ContractDwnBoxHeight, flexDirection:"row", borderRightWidth:1, borderBottomColor:"#0a0a0a", alignItems:"center", justifyContent:"space-evenly",paddingLeft:6, paddingRight:6,}}>
                <Text style={styles.ContractMdlText}>4</Text>
                <Image source={require('./assets/images/hearts_inactive_2.png')} />

                {/* <Text style={styles.ContractTextSpade}>4</Text>
                <Image source={require('./assets/images/spade.png')} /> */}

                {/* <Text style={styles.ContractTextHearts}>4</Text>
                <Image source={require('./assets/images/hearts.png')} /> */}

                {/* <Text style={styles.ContractTextDiamond}>4</Text>
                <Image source={require('./assets/images/diamond.png')} /> */}

                {/* <Text style={styles.ContractTextClubs}>4</Text>
                <Image source={require('./assets/images/clubs.png')} /> */}
            </View>
            <View style={{width:ContractDwnBoxHeight, height:ContractDwnBoxHeight, alignItems:"center", justifyContent:"center"}}>
                <Text style={styles.ContractTextHeader}>x4</Text>
                {/* <Text style={styles.ContractTextSpade}>x4</Text> */}
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#525252", borderBottomWidth:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center" }}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
            <Text style={styles.ContractText}>N • S</Text>
            <Text style={styles.bidPlayText}>02</Text>
            </View>
        </View>
        <View style={{height:ShareHehightWidth, width:ShareHehightWidth, borderBottomColor:"#525252", borderBottomWidth:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center" }}>
        <View style={{width:"100%", height:"100%", flexDirection:"column", alignItems:"center", justifyContent:"center",}}>
            <Text style={styles.ContractText}>E • W</Text>
            <Text style={styles.bidPlayText}>04</Text>
            </View>
        </View>
    </View>

    <View style={{flexDirection:"column"}} >     
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
    <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/settings_2.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/flag_2.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#DBFF00", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/black_x.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/revers.png')} />
            </View>
        </View>
        <View style={{flexDirection:"row", alignItems:"center", marginTop:1}}>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, marginRight:1, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/chat_2.png')} />
            </View>
            <View style={{width:HeightWidthBoxes, height:HeightWidthBoxes, backgroundColor:"#151515", alignItems:"center", justifyContent:"center"}}>
                <Image source={require('./assets/images/on_2.png')} />
            </View>
        </View>
    </View>
  </View>

    <View style={{position:"absolute", right:0, width:PlayBodyWidth, height:"100%", flexDirection:"column", justifyContent:"space-between"}}>
        <View style={{width:"100%", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <View style={styles.hanNorthSauthTotalWidthHeightArea}>
               
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View> */}

                {/* <View style={{width:TotalCardWidth, height:45,  flexDirection:"row", justifyContent:"space-around"}}>
                { drawCardsNorth() }  
                </View> */}
                
               {/* <View style={{width:"100%", alignItems:"center", justifyContent:"center"}}>
                    <View style={{width:NorthCardWithKibitzerArea, height:45, flexDirection:"row", justifyContent:"space-around", alignItems:"center",}}>
                        { drawCardsNorthKibitzer() }  
                    </View>
                </View> */}

                <View style={styles.cheatSheetTotalBodyArea}>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>HCP</Text>
                        {/* <Text style={styles.cheatSheetTextWhiteAssQues}>?</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAro} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>Pattern</Text>
                        <Text style={styles.cheatSheetTextWhite}>4,4,3,2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/spade.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>4</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/hearts.png')} />
                        <Text style={styles.cheatSheetTextWhite}>2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/diamond.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>3</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/clubs.png')} />
                        <Text style={styles.cheatSheetTextWhiteAssNumb}>4</Text> 
                    </View>
                </View>

                
                <View style={styles.hanNameWidthHeightArea}>
                    {/* <Text style={styles.shareHandTextHandNameBlack}>3 tricks</Text>
                    <Image style={styles.tkbImgMrgTp} source={require('./assets/images/tkbflag_black.png')} /> */}

                    <Text style={styles.shareHandTextHandName}>Han-SoloName</Text>

                    {/* <Image style={styles.tkbImgMrgTp} source={require('./assets/images/tkbflag.png')} />
                    <Text style={styles.shareHandTextHandNameRed}>3 tricks</Text> */}
                </View> 

            </View>
        </View>

        <View style={{flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
            <View style={{position:"absolute",left:3, borderWidth:0, borderColor:"red", width:68, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center", }}>
             <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:68,  transform: [{ rotate: '-90deg' }]}}>
            
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                         <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>  */}

               {/* <View style={{width:EastWestPalyCardArea, height:45, marginLeft:15, flexDirection:"row", justifyContent:"space-around"}}>
                { drawCardsWest() }  
                </View> */}

                <View style={styles.cheatSheetTotalBodyArea}>
                <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>HCP</Text>
                        {/* <Text style={styles.cheatSheetTextWhiteAssQues}>?</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAro} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>Pattern</Text>
                        <Text style={styles.cheatSheetTextWhite}>4,4,3,2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/spade.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>4</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/hearts.png')} />
                        <Text style={styles.cheatSheetTextWhite}>2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/diamond.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>3</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/clubs.png')} />
                        <Text style={styles.cheatSheetTextWhiteAssNumb}>4</Text> 
                    </View>
                </View>


                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={styles.shareHandTextHandName}>Han-SoloNameW</Text>
                </View>  

            </View>
            </View>

        <View style={{position:"absolute",right:0, width:68, height:HeightEastWestPalyCard, alignItems:"center",justifyContent:"center",  }}>
            <View style={{flexDirection:"column", width:HeightEastWestPalyCard, height:68,  transform: [{ rotate: '90deg' }]}}>
                
                {/* <View style={styles.patternTotalColumnArea}>
                    <View style={styles.patternTotalRowArea}>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Pattern</Text>
                            <Text style={styles.PaternText}>4,4,3,2</Text>
                        </View>
                        <View style={styles.patternWidthHeightArea}>
                            <Text style={styles.shareHandText}>Spades</Text>
                            <Text style={styles.PaternText}>4</Text>
                        </View>
                    </View>
                </View>   */}

                 {/* <View style={{width:EastWestPalyCardArea, marginRight:-17, height:45,justifyContent:"space-around", flexDirection:"row-reverse"}}>
                { drawCardsEast() }  
                </View> */}

                {/* <View style={{width:EastWestPalyCardArea, marginLeft:15, height:45,justifyContent:"space-around", flexDirection:"row"}}>
                { drawCardsWest() }  
                </View> */}

                <View style={styles.cheatSheetTotalBodyAreaEast}>
                <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>HCP</Text>
                        {/* <Text style={styles.cheatSheetTextWhiteAssQues}>?</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAro} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>Pattern</Text>
                        <Text style={styles.cheatSheetTextWhite}>4,4,3,2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/spade.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>4</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/hearts.png')} />
                        <Text style={styles.cheatSheetTextWhite}>2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/diamond.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>3</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/clubs.png')} />
                        <Text style={styles.cheatSheetTextWhiteAssNumb}>4</Text> 
                    </View>
                </View>


                <View style={styles.hanNameWidthHeightArea}>
                    <Text style={styles.shareHandText}>Han Solo E</Text>
                    
                </View> 
            </View>
        </View>


         <View style={styles.popupBodyAreaTotalLft}>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>1</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>2</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>3</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>4</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>5</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>6</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>7</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>8</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>9</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>,</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>0</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                    <Image source={require('./assets/images/cheat_sheet_undo.png')} />
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Text style={styles.popupBodyAreaRowBoxText}>C</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Image source={require('./assets/images/right.png')} />
                </View>
            </View>         
        </View>

        <View style={styles.popupBodyAreaTotalMd}>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>1</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>2</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>3</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>4</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>5</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>6</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>7</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>8</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>9</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>,</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>0</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                    <Image source={require('./assets/images/cheat_sheet_undo.png')} />
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Text style={styles.popupBodyAreaRowBoxText}>C</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Image source={require('./assets/images/right.png')} />
                </View>
            </View>         
        </View>
        <View style={styles.popupBodyAreaTotalRht}>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>1</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>2</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>3</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>4</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>5</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>6</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>7</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>8</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                   <Text style={styles.popupBodyAreaRowBoxText}>9</Text>
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBox}>
                    <Text style={styles.popupBodyAreaRowBoxText}>,</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                     <Text style={styles.popupBodyAreaRowBoxText}>0</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBox}>
                    <Image source={require('./assets/images/cheat_sheet_undo.png')} />
                </View>
            </View>
            <View style={styles.popupBodyAreaRow}>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Text style={styles.popupBodyAreaRowBoxText}>C</Text> 
                </View>
                <View style={styles.popupBodyAreaRowBoxBtm}>
                    <Image source={require('./assets/images/right.png')} />
                </View>
            </View>         
        </View>





        <View style={styles.totalWidthheightPositionDirectionArea}>
            <View style={styles.playCardDirectionPosition}>
                <Text style={styles.DirectionText}>N</Text>
                <View style={styles.pointerEastWestAreaPosition}>
                    <Text style={styles.DirectionText}>W</Text>
                    <View style={styles.directionPointerPosition}>
                        <Image source={require('./assets/images/direction_pointer_s.png')} />
                    </View>
                    <Text style={styles.DirectionTextActive}>E</Text>
                </View>
                <Text style={styles.DirectionText}>S</Text>
            </View>
        </View>


        <View style={{display:"none", opacity:0,}}>
            <View style={styles.playingCardTotalWidthHeightArea}>
                <View style={styles.northCardPosition}>
                {/* <View style={styles.CardPositionBorderAss}></View> */}
                    <View style={styles.playCardDiamond}>
                        <View style={styles.playCardNumberPosition}>
                            <Text style={styles.playCardNumberDiamond}>A</Text>
                        </View>
                        <Image source={require('./assets/images/diamond.png')} /> 
                    </View>
                </View>

                <View style={styles.playingEastWestCardTotalWidthHeightArea}>
                    <View style={styles.westCardPosition}>
                        <View style={styles.CardPositionBorderAss}></View>
                        {/* <View style={styles.playCardSpade}>
                            <View style={styles.playCardNumberPosition}>
                                <Text style={styles.playCardNumberSpade}>A</Text>
                            </View>
                            <Image source={require('./assets/images/spade.png')} /> 
                        </View> */}
                    </View>
                    <View style={styles.eastCardPosition}>
                        {/* <View style={styles.CardPositionBorderAss}></View> */}
                        <View style={styles.playCardHearts}>
                             <View style={styles.playCardNumberPosition}>
                                <Text style={styles.playCardNumberHearts}>A</Text>
                            </View>
                            <Image source={require('./assets/images/hearts.png')} /> 
                        </View>
                    </View>
                </View>
                
                <View style={styles.sauthCardPosition}>
                    {/* <View style={styles.CardPositionBorderAss}></View> */}
                    <View style={styles.playCardClubs}>
                        <View style={styles.playCardNumberPosition}>
                            <Text style={styles.playCardNumberClubs}>A</Text>
                        </View>
                        <Image source={require('./assets/images/clubs.png')} /> 
                    </View>
                </View>

            </View>
        </View>
    </View>

        <View style={styles.hanNorthSauthTotalWidthHeightArea}>
            <View style={styles.hanNameWidthHeightArea}>
                <Text style={styles.shareHandText}>Han Solo S</Text>
            </View>
            {/* <View style={{width:TotalCardWidth, height:45,  flexDirection:"row", justifyContent:"space-around", alignItems:"center", alignSelf: 'center',}}>
                { drawCards() }  
            </View> */}
       


        <View style={styles.cheatSheetTotalBodyArea}>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>HCP</Text>
                        {/* <Text style={styles.cheatSheetTextWhiteAssQues}>?</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAro} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetPatternBody}>
                        <Text style={styles.cheatSheetTextOffWhite}>Pattern</Text>
                        <Text style={styles.cheatSheetTextWhiteAssNumb}>4,4,3,2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/spade.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>4</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/hearts.png')} />
                        <Text style={styles.cheatSheetTextWhite}>2</Text>
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/diamond.png')} />
                        {/* <Text style={styles.cheatSheetTextWhite}>3</Text> */}
                        <Image style={styles.downPopupAroMd} source={require('./assets/images/down_popup_aro.png')} />
                        {/* <Image style={styles.downPopupAroCard} source={require('./assets/images/down_popup_aro.png')} /> */}
                    </View>
                    <View style={styles.cheatSheetCardBody}>
                        <Image style={styles.cardMrgTop} source={require('./assets/images/clubs.png')} />
                        <Text style={styles.cheatSheetTextWhiteAssNumb}>4</Text> 
                    </View>
                </View>

                 </View>

   </View>
</View>

  );
};


const styles = StyleSheet.create({
    popupBodyAreaTotalRht:{
        position:"absolute",
        width:131,
        height:210,
        right:74,
        borderWidth:0,
        borderColor:"red",
        flexDirection:"column",
    },
    popupBodyAreaTotalMd:{
        position:"absolute",
        width:131,
        height:210,
        borderWidth:0,
        borderColor:"red",
        flexDirection:"column",
        alignSelf:"center",
        zIndex:100,
    },
    popupBodyAreaTotalLft:{
        position:"absolute",
        width:131,
        height:210,
        left:78,
        borderWidth:0,
        borderColor:"red",
        flexDirection:"column",
    },
    popupBodyAreaRow:{
        width:131,
        height:43,
        flexDirection:"row",
    },
    popupBodyAreaRowBox:{
        width:43,
        height:43,
        backgroundColor:"#151515",
        borderRightWidth:1,
        borderRightColor:"#0a0a0a",
        borderBottomColor:"#0a0a0a",
        borderBottomWidth:1,
        alignItems:"center",
        justifyContent:"center",
    },
    popupBodyAreaRowBoxBtm:{
        width:64.5,
        height:38,
        backgroundColor:"#151515",
        borderRightWidth:1,
        borderRightColor:"#0a0a0a",
        borderBottomColor:"#0a0a0a",
        borderBottomWidth:1,
        alignItems:"center",
        justifyContent:"center",
    },
    popupBodyAreaRowBoxText:{
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "300",
        color:"#FFFFFF",
        textAlign:"center",
        alignItems:"center",
        justifyContent:"center",
    },
    /*******************/
    downPopupAroMd:{
        position:"absolute",
        bottom:12,
    },
    downPopupAro:{
        position:"absolute",
        right:10,
        bottom:10,
    },
    downPopupAroCard:{
        position:"absolute",
        right:5,
        bottom:10,
    },
    cheatSheetTotalBodyArea:{
        flexDirection:"row", 
        width:340, 
        height:45, 
        alignSelf:"center",
    },
    cheatSheetTotalBodyAreaEast:{
        flexDirection:"row", 
        width:340, 
        height:45, 
        alignSelf:"center",
        flexDirection:"row-reverse",
    },
    cheatSheetPatternBody:{
        width:80, 
        height:45, 
        backgroundColor:"#151515",
        borderRightWidth:1,
        borderRightColor:"#0a0a0a",
        alignItems:"center",
        flexDirection:"column",
        justifyContent:"space-between",
    },
    cheatSheetCardBody:{
        width:45, 
        height:45, 
        backgroundColor:"#151515",
        alignItems:"center",
        borderRightWidth:1,
        borderRightColor:"#0a0a0a",
        flexDirection:"column",
        justifyContent:"space-between",
       
    },
    cheatSheetTextWhite: {
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FFFFFF",
        textAlign:"center",
        bottom:4,
      },
      cheatSheetTextWhiteAssNumb: {
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#676767",
        textAlign:"center",
        bottom:4,
      },
      cheatSheetTextWhiteAssQues: {
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#353535",
        textAlign:"center",
        
      },
    cheatSheetTextOffWhite: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#676767",
        textAlign:"center",
        top:4,
      },
      cardMrgTop:{
        alignSelf:"center",
        top:7,
      },

    /******************************************************* */
    tkbImgMrgTp:{
        marginTop:4,
        marginLeft:8,
        marginRight:8,
    },
    shareHandTextHandName: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#6D6D6D",
        textAlign:"center",
        paddingTop:3,
        
      },
      shareHandTextHandNameBlack: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#0a0a0a",
        textAlign:"center",
        paddingTop:3,
       
      },
      shareHandTextHandNameRed: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#FF0C3E",
        textAlign:"center",
        paddingTop:3,
       
      },
      shareHandText: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#6D6D6D",
        textAlign:"center",
        paddingTop:3,
      },

    /********************************* */
    
    /************************************/

    
    playingEastWestCardTotalWidthHeightArea:{
        flexDirection:"row", 
        width:"100%", 
        height:55, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    playingCardTotalWidthHeightArea:{
        position:"absolute",
        width:225, 
        height:225,
        flexDirection:"column", 
        alignItems:"center",
        justifyContent:"space-between",
    },
    totalWidthheightPositionDirectionArea:{
        position:"absolute",
        flexDirection:"row", 
        width:"100%", 
        height:225, 
        justifyContent:"center", 
        alignItems:"center",
    },
    pointerEastWestAreaPosition:{
        flexDirection:"row", 
        justifyContent:"space-between", 
        alignItems:"center", 
        paddingLeft:3, 
        paddingRight:3,
    },
    patternTotalColumnArea:{
        flexDirection:"column", 
        width:"100%", 
        height:45,  
        alignItems:"center",
    },
    patternTotalRowArea:{
        flexDirection:"row",
        width:181, 
        height:45, 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    hanNorthSauthTotalWidthHeightArea:{
        width:"100%", 
        height:68, 
        alignItems:"flex-end",
        flexDirection:"column",
    },
    hanNameWidthHeightArea:{
        width:"100%", 
        height:23,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
    },
    patternWidthHeightArea:{
        width:90, 
        height:45, 
        backgroundColor:"#151515",
    },
    // cardDisplayBodyNorth:{
    //     width:61,
    //     height:45,
    //     borderBottomLeftRadius:4,
    //     borderBottomRightRadius:4,
    //     borderWidth:1,
    //     borderColor:"#0a0a0a",
    //     backgroundColor:"#151515",
    // },
        cardDisplayBodyNorth:{
        width:41,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
    },
    cardDisplayBodyEast:{
        width:61,
        height:45,
        borderBottomLeftRadius:4,
        borderBottomRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515",
        flexDirection:"row-reverse",
    },
    cardPositionW:{
        width:22,
        marginTop:3,
        marginLeft:1,
        alignItems:"center",
        justifyContent:"center",
     },
     cardPositionE:{
        width:22,
        marginTop:-4,
        marginLeft:2,
        alignItems:"center",
        justifyContent:"center",
     },
    directionPointerPosition:{
        width:27,
        height:27,
        borderWidth:0,
        borderColor:"red",
        alignItems:"center",
        justifyContent:"center",
    },
    ContractTextSpade: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#C000FF",
    },
    ContractTextHearts: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    ContractTextDiamond: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#04AEFF",
    },
    ContractTextClubs: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#79E62B",
    },
    bidPlayText: {
        top:5,
        fontFamily:"Roboto-Thin",
        fontSize: 55,
        color:"#353535",
        fontWeight:"normal",
      },
      bidPlayTextBlack: {
        fontFamily:"Roboto-Thin",
        fontSize: 55,
        color:"#0a0a0a",
        fontWeight:"normal",
      },
    CardPositionBorderAss:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#676767",
    },
    northCardPosition:{
        width:55,
        height:81,
    },
    sauthCardPosition:{
        width:55,
        height:81,
    },
    westCardPosition:{
        width:55,
        height:81,
        marginLeft: 12, 
        transform: [{ rotate: '-90deg' }],
    },
    eastCardPosition:{
        width:55,
        height:81,
        marginRight: 12, 
        transform: [{ rotate: '90deg' }],
    },
    playCardNumberPosition:{
        position:"absolute",
        width:55,
        height:81,
        borderWidth:0,
        borderColor:"red",
        paddingTop:2,
        paddingLeft:6,

    },
    playCardNumberDiamond:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
    },
    playCardDiamond:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberHearts:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
    },
    playCardHearts:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberClubs:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
    },
    playCardClubs:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardNumberSpade:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
    },
    playCardSpade:{
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        backgroundColor:"#151515",
        alignItems:"center",
        justifyContent:"center",
    },
    playCardDirectionPosition:{
        width:60, 
        height:60, 
        justifyContent:"space-between", 
        flexDirection:"column",
    },
    DirectionText: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#676767",
        textAlign:"center", 
      }, 
      DirectionTextActive: {
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
      }, 


/*************************************** */

    SharePatternCardSpade:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#C000FF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardHearts:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#FF0C3E",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardDiamond:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#04AEFF",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    SharePatternCardClubs:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#79E62B",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    PatternText:{
        fontFamily:"Roboto-Light",
        fontSize: 14,
        fontWeight: "400",
        color:"#FFFFFF",
    },
    SharePatternCard:{
        flexDirection:"column",
        width:55,
        height:81,
        borderRadius:4,
        borderWidth:1,
        borderColor:"#DBFF00",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#151515",
    },
    // cardDisplayBody:{
    //     width:61,
    //     // height:91,
    //     height:45,
    //     borderTopLeftRadius:4,
    //     borderTopRightRadius:4,
    //     borderWidth:1,
    //     borderColor:"#0a0a0a",
    //     backgroundColor:"#151515"
    // },
    cardDisplayBody:{
        width:41,
        // height:91,
        height:45,
        borderTopLeftRadius:4,
        borderTopRightRadius:4,
        borderWidth:1,
        borderColor:"#0a0a0a",
        backgroundColor:"#151515"
    },
    
    cardPosition:{
        width:22,
        marginTop:3,
        marginLeft:4,
        alignItems:"center",
        justifyContent:"center",
     },
     SpadeCardNumber:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#C000FF",
     },
     heartsCardNumber:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#FF0C3E",
     },
     clubsCardNumber:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#79E62B",
     },
     diamondCardNumber:{
        fontFamily:"Roboto-Light",
        fontSize: 16,
        fontWeight: "400",
        color:"#04AEFF",
     },

     ContractTextHeader: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
    },
    ContractText: {
        position:"absolute",
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
        top:8,
    },
    ContractTextBlack: {
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#0a0a0a",
    },
    ContractMdlText:{
        fontFamily:"Roboto-Light",
        fontSize: 13,
        fontWeight: "400",
        color:"#353535",
        paddingRight:4,
    },
    ShareText: {
        fontFamily:"Roboto-Thin",
        fontSize: 55,
        fontWeight: "normal",
        color:"#6D6D6D",
        paddingTop:10,
      },
      
     
    OpenText: {
        fontFamily:"Roboto-Light",
        fontSize: 11,
        fontWeight: "400",
        color:"#DBFF00",
        textAlign:"center", 
        padding:4,
    },
  });

 export default ViewBoxesWithColorAndText;
