import React from 'react';
import { Text, View, Alert, TextInput, BackHandler, Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import { APPLABELS } from '../../constants/applabels';
import { handleBackButton, HOOL_CLIENT } from '../../shared/util';
import { ScaledSheet, verticalScale, scale, moderateScale } from 'react-native-size-matters/extend';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { normalize } from '../../styles/global';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SignInConstants } from '../../redux/actions/SignInAction';
import { connect } from 'react-redux';
import { HOOL_EVENTS } from '../../constants/hoolEvents';
import { API_ENV } from '../../redux/api/config/config';
import { ChatConstants } from '../../redux/actions/ChatActions';

class LoginRegister extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      spinner: false
    }
  }

  async saveArticle(key, value) {
    try {
      await EncryptedStorage.setItem(key, value);
    } catch (error) {
      // There was an error on the native side
      console.log(error);
    }
  }

  async componentDidUpdate(){
    // if(this.props.jwt != ''&& prevProps.jwt != this.props.jwt){
    //   let username = this.props.username;
    //   let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    //   if (!emailReg.test(this.props.username)) {
    //     username = username + API_ENV.userSuffix;
    //   }
    //   this.props.showSpinner({
    //     showSpinner: false
    //   })
    //   await HOOL_CLIENT.xmppStop();
    //   await HOOL_CLIENT.xmppConnect(username, this.props.password);
    // }
    if( this.props.showLogin){
      let username = this.props.username;
      let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
      if (!emailReg.test(this.props.username)) {
        username = username + API_ENV.userSuffix;
      }
      this.props.disbleLoginConnect();
      await HOOL_CLIENT.xmppConnect(username, this.props.password);
    }
  }

  async componentDidMount() {
    this.props.showMenuScreen({
      showMenu: false
    })
    this.props.setLoginStyle({
      loginBtnStyle: styles.submitButtonArea,
      loginTextStyle: styles.loginText,
      disableLogin: false,
    })
    this.willFocusSubscription = this.props.navigation.addListener(
      'focus',
      () => {
        EncryptedStorage.getItem('username').then(userName => {
          if (userName !== null) {
            let userText = userName.split('@');
            userName = userText[0];
            this.props.setUserName({
              username: userName
            });
          }
        });
        EncryptedStorage.getItem('password').then(password => {
          this.props.setPassword({
            password: password
          });
          this.enableLogin();
        });
      }
    );
    await this.initiateEventemitter();
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }

  async initiateEventemitter() {
    this.emitterConnected = HOOL_CLIENT.addListener(HOOL_EVENTS.CONNECTED, async e => {
      this.props.fetchFriendsList()
      this.props.showSpinner({
        showSpinner: false
      })
      await this.saveArticle('username', this.props.username);
      await this.saveArticle('password', this.props.password);
      AsyncStorage.setItem('auth', 'true').then(() => {
        this.clearListeners();
        this.props.setSignInSuccessfull(true)
      });
    })

    this.emitterConnectionError = HOOL_CLIENT.addListener(HOOL_EVENTS.CONNECTION_ERROR, e =>{
      console.log("Error :",e)
      this.props.showSpinner({
        showSpinner: false
      })
      //this.setState({spinner: getFirstInstallTimeSync})
      this.props.setLoginFailure({
        message: 'Please try Again'
      })
    })
   this.emitterError =  HOOL_CLIENT.addListener(HOOL_EVENTS.ERROR, e =>{
    console.log("Error :",e)

    this.props.showSpinner({
      showSpinner: false
    })
      // this.setState({spinner: false})
      this.props.setLoginFailure({
        message: e.message
      })
    })
  }

  componentWillUnmount() {
    this.clearListeners();
    this.willFocusSubscription;
  }

  clearListeners() {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
    this.emitterConnected.removeAllListeners()
    this.emitterConnectionError.removeAllListeners()
    this.emitterError.removeAllListeners()
  }
  
  onBlur() {
    this.props.setTextColor({
      txtcolor: 'white',
    });
    this.enableLogin();
  }

  onPwdBlur() {
    this.enableLogin();
  }

  enableLogin() {
    if (this.props.username !== '' && this.props.password !== '') {
      this.props.setLoginStyle({
        loginBtnStyle: styles.submitButtonAreaActive,
        loginTextStyle: styles.loginTextActive,
        disableLogin: false,
      })
    } else {
      this.props.setLoginStyle({
        loginBtnStyle: styles.submitButtonArea,
        loginTextStyle: styles.loginText,
        disableLogin: true,
      })
    }
  }

  showSignUp() {
    this.clearListeners();
    this.props.navigation.navigate('Signup', {});
  }

  showForgotPassword() {
    this.clearListeners();
    this.props.navigation.navigate('ForgotPassword', {});
  }

  async loginUser (){
    this.props.fetchUsersList()
    this.props.fetchNotificationList()
    this.props.startApp()
    let username = this.props.username;
    let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (!emailReg.test(this.props.username)) {
      username = username + API_ENV.userSuffix;
    }
    //this.setState({spinner: true})
    // await HOOL_CLIENT.xmppStop()
    console.log("--------------",username);
    console.log("--------------",this.props.password);
    this.props.signInUser({
      username: username,
      password: this.props.password
    })    
  }


  render() {
    return (
      <KeyboardAwareScrollView style={{backgroundColor:'#0A0A0A'}}>
      <View style={styles.loginBody}>
          {
            this.props.message != '' &&
            <Text style={styles.sgnMsgAreaTextFunky}>
              {this.props.message}
            </Text>
          }
        <Spinner
          visible={this.props.spinner}
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={{...styles.loginTotalBody}}>
          <View style={styles.inputBoxArea}>
            <TextInput
              onBlur={() => this.onBlur()}
              style={styles.input}
              placeholder={APPLABELS.PLACEHOLDER_USERNAME}
              placeholderTextColor="#6D6D6D"
              autoCapitalize="none"
              keyboardType={'ascii-capable'}
              disableFullscreenUI={true}
              onChangeText={value => this.props.setUserName({ username: value })}
              value={this.props.username}
            />
          </View>
          <View style={styles.passwordInputBoxArea}>
            <TextInput
              onBlur={() => this.onPwdBlur()}
              style={styles.input}
              secureTextEntry={true}
              placeholder={APPLABELS.PLACEHOLDER_PASSWORD}
              placeholderTextColor="#6D6D6D"
              autoCapitalize='none'
              disableFullscreenUI={true}
              onChangeText={value => {
                let disableLogin = true;
                this.enableLogin();
                if (value.trim() !== '') {
                  disableLogin = false;
                }
                this.props.setPassword({disableLogin: disableLogin, password: value})
              }}
              value={this.props.password}
            />
          </View>
          <View style={{width: scale(273),alignSelf: 'flex-end', paddingRight: moderateScale(5)}}>
            <TouchableOpacity 
                onPress={() => this.showForgotPassword()}>
              <Text style={styles.forgotPasswordText}>
                {APPLABELS.FORGOTPASSWORD}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={this.props.loginBtnStyle}>
            <TouchableOpacity
              activeOpacity={0.3}
              onPress={() =>{
                this.loginUser()
                this.props.showSpinner({
                  showSpinner: true
                })
              }}
              disabled={this.props.disableLogin}>
              <Text style={{ ...this.props.loginTextStyle }}>{APPLABELS.LOGIN}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ ...styles.singUpView }}>
            <TouchableOpacity onPress={() => this.showSignUp()}>
              <Text style={styles.singUpText}>{APPLABELS.SIGNUP}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = ScaledSheet.create({
  loginBody: {
    backgroundColor: '#0A0A0A',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginTotalBody: {
    flexDirection: 'column',
    width: '273@vs',
  },
  inputBoxArea: {
    height: '23.14@s',
    width: '273@vs',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '86@ms',
    borderBottomWidth: '1@ms',
    borderBottomColor: '#1B1B25',
  },
  passwordInputBoxArea: {
    height: '35@s',
    width: '273@vs',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '31@ms',
    borderBottomWidth: '1@ms',
    borderBottomColor: '#1B1B25',
  },
  input: {
    width: '273@vs',
    fontFamily: 'Roboto-Light',
    color: '#fff',
    fontSize: normalize(13),
    height: '40@s'
  },
  submitButtonArea: {
    height: '54@s',    
    width: '273@vs',
    marginTop: '40@ms',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: '1@ms',
    borderColor: '#1B1B25',
  },
  submitButtonAreaActive: {
    height: '54@s',
    width: '273@vs',
    marginTop: '40@ms',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: '1@ms',
    borderColor: '#DBFF00',
  },
  loginText: {
    width: '273@vs',
    height: '54@s',
    fontFamily: 'Roboto-Light',
    color: '#6D6D6D',
    fontSize: normalize(13),
    textAlign: 'center',
    textAlignVertical: 'center',
    ...Platform.select({
      ios: {
          lineHeight: '54@s'
      },
      android: {}
  })
  },
  loginTextActive: {
    width: '273@vs',
    height: '54@s',
    fontFamily: 'Roboto-Light',
    color: '#FFF',
    fontSize: normalize(13),
    textAlign: 'center',
    textAlignVertical: 'center',

    ...Platform.select({
      ios: {
          lineHeight: '54@s'
      },
      android: {}
  })
  },
  forgotPasswordText: {
    fontFamily: 'Roboto-Italic',
    color: '#FFFFFF',
    fontSize: normalize(10),
    textAlign: 'right',
    marginTop: moderateScale(5)
  },
  singUpView: {
    height: '27@s',
    width: '86@vs',
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: moderateScale(24), 
    marginBottom: moderateScale(35)
  },
  singUpText: {
    fontFamily: 'Roboto-Light',
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: normalize(13),
  },
  spinnerTextStyle: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FFF',
  },
  sgnMsgAreaTextFunky: {
    fontFamily: 'Roboto-LightItalic',
    position: 'absolute',
    alignSelf: 'center',
    top: '13@ms',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: normalize(13),
    color: '#FF0C3E',
  },
});


const mapStateToProps = (state) => ({
  spinner: state.signInActions.showSpinner,
  password:  state.signInActions.password,
  username: state.signInActions.username,
  loginTextStyle: state.signInActions.loginTextStyle,
  loginBtnStyle: state.signInActions.loginBtnStyle,
  loginBtnStyle: state.signInActions.loginBtnStyle,
  usernameReg: state.signInActions.usernameReg,
  disableLogin: state.signInActions.disableLogin,
  showLogin: state.signInActions.showLogin,
  passwordReg: state.signInActions.passwordReg,
  confirmPassword: state.signInActions.confirmPassword,
  email: state.signInActions.email,
  message: state.signInActions.message,
  jwt: state.signInActions.jwt
});

const mapDispatchToProps = (dispatch) => ({
  signInUser: (params) =>
    dispatch({ type: SignInConstants.SIGN_IN, params: params }),
  showSpinner: (params) =>
    dispatch({ type: SignInConstants.SHOW_SPINNER, params: params }),
  setUserName: (params) =>
    dispatch({ type: SignInConstants.SET_USERNAME, params: params }),
  setPassword: (params) =>
    dispatch({ type: SignInConstants.SET_PASSWORD, params: params }),
  setEnableDisableLogin: (params) =>
    dispatch({ type: SignInConstants.LOGIN_ENABLE_DISABLE, params: params }),
  setTextColor: (params) =>
    dispatch({ type: SignInConstants.SET_TEXT_COLOR, params: params }),
  setLoginStyle: (params) =>
    dispatch({ type: SignInConstants.SET_LOGIN_STYLE, params: params }),
  setLoginFailure: (params) =>
    dispatch({ type: SignInConstants.SIGN_IN_FAILURE, params: params }),
  startApp: () =>
   dispatch({ type: SignInConstants.START_APP }),
  fetchNotificationList: () =>
   dispatch({ type: ChatConstants.FETCH_NOTIFICATION_LIST}),
  fetchUsersList: () => 
   dispatch({ type: ChatConstants.FETCH_USERS_LIST}),
  disbleLoginConnect: () =>
   dispatch({ type: SignInConstants.DISABLE_LOGIN}),
  showMenuScreen: (params) =>
   dispatch({ type: SignInConstants.SHOW_MENU, params: params}),
  fetchFriendsList: (params) =>
    dispatch({ type: ChatConstants.FETCH_FRIENDS_LIST, params: params }),
  setSignInSuccessfull: (params) => 
    dispatch({ type: SignInConstants.SIGNIN_SUCCESSFULL, params: params })
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegister);

