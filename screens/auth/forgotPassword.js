import React, { Component } from 'react';
import {
  BackHandler,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { handleBackButton } from '../../shared/util';
import { APPLABELS } from '../../constants/applabels';
import { ScaledSheet, verticalScale , scale} from 'react-native-size-matters/extend';
import { SvgXml } from 'react-native-svg';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CommonActions } from '@react-navigation/native';


export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      disableReset: true,
      isResetSentSuccess: false,
      showError: false,
      username: '',
      password: '',
      confirmPassword: '',
      resetBtnStyle: styles.resetPasswordButtonArea,
      resetTextStyle: styles.resetPasswordText
    };
  }

  handleCurrentBackButton = () => {
    this.clearListeners();
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    this.props.navigation.goBack();
    return true;
  };

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  goBack() {
    this.props.navigation.dispatch(CommonActions.navigate({
      name: "Login"
    }));
  }

  componentWillUnmount() {
    this.clearListeners();
  }

  clearListeners() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  enableResetForm() {
    this.setState({
      showError: false
    });
    let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (this.state.username !== '') {
      this.setState({
        resetBtnStyle: styles.resetPasswordButtonAreaActive,
        resetTextStyle: styles.resetPasswordTextActive,
        disableReset: false
      });
    } else {
      this.setState({
        resetBtnStyle: styles.resetPasswordButtonArea,
        resetTextStyle: styles.resetPasswordText,
        disableReset: true
      });
    }
  }

  onFocus() {
    // this.setState({
    //   txtcolor: 'black'
    // });
  }

  onBlur() {
    this.enableResetForm();
  }

  async resetRequest() {
    try {
      this.setState({ spinner: true, showError: false });
      let data = {
        jid: this.state.username
      };
      console.log('data', data);
      const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      };
      fetch('http://xmpp.hool.org:5000/api/reset_password', requestOptions)
        .then(response => response.json())
        .then(response => {
          console.log('res');
          console.log(response);
          if (response.status === 'ok') {
            console.log('success');
            let successMessage = response.message
              ? response.message
              : 'Please verify your email';
            this.setState({ isResetSentSuccess: true, successMessage });
          } else {
            let errorMessage = response.message
              ? response.message
              : 'Something went wrong. try again later';
            this.setState({ showError: true, errorMessage });
          }
          this.setState({ spinner: false });
        })
        .catch(error => {
          console.error(error);
          this.setState({ spinner: false });
        });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: '#0a0a0a', height: '100%' }}>
        <KeyboardAwareScrollView style={styles.loginBody}>
          <Spinner
            visible={this.state.spinner}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
          />
          <TouchableOpacity
            style={styles.backPosition}
            onPress={() => this.goBack()}
          >
            <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.back} />
          </TouchableOpacity>
          <View style={styles.loginBackgroundView}>
            {this.state.isResetSentSuccess ? (
              <View style={styles.msgTextPositionArea}>
                <Text style={styles.msgText}>
                  {/* An verification link has been sent to{' '}
                  <Text></Text> */}
                  {this.state.successMessage}
                </Text>
                <Text style={styles.msgText}>
                  Please click the link to reset your password!
                </Text>
              </View>
            ) : (
              <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                <View style={styles.inputBoxAreaPassword}>
                  <TextInput
                    onBlur={() => this.onBlur()}
                    onFocus={() => this.onFocus()}
                    onChangeText={value => this.setState({ username: value })}
                    style={styles.input}
                    placeholder="Username OR email"
                    placeholderTextColor="#6D6D6D"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => this.resetRequest()}
                  disabled={this.state.resetRequest}
                  style={this.state.resetBtnStyle}
                >
                  <Text style={this.state.resetTextStyle}>Resend Email</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}
const styles = ScaledSheet.create({
  msgTextPositionArea: {
    marginBottom: '50@ms',
    alignItems: 'center'
  },
  backPosition: {
    position: 'absolute',
    right: '0@ms',
  },
  msgText: {
    fontFamily: 'Roboto-Light',
    color: '#FF13FFFF',
    fontSize: normalize(13),
  },
  loginBody: {
    height: '100%',
    width: '100%',
  },
  loginBackgroundView: {
    height: '100%',
    width: '100%',
    marginTop: '70@ms',
  },
  resetPasswordButtonArea: {
    backgroundColor: '#0a0a0a',
    width: '273@vs',
    height: '54@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#1B1B25',
    marginTop: '93@ms',
    marginBottom: '46@ms'
  },
  resetPasswordButtonAreaActive: {
    width: '273@vs',
    height: '54@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    marginTop: '93@ms',
    marginBottom: '46@ms',
    borderColor: '#DBFF00',
    alignSelf: 'center'
  },
  resetPasswordText: {
    fontFamily: 'Roboto-Light',
    color: '#6D6D6D',
    fontSize: normalize(13),
    paddingTop: '15@ms',
    paddingBottom: '16@ms',
    paddingStart: '50@ms',
    paddingEnd: '50@ms',
  },
  resetPasswordTextActive: {
    fontFamily: 'Roboto-Light',
    color: '#FFFFFF',
    fontSize: normalize(13),
    paddingTop: '15@ms',
    paddingBottom: '16@ms',
    paddingStart: '50@ms',
    paddingEnd: '50@ms',
  },
  inputBoxAreaPassword: {
    backgroundColor: '#0a0a0a',
    width: '273@vs',
    height: '30@s',
    borderBottomWidth: '1@ms',
    borderBottomColor: '#1B1B25',
  },
  input: {
    fontFamily: 'Roboto-Light',
    color: '#fff',
    fontSize: normalize(13),
    height: '40@s'
  }
});


