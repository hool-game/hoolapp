import React from 'react';
import {
  View,
  Text,
  TextInput,
  BackHandler,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  Keyboard,
} from 'react-native';

import { Dropdown } from 'react-native-element-dropdown';
import Spinner from 'react-native-loading-spinner-overlay';
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { APPLABELS } from '../../constants/applabels';
import { countries, handleBackButton } from '../../shared/util';
import {
  ScaledSheet,
  scale,
  moderateScale,
  verticalScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { SignUpConstants } from '../../redux/actions/SignUpAction';
import { connect } from 'react-redux';
import EncryptedStorage from 'react-native-encrypted-storage';
import { CommonActions } from '@react-navigation/native'

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      password: '',
      isPasswordSecure: true
    }
  }

  goBack() {
    this.props.clearAllField()

    // to goBack to login page
    this.props.navigation.dispatch(CommonActions.navigate({
      name: 'Login'
    }));
  }

  handleCurrentBackButton = () => {
    this.clearListeners();
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    this.props.navigation.goBack();
    return true;
  };

  componentDidMount() {
    this.props.setSignUpStyle({
      signupBtnStyle: styles.submitButtonArea,
      signupTextStyle: styles.signupText,
      disableSignup: false
    })
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  componentWillUnmount() {
    this.props.setRegisterSuccess({
      isRegisterSuccess: false
    })
    this.clearListeners();
  }

  componentDidUpdate(prevProps){
    if(prevProps.isRegisterSuccess != this.props.isRegisterSuccess && this.props.username != '' && this.props.password != ''){
      EncryptedStorage.setItem('username', this.props.username);
      EncryptedStorage.setItem('password', this.props.password);
    }
    if(prevProps.agreeTerms != this.props.agreeTerms){
      this.enableSignup()
    }
  }

  clearListeners() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleCurrentBackButton
    );
  }

  enableSignup() {
    this.props.setErrorMessage({
      showError: false,
      errorMessage: '',
      username: this.props.username
    });
    let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    var format =  /[!@#%^&*()+\~=\[\]{};`':"\\|,.<>\/?]/;
    if (
      format.test(this.props.username)
    ) {
      this.props.setErrorMessage({
        showError: true,
        errorMessage: 'Please create UserName without Special Character !#%^*()[]"|{}&\'/:<>@',
        username: this.props.username
      });
    }else{
      if (
        this.props.username !== '' &&
        this.props.password !== '' &&
        this.props.confirmPassword !== '' &&
        this.props.email !== '' &&
        this.props.country !== '' &&
        this.props.password === this.props.confirmPassword &&
        emailReg.test(this.props.email.trim()) &&
        this.props.agreeTerms
      ) {
        this.props.setSignUpStyle({
          signupBtnStyle: styles.submitButtonAreaActive,
          signupTextStyle: styles.signupTextActive,
          disableSignup: false,
        });
      } else {
        if (
          this.props.password !== '' &&
          this.props.confirmPassword !== '' &&
          this.props.password !== this.props.confirmPassword
        ) {
          this.props.setErrorMessage({
            showError: true,
            errorMessage: 'Password and confirm password mismatch',
            username: this.props.username
          });
        } else if (
          this.props.email !== '' &&
          emailReg.test(this.props.email.trim()) === false
        ) {
          this.props.setErrorMessage({
            showError: true,
            errorMessage: 'Please enter valid email',
            username: this.props.username
          });
        }
        this.props.setSignUpStyle({
          signupBtnStyle: styles.submitButtonArea,
          signupTextStyle: styles.signupText,
          disableSignup: true,
        });
      }
    }
  }

  onBlur() {
    this.enableSignup();
  }

  userNameValidation(value) {
    var format =  /[!@#%^&*()+\~=\[\]{};`':"\\|,.<>\/?]/;
    if(format.test(value)){
      this.props.setErrorMessage({
        username: value,
        showError: true,
        errorMessage: 'Please create User Name without special characters: ! # % ^ * () [] " | {} & \'/ : <> @'
      })
    }else{
      this.props.setErrorMessage({
        username: value,
        showError: false,
        errorMessage: ''
      })
    }
  }

  signupRequest(){
    this.props.registerUser({
      username: this.props.username,
      password: this.props.password,
      email: this.props.email,
      name: this.props.name,
      location: this.props.country
    })
  }

  // setPasswordSecure(){
  //   this.setState({
  //     isPasswordSecure: true
  //   })
  // }

  countriesFilterByName = (name) =>{
    const items = countries.filter(data => 
      data.name.toUpperCase().startsWith(name.toUpperCase())
    )
    this.props.setFilteredItems({
      filteredItems: items
    })
  }
  
  render() {
    return (
      <SafeAreaView style={{ backgroundColor: '#0a0a0a', height: '100%' }}>
        <KeyboardAwareScrollView style={{...styles.signUpBody}} showsVerticalScrollIndicator={true}
          contentContainerStyle={{flexGrow: 1}}>
          <Spinner
            visible={this.props.spinner}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
          />
          <TouchableOpacity
            style={styles.backPosition}
            onPress={() => this.goBack()}
          >
            <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.back} />
          </TouchableOpacity>
          {this.props.showError ? (
            <Text style={styles.sgnMsgAreaTextFunky}>
              {this.props.errorMessage}
            </Text>
          ) : null}
          {this.props.isRegisterSuccess ? (
            <View style={{flex: 1,right:scale(45)}}>
              <Text style={styles.sgnMsgAreaTextWooh}>
                Woohoo! Your account has been created!
              </Text>

              <View style={styles.verificationLink}>
                <Text style={styles.verificationLinkText}>
                  An verification link has been sent to{' '}
                  <Text>{this.props.email}</Text>
                </Text>
                <Text style={styles.verificationLinkText}>
                  Please verify your account to get started!
                </Text>
              </View>
            </View>
          ) : (
            <View style={styles.signUpTotalBodyWidth}>
              <View style={styles.twoClmTotal}>
                <View style={{...styles.sgnLeftRight}}>
                  <View style={styles.sgnInputBoxArea}>
                    <TextInput
                      onBlur={() => this.onBlur()}
                      autoCapitalize="none"
                      disableFullscreenUI={true}
                      onChangeText={value =>  this.userNameValidation(value)}
                      returnKeyType = "next"
                      style={styles.sgnInput}
                      placeholder="User Name"
                      placeholderTextColor="#6D6D6D"
                      onSubmitEditing={(event) => { 
                        this.refs.emailInput.focus(); 
                      }}
                    />
                  </View>
                  <View
                    style={{
                      ...styles.sgnInputBoxArea,
                      marginTop: moderateScale(36)
                    }}
                  >
                    <TextInput
                      ref='Password'
                      //{ref => ref && ref.setNativeProps({ 
                        //style : { fontFamily: 'Roboto-Light'}})}

                      //ref='passwordInput'
                      secureTextEntry={true}
                      disableFullscreenUI={true}
                      onBlur={() => this.onBlur()}
                      autoCapitalize="none"
                      returnKeyType = "next"
                      onChangeText={value => this.props.setPassword({password: value})}
                      style={
                        styles.sgnInput
                      }
                      placeholder='Password'
                      fontFamily='Roboto-Light'
                      placeholderTextColor="#6D6D6D"
                      onSubmitEditing={(event) => { 
                        this.refs.nameInput.focus(); 
                      }}
                    />
                  </View>
                  <View
                    style={{
                      ...styles.sgnInputBoxArea,
                      marginTop: moderateScale(36),
                    }}
                  >
                    <TextInput
                      ref='confirmpasswordInput'
                      onBlur={() => this.onBlur()}
                      onChangeText={value =>
                        this.props.setConfirmPassword({ confirmPassword: value })
                      }
                      disableFullscreenUI={true}
                      style={styles.sgnInput}
                      autoCapitalize="none"
                      placeholder={APPLABELS.PLACEHOLDER_CONFIRM_PASSWORD}
                      placeholderTextColor="#6D6D6D"
                    />
                  </View>
                </View>

                <View style={{ ...styles.sgnLeftRight, paddingStart: moderateScale(20)}}>
                  <View style={styles.sgnInputBoxArea}>
                    <TextInput
                      ref='emailInput'
                      onBlur={() => this.onBlur()}
                      autoCapitalize="none"
                      returnKeyType = "next"
                      disableFullscreenUI={true}
                      onChangeText={value => this.props.setEmail({ email: value })}
                      style={styles.sgnInput}
                      placeholder={APPLABELS.PLACEHOLDER_EMAIL}
                      placeholderTextColor="#6D6D6D"
                      onSubmitEditing={(event) => { 
                        this.refs.passwordInput.focus(); 
                      }}
                    />
                  </View>
                  <View
                    style={{
                      ...styles.sgnInputBoxArea,
                      marginTop: moderateScale(36)
                    }}
                  >
                    <TextInput
                      ref='nameInput'
                      returnKeyType = "next"
                      onBlur={() => this.onBlur()}
                      onChangeText={value => this.props.setName({ name: value })}
                      style={styles.sgnInput}
                      autoCapitalize="none"
                      disableFullscreenUI={true}
                      placeholder={APPLABELS.PLACEHOLDER_NAME}
                      placeholderTextColor="#6D6D6D"
                      onSubmitEditing={(event) => { 
                        this.refs.confirmpasswordInput.focus(); 
                      }}
                    />
                  </View>
                  <View
                    style={{
                      ...styles.sgnInputBoxArea,
                      marginTop: moderateScale(36),
                    }}
                  >
                    <Dropdown
                      style={styles.dropdown2BtnStyle}
                      data={this.props.filteredItems.length >0? this.props.filteredItems: countries}
                      renderItem={(country) => <Text style={{...styles.signupText,color: '#fff', backgroundColor: '#000', height: scale(54), alignSelf: 'center' }}>{country.name}</Text>}
                      onChange={(selectedCountry) => {
                        this.props.setCountry({ country: selectedCountry.code3 }, () => {
                          this.onBlur();
                        });
                      }}
                      containerStyle={{
                        backgroundColor: '#000', borderColor: '#1B1B25',
                        // left: moderateVerticalScale(40),
                        minWidth: verticalScale(244), minHeight: scale(200),
                        top: Platform.OS == 'android'&& moderateVerticalScale(-25), 
                        maxWidth: verticalScale(244), maxHeight: scale(200),
                        position:'absolute'
                      }}
                      keyboardAvoiding={true}
                      value={this.props.country}
                      labelField='name'
                      valueField='code3'
                      search={true}
                      // dropdownPosition={'top'}
                      renderInputSearch={(onSearch) => (<TextInput
                        placeholderTextColor='#6D6D6D'
                        placeholder='Search by Country'
                        disableFullscreenUI={true}
                        style={{
                          ...styles.dropdown2BtnTxtStyle,
                          marginTop: moderateScale(15),
                          paddingBottom: moderateScale(15),
                          paddingLeft: moderateScale(10),
                          borderBottomColor: '#1B1B25', borderBottomWidth: 1
                        }}
                        onChangeText={text => {
                          this.countriesFilterByName(text)
                        }}
                        onBlur={() => {
                          Keyboard.dismiss()
                        }}
                      />)}
                      placeholder='Country (optional)'
                      searchPlaceholder='Search by Country'
                      searchPlaceholderStyle={{ ...styles.dropdown2BtnTxtStyle }}
                      placeholderStyle={styles.dropdown2BtnTxtStyle}
                      selectedTextStyle={{ color: '#fff', fontSize: normalize(13), fontFamily: 'Roboto-Light', paddingLeft: scale(2)}}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.totalAgreeArea}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.setAgreeTerms(
                      { agreeTerms: !this.props.agreeTerms })
                  }
                  style={{ width: scale(20), height: scale(20), justifyContent: 'center', alignItems: 'center' }}
                >
                  {this.props.agreeTerms ? (
                    <SvgXml
                      height={scale(8)}
                      width={verticalScale(8)}
                      xml={svgImages.signupFilled}
                      style={{
                        position: 'absolute',
                      }}
                    />
                  ) : null}
                  <SvgXml
                    height={scale(12)}
                    width={verticalScale(12)}
                    xml={svgImages.signUpCircle}
                  />
                </TouchableOpacity>
                <Text style={styles.conditionText}>
                  I agree to the terms and conditions
                </Text>
              </View>

              <View style={this.props.signupBtnStyle}>
                <TouchableOpacity
                  activeOpacity={0.3}
                  onPress={() => this.signupRequest()}
                  disabled={this.props.disableSignup}
                >
                  <Text style={this.props.signupTextStyle}>
                    {APPLABELS.SIGNUP}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}
const styles = ScaledSheet.create({
  totalAgreeArea: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '36@ms',
    flexDirection: 'row',
  },
  backPosition: {
    position: 'absolute',
    right: '0@ms',
  },
  conditionText: {
    fontFamily: 'Roboto-Light',
    color: '#FFFFFF',
    fontSize: normalize(13),
    marginLeft: '6@ms',
  },
  textEditionCondition: {
    fontFamily: 'Roboto-Light',
    color: '#FFFFFF',
    fontSize: normalize(13),
    marginTop: '30@ms',
  },
  verificationLink: {
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center',
    marginTop: '143@ms',
    marginLeft: '158@ms',
    marginRight: '157@ms',
  },
  verificationLinkText: {
    fontFamily: 'Roboto-Light',
    alignSelf: 'center',
    color: '#FFFFFF',
    fontSize: normalize(13),
    flexDirection: 'column'
  },
  sgnMsgAreaTextFunky: {
    fontFamily: 'Roboto-LightItalic',
    position: 'absolute',
    alignSelf: 'center',
    top: '13@ms',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: normalize(13),
    color: '#FF0C3E',
  },
  sgnMsgAreaTextWooh: {
    fontFamily: 'Roboto-LightItalic',
    position: 'absolute',
    alignSelf: 'center',
    top: '13@ms',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: normalize(13),
    color: '#DBFF00',
  },
  signUpBody: {
    backgroundColor: '#0a0a0a',
    height: '100%',
    width: '100%',
  },
  twoClmTotal: {
    flexDirection: 'row'
  },
  signUpTotalBodyWidth: {
    flexDirection: 'column',
    marginStart: '48@ms',
    marginEnd: '48@ms',
    marginTop: '72@ms',
  },
  sgnLeftRight: {
    width: '262@vs',
    flexDirection: 'column',
  },
  signupText: {
    width: '272@vs',
    height: '54@s',
    fontFamily: 'Roboto-Light',
    color: '#6D6D6D',
    fontSize: normalize(13),
    textAlignVertical: 'center',
    textAlign: 'center',
    ...Platform.select({
      ios: {
          lineHeight: '54@s'
      },
      android: {}
  })
  },
  signupTextActive: {
    width: '272@vs',
    height: '54@s',
    fontFamily: 'Roboto-Light',
    color: '#FFFFFF',
    fontSize: normalize(13),
    textAlignVertical: 'center',
    textAlign: 'center',
    ...Platform.select({
      ios: {
          lineHeight: '54@s'
      },
      android: {}
    })
  },
  sgnInputBoxArea: {
    height: '23@s',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: '1@ms',
    borderBottomColor: '#1B1B25',
  },
  sgnInput: {
    width: '262@vs',
    fontFamily: 'Roboto-Light',
    color: '#fff',
    fontSize: normalize(13),
    height: '38@s',
  },
  submitButtonArea: {
    width: '272@vs',
    height: '54@s',
    marginTop: '17@ms',
    marginBottom: '22@ms',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: '1@ms',
    borderColor: '#1B1B25',
    alignSelf: 'center',
  },
  submitButtonAreaActive: {
    width: '272@vs',
    height: '54@s',
    marginTop: '17@ms',
    backgroundColor: '#0a0a0a',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: '1@ms',
    borderColor: '#DBFF00',
    alignSelf: 'center',
  },
  dropdown2BtnStyle: {
    width: '244@vs',
    height: '23@s',
    backgroundColor: '#0a0a0a',
    borderBottomWidth: 1,
    borderBottomColor: '#1B1B25',
  },
  dropdown2BtnTxtStyle: {
    color: '#6D6D6D',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    paddingLeft: scale(2)
  },
  dropdown2DropdownStyle: { backgroundColor: '#0a0a0a' },
  dropdown2RowStyle: {
    backgroundColor: '#0a0a0a',
    borderBottomColor: '#1B1B25',
  },
  dropdown2RowTxtStyle: {
    color: '#FFF',
    textAlign: 'center',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
  },
});

const mapStateToProps = (state) => ({
    spinner: state.signUpActions.spinner,
    disableSignup: state.signUpActions.disableSignup,
    isRegisterSuccess: state.signUpActions.isRegisterSuccess,
    showError: state.signUpActions.showError,
    username: state.signUpActions.username,
    name: state.signUpActions.name,
    password: state.signUpActions.password,
    confirmPassword: state.signUpActions.confirmPassword,
    email: state.signUpActions.email,
    country: state.signUpActions.country,
    agreeTerms: state.signUpActions.agreeTerms,
    signupBtnStyle: state.signUpActions.signupBtnStyle,
    signupTextStyle: state.signUpActions.signupTextStyle,
    filteredItems: state.signUpActions.filteredItems,
    errorMessage: state.signUpActions.errorMessage
});

const mapDispatchToProps = (dispatch) => ({
  registerUser: (params) =>
    dispatch({ type: SignUpConstants.REGISTER_USER, params: params }),
  setPassword: (params) =>
    dispatch({ type: SignUpConstants.SET_PASSWORD, params: params }),
  setAgreeTerms: (params) =>
    dispatch({ type: SignUpConstants.AGREE_TERMS, params: params }),
  setConfirmPassword: (params) =>
    dispatch({ type: SignUpConstants.SET_CONFIRM_PASSWORD, params: params }),
  setName: (params) =>
    dispatch({ type: SignUpConstants.SET_NAME, params: params }),
  setEmail: (params) =>
    dispatch({ type: SignUpConstants.SET_EMAIL, params: params }),
  setCountry: (params) =>
    dispatch({ type: SignUpConstants.SET_COUNTRY, params: params }),
  setFilteredItems: (params) =>
    dispatch({ type: SignUpConstants.SET_FILTERED_ITEMS, params: params }),
  setErrorMessage: (params) =>
    dispatch({ type: SignUpConstants.SET_ERROR_MESSAGE, params: params }),
  setRegisterSuccess: (params) =>
    dispatch({ type: SignUpConstants.SET_REGISTER_SUCCESS, params: params }),
  setSignUpStyle: (params) =>
    dispatch({ type: SignUpConstants.SET_SIGNUP_STYLE, params: params }),
  clearAllField: (params) =>
    dispatch({ type: SignUpConstants.CLEAR, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);


