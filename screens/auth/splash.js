import React, { Component } from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';
import { StatusBar } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

var ScreenWidth = Dimensions.get('window').width;
var ScreenHeight = Dimensions.get('window').height;
export default class Splash extends Component {
  constructor(props) {
    ///navigation.na
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(true);
    const interval = setInterval(async () => {
      // const auth = await AsyncStorage.getItem('auth');
      // // await this.delay(2000);
      // if(auth == 'true') {
      //   this.props.navigation.navigate('Home');
      // }
      // else {
      //   this.props.navigation.navigate('Login');
      // }
      this.props.navigation.replace('Login');
      clearInterval(interval);
    }, 1500);
    // this.checkAlreadyLoggedIn();
  }

  async delay(ms) {
    // return await for better async stack trace support in case of errors.
    return await new Promise(resolve => setTimeout(resolve, ms));
  }

  async checkAlreadyLoggedIn() {
    const auth = await AsyncStorage.getItem('auth');
    await this.delay(2000);
    if (auth == 'true') {
      this.props.navigation.navigate('Home');
    } else {
      this.props.navigation.replace('Login');
    }
    return;
  }

  render() {
    return (
      <View style={styles.splashBody}>
        <Image source={require('../../assets/images/hool_logo.png')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  splashBody: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DBFF00'
  }
});
