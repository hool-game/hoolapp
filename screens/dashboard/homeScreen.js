import React from 'react';
import {
  View,
  Text,
  BackHandler,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {
  handleBackButton,
  HOOL_CLIENT
} from '../../shared/util';
import { SvgXml } from 'react-native-svg';
import { APPLABELS } from '../../constants/applabels';
import {
  ScaledSheet,
  moderateScale,
  verticalScale,
  scale
} from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import svgImages from '../../API/SvgFiles';
import DeviceInfo from 'react-native-device-info';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import ChatScreen from '../chat/chat';
import { API_ENV } from '../../redux/api/config/config';
import { SCREENS } from '../../constants/screens';
import { SignInConstants } from '../../redux/actions/SignInAction';
import { LogoutButton } from '../../shared/LogoutButton';

class Home extends React.Component {
  constructor(props) {
    ///navigation.na
    console.log('home screen contructor called');
    super(props);
    this.state = {
      isTablet: DeviceInfo.isTablet(),
      ios: DeviceInfo.getSystemName(),
      playerType: 'normal',
      switchButton: svgImages.switchSvgDisabled,
      playBots: svgImages.playBotSvgInactive,
      instantGame: svgImages.instantGameSvgDisabled,
      findPartner: svgImages.findPartnerSvgDisabled,
      createTable: svgImages.createTableSvg,
      findTable: svgImages.findTableSvg,
      ProColor: '#363636',
      isNetworkConnected: true,
      showNetworkConnectionMessage: false,
    };
    this.logout = this.logout.bind(this)
  }

  async componentDidMount() {
    console.log('home screen mount');
    // this.unsubscribe = NetInfo.addEventListener(state => {
    //   if (!state.isConnected) {
    //     showNoNetworkDialog();
    //   }
    // });
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }

  componentWillUnmount() {
    console.log('UnMount Home Screen')
    this.clearListeners();
  }

  clearListeners() {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
  }
  
  navigateToScreen(screenName, params) {
    this.clearListeners();
    this.props.navigation.navigate(screenName, params);
  }

  onclick = () => {
    //console.log('On click works')
    if (this.state.playerType === 'normal') {
      this.setState({ playBots: svgImages.playBotProSvg });
      this.setState({ switchButton: svgImages.switchProSvg });
      this.setState({ instantGame: svgImages.instantGameProSvg });
      this.setState({ findPartner: svgImages.findPartnerProSvg });
      this.setState({ createTable: svgImages.createTableProSvg });
      this.setState({ findTable: svgImages.finaTableProSvg });
      this.setState({ ProColor: '#FFFFFF' });
      this.setState({ playerType: 'pro' });
    } else if (this.state.playerType === 'pro') {
      this.setState({ playBots: svgImages.playBotSvg });
      this.setState({ switchButton: svgImages.switchSvg });
      this.setState({ instantGame: svgImages.instantGameSvg });
      this.setState({ findPartner: svgImages.findPartnerSvg });
      this.setState({ createTable: svgImages.createTableSvg });
      this.setState({ findTable: svgImages.findTableSvg });
      this.setState({ ProColor: '#363636' });
      this.setState({ playerType: 'normal' });
    }
  };

  findTable = () => {
    //console.log('On find table')
    let gamestate = {};
    var tableID = '';
    this.navigateToScreen('FindTable', {});
  };

  createTable = () => {
    //console.log('On create table')
    this.navigateToScreen('CreateTable', {});
  };

  shareInfo = () => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    var currDate = date + '' + month + '' + year + '' + hours + '' + min + ''+sec;
    var tableId = HOOL_CLIENT.jid._local + '_' + currDate + '@'+API_ENV.xmppTable;
    var Role = 'rover';

    this.props.navigation.navigate('MainDesk', {
      loadScreen: SCREENS.JOIN_TABLE,
      tableID: tableId,
      role: Role,
      isFromScreen: true,
      fromPlayBot: true,
    });
  };

  biddingScreen = () => {
    //console.log('On create table')
    var tableID = '';
    var gamestate = [];
    this.props.navigation.navigate('Bidding', {
      xmppconn: HOOL_CLIENT,
      tableID: tableID,
      gameState: gamestate,
    });
  };

  cardPlay = () => {
    //console.log('On create table')
    var tableID = '';
    let gamestate = {};
    gamestate.runningScoreNS = 0;
    gamestate.runningScoreEW = 30;
    gamestate.dealWinner = 'EW';
    gamestate.dealScoreNS = 0;
    gamestate.dealScoreEW = 30;
    this.props.navigation.navigate('CardPlay', {
      xmppconn: HOOL_CLIENT,
      tableID: tableID,
      gameState: gamestate,
    });
  };

  handleBackButton() {
    ToastAndroid.show('Back button is pressed', ToastAndroid.LONG);
    // return true;
  }
  showChatWindow(){

    this.props.changeShowChatWindow({
      showChatWindow: true
    })
    this.props.isTableUser({
      isTableUser: false
    })
  }
  
  async logout(){
    this.props.changeShowChatWindow({
      showChatWindow: false
    })
    await HOOL_CLIENT.xmppStop();
    this.props.setInitialScreen("Login")
    this.props.setSignInSuccessfull(false)
  }

  render() {
    
     return (
      <SafeAreaView edges={['top', 'left']} style={{ backgroundColor: '#0a0a0a', flex:1}}>
        <View style={{ backgroundColor: '#0a0a0a', height: '100%' }}>
          { 
            this.props.chatScreen ?
               <>
                 <View style={{
                   flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                   width: '100%', height: scale(45)
                 }}>
                    <View style={styles.menuPosition}>
                      <TouchableOpacity
                        onPress={() => {
                          let showMenu = this.props.showMenu
                          showMenu = !showMenu
                          this.props.showMenuScreen({
                            showMenu
                          })
                        }}
                      >
                        <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
                      </TouchableOpacity>
                    </View>
                 </View>
                 {
                     (!this.props.isFriendsListShown && !this.props.setClicked) &&
                     <View style={styles.backPosition}>
                         <TouchableOpacity onPress={() => {
                           this.props.changeShowChatWindow({
                             showChatWindow: false
                           })
                          }
                         }>
                         <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.back} />
                       </TouchableOpacity>
                     </View>
                  }

                 <View style={{
                   width: '87%',
                   height: '100%',
                   backgroundColor: 'green',
                   marginTop: moderateScale(-45),
                   marginLeft: moderateScale(45),
                 }}>
                   <ChatScreen />
                 </View>
               </>
               : 
               <>
                 <View
                   style={{
                     width: '100%',
                     height: scale(45),
                     flexDirection: 'row',
                     justifyContent: 'space-between',
                     alignItems: 'center',
                   }}
                 >
                   <TouchableOpacity
                    onPress={() => {
                      let showMenu = this.props.showMenu
                      showMenu = !showMenu
                      this.props.showMenuScreen({
                        showMenu
                      })
                    }}
                   >
                     <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
                   </TouchableOpacity>
                   <View
                     style={{
                       paddingRight: moderateScale(15),
                       flexDirection: 'row',
                       alignItems: 'center',
                     }}
                   >
                     <TouchableOpacity
                       disabled={this.state.switchButton == svgImages.switchSvgDisabled}
                       onPress={() => { this.onclick }}>
                       <SvgXml height={scale(11)} width={verticalScale(42.94)} xml={this.state.switchButton} />
                     </TouchableOpacity>
                   </View>
                 </View>
                 <View
                   style={{
                     height: '100%',
                     width: '100%',
                     position: 'absolute',
                     flexDirection: 'column',
                     alignItems: 'center',
                     justifyContent: 'center',
                   }}
                 >
                   <View
                     style={{
                       flexDirection: 'row',
                       borderWidth: moderateScale(0.5),
                       borderRightWidth: 0,
                     }}
                   >
                     <TouchableOpacity
                      //  disabled={this.state.playBots == svgImages.playBotSvgInactive}
                       onPress={this.shareInfo}>
                       <View
                         style={{
                           width: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(135) : scale(182),
                           height: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(120) : scale(167),
                           borderWidth: moderateScale(0.5),
                           borderColor: '#1B1B25',
                           justifyContent: 'center',
                           alignItems: 'center',

                         }}
                       >
                         <SvgXml height={scale(53.31)} width={verticalScale(47.24)} xml={svgImages.playBotSvg} />
                         {/* //this.state.playBots == svgImages.playBotSvgInactive ? styles.titleTextDisbaled :  */}
                         <Text style={styles.titleText}>
                           {APPLABELS.TEXT_PLAY_BOTS}
                         </Text>
                       </View>
                     </TouchableOpacity>
                     <TouchableOpacity
                       disabled={this.state.instantGame == svgImages.instantGameSvgDisabled}
                       onPress={this.biddingScreen}>
                       <View
                         style={{
                           width: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(135) : scale(182),
                           height: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(120) : scale(167),
                           borderWidth: moderateScale(0.5),
                           borderColor: '#1B1B25',
                           justifyContent: 'center',
                           alignItems: 'center',

                         }}
                       >
                         <SvgXml height={scale(52.15)} width={verticalScale(44)} xml={this.state.instantGame} />
                         <Text style={this.state.instantGame == svgImages.instantGameSvgDisabled ? styles.titleTextDisbaled : styles.titleText}>
                           {APPLABELS.TEXT_INSTANT_GAME}
                         </Text>
                       </View>
                     </TouchableOpacity>
                     <TouchableOpacity
                       disabled={this.state.findPartner == svgImages.findPartnerSvgDisabled}
                       onPress={this.cardPlay}>
                       <View
                         style={{
                           width: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(135) : scale(182),
                           height: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(120) : scale(167),
                           borderWidth: moderateScale(0.5),
                           borderColor: '#1B1B25',
                           alignItems: 'center',
                           justifyContent: 'center',

                         }}
                       >
                         <SvgXml height={scale(51.9)} width={verticalScale(51.9)} xml={this.state.findPartner} />
                         <Text style={this.state.findPartner == svgImages.findPartnerSvgDisabled ? styles.titleTextDisbaled : styles.titleText}>
                           {APPLABELS.TEXT_FIND_A_PARTNER}
                         </Text>
                       </View>
                     </TouchableOpacity>
                   </View>

                   <View
                     style={{
                       flexDirection: 'row',
                       borderWidth: moderateScale(0.5),
                       borderTopWidth: 0,
                       borderRightWidth: 0,
                     }}
                   >
                     <TouchableOpacity onPress={this.createTable}>
                       <View
                         style={{
                           width: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(202) : scale(273),
                           height: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(40) : scale(54),
                           borderWidth: moderateScale(0.5),
                           borderColor: '#1B1B25',
                           flexDirection: 'row',
                           alignItems: 'center',
                           justifyContent: 'center',
                         }}
                       >
                         <SvgXml height={scale(15)} width={verticalScale(15)} xml={this.state.createTable} />
                         <Text style={styles.iconText}>
                           {APPLABELS.TEXT_CREATE_TABLE}
                         </Text>
                       </View>
                     </TouchableOpacity>

                     <TouchableOpacity onPress={this.findTable}>
                       <View
                         style={{
                           width: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(202) : scale(273),
                           height: (this.state.ios == 'iOS' && this.state.isTablet) ? scale(40) : scale(54),
                           borderWidth: moderateScale(0.5),
                           borderColor: '#1B1B25',
                           flexDirection: 'row',
                           alignItems: 'center',
                           justifyContent: 'center',


                         }}
                       >
                         <SvgXml height={scale(13.31)} width={verticalScale(17.43)} xml={this.state.findTable} />
                         <Text style={styles.iconText}>
                           {APPLABELS.TEXT_FIND_A_TABLE}
                         </Text>
                       </View>
                     </TouchableOpacity>
                   </View>
                 </View>
               </>
          }
          {
            this.props.showMenu ?
            <>
              <LogoutButton logout={this.logout}/>
            </>
            :
            null
          }
           {
             !this.props.chatScreen &&
             <View style={{
              width: scale(45),
              height: scale(45),
              bottom: 0,
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
              opacity: 0.4
            }}>
             <View  
                // onPress={() => this.showChatWindow()}
                activeOpacity={0}
              >
               <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.chatSvg} />
             </View>
             </View>
           }
        </View>
      </SafeAreaView>
    );
  }
}
const styles = ScaledSheet.create({
  titleText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: 'white',
    paddingTop: '25@ms',
  },
  menuPosition: {
    position: 'absolute',
    left: '0@ms',
  },
  backPosition: {
    position: 'absolute',
    right: '0@ms',
    height:scale(45),
    width:scale(45),

  },
  titleTextDisbaled: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#353535',
    paddingTop: '25@ms',
  },
  proText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#363636',
    paddingLeft: '6@ms',
  },

  iconText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: 'white',
    paddingLeft: '8@ms',
  },
});


const mapStateToProps = (state) => ({
  chatScreen: state.chatActions.showChatWindow,
  isFriendsListShown: state.chatActions.showFriendsList,
  setClicked: state.chatActions.setClicked,
  showMenu: state.signInActions.showMenu
});

const mapDispatchToProps = (dispatch) => ({
  isTableUser: (params) =>
    dispatch({ type: ChatConstants.SET_IS_TABLE_USER_TYPE, params: params }),
  changeShowChatWindow: (params) =>
    dispatch({ type: ChatConstants.SHOW_CHAT_WINDOW, params: params }),
  showMenuScreen: (params) =>
   dispatch({ type: SignInConstants.SHOW_MENU, params: params}),
  setUserName: (params) =>
    dispatch({ type: SignInConstants.SET_USERNAME, params: params }),
  setSignInSuccessfull: (params) =>
    dispatch({ type: SignInConstants.SIGNIN_SUCCESSFULL, params: params }),
  setInitialScreen: (params) =>
    dispatch({ type: SignInConstants.SET_INITIAL_SCREEN, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
