import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { HOOL_CLIENT } from '../../shared/util';
import { SCREENS } from '../../constants/screens';
import { SvgXml } from 'react-native-svg';
import _ from 'lodash';
import svgImages from '../../API/SvgFiles';
import { APPLABELS } from '../../constants/applabels';
import { scale, ScaledSheet, verticalScale, moderateScale, moderateVerticalScale } from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import DeviceInfo from 'react-native-device-info';
import { SafeAreaView } from 'react-native-safe-area-context';
import ChatScreen from '../chat/chat';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import { Strings } from '../../styles/Strings';
import { API_ENV } from '../../redux/api/config/config';
import { SignInConstants } from '../../redux/actions/SignInAction';
import { LogoutButton } from '../../shared/LogoutButton';

let gamestate = {};
gamestate.hands = new Map();
gamestate.info = new Map();

class CreateTable extends React.Component {
  constructor(props) {
    ///navigation.na
    super(props);
    this.state = {
      isTablet: DeviceInfo.isTablet(),
      ios: DeviceInfo.getSystemName(),
      output: '',
      tableService: '',
      message: '',
    };
    this.logout = this.logout.bind(this)
  }

  goBack() {
    console.log('Findtable componentDidMount called');
    if(this.props.chatScreen){
      this.props.changeShowChatWindow({
        showChatWindow: false
      })
    }else{
    this.props.navigation.goBack();
    }
  }

  componentDidMount() {
    this.getService();
  }

  async getService() {
    const tableService = await HOOL_CLIENT.xmppDiscoverServices();
    this.setState({ tableService: tableService });
  }

  joinTable() {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    //console.log(date + '/' + month + '/' + year + '-' + hours + ':' + min);
    var currDate = date + '' + month + '' + year + '' + hours + '' + min + ''+sec;
    var tableId = HOOL_CLIENT.jid._local + '_' + currDate + '@'+API_ENV.xmppTable;
    var Role = 'rover';
    console.log(`Joining table ${tableId} as ${Role}`);
    this.props.navigation.navigate('MainDesk', {
      loadScreen: SCREENS.JOIN_TABLE,
      tableID: tableId,
      role: Role,
    });
  }

  showChatWindow(){
    this.props.changeShowChatWindow({
      showChatWindow: true
    })
    this.props.isTableUser({
      isTableUser: false
    })
  }

  async logout(){
    this.props.changeShowChatWindow({
      showChatWindow: false
    })
    await HOOL_CLIENT.xmppStop();
    this.props.setInitialScreen("Login")
    this.props.setSignInSuccessfull(false) 
  }
  
  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView edges={['top', 'left']} style={{ backgroundColor: '#0a0a0a', flex:1}}>
        <View style={styles.totalBodyBg}>
         
          {
            !this.props.chatScreen &&
            <TouchableOpacity style={styles.chatPosition} 
            // onPress={() => 
            //   this.showChatWindow()
            // }
            >
              <SvgXml height={verticalScale(48)} width={scale(45)} xml={svgImages.chatSvg} />
            </TouchableOpacity>
          }
          {
            this.props.chatScreen ?
              <>
                <View style={{
                  flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                  width: '100%', height: scale(45)
                }}>
                  <View style={styles.menuPosition}>
                    <TouchableOpacity
                      onPress={() => {
                        let showMenu = this.props.showMenu
                        showMenu = !showMenu
                        this.props.showMenuScreen({
                          showMenu
                        })
                      }}
                    >
                      <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
                    </TouchableOpacity>
                  </View>
                  {
                    !this.props.isFriendsListShown &&
                    <View style={styles.backPosition}>
                      <TouchableOpacity onPress={() => this.goBack()}>
                        <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.back} />
                      </TouchableOpacity>
                    </View>
                  }
                </View>

                <View style={{
                  width: '87%',
                  height: '100%',
                  marginTop: moderateScale(-45),
                  marginLeft: moderateScale(45),
                }}>
                  <ChatScreen />
                </View>
              </>
              :
              <>
                <View style={{
                  flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                  width: '100%', height: scale(45)
                }}>
                  <View style={styles.menuPosition}>
                    <TouchableOpacity
                      onPress={() => {
                        let showMenu = this.props.showMenu
                        showMenu = !showMenu
                        this.props.showMenuScreen({
                          showMenu
                        })
                      }}
                    >
                      <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.createTitleText}>
                    {APPLABELS.TEXT_CREATE_A_TABLE}
                  </Text>
                  <View style={styles.backPosition}>
                    <TouchableOpacity onPress={() => this.goBack()}>
                      <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.back} />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.createBody}>
                  <View style={styles.createBodyArea}>
                    <View style={{
                      marginEnd: moderateScale(14)
                    }}>
                      <View style={{ ...styles.createClmPosition, flexDirection: 'row' }}>
                        <View style={styles.createIconBox}>
                          <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.timerSvgDisabled} />
                        </View>
                        <View style={styles.createTextBoxTotal}>
                          <Text style={styles.subTitleText}>
                            {APPLABELS.TEXT_TIMER}
                          </Text>
                          <View style={{ ...styles.createAroAreaBoxTotal }}>
                            <SvgXml xml={svgImages.leftArrowDisbaled} height={scale(8)} width={scale(8)} />
                            <Text style={{ ...styles.aroTitleText, letterSpacing: 0.4, color: '#353535' }}>None</Text>
                            <SvgXml xml={svgImages.rightArrowdisbaled} height={verticalScale(8)} width={scale(8)} />
                          </View>
                        </View>
                      </View>
                      <View style={{ ...styles.createClmPosition, flexDirection: 'row', marginTop: moderateScale(14) }}>
                        <View style={{ ...styles.createIconBox }}>
                          <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.historySvgDisabled} />
                        </View>
                        <View style={styles.createTextBoxTotal}>
                          <Text style={{ ...styles.subTitleText }}>
                            {APPLABELS.TEXT_HISTORY}
                          </Text>
                          <View style={{ ...styles.createAroAreaBoxTotal }}>
                            <SvgXml xml={svgImages.leftArrowDisbaled} height={verticalScale(8)} width={scale(8)} />
                            <Text style={{ ...styles.aroTitleText, color: '#353535' }}>On</Text>
                            <SvgXml xml={svgImages.rightArrowdisbaled} height={verticalScale(8)} width={scale(8)} />
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={{ marginEnd: 0 }}>
                      <View style={{ ...styles.createClmPosition, flexDirection: 'row' }}>
                        <View style={{ ...styles.createIconBox }}>
                          <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.kibitzerSvgDisabled} />
                        </View>
                        <View style={styles.createTextBoxTotal}>
                          <Text style={{ ...styles.subTitleText, letterSpacing: 0.3 }}>Kibitzer</Text>
                          <View style={{ ...styles.createAroAreaBoxTotal, }}>
                            <SvgXml xml={svgImages.leftArrowDisbaled} height={verticalScale(8)} width={scale(8)} />
                            <Text style={{ ...styles.aroinactiveTitleText, width: verticalScale(75) }}>
                              {APPLABELS.TEXT_RESTRICTED}
                            </Text>
                            <SvgXml xml={svgImages.rightArrowdisbaled} height={verticalScale(8)} width={scale(8)} />
                          </View>
                        </View>
                      </View>
                      <View style={{ ...styles.createClmPosition, flexDirection: 'row', marginTop: moderateScale(14) }}>
                        <View style={{ ...styles.createIconBox, }}>
                          <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.playersSvgDisbaled} />
                        </View>
                        <View style={styles.createTextBoxTotal}>
                          <Text style={{ ...styles.subTitleText, letterSpacing: 0.4 }}>Players</Text>
                          <View style={{ ...styles.createAroAreaBoxTotal, }}>
                            <SvgXml xml={svgImages.leftArrowDisbaled} height={verticalScale(8)} width={scale(8)} />
                            <Text style={{ ...styles.aroTitleText, color: '#353535' }}>{Strings.open}</Text>
                            <SvgXml xml={svgImages.rightArrowdisbaled} height={verticalScale(8)} width={scale(8)} />
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.createStartButton}>
                    <TouchableOpacity onPress={() => this.joinTable()}>
                      <Text style={styles.createStartButtonText}>START</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </>
          }
           {
            this.props.showMenu ?
            <>
              <LogoutButton logout={this.logout}/>
            </>
            :
            null
          }
        </View>
      </SafeAreaView>
    );
  }
}
const styles = ScaledSheet.create({
  totalBodyBg: {
    backgroundColor: '#0a0a0a',
    height: '100%',
  },
  totalBodyBackground: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  menuPosition: {
    position: 'absolute',
    left: '0@ms',
  },
  backPosition: {
    position: 'absolute',
    right: '0@ms',
  },
  createTitleText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    color: '#DBFF00',
    position: 'absolute',
    textAlign: 'center',
    letterSpacing: 0.7,
    alignSelf: 'center'
  },
  chatPosition: {
    position: 'absolute',
    bottom: '0@ms',
    width: '45@s',
    height: '45@s',
    flexDirection: 'row',
    alignItems: 'center',
    opacity: 0.4
  },
  /***************************** */
  subTitleText: {
    fontFamily: 'Roboto-Regular',
    fontSize: normalize(14),
    width: '70@vs',
    color: 'white',
    textAlign: 'center',
    opacity: 0.3
  },
  aroTitleText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    width: '53@vs',
    fontWeight: '300',
    color: '#DBFF00',
    textAlign: 'center',
  },
  aroinactiveTitleText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '300',
    color: '#45454D',
    letterSpacing: 0.4,
    textAlign: 'center',
  },

  createBody: {
    width: '100%',
    alignItems: 'center',
  },
  createBodyArea: {
    marginTop: '41@s',
    flexDirection: 'row',
    marginBottom: '65@s',
  },
  createClmPosition: {
    flexDirection: 'column',
    borderWidth: 0.5,
    borderColor: '#1B1B25',
  },
  createIconBox: {
    height: '64@s',
    width: '64@s',
    justifyContent: 'center',
    alignItems: 'center'
  },
  createTextBoxTotal: {
    width: '200@vs',
    height: '64@s',
    borderLeftWidth: 0.5,
    borderLeftColor: '#1B1B25',
    alignItems: 'center',
    justifyContent: 'center',
  },
  createAroAreaBoxTotal: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '9@ms'
  },
  createStartButton: {
    width: '169@vs',
    height: '37@s',
    borderWidth: 1,
    borderColor: '#DBFF00',
  },
  createStartButtonText: {
    width: '169@vs',
    height: '37@s',
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#FFFFFF',
    letterSpacing: 0.4,
    textAlign: 'center',
    textAlignVertical: 'center',
    ...Platform.select({
      ios: {
          lineHeight: '37@s'
      },
      android: {}
    }),  
  },
});

const mapStateToProps = (state) => ({
  chatScreen: state.chatActions.showChatWindow,
  isFriendsListShown: state.chatActions.showFriendsList,
  showMenu: state.signInActions.showMenu
});

const mapDispatchToProps = (dispatch) => ({
  isTableUser: (params) =>
    dispatch({ type: ChatConstants.SET_IS_TABLE_USER_TYPE, params: params }),
  changeShowChatWindow: (params) =>
    dispatch({ type: ChatConstants.SHOW_CHAT_WINDOW, params: params }),
  showMenuScreen: (params) =>
   dispatch({ type: SignInConstants.SHOW_MENU, params: params}),
  setUserName: (params) =>
    dispatch({ type: SignInConstants.SET_USERNAME, params: params }),
  setSignInSuccessfull: (params) => 
    dispatch({ type: SignInConstants.SIGNIN_SUCCESSFULL, params: params }),
  setInitialScreen: (params) =>
    dispatch({ type: SignInConstants.SET_INITIAL_SCREEN, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateTable);
