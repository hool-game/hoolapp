import React from 'react';
import {
  Text,
  TextInput,
  View,
  ScrollView,
  LogBox,
  TouchableOpacity,
  TouchableHighlight,
  SafeAreaView,
  Platform,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import { connect } from 'react-redux';
import { ChatConstants } from '../../redux/actions/ChatActions';
import ChatScreen from '../chat/chat';
import { HOOL_CLIENT } from '../../shared/util';
import { SCREENS } from '../../constants/screens';
import { Table, Row, TableWrapper, Cell } from 'react-native-table-component';
const { jid } = require('@xmpp/client');
import { SvgXml } from 'react-native-svg';
import svgImages from '../../API/SvgFiles';
import { APPLABELS } from '../../constants/applabels';
const profileIcon = svgImages.profile1
import {
  ScaledSheet,
  scale,
  verticalScale,
  moderateScale
} from 'react-native-size-matters/extend';
import { normalize } from '../../styles/global';
import DeviceInfo from 'react-native-device-info';
import Toast from 'react-native-toast-message';
import { SignInConstants } from '../../redux/actions/SignInAction';
import { LogoutButton } from '../../shared/LogoutButton';


class FindTable extends React.Component {
  constructor(props) {
    super(props);
    const headerIcon = (value, func, width, arrowIcon) => (
      <TouchableOpacity onPress={() => this.headerFunction(func)}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row',
           height:scale(width), width:scale(45),}}>
          <SvgXml xml={value} height={scale(width)} width={scale(30)}/>
          {
            arrowIcon &&
            <SvgXml height={scale(6)} width={scale(6)} xml={svgImages.dropArrow} style={{left: -8}}/>
          }
        </View>
      </TouchableOpacity>
    );
    const hostDisplay = (value, func, width, arrowIcon) => (
      <TouchableOpacity onPress={() => this.headerFunction(func)}>

        <View style={{ flexDirection: 'row',width:scale(width),paddingLeft:scale(8)}}>
          <Text style={{...styles.text}}>{value}</Text>
          {
            arrowIcon &&
            <SvgXml height={scale(6)} width={scale(6)} xml={svgImages.dropArrow} style={{left: 5, alignSelf: 'center'}}/>
          }
        </View>
      </TouchableOpacity>
    );
    this.state = {
      spinner: true,
      isNotch: DeviceInfo.hasNotch(),
      isTablet: DeviceInfo.isTablet(),
      tableHead: [
        hostDisplay('Host', '', (DeviceInfo.isTablet()&& Platform.OS =='ios')? 145: 205, true),
        headerIcon(svgImages.players, '', 30, true),
        headerIcon(svgImages.kibitzer, '', 30, true),
        headerIcon(svgImages.timer,'', 30, true),
        headerIcon(svgImages.cheat_Sheet,'', 30, true),
        headerIcon(svgImages.history,'', 30, true),
      ],
      widthArr: [
        scale((DeviceInfo.isTablet()&& Platform.OS =='ios')? 145: 205),
        scale(45),
        scale(45),
        scale(45),
        scale(45),
        scale(45),
      ],
      heightArr: [
        verticalScale(12),
        verticalScale(45),
        verticalScale(45),
        verticalScale(45),
        verticalScale(45),
        verticalScale(45),
      ],
      columnData: [],
      tableData: [],
      tablst: [],
      bkptablelst: [],
      searchText: '',
      refreshIcon: headerIcon(svgImages.refresh, 'refresh', 14, false)
    };
    this.logout = this.logout.bind(this)
    this.gettables();
    LogBox.ignoreAllLogs();
  }

  headerFunction(func) {
    if (func === 'refresh') {
      this.gettables();
    }
  }

  async gettables() {
    try{
      var tablelst = await HOOL_CLIENT.getTableList();
      console.log("TABEL LIST AFTER REFRESH : ",tablelst," yes  ", tablelst.reverse())
      this.setState(
        {
          spinner: false,
          tablst: tablelst,
          bkptablelst: [...tablelst],
        },
        () => {
          this.updateTableData();
        }
      );
    }catch(e){
      this.setState(
        {
          spinner: false,
          tablst: [],
          bkptablelst: [],
        },
        () => {
          this.updateTableData();
        }
      );
      Toast.show({
        type: 'error',
        text1: 'Please Try Again',
        position: 'bottom',
        bottomOffset: 50,
      });
      this.goBack()
    }
  }

  gotoTable(role, host) {
    this.props.navigation.navigate('MainDesk', {
      loadScreen: SCREENS.JOIN_TABLE,
      tableID: host,
      role: role,
    });
  }

  goBack() {
    if(this.props.chatScreen){
      this.props.changeShowChatWindow({
        showChatWindow: false
      })
    }else{
    this.props.navigation.goBack();
    }
  }

  _alertIndex(index) {
    this.gotoTable('rover', this.state.tablst[index].jid);
  }

  componentDidMount() {
  }

  searchTableList = props => {
    let tablst = this.state.bkptablelst.filter(
      t =>
        t.host !== undefined &&
        jid(t.host)
          ._local.toLowerCase()
          .includes(this.state.searchText.toLowerCase())
    );
    this.setState({ tablst }, () => {
      this.updateTableData();
    });
  };


  userProfile = (user,index) => {
    // Handle userProfile Page
    console.log("User JID and index ",user,index)
  }

  updateTableData() {
    const tablelst = this.state.tablst;
    const elementButton = (value, host,index) => (
      <TouchableOpacity onPress={() => this.userProfile(host,index)}>
      <View style={{ ...styles.avterPosition}}>
          <SvgXml width={scale(31)} height={verticalScale(31)} xml={value} />
      </View>
      </TouchableOpacity>
    );

    const bindClick = (host) => (
        <Text style={{ ...styles.textRow,paddingLeft:scale(8)}}>{host}</Text>
    );

    const textWithIcon = (playerCount, idx) => (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <SvgXml width={scale(10)} height={scale(10)} xml={svgImages.timerActive} />
        <Text style={{ ...styles.textSubRow, marginLeft: moderateScale(5) }}>{playerCount + idx}</Text>
      </View>
    );

    const text = (playerCount, count, enabled) => (
        <Text style={enabled? { ...styles.textSubRow }: { ...styles.textSubRow, color:'#45454c'}}>{playerCount + count}</Text>
    );
    const tableData = []; 
    const columnData = [];    
    tablelst.map((r, idx) => {
        const rowData = [];
        var valkibitzerCount = '';
        valkibitzerCount = r.kibitzerCount;
        if (Number(valkibitzerCount) < 10) {
          valkibitzerCount = '0' + valkibitzerCount;
        }

        columnData.push(elementButton(profileIcon, r.jid,idx));
        rowData.push(bindClick(jid(r.host)._local));
        rowData.push(textWithIcon(r.playerCount, '/4'));
        rowData.push(text(valkibitzerCount, '', true));
        rowData.push(text('None', '', false));
        rowData.push(text('On', '', true));
        rowData.push(text('On', '', true));
        tableData.push(rowData);
    });
    this.setState({ tableData ,columnData});
  }

  showChatWindow(){
    this.props.changeShowChatWindow({
      showChatWindow: true
    })
    this.props.isTableUser({
      isTableUser: false
    })
  }

  async logout(){
    this.props.changeShowChatWindow({
      chatScreen: false
    })
    await HOOL_CLIENT.xmppStop();
    this.props.setInitialScreen("Login")
    this.props.setSignInSuccessfull(false)
  }

  render() {
    const state = this.state;

    return (
      <SafeAreaView style={{ backgroundColor: '#0a0a0a', height: '100%' }}>
        <View style={styles.container}>
        {
            this.props.chatScreen ?
              <>
                <View style={{
                  flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                  width: '100%', height: scale(45)
                }}>
                  <View style={styles.menuPosition}>
                  <TouchableOpacity
                          onPress={() => {
                            let showMenu = this.props.showMenu
                            showMenu = !showMenu
                            this.props.showMenuScreen({
                              showMenu
                            })
                          }}
                  >
                     <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
                  </TouchableOpacity>
                  </View>
                  {
                    !this.props.isFriendsListShown &&
                    <View style={styles.backPosition}>
                      <TouchableOpacity onPress={() => this.goBack()}>
                        <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.back} />
                      </TouchableOpacity>
                    </View>
                  }
                </View>

                <View style={{
                  width: '87%',
                  height: '100%',
                  marginTop: moderateScale(-45),
                  marginLeft: moderateScale(45),
                }}>
                  <ChatScreen />
                </View>
              </>
              :
              <>
          <Spinner
            visible={this.state.spinner}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
          />
          <View style={{
            flexDirection: 'row', height: scale(45),
            width: '100%', justifyContent: 'center', alignItems: 'center'
          }}>
            <View style={styles.menuPosition}>
              <TouchableOpacity
                          onPress={() => {
                            let showMenu = this.props.showMenu
                            showMenu = !showMenu
                            this.props.showMenuScreen({
                              showMenu
                            })
                          }}
              >
                  <SvgXml height={scale(45)} width={scale(45)} xml={this.props.showMenu ? svgImages.iconCancel : svgImages.menuSvg} />
              </TouchableOpacity>
            </View>
            <Text style={styles.createTitleText}>
              {APPLABELS.TEXT_FIND_A_TABLE_HEADING}
            </Text>
            <View style={styles.backPosition}>
              <TouchableOpacity onPress={() => this.goBack()}>
                <SvgXml height={scale(45)} width={scale(45)} xml={svgImages.back} />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity  style={{...styles.chatButtonPosition, opacity: 0.4}}> 
          {/* onPress={() => this.showChatWindow()}  */}
            <SvgXml height={verticalScale(45)} width={scale(45)} xml={svgImages.chatSvg} />
          </TouchableOpacity>
          <View
            style={{...
              styles.containerTableBody,
            }}
          >
            <View style={this.state.isNotch? {...styles.searchArea, width: verticalScale(500)} :styles.searchArea}>
              <View style={this.state.isNotch? {...styles.searchBerTextInputArea, width: verticalScale(465)} :styles.searchBerTextInputArea}>
                <TextInput
                  style={this.state.isNotch? {...styles.searchTxtIpt, width: verticalScale(465)} :styles.searchTxtIpt}
                  placeholder="Search by player name"
                  placeholderTextColor="#45454D"
                  returnKeyType="search"
                  onSubmitEditing={() => this.searchTableList()}
                  onChangeText={value => {
                    this.setState({ searchText: value });
                  }}
                />
              </View>

              <TouchableHighlight
                style={styles.searchButtonArea}
                onPress={() => this.searchTableList()}
              >
                <SvgXml
                  width={scale(13.31)}
                  height={verticalScale(17.43)}
                  xml={svgImages.search}
                />
              </TouchableHighlight>
            </View>
            <ScrollView horizontal={true}>
              <View
                style={this.state.isNotch? {...styles.containerTable, width: verticalScale(500)} : styles.containerTable}
              >
                <Table>
                  <TableWrapper style={{flexDirection:'row',justifyContent:'space-evenly',alignSelf:'center'}} >
                  <Cell
                    data={this.state.refreshIcon}
                    height={verticalScale(45)}
                    width={scale(45)}
                    style={{paddingBottom:verticalScale(2)}}
                  />
                  <Row
                    data={state.tableHead}
                    style={this.state.isNotch? {...styles.header, width: verticalScale(455)} : styles.header}
                    widthArr={state.widthArr}
                    heightArr={state.heightArr}
                    textStyle={styles.text}
                  />
                  </TableWrapper>
                </Table>
                <ScrollView contentContainerStyle={{paddingBottom: moderateScale(60) }}>
                  <Table>
                    {this.state.tableData.map((rowData, index) => (
                      <TableWrapper style={{flexDirection:'row',justifyContent:'space-evenly'}} key={index}>
                      <Cell
                      data={this.state.columnData[index]}
                      height={verticalScale(45)}
                      width={scale(45)}
                      />
                      <TouchableOpacity onPress={() => this._alertIndex(index)}
                      key={index}
                      >
                        <Row
                          data={rowData}
                          widthArr={state.widthArr}
                          heightArr={state.heightArr}
                          style={this.state.isNotch ? { ...styles.headerelement, width: verticalScale(455) } : styles.headerelement}
                          textStyle={styles.textRow}
                        />
                      </TouchableOpacity> 
                      </TableWrapper>  
                    ))}
                  </Table>
                </ScrollView>
              </View>
            </ScrollView>
          </View>
          </> }
          {
            this.props.showMenu ?
            <>
              <LogoutButton logout={this.logout}/>
            </>
            :
            null
          }
        </View>
        
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    height: '100%',
    width: '100%',
    backgroundColor: '#0a0a0a',
  },
  menuPosition: {
    position: 'absolute',
    left: '0@ms',
  },
  backPosition: {
    position: 'absolute',
    right: '0@ms',
  },
  createTitleText: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(14),
    fontWeight: '400',
    color: '#DBFF00',
    alignSelf: 'center',
    textAlign: 'center',
  },

  avterPosition: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  chatButtonPosition: {
    position: 'absolute',
    bottom: '0@ms',
    height: '45@s',
    width: '45@s'
  },
  containerTableBody: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  searchButtonArea: {
    width: '35@vs',
    height: '35@s',
    borderLeftWidth: 1,
    borderColor: '#1B1B25',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBerTextInputArea: {
    width: '515@vs',
    height: '35@s',
  },
  searchTxtIpt: {
    width: '515@vs',
    height: '35@s',
    fontFamily: 'Roboto-Light',
    color: '#fff',
    fontSize: normalize(14),
    textAlign: 'left',
    paddingVertical: 0,
    paddingLeft: '11@ms'
  },
  searchArea: {
    height: '35@s',
    width: '550@vs',
    borderWidth: '1@ms',
    borderColor: '#1B1B25',
    flexDirection: 'row',
  },
  containerTable: {
    width: '550@vs',
    height: '100%',
    borderWidth: 1,
    borderColor: '#1B1B25',
    marginTop: '5@ms',
  },
  header: {
    width: '505@vs',
    height: '40@s',
    borderWidth: 0,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  headerelement: {
    width: '505@vs',
    height: '40@vs',
    borderWidth: 0,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontFamily: 'Roboto-Light',
    textAlign: 'left',
    fontSize: normalize(13),
    color: '#DBFF00',
  },
  textRow: {
    fontFamily: 'Roboto-Light',
    color: 'white',
    fontSize: normalize(13),
    textAlign: 'left',
  },
  textSubRow: {
    fontFamily: 'Roboto-Light',
    color: 'white',
    fontSize: normalize(13),
    textAlignVertical: 'center',
    alignSelf: 'center',
    borderWidth: 0,
  },
  spinnerTextStyle: {
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '400',
    color: '#FFF'
  },
});

const mapStateToProps = (state) => ({
  chatScreen: state.chatActions.showChatWindow,
  isFriendsListShown: state.chatActions.showFriendsList,
  showMenu: state.signInActions.showMenu
});

const mapDispatchToProps = (dispatch) => ({
  isTableUser: (params) =>
    dispatch({ type: ChatConstants.SET_IS_TABLE_USER_TYPE, params: params }),
  changeShowChatWindow: (params) =>
    dispatch({ type: ChatConstants.SHOW_CHAT_WINDOW, params: params }),
  showMenuScreen: (params) =>
   dispatch({ type: SignInConstants.SHOW_MENU, params: params}),
  setUserName: (params) =>
    dispatch({ type: SignInConstants.SET_USERNAME, params: params }),
  setSignInSuccessfull: (params) =>
    dispatch({ type: SignInConstants.SIGNIN_SUCCESSFULL, params: params }),
  setInitialScreen: (params) =>
    dispatch({ type: SignInConstants.SET_INITIAL_SCREEN, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(FindTable);
