import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TextInput,
  BackHandler
} from 'react-native';
import { handleBackButton } from '../../shared/util';
import _ from 'lodash';
import { APPLABELS } from '../../constants/applabels';
const { jid } = require('@xmpp/client');

let gamestate = {};
gamestate.hands = new Map();
gamestate.info = new Map();
var userInfo = [];
export default class Home extends React.Component {
  constructor(props) {
    ///navigation.na
    super(props);
    this.state = {
      output: '',
      username: '',
      password: '',
      message: ''
    };
    this.getService();
  }

  async getService() {
    const { navigation } = this.props;
    const hool = navigation.getParam('xmppconn');
    //console.log(hool.xmpp.status);
    const tableService = await hool.xmppDiscoverServices();

    //console.log('userName : ' + tableService);
    this.setState({ username: tableService });
  }

  async onStartConnect() {
    const { navigation } = this.props;
    const hool = navigation.getParam('xmppconn');
    let tableID = this.state.password + '@' + this.state.username;
    let role = 'any';
    console.log(`Joining table ${tableID} as ${role}`);
    var gamestate = [];
    // hool.joinTable(tableID, role)
    this.props.navigation.navigate('JoinTable', {
      xmppconn: hool,
      tableID: tableID,
      role: role
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }

  componentWillUnmount() {
    this.clearListeners();
  }

  clearListeners() {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
  }

  async onSendMessage() {
    const { navigation } = this.props;
    const hool = navigation.getParam('xmppconn');
    var tablelst = await hool.getTableList();
    this.props.navigation.navigate('FindTable', {
      xmppconn: hool,
      tablst: tablelst
    });
  }

  render() {
    var buttons = (
      <View style={styles.container}>
        {/* <SvgUri width="45" height="200" source={require('../../assets/Players.svg')} /> */}
        <TouchableHighlight
          onPress={this.onStartConnect.bind(this)}
          style={styles.button}
        >
          <Text style={styles.buttonText}>{APPLABELS.TEXT_CREATE_JOIN_TABLE}</Text>
        </TouchableHighlight>
        {/* <TouchableHighlight
            onPress={this.onSendMessage.bind(this)}
            style={styles.button}>
            <Text style={styles.buttonText}>
                Find Table
            </Text>
          </TouchableHighlight> */}
      </View>
    );

    return (
      <View style={styles.container}>
        <TextInput
          placeholder={APPLABELS.PLACEHOLDER_TABLE_SERVICE}
          onChangeText={value => this.setState({ username: value })}
          value={this.state.username}
        />

        <TextInput
          placeholder={APPLABELS.PLACEHOLDER_TABLE_NAME}
          onChangeText={value => this.setState({ password: value })}
          value={this.state.password}
        />

        {/* <TextInput
        placeholder="Message"
        onChangeText={(value) => this.setState({message: value})}
        value={this.state.message}
    />      */}
        {buttons}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    height: 50,
    backgroundColor: '#48BBEC',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 22,
    color: '#FFF',
    alignSelf: 'center'
  },
  output_result: {
    color: '#000',
    marginTop: 20
  }
});
