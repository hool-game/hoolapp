export const SCREENS = {
  JOIN_TABLE: 'JoinTable',
  PLAY_SCREEN: 'PlayScreen',
  SHARE_INFO: 'ShareInfo',
  BID_INFO: 'BidInfo',
  HISTORY: 'History',
  SCOREBOARD: 'Scoreboard',
  USERPROFILE: 'UserProfile',
};
