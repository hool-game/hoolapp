export const APPLABELS = {
  LOGIN: 'Login',
  SIGNUP: 'Sign up',
  FORGOTPASSWORD: 'Forgot password',
  PLACEHOLDER_USERNAME: 'Username',
  PLACEHOLDER_PASSWORD: 'Password',
  PLACEHOLDER_CONFIRM_PASSWORD: 'Confirm Password',
  PLACEHOLDER_EMAIL: 'Email',
  PLACEHOLDER_RESET_EMAIL: 'Reset Email',
  PLACEHOLDER_NAME: 'Name (optional)',
  PLACEHOLDER_COUNTRY: 'Country (optional)',
  PLACEHOLDER_NEW_PASSWORD: 'New Password',
  PLACEHOLDER_TABLE_SERVICE: 'Table Service',
  PLACEHOLDER_TABLE_NAME: 'Table Name',
  TEXT_PLAY_HISTORY: 'Play History',
  TEXT_HANSOLOMAN_PARTNER: 'Han-SoloName - Partner',
  TEXT_HANSOLOMAN_LHO: 'Han-SoloName - LHO',
  TEXT_HANSOLO_RHO: 'Han Solo - RHO',
  TEXT_CREATE_A_TABLE: 'CREATE  A TABLE',
  TEXT_TIMER: 'Timer', 
  TEXT_HISTORY: 'History',
  TEXT_CHEAT_SHEET: 'Cheat Sheet',
  TEXT_TABLE_LISTING: 'Table Listing',
  TEXT_WELCOMESCREEN: 'WelcomeScreen',
  TEXT_DASHBOARDSCREEN: 'DashboardScreen',  
  TEXT_Go_To_DASHBOARDSCREEN: 'Go to DashboardScreen', 
  TEXT_FIND_A_TABLE: 'Find a table',
  TEXT_FIND_A_TABLE_HEADING: 'FIND A TABLE',
  TEXT_PLAY_BOTS: 'Play bots', 
  TEXT_INSTANT_GAME: 'Instant game',
  TEXT_FIND_A_PARTNER: 'Find a partner',
  TEXT_NEXT_DEAL: 'NEXT DEAL?',
  TEXT_RESTRICTED: 'Restricted',
  TEXT_CREATE_JOIN_TABLE: 'Create/Join Table',
  TEXT_CREATE_TABLE: 'Create a table',
};
