import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { connect } from 'react-redux';

import Splash from '../screens/auth/splash';
import Login from '../screens/auth/loginRegister';
import Signup from '../screens/auth/signUp';
import ForgotPassword from '../screens/auth/forgotPassword';
import Home from '../screens/dashboard/homeScreen';
import CreateTable from '../screens/dashboard/createTable';
import FindTable from '../screens/dashboard/findTable';
import MainDesk from '../screens/play/mainDesk';
import Chat from '../screens/chat/chat';

const Stack = createStackNavigator()

const HomeStack = ({ signInSuccessfull, initialScreen }) => {
  const navigatorOptions = {
    headerShown: false,
    cardStyle: { backgroundColor: 'transparent' },
    cardStyleInterpolator: ({ current: { progress } }) => ({
      cardStyle: {
        opacity: progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
        }),
      },
      overlayStyle: {
        opacity: progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 0.5],
          extrapolate: 'clamp',
        }),
      },
    }),
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={navigatorOptions}>
        {
          signInSuccessfull ?
          ( <Stack.Group initialRouteName="Home">
              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="CreateTable" component={CreateTable} />
              <Stack.Screen name="FindTable" component={FindTable} />
              <Stack.Screen name="MainDesk" component={MainDesk} />
              <Stack.Screen name="Chat" component={Chat} />
            </Stack.Group>
          ) :
            (<Stack.Group initialRouteName={initialScreen}>
              { initialScreen === "Splash" ? <Stack.Screen name="Splash" component={Splash} /> : null }
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Signup" component={Signup} />
              <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
            </Stack.Group>
          )
        }  
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const mapStateToProps = (state) => ({
  signInSuccessfull: state.signInActions.signInSuccessfull,
  initialScreen: state.signInActions.initialScreen
}
)


export default connect(mapStateToProps)(HomeStack);
