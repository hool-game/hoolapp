import spadeSvg from '../assets/Svg/spade.svg';
import heartsSvg from '../assets/Svg/hearts.svg';
import diamondSvg from '../assets/Svg/diamond.svg';
import clubsSvg from '../assets/Svg/clubs.svg';
import playersSvg from '../assets/Svg/Players.svg';
import playersSvgDisbaled from '../assets/Svg/Players_disbaled.svg';
import refreshSvg from '../assets/Svg/refresh.svg';
import refreshSvgDisabled from '../assets/Svg/refresh_disabled.svg';
import kibitzerSvg from '../assets/Svg/Kibitzer.svg';
import kibitzerSvgDisabled from '../assets/Svg/Kibitzer_disabled.svg';
import kibitzerProfile from '../assets/Svg/kibitzer_profile.svg';
import cheat_SheetSvg from '../assets/Svg/Cheat-Sheet.svg';
import cheat_Sheet_diabled from '../assets/Svg/icon_cheatSheet.svg';
import cheat_Sheet_white from '../assets/Svg/Cheat-Sheet-white.svg';
import historySvg from '../assets/Svg/History.svg';
import historySvgDisabled from '../assets/Svg/History_disabled.svg';
import timerSvg from '../assets/Svg/Timer.svg';
import timerSvgDisabled from '../assets/Svg/Timer_disabled.svg';
import table_listingSvg from '../assets/Svg/table_listing.svg';
import card_blackSvg from '../assets/Svg/card_black.svg';
import winner_dealer_lineSvg from '../assets/Svg/winner_dealer_line.svg';
import spade_blackSvg from '../assets/Svg/spade_black.svg';
import hearts_blackSvg from '../assets/Svg/hearts_black.svg';
import diamond_blackSvg from '../assets/Svg/diamond_black.svg';
import clubs_blackSvg from '../assets/Svg/clubs_black.svg';
import nt_yellowSvg from '../assets/Svg/nt_yellow.svg';
import nt_blackSvg from '../assets/Svg/nt_black.svg';
import bidding_lineSvg from '../assets/Svg/bidding_line.svg';
import settingsSvg from '../assets/Svg/settings.svg';
import flagSvg from '../assets/Svg/flag.svg';
import cheat_sheet_assSvg from '../assets/Svg/cheat_sheet_ass.svg';
import revers_2Svg from '../assets/Svg/revers_2.svg';
import chatSvg from '../assets/Svg/Chat.svg';
import green_crxSvg from '../assets/Svg/green_crx.svg';
import play_history_slider_icon1Svg from '../assets/Svg/play_history_slider_icon1.svg';
import play_history_slider_icon2Svg from '../assets/Svg/play_history_slider_icon2.svg';
import close_yellow from '../assets/Svg/close_yellow.svg';
import signUpCircleSvg from '../assets/Svg/signupCircle.svg';
import signupFilledSvg from '../assets/Svg/signupFilled.svg';
import EyeIndicator from '../assets/Svg/Group 193.svg';
import JoinSymbol from '../assets/Svg/join_symbol.svg';
import Profile1 from '../assets/Svg/profile_icon_1.svg';
import Profile2 from '../assets/Svg/profile_icon_2.svg';
import ClubsBlack from '../assets/Svg/clubs_black.svg';
import DiamondsBlack from '../assets/Svg/diamond_black.svg';
import SpadesBlack from '../assets/Svg/spade_black.svg';
import HeartsBlack from '../assets/Svg/hearts_black.svg';
import BidNonActive from '../assets/Svg/Line 14.svg'
import XFour from '../assets/Svg/x4.svg';
import Nt from '../assets/Svg/nt.svg';
import NtActive from '../assets/Svg/nt_yellow.svg'
import ClubsInactive from '../assets/Svg/Clubs_inactive.svg';
import DiamondsInactive from '../assets/Svg/Diamond_inactive.svg';
import SpadesInactive from '../assets/Svg/Spade_inactive.svg';
import HeartsInactive from '../assets/Svg/Hearts_inactive.svg';
import BidActive from '../assets/Svg/Line_active.svg'
import NtInaactive from '../assets/Svg/nt_inactive.svg';
import BidCircle from '../assets/Svg/Ellipse 71.svg';
import CancelRed from '../assets/Svg/Group 246red.svg';
import CancelBlack from '../assets/Svg/Group 246.svg';
import Lock from '../assets/Svg/Lock.svg';
import Back from '../assets/Svg/Back.svg';
import find_partnerSvg from '../assets/Svg/find_partner.svg';
import find_partnerSvgDisbaled from '../assets/Svg/find_partner_disabled.svg';
import instant_gameSvg from '../assets/Svg/instant_game.svg';
import instant_gameSvgDisabled from '../assets/Svg/instant_game_disabled.svg';
import play_botsSvg from '../assets/Svg/play_bots.svg';
import play_botsSvgDisabled from '../assets/Svg/play_bots_disbaled.svg';
import create_tableSvg from '../assets/Svg/create_table.svg';
import find_tableSvg from '../assets/Svg/find_table.svg';
import switchSvg from '../assets/Svg/switch.svg';
import switchSvgDisabled from '../assets/Svg/switch_disabled.svg';
import find_partner_proSvg from '../assets/Svg/find_partner_pro.svg';
import instant_game_proSvg from '../assets/Svg/instant_game_pro.svg';
import play_bots_proSvg from '../assets/Svg/play_bots_pro.svg';
import create_table_proSvg from '../assets/Svg/create_table_pro.svg';
import find_table_proSvg from '../assets/Svg/find_table_pro.svg';
import switch_proSvg from '../assets/Svg/switch_pro.svg';
import Menu from '../assets/Svg/Menu.svg';
import MenuDisabled from '../assets/Svg/Menu_disabled.svg';
import Chat from '../assets/Svg/icon_chat.svg';
import RightArrow from '../assets/Svg/Rectangle 21.svg';
import RightArrowDisabled from '../assets/Svg/Rectangle 21_Disabled.svg';
import LeftArrow from '../assets/Svg/Rectangle 22.svg';
import LeftArrowDisabled from '../assets/Svg/Rectangle 22 _Disabled.svg';
import CenterCircle from '../assets/Svg/Ellipse21.svg';
import Crown from '../assets/Svg/crown.svg';
import Search from '../assets/Svg/Union.svg';
import ClubsWhite from '../assets/Svg/clubs_white.svg';
import HeartsWhite from '../assets/Svg/hearts_white.svg';
import DiamondsWhite from '../assets/Svg/diamond_white.svg';
import SpadesWhite from '../assets/Svg/spade_white.svg';
import EastDirectionPointer from '../assets/Svg/direction_pointer_e.svg';
import westDirectionPointer from '../assets/Svg/direction_pointer_w.svg';
import southDirectionPointer from '../assets/Svg/direction_pointer_s.svg';
import northDirectionPointer from '../assets/Svg/direction_pointer_n.svg';
import undo from '../assets/Svg/icon_undo.svg';
import undoActive from '../assets/Svg/icon_undo_active.svg';
import iconCancel from '../assets/Svg/icon_cancel.svg';
import ChatDisbaled from '../assets/Svg/icon_chat_diabled.svg';
import LockActive from '../assets/Svg/lock_active.svg';
import TimerActive from '../assets/Svg/timer_active.svg';
import DropArrow from '../assets/Svg/down_arrow.svg';
import EmptyCard from '../assets/Svg/empty.svg';
import EmptyBidding from '../assets/Svg/emptybidding.svg';
import CreatePlay from '../assets/Svg/Create_Play.svg';
import JoinSymbolPlay from '../assets/Svg/Join Symbol.svg';
import CreatePlayScore from '../assets/Svg/Create Play.svg';
import LinePlay from '../assets/Svg/Line 16.svg';
import South from '../assets/Svg/group_373.svg';
import East from '../assets/Svg/group_374.svg';
import West from '../assets/Svg/group_371.svg';
import Indicator from '../assets/Svg/icon-Kibitzer.svg';
import AcceptBackground from '../assets/Svg/rectangle_169.svg';
import Group from '../assets/Svg/ellipse_101.svg';
import NotificationBG from '../assets/Svg/Rectangle 168.svg';
import Active from '../assets/Svg/active.svg';
import OtherBG from '../assets/Svg/Ellipse 97.svg';
import Blue from '../assets/Svg/blue.svg';
import Green from '../assets/Svg/green.svg';
import SendButton from '../assets/Svg/rectangle_175.svg';
import DropDown from '../assets/Svg/vector.svg';
import AddFriend from '../assets/Svg/Rectangle 169.svg';
import Message from '../assets/Svg/Rectangle 170.svg';
import Leave from '../assets/Svg/Rectangle 171.svg';
import Ellipse from '../assets/Svg/ellipse_104.svg';
import ScoreLine from '../assets/Svg/Line 17.svg';
import RightChat from '../assets/Svg/Vector 46.svg';
import LeftChat from '../assets/Svg/Vector 44.svg';
import Host from '../assets/Svg/Group 374.svg';
import HalfCircle from '../assets/Svg/Ellipse 104.svg';
import PointerLeft from '../assets/Svg/pointer_left.svg';
import PointerRight from '../assets/Svg/pointer_right.svg';
import PointerNorth from '../assets/Svg/pointer_north.svg';
import PointerSouth from '../assets/Svg/pointer_south.svg';
import Pointer from '../assets/Svg/circle_pointer.svg';
import AddPlayer from '../assets/Svg/add_player.svg';
import tick from '../assets/Svg/tick.svg';
import  dropNorth from '../assets/Svg/drop_noth.svg';
import  dropNorthEnabled from '../assets/Svg/drop_noth_enabled.svg';
import  dropWest from '../assets/Svg/drop_west.svg';
import  questionNorth from '../assets/Svg/question_north.svg';
import  questionWest from '../assets/Svg/question_west.svg';
import  clear from '../assets/Svg/Vector 7.svg';
import  plus from '../assets/Svg/Group 143.svg';
import  minus from '../assets/Svg/Line 15.svg';
import  clearDisabled from '../assets/Svg/clear_disabled.svg';
import  plusDisabled from '../assets/Svg/plus_disabled.svg';
import  minusDisabled from '../assets/Svg/minus_disabled.svg';
import  tickDisabled from '../assets/Svg/tick_disabled.svg';
import  clubDisabled from '../assets/Svg/clubs_disabled.svg';
import online from '../assets/Svg/online.svg';
import flagActive from '../assets/Svg/flagActive.svg'

const svgImages = {
  spade: spadeSvg,
  hearts: heartsSvg,
  diamond: diamondSvg,
  clubs: clubsSvg,
  spade_black: spade_blackSvg,
  hearts_black: hearts_blackSvg,
  diamond_black: diamond_blackSvg,
  clubs_black: clubs_blackSvg,
  nt_black: nt_blackSvg,
  players: playersSvg,
  playersSvgDisbaled: playersSvgDisbaled,
  refreshSvgDisabled: refreshSvgDisabled,
  refresh: refreshSvg,
  kibitzerSvgDisabled: kibitzerSvgDisabled,
  kibitzer: kibitzerSvg,
  kibitzerProfile: kibitzerProfile,
  cheat_Sheet: cheat_SheetSvg,
  cheat_Sheet_diabled: cheat_Sheet_diabled,
  cheat_Sheet_white: cheat_Sheet_white,
  history: historySvg,
  historySvgDisabled: historySvgDisabled,
  timer: timerSvg,
  timerSvgDisabled: timerSvgDisabled,
  table_listing: table_listingSvg,
  card_black: card_blackSvg,
  winner_dealer_line: winner_dealer_lineSvg,
  nt_yellow: nt_yellowSvg,
  nt_black: nt_blackSvg,
  bidding_line: bidding_lineSvg,
  settings: settingsSvg,
  flagActive: flagActive,
  flag: flagSvg,
  cheat_sheet_ass: cheat_sheet_assSvg,
  revers_2: revers_2Svg,
  chat: chatSvg,
  green_crx: green_crxSvg,
  play_history_slider_icon1: play_history_slider_icon1Svg,
  play_history_slider_icon2: play_history_slider_icon2Svg,
  close_yellow: close_yellow,
  signupFilled: signupFilledSvg,
  signUpCircle: signUpCircleSvg,
  eyeIndicator: EyeIndicator,
  joinSymbol: JoinSymbol, 
  profile1: Profile1,
  profile2: Profile2,
  clubsBlack: ClubsBlack,
  diamondBlack: DiamondsBlack,
  heartsBlack: HeartsBlack,
  spadesBlack: SpadesBlack,
  bidNonActive: BidNonActive,
  x4: XFour,
  clubsInactive: ClubsInactive,
  diamondInactive: DiamondsInactive,
  heartsInactive: HeartsInactive,
  spadesInactive: SpadesInactive,
 // ntInactive: NtInactive,
  bidActive: BidActive,
  nt: Nt,
  ntActive: NtActive,
  ntIn_Active: NtInaactive,
 // ntINActive: NtInaactive,
  bidCircle: BidCircle,
  cancelRed: CancelRed,
  cancelBlack: CancelBlack,
  lock: Lock,
  back: Back,
  findPartnerSvg: find_partnerSvg,
  findPartnerSvgDisabled: find_partnerSvgDisbaled,
  instantGameSvg: instant_gameSvg,
  instantGameSvgDisabled: instant_gameSvgDisabled,
  playBotSvg: play_botsSvg,
  playBotSvgInactive: play_botsSvgDisabled,
  createTableSvg: create_tableSvg,
  findTableSvg: find_tableSvg,
  switchSvg: switchSvg,
  switchSvgDisabled: switchSvgDisabled,
  findPartnerProSvg: find_partner_proSvg,
  instantGameProSvg: instant_game_proSvg,
  playBotProSvg: play_bots_proSvg,
  createTableProSvg: create_table_proSvg,
  finaTableProSvg: find_table_proSvg,
  switchProSvg: switch_proSvg,
  menuSvg: Menu,
  MenuDisabled: MenuDisabled,
  chatSvg: Chat,
  rightArrow: RightArrow,
  rightArrowdisbaled: RightArrowDisabled,
  leftArrow: LeftArrow,
  leftArrowDisbaled: LeftArrowDisabled,
  centerCircle: CenterCircle,
  crown: Crown,
  search: Search,
  clubsWhite: ClubsWhite,
  heartsWhite: HeartsWhite,
  diamondsWhite: DiamondsWhite,
  spadesWhite: SpadesWhite,
  eastDirectionPointer: EastDirectionPointer,
  westDirectionPointer: westDirectionPointer,
  southDirectionPointer: southDirectionPointer,
  northDirectionPointer: northDirectionPointer,
  undo: undo,
  undoActive: undoActive,
  iconCancel: iconCancel,
  chatDisbaled: ChatDisbaled,
  lockActive: LockActive,
  timerActive: TimerActive,
  dropArrow: DropArrow,
  emptyCard: EmptyCard,
  emptyBidding: EmptyBidding,
  createPlay: CreatePlay,
  joinSymbolPlay: JoinSymbolPlay,
  line: LinePlay,
  south: South,
  east: East,
  west: West,
  indicator: Indicator,
  acceptBackGround: AcceptBackground,
  group: Group,
  notificationBG: NotificationBG,
  active: Active,
  otherBG: OtherBG,
  blue: Blue,
  green: Green,
  sendButton: SendButton,
  dropDown: DropDown,
  addFriend: AddFriend,
  message: Message,
  leave: Leave,
  ellipse: Ellipse,
  createPlayScore: CreatePlayScore,
  scoreLine: ScoreLine,
  rightChat: RightChat,
  leftChat: LeftChat,
  host: Host,
  halfCircle: HalfCircle,
  pointerLeft: PointerLeft,
  pointerRight: PointerRight,
  pointerNorth: PointerNorth,
  pointerSouth: PointerSouth,
  pointer: Pointer,
  addPlayer: AddPlayer,
  tick: tick,
  dropNorth: dropNorth,
  dropWest: dropWest,
  questionNorth: questionNorth,
  questionWest: questionWest,
  clear: clear,
  plus: plus,
  minus: minus,
  clearDisabled: clearDisabled,
  plusDisabled: plusDisabled,
  minusDisabled: minusDisabled,
  tickDisabled: tickDisabled,
  dropNorthEnabled: dropNorthEnabled,
  clubDisabled: clubDisabled,
  online: online,
};
export default svgImages;
