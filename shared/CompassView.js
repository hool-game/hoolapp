import React from 'react'

import { Text, View } from 'react-native'
import { moderateScale, moderateVerticalScale, scale, ScaledSheet, verticalScale } from 'react-native-size-matters/extend'
import { Colors } from '../styles/Colors'
import { normalize } from '../styles/global'

const CompassView = (props) => {
    return (
        <View style={styles.compassView}>
            <View style={styles.weContainer}>
                <Text style={styles.title}>
                    W
                </Text>
            </View>
            <View style={styles.nsContainer}>
                <Text style={styles.title}>
                    N
                </Text>
                <View style={styles.pointerView}>
                    {/* <View style={styles.pointer} /> */}
                </View>
                <Text style={{...styles.title, paddingTop: moderateVerticalScale(6.89)}}>
                    S
                </Text>
            </View>
            <View style={styles.weContainer}>
                <Text style={styles.title}>
                    E
                </Text>
            </View>
        </View>
    )
}

const styles = ScaledSheet.create({
    title: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(10),
        height: scale(21),
        color: Colors.rgb_676767
    },
    compassView: {
        flexDirection: 'row',
    },
    nsContainer: {
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    weContainer: {
        alignSelf: 'center',
        alignItems: 'center',
        height: scale(10),
        width: scale(10),
        margin: moderateVerticalScale(6.89)
    },
    pointerView: {
        borderRadius: moderateVerticalScale(21),
        borderColor: Colors.rgb_676767,
        borderWidth: moderateVerticalScale(0.5),
        width: scale(21),
        height: scale(21)
    },
    pointer: {
        backgroundColor: Colors.rgb_DBFF00,
        borderColor: Colors.rgb_DBFF00,
        borderRadius: moderateVerticalScale(4),
        borderWidth: moderateVerticalScale(0.1),
        width: scale(4),
        height: scale(4),
    }
})

export default CompassView