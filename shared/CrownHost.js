import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SvgXml } from 'react-native-svg';
import { moderateScale, scale, ScaledSheet } from 'react-native-size-matters/extend';
import svgImages from '../API/SvgFiles';
import { restrictUserNameToEightChar } from './util';
import { verticalScale } from 'react-native-size-matters';
/**
 * FlightInfo.js
 * Flight Icon on Task Details Item and Task Results
 * @param {*} props
 */
const CrownHost = props => {

    const returnViewWithHost = (props) => {
        if (!props.shareBidPlay) {
            if (props.hostName == props.userName) {
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <SvgXml
                            xml={svgImages.crown}
                            width={scale(11)}
                            height={scale(11)}
                            style={props.crownStyle == undefined ? {
                                alignSelf: 'center',
                                left: moderateScale(-14),
                                alignContent: 'center',
                                position: 'absolute'
                            } : props.crownStyle}
                        />
                        <View>
                            <Text style={props.textStyle} >
                                {restrictUserNameToEightChar(props.userName)}
                            {/* {props.reduceChar ? restrictUserNameToEightChar(props.userName) : props.userName} */}
                            </Text>
                        </View>
                    </View>)
            } else {
                return (<View >
                    <Text style={props.textStyle} numberOfLines={1} ellipsizeMode={'tail'}>
                    {restrictUserNameToEightChar(props.userName)}
                    {/* {props.reduceChar ? restrictUserNameToEightChar(props.userName) : props.userName} */}
                    </Text>
                </View>)
            }
        } else {
            if (props.hostName == props.userName) {
                if (!props.rightSide) {
                    return (
                        <View style={ props.isRotated == undefined ? { flexDirection: 'row', justifyContent: 'space-evenly' ,transform:[{ rotate: '-90deg'}]} : 
                        { flexDirection: 'row', justifyContent: 'space-evenly'  }}>
                            <SvgXml
                                xml={svgImages.crown}
                                width={scale(11)}
                                height={scale(11)}
                                style={{ ...props.crownStyle }}
                            />
                            <View style={{ paddingLeft: scale(5)}}>
                                <Text style={props.textStyle} numberOfLines={1} ellipsizeMode={'tail'}>
                                {restrictUserNameToEightChar(props.userName)}
                                {/* {props.reduceChar ? restrictUserNameToEightChar(props.userName) : props.userName} */}
                                </Text>
                            </View>
                        </View>)
                } else {
                    return (
                        <View style={props.isRotated == undefined ? { flexDirection: 'row', justifyContent: 'space-evenly' ,transform:[{ rotate: '90deg'}] } : 
                        { flexDirection: 'row', justifyContent: 'space-evenly'  }}>
                            <SvgXml
                                xml={svgImages.crown}
                                width={scale(11)}
                                height={scale(11)}
                                style={{ ...props.crownStyle}}
                            />
                            <View style={{ paddingLeft: scale(5)}} >
                                <Text style={props.textStyle} numberOfLines={1} ellipsizeMode={'tail'}>
                                {restrictUserNameToEightChar(props.userName)}
                                {/* {props.reduceChar ? restrictUserNameToEightChar(props.userName) : props.userName} */}
                                </Text>
                            </View>
                        </View>)
                }
            } else {
                return (
                    <View style={
                        props.isRotated == undefined ?  props.rightSide ? {transform:[{ rotate: '90deg'}],width:scale(180),height:scale(23) ,paddingTop:scale(5)} :
                        { transform:[{ rotate: '-90deg'}],width:scale(180),height:scale(23),paddingTop:scale(5) }: {}
                    }>
                        <Text style={props.textStyle} numberOfLines={1} ellipsizeMode={'tail'}>
                        {restrictUserNameToEightChar(props.userName)}
                        {/* {props.reduceChar ? restrictUserNameToEightChar(props.userName) : props.userName} */}
                        </Text>
                    </View>
                )
            }
        }

    }

    return (
        <View>
            {returnViewWithHost(props)}
        </View>
    )
}

export default CrownHost