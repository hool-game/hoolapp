import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Alert, Button } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { Colors } from '../styles/Colors';
import { Strings } from '../styles/Strings';
import { normalize } from '../styles/global';
import svgImages from '../API/SvgFiles';
import { moderateScale, moderateVerticalScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { ChatConstants } from '../redux/actions/ChatActions';
import { JoinTableConstants } from '../redux/actions/JoinaTableActions';
import  UserProfile  from '../screens/chat/UserProfile'
import { HOOL_CLIENT } from './util';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';


class ChatItem extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      hitProfile:false
    };
  }

  componentDidMount() {}
  componentWillUnmount() {}

  getImageIndicatorBasedOnUser = type => {
      if (type === 's') {
        return <SvgXml height={scale(14)} width={scale(14)} xml={svgImages.south} style={styles.imageStyle} />;
      } else if (type === 'w') {
        return <SvgXml height={scale(14)} width={scale(14)} xml={svgImages.west} style={styles.imageStyle} />;
      } else if (type === 'e') {
        return <SvgXml height={scale(14)} width={scale(14)} xml={svgImages.east} style={styles.imageStyle} />;
      } else if (type === 'h') {
        return <SvgXml height={scale(14)} width={scale(14)} xml={svgImages.host} style={styles.imageStyle} />;
      }else {
        return <SvgXml height={scale(33)} width={scale(33)} xml={svgImages.indicator} style={styles.eyeImageStyle} />;
      }
  };

  showProfileIconBasedOnUSer = showProfileIcon => {
    if (showProfileIcon) {
      return <SvgXml height={scale(22)} width={scale(22)} xml={svgImages.profile1} style={styles.profileIndicatorStyle} />;
    } else {
      return (
        (!this.props.addFriend ? 
        <View style={{
          height: scale(22), width: scale(22), 
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center'
        }}>
          <SvgXml height={scale(5)} width={scale(5)} xml={this.props.ChatItem.isOnline ? svgImages.online : svgImages.group}/>
        </View> : 
        <View style={{
          height: scale(22), width: scale(22), 
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          marginTop: moderateScale(30),
          
        }}>
          <SvgXml height={scale(5)} width={scale(5)} xml={this.props.ChatItem.isOnline ? svgImages.online : svgImages.group}/>
        </View>)
        );
    }
  };

  showProfileIcon = () => {
     if( this.props.view === Strings.notification ){
      if( this.props.ChatItem.avatarImage === 'profile1' ){
       return <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: moderateScale(15), marginLeft: moderateScale(8) }}>
          <SvgXml height={scale(28)} width={scale(28)} xml={svgImages.profile1} />
        </View>
      }else if( this.props.ChatItem.avatarImage === 'profile2' ){
        return <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: moderateScale(15), marginLeft: moderateScale(8) }}>
          <SvgXml height={scale(28)} width={scale(28)} xml={svgImages.profile2} />
        </View>
      }else{
        return <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: moderateScale(15), marginLeft: moderateScale(8) }}>
          <SvgXml height={scale(28)} width={scale(28)} xml={svgImages.kibitzerProfile} />
        </View>
      } 
     }else if (this.props.ChatItem.avatarImage === 'bot'){
      return <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: moderateScale(15), marginLeft: moderateScale(8) }}>
          <SvgXml height={scale(25)} width={scale(25)} xml={svgImages.playBotSvg} />
        </View>
     }
     else{
      return <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: moderateScale(15), marginLeft: moderateScale(8) }}>
        <SvgXml height={scale(28)} width={scale(28)} xml={svgImages.profile1} />
      </View>
     }
  }
  
  acceptFriendRequest = async (userID) => {
    HOOL_CLIENT.rosterSubscribed(userID)
    HOOL_CLIENT.rosterSubscribe(userID)

    this.props.fetchFriendsList();
    
    let notificationCount = this.props.notificationCount
    notificationCount = notificationCount - 1
    await AsyncStorage.setItem('notificationCount',JSON.stringify(notificationCount))
    this.props.setNotificationCount({
      notificationCount: notificationCount
    })
    let updatedList = [...this.props.notificationList]
    updatedList.forEach( (element,index) => {
      if( element.name === this.props.ChatItem.name)
      updatedList[index].isInviteAccepted = true
    })
    await AsyncStorage.setItem('notificationList',JSON.stringify(updatedList))

    this.props.updateNotificationList({
      updatedList : updatedList
    })
  
  }

  declineFriendRequest = async (userID) => {
   HOOL_CLIENT.rosterUnsubscribed(userID)

   let notificationCount = this.props.notificationCount
    notificationCount = notificationCount - 1
    await AsyncStorage.setItem('notificationCount',JSON.stringify(notificationCount))
    this.props.setNotificationCount({
      notificationCount: notificationCount
    })
   let updatedList = [...this.props.notificationList]
   updatedList.forEach( (element,index) => {
     if( element.name === this.props.ChatItem.name)
     updatedList[index].isInviteDeclined = true
   })
   await AsyncStorage.setItem('notificationList',JSON.stringify(updatedList))
   this.props.updateNotificationList({
     updatedList : updatedList
   })
  }

  acceptOrDeclineButton = () => {
    return (
      <View style={styles.viewHolder}>
        <View style={styles.notificationContainerStyle}>
          <TouchableOpacity style={{marginEnd: scale(3)}} onPress={async () => await this.acceptFriendRequest(this.props.ChatItem.jid)}>
            <View style={{height: scale(24),width: scale(94) , borderRadius: scale(40) , backgroundColor: '#151515'}}>
              <View style={{...styles.centerView, height: scale(25), width: scale(78),
                  justifyContent:'center', alignItems: 'center'}}>
                <Text style={styles.acceptTextStyle}>{Strings.accept}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={async () => await this.declineFriendRequest(this.props.ChatItem.jid)}>
          <View style={{height: scale(24),width: scale(94) , borderRadius: scale(40) , backgroundColor: '#151515'}}>
              <View style={{...styles.centerView, height: scale(25), width: scale(78),
                  justifyContent:'center', alignItems: 'center'}}>
                <Text style={styles.acceptTextStyle}>{Strings.decline}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  
  notificationStatus = (status) => {
    return(
        <View style={styles.viewHolder}>
          <View style={{...styles.centerView,  height: scale(24), width: scale(94),}}>
            <Text style={{...styles.acceptTextStyle,fontFamily: 'Roboto-LightItalic', }}>
              {status}
            </Text>
          </View>
        </View>
    );
  }

  showAcceptAndDecline = () => {
   if( this.props.ChatItem.isGameInvite ){
      if(!this.props.ChatItem.isInviteAccepted && !this.props.ChatItem.isInviteDeclined) {
        
        if(this.props.ChatItem.isGameInviteMissed){
          return this.notificationStatus('Missed')
        }else{
          return this.acceptOrDeclineButton()
        }
      }else{
        if(this.props.ChatItem.isInviteAccepted){
          return this.notificationStatus('Accepted')
        }else if(this.props.ChatItem.isInviteDeclined){
          return this.notificationStatus('Declined') 
        }
      }
   }else if( this.props.ChatItem.isFriendRequest ) {
    if(this.props.ChatItem.isInviteAccepted){
      return this.notificationStatus('Accepted')
    }else if(this.props.ChatItem.isInviteDeclined){
      return this.notificationStatus('Declined') 
    }else{
      return this.acceptOrDeclineButton()
    }
   }
  }

  loadOptions = () => {
    if (this.props.view !== Strings.notification) {
      if (this.props.view === Strings.friends) {
        return (
          <View style={styles.friendsViewHolder}>
            
            {this.props.ChatItem.indication &&
              this.getImageIndicatorBasedOnUser(
                this.props.ChatItem.indicatorType
              )
            }
            {this.showProfileIconBasedOnUSer(
              this.props.ChatItem.showProfileIcon
            )}
          </View>
        );
      } else if (this.props.view !== Strings.friends) {
        return (
          <View style={styles.viewHolder}>
            {this.props.ChatItem.indication &&
              this.getImageIndicatorBasedOnUser(
                this.props.ChatItem.indicatorType
              )}
          </View>
        );
      }
    } else {
      return this.showAcceptAndDecline(false);
    }
  };


 
  playerAdd = () =>{
    this.props.playerAdd({
      direction: this.props.currentDirection,
      userName: this.props.ChatItem.name,
      isFriend: false
    })
    this.props.closeFriendsList()
    this.props.addPlayerSuccess()
  }


  render() {
    return ( 
      <View style={{...styles.containerStyle}}>
        <TouchableOpacity 
          style={{}}
          // onPress={()=> {
          //   this.props.onUserProfile()
          // }}
        >
          <View style={{ height: scale(50), width: scale(50) , alignItems:'center', alignSelf:'center' , justifyContent:'center',marginLeft: moderateScale(3)}}>
            {
              this.props.ChatItem.name == 'Table Messages' ?
                <View style={{ position: 'relative',alignItems: 'center', justifyContent: 'center', width: scale(50) , height:scale(50),}}>
                  <View style={{ flexDirection: 'row', }}>
                    <SvgXml height={scale(17)} width={scale(17)}  xml={svgImages.profile2}
                      style={{ backgroundColor: 'transparent', marginRight: moderateScale(3), marginLeft: moderateScale(7)}} />
                    <SvgXml height={scale(17)} width={scale(17)} xml={svgImages.profile1}
                      style={{ backgroundColor: 'transparent' ,marginRight: moderateScale(8),}} />
                  </View>
                  <SvgXml height={scale(17)} width={scale(17)}  xml={svgImages.kibitzerProfile}
                    style={{ position:'absolute' ,backgroundColor: 'transparent', top: scale(26) ,}} />
                </View>
                :
                this.showProfileIcon()
            }
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            (!this.props.addFriend ? 
              this.props.onTouch() : this.props.onGameInv()) //: this.playerAdd(this.props.ChatItem)
          }}
          disabled ={this.props.view === Strings.notification}
        >
          <View style={{ height: scale(50) , width: scale(460)}}>
            <View style={( this.props.view == Strings.notification ) ? {justifyContent: 'center', marginTop: moderateScale(5)}: {justifyContent: 'center', marginTop: moderateScale(15)}}>
              <Text style={ (( this.props.ChatItem.isInviteAccepted || this.props.ChatItem.isInviteDeclined) || this.props.ChatItem.isNewMessages === false) ? {...styles.textStyle, alignSelf: 'flex-start'}: {...styles.textStyle, fontFamily: 'Roboto',fontWeight: '700',alignSelf: 'flex-start'}}>{this.props.view === Strings.friends ? this.props.ChatItem.name : this.props.ChatItem.name}</Text>
                {
                this.props.view == Strings.notification &&
                <View style={{flexDirection: 'row', alignSelf: 'flex-start',justifyContent: 'center'}}>
                  <Text style={{ ...styles.subTextStyle, }}>
                    {this.props.ChatItem.isGameInvite ? 'Game invite' : 'Friend request' }
                  </Text>
                  <View style={{justifyContent: 'center',left: scale(3) ,top:moderateScale(1)}}>
                    <Text style={{ ...styles.subTextTimeStyle, alignSelf: 'center',}}>{moment(new Date(this.props.ChatItem.timeOfRequest)).fromNow()}</Text>
                  </View>
                </View>
                }
            </View>
            {
              this.props.addFriend ?
                <View style={styles.friendsViewHolder}>
                  <TouchableOpacity style={{
                    height: scale(22), width: scale(22), marginEnd: moderateVerticalScale(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                  }} 
                  // onPress={ () => {
                  //   this.playerAdd(this.props.ChatItem)
                  // }}
                >
                    {/* <SvgXml height={scale(22)} width={scale(22)} xml={svgImages.addPlayer} /> */}
                  </TouchableOpacity>
                  {
                    this.props.ChatItem.indication ?
                      <View style={{
                        height: scale(22), width: scale(22), marginEnd: moderateVerticalScale(5),
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                        {this.showProfileIconBasedOnUSer(
                          this.props.ChatItem.showProfileIcon
                        )}
                      </View>
                    : 
                      <View style={{
                        height: scale(22), width: scale(22), marginEnd: moderateVerticalScale(5),
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      </View>
                  }
                  
                </View>
              :
              this.loadOptions()          
            }
          </View>
        </TouchableOpacity>
      </View>
    )
  }
           
}
const styles = ScaledSheet.create({
  containerStyle: {
    backgroundColor: Colors.searchBackGround,
    flexDirection: 'row',
    height: scale(50),
    width: '100%'
  },
  textStyle: {
    color: Colors.white,
    fontSize: normalize(13),
    alignSelf: 'center',
    fontFamily: 'Roboto-Light',
    fontWeight: 'normal',
  },
  subTextStyle: {
    color: Colors.white,
    fontSize: normalize(13),
    alignSelf: 'center',
    fontFamily: 'Roboto-LightItalic',
  },
  subTextTimeStyle: {
    color: '#4A4A4A',
    fontSize: normalize(10),
    alignSelf: 'center',
    fontFamily: 'Roboto-LightItalic',
  },
  imageStyle: {
    marginRight: '9@ms',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  eyeImageStyle: {
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  avatarStyle: {
    alignItems: 'center',
    alignContent: 'center',
    marginStart: '11@ms',
    //marginEnd: '1@ms',
    marginTop: '11@ms',
    marginBottom: '11@ms'
  },
  profileIndicatorStyle: {
    marginStart: '11@ms',
    marginEnd: '20@ms',
    marginTop: '15@ms',
    marginBottom: '11@ms',
    alignContent: 'center',
    alignSelf: 'flex-end'
  },
  notificationContainerStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },
  centerView: {
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent:'center'
  },
  acceptTextStyle: {
    color: Colors.white,
    fontFamily: 'Roboto-Light',
    fontSize: normalize(13),
    fontWeight: '300'
  },
  viewHolder: {
    position: 'absolute',
    height: scale(50),
    right:0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginEnd: '10@ms',
  },
  friendsViewHolder: {
    position: 'absolute',
    height: scale(50),
    right: 0,
    alignSelf: 'center',
    flexDirection: 'row',
    marginEnd: '10@ms',
  },
});

const mapStateToProps = (state) => ({
  currentDirection: state.joinTableActions.currentDirection,
  notificationList: state.chatActions.notificationList,
  notificationCount: state.chatActions.notificationCount,
  setClicked: state.chatActions.setClicked,
  clickedUser: state.chatActions.clickedUser
});

const mapDispatchToProps = (dispatch) => ({
  playerAdd: (params) =>
    dispatch({ type: ChatConstants.SET_USER_INVITED, params: params }),
  closeFriendsList: (params) =>
    dispatch({ type: JoinTableConstants.CLOSE_FRIEND_LIST, params: params }),
  addPlayerSuccess: (params) =>
    dispatch({ type: JoinTableConstants.ADD_PLAYER_SUCCESS, params: params }),
  fetchFriendsList: (params) =>
    dispatch({ type: ChatConstants.FETCH_FRIENDS_LIST, params: params}),
  setNotificationList: (params) =>
    dispatch({ type: ChatConstants.NOTIFICATION_LIST , params: params}),
  updateNotificationList: (params) => 
    dispatch({ type: ChatConstants.UPDATE_NOTIFICATION_LIST , params: params}),
  setNotificationCount: (params) => 
    dispatch({ type: ChatConstants.SET_NOTIFICATION_COUNT , params: params}),
  setClick: (params) => 
    dispatch({ type: ChatConstants.SET_CLICKED, params: params}),
  setClickedUser: (params) => 
    dispatch({ type: ChatConstants.CLICKED_USER, params: params}),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatItem);
