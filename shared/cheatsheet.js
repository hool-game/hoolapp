import React, { useEffect, useState } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { moderateScale, moderateVerticalScale, scale, ScaledSheet } from 'react-native-size-matters/extend'
import { Colors } from '../styles/Colors'
import { normalize } from '../styles/global'
import { Strings } from '../styles/Strings'
import InfoView from './InfoView'
import AsyncStorage from '@react-native-async-storage/async-storage';
import NumberSelectView from './NumberSelectView'
import { SCREENS } from '../constants/screens'
import Directions from './Directions'

const CheatSheet = (props) => {
    const [modalVisible, setmodalVisible] = useState(false)
    const [playClickedDirection, setClickedDirection] = useState('')
    const [type, setType] = useState('')
    const [selectedItemValue, setSelectedItemValue] = useState('')
    const [isTableMessages,setIsTableMessages] = useState(false)
    const oldValues = [
        {
            direction: 'N',
            value: [
                {
                    type: 'HCP',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Pattern',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Spade',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Heart',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Diamond',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Club',
                    oldValue: '-1',
                    newValue: '-1'
                },
            ]
        },
        {
            direction: 'S',
            value: [
                {
                    type: 'HCP',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Pattern',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Spade',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Heart',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Diamond',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Club',
                    oldValue: '-1',
                    newValue: '-1'
                },
            ]
        },
        {
            direction: 'W',
            value: [
                {
                    type: 'HCP',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Pattern',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Spade',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Heart',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Diamond',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Club',
                    oldValue: '-1',
                    newValue: '-1'
                },
            ]
        },
        {
            direction: 'E',
            value: [
                {
                    type: 'HCP',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Pattern',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Spade',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Heart',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Diamond',
                    oldValue: '-1',
                    newValue: '-1'
                },
                {
                    type: 'Club',
                    oldValue: '-1',
                    newValue: '-1'
                },
            ]
        },
    ]
    const [values, setValues] = useState(oldValues)
    const [tempValues, setTempValues] = useState(values)
    const updateModalState = (direction, type, selectedValue) =>{
        setmodalVisible(true)
        setClickedDirection(direction)
        setType(type)
        setSelectedItemValue(selectedValue)
    }

    const showUpdateValues = (value) => {
        let updatedArray = tempValues.map((values) => (
            values.direction == playClickedDirection ?
                {...values,
                    value: values.value.map(el => (
                    (el.type == type) ? {...el, newValue: value.toString()}: el
                ))}
            :
            values
        ))
        setTempValues(updatedArray)
    }


    const revertOldValues = () => {
        setmodalVisible(false)
        setClickedDirection('')
        setType('')
        setSelectedItemValue('')
        setTempValues(values)
    }

    const saveValuesProvided = (value) => {
        setmodalVisible(false)
        setClickedDirection('')
        setType('')
        setSelectedItemValue('')
        let updatedArray = tempValues.map((values) => (
            values.direction == playClickedDirection ?
                {...values,
                    value: values.value.map(el => (
                    (el.type == type && el.oldValue != value) ? {...el, newValue: value}:el
                ))}
            :
            values
        ))
        setValues(updatedArray)
        AsyncStorage.setItem('cheatsheetTableID', props.tableID);
        AsyncStorage.setItem('values', JSON.stringify(updatedArray));
    }

    const getGameStateValueForMine = (values) =>{
            let hcp = -1
            let pattern = -1
            let spadeSuit = -1
            let heartsSuit = -1
            let diamondsSuit = -1
            let clubsSuit = -1
            let tempArray = values
            let westArrayValue = tempArray.filter(state => state.direction == 'W')
            let southArrayValue = tempArray.filter(state => state.direction == 'S')
            let northArrayValue = tempArray.filter(state => state.direction == 'N')
            let eastArrayValue = tempArray.filter(state => state.direction == 'E')
            props.gamestate.hands.forEach((h, key) => {
                if (h.side === props.gamestate.mySeat && h.cards.length !=0) {
                    if(h.cards.length < 13){
                        props.tricks['S'].forEach((item) => {
                            var cardSuit = {}
                            cardSuit['rank']= item.rank
                            cardSuit['suit']= item.suit
                            h.cards.push(cardSuit)
                        })
                    }
                }else if(h.side === props.gamestate.myPartnerSeat && h.cards.length !=0){
                    if(h.cards.length < 13){
                        props.tricks['N'].forEach((item) => {
                            var cardSuit = {}
                            cardSuit['rank']= item.rank
                            cardSuit['suit']= item.suit
                            h.cards.push(cardSuit)
                        })
                    }
                }else if(h.side === props.gamestate.myLHOSeat && h.cards.length !=0){
                    if(h.cards.length < 13){
                        props.tricks['W'].forEach((item) => {
                            var cardSuit = {}
                            cardSuit['rank']= item.rank
                            cardSuit['suit']= item.suit
                            h.cards.push(cardSuit)
                        })
                    }
                }else if (h.side === props.gamestate.myRHOSeat && h.cards.length !=0){
                    if(h.cards.length < 13){
                        props.tricks['E'].forEach((item) => {
                            var cardSuit = {}
                            cardSuit['rank']= item.rank
                            cardSuit['suit']= item.suit
                            h.cards.push(cardSuit)
                        })
                    }
                }
            })
            props.gamestate.hands.forEach((h, key) => {
                if (h.side === props.gamestate.mySeat) {
                    const king = h.cards.filter(card => card.rank == 'K')
                    const Queen = h.cards.filter(card => card.rank == 'Q')
                    const jack = h.cards.filter(card => card.rank == 'J')
                    const ace = h.cards.filter(card => card.rank == 'A')
                    const diamond = h.cards.filter(card => card.suit == 'D')
                    const heart = h.cards.filter(card => card.suit == 'H')
                    const spade = h.cards.filter(card => card.suit == 'S')
                    const club = h.cards.filter(card => card.suit == 'C')
                    hcp = (ace.length*4)+(king.length*3)+(Queen.length*2)+(jack.length*1)
                    pattern = spade.length+','+heart.length+","+diamond.length+","+club.length
                    let patternArray = pattern.split(',').sort((a, b) => a < b ? 1 : -1)
                    spadeSuit = spade.length
                    heartsSuit = heart.length
                    diamondsSuit = diamond.length
                    clubsSuit = club.length
                    southArrayValue = tempArray.filter(state => state.direction == Strings.directionSouth).
                        map((el1) => (
                            {...el1,
                                value: el1.value.map(el => (
                                el.type == 'HCP' ? {...el, oldValue: hcp}:
                                el.type == 'Pattern' ? {...el, oldValue: patternArray.toString()} :
                                el.type == 'Spade' ? {...el, oldValue: spadeSuit} :
                                el.type == 'Heart' ? {...el, oldValue: heartsSuit} :
                                el.type == 'Diamond' ? {...el, oldValue: diamondsSuit} :
                                el.type == 'Club' ? {...el, oldValue: clubsSuit} 
                                : el
                            ))}
                    ))
                }else if(h.side === props.gamestate.myPartnerSeat){
                    const king = h.cards.filter(card => card.rank == 'K')
                    const Queen = h.cards.filter(card => card.rank == 'Q')
                    const jack = h.cards.filter(card => card.rank == 'J')
                    const ace = h.cards.filter(card => card.rank == 'A')
                    const diamond = h.cards.filter(card => card.suit == 'D')
                    const heart = h.cards.filter(card => card.suit == 'H')
                    const spade = h.cards.filter(card => card.suit == 'S')
                    const club = h.cards.filter(card => card.suit == 'C')
                    hcp = (ace.length*4)+(king.length*3)+(Queen.length*2)+(jack.length*1)
                    pattern = spade.length+','+heart.length+","+diamond.length+","+club.length
                    let patternArray = pattern.split(',').sort((a, b) => a < b ? 1 : -1)
                    spadeSuit = spade.length
                    heartsSuit = heart.length
                    diamondsSuit = diamond.length
                    clubsSuit = club.length
                    if(hcp == 0 && spadeSuit == 0 && heartsSuit == 0 && 
                        diamondsSuit== 0 && 
                        clubsSuit == 0){
                            northArrayValue = tempArray.filter(state => state.direction == Strings.directionNorth)
                    } else {
                        northArrayValue = tempArray.filter(state => state.direction == Strings.directionNorth)
                            .map((el1) => (
                                {
                                    ...el1,
                                    value: el1.value.map(el => (
                                        el.type == 'HCP' ? { ...el, oldValue: hcp } :
                                            el.type == 'Pattern' ? { ...el, oldValue: patternArray.toString() } :
                                                el.type == 'Spade' ? { ...el, oldValue: spadeSuit } :
                                                    el.type == 'Heart' ? { ...el, oldValue: heartsSuit } :
                                                        el.type == 'Diamond' ? { ...el, oldValue: diamondsSuit } :
                                                            el.type == 'Club' ? { ...el, oldValue: clubsSuit }
                                                                : el
                                    ))
                                }
                            ))
                    }
                }else if(h.side === props.gamestate.myLHOSeat ){
                    const king = h.cards.filter(card => card.rank == 'K')
                    const Queen = h.cards.filter(card => card.rank == 'Q')
                    const jack = h.cards.filter(card => card.rank == 'J')
                    const ace = h.cards.filter(card => card.rank == 'A')
                    const diamond = h.cards.filter(card => card.suit == 'D')
                    const heart = h.cards.filter(card => card.suit == 'H')
                    const spade = h.cards.filter(card => card.suit == 'S')
                    const club = h.cards.filter(card => card.suit == 'C')
                    hcp = (ace.length*4)+(king.length*3)+(Queen.length*2)+(jack.length*1)
                    pattern = spade.length+','+heart.length+","+diamond.length+","+club.length
                    let patternArray = pattern.split(',').sort((a, b) => a < b ? 1 : -1)
                    spadeSuit = spade.length
                    heartsSuit = heart.length
                    diamondsSuit = diamond.length
                    clubsSuit = club.length
                    if(hcp == 0 && spadeSuit == 0 && heartsSuit == 0 && 
                        diamondsSuit== 0 && 
                        clubsSuit == 0){
                        westArrayValue = tempArray.filter(state => state.direction == Strings.directionWest)
                    } else {
                        westArrayValue = tempArray.filter(state => state.direction == Strings.directionWest)
                            .map((el1) => (
                                {
                                    ...el1,
                                    value: el1.value.map(el => (
                                        el.type == 'HCP' ? { ...el, oldValue: hcp } :
                                            el.type == 'Pattern' ? { ...el, oldValue: patternArray.toString() } :
                                                el.type == 'Spade' ? { ...el, oldValue: spadeSuit } :
                                                    el.type == 'Heart' ? { ...el, oldValue: heartsSuit } :
                                                        el.type == 'Diamond' ? { ...el, oldValue: diamondsSuit } :
                                                            el.type == 'Club' ? { ...el, oldValue: clubsSuit }
                                                                : el
                                    ))
                                }
                            ))
                    }
                }else if (h.side === props.gamestate.myRHOSeat ){
                    const king = h.cards.filter(card => card.rank == 'K')
                    const Queen = h.cards.filter(card => card.rank == 'Q')
                    const jack = h.cards.filter(card => card.rank == 'J')
                    const ace = h.cards.filter(card => card.rank == 'A')
                    const diamond = h.cards.filter(card => card.suit == 'D')
                    const heart = h.cards.filter(card => card.suit == 'H')
                    const spade = h.cards.filter(card => card.suit == 'S')
                    const club = h.cards.filter(card => card.suit == 'C')
                    hcp = (ace.length*4)+(king.length*3)+(Queen.length*2)+(jack.length*1)
                    pattern = spade.length+','+heart.length+","+diamond.length+","+club.length
                    let patternArray = pattern.split(',').sort((a, b) => a < b ? 1 : -1)
                    spadeSuit = spade.length
                    heartsSuit = heart.length
                    diamondsSuit = diamond.length
                    clubsSuit = club.length
                    if(hcp == 0 && spadeSuit == 0 && heartsSuit == 0 && 
                        diamondsSuit== 0 && 
                        clubsSuit == 0){
                        eastArrayValue = tempArray.filter(state => state.direction == Strings.directionEast)
                    }else{
                        eastArrayValue = tempArray.filter(state => state.direction == Strings.directionEast)
                        .map((el1) => (
                                {...el1,
                                    value: el1.value.map(el => (
                                    el.type == 'HCP' ? {...el, oldValue: hcp}:
                                    el.type == 'Pattern' ? {...el, oldValue: patternArray.toString()} :
                                    el.type == 'Spade' ? {...el, oldValue: spadeSuit} :
                                    el.type == 'Heart' ? {...el, oldValue: heartsSuit} :
                                    el.type == 'Diamond' ? {...el, oldValue: diamondsSuit} :
                                    el.type == 'Club' ? {...el, oldValue: clubsSuit} 
                                    : el
                                ))}
                        ))
                    }
                   
                }
            })
            let updatedArray = []
            updatedArray.push(northArrayValue[0])
            updatedArray.push(southArrayValue[0])
            updatedArray.push(westArrayValue[0])
            updatedArray.push(eastArrayValue[0])
            setValues(updatedArray)
            setTempValues(updatedArray)
            AsyncStorage.setItem('values', JSON.stringify(updatedArray));     
    }

    const getTableMessages = (seat) => {
        if( seat === 'N'){
            if(props.chatScreen.isNorthSentMsg){
               return false
            }else{
               return true
            }
          }else if( seat === 'S'){
            if(props.chatScreen.isSouthSentMsg){
               return false
            }else{
               return true
            }
          }else if( seat === 'W'){
            if(props.chatScreen.isWestSentMsg){
               return false
            }else{
               return true
            }
          }else if( seat === 'E'){
            if(props.chatScreen.isEastSentMsg){
               return false
            }else{
               return true
            }
          }else{
            return true
          }
    }

    useEffect(() => {
        AsyncStorage.getItem('cheatsheetTableID').then(cheatsheetTableID => {
            if (cheatsheetTableID == props.tableID) {
                AsyncStorage.getItem('values').then(value => {
                    if(value == undefined){
                        getGameStateValueForMine(values) 
                    }else{
                        getGameStateValueForMine(JSON.parse(value))
                    }
                })
            } else {
                AsyncStorage.setItem('cheatsheetTableID', props.tableID);
                getGameStateValueForMine(values)
            }
        })
    }, []);
    return (
        <View style={{...styles.fullView,}}>
            {
                modalVisible ?
                <TouchableOpacity
                    onPress={() => revertOldValues(selectedItemValue)}
                    style={{ justifyContent: 'center', backgroundColor: 'transparent', 
                        width: '100%', height: '100%'}}>
                    
                    <NumberSelectView
                        playClickedDirection={playClickedDirection}
                        updateModalState={updateModalState}
                        type={type}
                        saveValuesProvided={saveValuesProvided}
                        selectedValue={selectedItemValue}
                        showUpdateValues={showUpdateValues}
                    />
                </TouchableOpacity>
                :
                <Directions 
                    currentScreen={props.currentScreen}
                    bidInfoScreen={props.bidInfoScreen}
                    playScreen={props.playScreen}
                    infoScreen={props.infoScreen}
                    gamestate={props.gamestate}
                />
            }

            <>
                <View style={styles.nInfoContainer}>
                    <InfoView direction={Strings.directionNorth}
                        value1={tempValues[0].value[0].newValue != -1 ? tempValues[0].value[0].newValue: tempValues[0].value[0].oldValue}
                        oldValue1={tempValues[0].value[0].oldValue}
                        newValue1={tempValues[0].value[0].newValue}
                        value2={tempValues[0].value[1].newValue != -1 ? tempValues[0].value[1].newValue: tempValues[0].value[1].oldValue}
                        oldValue2={tempValues[0].value[1].oldValue}
                        newValue2={tempValues[0].value[1].newValue}
                        value3={tempValues[0].value[2].newValue != -1 ? tempValues[0].value[2].newValue: tempValues[0].value[2].oldValue}
                        oldValue3={tempValues[0].value[2].oldValue}
                        newValue3={tempValues[0].value[2].newValue}
                        value4={tempValues[0].value[3].newValue != -1 ? tempValues[0].value[3].newValue: tempValues[0].value[3].oldValue}
                        oldValue4={tempValues[0].value[3].oldValue}
                        newValue4={tempValues[0].value[3].newValue}
                        value5={tempValues[0].value[4].newValue != -1 ? tempValues[0].value[4].newValue: tempValues[0].value[4].oldValue}
                        oldValue5={tempValues[0].value[4].oldValue}
                        newValue5={tempValues[0].value[4].newValue}
                        value6={tempValues[0].value[5].newValue != -1 ? tempValues[0].value[5].newValue: tempValues[0].value[5].oldValue}
                        oldValue6={tempValues[0].value[5].oldValue}
                        newValue6={tempValues[0].value[5].newValue}
                        modalVisible={modalVisible}
                        playClickedDirection={playClickedDirection}
                        type={type}
                        userTextColor={props.currentScreen == SCREENS.PLAY_SCREEN ?props.playScreen.northUserText:props.infoScreen.northUserText}
                        updateModalState={updateModalState}
                        playerDirection={props.gamestate.myPartnername}
                        showTableMessages={props.showTableMessages}
                        chatScreen={props.chatScreen}
                        reduceChar={getTableMessages('N')} 
                        style={{ infoView: styles.nInfoView, playerInfoView: styles.fdColumn }} />
                </View>
                <View style={styles.sInfoContainer}>
                    <InfoView direction={Strings.directionSouth} playerDirection={props.gamestate.myUsername}
                        value1={tempValues[1].value[0].newValue != -1 ? tempValues[1].value[0].newValue: tempValues[1].value[0].oldValue}
                        oldValue1={tempValues[1].value[0].oldValue}
                        newValue1={tempValues[1].value[0].newValue}
                        value2={tempValues[1].value[1].newValue != -1 ? tempValues[1].value[1].newValue: tempValues[1].value[1].oldValue}
                        oldValue2={tempValues[1].value[1].oldValue}
                        newValue2={tempValues[1].value[1].newValue}
                        value3={tempValues[1].value[2].newValue != -1 ? tempValues[1].value[2].newValue: tempValues[1].value[2].oldValue}
                        oldValue3={tempValues[1].value[2].oldValue}
                        newValue3={tempValues[1].value[2].newValue}
                        value4={tempValues[1].value[3].newValue != -1 ? tempValues[1].value[3].newValue: tempValues[1].value[3].oldValue}
                        oldValue4={tempValues[1].value[3].oldValue}
                        newValue4={tempValues[1].value[3].newValue}
                        value5={tempValues[1].value[4].newValue != -1 ? tempValues[1].value[4].newValue: tempValues[1].value[4].oldValue}
                        oldValue5={tempValues[1].value[4].oldValue}
                        newValue5={tempValues[1].value[4].newValue}
                        value6={tempValues[1].value[5].newValue != -1 ? tempValues[1].value[5].newValue: tempValues[1].value[5].oldValue}
                        oldValue6={tempValues[1].value[5].oldValue}
                        newValue6={tempValues[1].value[5].newValue}
                        modalVisible={modalVisible}
                        playClickedDirection={playClickedDirection}
                        type={type}
                        userTextColor={props.currentScreen == SCREENS.PLAY_SCREEN ?props.playScreen.southUserText:props.infoScreen.southUserText}
                        updateModalState={updateModalState}
                        showTableMessages={props.showTableMessages}
                        chatScreen={props.chatScreen}
                        reduceChar={getTableMessages('S')} 
                        style={{ infoView: { ...styles.sInfoView }, playerInfoView: styles.fdColumnReverse }} />
                </View>
                <View style={styles.wInfoContainer}>
                    <InfoView direction={Strings.directionWest} playerDirection={props.gamestate.myLHOname}
                        value1={tempValues[2].value[0].newValue != -1 ? tempValues[2].value[0].newValue: tempValues[2].value[0].oldValue}
                        oldValue1={(tempValues[2].value[0].oldValue)}
                        newValue1={tempValues[2].value[0].newValue}
                        value2={tempValues[2].value[1].newValue != -1 ? tempValues[2].value[1].newValue: tempValues[2].value[1].oldValue}
                        oldValue2={tempValues[2].value[1].oldValue}
                        newValue2={tempValues[2].value[1].newValue}
                        value3={tempValues[2].value[2].newValue != -1 ? tempValues[2].value[2].newValue: tempValues[2].value[2].oldValue}
                        oldValue3={tempValues[2].value[2].oldValue}
                        newValue3={tempValues[2].value[2].newValue}
                        value4={tempValues[2].value[3].newValue != -1 ? tempValues[2].value[3].newValue: tempValues[2].value[3].oldValue}
                        oldValue4={tempValues[2].value[3].oldValue}
                        newValue4={tempValues[2].value[3].newValue}
                        value5={tempValues[2].value[4].newValue != -1 ? tempValues[2].value[4].newValue: tempValues[2].value[4].oldValue}
                        oldValue5={tempValues[2].value[4].oldValue}  
                        newValue5={tempValues[2].value[4].newValue}
                        value6={tempValues[2].value[5].newValue != -1 ? tempValues[2].value[5].newValue: tempValues[2].value[5].oldValue}
                        oldValue6={tempValues[2].value[5].oldValue}
                        newValue6={tempValues[2].value[5].newValue}
                        modalVisible={modalVisible}
                        playClickedDirection={playClickedDirection}
                        type={type}
                        showTableMessages={props.showTableMessages}
                        chatScreen={props.chatScreen}
                        reduceChar={getTableMessages('W')}
                        userTextColor={props.currentScreen == SCREENS.PLAY_SCREEN ?props.playScreen.westUserText:props.infoScreen.westUserText}
                        updateModalState={updateModalState}
                        style={{
                            infoView: {
                                ...styles.weInfoView,
                                transform: [
                                    { rotate: "-90deg" },
                                ],
                            },
                            playerInfoView: {
                                ...styles.fdRow,
                                alignItems:'center',
                                justifyContent:'center'
                            },
                            rotate: { transform: [{ rotate: "-90deg" }] },
                            wePlayerNameContainer: { ...styles.wPlayerNameContainer, }
                        }} />
                </View>
                <View style={styles.eInfoContainer}>
                    <InfoView direction={Strings.directionEast}
                        value1={tempValues[3].value[0].newValue != -1 ? tempValues[3].value[0].newValue: tempValues[3].value[0].oldValue}
                        oldValue1={tempValues[3].value[0].oldValue}
                        newValue1={tempValues[3].value[0].newValue}
                        value2={tempValues[3].value[1].newValue != -1 ? tempValues[3].value[1].newValue: tempValues[3].value[1].oldValue}
                        oldValue2={tempValues[3].value[1].oldValue}
                        newValue2={tempValues[3].value[1].newValue}
                        value3={tempValues[3].value[2].newValue != -1 ? tempValues[3].value[2].newValue: tempValues[3].value[2].oldValue}
                        oldValue3={tempValues[3].value[2].oldValue}
                        newValue3={tempValues[3].value[2].newValue}
                        value4={tempValues[3].value[3].newValue != -1 ? tempValues[3].value[3].newValue: tempValues[3].value[3].oldValue}
                        oldValue4={tempValues[3].value[3].oldValue}
                        newValue4={tempValues[3].value[3].newValue}
                        value5={tempValues[3].value[4].newValue != -1 ? tempValues[3].value[4].newValue: tempValues[3].value[4].oldValue}
                        oldValue5={tempValues[3].value[4].oldValue}
                        newValue5={tempValues[3].value[4].newValue}
                        value6={tempValues[3].value[5].newValue != -1 ? tempValues[3].value[5].newValue: tempValues[3].value[5].oldValue}
                        oldValue6={tempValues[3].value[5].oldValue}
                        newValue6={tempValues[3].value[5].newValue}
                        modalVisible={modalVisible}
                        playClickedDirection={playClickedDirection}
                        type={type}
                        reduceChar={getTableMessages('E')}
                        chatScreen={props.chatScreen}
                        showTableMessages={props.showTableMessages}
                        userTextColor={props.currentScreen == SCREENS.PLAY_SCREEN ?props.playScreen.eastUserText:props.infoScreen.eastUserText}
                        updateModalState={updateModalState}
                        playerDirection={props.gamestate.myRHOname} style={{
                            infoView: {
                                ...styles.weInfoView,
                                flexDirection: 'row-reverse',
                                transform: [
                                    { rotate: "90deg" },
                                ],
                            },
                            playerInfoView: {
                                ...styles.fdRowReverse,
                            },
                            wePlayerNameContainer: { ...styles.wPlayerNameContainer, }
                        }} />
                </View>
            </>      
        </View>
    )
}

const styles = ScaledSheet.create({
    title: {
        fontFamily: 'Roboto-Regular',
        fontSize: normalize(10),
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    fullView: {
        flexDirection: 'row',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    scoreView: {
        width: scale(90),
        borderColor: Colors.rgb_353535,
        borderWidth: moderateVerticalScale(0.5),
        height: scale(340)
    },
    nInfoContainer: {
        flexDirection: 'column',
        height: scale(68),
        top: 0,
        position: 'absolute',
    },
    sInfoContainer: {
        flexDirection: 'column',
        height: scale(45),
        bottom: 0,
        position: 'absolute',
    },
    wInfoContainer: {
        width: scale(45),
        left: 0,
        position: 'absolute',
        flexDirection: 'column',
    },
    eInfoContainer: {
        width: scale(68),
        right: 0,
        position: 'absolute',
        flexDirection: 'column',
    },
    nInfoView: {
        alignItems: 'center',
    },
    weInfoView: {
        width: scale(45),
    },
    sInfoView: {
        alignItems: 'center',
    },
    rotate90Deg: {
        transform: [{ rotate: "90deg" }],
    },
    rotate90DegMinus: {
        transform: [{ rotate: "-90deg" }],
    },
    fdColumn: {
        flexDirection: 'column',
    },
    fdColumnReverse: {
        flexDirection: 'column-reverse'
    },
    fdRow: {
        flexDirection: 'row'
    },
    fdRowReverse: {
        flexDirection: 'row-reverse'
    },
    wPlayerNameContainer: {
        // transform: [
        //     { rotate: '-90deg' },
        // ],
        justifyContent: 'center',
        backgroundColor: 'red'
    },
    ePlayerNameContainer: {
        transform: [
            { rotate: '90deg' },
        ],
        alignItems: 'center'
    },
    wHcpView: {
        transform: [
            { rotate: "-90deg" },
        ]
    },
    eHcpView: {
        transform: [
            { rotate: "90deg" },
        ]
    },
    ePatternView: {
        transform: [
            { rotate: "90deg" },
        ]
    },

})

export default CheatSheet