import React from 'react'
import { View, Text } from 'react-native'
import { SvgXml } from 'react-native-svg';
import { moderateScale, scale, ScaledSheet } from 'react-native-size-matters/extend';
import { normalize } from '../styles/global';
import { SCREENS } from '../constants/screens';
import svgImages from '../API/SvgFiles';
/**
 * FlightInfo.js
 * Flight Icon on Task Details Item and Task Results
 * @param {*} props
 */
const Directions = props => {

    const getDirectionTextStyle = (side) => {
        if(props.currentScreen == SCREENS.SHARE_INFO){
            if(side == "leftSeat"){
                return props.infoScreen.DirectionTextLHO
            }else if(side == "rightSeat"){
                return props.infoScreen.DirectionTextRHO
            }else if(side == "partnerSeat"){
                return props.infoScreen.DirectionTextPartner
            }else if(side == "mySeat"){
                return props.infoScreen.DirectionTextMySeat
            }
        }else if(props.currentScreen == SCREENS.BID_INFO){
            if(side == "leftSeat"){
                return props.bidInfoScreen.DirectionTextLHO
            }else if(side == "rightSeat"){
                return props.bidInfoScreen.DirectionTextRHO
            }else if(side == "partnerSeat"){
                return props.bidInfoScreen.DirectionTextPartner
            }else if(side == "mySeat"){
                return props.bidInfoScreen.DirectionTextMySeat
            }
        }else if(props.currentScreen == SCREENS.PLAY_SCREEN){
            if(side == "leftSeat"){
                return props.playScreen.DirectionTextLHO.color
            }else if(side == "rightSeat"){
                return props.playScreen.DirectionTextRHO.color
            }else if(side == "partnerSeat"){
                return props.playScreen.DirectionTextPartner.color
            }else if(side == "mySeat"){
                return props.playScreen.DirectionTextMySeat.color
            }
        }
    }

    const getDirectionText = (side) => {
        if(side == "leftSeat"){
            return props.gamestate.myLHOSeat
        }else if(side == "rightSeat"){
            return props.gamestate.myRHOSeat
        }else if(side == "partnerSeat"){
            return props.gamestate.myPartnerSeat
        }else if(side == "mySeat"){
            return props.gamestate.mySeat
        }
    }

    return (
        <View
          style={{
            width: '100%',
            height: '100%',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              width: scale(61),
              height: scale(61),
              flexDirection: 'column',
            }}
          >
            <Text
              style={[
                styles.DirectionText,
                {
                  alignItems: 'center',
                  color: getDirectionTextStyle('partnerSeat'),
                  marginBottom: moderateScale(3)
                }
              ]}
            >
              {getDirectionText('partnerSeat')}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text
                style={[
                  styles.DirectionText,
                  {flex: 0.5, color: getDirectionTextStyle('leftSeat'), textAlign: 'center'}
                ]}
              >
                {getDirectionText('leftSeat')}
              </Text>
              <View style={{width: scale(27), height: scale(27), alignItems: 'center', justifyContent: 'center'}}>
                <SvgXml
                  width={scale(21)}
                  height={scale(21)}
                  xml={svgImages.centerCircle}
                />
              </View>
              <Text
                style={[
                  styles.DirectionText,
                  {flex: 0.5, color: getDirectionTextStyle('rightSeat')}
                ]}
              >
                {getDirectionText('rightSeat')}
              </Text>
            </View>
            <Text
              style={[
                styles.DirectionText,
                {
                  alignItems: 'center',
                  color: getDirectionTextStyle('mySeat'),
                  marginTop: moderateScale(3)
                }
              ]}
            >
              {getDirectionText('mySeat')}
            </Text>
          </View>
        </View>

    )
}

const styles = ScaledSheet.create({
    DirectionText: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(11),
        fontWeight: '400',
        color: '#DBFF00',
        textAlign: 'center'
    },
})

export default Directions