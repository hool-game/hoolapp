import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SvgXml } from 'react-native-svg';
import { moderateScale, moderateVerticalScale, scale, ScaledSheet } from 'react-native-size-matters/extend';
import svgImages from '../API/SvgFiles';
import { Strings } from '../styles/Strings';
import { normalize } from '../styles/global';
/**
 * FlightInfo.js
 * Flight Icon on Task Details Item and Task Results
 * @param {*} props
 */
const AddPlayer = props => {

    const returnView = (props) =>{
        if(props.isNorthInvited && props.directionNorth == props.direction){
            return(
                <View style={{ flexDirection: 'row', alignItems: 'center', marginStart: moderateScale(48), }}>
                <TouchableOpacity onPress={() =>
                    props.cancelRequest(Strings.directionNorth)
                }
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <SvgXml
                        height={scale(24)}
                        width={scale(24)}
                        xml={svgImages.joinSymbolPlay} />
                    <Text style={{
                        ...styles.OpenText,
                        letterSpacing: 0.2,
                        marginStart: moderateScale(8)
                    }}>{props.northUserName}</Text>
                </TouchableOpacity>
            </View>)
        } else if(props.isEastInvited && props.directionEast == props.direction){
            return(
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Text style={{
                    ...styles.OpenText,
                    letterSpacing: 0.2,
                    marginLeft: moderateScale(-10),
                    marginRight: moderateScale(8)
                }}>{props.eastUserName}</Text>
                <TouchableOpacity onPress={() =>
                    props.cancelRequest(Strings.directionEast)
                }
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <SvgXml
                        height={scale(24)}
                        width={scale(24)}
                        xml={svgImages.joinSymbolPlay} />
                </TouchableOpacity>
            </View>)
        }else if(props.isSouthInvited && props.directionSouth == props.direction){
            return(
            <View style={{ flexDirection: 'row', alignItems: 'center', marginStart: moderateScale(48), }}>
                <TouchableOpacity onPress={() =>
                    props.cancelRequest(Strings.directionSouth)
                }
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <SvgXml
                        height={scale(24)}
                        width={scale(24)}
                        xml={svgImages.joinSymbolPlay} />
                    <Text style={{
                        ...styles.OpenText,
                        letterSpacing: 0.2,
                        marginStart: moderateScale(8)
                    }}>{props.southUserName}</Text>
                </TouchableOpacity>
            </View>)
        }else if(props.isWestInvited && props.directionWest == props.direction){
            return(
            <View style={{ flexDirection: 'row', alignItems: 'center', marginStart: moderateScale(33),}}>
            <TouchableOpacity onPress={() =>
              props.cancelRequest(Strings.directionWest)
            }
              style={{
                alignItems: 'flex-start',
                justifyContent: 'center',
                flexDirection: 'row',
                alignItems: 'center'
              }}
            >
              <SvgXml
                height={scale(24)}
                width={scale(24)}
                xml={svgImages.joinSymbolPlay} />
              <Text style={{
                ...styles.OpenText,
                letterSpacing: 0.2,
                marginStart: moderateScale(8)
              }}>{props.westUserName}</Text>
            </TouchableOpacity>
          </View>)
        }else{
            return(
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: moderateVerticalScale(2.5)}}>
                <TouchableOpacity onPress={() => props.onAddPlayerClick(props.direction, props.tableID)} >
                    <SvgXml
                        height={scale(20)}
                        width={scale(20)}
                        xml={svgImages.addPlayer} />
                </TouchableOpacity>
            </View>)
        }
    }
    return (
        <View>
            {returnView(props)}
        </View>
    )
}

const styles = ScaledSheet.create({
    OpenText: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(12),
        color: '#DBFF00',
        textAlign: 'center',
    },
})

export default AddPlayer