import React from 'react'

import { View, Text, Dimensions } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { moderateScale, moderateVerticalScale, scale, ScaledSheet ,verticalScale} from 'react-native-size-matters/extend'
import { SvgXml } from 'react-native-svg'
import svgImages from '../API/SvgFiles'
import { Colors } from '../styles/Colors'
import { normalize } from '../styles/global'
import { Strings } from '../styles/Strings'
import { restrictUserNameToEightChar } from './util'


const InfoView = (props) => {

    const getStyleForText = (props, value, type, newValue) => {
        if(props.modalVisible){
            if(props.playClickedDirection == props.direction
                && props.type == type){
                    if(newValue != -1){
                        return  { ...styles.cardNumberTextEnabled, ...props.style.hcpView }
                    }else{
                        return({...styles.cardNumberText, ...props.style.hcpView})
                    }
            }else{
                return({...styles.cardNumberText, ...props.style.hcpView})
            }
        }else if(!props.modalVisible){
            if(newValue == -1){
                return({...styles.cardNumberText, ...props.style.hcpView})
            }else{
                return({...styles.cardNumberTextEnabled, ...props.style.hcpView})
            }
        }
    }
    
    return (
        <>
        {
            (props.direction == Strings.directionEast) &&
            <View 
              style={{
                position: 'absolute',
                right: scale(44),
                justifyContent:'center',
                alignItems:'center',
                height: '100%',
                width:scale(23),
                alignSelf:'center',
              }}
            >
                <TouchableOpacity
                        style={{ flexDirection:'row' }}
                        disabled={!props.chatScreen.isEastSentMsg}
                        onPress={() => {
                        props.showTableMessages()
                        }}
                >
                    <View style={{height: scale(180), justifyContent: 'center', 
                                alignItems: 'center', 
                                alignSelf: 'center',
                                width:scale(23),
                                backgroundColor: 'transparent'}}>
                        <View style={
                            { 
                                transform: [
                                { rotate: "90deg" },
                            ],
                            width:scale(180),
                            height:scale(23),
                            justifyContent: 'center', 
                            alignItems: 'center', 
                            alignSelf: 'center',
                            }}>
                            <Text style={{...styles.playerName, textAlignVertical:'center', color: props.userTextColor}} numberOfLines={1} ellipsizeMode={'tail'}>
                                {props.reduceChar ? restrictUserNameToEightChar(props.playerDirection) : props.playerDirection}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        }
        <View style={{...props.style.playerInfoView}}>
            <View style={{...styles.infoView, ...props.style.infoView, alignSelf:'center' ,justifyContent:'center'}}>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?
                    {...styles.hcpNSPatternView, flexDirection: 'column-reverse'}:
                    {...styles.hcpNSPatternView}} onPress={ () => {props.updateModalState(props.direction, 'HCP', props.value1)}}
                    disabled={props.modalVisible}
                    >
                    <View style={styles.letterOrSVGBox}>
                        <Text style={(props.type == 'HCP' && props.playClickedDirection == props.direction) ? 
                        {...styles.titleEnabled, ...props.style.hcpView} : props.modalVisible ?
                        {...styles.title, ...props.style.hcpView} :
                        {...styles.titleEnabled, ...props.style.hcpView}}>
                            {'HCP'}
                        </Text>
                    </View>
                    <View style={styles.sharedDetailBox}>
                    {props.value1 != -1 ?
                        <View 
                        style={{borderBottom: scale(1)}}
                        >
                        <Text style={getStyleForText(props, props.oldValue1, 'HCP', props.newValue1)}>{props.value1}</Text>
                        </View>
                        :
                        
                            <SvgXml
                                width={scale(10)}
                                height={verticalScale(10)}
                                style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                    { rotate: "-180deg" },
                                ]}:{ alignSelf: 'center',}}
                                xml={svgImages.dropNorth}
                                //(props.type == 'HCP' && props.playClickedDirection == props.direction) ? svgImages.dropNorthEnabled : 
                            />
                        
                    }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?{...styles.hcpNSPatternView, flexDirection: 'column-reverse'}:
                {...styles.hcpNSPatternView}} 
                    onPress={ () => {props.updateModalState(props.direction, 'Pattern',  props.value2)}}
                    disabled={props.modalVisible}
                    >
                    <View style={styles.letterOrSVGBox}> 
                        <Text style={
                            (props.type == 'Pattern' && props.playClickedDirection == props.direction) ? 
                            {...styles.titleEnabled, ...props.style.hcpView} : props.modalVisible ?
                            {...styles.title, ...props.style.hcpView}:{...styles.titleEnabled, ...props.style.hcpView}}>
                         {'Pattern'}
                        </Text>
                    </View>
                    <View style={styles.sharedDetailBox}>   
                    {
                        props.value2 != -1 ?
                        <Text style={{...getStyleForText(props, props.oldValue2, 'Pattern', props.newValue2),letterSpacing: normalize(1.92)}}>{props.value2}</Text>
                        :
                        <SvgXml
                            width={scale(10)}
                            height={scale(10)}
                            style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                { rotate: "-180deg" },
                            ]}:{ alignSelf: 'center',}}
                            xml={svgImages.dropNorth}
                            //(props.type == 'Pattern' && props.playClickedDirection == props.direction) ?svgImages.dropNorthEnabled :
                        />

                    }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?{...styles.cardShapeView, flexDirection: 'column-reverse'}:
                    {...styles.cardShapeView}} onPress={ () => {props.updateModalState(props.direction, 'Spade',  props.value3)}}
                    disabled={props.modalVisible}>
                    <View style={styles.letterOrSVGBox}>
                        <SvgXml 
                            width={scale(11)}
                            height={scale(11)} 
                            xml={((props.type == 'Spade' && props.playClickedDirection == props.direction) || !props.modalVisible) ?
                        svgImages.spade: svgImages.spadesInactive} />
                    </View>
                    <View style={styles.sharedDetailBox}>
                    {
                        props.value3 != -1 ?
                        <Text style={getStyleForText(props, props.oldValue3, 'Spade', props.newValue3)}>{props.value3}</Text>
                        :
                        <SvgXml
                            width={scale(10)}
                            height={scale(10)}
                            style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                { rotate: "-180deg" },
                            ]}:{ alignSelf: 'center',}}
                            xml={svgImages.dropNorth}
                            //(props.type == 'Spade' && props.playClickedDirection == props.direction) ? svgImages.dropNorthEnabled: 
                        />

                    }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?{...styles.cardShapeView, flexDirection: 'column-reverse'}:
                    {...styles.cardShapeView}} onPress={ () => {props.updateModalState(props.direction, 'Heart', props.value4)}} disabled={props.modalVisible}>
                    <View style={styles.letterOrSVGBox}>
                        <SvgXml 
                            width={scale(11)}
                            height={scale(11)} 
                            xml={((props.type == 'Heart' && props.playClickedDirection == props.direction) || !props.modalVisible) ?
                        svgImages.hearts: svgImages.heartsInactive} />
                    </View>
                    <View style={styles.sharedDetailBox}>
                    {
                        props.value4 != -1 ?
                        <Text style={getStyleForText(props, props.oldValue4, 'Heart', props.newValue4)}>{props.value4}</Text>
                        :
                        <SvgXml
                            width={scale(10)}
                            height={scale(10)}
                            style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                { rotate: "-180deg" },
                            ]}:{ alignSelf: 'center',}}
                            xml={svgImages.dropNorth}
                            //(props.type == 'Heart' && props.playClickedDirection == props.direction) ? svgImages.dropNorthEnabled: 
                        />

                    }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?{...styles.cardShapeView, flexDirection: 'column-reverse'}:
                    {...styles.cardShapeView}} onPress={ () => {props.updateModalState(props.direction, 'Diamond',  props.value5)}}
                    disabled={props.modalVisible}>
                    <View style={styles.letterOrSVGBox}>
                        <SvgXml 
                            width={scale(11)}
                            height={scale(11)} xml={((props.type == 'Diamond' && props.playClickedDirection == props.direction) || !props.modalVisible) ?
                        svgImages.diamond: svgImages.diamondInactive} />
                    </View>
                    <View style={styles.sharedDetailBox}>
                    {
                        props.value5 != -1 ?
                        <Text style={getStyleForText(props, props.oldValue5, 'Diamond', props.newValue5)}>{props.value5}</Text>
                        :
                        <SvgXml
                            width={scale(10)}
                            height={scale(10)}
                            style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                { rotate: "-180deg" },
                            ]}:{ alignSelf: 'center',}}
                            xml={svgImages.dropNorth}
                            //(props.type == 'Diamond' && props.playClickedDirection == props.direction) ? svgImages.dropNorthEnabled: 
                        />

                    }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={props.direction == Strings.directionSouth ?{...styles.cardShapeView, flexDirection: 'column-reverse'}:
                    {...styles.cardShapeView}} onPress={ () => {props.updateModalState(props.direction, 'Club',  props.value6)}}
                    disabled={props.modalVisible}>
                    <View style={{...styles.letterOrSVGBox, width: scale(45)}}>
                        <SvgXml  
                            width={scale(11)}
                            height={scale(11)} 
                            xml={((props.type == 'Club' && props.playClickedDirection == props.direction) || !props.modalVisible) ?
                        svgImages.clubs: svgImages.clubDisabled} />
                    </View>
                     <View style={{...styles.sharedDetailBox, width: scale(45)}}>
                    {
                        props.value6 != -1 ?
                        <Text style={getStyleForText(props, props.oldValue6, 'Club', props.newValue6)}>{props.value6}</Text>
                        :
                        <SvgXml
                            width={scale(10)}
                            height={scale(10)}
                            style={props.direction == Strings.directionSouth ?{ alignSelf: 'center', transform: [
                                { rotate: "-180deg" },
                            ]}:{ alignSelf: 'center',}}
                            xml={svgImages.dropNorth}
                            //(props.type == 'Club' && props.playClickedDirection == props.direction) ? svgImages.dropNorthEnabled: 
                        />

                    }
                    </View>
                </TouchableOpacity>
            </View>
            {
            (props.direction == Strings.directionWest) &&
            <View
            style={{
                position: 'absolute',
                left: scale(44),
                justifyContent:'center',
                alignItems:'center',
                alignSelf:'center',
                height: '100%',
                width:scale(23),
              }}
            >
            <TouchableOpacity
                    disabled={!props.chatScreen.isWestSentMsg}
                    onPress={() => {
                       props.showTableMessages()
                    }}
            >
                <View style={{height: scale(180), 
                            alignItems: 'center',
                            justifyContent:'center',
                            alignSelf:'center',
                            width: scale(23),
                            backgroundColor:'transparent' }}>
                    <View style={
                        { 
                            transform: [
                            { rotate: "-90deg" },
                            ],
                            width:scale(180),
                            height:scale(23),
                            justifyContent: 'center', 
                            alignItems: 'center', 
                            alignSelf: 'center',
                        }}>
                        <Text style={{...styles.playerName, textAlignVertical:'center',color: props.userTextColor}} numberOfLines={1} ellipsizeMode={'tail'}>
                        {props.reduceChar ? restrictUserNameToEightChar(props.playerDirection) : props.playerDirection}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
            </View>
            }
            {
                props.direction == Strings.directionNorth &&
                <View 
                    style={{
                        position: 'absolute',
                        top: scale(44),
                        justifyContent:'center',
                        alignItems:'center',
                        alignSelf:'center',
                        height: scale(23),
                        width:'100%',
                    }}
                >
                <TouchableOpacity
                    style={{ flexDirection:'row' }}
                    disabled={!props.chatScreen.isNorthSentMsg}
                    onPress={() => {
                       props.showTableMessages()
                    }}
                >
                <View style={{width: scale(180), justifyContent: 'center', 
                        alignItems: 'center', 
                        alignSelf: 'center',
                        height:scale(23),
                        backgroundColor: 'transparent'}}>
                        <Text style={{...styles.playerName, textAlignVertical:'center', color: props.userTextColor}} numberOfLines={1} ellipsizeMode={'tail'}>
                        {props.reduceChar ? restrictUserNameToEightChar(props.playerDirection) : props.playerDirection}
                        </Text>
                </View>
                </TouchableOpacity>
                </View>
           }
        </View>
           {
                props.direction == Strings.directionSouth &&
                <View 
                    style={{
                        position: 'absolute',
                        bottom: scale(44),
                        justifyContent:'center',
                        alignItems:'center',
                        alignSelf:'center',
                        height: scale(23),
                        width:'100%',
                    }}
                >
                <TouchableOpacity
                    style={{ flexDirection:'row' }}
                    disabled={!props.chatScreen.isSouthSentMsg}
                    onPress={() => {
                       props.showTableMessages()
                    }}
                >
                    <View style={{width: scale(180), justifyContent: 'center', 
                            alignItems: 'center', 
                            alignSelf: 'center',
                            height:scale(23) ,
                            backgroundColor: 'transparent'}}>
                            <Text style={{...styles.playerName, textAlignVertical:'center', color: props.userTextColor}} numberOfLines={1} ellipsizeMode={'tail'}>
                            {props.reduceChar ? restrictUserNameToEightChar(props.playerDirection) : props.playerDirection}
                            </Text>
                    </View>
                </TouchableOpacity>
                </View>
            }
        </>

    )
}
{/*  */}
const styles = ScaledSheet.create({
    title: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(11),
        color: Colors.rgb_676767,
        alignSelf: 'center',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    titleEnabled: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(11),
        color: '#FFF',
        alignSelf: 'center',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    playerName: {
        fontFamily: 'Roboto-Light',
        fontSize: normalize(11),
        textAlign: 'center',
        width: '100%',
        color: Colors.rgb_DBFF00,
    },
    infoView: {
        flexDirection: 'row',
    },
    hcpEWPatternView: {
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: scale(80),
        flexDirection: 'row',
        width: scale(45),
        transform: [
            { rotate: "-90deg" },
        ],
        backgroundColor: Colors.rgb_1C1C1C,
        borderColor: Colors.rgb_0A0A0A,
        borderWidth: moderateVerticalScale(0.5)
    },
    hcpNSPatternView: {
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: scale(45),
        width: scale(80),
        backgroundColor: Colors.rgb_1C1C1C,
        borderColor: Colors.rgb_0A0A0A,
        borderWidth: moderateVerticalScale(0.5)
    },
    hcpNSPatternViewDisabled: {
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: scale(45),
        width: scale(80),
        backgroundColor: Colors.rgb_1C1C1C,
        borderColor: Colors.rgb_0A0A0A,
        borderWidth: moderateVerticalScale(0.5)
    },
    cardShapeView: {
        alignSelf: 'center',
        justifyContent: 'center',
        height: scale(45),
        width: scale(45),
        backgroundColor: Colors.rgb_1C1C1C,
        borderColor: Colors.rgb_0A0A0A,
        borderWidth: moderateVerticalScale(0.5),
    },
    cardImageView: {
        alignSelf: 'center',
        width: scale(11),
        height: scale(11)
    },
    cardNumberText: {
        color: Colors.rgb_676767,
        alignSelf: 'center',
        fontFamily: 'Roboto-Light',
        fontSize: normalize(16),
        letterSpacing: normalize(1.92)
    },
    cardNumberTextEnabled: {
        color: Colors.rgb_FFFFFF,
        alignSelf: 'center',
        fontFamily: 'Roboto-Light',
        fontSize: normalize(16),
    },
    letterOrSVGBox:{
        height: scale(23), 
        alignItems:'center', 
        justifyContent:'center'
    },
    sharedDetailBox:{
        height: scale(22), 
        alignItems:'center', 
        justifyContent:'center',
        bottom: scale(2)
    }

})

export default InfoView