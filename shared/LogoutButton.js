import React from 'react'
import { View, Text, TouchableOpacity, } from 'react-native'
import { SvgXml } from 'react-native-svg';
import { moderateScale, scale ,verticalScale} from 'react-native-size-matters/extend';
import svgImages from '../API/SvgFiles';
import { normalize } from '../styles/global';



export const LogoutButton = (props) => {
    return(
        <>
            <View style={{
                position: 'absolute',
                width: '93%',
                height: '100%',
                zIndex: 99,
                justifyContent:'center',
                alignItems:'center',
                alignSelf:'center',
                backgroundColor:'#0a0a0acf',
                flexDirection:'column',
                borderWidth: scale(1),
                left: scale(45)
              }}>
                <View style={{
                  position:'absolute',
                  borderWidth: moderateScale(0.5),
                  justifyContent:'center',
                  alignItems:'center',
                  alignSelf:'center',
                  borderColor: '#6D6D6D'
                }}>
                  <View style={{
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: scale(180)
                  }}>
                      <TouchableOpacity
                        onPress={async () => await props.logout()}
                        style={{justifyContent: 'center',height: verticalScale(45),flexDirection: 'row', alignItems:'center' , alignSelf:'center'}}
                      >
                        <View style={{ paddingTop: scale(2)}} >
                          <SvgXml height={scale(12.5)} width={verticalScale(12)} xml={svgImages.flagActive} />
                        </View>
                        <Text
                          style={{
                            fontFamily: 'Roboto-Light',
                            fontSize: normalize(14),
                            fontWeight: '400',
                            color: '#ffffff',
                            textAlign: 'center',
                            paddingLeft: moderateScale(6)
                          }}
                        >
                          Logout
                        </Text>
                      </TouchableOpacity>
                  </View>
                </View>
            </View>
        </>
    )
}