import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { SvgXml } from 'react-native-svg';
// import SearchIcon from '../assets/Svg/group_16.svg';
import { Colors } from '../styles/Colors';
import { normalize } from '../styles/global';
import { Strings } from '../styles/Strings';
import { moderateScale, moderateVerticalScale, scale, ScaledSheet} from 'react-native-size-matters/extend';
import svgImages from '../API/SvgFiles';
import { ChatConstants } from '../redux/actions/ChatActions';
import { connect } from 'react-redux';
import { JoinTableConstants } from '../redux/actions/JoinaTableActions';

class SearchFilter extends React.Component {
  constructor(props) {
    super(props);
    console.log('search list called at '+ this.props.index)
  }
  componentDidMount() {}

  searchTableList = (text) => {
    let listTofilter = []
    if(this.props.index == 0 && !this.props.isTableUser){
      listTofilter = [...this.props.usersList]
    }else if(this.props.index == 2 && this.props.isTableUser){
      listTofilter = [...this.props.usersList]
    }
    
    let friendsList = [...this.props.friendsList]
    friendsList.forEach( (element) => {
      let idx = listTofilter.findIndex( (item) => item.name === element.name)
      listTofilter.splice(idx , 1)
    })
    let tablst = listTofilter.filter(data => data.name.toLowerCase().startsWith(text.toLowerCase()));
    this.props.updateList({
      searchList: tablst
    })
    
  };

  onCancelPlayerSelect = () => {
      this.props.closeFriendsList({
        addFriend: false,
      })
      this.props.searchChatQuery({
        searchQuery: ''
      })
  }

  render() {
    return (
      <>
        {
          this.props.addFriend &&
          <TouchableOpacity
            onPress={() => { this.onCancelPlayerSelect(true) }}
            style={{ position: 'absolute', right: moderateScale(10), top: 0}}
          >
            <SvgXml
              height={scale(45)}
              width={scale(45)}
              xml={svgImages.back}
            />
          </TouchableOpacity>
        }      
        <View style={this.props.addFriend? { ...styles.rowContainer, marginEnd: moderateVerticalScale(55) }: { ...styles.rowContainer}}>
          <SvgXml xml={svgImages.search} height={scale(15)} width={scale(11)} style={styles.iconStyle} />
          <TextInput
            placeholder={Strings.searchHintText}
            placeholderTextColor={Colors.searchPlaceHolder}
            style={{ ...styles.inputText }}
            value={this.props.searchQuery}
            onChangeText={text => {
              this.props.searchChatQuery({
                searchQuery: text
              })
              this.searchTableList(text)
            }}
            returnKeyType="search"
          />
        </View>
      </>
    );
  }
}

const styles = ScaledSheet.create({
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: Colors.searchBackGround,
    height: '35@s',
    marginStart: '17@ms',
    marginEnd: '18@ms',
    marginTop: '20@vs',
  },
  inputText: {
    fontSize: normalize(13),
    flex: 1.5,
    alignContent: 'center',
    alignItems: 'center',
    color: Colors.inputText,
    fontFamily: 'Roboto-Regular'
  },
  iconStyle: {
    alignContent: 'center',
    alignSelf: 'center',
    marginStart: '10@ms',
    marginEnd: '6@ms'
  },
});

const mapStateToProps = (state) => ({
  searchQuery: state.chatActions.searchQuery,
  friendsList: state.chatActions.friendsList,
  lobbyList: state.chatActions.lobbyList,
  tableList: state.chatActions.tableList,
  usersList: state.chatActions.usersList,
  searchQuery: state.chatActions.searchQuery,
  index: state.chatActions.index,
  addFriend: state.joinTableActions.addFriend,
  usersList: state.chatActions.usersList,
  isTableUser: state.chatActions.isTableUser
});

const mapDispatchToProps = (dispatch) => ({
  searchChatQuery: (params) =>
    dispatch({ type: ChatConstants.SEARCH_CHAT_QUERY, params: params }),
  updateList: (params) =>
    dispatch({ type: ChatConstants.SEARCH_CHAT_LIST, params: params }),
  closeFriendsList: (params) =>
      dispatch({ type: JoinTableConstants.CLOSE_FRIEND_LIST, params: params }),
  searchChatQuery: (params) =>
      dispatch({ type: ChatConstants.SEARCH_CHAT_QUERY, params: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchFilter);