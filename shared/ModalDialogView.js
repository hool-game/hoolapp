import React, { Component } from "react";
import { View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { moderateScale, scale, ScaledSheet } from "react-native-size-matters/extend";
import { SvgXml } from "react-native-svg";
import svgImages from "../API/SvgFiles";
import { Colors } from "../styles/Colors";
import { Strings } from "../styles/Strings";
import NumberSelectView from "./NumberSelectView";

const ModalDialogView = (props) => {

    return (
        // <View style={{ ...styles.numberPadContainer }}>
          <NumberSelectView
            updateModalState={props.updateModalState}
            type={props.type}
            saveValuesProvided={props.saveValuesProvided}
          />
        // </View>
    );
}
const styles = ScaledSheet.create({
    modalView: {
      // justifyContent: 'center',
      height: '100%',
      width: '100%',
      backgroundColor: 'green',
    },
    numberPadContainer: {      
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      // width: scale(129),
      // height: scale(215),
      height: '100%',
      backgroundColor:'red'
    },
  });
export default ModalDialogView
