import React, { useEffect, useState } from 'react'

import { Text, TouchableOpacity, View} from 'react-native'
import { moderateVerticalScale, ScaledSheet,scale, verticalScale, moderateScale } from 'react-native-size-matters/extend';
import { SvgXml } from 'react-native-svg';
import svgImages from '../API/SvgFiles';

import { Colors } from '../styles/Colors';
import { normalize } from '../styles/global';

const inputs = [0, 1, 2, 3, 4,svgImages.plus, 5, 6, 7, 8, 9, svgImages.minus, 10, 11, 12, 13, svgImages.clear, svgImages.tick];

const NumberSelectView = (props) => {
    const [values, enterValue] = useState(-1)
    const [clearDefaultPattern, setClearDefaultPattern] = useState(false)
    const [valueEnterd, setEnterValue] = useState(false)

    const performOnclick = (props, index, value) =>{
        let properValue = values
        if(index == 17){
            props.saveValuesProvided(values)
        }else if((props.type == 'Spade' || props.type == 'Heart' 
                || props.type == 'Diamond' ||props.type == 'Club')&& index !== 17){
            if(index == 5){
                properValue = parseInt(properValue) + 1
                enterValue(properValue)
            } else if(index == 11){
                properValue = parseInt(properValue) - 1
                enterValue(properValue)
            } else if(index == 16){
                properValue = -1
            }else{
                properValue = value
            }
            enterValue(properValue)
            props.showUpdateValues(properValue)
        }else if(props.type == 'HCP' && index !== 17){
            if(index == 5){
                properValue = parseInt(properValue) + 1
                enterValue(properValue)
            } else if(index == 11){
                properValue = parseInt(properValue) - 1
                enterValue(properValue)
            } else if(index == 16){
                properValue = -1
            }else if(!diablePattern(props.type, value,values)){
                if(properValue == -1){
                  properValue = value
                }else{
                    let hcp = properValue.toString().length
                    if(hcp == 1 && props.selectedValue !== properValue){
                        properValue = properValue.toString()+value.toString()
                    }else{
                        properValue = value.toString()
                    }
                }
            }
            setEnterValue(true)
            enterValue(properValue)
            props.showUpdateValues(properValue)
        }else if(props.type == 'Pattern' && index !== 17){
                if(index == 5){
                    if(properValue == -1){
                        properValue = properValue + 1
                    }else{
                      var splitevalue = properValue.toString().split(',')
                      if(splitevalue.length == 1){
                        let updatedValue = parseInt(splitevalue[splitevalue.length-1])
                        properValue = updatedValue + 1;
                      }else{
                        let updatedValue = parseInt(splitevalue[splitevalue.length-1])
                        if(splitevalue.length == 2){
                            properValue = splitevalue[0] + ',' + (updatedValue+1)
                        }else if(splitevalue. length == 3){
                            properValue = splitevalue[0] + ',' +splitevalue[1] + ',' + (updatedValue+1)
                        }else if(splitevalue.length == 4){
                            properValue = splitevalue[0] + ',' +splitevalue[1] + ','+splitevalue[1] + ',' + (updatedValue+1)
                        }
                      }
                    }
                    enterValue(properValue)
                } else if(index == 11){
                    if(properValue == -1){
                        properValue = properValue - 1
                    }else{
                      var splitevalue = properValue.toString().split(',')
                      if(splitevalue.length == 1){
                        let updatedValue = parseInt(splitevalue[splitevalue.length-1])
                        properValue = updatedValue - 1;
                      }else{
                        let updatedValue = parseInt(splitevalue[splitevalue.length-1])
                        if(splitevalue.length > 1){
                            splitevalue = splitevalue.splice(0,splitevalue.length-1)
                            updatedValue = parseInt(splitevalue[splitevalue.length-1])
                        }else{
                            updatedValue =  (updatedValue - 1)
                        }
                        if(splitevalue.length == 2){
                            properValue = splitevalue[0] + ',' + updatedValue
                        }else if(splitevalue. length == 3){
                            properValue = splitevalue[0] + ',' +splitevalue[1] + ',' + updatedValue
                        }else if(splitevalue.length == 4){
                            properValue = splitevalue[0] + ',' +splitevalue[1] + ','+splitevalue[2] + ',' + updatedValue
                        }else{
                            properValue = splitevalue[0]
                        }
                      }
                    }
                    enterValue(properValue)
                } else if(index == 16){
                    if(!properValue.toString().includes(',')){
                        properValue = -1
                    }else{
                        if(props.selectedValue == properValue){
                            properValue = -1
                            setClearDefaultPattern(true)
                        }else{
                            setClearDefaultPattern(false)
                            var splitevalue = properValue.toString().split(',')
                            if(splitevalue. length == 2){
                                properValue = splitevalue[0]
                            }else if(splitevalue. length == 3){
                                properValue = splitevalue[0] + ',' +splitevalue[1]
                            }else if(splitevalue. length == 4){
                                properValue = splitevalue[0] + ',' +splitevalue[1] + ','+splitevalue[2]
                            }
                        }
                    }
                    enterValue(properValue)
                }else if(!diablePattern(props.type, value,values)){
                    if(properValue == -1){
                        properValue = value
                    }else{
                        var splitevalue = properValue.toString().split(',')
                        if(splitevalue.length < 4){
                            properValue = properValue+','+value
                        }
                    }
                    enterValue(properValue)
                }
                setEnterValue(true)
                props.showUpdateValues(properValue)
        }
    }

    const checkDiableIconInPattern = (values, index) => {
        let splitValue = values.toString().split(',')
        if(splitValue.length > 1){
            let updatedValue = 0
            for(i =0; i < splitValue.length; i++){
                updatedValue = updatedValue+parseInt(splitValue[i])
            }
            values = updatedValue
        }
        if(props.type !== 'Pattern'){
            if ((index == 5 && values == 13) ||
                (index == 11 && values == 1)) {
                return true
            } else {
                return false
            }
        }else{
            if ((index == 5 && values == 13) ||
                (index == 11 && values <= 0)) {
                return true
            } else {
                return false
            }
        }
       
    }

    const loadIconInnPad = (index, value, props) => {
        if(index == 17){
            if((props.type == 'HCP' && values > 37) || 
            (props.type == 'Pattern' && values.toString().split(',').length < 4 && !clearDefaultPattern)
            ){
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43), justifyContent: 'center', 
                        alignItems: 'center',}}
                    >
                        <SvgXml style={styles.imageView} xml={svgImages.tickDisabled} />
                    </TouchableOpacity>
                )
            }else{
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43), justifyContent: 'center', 
                        alignItems: 'center',}}
                        onPress={() => { performOnclick(props, index, value) }}
                    >
                        <SvgXml style={styles.imageView} xml={value} />
                    </TouchableOpacity>
                )
            }
        }else if(index == 5){
            if((props.type == 'HCP' && values > 36 ) || (props.type !== 'HCP' && props.type !== 'Pattern' && values == 13) || (
                (props.type == 'Pattern' && checkDiableIconInPattern(values, index)))){
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                        alignItems: 'center'}}
                    >
                        <SvgXml style={styles.imageView} xml={svgImages.plusDisabled} />
                    </TouchableOpacity>
                )
            }else{
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                        alignItems: 'center',}}
                        onPress={() => { performOnclick(props, index, value) }}
                    >
                        <SvgXml style={styles.imageView} xml={value} />
                    </TouchableOpacity>
                )
            }
        }else if(index == 11){
            if((props.type == 'HCP' && values < 1 ) || (props.type !== 'HCP' && props.type !== 'Pattern' && values == 0) || (
                (props.type == 'Pattern' && checkDiableIconInPattern(values, index)))){
                return (
                    <TouchableOpacity
                    style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                    alignItems: 'center'}}
                    >
                        <SvgXml style={styles.imageView} xml={svgImages.minusDisabled} />
                    </TouchableOpacity>
                )
            }else{
                return (
                    <TouchableOpacity
                    style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                    alignItems: 'center',}}
                    onPress={() => { performOnclick(props, index, value) }}
                    >
                        <SvgXml style={styles.imageView} xml={value} />
                    </TouchableOpacity>
                )
            }
        }else if(index == 16){
            if(values == -1){
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                        alignItems: 'center'}}
                    >
                        <SvgXml style={styles.imageView} xml={svgImages.clearDisabled} />
                    </TouchableOpacity>
                )
            }else{
                return (
                    <TouchableOpacity
                        style={{width:scale(43), height:scale(43),justifyContent: 'center', 
                        alignItems: 'center',}}
                        onPress={() => { performOnclick(props, index, value) }}
                    >
                        <SvgXml style={styles.imageView} xml={value} />
                    </TouchableOpacity>
                )
            }
        }
    }

    const diablePattern = (type, value, values) => {
        if(type == 'HCP'){
            if(values > 37){
                return true
            }else if(values != -1) {
                if((values == 3) && (value == 0 || value == 1 || value == 2 || value == 3 || value == 4 || value == 5 || value == 6 || value == 7)){
                    return false
                }else if((values <= 2 && values >0) && (value == 0 || value == 1 || value == 2 || value == 3 || value == 4 || value == 5 || value == 6 || value == 7 || value == 8|| value == 9)){
                    return false
                }else if(values == 0){
                    return false
                }else{
                    if(valueEnterd){
                        return true
                    }else{
                        return false
                    }
                }
            }else{
                return false
            }
        }else{
            let splitValue = values.toString().split(',')
            if(splitValue.length > 1){
                let updatedValue = 0
                for(i =0; i < splitValue.length; i++){
                    updatedValue = updatedValue+parseInt(splitValue[i])
                }
                values = updatedValue
            }
            if(values == 13){
                if((value == 0 && splitValue.length < 4)){
                    return false
                }else{
                    if(valueEnterd){
                        return true
                    }else{
                        return false
                    }
                }
            }else if(values == 12){
                if((value == 0 && splitValue.length < 3) || value == 1){
                    return false
                }else{
                    return true
                }
            }else if(values == 11){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) || value == 2){
                    return false
                }else{
                    return true
                }
            }else if(values == 10){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) || (value == 2 && splitValue.length < 3) || value ==3){
                    return false
                }else{
                    return true
                }
            }else if(values == 9){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) || value == 4){
                    return false
                }else{
                    return true
                }
            }else if(values == 8){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) || (value == 4 && splitValue.length < 3) || value == 5){
                    return false
                }else{
                    return true
                }
            }else if(values == 7){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3) ||  value == 6){ 
                    return false
                }else{
                    return true
                }
            }else if(values == 6){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) 
                || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) 
                || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3)
                || (value == 6 && splitValue.length < 3)|| value == 7){
                    return false
                }else{
                    return true
                }
            }else if(values == 5){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) 
                || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) 
                || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3)
                || (value == 6 && splitValue.length < 3) || (value == 7 && splitValue.length < 3)|| value == 8){
                    return false
                }else{
                    return true
                }
            }else if(values == 4){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) 
                || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) 
                || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3)
                || (value == 6 && splitValue.length < 3) || (value == 7 && splitValue.length < 3)
                || (value == 8 && splitValue.length < 3)|| value == 9){
                    return false
                }else{
                    return true
                }
            }else if(values == 3){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) 
                || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) 
                || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3)
                || (value == 6 && splitValue.length < 3) || (value == 7 && splitValue.length < 3)
                || (value == 8 && splitValue.length < 3) || (value == 9 && splitValue.length < 3)||
                 value == 10){
                    return false
                }else{
                    return true
                }
            }else if(values == 2){
                if((value == 0 && splitValue.length < 3) || (value == 1 && splitValue.length < 3) 
                || (value == 2 && splitValue.length < 3)  || (value ==3 && splitValue.length < 3) 
                || (value == 4 && splitValue.length < 3)  || (value == 5  && splitValue.length < 3)
                || (value == 6 && splitValue.length < 3) || (value == 7 && splitValue.length < 3)
                || (value == 8 && splitValue.length < 3) || (value == 9 && splitValue.length < 3)||
                (value == 10 && splitValue.length < 3) || value == 11){
                    return false
                }else{
                    return true
                }
            }else if(values == 1){
                if(value == 0 || value == 1 || value == 2 || value ==3 || value == 4 || value == 5
                    || value == 6|| value == 7|| value == 8|| value == 9|| value == 10 || value == 11 || value == 12){
                    return false
                }else{
                    return true
                }
            }
        }
    }

    useEffect(() => {
        if(props.type == 'Pattern'){
            enterValue(-1)
        }else{
            enterValue(props.selectedValue)
        }
        setEnterValue(false)
    }, [])

    return (
        <View style={{alignSelf: 'center', justifyContent: 'center',width: scale(280),
                position: 'absolute',}}>
            <View style={styles.pad}>
                {inputs.map((value, index) => {
                    return (
                        <View key={index}
                        style={styles.button}>
                            {
                                index == 5 || index == 11 || index == 16 || index == 17
                                    ?
                                    loadIconInnPad(index, value, props)
                                    :
                                    <TouchableOpacity 
                                    onPress={() => { performOnclick(props, index, value)}}
                                    style={{
                                        width: scale(43),
                                        height: scale(43),
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Text 
                                        style={
                                            (props.type == 'Pattern' && diablePattern(props.type, value, values) == true) ||
                                            (props.type == 'HCP' && diablePattern(props.type, value,values)) ?
                                                styles.buttonDisabledText
                                                :
                                                styles.buttonText}>
                                            {value}
                                        </Text>
                                    </TouchableOpacity>
                            }
                        </View>                    
                    );
                })} 
            </View>
        </View>
    )
}

const styles = ScaledSheet.create({
    buttonText: {
        fontFamily: 'Roboto-Regular',
        fontSize: normalize(13),
        color: Colors.rgb_FFFFFF,
    },
    buttonDisabledText: {
        fontFamily: 'Roboto-Regular',
        fontSize: normalize(13),
        color: '#676767',
    },
    imageView: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    pad: {
        flexWrap: 'wrap',   
        flexDirection: 'row',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        width: scale(43),
        height: scale(43),
        backgroundColor: '#000',
        borderWidth: moderateScale(0.1), 
    },
    actionButton: {
        alignItems: 'center',
        justifyContent: 'center',
        width: scale(64.5),
        height: scale(38),
        borderColor: Colors.rgb_0A0A0A,
        borderWidth: moderateVerticalScale(0.3), 
    }
})

export default NumberSelectView