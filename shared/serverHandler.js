// async function handleTableCleanup(){

//     let tables = await Table.find({stage :{$ne: 'deleted'} })
//     console.log("Available tables: ", tables.length)
//     let today = new Date();
//     let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//     tables.forEach(function(table){
//       let activePlayers = table.occupants.filter(occupant => occupant.role === 'player' && occupant.status ==='active')
//       let inactiveTable = ((table.stage ==='inactive' || activePlayers.length < 4) && (table.createdTime.getMinutes() > 10) ? true : false); //table which is inactive for more than 10 minutes
//       if(inactiveTable ){
//         console.log(`${table.table_id} is an inactive table since one hour => ${table.createdTime.getMinutes()>1} => ${ table.createdTime.getMinutes()} => activeplayers = ${activePlayers.length}`)
//       }
//       let inactiveMembers = table.occupants.filter(occupant => occupant.status === 'unavailable' || occupant.status === 'left' )
//       let totalHumans = table.occupants.filter(occupant=> !occupant.bot)
//       let inactiveHumans = table.occupants.filter(occupant => occupant.status === 'unavailable' && occupant.status === 'left' && !occupant.bot)
//       if((inactiveMembers.length === table.occupants.length) || ((totalHumans.length > 0) && (totalHumans.length === inactiveHumans.length)) || (inactiveTable)){
//           console.log("In active members count: ", inactiveMembers.length, " total occupants count: ", table.occupants.length, " total humans count: ",totalHumans.length)
//           table.stage = 'deleted'
//           table.save()
//           console.log(table.table_id," is deleted")
//       }
//     })
//     return {
//       message : "Table CleanUp Success" + time
//     }
//   }


//   async function handleTableJoin (request) {
//     let events = []

//     console.log(`${request.user} wants to join ${request.table} as ${request.role} on ${request.seat} `)

//     /*
//       If the user is already active in another table. We have to make the status to left in that table
//     */
//     let oldTables = await Table.getOldTables(request.user)

//     oldTables.forEach(async (element) => {
//       if( element.table_id !== request.table)
//       await this.handleTableExit({user: request.user, table: element.table_id})
//     })

//     /**
//      * Fetch table (if it exists)
//      */

//     let table = await Table.getTableById(request.table)
//     // Disallow from deleted tables
//     if (table.stage == 'deleted') {
//       console.error(`${request.user} failed to join: Table Deleted`)
//       events.push({
//         event: 'presence',
//         to: request.user, // send to self
//         user: request.user,
//         role: request.role,

//         seat: request.seat,
//         table: request.table,
//         error: {
//           type: 'cancel',
//           tag: 'conflict',
//           text: 'Trying to join in the deleted table',
//         },
//       })
//       return events
//     }
//     if(request.role === 'currentPresence'){
//         // broadcast presences
//         for (let occupant of table.filterOccupants('active')) {
//           // inform new occupant of everyone else
//           events.push({
//             event: 'presence',
//             to: request.user,
//             user: occupant.jid,
//             role: occupant.role,
//             seat: occupant.seat,
//             table: request.table,
//           })
//         }
//         return events
//       }
//     }

//     // Fetch the tableList of a user who's status is not 'left'
// tableSchema.statics.getOldTables = async function(JID) {
//     var tables = await this.find({
//       occupants: {
//           $elemMatch: { jid: { $eq: JID } , status: { $ne: 'left'} }
//         },
//       stage: { $ne: 'deleted' }
//     },
//     { table_id : 1, _id : 0 ,stage : 1, occupants: 1}
//     )
//     return tables
//   }
  