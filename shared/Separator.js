import React from 'react'

import { View } from 'react-native'
import { ScaledSheet } from 'react-native-size-matters/extend'
import { Colors } from '../styles/Colors'


function Separator(props) {
    return (
        <View style={[styles.container, props.style]} />
    )
}

const styles = ScaledSheet.create({
    container: {
        backgroundColor: Colors.rgb_0A0A0A,
        width: moderateVerticalScale(0.5)
    }
})

export default Separator