import 'react-native-gesture-handler'
import React from 'react';
import { Provider } from 'react-redux';
import Navigator from './routes/homeStack';
import Toast from 'react-native-toast-message';
import configureStore from './redux/configureStore';

export const store = configureStore()
export default function App() {
  return (
    <Provider store={store}>
      <Navigator />
      <Toast />
    </Provider>
  );
}
